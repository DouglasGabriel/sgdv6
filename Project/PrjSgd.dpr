program PrjSgd;

uses
  Forms,
  Controls,
  Windows,
  Dialogs,
  ULogin in '..\Source\ULogin.pas' {FrmLogin},
  UPrincipal in '..\Source\UPrincipal.pas' {FrmPrincipal},
  UDm in '..\Source\UDm.pas' {DM: TDataModule},
  UInventario in '..\Source\UInventario.pas' {FrmInventario},
  UFuncProc in '..\Source\UFuncProc.pas',
  UImportaCad in '..\Source\UImportaCad.pas' {FrmImportaCad},
  UHorario in '..\Source\UHorario.pas' {FrmHorario},
  UFinalizaInvent in '..\Source\UFinalizaInvent.pas' {FrmFinalizaInvent},
  URestTransm in '..\Source\URestTransm.pas' {FrmRestTram},
  UPesquisa in '..\Source\UPesquisa.pas' {FrmLocalizar},
  UBackupInvent in '..\Source\UBackupInvent.pas' {FrmBackupInvent},
  USelecionaSecao in '..\Source\USelecionaSecao.pas' {FrmSelSecao},
  UAguarde in '..\Source\UAguarde.pas' {FrmAguarde},
  UFrmMasterLibrary in '..\Source\UFrmMasterLibrary.pas' {FrmMasterLibrary},
  ULayout in '..\Source\ULayout.pas' {FrmLayout},
  UFrmLayoutOutput in '..\Source\UFrmLayoutOutput.pas' {FrmLayoutOutput},
  UFrmProdutividade in '..\Source\UFrmProdutividade.pas' {FrmProdutividade},
  UFrmLayoutExiste in '..\Source\UFrmLayoutExiste.pas' {FrmLayoutExiste},
  UFrmUpdate in '..\Source\UFrmUpdate.pas' {FrmUpdate},
  UFrmConfigSMTP in '..\Source\UFrmConfigSMTP.pas' {FrmConfigSMTP},
  UCadInv in '..\Source\UCadInv.pas' {FrmCadInv},
  UFrmAllLayouts in '..\Source\UFrmAllLayouts.pas' {FrmAllLayouts},
  UDivergencia in '..\Source\UDivergencia.pas' {FrmDivergencia},
  Udm2 in '..\Source\Udm2.pas' {Dm2: TDataModule},
  U_RelEAN in '..\Source\U_RelEAN.pas' {FrmRelCodigo},
  UDivergenciaQtd in '..\Source\UDivergenciaQtd.pas' {FrmDivergenciaQtd},
  URuptura in '..\Source\URuptura.pas' {FrmRuptura},
  UFrmHorGlobal in '..\Source\UFrmHorGlobal.pas' {FrmHorarioGlobal},
  UFrmProdQuant in '..\Source\UFrmProdQuant.pas' {FrmProdQuant},
  URelAuditQtdSecEan in '..\Source\URelAuditQtdSecEan.pas' {FrmAuditQtdSecEan},
  UFrmAuditQtdSecEanReal in '..\Source\UFrmAuditQtdSecEanReal.pas' {FrmAuditQtdSecEanReal},
  UCadastroLayout in '..\Source\UCadastroLayout.pas' {FrmCadastroLayout},
  U_Rel_AuditSint in '..\Source\U_Rel_AuditSint.pas' {FrmAudit_Sint},
  UFrmBkp in '..\Source\UFrmBkp.pas' {FrmBkp},
  UTotalNCadastrado in '..\Source\UTotalNCadastrado.pas' {FrmNCadastrado};

{$R *.res}

var    //
hMutex: THandle; //

begin
hMutex := CreateMutex(nil, true, PChar('[{23B1EEFF-4775-4FA9-9C5-6BFEAC48F551}]'));  //
if (hMutex <> 0) and (GetLastError = 0) then   //
Begin
  Application.Initialize;
  Application.Title := 'SGD - Sistema Gerenciador de Dados';
  Application.CreateForm(TFrmLogin, FrmLogin);
  Application.CreateForm(TDM, DM);
  Application.CreateForm(TDm2, Dm2);
  Application.CreateForm(TFrmNCadastrado, FrmNCadastrado);
  Application.Run;
End
else
Application.MessageBox('O sistema j� est� aberto!', 'Mensagem do Sistema', MB_ICONERROR + MB_OK);



end.
