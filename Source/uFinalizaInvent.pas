unit uFinalizaInvent;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, ComCtrls, StdCtrls, Mask, DBCtrls, ToolWin,
  ActnMan, ActnCtrls, ExtCtrls, Buttons, sBitBtn, Data.DB, sLabel;

type
  TFrmFinalizaInvent = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    StatusBar1: TStatusBar;
    Panel3: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Panel4: TPanel;
    DBGrid1: TDBGrid;
    DBText1: TDBText;
    sBitBtn1: TsBitBtn;
    sBitBtn2: TsBitBtn;
    DBGrid2: TDBGrid;
    Label5: TLabel;
    Label6: TLabel;
    sLabelFX1: TsLabelFX;
    procedure FormShow(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure sBitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmFinalizaInvent: TFrmFinalizaInvent;

implementation

uses UDm, UInventario, UFuncProc, ULogin, URelAuditQtdSecEan;

{$R *.dfm}

procedure TFrmFinalizaInvent.FormShow(Sender: TObject);
begin
  Dm.QFinalizaInvent.Close;
end;

procedure TFrmFinalizaInvent.sBitBtn1Click(Sender: TObject);
begin

With Dm.IBHorarios Do
  begin
    Close;
    SelectSql.Clear;
    SelectSql.Add('Select * from Horario where hor_saida is null');
    Open;
  end;

  Dm.IBHorarios.First;

  Dm.IBLayoutInput.Close;
  Dm.IBLayoutInput.SelectSql.Clear;
  Dm.IBLayoutInput.SelectSql.Add('Select * from layout_input ');
  Dm.IBLayoutInput.SelectSql.Add('where cliente = :cliente ');
  Dm.IBLayoutInput.ParamByName('cliente').Value := Dm.IBInventarioINV_LOJA.AsString;
  Dm.IBLayoutInput.Open;


 if (DM.IBLayoutInputREL_AUDIT_VALOR.AsString = 'T') and (DM.IBLayoutInputREL_IMPRESSO.AsString <> 'X') then
   begin
     Application.MessageBox('O Relat�rio de Auditoria For�ada n�o foi gerado, Por favor antes de finalizar gere o Relat�rio','Mensagem do Sistema', MB_ICONERROR+ MB_OK);
   end
 else
  if (DM.IBLayoutInputREL_AUDIT_QUANT.AsString = 'T') and (DM.IBLayoutInputREL_IMPRESSO.AsString <> 'X') then
   begin
       Application.MessageBox('O Relat�rio de Auditoria For�ada n�o foi gerado, Por favor antes de finalizar gere o Relat�rio','Mensagem do Sistema', MB_ICONERROR+ MB_OK);
   end
  else
   if (DM.IBLayoutInputREL_AUDIT_QUANT.AsString = 'T') and (DM.IBLayoutInputREL_AUDIT_VALOR.AsString = 'T') and (DM.IBLayoutInputREL_IMPRESSO.AsString <> 'X') then
    begin
      Application.MessageBox('O Relat�rio de Auditoria For�ada n�o foi gerado, Por favor antes de finalizar gere o Relat�rio','Mensagem do Sistema', MB_ICONERROR+ MB_OK);
    end
   else
  if (DM.IBLayoutInputREL_AUDITORIA_PECA.AsString = 'T') and (DM.IBLayoutInputREL_IMPRESSO.AsString <> 'X') then
   begin
       Application.MessageBox('O Relat�rio de Auditoria Pe�a a Pe�a n�o foi gerado, Por favor antes de finalizar gere o Relat�rio','Mensagem do Sistema', MB_ICONERROR+ MB_OK);
   end
  else
  If (Dm.IBInventarioINV_LOJA.Value ='MRS') then
   begin
    Application.CreateForm(TFrmAuditQtdSecEan, FrmAuditQtdSecEan); // cria o form
    FrmAuditQtdSecEan.ShowModal;
    FreeAndNil(FrmAuditQtdSecEan); // libera o form da memoria
   end
  else   
  begin

   {  If Not Dm.IBHorarios.IsEmpty then
      begin //begin if
          While Not Dm.IBHorarios.Eof Do
           begin //begin while
            ShowMessage('Antes de finalizar o invent�rio, ser� preciso fechar o horario do(a) '+#13+Dm.IBhorariosROS_NOME.Value+#13+'Matr�cula '+Dm.IBHorariosROS_MATRICULA.Value);
            Dm.IBHorarios.Next;
           end; //end begin while

      end //end begin if
     Else
     begin     }

       with Dm.Query1 do
        begin
           Close;
           SQL.Clear;
           SQL.Add('SELECT * FROM LAYOUT_OUTPUT ');
           SQL.Add('WHERE CLIENTE = :CLIENTE');
           Params.ParamByName('CLIENTE').Value := DM.IBInventarioINV_LOJA.Value;
           Open;
        end;
     // if DM.Query1.FieldByName('BREAK_FILE').Value = 'T' then
         begin
            If Dm.IBContagens.RecordCount > 0 then
             Begin
              DM.QFinalizaMapeamento.Close;
              Dm.QFinalizaInvent.Close;
              Dm.QFinalizaInvent.Open;
              DM.QFinalizaMapeamento.Open;

              if (Dm.QFinalizaInvent.RecordCount = 0) AND (DM.QFinalizaMapeamento.RecordCount = 0) then
               Begin

                Dm.IBInventario.Edit;
                Dm.IBInventarioINV_STATUS.Value := 'FINALIZADO';
                FrmInventario.MainMenu1.Items[1].Items[3].Enabled := True;
                FrmInventario.MainMenu1.Items[1].Items[4].Enabled := True;
                FrmInventario.MainMenu1.Items[1].Items[5].Enabled := True;
                FrmInventario.MainMenu1.Items[1].Items[6].Enabled := True;

                If NivelUsu = 1 then
                begin
                 FrmInventario.MainMenu1.Items[1].Items[7].Enabled := True;
                 FrmInventario.MainMenu1.Items[1].Items[8].Enabled := True;
                end;


                If (Dm.IBInventarioINV_LOJA.Value ='CLB') then
                FrmInventario.MainMenu1.Items[1].Items[12].Enabled := True;

                FrmInventario.Panel4.Enabled := False;
                FrmInventario.Panel15.Enabled := False;
                FrmInventario.Panel9.Enabled := False;
                FrmInventario.Panel7.Enabled := False;
                FrmInventario.Panel8.Enabled := False;
                FrmInventario.Panel11.Enabled := False;
                FrmInventario.Panel12.Enabled := False;
                FrmInventario.Panel13.Enabled := False;
                FrmInventario.MainMenu1.Items[0].Items[0].Caption:= 'Reabrir Invent�rio';
                Application.MessageBox('N�o ser� poss�vel fazer nenhum altera��o no invent�rio enquanto ele estiver Finalizado','Mensagem do Sistema', MB_ICONINFORMATION + MB_OK);
                Dm.IBInventario.Post;
                Dm.Transacao.CommitRetaining;

                Dm.IBHorarios.Close;
                Dm.IBHorarios.SelectSql.Clear;
                Dm.IBHorarios.SelectSql.Add('Select * from Horario');
                Dm.IBHorarios.Open;
               End
              else
               begin
                  Application.MessageBox('Ainda existem as Pend�ncias abaixo','Mensagem do Sistema', MB_ICONEXCLAMATION + MB_OK);
                  Dm.IBHorarios.Close;
                  Dm.IBHorarios.SelectSql.Clear;
                  Dm.IBHorarios.SelectSql.Add('Select * from Horario');
                  Dm.IBHorarios.Open;
               end;
             End
            else
              begin
                Application.MessageBox('N�o foi contado nenhum item. N�o � poss�vel finalizar','Mensagem do Sistema', MB_ICONEXCLAMATION + MB_OK);
                Dm.IBHorarios.Close;
                Dm.IBHorarios.SelectSql.Clear;
                Dm.IBHorarios.SelectSql.Add('Select * from Horario');
                Dm.IBHorarios.Open;
              end;
         end;
         //Evandro (O sistema s� vai finalizar se todos os ranges do mapeamento estiverem parametrizados por loja ou dep�sito, devido a isso o c�digo abaixo foi comentado)
      { else
               begin

           If Dm.IBContagens.RecordCount > 0 then
             Begin
              Dm.QFinalizaInvent.Close;
              Dm.QFinalizaInvent.Open;

              if (Dm.QFinalizaInvent.RecordCount = 0) then
               Begin
                Dm.IBInventario.Edit;
                Dm.IBInventarioINV_STATUS.Value := 'FINALIZADO';
                FrmInventario.MainMenu1.Items[1].Items[3].Enabled := True;
                FrmInventario.MainMenu1.Items[1].Items[4].Enabled := True;
                FrmInventario.MainMenu1.Items[1].Items[5].Enabled := True;
                FrmInventario.MainMenu1.Items[1].Items[6].Enabled := True;

                If NivelUsu = 1 then
                 begin
                  FrmInventario.MainMenu1.Items[1].Items[7].Enabled := True;
                  FrmInventario.MainMenu1.Items[1].Items[8].Enabled := True;
                 end;

                If (Dm.IBInventarioINV_LOJA.Value ='CLB') then
                FrmInventario.MainMenu1.Items[1].Items[12].Enabled := True;

                FrmInventario.Panel4.Enabled := False;
                FrmInventario.Panel15.Enabled := False;
                FrmInventario.Panel9.Enabled := False;
                FrmInventario.Panel7.Enabled := False;
                FrmInventario.Panel8.Enabled := False;
                FrmInventario.Panel11.Enabled := False;
                FrmInventario.Panel12.Enabled := False;
                FrmInventario.Panel13.Enabled := False;
                FrmInventario.MainMenu1.Items[0].Items[0].Caption:= 'Reabrir Invent�rio';
                Application.MessageBox('N�o ser� poss�vel fazer nenhum altera��o no invent�rio enquanto ele estiver Finalizado','Mensagem do Sistema', MB_ICONINFORMATION + MB_OK);
                Dm.IBInventario.Post;
                Dm.Transacao.CommitRetaining;

                Dm.IBHorarios.Close;
                Dm.IBHorarios.SelectSql.Clear;
                Dm.IBHorarios.SelectSql.Add('Select * from Horario');
                Dm.IBHorarios.Open;
               End
              else
               begin
                  Application.MessageBox('Ainda existem as Pend�ncias abaixo','Mensagem do Sistema', MB_ICONEXCLAMATION + MB_OK);
                  Dm.IBHorarios.Close;
                  Dm.IBHorarios.SelectSql.Clear;
                  Dm.IBHorarios.SelectSql.Add('Select * from Horario');
                  Dm.IBHorarios.Open;
               end;
             End
           else
              begin
                Application.MessageBox('N�o foi contado nenhum item. N�o � poss�vel finalizar','Mensagem do Sistema', MB_ICONEXCLAMATION + MB_OK);
                Dm.IBHorarios.Close;
                Dm.IBHorarios.SelectSql.Clear;
                Dm.IBHorarios.SelectSql.Add('Select * from Horario');
                Dm.IBHorarios.Open;
              end;
         end; }
    // end;
  end;

end;


procedure TFrmFinalizaInvent.sBitBtn2Click(Sender: TObject);
begin
    Dm.IBInventario.Edit;
    Dm.IBInventarioINV_STATUS.Value := 'ABERTO';
    FrmInventario.MainMenu1.Items[1].Items[3].Enabled := False;
    FrmInventario.MainMenu1.Items[1].Items[4].Enabled := False;
    FrmInventario.MainMenu1.Items[1].Items[5].Enabled := False;
    FrmInventario.MainMenu1.Items[1].Items[6].Enabled := False;
    FrmInventario.MainMenu1.Items[1].Items[7].Enabled := False;
    FrmInventario.MainMenu1.Items[1].Items[8].Enabled := False;
    FrmInventario.Panel4.Enabled := True;
    FrmInventario.Panel15.Enabled := True;
    FrmInventario.Panel9.Enabled := True;
    FrmInventario.Panel7.Enabled := True;
    FrmInventario.Panel8.Enabled := True;
    FrmInventario.Panel11.Enabled := True;
    FrmInventario.Panel12.Enabled := True;
    FrmInventario.Panel13.Enabled := True;
    FrmInventario.MainMenu1.Items[0].Items[0].Caption:= 'Finalizar Invent�rio';
    Dm.IBInventario.Post;
    Dm.Transacao.CommitRetaining;

    //Apaga arquivos gerados
 //Apagar todos os diret�rios com dados do sistema
  If DirectoryExists('C:\SGD\Horas Conferentes') then
    Begin
     DeleteDir(Self.Handle,'C:\SGD\Horas Conferentes');
    End;

  If DirectoryExists('C:\SGD\Relatorios') then
    Begin
     DeleteDir(Self.Handle,'C:\SGD\Relatorios');
    End;

  If DirectoryExists('C:\SGD\Arquivo Final') then
    Begin
     DeleteDir(Self.Handle,'C:\SGD\Arquivo Final');
    End;

  //Procedure para criar novamente os diret�rios do sistema. Caso n�o exista � criado
     If not DirectoryExists('C:\Sgd\Cadastro') then
    Begin
     CreateDir('C:\Sgd\Cadastro');
    End;

  If not DirectoryExists('C:\SGD\Horas Conferentes') then
    Begin
     CreateDir('C:\SGD\Horas Conferentes');
    End;

  If not DirectoryExists('C:\SGD\Relatorios') then
    Begin
     CreateDir('C:\SGD\Relatorios');
    End;

  If not DirectoryExists('C:\SGD\Arquivo Final') then
    Begin
     CreateDir('C:\SGD\Arquivo Final');
    End;

    Application.MessageBox('Invent�rio reaberto. Foram apagados o Arquivo Final, Horas dos Conferentes e Relat�rios.','Mensagem do Sistema', MB_ICONINFORMATION + MB_OK);

end;

end.
