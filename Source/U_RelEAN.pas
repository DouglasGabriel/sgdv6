unit U_RelEAN;

interface

uses
 Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ComCtrls, ExtCtrls, RpRave,
  DB, RpCon, RpConDS, RpBase, RpSystem, RpDefine, Grids, DBGrids, RpRender,
  RpRenderPDF, RpRenderText, sBitBtn, sLabel;

type
  TFrmRelCodigo = class(TForm)
    Panel3: TPanel;
    Label2: TLabel;
    Edt_Codigo: TEdit;
    BitBtn1: TBitBtn;
    RadioGroup1: TRadioGroup;
    sLabelFX1: TsLabelFX;
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmRelCodigo: TFrmRelCodigo;

implementation

uses UDm;

{$R *.dfm}

procedure TFrmRelCodigo.BitBtn1Click(Sender: TObject);
var codigo,sql:string;
begin
  codigo:=Edt_Codigo.Text;
  if RadioGroup1.ItemIndex = 0 then
  begin
   try
    try
      Dm.QRelEAN.Close;
      DM.QRelEAN.SQL.Clear;
      sql := 'SELECT ca.cad_ean, sum(CO.QTD) as QTD,CA.CAD_UN,';
      sql:= sql + ' CA.CAD_DESC, CA.CAD_PRECO,CA.CAD_CODINT,';
      sql:= sql + ' (CA.CAD_PRECO * SUM(CO.QTD)) as PRECOTOTAL';
      sql:= sql + ' from CONTAGENS_CONSO CO INNER join CADASTRO CA on (CA.CAD_CODINT = CO.CAD_CODINT)';
      sql:= sql + ' where (ca.cad_codint = :CODIGO)';
      sql:= sql + ' group by CA.CAD_CODINT,ca.cad_ean, CA.CAD_DESC, CA.CAD_PRECO,CA.CAD_CODINT,CAD_UN';
      sql:= sql + ' ORDER BY CA.CAD_EAN,CA.CAD_CODINT';
      Dm.QRelEAN.SQL.Add(sql);
      Dm.QRelEAN.Close;
      Dm.QRelEAN2.Close; 
      DM.QRelEAN.ParamByName('CODIGO').Value := Edt_Codigo.Text;
      DM.QRelEAN2.ParamByName('CODIGO').Value := Edt_Codigo.Text;
      Dm.QRelEAN.Open;
      Dm.QRelEAN2.Open;
      If Dm.QRelEAN.IsEmpty or Dm.QRelEAN2.IsEmpty then
        ShowMessage('C�digo PLU n�o encontrado, tente novamente')
      else
     DM.RvPrjWis.Close;
     Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Rel_Ean.rav';
     DM.RvPrjWis.Open;
     Dm.RvSysWis.DefaultDest := rdPreview;
     Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
     Dm.RvPrjWis.ExecuteReport('Rel_Ean');
    except
      ShowMessage('C�digo Interno n�o encontrado');
    end;
   Finally
      Dm.QRelEAN.Close;
      Dm.QRelEAN2.Close;
   end
  end
  else
  begin
  try
   try
      Dm.QRelEAN.Close;
      DM.QRelEAN.SQL.Clear;
      sql := 'SELECT ca.cad_ean, sum(CO.QTD) as QTD,CA.CAD_UN,';
      sql:= sql + ' CA.CAD_DESC, CA.CAD_PRECO,CA.CAD_CODINT,';
      sql:= sql + ' (CA.CAD_PRECO * SUM(CO.QTD)) as PRECOTOTAL';
      sql:= sql + ' from CONTAGENS_CONSO CO INNER join CADASTRO CA on (CA.CAD_EAN = CO.CON_EAN)';
      sql:= sql + ' where (ca.cad_ean = :CODIGO)';
      sql:= sql + ' group by CA.CAD_CODINT,ca.cad_ean, CA.CAD_DESC, CA.CAD_PRECO,CA.CAD_CODINT,CAD_UN';
      sql:= sql + ' ORDER BY CA.CAD_EAN,CA.CAD_CODINT';
      Dm.QRelEAN.SQL.Add(sql); 
      Dm.QRelEAN.Close;
      Dm.QRelEAN2.Close;
      DM.QRelEAN.ParamByName('CODIGO').Value := Edt_Codigo.Text;
      DM.QRelEAN2.ParamByName('CODIGO').Value := Edt_Codigo.Text;
      Dm.QRelEAN.Open;
      Dm.QRelEAN2.Open;
      If Dm.QRelEAN.IsEmpty or Dm.QRelEAN2.IsEmpty then
        ShowMessage('C�digo EAN n�o encontrado, tente novamente')
      else
     DM.RvPrjWis.Close;
     Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Rel_Ean.rav';
     DM.RvPrjWis.Open;
     Dm.RvSysWis.DefaultDest := rdPreview;
     Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
     Dm.RvPrjWis.ExecuteReport('Rel_Ean');
   except
      ShowMessage('C�digo EAN n�o encontrado');
   end;
    Finally
      Dm.QRelEAN.Close;
      Dm.QRelEAN2.Close;
   end;
  end;
end;

end.
