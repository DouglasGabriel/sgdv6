unit UFrmConfigSMTP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, IniFiles, DBCtrls, Mask, sButton, Buttons, sBitBtn,
  ImgList, System.ImageList;

type
  TFrmConfigSMTP = class(TForm)
    sButton1: TsButton;
    GroupBox1: TGroupBox;
    Label8: TLabel;
    EdtPorta: TDBEdit;
    Label4: TLabel;
    Label3: TLabel;
    Label2: TLabel;
    Label1: TLabel;
    EdtHost: TDBEdit;
    EdtUser: TDBEdit;
    EdtPass: TDBEdit;
    EdtNomeExibicao: TDBEdit;
    GroupBox2: TGroupBox;
    CmbDestinatarios: TDBMemo;
    Label9: TLabel;
    Label10: TLabel;
    sBitBtn2: TsBitBtn;
    sBitBtn3: TsBitBtn;
    aaa: TsBitBtn;
    ImageList1: TImageList;
    procedure BmsXPButton1Click(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
    procedure sBitBtn3Click(Sender: TObject);
    procedure sBitBtn2Click(Sender: TObject);
    procedure aaaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmConfigSMTP: TFrmConfigSMTP;


implementation

Uses Udm;
{$R *.dfm}

procedure TFrmConfigSMTP.BmsXPButton1Click(Sender: TObject);
var
  ServerIni: TIniFile;
  passivo: string;
begin

end;

procedure TFrmConfigSMTP.sButton1Click(Sender: TObject);
begin
Screen.Cursor:=crSqlWait;
  try
    Dm.IBMAIL.Post;
    Dm.Transacao.CommitRetaining;
    Application.MessageBox('Configuração salva com sucesso!', 'Mensagem do Sistema', MB_ICONINFORMATION + MB_OK);

    Close;
  Except
    Application.MessageBox('Erro ao salvar as configurações. Favor verificar.', 'Mensagem do Sistema', MB_ICONERROR + MB_OK);
    Screen.Cursor:=crDefault;
  End;
Screen.Cursor:=crDefault;
End;



procedure TFrmConfigSMTP.sBitBtn3Click(Sender: TObject);
begin
Close;
end;

procedure TFrmConfigSMTP.sBitBtn2Click(Sender: TObject);
begin
Screen.Cursor:=crSqlWait;
Try
  Dm.IBMAIL.Delete;
  Dm.Transacao.CommitRetaining;
  Application.MessageBox('Configuração excluída', 'Mensagem do Sistema', MB_ICONINFORMATION + MB_OK);
  Screen.Cursor:=crDefault;
except
  Application.MessageBox('Erro ao excluir as configurações', 'Mensagem do Sistema', MB_ICONERROR + MB_OK);
  Screen.Cursor:=crDefault;
end;
Screen.Cursor:=crDefault;
End;

procedure TFrmConfigSMTP.aaaClick(Sender: TObject);
begin
Dm.IBMAIL.First;
Dm.IBMAIL.Last;
If Dm.IBMAIL.RecordCount > 0 then Dm.IBMAIL.Edit  else
Dm.IBMAIL.Insert;

end;

procedure TFrmConfigSMTP.FormActivate(Sender: TObject);
begin
Dm.IBMAIL.Open;
end;

end.
