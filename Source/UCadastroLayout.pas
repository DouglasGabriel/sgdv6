unit UCadastroLayout;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls, DB, Grids, DBGrids, Buttons, sLabel;

type
  TFrmCadastroLayout = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GroupBox1: TGroupBox;
    DBGrid1: TDBGrid;
    Label2: TLabel;
    edtcliente: TEdit;
    BitBtn1: TBitBtn;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Edit1: TEdit;
    BitBtn2: TBitBtn;
    DBGrid2: TDBGrid;
    sLabelFX1: TsLabelFX;
    procedure FormShow(Sender: TObject);
    procedure DBGrid1ColExit(Sender: TObject);
    procedure DBGrid2ColExit(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn2Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmCadastroLayout: TFrmCadastroLayout;

implementation
uses UfuncProc, Udm;

{$R *.dfm}



procedure TFrmCadastroLayout.FormShow(Sender: TObject);
begin
Dm.IBLayoutInput.Open;
Dm.IBLayoutOutput.Open;
end;

procedure TFrmCadastroLayout.DBGrid1ColExit(Sender: TObject);
begin
If (Dm.DsLayoutInput.State = Dsedit) then
Dm.IBLayoutInput.Post;
Dm.Transacao.CommitRetaining;
end;

procedure TFrmCadastroLayout.DBGrid2ColExit(Sender: TObject);
begin
If (Dm.DsLayoutOutput.State = Dsedit) then
Dm.IBLayoutOutput.Post;
Dm.Transacao.CommitRetaining;
end;

procedure TFrmCadastroLayout.BitBtn1Click(Sender: TObject);
begin
If Dm.IBLayoutInput.IsEmpty then ShowMessage('Cliente N�o Encontrado') else
begin
If EdtCliente.Text = '' then
  begin
    with Dm.IBLayoutInput do
      begin
        close;
        SelectSql.Clear;
        SelectSql.add ('SELECT * FROM LAYOUT_INPUT');
        Open;
      end;
  end
else

with Dm.IBLayoutInput do
begin
     close;
     SelectSql.Clear;
     SelectSql.add ('SELECT * FROM LAYOUT_INPUT WHERE CLIENTE = :CLIENTE ');
     Parambyname('CLIENTE').AsString := edtCliente.text;

     Open;

end;
end;
end;

procedure TFrmCadastroLayout.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
If Key = VK_DELETE then
  begin
    Dm.IBLayoutInput.Delete;
  End;
end;

procedure TFrmCadastroLayout.DBGrid2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
If Key = VK_DELETE then
  begin
    Dm.IBLayoutOutput.Delete;
  End;
end;

procedure TFrmCadastroLayout.BitBtn2Click(Sender: TObject);
begin
If Dm.IBLayoutOutput.IsEmpty then ShowMessage('Cliente N�o Encontrado') else
begin
If Edit1.Text = '' then
  begin
    with Dm.IBLayoutOutput do
      begin
        close;
        SelectSql.Clear;
        SelectSql.add ('SELECT * FROM LAYOUT_OUTPUT');
        Open;
      end;
  end
else

with Dm.IBLayoutOutput do
begin
     close;
     SelectSql.Clear;
     SelectSql.add ('SELECT * FROM LAYOUT_OUTPUT WHERE CLIENTE = :CLIENTE ');
     Parambyname('CLIENTE').AsString := edit1.text;

     Open;

end;
end;
end;

procedure TFrmCadastroLayout.PageControl1Change(Sender: TObject);
begin
Dm.IBLayoutOutput.Close;
Dm.IBLayoutOutput.Open;
Dm.IBLayoutInput.Close;
Dm.IBLayoutInput.Open;
end;

end.
