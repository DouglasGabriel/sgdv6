unit URelAuditQtdSecEan;

interface

uses
   Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ComCtrls, ExtCtrls, sBitBtn, RpDefine, RpRender,
  RpRenderPDF, RpRenderText, DBCtrls, sLabel;

type
  TFrmAuditQtdSecEan = class(TForm)
    Panel2: TPanel;
    Panel3: TPanel;
    Label2: TLabel;
    Lbl_ValorFinal: TLabel;
    Edit1: TEdit;
    sBitBtn1: TsBitBtn;
    Edit2: TEdit;
    StatusBar1: TStatusBar;
    rvTxt: TRvRenderText;
    rvPdf: TRvRenderPDF;
    sLabelFX1: TsLabelFX;
    procedure sBitBtn1Click(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure Edit2KeyPress(Sender: TObject; var Key: Char);
    procedure Edit1Exit(Sender: TObject);
    procedure Edit2Exit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmAuditQtdSecEan: TFrmAuditQtdSecEan;

implementation

uses UDm, UInventario;

{$R *.dfm}

procedure TFrmAuditQtdSecEan.sBitBtn1Click(Sender: TObject);
var NivelUsu :Integer;
begin
Try
  try

    If (Edit1.Text <> '') and  (Edit2.Text <> '') then
      begin
       if StrToInt(Edit2.Text) < StrToInt(Edit1.Text) then
        begin
         ShowMessage('A Quantidade inicial deve ser menor que quantidade final. Digite um intervalo v�lido');
         Edit1.Clear;
         Edit2.Clear;
         Edit1.SetFocus;
        end
       else
        begin
          Dm.QRelAudQtdSecEan.Close;
          Dm.QRelAudQtdSecEan.ParamByName('QTDINI').Value := Edit1.Text;
          Dm.QRelAudQtdSecEan.ParamByName('QTDFIM').Value := Edit2.Text;
          Dm.QRelAudQtdSecEan.Open;

          if Dm.QRelAudQtdSecEan.RecordCount > 0 then
            begin
             Dm.RvPrjWis.Close;
             Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\AuditoriaEanSecValor.rav';
             Dm.RvPrjWis.Open;
             Dm.RvSysWis.DefaultDest := rdPreview;
             Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
             Dm.RvPrjWis.ExecuteReport('RelEanSecQtd');

              // Salvando relat�rio em PDF automaticamente
              Try
                Dm.RvSysWis.DefaultDest:= rdFile;
                Dm.RvSysWis.DoNativeOutput:= false;
                Dm.RvSysWis.RenderObject:= rvPdf;
                Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Relatorio de Auditoria Se��o por faixa de Quantidade_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
                Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
                Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\AuditoriaEanSecValor.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
                Dm.RvPrjWis.Engine:= Dm.RvSysWis;
                Dm.RvPrjWis.Execute;
              Except
                ShowMessage('Erro ao gerar o relat�rio em PDF');
              end;

              // Salvando relat�rio em TXT automaticamente
              Try
                Dm.RvSysWis.DefaultDest:= rdFile;
                Dm.RvSysWis.DoNativeOutput:= false;
                Dm.RvSysWis.RenderObject:= rvTxt;
                Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Relatorio de Auditoria Se��o por faixa de Quantidade_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.txt'; //caminho onde vai gerar o arquivo pdf
                Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
                Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\AuditoriaEanSecValor.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
                Dm.RvPrjWis.Engine:= Dm.RvSysWis;
                Dm.RvPrjWis.Execute;
              except
                ShowMessage('Erro ao gerar o relat�rio em TXT');
              end;

               with Dm.Query1 do
                begin
                   Close;
                   SQL.Clear;
                   SQL.Add('SELECT * FROM LAYOUT_OUTPUT ');
                   SQL.Add('WHERE CLIENTE = :CLIENTE');
                   Params.ParamByName('CLIENTE').Value := DM.IBInventarioINV_LOJA.Value;
                   Open;
                end;
             // if DM.Query1.FieldByName('BREAK_FILE').Value = 'T' then
                 begin
                    If Dm.IBContagens.RecordCount > 0 then
                     Begin
                      DM.QFinalizaMapeamento.Close;
                      Dm.QFinalizaInvent.Close;
                      Dm.QFinalizaInvent.Open;
                      DM.QFinalizaMapeamento.Open;

                      if (Dm.QFinalizaInvent.RecordCount = 0) AND (DM.QFinalizaMapeamento.RecordCount = 0) then
                       Begin

                        Dm.IBInventario.Edit;
                        Dm.IBInventarioINV_STATUS.Value := 'FINALIZADO';
                        FrmInventario.MainMenu1.Items[1].Items[3].Enabled := True;
                        FrmInventario.MainMenu1.Items[1].Items[4].Enabled := True;
                        FrmInventario.MainMenu1.Items[1].Items[5].Enabled := True;
                        FrmInventario.MainMenu1.Items[1].Items[6].Enabled := True;

                        If NivelUsu = 1 then
                        begin
                         FrmInventario.MainMenu1.Items[1].Items[7].Enabled := True;
                         FrmInventario.MainMenu1.Items[1].Items[8].Enabled := True;
                        end;


                        If (Dm.IBInventarioINV_LOJA.Value ='CLB') then
                        FrmInventario.MainMenu1.Items[1].Items[12].Enabled := True;

                        FrmInventario.Panel4.Enabled := False;
                        FrmInventario.Panel15.Enabled := False;
                        FrmInventario.Panel9.Enabled := False;
                        FrmInventario.Panel7.Enabled := False;
                        FrmInventario.Panel8.Enabled := False;
                        FrmInventario.Panel11.Enabled := False;
                        FrmInventario.Panel12.Enabled := False;
                        FrmInventario.Panel13.Enabled := False;
                        FrmInventario.MainMenu1.Items[0].Items[0].Caption:= 'Reabrir Invent�rio';
                        Application.MessageBox('N�o ser� poss�vel fazer nenhum altera��o no invent�rio enquanto ele estiver Finalizado','Mensagem do Sistema', MB_ICONINFORMATION + MB_OK);
                        Dm.IBInventario.Post;
                        Dm.Transacao.CommitRetaining;

                        Dm.IBHorarios.Close;
                        Dm.IBHorarios.SelectSql.Clear;
                        Dm.IBHorarios.SelectSql.Add('Select * from Horario');
                        Dm.IBHorarios.Open;
                       End
                      else
                       begin
                          Application.MessageBox('Ainda existem as Pend�ncias abaixo','Mensagem do Sistema', MB_ICONEXCLAMATION + MB_OK);
                          Dm.IBHorarios.Close;
                          Dm.IBHorarios.SelectSql.Clear;
                          Dm.IBHorarios.SelectSql.Add('Select * from Horario');
                          Dm.IBHorarios.Open;
                       end;
                     End
                    else
                      begin
                        Application.MessageBox('N�o foi contado nenhum item. N�o � poss�vel finalizar','Mensagem do Sistema', MB_ICONEXCLAMATION + MB_OK);
                        Dm.IBHorarios.Close;
                        Dm.IBHorarios.SelectSql.Clear;
                        Dm.IBHorarios.SelectSql.Add('Select * from Horario');
                        Dm.IBHorarios.Open;
                      end;
                 end;
                FrmAuditQtdSecEan.Close;
            end
          else
            begin
             Application.MessageBox('N�o existem produtos nessa faixa de quantidade','Mensagem do sistema', MB_ICONEXCLAMATION+ MB_OK);
            end;
        end;
      end
    else
      Begin
       ShowMessage('N�o foi informado o intervalo de quantidades. O sistema ir� atribuir quantidades de 1 at� 100');
       Dm.QRelAudQtdSecEan.Close;
       Dm.QRelAudQtdSecEan.ParamByName('QTDINI').Value := 1;
       Dm.QRelAudQtdSecEan.ParamByName('QTDFIM').Value := 100;
       Dm.QRelAudQtdSecEan.Open;

        if Dm.QRelAudQtdSecEan.RecordCount > 0 then
         begin
          Dm.RvPrjWis.Close;
          Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\AuditoriaEanSecValor.rav';
          Dm.RvPrjWis.Open;
          Dm.RvSysWis.DefaultDest := rdPreview;
          Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
          Dm.RvPrjWis.ExecuteReport('RelEanSecQtd');

                      // Salvando relat�rio em PDF automaticamente
            Try
              Dm.RvSysWis.DefaultDest:= rdFile;
              Dm.RvSysWis.DoNativeOutput:= false;
              Dm.RvSysWis.RenderObject:= rvPdf;
              Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Relatorio de Auditoria Se��o por faixa de Quantidade_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
              Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
              Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\AuditoriaEanSecValor.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
              Dm.RvPrjWis.Engine:= Dm.RvSysWis;
              Dm.RvPrjWis.Execute;
            Except
              ShowMessage('Erro ao gerar o relat�rio em PDF');
            end;

            // Salvando relat�rio em TXT automaticamente
            Try
              Dm.RvSysWis.DefaultDest:= rdFile;
              Dm.RvSysWis.DoNativeOutput:= false;
              Dm.RvSysWis.RenderObject:= rvTxt;
              Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Relatorio de Auditoria Se��o por faixa de Quantidade_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.txt'; //caminho onde vai gerar o arquivo pdf
              Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
              Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\AuditoriaEanSecValor.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
              Dm.RvPrjWis.Engine:= Dm.RvSysWis;
              Dm.RvPrjWis.Execute;
            except
              ShowMessage('Erro ao gerar o relat�rio em TXT');
            end;

             with Dm.Query1 do
             begin
                Close;
                SQL.Clear;
                SQL.Add('SELECT * FROM LAYOUT_OUTPUT ');
                SQL.Add('WHERE CLIENTE = :CLIENTE');
                Params.ParamByName('CLIENTE').Value := DM.IBInventarioINV_LOJA.Value;
                Open;
             end;

              begin
                 If Dm.IBContagens.RecordCount > 0 then
                  Begin
                   DM.QFinalizaMapeamento.Close;
                   Dm.QFinalizaInvent.Close;
                   Dm.QFinalizaInvent.Open;
                   DM.QFinalizaMapeamento.Open;

                   if (Dm.QFinalizaInvent.RecordCount = 0) AND (DM.QFinalizaMapeamento.RecordCount = 0) then
                    Begin

                     Dm.IBInventario.Edit;
                     Dm.IBInventarioINV_STATUS.Value := 'FINALIZADO';
                     FrmInventario.MainMenu1.Items[1].Items[3].Enabled := True;
                     FrmInventario.MainMenu1.Items[1].Items[4].Enabled := True;
                     FrmInventario.MainMenu1.Items[1].Items[5].Enabled := True;
                     FrmInventario.MainMenu1.Items[1].Items[6].Enabled := True;

                     If NivelUsu = 1 then
                     begin
                      FrmInventario.MainMenu1.Items[1].Items[7].Enabled := True;
                      FrmInventario.MainMenu1.Items[1].Items[8].Enabled := True;
                     end;


                     If (Dm.IBInventarioINV_LOJA.Value ='CLB') then
                     FrmInventario.MainMenu1.Items[1].Items[12].Enabled := True;

                     FrmInventario.Panel4.Enabled := False;
                     FrmInventario.Panel15.Enabled := False;
                     FrmInventario.Panel9.Enabled := False;
                     FrmInventario.Panel7.Enabled := False;
                     FrmInventario.Panel8.Enabled := False;
                     FrmInventario.Panel11.Enabled := False;
                     FrmInventario.Panel12.Enabled := False;
                     FrmInventario.Panel13.Enabled := False;
                     FrmInventario.MainMenu1.Items[0].Items[0].Caption:= 'Reabrir Invent�rio';
                     Application.MessageBox('N�o ser� poss�vel fazer nenhum altera��o no invent�rio enquanto ele estiver Finalizado','Mensagem do Sistema', MB_ICONINFORMATION + MB_OK);
                     Dm.IBInventario.Post;
                     Dm.Transacao.CommitRetaining;

                     Dm.IBHorarios.Close;
                     Dm.IBHorarios.SelectSql.Clear;
                     Dm.IBHorarios.SelectSql.Add('Select * from Horario');
                     Dm.IBHorarios.Open;
                    End
                   else
                    begin
                       Application.MessageBox('Ainda existem as Pend�ncias abaixo','Mensagem do Sistema', MB_ICONEXCLAMATION + MB_OK);
                       Dm.IBHorarios.Close;
                       Dm.IBHorarios.SelectSql.Clear;
                       Dm.IBHorarios.SelectSql.Add('Select * from Horario');
                       Dm.IBHorarios.Open;
                    end;
                  End
                 else
                   begin
                     Application.MessageBox('N�o foi contado nenhum item. N�o � poss�vel finalizar','Mensagem do Sistema', MB_ICONEXCLAMATION + MB_OK);
                     Dm.IBHorarios.Close;
                     Dm.IBHorarios.SelectSql.Clear;
                     Dm.IBHorarios.SelectSql.Add('Select * from Horario');
                     Dm.IBHorarios.Open;
                   end;
              end;
             FrmAuditQtdSecEan.Close;

         end
        else
            begin
             Application.MessageBox('N�o existem produtos nessa faixa de quantidade','Mensagem do sistema', MB_ICONEXCLAMATION+ MB_OK);
            end;
      end;

  except
     Application.MessageBox('Erro ao gerar relat�rio','Mensagem do sistema', MB_ICONERROR+ MB_OK);
  end;

Finally
   Dm.QRelAudQtdSecEan.Close;
End;
end;

procedure TFrmAuditQtdSecEan.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',#8]) then
         begin
          key:=#0;
          beep;
         end;
end;

procedure TFrmAuditQtdSecEan.Edit2KeyPress(Sender: TObject; var Key: Char);
begin
if not (key in ['0'..'9',#8]) then
   begin
     key:=#0;
     beep;
   end;
end;

procedure TFrmAuditQtdSecEan.Edit1Exit(Sender: TObject);
begin
 if not (Edit2.Text = '') then
  begin
   if StrToInt(Edit2.Text) < StrToInt(Edit1.Text) then
      begin
       ShowMessage('A Quantidade inicial deve ser menor que quantidade final. Digite um intervalo v�lido');
       Edit1.Clear;
       Edit2.Clear;
       Edit1.SetFocus;
      end
  end;
end;

procedure TFrmAuditQtdSecEan.Edit2Exit(Sender: TObject);
begin
 if (StrToInt(Edit2.Text) < StrToInt(Edit1.Text)) then
    begin
     ShowMessage('A Quantidade inicial deve ser menor que quantidade final. Digite um intervalo v�lido');
     Edit1.Clear;
     Edit2.Clear;
     Edit1.SetFocus;
    end
end;

end.
