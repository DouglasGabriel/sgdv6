unit ULayout;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, sEdit, sLabel, Grids, DBGrids, Buttons, Spin,
  jpeg, Data.DB;

type
  TFrmLayout = class(TForm)
    Wizard: TNotebook;
    Label1: TLabel;
    EdtLoja1: TsEdit;
    Label4: TLabel;
    Label2: TLabel;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sLabel3: TsLabel;
    sLabel4: TsLabel;
    EdtEAN1: TsEdit;
    EdtEAN2: TsEdit;
    EdtDesc1: TsEdit;
    sLabel6: TsLabel;
    EdtDesc2: TsEdit;
    EdtQTDE1: TsEdit;
    EdtQTDE2: TsEdit;
    sLabel7: TsLabel;
    sLabel8: TsLabel;
    EdtPRECO1: TsEdit;
    EdtPRECO2: TsEdit;
    DBGrid1: TDBGrid;
    Label5: TLabel;
    EdtCliente: TsEdit;
    GroupBox1: TGroupBox;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    Label3: TLabel;
    EdtCODINT1: TEdit;
    EdtCODINT2: TEdit;
    Label7: TLabel;
    Image1: TImage;
    Image2: TImage;
    Image3: TImage;
    Label9: TLabel;
    Label10: TLabel;
    Image4: TImage;
    Label12: TLabel;
    Image5: TImage;
    Label14: TLabel;
    Image6: TImage;
    Image7: TImage;
    edtLoja2: TsEdit;
    sLabel9: TsLabel;
    sLabel10: TsLabel;
    Label6: TLabel;
    CmbZerados: TComboBox;
    Label8: TLabel;
    chkZeroPLU: TCheckBox;
    ChkZeroEan: TCheckBox;
    Label11: TLabel;
    CmbSetor: TComboBox;
    Label13: TLabel;
    EdtSetor1: TEdit;
    Label15: TLabel;
    EdtSetor2: TEdit;
    Label16: TLabel;
    edtCasasDecQtde: TsEdit;
    Label17: TLabel;
    cbUN: TComboBox;
    lbIniUN: TLabel;
    edtIniUN: TEdit;
    lbPosUN: TLabel;
    edtPosUN: TEdit;
    Label18: TLabel;
    cbEnd: TComboBox;
    Label19: TLabel;
    CmbLote: TComboBox;
    Label20: TLabel;
    Label21: TLabel;
    lblIniEnd: TLabel;
    EdtIniEnd: TEdit;
    lblPosEnd: TLabel;
    EdtPosEnd: TEdit;
    lblIniLote: TLabel;
    EdtIniLote: TEdit;
    lblPosLote: TLabel;
    EdtPosLote: TEdit;
    Label22: TLabel;
    CmbAuditForcada: TComboBox;
    Label23: TLabel;
    EdtValorAudit: TEdit;
    Label24: TLabel;
    EdtQuantAudit: TEdit;
    sLabelFX1: TsLabelFX;
    procedure Button2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardEnter(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure CmbZeradosChange(Sender: TObject);
    procedure CmbSetorClick(Sender: TObject);
    procedure edtCasasDecQtdeKeyPress(Sender: TObject; var Key: Char);
    procedure EdtEAN2Exit(Sender: TObject);
    procedure cbUNClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmbLoteClick(Sender: TObject);
    procedure cbEndClick(Sender: TObject);
    procedure CmbAuditForcadaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmLayout: TFrmLayout;
  var  Ini, Fim: integer;

implementation

uses UDm;

{$R *.dfm}

procedure TFrmLayout.Button2Click(Sender: TObject);

begin
Wizard.PageIndex := Wizard.PageIndex - 1;
end;

procedure TFrmLayout.FormShow(Sender: TObject);
begin
Ini:= 1;
Fim:= 8;
Wizard.ActivePage:='SIGLA';
//Panel1.caption:='Configura��o de Layout';

end;

procedure TFrmLayout.WizardEnter(Sender: TObject);
begin
EdtCliente.setfocus;
end;

procedure TFrmLayout.SpeedButton1Click(Sender: TObject);

begin

//CADASTRA A SIGLA
IF Wizard.ActivePage = 'SIGLA' then
  begin //1
    If EdtCliente.Text = '' then
      begin //2
      Application.MessageBox('Favor digitar a SIGLA','Mensagem do Sistema', MB_ICONEXCLAMATION + MB_OK);
      end //2
        else
          begin //3
            Dm.IBLayoutInput.close;
            Dm.IBLayoutInput.SelectSQL.Clear;
            Dm.IBLayoutInput.SelectSQL.Add('Select * from LAYOUT_INPUT where CLIENTE = :CLIENTE');
            Dm.IBLayoutInput.ParamByname('CLIENTE').Value := EdtCliente.Text;
            Dm.IBLayoutInput.Open;


              If Dm.IBLayoutInput.IsEmpty then
                begin //4
                  Dm.IBLayoutInput.Insert;
                  Dm.IBLayoutInputLOJA.Value := EdtLoja1.Text + ',' + EdtLoja2.Text;
                  Dm.IBLayoutInputCLIENTE.Value := EdtCliente.text;
                  //Dm.IBLayoutInput.Post;
                  //Dm.Transacao.CommitRetaining;
                  Wizard.PageIndex := Wizard.PageIndex + 1;

                end //4
              else
              Application.MessageBox('Esta SIGLA j� est� cadastrada','Mensagem do Sistema', MB_ICONERROR + MB_OK);

            end;
    end; //1

//CADASTRA O C�DIGO INTERNO
IF Wizard.ActivePage = 'COD_INT' then
  begin //1
    If (EdtCODINT1.Text = '') or (EdtCODINT2.Text = '') then
      begin //2
       Application.MessageBox('Informar o C�DIGO INTERNO','Mensagem do Sistema', MB_ICONEXCLAMATION + MB_OK);
      end //2
        else
          begin //3
                  //Dm.IBLayoutInput.Open;
                  //Dm.IBLayoutInput.Insert;
                  Dm.IBLayoutInputCOD_INT.Value := EdtCODINT1.Text + ',' + EdtCODINT2.Text;;
                  If ChkZeroPlu.Checked = true then Dm.IBLayoutInputCOD_INT_ZERO.Value := 'T' else Dm.IBLayoutInputCOD_INT_ZERO.Value := 'F';
                  //Dm.IBLayoutInput.Post;
                  //Dm.Transacao.CommitRetaining;
                  Wizard.PageIndex := Wizard.PageIndex + 1;

          end //4

    end; //1

// CADASTRA O C�DIGO EAN
IF Wizard.ActivePage = 'EAN' then
  begin //1
    If (EdtEAN1.Text = '') or (EdtEAN2.Text = '') then
      begin //2
       Application.MessageBox('Informar o C�DIGO EAN','Mensagem do Sistema', MB_ICONEXCLAMATION + MB_OK);
      end //2
        else
          begin //3
            //Dm.IBLayoutInput.Open;
            //Dm.IBLayoutInput.Insert;
            Dm.IBLayoutInputEAN.Value := EdtEAN1.Text + ',' + EdtEAN2.Text;
            If ChkZeroEAN.Checked = true then Dm.IBLayoutInputEAN_ZERO.Value := 'T' else Dm.IBLayoutInputEAN_ZERO.Value := 'F';
           // Dm.IBLayoutInput.Post;
            //Dm.Transacao.CommitRetaining;
            Wizard.PageIndex := Wizard.PageIndex + 1;
          end //4

    end; //1

// CADASTRA A DESCRICAO
IF Wizard.ActivePage = 'DESCRICAO' then
  begin //1
    If (EdtDESC1.Text = '') or (EdtDESC2.Text = '') then
      begin //2
        Application.MessageBox('Informar a DESCRI��O','Mensagem do Sistema', MB_ICONEXCLAMATION + MB_OK);
      end //2
        else
          begin //3
                  //Dm.IBLayoutInput.Open;
                  //Dm.IBLayoutInput.Insert;
                  Dm.IBLayoutInputDESCRICAO.Value := EdtDESC1.Text + ',' + EdtDESC2.Text;;
                 // Dm.IBLayoutInput.Post;
                  //Dm.Transacao.CommitRetaining;
                  Wizard.PageIndex := Wizard.PageIndex + 1;

          end //4

    end; //1

// CADASTRA A QTDE
IF Wizard.ActivePage = 'QTDE' then
  begin //1
    If (EdtQTDE1.Text = '') or (EdtQTDE2.Text = '') then
         Application.MessageBox('Informar a QUANTIDADE','Mensagem do Sistema', MB_ICONEXCLAMATION + MB_OK)
  else
    If (EdtQTDE1.Text = '*') and (EdtQTDE2.Text = '*') then //2
            begin //3
                  //Dm.IBLayoutInput.Open;
                 // Dm.IBLayoutInput.Insert;
                 if (edtCasasDecQtde.Text = '') or (edtCasasDecQtde.Text = '*') then
                    DM.IBLayoutInputCASADEC_QTDE.AsInteger := 0
                 else
                    DM.IBLayoutInputCASADEC_QTDE.AsInteger := StrToInt(edtCasasDecQtde.text);

                  Dm.IBLayoutInputQTDE.Value := '*,*';
                  //Dm.IBLayoutInput.Post;
                  //Dm.Transacao.CommitRetaining;
                  Wizard.PageIndex := Wizard.PageIndex + 1;
            end
    else
      begin //3
                  //Dm.IBLayoutInput.Open;
                 // Dm.IBLayoutInput.Insert;
                 if (edtCasasDecQtde.Text = '') or (edtCasasDecQtde.Text = '*') then
                    DM.IBLayoutInputCASADEC_QTDE.AsInteger := 0
                 else
                    DM.IBLayoutInputCASADEC_QTDE.AsInteger := StrToInt(edtCasasDecQtde.text);

                  Dm.IBLayoutInputQTDE.Value := EdtQTDE1.Text + ',' + EdtQTDE2.Text;;
                  //Dm.IBLayoutInput.Post;
                  //Dm.Transacao.CommitRetaining;
                  Wizard.PageIndex := Wizard.PageIndex + 1;
      end;


          end; //4


// CADASTRA O PRE�O
IF Wizard.ActivePage = 'PRECO' then
  begin //1
    If (EdtPRECO1.Text = '') or (EdtPRECO2.Text = '') then
       //2
        Application.MessageBox('Informar o PRE�O','Mensagem do Sistema', MB_ICONEXCLAMATION + MB_OK)
      else
    If (EdtPRECO1.Text = '*') and (EdtPRECO2.Text = '*') then //2
            begin //3
                  //Dm.IBLayoutInput.Open;
                 // Dm.IBLayoutInput.Insert;
                  Dm.IBLayoutInputPRECO.Value := '*,*';
                  DM.IBLayoutInputPRECODEC.Value:='*,*';
                  //Dm.IBLayoutInput.Post;
                  //Dm.Transacao.CommitRetaining;
                  Wizard.PageIndex := Wizard.PageIndex + 1;
            end
    else
        begin //3
                  //Dm.IBLayoutInput.Open;
                  //Dm.IBLayoutInput.Insert;
                  Dm.IBLayoutInputPRECO.Value := EdtPRECO1.Text + ',' + EdtPRECO2.Text;
                  Dm.IBLayoutInputPRECODEC.Value := IntToStr((StrToInt(EdtPRECO1.Text)+StrToInt(EdtPRECO2.Text))) + ',' + '2';
                //  Dm.Transacao.CommitRetaining;
                  Wizard.PageIndex := Wizard.PageIndex + 1;

          end //4

    end; //1

// CADASTO OUTROS
IF Wizard.ActivePage = 'OUTROS' then
  begin //1
   If (CmbZerados.ItemIndex = -1)then
       //2
        Application.MessageBox('Informa��es Adicionais','Mensagem do Sistema', MB_ICONEXCLAMATION + MB_OK)
      else
    If (CmbZerados.ItemIndex = 0) then
            begin //3
                  //Dm.IBLayoutInput.Open;
                 // Dm.IBLayoutInput.Insert;
                  Dm.IBLayoutInputZERADOS.Value := 'T';
                  //Dm.IBLayoutInput.Post;
                  //Dm.Transacao.CommitRetaining;
                  Wizard.PageIndex := Wizard.PageIndex + 1;
            end
    else
        begin //3
          Dm.IBLayoutInputZERADOS.Value := 'F';
          Wizard.PageIndex := Wizard.PageIndex + 1;

          end;//4



    If CmbSetor.Text = 'T' then
      begin
         If (EdtSetor1.Text = '*') and (EdtSetor2.Text = '*') then //2
            begin
              Dm.IBLayoutInputSETOR.Value := '*,*';
              Wizard.PageIndex := Wizard.PageIndex + 1;
            end
         else
            begin
              Dm.IBLayoutInputSETOR.Value := EdtSetor1.text+','+EdtSetor2.text;
              Wizard.PageIndex := Wizard.PageIndex + 1;
            end;

    end; //1


    if cbUN.ItemIndex = 0 then
      begin
       Dm.IBLayoutInputUN.AsString := 'T';
       Dm.IBLayoutInputUNTAM.AsString := edtIniUN.Text+','+edtPosUN.Text;
       Wizard.PageIndex := Wizard.PageIndex + 1;
      end
    else
       Dm.IBLayoutInputUN.AsString := 'F';
       

    if cbEnd.ItemIndex = 0 then
      begin
       Dm.IBLayoutInputENDERECO.AsString := 'T';
       Dm.IBLayoutInputEND_ZERO.Value := Label21.Caption;
       DM.IBLayoutInputEND_POSICAO.AsString := EdtIniEnd.Text+','+EdtPosEnd.Text;
       Wizard.PageIndex := Wizard.PageIndex + 1;
      end;

    if CmbLote.ItemIndex = 0 then
      begin
       DM.IBLayoutInputLOTE.AsString := 'T';
       DM.IBLayoutInputLOTE_ZERO.Value := Label20.Caption;
       DM.IBLayoutInputLOTE_POSICAO.Value := EdtIniLote.Text+','+EdtPosLote.Text;
       Wizard.PageIndex := Wizard.PageIndex + 1;
      end;

    If CmbAuditForcada.ItemIndex = 0 then
     begin
      DM.IBLayoutInputREL_AUDIT_VALOR.AsString := 'T';
      DM.IBLayoutInputREL_AUDIT_VALOR2.Value   := EdtValorAudit.Text;
      Wizard.PageIndex := Wizard.PageIndex + 1;
     end;

    If CmbAuditForcada.ItemIndex = 1 then
     begin
      DM.IBLayoutInputREL_AUDIT_QUANT.AsString := 'T';
      DM.IBLayoutInputREL_AUDIT_QUANT2.Value   := EdtQuantAudit.Text;
      Wizard.PageIndex := Wizard.PageIndex + 1;
     end;

    If CmbAuditForcada.ItemIndex = 2 then
     begin
      DM.IBLayoutInputREL_AUDIT_QUANT.AsString := 'T';
      DM.IBLayoutInputREL_AUDIT_VALOR.AsString := 'T';
      DM.IBLayoutInputREL_AUDIT_QUANT2.Value   := EdtQuantAudit.Text;
      DM.IBLayoutInputREL_AUDIT_VALOR2.Value   := EdtValorAudit.Text;
      Wizard.PageIndex := Wizard.PageIndex + 1;
     end;

  end;


 //1
If Wizard.ActivePage = 'FIM' then
begin
    Dm.IBLayoutInput.Post;
    Dm.Transacao.CommitRetaining;
    SpeedButton1.Enabled:=false;
    Application.MessageBox('Informarma��es salvas','Mensagem do Sistema', MB_ICONINFORMATION + MB_OK);
end;
end;

procedure TFrmLayout.SpeedButton3Click(Sender: TObject);
begin
Close;
end;

procedure TFrmLayout.SpeedButton2Click(Sender: TObject);

begin
Wizard.PageIndex := Wizard.PageIndex - 1;
Ini:= ini-1;
end;

procedure TFrmLayout.CmbZeradosChange(Sender: TObject);
begin
If CmbZerados.ItemIndex = 0 then
  Label8.Caption := 'SIM' else Label8.Caption := 'N�O';
end;

procedure TFrmLayout.CmbSetorClick(Sender: TObject);
begin
If CmbSetor.Text = 'T' then
  begin
    label13.Visible:=true;
    label15.Visible:=true;
    EdtSetor1.Visible:=true;
    EdtSetor2.Visible:=true;
  end
else
  begin
    label13.Visible:=false;
    label15.Visible:=false;
    EdtSetor1.Visible:=false;
    EdtSetor2.Visible:=false;
  end;
end;

procedure TFrmLayout.edtCasasDecQtdeKeyPress(Sender: TObject;
  var Key: Char);
begin
   if not (Key in['0'..'9',Chr(8)]) then Key:= #0;
end;

procedure TFrmLayout.EdtEAN2Exit(Sender: TObject);
begin
   if StrToInt(EdtEAN2.Text) > 25 then
     begin
      Application.MessageBox('Posi��o M�xima do Sistema excedida !!!','Mensagem do Sistema', MB_ICONEXCLAMATION + MB_OK);
      EdtEAN2.SetFocus;
     end;   
end;



procedure TFrmLayout.cbUNClick(Sender: TObject);
begin
If cbUN.Text = 'T' then
  begin
    lbIniUN.Visible  := true;
    lbPosUN.Visible  := true;
    edtIniUN.Visible := true;
    edtPosUN.Visible := true;
  end
else
  begin
    lbIniUN.Visible  := false;
    lbPosUN.Visible  := false;
    edtIniUN.Visible := false;
    edtPosUN.Visible := false;
  end;
end;

procedure TFrmLayout.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//  Dm.IBLayoutInput.Close;
//  FreeAndNil(FrmLayout);
//  FrmLayout.Close;
end;

procedure TFrmLayout.CmbLoteClick(Sender: TObject);
var Zero : string;
begin
 if CmbLote.Text = 'T' then
  begin
    Zero := InputBox('Lote', 'Completar LOTE com zeros � esquerda?','S');
     If (Zero = 'S') or (Zero = 's') then
      begin
       Label20.caption := 'T';
      end
     else
      begin
       label20.caption:='F';
      end;
  end;

  If CmbLote.Text = 'T' then
  begin
    lblIniLote.Visible  := true;
    lblPosLote.Visible  := true;
    EdtIniLote.Visible := true;
    EdtPosLote.Visible := true;
  end
else
  begin
    lblIniLote.Visible  := false;
    lblPosLote.Visible  := false;
    EdtIniLote.Visible := false;
    EdtPosLote.Visible := false;
  end;



end;

procedure TFrmLayout.cbEndClick(Sender: TObject);
var Zero : string;
begin
if cbEnd.Text = 'T' then
  begin
    Zero := InputBox('Endere�o', 'Completar ENDERE�O com zeros � esquerda?','S');
     If (Zero = 'S') or (Zero = 's') then
      begin
       Label21.caption := 'T';
      end
     else
      begin
       label21.caption:='F';
      end;
  end;

  If cbEnd.Text = 'T' then
  begin
    lblIniEnd.Visible  := true;
    lblPosEnd.Visible  := true;
    EdtIniEnd.Visible := true;
    EdtPosEnd.Visible := true;
  end
else
  begin
    lblIniEnd.Visible  := false;
    lblPosEnd.Visible  := false;
    EdtIniEnd.Visible := false;
    EdtPosEnd.Visible := false;
  end;



end;

procedure TFrmLayout.CmbAuditForcadaClick(Sender: TObject);
begin
    If CmbAuditForcada.ItemIndex = 0 then
     begin
        Label23.Visible       := True;
        EdtValorAudit.Visible := True;
        Label24.Visible       := False;
        EdtQuantAudit.Visible := False;
     end
    else
     if CmbAuditForcada.ItemIndex = 1 then
      begin
        Label23.Visible       := False;
        EdtValorAudit.Visible := False;
        Label24.Visible       := True;
        EdtQuantAudit.Visible := True;
      end
     else
      if CmbAuditForcada.ItemIndex = 2 then
       begin
         Label23.Visible       := True;
         EdtValorAudit.Visible := True;
         Label24.Visible       := True;
         EdtQuantAudit.Visible := True;
       end;
end;

end.
