unit UInventario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, Menus, ExtCtrls, Grids, DBGrids, StdCtrls, Buttons,
  Mask, DBCtrls, ShellApi, TeeProcs, TeEngine, Chart, DbChart, Series,
  ImgList, AppEvnts, MidasLib, sSkinManager, sEdit, RpDefine, RpRender,
  RpRenderPDF, RpRenderText, sBitBtn, DBClient;

type
  TFrmInventario = class(TForm)
    MainMenu1: TMainMenu;
    Opes1: TMenuItem;
    Fechar1: TMenuItem;
    Timer1: TTimer;
    DesabilitarimportaoAutomatica1: TMenuItem;
    N1: TMenuItem;
    Relatrios1: TMenuItem;
    FinalizarInventrio1: TMenuItem;
    RelatriosdeAuditoria1: TMenuItem;
    Panel10: TPanel;
    GeraArquivoFinal1: TMenuItem;
    RelatrioTotaldePeaseFinanceiro1: TMenuItem;
    xtcomtotaldeHorasdosConferente1: TMenuItem;
    Ajuda1: TMenuItem;
    AtalhosdoSistema1: TMenuItem;
    RelatriodePsicount1: TMenuItem;
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Label11: TLabel;
    DBText1: TDBText;
    Label13: TLabel;
    DBText4: TDBText;
    Label12: TLabel;
    DBText3: TDBText;
    DBText2: TDBText;
    Panel2: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel3: TPanel;
    Panel4: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    Panel15: TPanel;
    DBChart1: TDBChart;
    Series1: TPieSeries;
    TabSheet2: TTabSheet;
    Panel6: TPanel;
    Panel9: TPanel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    Label14: TLabel;
    DBGrid6: TDBGrid;
    DBGrid7: TDBGrid;
    BitBtn3: TBitBtn;
    Edit4: TEdit;
    TabSheet3: TTabSheet;
    Panel7: TPanel;
    Panel8: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel11: TPanel;
    DBGrid1: TDBGrid;
    TabSheet4: TTabSheet;
    Panel12: TPanel;
    DBGrid2: TDBGrid;
    TabSheet5: TTabSheet;
    Panel13: TPanel;
    DBGrid3: TDBGrid;
    Panel16: TPanel;
    TabSheet7: TTabSheet;
    Panel17: TPanel;
    RelatriodeDuplicidades1: TMenuItem;
    RelatriodeSeesAbertas1: TMenuItem;
    DBGrid4: TDBGrid;
    RelatriodeSeesInesperadas1: TMenuItem;
    ImageList1: TImageList;
    RelatriodeItensAlteradosdepoisdedescarregados1: TMenuItem;
    ApplicationEvents1: TApplicationEvents;
    BitBtn4: TBitBtn;
    GerarDivergncia1: TMenuItem;
    RelatriodeProdutosNoCadastrados1: TMenuItem;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    EdtHoras: TsEdit;
    EdtProdutividade: TsEdit;
    EdtQtde: TsEdit;
    Label18: TLabel;
    EdtPessoas: TsEdit;
    Label19: TLabel;
    DBEdit8: TDBEdit;
    RvPDF: TRvRenderPDF;
    RvTxt: TRvRenderText;
    RelatriodeCdigosEANePLU1: TMenuItem;
    ransmitirBancodeDados1: TMenuItem;
    Timer2: TTimer;
    sBitBtn1: TsBitBtn;
    sBitBtn2: TsBitBtn;
    RelatriodeSetorConsolidado1: TMenuItem;
    DBGrid5: TDBGrid;
    CheckBox1: TCheckBox;
    Geratxtconfernciaseo1: TMenuItem;
    Label20: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure Edit1Exit(Sender: TObject);
    procedure Edit2Exit(Sender: TObject);
    procedure TabSheet3Show(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure TabSheet5Show(Sender: TObject);
    procedure Fechar1Click(Sender: TObject);
    procedure DBGrid5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn3Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure FinalizarInventrio1Click(Sender: TObject);
    procedure GeraArquivoFinal1Click(Sender: TObject);
    procedure DBGrid3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RelatrioTotaldePeaseFinanceiro1Click(Sender: TObject);
    procedure xtcomtotaldeHorasdosConferente1Click(Sender: TObject);
    procedure TabSheet4Show(Sender: TObject);
    procedure RelatriosdeAuditoria1Click(Sender: TObject);
    procedure AtalhosdoSistema1Click(Sender: TObject);
    procedure TabSheet1Show(Sender: TObject);
    procedure DesabilitarimportaoAutomatica1Click(Sender: TObject);
    procedure RelatriodePsicount1Click(Sender: TObject);
    procedure TabSheet2Show(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure RelatriodeDuplicidades1Click(Sender: TObject);
    procedure RelatriodeSeesAbertas1Click(Sender: TObject);
    procedure DBGrid5KeyPress(Sender: TObject; var Key: Char);
    procedure DBGrid3KeyPress(Sender: TObject; var Key: Char);
    procedure Edit4Change(Sender: TObject);
    procedure TabSheet7Show(Sender: TObject);
    procedure RelatriodeSeesInesperadas1Click(Sender: TObject);
    procedure DBGrid3CellClick(Column: TColumn);
    procedure DBGrid5CellClick(Column: TColumn);
    procedure RelatriodeItensAlteradosdepoisdedescarregados1Click(
      Sender: TObject);
    procedure DBGrid5ColExit(Sender: TObject);
    procedure TabSheet7Exit(Sender: TObject);
    procedure DBGrid5Exit(Sender: TObject);
    procedure ApplicationEvents1Message(var Msg: tagMSG;
      var Handled: Boolean);
    procedure BitBtn4Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure GerarDivergncia1Click(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure RelatriodeProdutosNoCadastrados1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure RelatriodeCdigosEANePLU1Click(Sender: TObject);
    procedure DBGrid6DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid7DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid3DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid4DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid5DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure ransmitirBancodeDados1Click(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure sBitBtn2Click(Sender: TObject);
    procedure RelatriodeSetorConsolidado1Click(Sender: TObject);
    procedure RelatriodeAuditoriaConsolidado1Click(Sender: TObject);
    procedure DBGrid5TitleClick(Column: TColumn);
    procedure DBGrid2TitleClick(Column: TColumn);
    procedure DBGrid3TitleClick(Column: TColumn);
    procedure CheckBox1Click(Sender: TObject);
    procedure Geratxtconfernciaseo1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }

    click : boolean;

  end;

var
  FrmInventario: TFrmInventario;
  IDSECAO : integer;
  IDCONTAGEM : integer;
  Cod : Integer;
  LastSearch: string;
  LastItem: Integer;
  //SecaoGlobal: String[4];
  //TransIdGlobal: Integer;
  //SecIdGlobal: Integer;
implementation

uses UDm, UFuncProc, ULogin, uHorario, uFinalizaInvent, DB, UPesquisa,
  USelecionaSecao, IBCustomDataSet, Math, UAguarde, UDivergencia, UCadInv,
  UFrmProdutividade, UFrmUpdate, Udm2;

{$R *.dfm}

procedure TFrmInventario.BitBtn1Click(Sender: TObject);
Begin
Edit2.Enabled := True;
If ((Edit1.Text <> '') and (Edit2.Text <> '')) then //Verifica se os campos n�o est�o em Branco
  Begin
    DM.qrySQL.Close;
    DM.qrySQL.SQL.Clear;
    DM.qrySQL.SQL.Add('SELECT * FROM LAYOUT_INPUT WHERE CLIENTE = :CLIENTE');
    DM.qrySQL.ParamByName('CLIENTE').AsString := Dm.IBInventarioINV_LOJA.Value;
    DM.qrySQL.Open;

     if DM.qrySQL.FieldByName('ENDERECO').AsString = 'T' then
       begin
          Try
            Timer1.Enabled := False;
            AbreTeladeAguarde('Aguarde!!! Cadastrando Se��es');
            CadastraMapeamentoEnd(Edit1.Text, Edit2.Text, Edit3.Text);
          Finally
            Timer1.Enabled := True;
            FechaTeladeAguarde;
          End;


       end
     else
       begin
        If (Edit1.Text <= Edit2.Text)
        and ((strtoint(edit1.Text) >= 0000) and (strtoint(edit2.Text) >= 0000)) then //Verifica se Se��o Final � Maior que Se��o Inicial
        Begin
          Try
            Timer1.Enabled := False;
            AbreTeladeAguarde('Aguarde!!! Cadastrando Se��es');
            CadastraMapeamento(AjustaStrEsq(Edit1.Text,4,'0'),AjustaStrEsq(Edit2.Text,4,'0'), Edit3.Text);
          Finally
            Timer1.Enabled := True;
            FechaTeladeAguarde;
          End;
        end
        Else //Caso Se��o Final � Maior que Se��o Inicial
         Application.MessageBox('A se��o final deve ser menor que a se��o incial ','Mensagem do Sistema', MB_ICONERROR+ MB_OK);
       end;

  End
  Else
    Application.MessageBox('� obrigat�rio o preenchimento do valor na Se��o Incial e Final','Mensagem do Sistema', MB_ICONERROR+ MB_OK);

  //Zerar campo
  Edit1.Text := '';
  Edit2.Text := '';
  Edit3.Text := '';
  Edit1.SetFocus;
  Dm.IBMapeamento.Close;
  Dm.IBMapeamento.ParamByName('SecIni').Value := '0';
  Dm.IBMapeamento.ParamByName('SecFin').Value := 'ZZZZZZZZZZZZZZZZZZZZZZZZZ';
  Dm.IBMapeamento.Open;
  Dm.IBMapeamento.First;
  Dm.IBSecao.Close;
  Dm.IBSecao.Open;
End;

procedure TFrmInventario.BitBtn2Click(Sender: TObject);
begin
if Dm.IBMapeamento.RecordCount > 0 then
 Begin
  Try
    AbreTeladeAguarde('Aguarde!!! Excluindo Se��es Abertas');
    DeletaSecao(Dm.IBMapeamentoMAP_SECINI.Value, Dm.IBMapeamentoMAP_SECFIN.Value);
    Dm.IBMapeamento.Delete;
    DM.Transacao.CommitRetaining;
    PosicionaSecao(IDSECAO);
  Finally
    FechaTeladeAguarde;
  End;
 End;
end;

procedure TFrmInventario.Edit1Exit(Sender: TObject);
begin
  DM.qrySQL.Close;
  DM.qrySQL.SQL.Clear;
  DM.qrySQL.SQL.Add('SELECT * FROM LAYOUT_INPUT WHERE CLIENTE = :CLIENTE');
  DM.qrySQL.ParamByName('CLIENTE').AsString := Dm.IBInventarioINV_LOJA.Value;
  DM.qrySQL.Open;

  if DM.qrySQL.FieldByName('ENDERECO').AsString <> 'T' then
    begin
      If IntegerIsStr(Edit1.text) then
       Begin
        Edit1.Text := '';
        Edit1.SetFocus;
        Application.MessageBox('� obrigat�rio o preenchimento do valor na Se��o','Mensagem do Sistema', MB_ICONERROR+ MB_OK);
       End;
    end
  else
    begin
     Edit2.Text := Edit1.Text;
     Edit2.Enabled := False;
    end;
   
End;

procedure TFrmInventario.Edit2Exit(Sender: TObject);
begin
  DM.qrySQL.Close;
  DM.qrySQL.SQL.Clear;
  DM.qrySQL.SQL.Add('SELECT * FROM LAYOUT_INPUT WHERE CLIENTE = :CLIENTE');
  DM.qrySQL.ParamByName('CLIENTE').AsString := Dm.IBInventarioINV_LOJA.Value;
  DM.qrySQL.Open;

  if DM.qrySQL.FieldByName('ENDERECO').AsString <> 'T' then
    begin
      If IntegerIsStr(Edit2.text) then
       Begin
        Edit2.Text := '';
        Edit2.SetFocus;
        Application.MessageBox('S� � permitido abrir se��es com o valores num�ricos','Mensagem do Sistema', MB_ICONERROR+ MB_OK);
       End;
    end;
End;

procedure TFrmInventario.TabSheet3Show(Sender: TObject);
begin
DbGrid1.Refresh;
if Panel7.Enabled = True then
 Edit1.SetFocus;
end;

procedure TFrmInventario.Timer1Timer(Sender: TObject);
Var
Origem, Destino1, Destino2: String;
TransID, OS : String;
Cont: Integer;
DataHora, NomedaTransmissao1, NomedaTransmissao2, NomeArqBackup: String;
S, T: TFileStream;
Begin


  Timer1.Enabled := False;  //Desativa o Timer para n�o rodar ao mesmo tempo que j� estiver rodando
  Cont := 1650;

  //Corre da pasta 1650 a 2999 dentro do diret�rio 'C:\SGD\IGT_CE\IgtCeSinc\' para ver qual pasta existe
  While Cont <= 2999 Do
  Begin
    Origem := 'C:\SGD\IGT_CE\IgtCeSinc\' + AjustaStrEsq(inttostr(Cont),4,'0') + '\invent.txt';  //Origem do arquivo "invent.txt"

    //Verica se o arquivo existe dentro da pasta
    If FileExists(Origem) then //Caso exista o arquivo
    Begin
      AbreTeladeAguarde('Aguarde!!! Importanto Transmiss�o do Coletor ' + inttostr(Cont) + '.');
      Try // ##01## Para verificar algum problema na transmiss�o do arquivo para pasta C:\SGD\Transmissao\

        //Copia arquivo da pasta 'C:\SGD\IGT_CE\IgtCeSinc\XXXX' para 'C:\SGD\Transmissao\'
        S := TFileStream.Create(Origem, fmOpenRead);

        //Gera ID da Transmiss�o
        Try
          Dm.QCriaTransID.Open;
          TransID := AjustaStrEsq(inttostr(Dm.QCriaTransIDGEN_ID.Value),5,'0');
          Dm.Transacao.CommitRetaining;
          Dm.QCriaTransID.Close;
        Except
          Application.MessageBox('Problema na gera��o do Id da transmiss�o','Mensagem do Sistema', MB_ICONERROR+ MB_OK);
        End;

        //N�mero da OS
        OS := Dm.IBInventarioINV_OS.Value;

        Destino1 := 'C:\SGD\Transmissao\Trans_' + TransID + '_OS' + OS + '.txt' ;   //Destino para onde ser� copiado
        Try //##02##
          T := TFileStream.Create( Destino1, fmOpenWrite or fmCreate );
          Try //##03##
            T.CopyFrom(S, S.Size )
          Finally //##03##
            T.Free
          End ////##03## Finaliza 2� Try
        Finally //##02##
          S.Free
        End;////##02## Finaliza 1� Try
      Except //##01##
        Application.MessageBox('N�o foi copiado o arquivo para pasta "C:\SGD\Transmissao\" contate o suporte','Mensagem do Sistema', MB_ICONERROR+ MB_OK);
      End; //##01##

      Try
        //Verifica se existe o diret�rios 'I:\N� OS'', sen�o � criado
        Try //##04## Try para verifica se ocorre erro na cria��o do diret�rio dentro do PEN-DRIVE
          If not FileExists('I:\BKP_SGD\OS_' + Dm.IBInventarioINV_OS.Value) then //Caso exista o arquivo
            CreateDir('I:\OS_' + Dm.IBInventarioINV_OS.Value);
        Except//##04##
          Application.MessageBox('N�o foi poss�vel criar o diret�rio dentro PEN-DRIVE','Mensagem do Sistema', MB_ICONERROR+ MB_OK);
        End;//##04##
      Finally
        //Copia arquivo da pasta 'C:\SGD\IGT_CE\IgtCeSinc\XXXX' para 'I:\N� OS''
        Try//##05## Try para verificar se ocorre problema na copia do arquivo para o PEN-DRIVE
          S := TFileStream.Create(Origem, fmOpenRead);
          Destino2 := 'I:\OS_' + Dm.IBInventarioINV_OS.Value + '\Trans_' + TransID + '_OS' + OS + '.txt';   //Destino para onde ser� copiado
          Try//##06##
              T := TFileStream.Create( Destino2, fmOpenWrite or fmCreate );
            Try//##07##
              T.CopyFrom(S, S.Size )
            Finally//##07##
              T.Free
            End //##07##
          Finally//##06##
            S.Free
          End;//##06##
        Except//##05##
          Application.MessageBox('Houve problema na copia da transmiss�o para o PEN-DRIVE. Essa transmiss�o ser� importada, ' + #13 +
                      'por�m voc� est� sem o backup completo das transmiss�es. Colocar o PEN-DRIVE e copiar da pasta ' + #13 +
                      'c:\SGD\Transmiss�es todas as transmiss�es, para caso ocorra algum porblema com o PC','Mensagem do Sistema', MB_ICONEXCLAMATION+ MB_OK);
        End;//##05##
      End;

      Try//##10##
      //Depois de copiado para pasta transmiss�o renomeio arquivo invent.txt da pasta do IGT e apaga o invent.txt
      //Pega Data e Hora da Transmiss�o do Arquivo
        DataHora := RetirarBarras(Retirar2Pontos(RetiraSpace(DataHoraArquivo(Origem))));
        NomeArqBackup := Copy(Origem,1,35) + '_' + DataHora + '.txt';
        RenameFile(Origem, NomeArqBackup);
        DeleteFile(Origem);
      Except //##10##
        Application.MessageBox('N�o foi poss�vel deletar o invent.txt da pasta do IGT','Mensagem do Sistema', MB_ICONERROR+ MB_OK);
      End; //##10##

     Try //##11##
      //Chama Fun��o que Importa os dados para para as tabelas
   //   IF DbEdit2.Text ='LOP' then
   //     ImportaDadosLOP(Destino1, Cont)
   //   else
     IF DbEdit2.Text ='CLB' then
        ImportaDadosCLB(Destino1, Cont)
      else
      IF DbEdit2.Text ='SCH' then
        ImportaDadosSCH(Destino1, Cont)
      else
      IF (DbEdit2.Text ='PDA') or (DbEdit2.Text ='ASA') then
        ImportaDadosPDA(Destino1, Cont)
        else
  //   IF DbEdit2.Text ='COV' then
  //      ImportaDadosCOV(Destino1, Cont)
  //      else
    IF (DbEdit2.Text ='CPV') then
        ImportaDadosCPV(Destino1, Cont)
        else
    IF (DbEdit2.Text ='LVC') then
        ImportaDadosLVC(Destino1, Cont)
        else

        ImportaDados(Destino1, Cont);
     Except//##11##
        Application.MessageBox('N�o foi poss�vel chamar a fun��o que importa os dados para o SGD','Mensagem do Sistema', MB_ICONERROR+ MB_OK);
     End;//##11##
      FechaTeladeAguarde;
    End;  //Fechar o If Caso encontre a pasta
    Cont := Cont + 1;
  End; //Fecha o While que corre as pastas para ver se encontra o arquivo

  Timer1.Enabled := True; //Ativa o Timer pois j� finalizou alguma importa��o
   //Abrir query que gera total de pe�as e valor financeiro


End;

procedure TFrmInventario.TabSheet5Show(Sender: TObject);
begin

// Dm.QGridSecao.Close;
// Dm.QGridSecao.Open;
 //DM.QGridSecao.;
 PosicionaSecao(IDSECAO);
 Dm.IBSecao.Refresh;

// DBGrid3.Refresh;
 if Panel12.Enabled = True then
   DBGrid3.SetFocus;
end;

procedure TFrmInventario.Fechar1Click(Sender: TObject);
begin
 Close;
end;

procedure TFrmInventario.DBGrid5KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
Secao, Status, TransID: String;
ID: Integer;
StrSql: String;
SecTransId, SecaoId: integer;
matricula, SecaoAntiga, SecaoNova: string;
NumCaracteres: integer;
Begin
//Pega valor do ID da linha
 IDCONTAGEM := DM.IBContagensCON_ID.Value;

  //Alterar Se��o com tecla de sobe e desce
  If ((Key = VK_DOWN) or (Key = VK_UP)) and (Dm.DsContagens.State = DsEdit) then
  Begin
    key := 13;
  End;


//###N�o deixa apagar registro do DbGrid, e s� coloca o STATUS como DEL###
If (Shift = [ssCtrl]) and (Key = 46) Then
Begin
 //Dm.IBContagens.DisableControls;
 Id := Dm.IBContagensCON_ID.Value;
 KEY := 0;
 Dm.SpDelContagem.Close;
 Dm.SpDelContagem.ParamByName('IDCON').Value := DM.IBContagensCON_ID.Value;
 Dm.SpDelContagem.ParamByName('TRANSID').Value := DM.IBContagensTRA_ID.Value;
 Dm.SpDelContagem.ParamByName('SECSECAO').Value := DM.IBContagensSEC_SECAO.Value;
 Dm.SpDelContagem.ParamByName('STATUS').Value := DM.IBContagensCON_STATUS.Value;
 Dm.SpDelContagem.ExecProc;
 //Fechar e abre as contagens
 Dm.IBContagens.Close;
 //Dm.CdsContagens.Close;
 DM.IBContagens.Open;
 //Dm.CdsContagens.Open;

 Dm.IBContagens.locate('CON_ID',Id,[Lopartialkey,locaseinsensitive]);

 //Atualiza Query com total de pe�as e valor financeiro
 Dm.QGeraRelFinal.Close;
 Dm.QGeraRelFinal.Open;
// Dm.IBContagens.EnableControls;
End;


    //Quando � pressionado o tecla para baixo e foi feita alguma altera��o na quantidade, � salvo a quantidade digitada

 { Else
      begin
        DBGrid5.Fields[6].Value := DBGrid5.Fields[6].OldValue;
        Dm.IBContagens.DisableControls;
        Dm.IBContagens.Next;
        If Dm.IBContagens.EOF then
          Key := 0
        Else
          Dm.IBContagens.Prior;
        Dm.IBContagens.EnableControls;
      End;
 }



  //###Imprime relat�rio de auditoria###
If key = vk_F1 then //Se precionado a tecla F1
 Begin
  Dm.IBContagens.DisableControls;
  ID := DM.IBContagensCON_ID.Value;
  Secao := Dm.IBContagensSEC_SECAO.Value;
 // TransID := IntToStr(Dm.IBContagensTRA_ID.value);
  Dm.IBSecao.locate('TRA_ID;SEC_SECAO',VarArrayOf([TransID, Secao]),[Lopartialkey,locaseinsensitive]);
  If Dm.IBSecaoSEC_STATUS.Value = '.' then
    RelAudit(dm.IBSecaoSEC_ID.Value,Dm.IBSecaoSEC_SECAO.Value,Dm.IBSecaoTRA_ID.Value );
  Dm.IBContagens.locate('CON_ID',ID,[Lopartialkey,locaseinsensitive]);
  Dm.IBContagens.EnableControls;
 End;

  //Chama tela de Consulta
  if key = vk_F3 then
   Begin
     Cod := 1;
     FrmLocalizar := TFrmLocalizar.Create(FrmLocalizar);
     try
      FrmLocalizar.ShowModal;
     finally
      FrmLocalizar.Release;
      FrmLocalizar := nil;
     end;
   End;

If (Shift = [ssCtrl]) and (Key = 65) and (Key = 46) Then
begin
Dm.SpDeletaBanco.open;
end;
end;


procedure TFrmInventario.BitBtn3Click(Sender: TObject);
var
 Txt: Textfile;
 Entrada: string;
Begin
//Deletar Tabela de Cadastro
if FileExists('C:\SGD\Roster\roster.ros') then
Begin
 Dm.QDeletaRoster.Open;
 Dm.Transacao.CommitRetaining;
 Dm.QDeletaRoster.Close;

    DM.IBRoster.Open;
    AssignFile(Txt,('C:\SGD\Roster\roster.ros')); // NOME do arquivo texto
    Reset(Txt);


    While not Eoln(Txt) do
     Begin
      Readln(Txt,Entrada);
      DM.IBRoster.Insert;
      DM.IBRosterROS_MATRICULA.Value := Copy(Entrada,14,11);
      Dm.IBRosterROS_NOME.Value := Copy(Entrada,26,29);
     End;
     Dm.IBRoster.Post;
     Dm.Transacao.CommitRetaining;
     Dm.IBRoster.First;
     CloseFile(Txt);
     Dm.IBRoster.close;
     Dm.IBRoster.Open;
     Application.MessageBox('Importa��o conclu�da','Mensagem do Sistema', MB_ICONINFORMATION+ MB_OK);
     Edit4.SetFocus;
End
else
        Application.MessageBox('O arquivo roster.ros n�o se encontra na pasta C:\SGD\Roster\','Mensagem do Sistema', MB_ICONERROR+ MB_OK);
end;

procedure TFrmInventario.SpeedButton1Click(Sender: TObject);
begin
//Transferir dados da tabela roster para a tabela de hor�rios, caso tenha algum registro
If Dm.IBRoster.RecordCount > 0 then
  Begin
   if Dm.IBHorarios.locate('ROS_MATRICULA',Dm.IBRosterROS_MATRICULA.Value,[Lopartialkey,locaseinsensitive]) = True then
    Application.MessageBox('Essa Matr�cula j� foi inclu�da','Mensagem do sistema', MB_ICONERROR+ MB_OK)
   else
    Begin
     Dm.IBHorarios.Insert;
     Dm.IBHorariosROS_MATRICULA.Value := Dm.IBRosterROS_MATRICULA.Value;
     Dm.IBHorariosROS_NOME.Value := Dm.IBRosterROS_NOME.Value;
     Dm.IBHorariosHOR_ENTRADA.Value := '';
     Dm.IBHorariosHOR_SAIDA.Value := '';
     Dm.IBHorarios.Post;
     Dm.Transacao.CommitRetaining;
     DBGrid7.SetFocus;
    End;
  End;
end;

procedure TFrmInventario.SpeedButton2Click(Sender: TObject);
begin
//Exluir registro de hor�rio caso tenha algum registro na tabela
If Dm.IBHorarios.RecordCount > 0 then
 Begin
  Dm.IBHorarios.Delete;
  Dm.Transacao.CommitRetaining;
 End;
End;

procedure TFrmInventario.FinalizarInventrio1Click(Sender: TObject);
begin

FrmFinalizaInvent := TFrmFinalizaInvent.Create(FrmFinalizaInvent);
try
  FrmFinalizaInvent.ShowModal;
finally
  FrmFinalizaInvent.Release;
  FrmFinalizaInvent := nil;
end;
end;

procedure TFrmInventario.GeraArquivoFinal1Click(Sender: TObject);
Var
Arquivo1, Arquivo2: TStringList;
DDia, DMes, DAno: String[2];
DData: String;
Cont, cont2 : integer;
Begin
   if (DM.IBInventarioINV_LOJA.Value = 'CLB') then
    GeraArqFinalCLB()
   else
   if (DM.IBInventarioINV_LOJA.Value = 'ASA') then
    GeraArqFinalASA()
   else
   if (DM.IBInventarioINV_LOJA.Value = 'CSV') then
    GeraArqFinalCSV()
   else
   if (DM.IBInventarioINV_LOJA.Value = 'FST') then
    GeraArqFinalFST()
   else
   if (DM.IBInventarioINV_LOJA.Value = 'DGA') then
    GeraArqFinalDGA()
   else
   if (DM.IBInventarioINV_LOJA.Value = 'LOP') then
    GeraArqFinalLOP()
   else
   if (DM.IBInventarioINV_LOJA.Value = 'COV') or (DM.IBInventarioINV_LOJA.Value = 'KIP') or (DM.IBInventarioINV_LOJA.Value = 'SAI') then
    GeraArqFinalCOV()
   else
   if (DM.IBInventarioINV_LOJA.Value = 'PDA') then
    GeraArqFinalPDA()
   else
   If (DM.IBInventarioINV_LOJA.Value = 'MRS') then
    GeraArqFinalMRS()
   else
   If (DM.IBInventarioINV_LOJA.Value = 'LUB') or (DM.IBInventarioINV_LOJA.Value = 'COR') or (DM.IBInventarioINV_LOJA.Value = 'EME')  then
    GeraArqFinalGEP()
   else
   If (DM.IBInventarioINV_LOJA.Value = 'CPV') then
    GeraArqFinalCPV()
   else
   If (DM.IBInventarioINV_LOJA.Value = 'LVC') then
    GeraArqFinalLVC();

    

    GeraArqFinalONF_CD();



   //GeraArqFinal();


//   begin
//   If
//  (DM.IBInventarioINV_LOJA.Value <> 'MRS') or (DM.IBInventarioINV_LOJA.Value <> 'PDA')then
//  GeraArqFinal();
end;



procedure TFrmInventario.DBGrid3KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
Secao, Status, TransID, dup : String;
Id, Id_secao: Integer;
SecaoNova, SecaoAntiga, Matricula: String;
SecaoID, SecTransID, NumCaracteres, secini, secfin : integer;
volta : boolean;
Dupli: integer;
Begin
//Pegar valor do ID da Linha
  
  IDSECAO := DM.IBSecaoSEC_ID.value;

  //Alterar Se��o com tecla de sobe e desce
  If ((Key = VK_DOWN) or (Key = VK_UP))
  and (Dm.DsSecao.State = DsEdit)
  and (Dm.IBSecaoSEC_STATUS.Value = '.')
  and (DM.IBSecaoSTATUS.Value <> 'ABE') then
  Begin
    key := 13;
  End;


  //###N�o deixa apagar registro do DbGrid, e s� coloca o STATUS como DEL###
  If (Shift = [ssCtrl]) and (Key = 46) Then
  Begin
      if CheckBox1.Checked = True then
       begin
         CheckBox1.Checked:= False;
       end;

    KEY := 0;
    UfuncProc.sec := true;
    Secao := Dm.IBSecaoSEC_SECAO.Value;
    //Secao := AjustaStrEsq(Secao, 4, '0');
    TransID := inttostr(DM.IBSecaoTRA_ID.value);
    Status := Dm.IBSecaoSEC_ABERTA.Value;
    dup := DM.IBSecaoSEC_DUPLI.Value;

    //###Caso queira tirar de deletado
    If Dm.IBSecaoSEC_STATUS.Value = 'DEL' Then
    Begin

      DM.IBSecao.Edit;

      DM.IBSecaoSEC_STATUS.Value := '.';

      //DM.IBSecao.Append;

      DM.IBSecao.Post;

      Dm.Transacao.CommitRetaining;

      //Caso for aceitar uma deletada que era duplicada
      If (DM.IBSecaoSEC_DUPLI.value = 'T') or (DM.IBSecaoSEC_DUPLI.value = '*') then  // or (DM.IBSecaoSEC_DUPLI.value = '*')
      Begin
        //Duplicidade(Secao);
        ColocaSecaoDupli(DM.IBSecaoSEC_ID.Value,Dm.IBSecaoSEC_SECAO.Value);
      End;
      // Tira as contagens de Deletada, caso exita contagens, pois a se��o pode ser uma Se��o Aberta sem contagens
      if  Dm.IBContagens.locate('TRA_ID;SEC_SECAO',VarArrayOf([TransID, Secao]),[Lopartialkey,locaseinsensitive]) = True then
      Begin
        While not (Dm.IBContagens.Eof) and (inttostr(Dm.IBContagensTRA_ID.Value) = TransID) and (Dm.IBContagensSEC_SECAO.Value = Secao)  Do
        Begin
          Dm.IBContagens.Edit;
          Dm.IBContagensCON_STATUS.Value := '.';
          Dm.IBContagens.Post;
          Dm.Transacao.CommitRetaining;
          Dm.IBContagens.Next;
        End;
        Dm.Transacao.CommitRetaining;
      End;
      if (TransID <> '0') then
        GravaAltDbGridSecao(IDSECAO, strtoint(TransID), Dm.IBSecaoHOR_MATRICULA.Value, Secao, Secao);
    End
    else if Dm.IBSecaoSEC_STATUS.Value = '.' then //###Caso queira deletar
    Begin
      DM.IBSecao.Edit;
      DM.IBSecaoSEC_STATUS.Value := 'DEL';

      DM.IBSEcao1.Close;
      DM.IBSEcao1.SelectSQL.Clear;
      DM.IBSEcao1.SelectSQL.Add('select count(*) as qntd from secao where sec_secao = :secao and sec_status = "."');
      DM.IBSEcao1.ParamByName('secao').Value := Secao;
      DM.IBSEcao1.Open;

      if (TransID <> '0') and (DM.IBSEcao1.FieldByName('qntd').AsInteger = 1) and (Dm.IBSecaoSEC_INESPERADA.Value <> 'T') then
      begin
        Dm.qrySQL.Close;
        DM.qrySQL.SQL.Clear;
        DM.qrySQL.SQL.Add('select coalesce(max(sec_id),0) as ID from secao');
        DM.qrySQL.Open;
        Id_secao := 0;
        Id_secao := DM.qrySQL.FieldByName('ID').AsInteger + 1;

        Dm.qrySQL.Close;
        DM.qrySQL.SQL.Clear;
        DM.qrySQL.SQL.Add('Insert into SECAO (SEC_ID, SEC_SECAO, SEC_ABERTA, SEC_INESPERADA, SEC_DUPLI, SEC_STATUS) ');
        DM.qrySQL.SQL.Add('values (:SEC_ID, :SEC_SECAO, :SEC_ABERTA, :SEC_INESPERADA, :SEC_DUPLI, :SEC_STATUS) ');
        DM.qrySQL.ParamByName('SEC_ID').Value         := Id_secao;
        DM.qrySQL.ParamByName('SEC_SECAO').Value      := Secao;
        DM.qrySQL.ParamByName('SEC_ABERTA').Value     := 'T';
        DM.qrySQL.ParamByName('SEC_INESPERADA').Value := 'F';
        DM.qrySQL.ParamByName('SEC_DUPLI').Value      := 'F';
        DM.qrySQL.ParamByName('SEC_STATUS').Value     := '.';
        DM.qrySQL.ExecSQL;

        {
        DM.IBSecao.Append;
        DM.IBSecaoSEC_SECAO.Value := Secao;
        DM.IBSecaoSEC_ABERTA.Value := 'T';
        Dm.IBSecaoSEC_STATUS.Value := '.';
        Dm.IBSecaoSEC_INESPERADA.Value := 'F';
        Dm.IBSecaoSEC_DUPLI.Value := 'F'; }
      end;
      //endi
      DM.IBSecao.Post;
      Dm.Transacao.CommitRetaining;

      If DM.IBSecaoSEC_DUPLI.value = 'T' then
      Begin
        Duplicidade(Secao);
      End;
      TiraSecaoDupli(DM.IBSecaoSEC_ID.value,Dm.IBSecaoSEC_SECAO.value);
      // Coloca as contagens em Deletada, caso exita contagens, pois a se��o pode ser uma Se��o Aberta sem contagens
      if  Dm.IBContagens.locate('TRA_ID;SEC_SECAO',VarArrayOf([TransID, Secao]),[Lopartialkey,locaseinsensitive]) = True then
      Begin
        While not (Dm.IBContagens.Eof) and (inttostr(Dm.IBContagensTRA_ID.Value) = TransID) and (Dm.IBContagensSEC_SECAO.Value = Secao)  Do
        Begin
          Dm.IBContagens.Edit;
          Dm.IBContagensCON_STATUS.Value := 'DEL';
          Dm.IBContagens.Post;
         // Dm.Transacao.CommitRetaining;   pq ???
          Dm.IBContagens.Next;
        End;
        Dm.Transacao.CommitRetaining;
      End;
    End;

    UFuncProc.sec := false;
    duplicidade(Dm.IBSecaoSEC_SECAO.Value);
    PosicionaSecao(IDSECAO);

    Dm.QGeraRelFinal.Close;
    Dm.QGeraRelFinal.Open;

   // Dm2.QMap_total.Close;
    //Dm2.QMap_total.Open;
    //Dm2.QMap_totalGeral.Close;
    //Dm2.QMap_totalGeral.Open;
  End;

  //###Imprime relat�rio de auditoria###
  if (key = vk_F1) and (Dm.IBSecaoSEC_STATUS.Value = '.') then
   Begin
    RelAudit(Dm.IBSecaoSEC_ID.value, Dm.IBSecaoSEC_SECAO.Value, Dm.IBSecaoTRA_ID.value);
   End;

  //Chama fun��o aceita duplicidade
  if (key = vk_F4) and (Dm.IBSecaoSEC_DUPLI.Value = 'T') and (Dm.IBSecaoSEC_INESPERADA.Value = 'F') then
  Begin
    Dm.SpAceitaDupli.Close;
    Dm.SpAceitaDupli.ParamByName('SEC_SECAO').Value := Dm.IBSecaoSEC_SECAO.Value;
    Dm.SpAceitaDupli.ExecProc;
    Dm.SpAceitaDupli.Close;
    Dm.Transacao.CommitRetaining;
    PosicionaSecao(IDSECAO);
    AceitaDuplicidade(Dm.IBSecaoSEC_ID.Value,Dm.IBSecaoSec_Secao.value);
   // Dm2.QMap_total.Close;
    //Dm2.QMap_total.Open;
    //Dm2.QMap_totalGeral.Close;
    //Dm2.QMap_totalGeral.Open;
  End;
// AceitaDuplicidade(inttostr(Dm.IBSecaoTra_ID.value), Dm.IBSecaoSec_Secao.value);
  //AceitaDuplicidade(Dm.IBSecaoSEC_ID.Value,Dm.IBSecaoSec_Secao.value);
  //Chama tela de consulta
  if key = vk_F3 then
   Begin
     Cod := 2;
     FrmLocalizar := TFrmLocalizar.Create(FrmLocalizar);
     try
      FrmLocalizar.ShowModal;
     finally
      FrmLocalizar.Release;
      FrmLocalizar := nil;
     end;
   End;


//Inserta Se��o
  if (key = vk_F5) and (Dm.IBSecaoSEC_INESPERADA.Value = 'T') then
    Begin
      With Dm.qrySQL do
        begin
          close;
          Sql.Clear;
          Sql.add ('SELECT ENDERECO FROM LAYOUT_INPUT WHERE CLIENTE = :CLIENTE');
          ParamByName('CLIENTE').Value:= Dm.IBInventarioINV_LOJA.Value;
          open;
         end;

      if DM.qrySQL.FieldByName('ENDERECO').asString <> 'T' then
        Begin

          Id := Dm.IBSecaoSEC_ID.Value;
          secini := Dm.IBSecaoSEC_SECAO.AsInteger;
          secfin := Dm.IBSecaoSEC_SECAO.AsInteger;
         //verifica se existem mapeamentos cadastrados que sejam subsequentes desta se��o
          DM.IBMapeamento.Close;
          Dm.IBMapeamento.ParamByName('secini').Value := '0';
          Dm.IBMapeamento.ParamByName('secfin').Value := secini - 1;
          DM.IBMapeamento.Open;
          Dm.IBMapeamento.Last;
          volta := true;
          while not(Dm.IBMapeamento.Bof) and volta do
          begin

            if ((secini - 1) = DM.IBMapeamentoMAP_SECFIN.AsInteger) then
               begin
                secini := DM.IBMapeamentoMAP_SECINI.AsInteger;
             end
            else
            volta := false;
            Dm.IBMapeamento.Prior;
           end;
          //endwh

          DM.IBMapeamento.Close;
          If (SecFin < 9999) then
          begin
          Dm.IBMapeamento.ParamByName('secini').Value := secfin+1;
          end
          else
          Dm.IBMapeamento.ParamByName('secini').Value := secfin;
          Dm.IBMapeamento.ParamByName('secfin').Value := '9999';
          DM.IBMapeamento.Open;
          Dm.IBMapeamento.First;
          volta := true;

          while not(Dm.IBMapeamento.Eof) and volta do
          begin
            if ((secfin + 1) = DM.IBMapeamentoMAP_SECINI.AsInteger) then
            begin
              secfin := DM.IBMapeamentoMAP_SECFIN.AsInteger;
            end
            else

              volta := false;

           // Dm2.QMap_total.Close;
           // Dm2.QMap_total.Open;
            //Dm2.QMap_totalGeral.Close;
            //Dm2.QMap_totalGeral.Open;
          end;
          //

           CadastraMapeamento(IntToStr(SecIni),IntToStr(SecFin), '');

           PosicionaSecao(IDSECAO);
        end
      else
        begin

          CadastraMapeamentoEnd(Dm.IBSecaoSEC_SECAO.AsString, Dm.IBSecaoSEC_SECAO.AsString, '');

          PosicionaSecao(IDSECAO);
        end;
     End;


     // Dm2.QMap_total.Close;

     // Dm2.QMap_total.Open;

     // Dm2.QMap_totalGeral.Close;

     // Dm2.QMap_totalGeral.Open;



END;

procedure TFrmInventario.RelatrioTotaldePeaseFinanceiro1Click(
  Sender: TObject);
begin
{If (Dm.IBInventarioINV_LOJA.Value = 'COV') or (Dm.IBInventarioINV_LOJA.Value = 'PDA') then
  begin
    Timer2.Enabled:=false;
    Dm.QGeraRelFinal.Close;
    Dm.QGeraRelFinal.SQL.Clear;
    Dm.QGeraRelFinal.Sql.Add('select sum(preco) as preco, sum(QTD) as QTD from CONTAGENS_CONSO D inner JOIN CADASTRO C ON (C.CAD_EAN = D.CON_EAN)');
    Dm.QGeraRelFinal.Open;
    Dm.QGeraRelFinal3.Close;
    Dm.QGeraRelFinal3.SQL.Clear;
    Dm.QGeraRelFinal3.Sql.Add('select sum(preco) as preco, sum(QTD) as QTD from CONTAGENS_CONSO D left JOIN CADASTRO C ON (C.CAD_EAN = D.CON_EAN)');
    Dm.QGeraRelFinal3.Open;
    Dm.RvSysWis.DefaultDest := rdPreview;
    Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
    Dm.RvPrjWis.ExecuteReport('Final');
    Timer2.Enabled:=true;
  end
  Else
    Begin
 }
If (Dm.IBInventarioINV_LOJA.Value = 'PDA') Or (Dm.IBInventarioINV_LOJA.Value = 'COV') or (Dm.IBInventarioINV_LOJA.Value = 'ASA')then
      begin
        Timer2.Enabled:=false;
        Dm.QGeraRelFinal.close;
        Dm.QGeraRelFinal.SQL.Clear;
        Dm.QGeraRelFinal.Sql.Add('SELECT SUM(ROUND(D.QTD)) AS QTD, SUM(D.PRECO) AS PRECO FROM CONTAGENS_CONSO D INNER JOIN cadastro_conso C on (C.cad_ean = D.con_ean)');
        Dm.QGeraRelFinal.Open;
        Dm.QGeraRelFinal3.Close;
        Dm.QGeraRelFinal3.SQL.Clear;
        Dm.QGeraRelFinal3.Sql.Add('select sum(preco) as preco, sum(ROUND(QTD)) as QTD from CONTAGENS_CONSO D left JOIN CADASTRO_conso C ON (C.CAD_EAN = D.CON_EAN)');
        Dm.QGeraRelFinal3.Open;
        DM.RvPrjWis.Close;
        Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Final.rav';
        DM.RvPrjWis.Open;
        Dm.RvSysWis.DefaultDest := rdPreview;
        Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups + [ssAllowSetup];
        Dm.RvPrjWis.ExecuteReport('Final');
        Timer2.Enabled:=true;
      end
else
  begin
      Timer2.Enabled:=false;
      Dm.QGeraRelFinal.Close;
      Dm.QGeraRelFinal.Sql.Clear;
      Dm.QGeraRelFinal.Sql.Add('Select sum(QTD) as QTD, sum(preco) as preco from contagens_conso_GERAL where cad_Codint is not null');
      Dm.QGeraRelFinal.Open;
      DM.RvPrjWis.Close;
      Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Final.rav';
      DM.RvPrjWis.Open;
      Dm.RvSysWis.DefaultDest := rdPreview;
      Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups + [ssAllowSetup];
      Dm.RvPrjWis.ExecuteReport('Final');
      Timer2.Enabled:=true;
  End;




// Salvando relat�rio em PDF automaticamente
Try
  Dm.RvSysWis.DefaultDest:= rdFile;
  Dm.RvSysWis.DoNativeOutput:= false;
  Dm.RvSysWis.RenderObject:= rvPdf;
  Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Relatorio Total de Pecas e Financeiro_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
  Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
  Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Final.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
  Dm.RvPrjWis.Engine:= Dm.RvSysWis;
  Dm.RvPrjWis.Execute;
Except
  ShowMessage('Erro ao gerar o relat�rio em PDF');
end;

// Salvando relat�rio em TXT automaticamente
Try
  Dm.RvSysWis.DefaultDest:= rdFile;
  Dm.RvSysWis.DoNativeOutput:= false;
  Dm.RvSysWis.RenderObject:= rvTxt;
  Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Relatorio Total de Pecas e Financeiro_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.txt'; //caminho onde vai gerar o arquivo pdf
  Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
  Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Final.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
  Dm.RvPrjWis.Engine:= Dm.RvSysWis;
  Dm.RvPrjWis.Execute;
except
  ShowMessage('Erro ao gerar o relat�rio em TXT');
end;

end;

procedure TFrmInventario.xtcomtotaldeHorasdosConferente1Click(
  Sender: TObject);
Var
Arquivo1: TStringList;
Begin
  DM.IBHorarios.Open;
DM.IBHorarios.First;


 Arquivo1 := TStringList.Create; //Instancia a variavel Arquivo

 //GRAVA LINHAS NOS ARQUIVOS

  While Not DM.IBHorarios.Eof Do
   Begin
      Arquivo1.Add(Dm.IBHorariosROS_MATRICULA.Value + '   ' + Dm.IBHorariosHOR_ENTRADA.Value + '   ' + Dm.IBHorariosHOR_SAIDA.Value + '   ' + Dm.IBHorariosHOR_TOTALHORA.Value+ '  '+Dm.IBHorariosROS_NOME.AsString) ;
      DM.IBHorarios.Next;
   End;
     Arquivo1.Add('');
     Arquivo1.Add('Total de Horas: '+EdtHoras.Text+'     Total Pessoas: '+EdtPessoas.text);
     Arquivo1.SaveToFile('C:\SGD\Horas Conferentes\Horas_OS '+ Dm.IBInventarioINV_OS.Value + '.TXT');
     FreeAndNil(Arquivo1);
     Application.MessageBox('Arquivo de horas gerados com sucesso','Mensagem do sistema', MB_ICONINFORMATION+ MB_OK);
end;


procedure TFrmInventario.TabSheet4Show(Sender: TObject);
begin
if Panel11.Enabled = True then
  DBGrid2.SetFocus;

 //Abre e fecha Transmiss�es
 Dm.IBTransmissao.Close;
 Dm.IBTransmissao.Open;
end;

procedure TFrmInventario.RelatriosdeAuditoria1Click(Sender: TObject);
begin
FrmSelSecao := TFrmSelSecao.Create(FrmSelSecao);
try
  FrmSelSecao.ShowModal;
finally
  FrmSelSecao.Release;
  FrmSelSecao := nil;
end;

end;

procedure TFrmInventario.AtalhosdoSistema1Click(Sender: TObject);
begin
 ShowMessage ('Tela de Se��es: ' + #13 +
              'F1 = Imprimir relat�rio de Auditoria ' + #13 +
              'F3 = Tela de Pesquisa ' + #13 +
              'F4 = Aceita Duplicidade ' + #13 +
              'F5 = Insere se��o no mapeamento ' + #13 +
              'Ctrl + Del = Deleta Se��o ' + #13 +
              'Alt + A = Ativa visualiza��o apenas das contagens da se��o selecionada ' + #13 +
              ' '  + #13 +
              'Tela de Contagem: ' + #13 +
              'F3 = Tela de Pesquisa ' + #13 +
              'Ctrl + Del = Deleta Contagem ');

end;

procedure TFrmInventario.TabSheet1Show(Sender: TObject);
begin
 If Panel4.Enabled = True then
   Panel4.SetFocus;

 Dm.QGrafico.Close;
 Dm.QGrafico.Open;
end;

procedure TFrmInventario.DesabilitarimportaoAutomatica1Click(
  Sender: TObject);
begin
 If Timer1.Enabled = True then
   Begin
    Timer1.Enabled := False;
    MainMenu1.Items[0].Items[1].Caption := 'Habilitar Importa��o das Transmiss�es';
     Application.MessageBox('O sistema n�o importar� mais as coletas descarregadas pelo coletor','Mensagem do sistema', MB_ICONEXCLAMATION+ MB_OK);
   End
  Else
   Begin
    Timer1.Enabled := True;
    MainMenu1.Items[0].Items[1].Caption := 'Desabilitar Importa��o das Transmiss�es';
     Application.MessageBox('O sistema importar� automaticamente as coletas descarregadas pelo coletor','Mensagem do sistema', MB_ICONEXCLAMATION+ MB_OK);

   End;
    end;

procedure TFrmInventario.RelatriodePsicount1Click(Sender: TObject);
begin
 Try
   Dm.QRelPiceCount.Close;
   Dm.QRelPiceCount.Open;
   if Dm.QRelPiceCount.RecordCount > 0 then
      Begin
        Dm.RvPrjWis.Close;
        Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\PiceCount.rav';
        Dm.RvPrjWis.Open;
        Dm.RvSysWis.DefaultDest := rdPreview;
        Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
        Dm.RvPrjWis.ExecuteReport('PiceCount');
        // Salvando relat�rio em PDF automaticamente
          Try
            Dm.RvSysWis.DefaultDest:= rdFile;
            Dm.RvSysWis.DoNativeOutput:= false;
            Dm.RvSysWis.RenderObject:= rvPdf;
            Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Relatorio PieceCount_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
            Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
            Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\PiceCount.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
            Dm.RvPrjWis.Engine:= Dm.RvSysWis;
            Dm.RvPrjWis.Execute;
          Except
            ShowMessage('Erro ao gerar o relat�rio em PDF');
          end;
        // Salvando relat�rio em TXT automaticamente

          Try
            Dm.RvSysWis.DefaultDest:= rdFile;
            Dm.RvSysWis.DoNativeOutput:= false;
            Dm.RvSysWis.RenderObject:= rvTxt;
            Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Relatorio PieceCount_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.txt'; //caminho onde vai gerar o arquivo pdf
            Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
            Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\PiceCount.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
            Dm.RvPrjWis.Engine:= Dm.RvSysWis;
            Dm.RvPrjWis.Execute;
          except
            ShowMessage('Erro ao gerar o relat�rio em TXT');
          end;
      end

        else
               Application.MessageBox('N�o existem se��es contadas','Mensagem do sistema', MB_ICONERROR+ MB_OK);
        Finally
   Dm.QRelPiceCount.Close;
 End;
end;

procedure TFrmInventario.TabSheet2Show(Sender: TObject);
begin

 if Panel9.Enabled = True then
   Edit4.SetFocus;

// DM.IBHorarios.Open;
// Dm.IBHorarios.First;

// While not Dm.IBHorarios.Eof do
//   Begin
//     if (Dm.IBHorariosHOR_ENTRADA.Value <> '') and (Dm.IBHorariosHOR_SAIDA.Value <> '') then
//       Begin
//         Dm.IBHorarios.Edit;
//         Dm.IBHorariosHOR_TOTALHORA.Value := CalculaTotaldeHoras(Dm.IBHorariosHOR_ENTRADA.Value,Dm.IBHorariosHOR_SAIDA.Value);
//         Dm.IBHorarios.Post;
//       End;
//     Dm.IBHorarios.Next;
//   End;

// Dm.Transacao.CommitRetaining;

end;

procedure TFrmInventario.FormActivate(Sender: TObject);
begin
  //Alinha Panel
   AlinharPanel(Panel10, Panel1, True);

end;
procedure TFrmInventario.RelatriodeDuplicidades1Click(Sender: TObject);
begin
Try
  Dm.QRelDupli.Close;
  Dm.QRelDupli.Open;
   if Dm.QRelDupli.RecordCount > 0 then
      begin
      Dm.RvPrjWis.Close;
      Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Duplicidade.rav';
      Dm.RvPrjWis.Open;
      Dm.RvSysWis.DefaultDest := rdPreview;
      Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
      Dm.RvPrjWis.ExecuteReport('Duplicidade');
      // Salvando relat�rio em PDF automaticamente
          Try
            Dm.RvSysWis.DefaultDest:= rdFile;
            Dm.RvSysWis.DoNativeOutput:= false;
            Dm.RvSysWis.RenderObject:= rvPdf;
            Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Relatorio de Duplicidades_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
            Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
            Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Duplicidade.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
            Dm.RvPrjWis.Engine:= Dm.RvSysWis;
            Dm.RvPrjWis.Execute;
          Except
            ShowMessage('Erro ao gerar o relat�rio em PDF');
          end;
        // Salvando relat�rio em TXT automaticamente

          Try
            Dm.RvSysWis.DefaultDest:= rdFile;
            Dm.RvSysWis.DoNativeOutput:= false;
            Dm.RvSysWis.RenderObject:= rvTxt;
            Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Relatorio de Duplicidades_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.txt'; //caminho onde vai gerar o arquivo pdf
            Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
            Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Duplicidade.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
            Dm.RvPrjWis.Engine:= Dm.RvSysWis;
            Dm.RvPrjWis.Execute;
          except
            ShowMessage('Erro ao gerar o relat�rio em TXT');
          end;
      end
   else
           Application.MessageBox('N�o existem se��es duplicadas','Mensagem do sistema', MB_ICONEXCLAMATION+ MB_OK);
 Finally
   Dm.QRelDupli.Close;
 End;

end;

procedure TFrmInventario.RelatriodeSeesAbertas1Click(Sender: TObject);
begin
Try
  Dm.QRelAberta.Close;
  Dm.QRelAberta.Open;
   if Dm.QRelAberta.RecordCount > 0 then
      begin
      Dm.RvPrjWis.Close;
      Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Abertas.rav';
      Dm.RvPrjWis.Open;
      Dm.RvSysWis.DefaultDest := rdPreview;
      Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
      Dm.RvPrjWis.ExecuteReport('Abertas');
       // Salvando relat�rio em PDF automaticamente
          Try
            Dm.RvSysWis.DefaultDest:= rdFile;
            Dm.RvSysWis.DoNativeOutput:= false;
            Dm.RvSysWis.RenderObject:= rvPdf;
            Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Relatorio de Secoes Abertas_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
            Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
            Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Abertas.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
            Dm.RvPrjWis.Engine:= Dm.RvSysWis;
            Dm.RvPrjWis.Execute;
          Except
            ShowMessage('Erro ao gerar o relat�rio em PDF');
          end;
        // Salvando relat�rio em TXT automaticamente

          Try
            Dm.RvSysWis.DefaultDest:= rdFile;
            Dm.RvSysWis.DoNativeOutput:= false;
            Dm.RvSysWis.RenderObject:= rvTxt;
            Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Relatorio de Secoes Abertas_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.txt'; //caminho onde vai gerar o arquivo pdf
            Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
            Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Abertas.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
            Dm.RvPrjWis.Engine:= Dm.RvSysWis;
            Dm.RvPrjWis.Execute;
          except
            ShowMessage('Erro ao gerar o relat�rio em TXT');
          end;
      end
   else
           Application.MessageBox('N�o existem se��es abertas','Mensagem do sistema', MB_ICONEXCLAMATION+ MB_OK);
 Finally
   Dm.QRelAberta.Close;
 End;

end;

procedure TFrmInventario.DBGrid5KeyPress(Sender: TObject; var Key: Char);
begin
//Quando � alterado uma quantidade e pressionado ENTER,grava a nova quantidade
if (Key = #13) and (Dm.DsContagens.State = Dsedit)  then
  Begin
    GravaAltDbGridContagem;
  End;
End;

procedure TFrmInventario.DBGrid3KeyPress(Sender: TObject; var Key: Char);
var
SecaoNova, SecaoAntiga, Matricula: String;
SecaoID, SecTransID : integer;
NumCaracteres: integer;
Secao : string;
begin
MouseShowCursor(false);
    If (Key = #27) then
     begin
       MouseShowCursor(True);
     end;

  //Alterar Se��o com tecla Enter
  If (Key = #13)
  and (Dm.DsSecao.State = DsEdit)
  and (Dm.IBSecaoSEC_STATUS.Value = '.')
  and (DM.IBSecaoSTATUS.Value <> 'ABE') then
  Begin
    SecTransId := Dm.IBSecaoTRA_ID.value;
    ShowMessage('Trans ID' +' '+intToStr(SecTransId));

    SecaoAntiga := Dm.IBSecaoSEC_SECAO.OldValue;
    ShowMessage('Se��o antiga' +' '+(SecaoAntiga));

    SecaoID := Dm.IBSecaoSEC_ID.value;
    ShowMessage('ID Se��o' +' '+intToStr(SecaoID));

    Matricula := DM.IBSecaohor_Matricula.value;
    ShowMessage('Matricula' +' '+(Matricula));

    SecaoNova := Dm.IBSecaoSEC_SECAO.value;
    ShowMessage('Se��o Nova' +' '+SecaoNova);

    NumCaracteres := Length(SecaoNova);
    ShowMessage('N�mero de caracteres' +' '+intToStr(NumCaracteres));

    Secao := (SecaoNova);
    ShowMessage('Se��o Nova 2' +' '+SecaoNova);

    With Dm.qrySQL do
        begin
          close;
          Sql.Clear;
          Sql.add ('SELECT ENDERECO FROM LAYOUT_INPUT WHERE CLIENTE = :CLIENTE');
          ParamByName('CLIENTE').Value:= Dm.IBInventarioINV_LOJA.Value;
          open;
         end;

      if DM.qrySQL.FieldByName('ENDERECO').asString <> 'T' then
        Begin

         If (NumCaracteres <> 4) or (Secao < '0001') or (BoolToStr(IntOK) = '-1') then
          Begin
           ShowMessage('N�mero de Se��o Inv�lido');
           Dm.Transacao.RollbackRetaining;
           Dm.IBSecao.Cancel;
           MouseShowCursor(True);
          End;

        end
      else
     begin
       try
          SecaoNova := Dm.IBSecaoSEC_SECAO.value;
          ShowMessage('Se��o Nova, else' +' '+SecaoNova);

          Dm.IBSecao.Post;
          ShowMessage('postou');

          //ShowMessage(SecaoAntiga);
          //ShowMessage(SecaoNova);
          Dm.SpAlteraSec.Close;
          Dm.SpAlteraSec.ParamByName('NOVASEC').Value := SecaoNova;
          Dm.SpAlteraSec.ParamByName('VELHASEC').Value := SecaoAntiga;
          Dm.SpAlteraSec.ParamByName('TRANSID').Value := SecTransID;
          Dm.SpAlteraSec.ParamByName('ID_SECAO').Value := SecaoID;
          ShowMessage('ID' + ' '+intTostr(SecaoID));
          Dm.SpAlteraSec.ExecProc;

          ShowMessage('Executou Procedure');

          GravaAltDbGridSecao(SecaoID, SecTransID, Matricula, SecaoAntiga, SecaoNova);

          ShowMessage('Gravou grid se��o');

          Duplicidade(SecaoAntiga);

          ShowMessage('Verificou duplicidade antiga');

          Duplicidade(SecaoNova);

          ShowMessage('Verificou duplicidade nova');

          PosicionaSecao(IDSECAO);

          ShowMessage('posicionou se��o');

          Dm.Transacao.CommitRetaining;

          ShowMessage('comitou');

          MouseShowCursor(True);
       // end;
       except
        ShowMessage('Erro na altera��o, por favor tente novamente');
        DM.IBSecao.Edit;
        DM.IBSecaoSEC_SECAO.Value := SecaoAntiga;
        MouseShowCursor(True);
        end;
     end;

 end
 Else if (DM.IBSecao.State in [dsedit,dsinsert]) and (key = #13) then
  Begin
      Dm.IBSecao.Cancel;
      MouseShowCursor(True);
  End;

  if not(Key in ['0'..'9']) then
    Key := #0;

end;
procedure TFrmInventario.Edit4Change(Sender: TObject);
begin
Dm.IBRoster.locate('ROS_MATRICULA',Edit4.Text,[Lopartialkey,locaseinsensitive]);
end;

procedure TFrmInventario.TabSheet7Show(Sender: TObject);
begin
 Dm.IBContagens.Close;
 //Dm.CdsContagens.Close;
 Dm.IBContagens.Open;
 //Dm.CdsContagens.Open;

 FrmInventario.DBGrid5.Refresh;

 Dm.IBContagens.locate('CON_ID',IDCONTAGEM,[Lopartialkey,locaseinsensitive]);

  if Panel17.Enabled = True then
   DBGrid5.SetFocus;
end;

procedure TFrmInventario.RelatriodeSeesInesperadas1Click(Sender: TObject);
begin
Try
  Dm.QRelInesperadas.Close;
  Dm.QRelInesperadas.Open;
  if Dm.QRelInesperadas.RecordCount > 0 then
       begin
        Dm.RvPrjWis.Close;
        Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Inesperadas.rav';
        Dm.RvPrjWis.Open;
        Dm.RvSysWis.DefaultDest := rdPreview;
        Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
        Dm.RvPrjWis.ExecuteReport('Inesperadas');
         // Salvando relat�rio em PDF automaticamente
          Try
            Dm.RvSysWis.DefaultDest:= rdFile;
            Dm.RvSysWis.DoNativeOutput:= false;
            Dm.RvSysWis.RenderObject:= rvPdf;
            Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Relatorio de Secoes Inesperadas_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
            Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
            Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Inesperadas.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
            Dm.RvPrjWis.Engine:= Dm.RvSysWis;
            Dm.RvPrjWis.Execute;
          Except
            ShowMessage('Erro ao gerar o relat�rio em PDF');
          end;
        // Salvando relat�rio em TXT automaticamente

          Try
            Dm.RvSysWis.DefaultDest:= rdFile;
            Dm.RvSysWis.DoNativeOutput:= false;
            Dm.RvSysWis.RenderObject:= rvTxt;
            Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Relatorio de Secoes Inesperadas_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.txt'; //caminho onde vai gerar o arquivo pdf
            Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
            Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Inesperadas.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
            Dm.RvPrjWis.Engine:= Dm.RvSysWis;
            Dm.RvPrjWis.Execute;
          except
            ShowMessage('Erro ao gerar o relat�rio em TXT');
          end;
      end
  else
           Application.MessageBox('N�o existem se��es inesperadas','Mensagem do sistema', MB_ICONEXCLAMATION+ MB_OK);

 Finally
   Dm.QRelInesperadas.Close;
 End;
end;

procedure TFrmInventario.DBGrid3CellClick(Column: TColumn);
begin
//Pegar valor do ID da Linha
IDSECAO := DM.IBSecaoSEC_ID.value;



end;

procedure TFrmInventario.DBGrid5CellClick(Column: TColumn);
begin
If click = true then
click :=false
else
click := true;
//Pega valor do ID da linha
 IDCONTAGEM := DM.IBContagensCON_ID.Value;
  Dm.IBContagens.Refresh;
  Dm.IBArea.Close;
  Dm.IBArea.Open;
end;

procedure TFrmInventario.RelatriodeItensAlteradosdepoisdedescarregados1Click(
  Sender: TObject);
begin
Try
  Dm.QRelItensAlterados.Close;
  Dm.QRelItensAlterados.Open;
  if Dm.QRelItensAlterados.RecordCount > 0 then
      begin
        Dm.RvPrjWis.Close;
        Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\ItensAlterados.rav';
        Dm.RvPrjWis.Open;
        Dm.RvSysWis.DefaultDest := rdPreview;
        Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
        Dm.RvPrjWis.ExecuteReport('ItensAlterados');
         // Salvando relat�rio em PDF automaticamente
          Try
            Dm.RvSysWis.DefaultDest:= rdFile;
            Dm.RvSysWis.DoNativeOutput:= false;
            Dm.RvSysWis.RenderObject:= rvPdf;
            Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Relatorio de Quantidades Alteradas_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
            Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
            Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\ItensAlterados.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
            Dm.RvPrjWis.Engine:= Dm.RvSysWis;
            Dm.RvPrjWis.Execute;
          Except
            ShowMessage('Erro ao gerar o relat�rio em PDF');
          end;
        // Salvando relat�rio em TXT automaticamente

          Try
            Dm.RvSysWis.DefaultDest:= rdFile;
            Dm.RvSysWis.DoNativeOutput:= false;
            Dm.RvSysWis.RenderObject:= rvTxt;
            Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Relatorio de Quantidades Alteradas_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.txt'; //caminho onde vai gerar o arquivo pdf
            Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
            Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\ItensAlterados.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
            Dm.RvPrjWis.Engine:= Dm.RvSysWis;
            Dm.RvPrjWis.Execute;
          except
            ShowMessage('Erro ao gerar o relat�rio em TXT');
          end;
      end
   else
          Application.MessageBox('N�o existem quantidades alteradas','Mensagem do sistema', MB_ICONEXCLAMATION+ MB_OK);
 Finally
   Dm.QRelItensAlterados.Close;
 End;

end;

procedure TFrmInventario.DBGrid5ColExit(Sender: TObject);
begin
{if (Dm.DsContagens.State = Dsedit) then
  GravaAltDbGridContagem;
  Dm.QGeraRelFinal.Close;
  Dm.QGeraRelFinal.Open;
  Dm.IBArea.Close;
  Dm.IBArea.Open;
}
end;

procedure TFrmInventario.TabSheet7Exit(Sender: TObject);
begin
if (Dm.DsContagens.State = Dsedit) then
  GravaAltDbGridContagem;
end;

procedure TFrmInventario.DBGrid5Exit(Sender: TObject);
begin

end;
{if (Dm.DsContagens.State = Dsedit) then
   GravaAltDbGridContagem;
}

procedure TFrmInventario.ApplicationEvents1Message(var Msg: tagMSG;
  var Handled: Boolean);
var
  i: SmallInt;
begin
//Fazer funcionar o Scroll do Mouse
  if Msg.message = WM_MOUSEWHEEL then
  begin
    Msg.message := WM_KEYDOWN;
    Msg.lParam := 0;
    i := HiWord(Msg.wParam) ;
    if i > 0 then
      Msg.wParam := VK_UP
    else
      Msg.wParam := VK_DOWN;
    Handled := False;
   end;
end;

procedure TFrmInventario.BitBtn4Click(Sender: TObject);
begin
   If Dm.IBHorarios.RecordCount > 0 then
    Begin
      FrmHorario := TFrmHorario.Create(FrmHorario);
      Try
       FrmHorario.ShowModal;
      Finally
       FrmHorario.Release;
       FrmHorario := nil;
     End;
    End
   Else
         Application.MessageBox('N�o existe nenhum conferente para ser abetrto o hor�rio','Mensagem do sistema', MB_ICONERROR+ MB_OK);


end;

procedure TFrmInventario.FormShow(Sender: TObject);
var prod: real;
begin
 Dm.IBInventario.Open;
If (Dm.IBInventarioINV_LOJA.Value = 'PDA') Or (Dm.IBInventarioINV_LOJA.Value = 'COV') or (Dm.IBInventarioINV_LOJA.Value = 'ASA')then
      begin
        Dm.QGeraRelFinal.close;
        Dm.QGeraRelFinal.SQL.Clear;
        Dm.QGeraRelFinal.Sql.Add('SELECT Round(SUM(QTD)) AS QTD, SUM(PRECO) AS PRECO FROM CONTAGENS_CONSO');
        Dm.QGeraRelFinal.Open;
      end
     else
      begin
       Dm.QGeraRelFinal.close;
        Dm.QGeraRelFinal.SQL.Clear;
        Dm.QGeraRelFinal.Sql.Add('SELECT SUM(QTD) AS QTD, SUM(PRECO) AS PRECO FROM CONTAGENS_CONSO_GERAL');
        Dm.QGeraRelFinal.Open;
      end;

    //Busca o total de pessoas no invent�rio que j� fizeram ao menos uma transmissao
    Dm.IBHorarios.First;
    Dm.IBHorarios.Last;
    EdtPessoas.Text:=IntToStr(Dm.IBHorarios.RecordCount);

    try
      //Busca o total de horas do form produtividade
     //EdtHoras.Text:=FormatFloat('##,###0.00',(TotalHoras));
      EdtHoras.Text:=TotalHoras;
       //Busca a quantidade do form produtividade
      EdtQtde.Text:=FormatFloat('###,##0.00',Dm.QGeraRelFinalQTD.Value);
      //Busca a produtividade do form produtividade

      Prod:=Dm.QGeraRelFinalQTD.Value/TotalHorasInv;
      EdtProdutividade.Text:=FormatFloat('###,##0.00',Prod);
    except
      EdtHoras.Text:='0';
      EdtQtde.Text:='0';
      EdtProdutividade.Text:='0';
      Prod:=0;
      end;


      //CASO CLIENTE N�O TENHA ENDERE�O, O TAMANHO M�XIMO DA SE��O SER� 4
      DM.qrySQL.Close;
      DM.qrySQL.SQL.Clear;
      DM.qrySQL.SQL.Add('SELECT * FROM LAYOUT_INPUT');
      DM.qrySQL.SQL.Add('WHERE CLIENTE = :CLIENTE');
      Dm.qrySQL.ParamByName('CLIENTE').Value := DBEdit2.Text;
      DM.qrySQL.Open;


     if DM.qrySQL.FieldByName('ENDERECO').Value = 'T' then
      begin
         Edit1.MaxLength := 25;
         Edit2.MaxLength := 25;
      end
     else
      begin
         Edit1.MaxLength := 4;
         Edit2.MaxLength := 4;
      end;

      //VERIFICA SE CLIENTE TEM ENDERE�O E LOTE AO INV�S DE SE��O E AREA
      DM.qrySQL.Close;
      DM.qrySQL.SQL.Clear;
      DM.qrySQL.SQL.Add('SELECT * FROM LAYOUT_INPUT');
      DM.qrySQL.SQL.Add('WHERE CLIENTE = :CLIENTE');
      Dm.qrySQL.ParamByName('CLIENTE').Value := DBEdit2.Text;
      DM.qrySQL.Open;


      if DM.qrySQL.FieldByName('ENDERECO').Value = 'T' then
      begin
        FrmInventario.DBGrid3.Columns[1].Title.Caption := 'Endere�o';
        FrmInventario.DBGrid5.Columns[3].Title.Caption := 'Endere�o';
        FrmInventario.DBGrid5.Columns[4].Title.Caption := 'Lote';
        FrmInventario.TabSheet5.Caption := '&Endere�o';
        FrmInventario.DBGrid1.Columns[0].Title.Caption := 'Endere�o Inicial';
        FrmInventario.DBGrid1.Columns[1].Title.Caption := 'Endere�o Final';
        FrmInventario.DBGrid1.Columns[2].Title.Caption := 'Descri��o do Endere�o';
        FrmInventario.Label1.Caption := 'End. Inicial';
        FrmInventario.Label2.Caption := 'End. Final';
        FrmInventario.Label3.Caption := 'Descri��o do Endere�o';
        FrmInventario.DBChart1.Title.Text.Clear;
        FrmInventario.DBChart1.Title.Text.Add('Total de Endere�os Contados');

        Dm.QGrafico.Close;
        DM.QGrafico.SQL.Clear;
        DM.QGrafico.SQL.Add('select SEC_ABERTA,CASE SEC_ABERTA ');
        DM.QGrafico.SQL.Add('WHEN "T" THEN "ENDERE�OS ABERTOS" ');
        DM.QGrafico.SQL.Add('WHEN "F" THEN "ENDERE�OS CONTADOS" ');
        DM.QGrafico.SQL.Add('END AS DESCRI, ');
        DM.QGrafico.SQL.Add('CASE SEC_ABERTA ');
        DM.QGrafico.SQL.Add('WHEN "T" THEN COUNT(*) ');
        Dm.QGrafico.SQL.Add('WHEN "F" THEN COUNT(*) ');
        Dm.QGrafico.SQL.Add('END AS QUANTIDADE ');
        Dm.QGrafico.SQL.Add('FROM SECAO ');
        Dm.QGrafico.SQL.Add('WHERE (SEC_STATUS = '+ QuotedStr('.') +') ');
        Dm.QGrafico.SQL.Add('GROUP BY SEC_ABERTA ');
        DM.QGrafico.Open;
      end;
           

 //Preenche StatusBar
  StatusBar1.Panels[0].Text := 'Usu�rio Conectado = ' + DM.QUsuarioUSU_NOME.Value;
  StatusBar1.Panels[1].Text := '[' +Datetostr(Date) + ']';

 //Abrir Invent�rios

 //Verificar se invent�rio esta finalizado e mudar menu
 if Dm.IBInventarioINV_STATUS.Value = 'FINALIZADO' then
   FrmInventario.MainMenu1.Items[0].Items[0].Caption:= 'Reabrir Invent�rio';


 //Abrir no 1� Page Control
 FrmInventario.PageControl1.ActivePageIndex := 0;

 //Altera o title do grid5 (contagens) para as colunas EAN e o C�digo Interno
 //quando o cliente for a Coopervision, pois eles trabalham apenas com LOTE E SKU

 If Dm.IBInventarioINV_LOJA.Value = 'CPV' then
  Begin
    FrmInventario.DBGrid5.Columns[5].Title.Caption := 'SKU'; //no lugar do C�digo Interno
    FrmInventario.DBGrid5.Columns[6].Title.Caption := 'LOTE'; //no lugar do EAN
  end;


 //Caso invent�rio esteja finalizado ativa o menu para gera��o do arquivo final
   if Dm.IBInventarioINV_STATUS.Value = 'FINALIZADO' then
    MainMenu1.Items[1].Items[1].Enabled := True;

 //Habilita o menu de Ativar e dasativar importa��o das transmiss�es para o adminitrador do sistema
  If NivelUsu = 1 then
    MainMenu1.Items[0].Items[1].Enabled := True;

 //Caso o invent�rio estaja finalizado desabilita alguns Panels e alguns menus
  If Dm.IBInventarioINV_STATUS.Value = 'FINALIZADO' then
   Begin
    MainMenu1.Items[1].Items[1].Enabled := True;
    MainMenu1.Items[1].Items[2].Enabled := True;
    MainMenu1.Items[1].Items[3].Enabled := True;
    MainMenu1.Items[1].Items[4].Enabled := True;
    Panel4.Enabled := False;
    Panel15.Enabled := False;
    Panel9.Enabled := False;
    Panel7.Enabled := False;
    Panel8.Enabled := False;
    Panel11.Enabled := False;
    Panel12.Enabled := False;
    Panel13.Enabled := False;
   End;
end;

procedure TFrmInventario.GerarDivergncia1Click(Sender: TObject);
begin
FrmDivergencia := TFrmDivergencia.Create(FrmDivergencia);
try
  FrmDivergencia.ShowModal;
finally
  FrmDivergencia.Release;
  FrmDivergencia := nil;
end;
end;

procedure TFrmInventario.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
  DM.qrySQL.Close;
  DM.qrySQL.SQL.Clear;
  DM.qrySQL.SQL.Add('SELECT * FROM LAYOUT_INPUT WHERE CLIENTE = :CLIENTE');
  DM.qrySQL.ParamByName('CLIENTE').AsString := Dm.IBInventarioINV_LOJA.Value;
  DM.qrySQL.Open;

  if DM.qrySQL.FieldByName('ENDERECO').AsString <> 'T' then
    begin
      if not (key in ['0'..'9',',',#8]) then
         begin
          key:=#0;
          beep;
         end;
    end;
end;


procedure TFrmInventario.RelatriodeProdutosNoCadastrados1Click(
  Sender: TObject);
begin
  Dm.QProdNaoCadastrado.Close;
  Dm.QProdNaoCadastrado.Open;
  if Dm.QProdNaoCadastrado.RecordCount > 0 then
      begin
        DM.RvPrjWis.Close;
        Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\NaoCadastrados.rav';
        DM.RvPrjWis.Open;
        Dm.RvSysWis.DefaultDest := rdPreview;
        Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
        Dm.RvPrjWis.ExecuteReport('NaoCadastrados');
         // Salvando relat�rio em PDF automaticamente
          Try
            Dm.RvSysWis.DefaultDest:= rdFile;
            Dm.RvSysWis.DoNativeOutput:= false;
            Dm.RvSysWis.RenderObject:= rvPdf;
            Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Relatorio de Produtos Nao Cadastrados_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
            Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
            Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\NaoCadastrados.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
            Dm.RvPrjWis.Engine:= Dm.RvSysWis;
            Dm.RvPrjWis.Execute;
          Except
            ShowMessage('Erro ao gerar o relat�rio em PDF');
          end;
        // Salvando relat�rio em TXT automaticamente

          Try
            Dm.RvSysWis.DefaultDest:= rdFile;
            Dm.RvSysWis.DoNativeOutput:= false;
            Dm.RvSysWis.RenderObject:= rvTxt;
            Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Relatorio de Produtos Nao Cadastrados_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.txt'; //caminho onde vai gerar o arquivo pdf
            Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
            Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\NaoCadastrados.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
            Dm.RvPrjWis.Engine:= Dm.RvSysWis;
            Dm.RvPrjWis.Execute;
          except
            ShowMessage('Erro ao gerar o relat�rio em TXT');
          end;
      end
   else
           Application.MessageBox('Todos os produtos est�o cadastrados','Mensagem do sistema', MB_ICONEXCLAMATION+ MB_OK);

 End;


procedure TFrmInventario.PageControl1Change(Sender: TObject);
  var prod: real;
begin

  if FrmInventario.TabSheet5.Visible then
    begin
     Label20.Visible := True;
    end
  else
    begin
      Label20.Visible := False;
    end;

 Dm.IBInventario.Open;
If (Dm.IBInventarioINV_LOJA.Value = 'PDA') Or (Dm.IBInventarioINV_LOJA.Value = 'COV') then
      begin
        Dm.QGeraRelFinal.close;
        Dm.QGeraRelFinal.SQL.Clear;
        Dm.QGeraRelFinal.Sql.Add('select sum(preco) as preco, sum(QTD) as QTD from CONTAGENS_CONSO D left JOIN CADASTRO_conso C ON (C.CAD_EAN = D.CON_EAN)');
        Dm.QGeraRelFinal.Open;
      end
     else
      begin
        Dm.QGeraRelFinal.close;
        Dm.QGeraRelFinal.SQL.Clear;
        Dm.QGeraRelFinal.Sql.Add('SELECT SUM(QTD) AS QTD, SUM(PRECO) AS PRECO FROM CONTAGENS_CONSO');
        Dm.QGeraRelFinal.Open;
      end;

//Busca o total de pessoas no invent�rio que j� fizeram ao menos uma transmissao
Dm.IBHorarios.First;
Dm.IBHorarios.Last;
EdtPessoas.Text:=IntToStr(Dm.IBHorarios.RecordCount);

 


    try
      //Busca o total de horas do form produtividade

      //EdtHoras.Text:=FormatFloat('##,###0.00',TotalHoras);
      EdtHoras.Text:=TotalHoras;
       //Busca a quantidade do form produtividade
      EdtQtde.Text:=FormatFloat('###,##0.00',Dm.QGeraRelFinalQTD.Value);
      //Busca a produtividade do form produtividade

      Prod:=Dm.QGeraRelFinalQTD.Value/TotalHorasInv;
      EdtProdutividade.Text:=FormatFloat('###,##0.00',Prod);
    except
    EdtHoras.Text:='0';
    EdtQtde.Text:='0';
    EdtProdutividade.Text:='0';
    Prod:=0;
end;
end;

procedure TFrmInventario.RelatriodeCdigosEANePLU1Click(Sender: TObject);
var Ean, CodInt :string;
begin
EAN:=InputBox('SGD','Digite o C�digo EAN','');
try
  Dm.QRelEAN.Close;
  Dm.QRelEAN2.Close;
  Dm.QRelEAn.ParamByName('EAN').Value:=EAn;
  Dm.QRelEAn2.ParamByName('EAN').Value:=EAn;
// Dm.QRelEAn.ParamByName('COD_INT').Value:=Ean;
  Dm.QRelEAN.Open;
  Dm.QRelEAN2.Open;
  If Dm.QRelEAN.IsEmpty or Dm.QRelEAN2.IsEmpty then ShowMessage('C�digo EAN n�o encontrado, tente novamente')
  else
DM.RvPrjWis.Close;
Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Rel_Ean.rav';
DM.RvPrjWis.Open;
Dm.RvSysWis.DefaultDest := rdPreview;
Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
Dm.RvPrjWis.ExecuteReport('Rel_Ean');
Finally
  Dm.QRelEAN.Close;
  Dm.QRelEAN2.Close;
end;
end;
procedure TFrmInventario.DBGrid6DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
{Procedure que altera a cor da linha selecionada no DBGrid}
{Altera a cor do fundo da linha selecionada no DBGrid}
if gdSelected in State then
with (Sender as TDBGrid).Canvas do
begin
  Brush.Color:=clBlue; {Cor de fundo}
  FillRect(Rect);
end;

TDbGrid(Sender).DefaultDrawDataCell(Rect, TDbGrid(Sender).columns[datacol].field, State);

end;

procedure TFrmInventario.DBGrid7DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
{Procedure que altera a cor da linha selecionada no DBGrid}
{Altera a cor do fundo da linha selecionada no DBGrid}
if gdSelected in State then
with (Sender as TDBGrid).Canvas do
begin
  Brush.Color:=clBlue; {Cor de fundo}
  FillRect(Rect);
end;

TDbGrid(Sender).DefaultDrawDataCell(Rect, TDbGrid(Sender).columns[datacol].field, State);

end;

procedure TFrmInventario.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
{Procedure que altera a cor da linha selecionada no DBGrid}
{Altera a cor do fundo da linha selecionada no DBGrid}
if gdSelected in State then
with (Sender as TDBGrid).Canvas do
begin
  Brush.Color:=clblue; {Cor de fundo}
  FillRect(Rect);
end;

TDbGrid(Sender).DefaultDrawDataCell(Rect, TDbGrid(Sender).columns[datacol].field, State);

end;

procedure TFrmInventario.DBGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
{Procedure que altera a cor da linha selecionada no DBGrid}
{Altera a cor do fundo da linha selecionada no DBGrid}
if gdSelected in State then
with (Sender as TDBGrid).Canvas do
begin
  Brush.Color:=clBlue; {Cor de fundo}
  FillRect(Rect);
end;

TDbGrid(Sender).DefaultDrawDataCell(Rect, TDbGrid(Sender).columns[datacol].field, State);

end;

procedure TFrmInventario.DBGrid3DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
{Procedure que altera a cor da linha selecionada no DBGrid
Altera a cor do fundo da linha selecionada no DBGrid
if gdSelected in State then
with (Sender as TDBGrid).Canvas do
begin
  Brush.Color:=clBlue; {Cor de fundo
  FillRect(Rect);
end;

TDbGrid(Sender).DefaultDrawDataCell(Rect, TDbGrid(Sender).columns[datacol].field, State);
                                                    }
end;

procedure TFrmInventario.DBGrid4DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
{Procedure que altera a cor da linha selecionada no DBGrid}
{Altera a cor do fundo da linha selecionada no DBGrid}
if gdSelected in State then
with (Sender as TDBGrid).Canvas do
begin
  Brush.Color:=clBlue; {Cor de fundo}
  FillRect(Rect);
end;

TDbGrid(Sender).DefaultDrawDataCell(Rect, TDbGrid(Sender).columns[datacol].field, State);

end;

procedure TFrmInventario.DBGrid5DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
{Procedure que altera a cor da linha selecionada no DBGrid}
{Altera a cor do fundo da linha selecionada no DBGrid}
if gdSelected in State then
with (Sender as TDBGrid).Canvas do
begin
  Brush.Color:=clBlue; {Cor de fundo}
  FillRect(Rect);
end;

TDbGrid(Sender).DefaultDrawDataCell(Rect, TDbGrid(Sender).columns[datacol].field, State);

end;

procedure TFrmInventario.ransmitirBancodeDados1Click(Sender: TObject);
begin
FrmUpdate := TFrmUpdate.Create(FrmInventario);
 try
   FrmUpdate.ShowModal;
 finally
   FrmUpdate.Release;
   FrmUpdate := nil;
 end;
end;

procedure TFrmInventario.Timer2Timer(Sender: TObject);
begin
If (Dm.IBInventarioINV_LOJA.Value = 'PDA') or (Dm.IBInventarioINV_LOJA.Value = 'COV') or (Dm.IBInventarioINV_LOJA.Value = 'ASA') then
  begin
        Dm.QGeraRelFinal.Close;
        Dm.QGeraRelFinal.Sql.Clear;
        Dm.QGeraRelFinal.Sql.Add('SELECT round(SUM(QTD)) AS QTD, SUM(PRECO) AS PRECO FROM CONTAGENS_CONSO');
        Dm.QGeraRelFinal.Open;
  end
Else
begin
       Dm.QGeraRelFinal.Close;
        Dm.QGeraRelFinal.Sql.Clear;
        Dm.QGeraRelFinal.Sql.Add('select sum(preco) as preco, sum(QTD) as QTD from CONTAGENS_CONSO_GERAL');
        Dm.QGeraRelFinal.Open;

 end;

end;

procedure TFrmInventario.sBitBtn1Click(Sender: TObject);
begin
AdicionarConferente;
end;

procedure TFrmInventario.sBitBtn2Click(Sender: TObject);
begin

FrmProdutividade := TFrmProdutividade.Create(FrmProdutividade);
try
  FrmProdutividade.ShowModal;
finally
  FrmProdutividade.Release;
  FrmProdutividade := nil;
end;
end;

procedure TFrmInventario.RelatriodeSetorConsolidado1Click(Sender: TObject);
begin
Try
Dm.QRelCLBLoja.Close;
Dm.QRelCLBLoja.Open;
Dm.QRelCLBDeposito.Close;
Dm.QRelCLBDeposito.Open;
Dm.QRelCLBVitrine.Close;
Dm.QRelCLBVitrine.Open;
Dm.QRelCLBTotal.Close;
Dm.QRelCLBTotal.Open;
if Dm.QRelCLBTotalSum.Value > 0 then
      begin
      Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Rel_CLBSetorizado.rav';
      Dm.RvSysWis.DefaultDest := rdPreview;
      Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
      Dm.RvPrjWis.ExecuteReport('Rel_CLBSetorizado');
      // Salvando relat�rio em PDF automaticamente
          Try
            Dm.RvSysWis.DefaultDest:= rdFile;
            Dm.RvSysWis.DoNativeOutput:= false;
            Dm.RvSysWis.RenderObject:= rvPdf;
            Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Relatorio CLB Setorizado_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
            Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
            Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Rel_CLBSetorizado.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
            Dm.RvPrjWis.Engine:= Dm.RvSysWis;
            Dm.RvPrjWis.Execute;
          Except
            ShowMessage('Erro ao gerar o relat�rio em PDF');
          end;
        // Salvando relat�rio em TXT automaticamente

          Try
            Dm.RvSysWis.DefaultDest:= rdFile;
            Dm.RvSysWis.DoNativeOutput:= false;
            Dm.RvSysWis.RenderObject:= rvTxt;
            Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Relatorio CLB Setorizado_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.txt'; //caminho onde vai gerar o arquivo pdf
            Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
            Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Rel_CLBSetorizado.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
            Dm.RvPrjWis.Engine:= Dm.RvSysWis;
            Dm.RvPrjWis.Execute;
          except
            ShowMessage('Erro ao gerar o relat�rio em TXT');
          end;
      end
   else
           Application.MessageBox('N�o existem contagens para gerar o relat�rio','Mensagem do sistema', MB_ICONEXCLAMATION+ MB_OK);
 Finally
   Dm.QRelCLBTotal.Close;
 End;
end;

procedure TFrmInventario.RelatriodeAuditoriaConsolidado1Click(
  Sender: TObject);
  var StrSql: string;
  begin
End;
procedure TFrmInventario.DBGrid5TitleClick(Column: TColumn);
var
   campo:string;
begin

  // Grid_Ordena_e_PintaTitulo(DBGrid5, Column, DM.CdsContagens);

   If click = true then
    begin
      Click := false;
      Campo := Column.FieldName; // CAMPO RECEBE O NOME DA COLUNA CLICADA,
      Application.ProcessMessages; // para considerar algo que aconte�a no dbgrid durante a entrada nesta procedure
      Dm.IBContagens.SelectSql.Clear; // LIMPA A QUERY
      Dm.IBContagens.SelectSql.Add('select CD.CAD_CODINT, c.CON_EAN, c.CON_ID, c.ARE_AREA, c.CON_DATA, c.CON_HORA, c.CON_QUANTIDADE, c.CON_QUANTIDADEORIGINAL, c.HOR_MATRICULA, c.SEC_SECAO, c.TRA_ID,  c.CON_STATUS ');
      Dm.IBContagens.SelectSql.Add('FROM CONTAGENS C LEFT JOIN CADASTRO_CONSO CD ON (C.CON_EAN = CD.CAD_EAN) ');
      Dm.IBContagens.SelectSql.Add('ORDER BY '+campo); // ESCREVE O SELECT COM O ORDER BY
      Dm.IBContagens.Open; // ABRE A QUERY COM A ORDEM ESCOLHIDA.
      Column.Font.Color:= clBlue; // COLOCAR A COLUNA NA COR DESEJADA
    end
else
   begin
      click := true;
      Campo := Column.FieldName; // CAMPO RECEBE O NOME DA COLUNA CLICADA,
      Application.ProcessMessages; // para considerar algo que aconte�a no dbgrid durante a entrada nesta procedure
      Dm.IBContagens.SelectSql.Clear; // LIMPA A QUERY
      Dm.IBContagens.SelectSql.Add('select CD.CAD_CODINT, c.CON_EAN, c.CON_ID, c.ARE_AREA, c.CON_DATA, c.CON_HORA, c.CON_QUANTIDADE, c.CON_QUANTIDADEORIGINAL, c.HOR_MATRICULA, c.SEC_SECAO, c.TRA_ID,  c.CON_STATUS ');
      Dm.IBContagens.SelectSql.Add('FROM CONTAGENS C LEFT JOIN CADASTRO_CONSO CD ON (C.CON_EAN = CD.CAD_EAN) ');
      Dm.IBContagens.SelectSql.Add('ORDER BY '+campo+ ' DESC'); // ESCREVE O SELECT COM O ORDER BY
      Dm.IBContagens.Open; // ABRE A QUERY COM A ORDEM ESCOLHIDA.
      Column.Font.Color:= clBlue; // COLOCAR A COLUNA NA COR DESEJADA
   end;

end;


procedure TFrmInventario.DBGrid2TitleClick(Column: TColumn);
var campo :string;
begin
Try
  If click = true then
    begin
      Click := false;
      Campo := Column.FieldName; // CAMPO RECEBE O NOME DA COLUNA CLICADA,
      Application.ProcessMessages; // para considerar algo que aconte�a no dbgrid durante a entrada nesta procedure
      Dm.IBTransmissao.SelectSql.Clear; // LIMPA A QUERY
      Dm.IBTransmissao.SelectSql.Add('select * from transmissao ');
      Dm.IBTransmissao.SelectSql.Add('ORDER BY '+campo); // ESCREVE O SELECT COM O ORDER BY
      Dm.IBTransmissao.Open; // ABRE A QUERY COM A ORDEM ESCOLHIDA.
      Column.Font.Color:= clBlue; // COLOCAR A COLUNA NA COR DESEJADA
    end
else
   begin
      click := true;
      Campo := Column.FieldName; // CAMPO RECEBE O NOME DA COLUNA CLICADA,
      Application.ProcessMessages; // para considerar algo que aconte�a no dbgrid durante a entrada nesta procedure
      Dm.IBTransmissao.SelectSql.Clear; // LIMPA A QUERY
      Dm.IBTransmissao.SelectSql.Add('select * from transmissao ');
      Dm.IBTransmissao.SelectSql.Add('ORDER BY '+campo+' DESC'); // ESCREVE O SELECT COM O ORDER BY
      Dm.IBTransmissao.Open; // ABRE A QUERY COM A ORDEM ESCOLHIDA.
      Column.Font.Color:= clBlue; // COLOCAR A COLUNA NA COR DESEJADA
   end;
except
end;
end;


procedure TFrmInventario.DBGrid3TitleClick(Column: TColumn);
var
  indice: string;
  existe: boolean;
  clientdataset_idx: tclientdataset;
begin
Try
 // Grid_Ordena_e_PintaTitulo(DBGrid3, Column, DM.IBSecao);
  clientdataset_idx := TClientDataset(Column.Grid.DataSource.DataSet);

  //  Column.FieldName

  if clientdataset_idx.IndexFieldNames = Column.FieldName then
  begin
    indice := AnsiUpperCase(Column.FieldName);
   try
      clientdataset_idx.IndexDefs.Find(indice);
      existe := true;
    except
      existe := false;
    end;

    if not existe then
      with clientdataset_idx.IndexDefs.AddIndexDef do begin
        Name := Indice;
        Fields := Column.FieldName;
        Options := [ixDescending];
      end;
    clientdataset_idx.IndexName := indice;
     Column.Font.Color:=clBlue; // COLOCAR A COLUNA NA COR DESEJADA
  end
  else
     clientdataset_idx.IndexFieldNames := Column.FieldName;
      Column.Font.Color:=clBlue; // COLOCAR A COLUNA NA COR DESEJADA }

Except
End;
End;

procedure TFrmInventario.CheckBox1Click(Sender: TObject);
begin
If not checkbox1.Checked then
  begin
    Dm.IBContagens.Close;
    Dm.IBContagens.SelectSql.Clear;
    Dm.IBContagens.SelectSQL.Add('select CD.CAD_CODINT, c.CON_EAN, c.CON_ID, c.ARE_AREA, c.CON_DATA, c.CON_HORA, c.CON_QUANTIDADE, c.CON_QUANTIDADEORIGINAL, c.HOR_MATRICULA, c.SEC_SECAO, c.TRA_ID,  c.CON_STATUS ');
    Dm.IBContagens.SelectSQL.Add('FROM CONTAGENS C LEFT JOIN CADASTRO_CONSO CD ON (C.CON_EAN = CD.CAD_EAN)');
    Dm.IBContagens.SelectSQL.Add('ORDER BY SEC_SECAO, CON_ID ');
    Dm.IBContagens.Close;
    //Dm.CdsContagens.Close;
    Dm.IBContagens.Open;
    //Dm.CdsContagens.Open;
  end;
If checkbox1.Checked then
  begin
    Dm.IBContagens.Close;
    Dm.IBContagens.SelectSql.Clear;
    Dm.IBContagens.SelectSQL.Add('select cd.cad_codint, c.CON_EAN, c.CON_ID, c.ARE_AREA, c.CON_DATA, c.CON_HORA, c.CON_QUANTIDADE, c.CON_QUANTIDADEORIGINAL, c.HOR_MATRICULA, c.SEC_SECAO, c.TRA_ID,  c.CON_STATUS ');
    Dm.IBContagens.SelectSQL.Add('FROM CONTAGENS C LEFT JOIN CADASTRO_CONSO CD ON (C.CON_EAN = CD.CAD_EAN) where (SEC_SECAO = :SEC_SECAO) and (TRA_ID = :TRA_ID)');
    Dm.IBContagens.SelectSQL.Add('ORDER BY SEC_SECAO, CON_ID');
    Dm.IBContagens.Close;
    //Dm.CdsContagens.Close;
    Dm.IBContagens.Open;
    //Dm.CdsContagens.Open;
  end;
end;

procedure TFrmInventario.Geratxtconfernciaseo1Click(Sender: TObject);
begin
GeraTxtConfSecao;
end;

end.

















