object FrmSecoes: TFrmSecoes
  Left = 464
  Top = 129
  Width = 421
  Height = 479
  Caption = 'SGD - Tratamento de Se'#231#245'es'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = -1
    Top = -24
    Width = 410
    Height = 50
    Align = alCustom
    Color = clNavy
    TabOrder = 0
    object Label1: TLabel
      Left = 12
      Top = 24
      Width = 200
      Height = 23
      Caption = 'Finaliza'#231#227'o do Invent'#225'rio'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Calibri'
      Font.Style = [fsBold, fsItalic]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 9
      Top = 24
      Width = 200
      Height = 23
      Caption = 'Finaliza'#231#227'o do Invent'#225'rio'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -19
      Font.Name = 'Calibri'
      Font.Style = [fsBold, fsItalic]
      ParentFont = False
      Transparent = True
    end
  end
  object Panel3: TPanel
    Left = 3
    Top = 28
    Width = 402
    Height = 149
    BevelOuter = bvSpace
    TabOrder = 1
    object Label2: TLabel
      Left = 8
      Top = 67
      Width = 41
      Height = 13
      Caption = 'Status:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 8
      Top = 11
      Width = 25
      Height = 13
      Caption = 'Loja'
      FocusControl = DBEdit2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 88
      Top = 11
      Width = 58
      Height = 13
      Caption = 'Num. Loja'
      FocusControl = DBEdit3
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBText1: TDBText
      Left = 59
      Top = 63
      Width = 126
      Height = 16
      DataField = 'INV_STATUS'
      DataSource = DM.DsInventario
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBEdit2: TDBEdit
      Left = 8
      Top = 27
      Width = 57
      Height = 21
      CharCase = ecUpperCase
      Color = clSkyBlue
      DataField = 'INV_LOJA'
      DataSource = DM.DsInventario
      Enabled = False
      TabOrder = 0
    end
    object DBEdit3: TDBEdit
      Left = 88
      Top = 27
      Width = 56
      Height = 21
      CharCase = ecUpperCase
      Color = clSkyBlue
      DataField = 'INV_NUMLOJA'
      DataSource = DM.DsInventario
      Enabled = False
      TabOrder = 1
    end
    object sBitBtn1: TsBitBtn
      Left = 8
      Top = 112
      Width = 169
      Height = 25
      Cursor = crHandPoint
      Caption = 'Finalizar Invent'#225'rio'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      SkinData.SkinSection = 'BUTTON'
      Images = FrmInventario.ImageList1
    end
    object sBitBtn2: TsBitBtn
      Left = 224
      Top = 112
      Width = 169
      Height = 25
      Cursor = crHandPoint
      Caption = 'Reabrir Invent'#225'rio'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      SkinData.SkinSection = 'BUTTON'
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 422
    Width = 405
    Height = 19
    Panels = <>
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 405
    Height = 422
    Align = alClient
    Color = clNavy
    TabOrder = 3
    object Panel4: TPanel
      Left = -1
      Top = -24
      Width = 410
      Height = 50
      Align = alCustom
      Color = clNavy
      TabOrder = 0
      object Label5: TLabel
        Left = 12
        Top = 24
        Width = 52
        Height = 23
        Caption = 'Se'#231#245'es'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'Calibri'
        Font.Style = [fsBold, fsItalic]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 9
        Top = 24
        Width = 52
        Height = 23
        Caption = 'Se'#231#245'es'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -19
        Font.Name = 'Calibri'
        Font.Style = [fsBold, fsItalic]
        ParentFont = False
        Transparent = True
      end
    end
    object Panel6: TPanel
      Left = 4
      Top = 32
      Width = 400
      Height = 393
      TabOrder = 1
      object DBGrid1: TDBGrid
        Left = 0
        Top = 120
        Width = 407
        Height = 249
        Align = alCustom
        Color = clWhite
        DataSource = DM.DsSecao
        FixedColor = cl3DLight
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clMaroon
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDrawColumnCell = DBGrid1DrawColumnCell
        OnKeyDown = DBGrid1KeyDown
        Columns = <
          item
            Expanded = False
            FieldName = 'SEC_SECAO'
            Title.Caption = 'Secao'
            Width = 53
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'STATUS'
            ReadOnly = True
            Title.Caption = 'Status'
            Width = 58
            Visible = True
          end>
      end
      object sBitBtn4: TsBitBtn
        Left = 224
        Top = 368
        Width = 169
        Height = 25
        Cursor = crHandPoint
        Caption = 'Fechar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        OnClick = sBitBtn4Click
        SkinData.SkinSection = 'BUTTON'
      end
    end
    object Panel5: TPanel
      Left = 4
      Top = 28
      Width = 402
      Height = 125
      BevelOuter = bvSpace
      TabOrder = 2
      object RadioGroup1: TRadioGroup
        Left = 8
        Top = 8
        Width = 385
        Height = 105
        Caption = 'Filtro'
        ItemIndex = 0
        Items.Strings = (
          'Se'#231#245'es ABERTAS'
          'Se'#231#245'es INESPERADAS'
          'Se'#231#245'es DUPLICADAS')
        TabOrder = 0
        OnClick = RadioGroup1Click
      end
    end
  end
end
