unit UFrmLayoutExiste;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, StdCtrls, sEdit, DBCtrls, Mask, sButton,
  Buttons, sBitBtn, Menus, Data.DB, sLabel;

type
  TFrmLayoutExiste = class(TForm)
    GroupBox1: TGroupBox;
    EdtCliente: TsEdit;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    GridInPut: TDBGrid;
    lblsizeloja: TLabel;
    lblordemloja: TLabel;
    chkloja1: TDBEdit;
    chkcodint1: TDBEdit;
    chkean1: TDBEdit;
    chkdata1: TDBEdit;
    chkqtde1: TDBEdit;
    chkpreco1: TDBEdit;
    chkseparador1: TDBEdit;
    chkloja2: TDBEdit;
    chkcodint2: TDBEdit;
    chkean2: TDBEdit;
    chkdata2: TDBEdit;
    chkqtde2: TDBEdit;
    chkpreco2: TDBEdit;
    chkseparador2: TDBEdit;
    chkloja: TDBCheckBox;
    chkseparador3: TDBEdit;
    Label1: TLabel;
    GroupBox4: TGroupBox;
    sButton1: TsButton;
    sButton2: TsButton;
    sBitBtn1: TsBitBtn;
    chkcodint: TDBCheckBox;
    chkean: TDBCheckBox;
    chkdata: TDBCheckBox;
    chkqtde: TDBCheckBox;
    chkpreco: TDBCheckBox;
    chkseparador: TDBCheckBox;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    DBCheckBox1: TDBCheckBox;
    sBitBtn2: TsBitBtn;
    Label6: TLabel;
    DBComboBox1: TDBComboBox;
    Label11: TLabel;
    CmbZeroEAN: TDBComboBox;
    CmbZeroCODINT: TDBComboBox;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label17: TLabel;
    Label7: TLabel;
    EdtNomeArq: TDBEdit;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    DBComboBox2: TDBComboBox;
    DBComboBox3: TDBComboBox;
    PopupMenu1: TPopupMenu;
    Incluirnovoformato1: TMenuItem;
    chkseparadorfixo: TDBCheckBox;
    chkSetor: TDBCheckBox;
    chkMarcador: TDBCheckBox;
    chkLote: TDBCheckBox;
    chkEndereco: TDBCheckBox;
    chkseparadorfixo1: TDBEdit;
    chksetor1: TDBEdit;
    chkMarcador1: TDBEdit;
    chkLote1: TDBEdit;
    chkEndereco1: TDBEdit;
    chkseparadorfixo2: TDBEdit;
    chksetor2: TDBEdit;
    chkMarcador2: TDBEdit;
    chkLote2: TDBEdit;
    chkEndereco2: TDBEdit;
    Label12: TLabel;
    Label16: TLabel;
    Label18: TLabel;
    CmbZeroSetor: TDBComboBox;
    CmbLoteZero: TDBComboBox;
    CmbEnderecoZero: TDBComboBox;
    DBCheckBox2: TDBCheckBox;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label45: TLabel;
    DBComboBox4: TDBComboBox;
    Label19: TLabel;
    DBComboBox5: TDBComboBox;
    Label20: TLabel;
    DBComboBox6: TDBComboBox;
    sLabelFX1: TsLabelFX;
    procedure sButton1Click(Sender: TObject);
    procedure sButton2Click(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure GroupBox3Click(Sender: TObject);
    procedure sBitBtn2Click(Sender: TObject);
    procedure GridInPutKeyPress(Sender: TObject; var Key: Char);
    procedure DBComboBox1Click(Sender: TObject);
    procedure CmbNumLojaClick(Sender: TObject);
    procedure CmbZeroCODINTClick(Sender: TObject);
    procedure CmbZeroEANClick(Sender: TObject);
    procedure Incluirnovoformato1Click(Sender: TObject);
    procedure DBComboBox4Click(Sender: TObject);
    procedure DBComboBox5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmLayoutExiste: TFrmLayoutExiste;

implementation
uses Udm, UFuncProc;

{$R *.dfm}

procedure TFrmLayoutExiste.sButton1Click(Sender: TObject);
var I: Integer;
begin

for I := 0 to ComponentCount - 1 do
begin
if (Components[I] is TDBComboBox) then
TDBComboBox(Components[I]).Enabled := true;
end;

//lopping que habilitar todos os DBEdits do form
for I := 0 to ComponentCount - 1 do
begin
if (Components[I] is TDBEdit) then
TDBEdit(Components[I]).Enabled := True;
end;

// Lopping que habilita todos os DBCheckBox do form
for I := 0 to ComponentCount - 1 do
begin
if (Components[I] is TDBCheckBox) then
TDBCheckBox(Components[I]).Enabled := True;
end;

// Lopping que habilita todos os Labels do form
for I := 0 to ComponentCount - 1 do
begin
if (Components[I] is TLabel) then
TLabel(Components[I]).Enabled := True;
end;
// Lopping que habilita todos os groups
for I := 0 to ComponentCount - 1 do
begin
if (Components[I] is TGroupBox) then
TGroupBox(Components[I]).Enabled := True;
end;


// Ativa o DBrig
GridInput.Enabled:=true;


Dm.IBLayoutInput.Open;
Dm.IBLayoutInput.Edit;
Dm.IBLayoutOutput.Open;
Dm.IBLayoutOutput.Edit;


End;
procedure TFrmLayoutExiste.sButton2Click(Sender: TObject);
var i: integer;
begin
try
  Application.MessageBox('Altera��es salvas','Mensagem do Sistema', MB_ICONINFORMATION + MB_OK);
  Dm.IBLayoutInput.Post;
  Dm.Transacao.CommitRetaining;
  Dm.IBLayoutInput.Close;



  Dm.IBLayoutOutput.Post;
  Dm.Transacao.CommitRetaining;
  DM.IBLayoutOutput.Close;
except
Application.MessageBox('Erro ao salvar as altera��es','Mensagem do Sistema', MB_ICONERROR + MB_OK);
end;
//lopping que habilitar todos os DBEdits do form
for I := 0 to ComponentCount - 1 do
begin
if (Components[I] is TDBEdit) then
TDBEdit(Components[I]).Enabled := false;
end;

for I := 0 to ComponentCount - 1 do
begin
if (Components[I] is TDBComboBox) then
TDBComboBox(Components[I]).Enabled := false;
end;
// Lopping que habilita todos os DBCheckBox do form
for I := 0 to ComponentCount - 1 do
begin
if (Components[I] is TDBCheckBox) then
TDBCheckBox(Components[I]).Enabled := false;
end;
// Lopping que habilita todos os Labels do form
for I := 0 to ComponentCount - 1 do
begin
if (Components[I] is TLabel) then
TLabel(Components[I]).Enabled := false;
end;


// Ativa o DBrig
GridInput.enabled:=false;
       
end;

procedure TFrmLayoutExiste.sBitBtn1Click(Sender: TObject);
var i: integer;
begin
    Dm.IBLayoutOutPut.Close;
    Dm.IBLayoutOutPut.SelectSQL.Clear;
    Dm.IBLayoutOutPut.SelectSQL.Add('SELECT * FROM LAYOUT_OUTPUT WHERE CLIENTE = :CLIENTE');
    Dm.IBLayoutOutPut.ParamByName('CLIENTE').Value := EdtCliente.text;
    Dm.IBLayoutOutPut.Open;
//    If Dm.IBLayoutOutPut.IsEmpty then
//    Application.MessageBox('Layout de sa�da n�o cadastrado para este cliente','Mensagem do Sistema', MB_ICONERROR + MB_OK);


    Dm.IBLayoutInput.Close;
    Dm.IBLayoutInput.SelectSQL.Clear;
    Dm.IBLayoutInput.SelectSQL.Add('SELECT * FROM LAYOUT_INPUT WHERE CLIENTE = :CLIENTE');
    Dm.IBLayoutInput.ParamByName('CLIENTE').Value := EdtCliente.text;
    Dm.IBLayoutInput.Open;
//    If Dm.IBLayoutInput.IsEmpty then
//    Application.MessageBox('Layout de entrada n�o cadastrado para este cliente','Mensagem do Sistema', MB_ICONERROR + MB_OK);

If (Dm.IBLayoutInput.IsEmpty) and (Dm.IBLayoutOutput.IsEmpty) then
    Application.MessageBox('N�o existem layouts cadastrados para este cliente','Mensagem do Sistema', MB_ICONERROR + MB_OK)
else
  begin
  //lopping que habilitar todos os Botoes do form
    for I := 0 to ComponentCount - 1 do
      begin
        if (Components[I] is TsBitBtn) then
          TsBitBtn(Components[I]).Enabled := True;
      end;
       for I := 0 to ComponentCount - 1 do
      begin
        if (Components[I] is TsButton) then
          TsButton(Components[I]).Enabled := True;
      end;
  end;


end;

procedure TFrmLayoutExiste.GroupBox3Click(Sender: TObject);
begin
Dm.IBLayoutOutput.edit;
end;

procedure TFrmLayoutExiste.sBitBtn2Click(Sender: TObject);
begin
close;
end;

procedure TFrmLayoutExiste.GridInPutKeyPress(Sender: TObject;
  var Key: Char);
begin
key:=Upcase(key); 
end;

procedure TFrmLayoutExiste.DBComboBox1Click(Sender: TObject);
begin
If DbComboBox1.Text = 'T' then
  Label11.Caption := 'SIM' else Label11.Caption := 'N�O';
end;

procedure TFrmLayoutExiste.CmbNumLojaClick(Sender: TObject);
begin
//If CmbNumLoja.Text = 'T' then
//  Label12.Caption := 'SIM' else Label12.Caption := 'N�O';
end;

procedure TFrmLayoutExiste.CmbZeroCODINTClick(Sender: TObject);
var zero1: string;
begin
If CmbZeroCODINT.Text = 'T' then
  Label15.Caption := 'SIM' else Label15.Caption := 'N�O';

 if CmbZeroCODINT.Text = 'F' then
  Zero1 := InputBox('C�digo Interno', 'Deseja C�digo Interno (PLU) sem altera��es?', 'S' );


 if (zero1 = 'S') or (zero1 = 's') then
   begin
    DM.IBLayoutOutputCOD_INT_F.Value := 'T';
   end;
end;

procedure TFrmLayoutExiste.CmbZeroEANClick(Sender: TObject);
var zero1: string;
begin
If CmbZeroEAN.Text = 'T' then
  Label10.Caption := 'SIM' else Label10.Caption := 'N�O';

  if CmbZeroEAN.Text = 'F' then
  Zero1 := InputBox('C�digo EAN', 'Deseja C�digo EAN sem altera��es?', 'S' );

  if (zero1 = 'S') or (zero1 = 's') then
   begin
    DM.IBLayoutOutputCOD_EAN_F.Value := 'T';
   end;  

end;

procedure TFrmLayoutExiste.Incluirnovoformato1Click(Sender: TObject);
var add : string;
begin
add := InputBox('Nova extens�o', 'Digite o novo formato da extens�o, sem asterisco. (Ex: doc, xls)','');
DbComboBox3.Items.Add(Add);
end;

procedure TFrmLayoutExiste.DBComboBox4Click(Sender: TObject);
var Form1, resultado : string;
begin
 DM.IBInventario.Open;

   if DBComboBox4.Text = 'T' then
    begin
      Dm.IBLayoutOutputDATA_FILE.Value := 'T';
      Form1 := UpperCase(InputBox('Formato Data', 'Digite o modelo do formato','Ex.: "DDMMAAAA" '));
       if (Form1 <> 'MMDDAA') and (Form1 <> 'DDMMAA') and (Form1 <> 'DDMMAAAA') and (Form1 <> 'MMDDAAAA') and
          (Form1 <> 'MM') and (Form1 <> 'DD') and (Form1 <> 'AA') and (Form1 <> 'AAAA') then
       begin
        Application.MessageBox('Formato inv�lido!','Mensagem do Sistema', MB_ICONERROR + MB_OK);
       end

    end
   else
    begin
      Dm.IBLayoutOutputDATA_FILE.Value := 'F';
    end;
 try
If (Form1 = 'MMDDAA') or (Form1 = 'mmddaa') then
  begin
   // Label46.caption := 'T';
    Resultado := Copy(DM.IBInventarioINV_DATAINV.AsString,4,2)+ Copy(DM.IBInventarioINV_DATAINV.AsString,1,2)+ Copy(DM.IBInventarioINV_DATAINV.AsString,9,2) ;
    DM.IBLayoutOutputDATA_FORMAT.Value := resultado;
  end;

If (Form1 = 'DDMMAA') or (Form1 = 'ddmmaa') then
  begin
    //Label47.caption := 'T';
    Resultado := Copy(DM.IBInventarioINV_DATAINV.AsString,1,2)+ Copy(DM.IBInventarioINV_DATAINV.AsString,4,2)+ Copy(DM.IBInventarioINV_DATAINV.AsString,9,2);
    DM.IBLayoutOutputDATA_FORMAT.Value := resultado;
  end;

If (Form1 = 'DDMMAAAA') or (Form1 = 'ddmmaaaa') then
  begin
    //Label48.caption := 'T';
    Resultado := Copy(DM.IBInventarioINV_DATAINV.AsString,1,2)+ Copy(DM.IBInventarioINV_DATAINV.AsString,4,2)+ Copy(DM.IBInventarioINV_DATAINV.AsString,7,4) ;
    DM.IBLayoutOutputDATA_FORMAT.Value := resultado;
  end;

If (Form1 = 'MMDDAAAA') or (Form1 = 'mmddaaaa') then
  begin
     //Label49.Caption := 'T';
     Resultado := Copy(DM.IBInventarioINV_DATAINV.AsString,4,2)+ Copy(DM.IBInventarioINV_DATAINV.AsString,1,2)+ Copy(DM.IBInventarioINV_DATAINV.AsString,7,4);
     DM.IBLayoutOutputDATA_FORMAT.Value := resultado;
  end;

If (Form1 = 'MM') or (Form1 = 'mm') then
  begin
     //Label50.Caption := 'T';
     Resultado := Copy(DM.IBInventarioINV_DATAINV.AsString,4,2);
     DM.IBLayoutOutputDATA_FORMAT.Value := resultado;
  end;

If (Form1 = 'DD') or (Form1 = 'dd') then
  begin
    //Label51.Caption := 'T';
    Resultado := Copy(DM.IBInventarioINV_DATAINV.AsString,1,2);
    DM.IBLayoutOutputDATA_FORMAT.Value := resultado;
  end;

If (Form1 = 'AA') or (Form1 = 'aa') then
  begin
    //Label52.Caption := 'T';
    Resultado := Copy(DM.IBInventarioINV_DATAINV.AsString,9,2);
    DM.IBLayoutOutputDATA_FORMAT.Value := resultado;
  end;

If (Form1 = 'AAAA') or (Form1 = 'aaaa') then
  begin
    //Label53.Caption := 'T';
    Resultado := Copy(DM.IBInventarioINV_DATAINV.AsString,7,4);
    DM.IBLayoutOutputDATA_FORMAT.Value := resultado;
  end;

except
   ShowMessage('Erro');
   Dm.Transacao.RollbackRetaining;
end;
end;
procedure TFrmLayoutExiste.DBComboBox5Click(Sender: TObject);
var geral: string;
begin

   if DBComboBox5.Text = 'T' then
    begin
       DM.IBLayoutOutputBREAK_FILE.Value := 'T';
       geral := InputBox('Quebrar Arquivo', 'Deseja gerar o 3� arquivo final GERAL?','S');
    end
   else
    begin
      Dm.IBLayoutOutputBREAK_FILE.Value := 'F';
    end;


    If (geral = 'S') or (geral = 's') then
      begin
        DM.IBLayoutOutputFILE_GERAL.Value := 'T';
      end
    else
      begin
        DM.IBLayoutOutputFILE_GERAL.Value := 'F';
      end;


end;

end.
