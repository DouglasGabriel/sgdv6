unit USelecionaSecao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ComCtrls, ExtCtrls, RpRave,
  DB, RpCon, RpConDS, RpBase, RpSystem, RpDefine, Grids, DBGrids, RpRender,
  RpRenderPDF, RpRenderText, sBitBtn, sLabel;

type
  TFrmSelSecao = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    StatusBar1: TStatusBar;
    Panel3: TPanel;
    Label3: TLabel;
    Edit1: TEdit;
    Label2: TLabel;
    Edit2: TEdit;
    BitBtn1: TBitBtn;
    RadioGroup1: TRadioGroup;
    sLabelFX1: TsLabelFX;
    procedure FormShow(Sender: TObject);
    procedure Edit1Exit(Sender: TObject);
    procedure Edit2Exit(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmSelSecao: TFrmSelSecao;

implementation

uses UDm, UFuncProc, UInventario;

{$R *.dfm}

procedure TFrmSelSecao.FormShow(Sender: TObject);
begin
Edit1.SetFocus;
end;

procedure TFrmSelSecao.Edit1Exit(Sender: TObject);
begin
edit2.SetFocus;
end;

procedure TFrmSelSecao.Edit2Exit(Sender: TObject);
begin
 BitBtn1.SetFocus;
end;

procedure TFrmSelSecao.BitBtn1Click(Sender: TObject);
var
StrSql : string;
Begin

  if (Edit1.Text = '') or (Edit2.Text = '') then
  Application.MessageBox('� necess�rio informar a se��o inicial e a se��o final.','Mensagem do Sistema', MB_ICONERROR + MB_OK)
  else
  Begin
   If RadioGroup1.ItemIndex = 0 then
    begin

         Dm.QGeraRelAudit1.Close;
         Dm.QGeraRelAudit1.SQL.Clear;
         StrSql :=          'select h.ros_nome, h.ros_matricula, s.sec_secao, c.are_area ';
         StrSql := StrSql + 'from  secao s right join contagens c on (c.sec_secao = s.sec_secao)left join horario h on (h.ros_matricula = s.hor_matricula) ';
         StrSql := StrSql + 'where (sec_status <> "DEL") and (sec_aberta = "F") ';
         StrSql := StrSql + 'and (SEC_SECAO >= ''' + Edit1.text + ''' and SEC_SECAO <= ''' + Edit2.text + ''') ';
         StrSql := StrSql + 'group by s.sec_secao, h.ros_nome, h.ros_matricula, c.are_area ';
         StrSql := StrSql + 'order by s.sec_secao';
         Dm.QGeraRelAudit1.SQL.Add(StrSql);
         Dm.QGeraRelAudit1.Open;

     if Dm.QGeraRelAudit1.RecordCount > 0 then
       Begin
        Try
         DM.RvPrjWis.Close;
          If (DM.IBInventarioINV_LOJA.Value = 'CP1') or (DM.IBInventarioINV_LOJA.Value = 'CP2') or (DM.IBInventarioINV_LOJA.Value = 'CP3') then
           begin
            Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\AuditoriaCPV.rav';
            DM.QGeraRelAudit2.Close;
            Dm.QGeraRelAudit2.SQL.Clear;
            DM.QGeraRelAudit2.SQL.Add('select co.con_ean, sum(co.CON_QUANTIDADE), co.con_id, ca.cad_codint, co.con_cadcodint, ca.cad_un, ');
            DM.QGeraRelAudit2.SQL.Add('ca.cad_preco, ca.cad_desc, co.are_area, co.sec_secao, co.hor_matricula, co.tonalidade ');
            DM.QGeraRelAudit2.SQL.Add('from contagens co ');
            DM.QGeraRelAudit2.SQL.Add('left join cadastro_conso ca on (ca.cad_ean = co.tonalidade) ');
            DM.QGeraRelAudit2.SQL.Add('where (co.con_status = ".") and (co.sec_secao >= :secini) and (co.sec_secao <= :secfim) ');
            DM.QGeraRelAudit2.SQL.Add('group by co.sec_secao, co.are_area , co.con_ean, ca.cad_codint, ca.cad_un, ca.cad_preco, ca.cad_desc, co.con_id, co.hor_matricula, co.tonalidade, co.con_cadcodint ');
            DM.QGeraRelAudit2.SQL.Add('order by co.con_id ');
            Dm.QGeraRelAudit2.Params.ParamByName('secini').Value := Edit1.Text;
            Dm.QGeraRelAudit2.Params.ParamByName('secfim').Value := Edit2.Text;
            Dm.QGeraRelAudit2.Open;
            Dm.RvPrjWis.Open;
            Dm.RvSysWis.DefaultDest := rdPreview;
            Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup] ;
            Dm.RvPrjWis.ExecuteReport('AuditoriaCPV')
           end
          else
           begin
            Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Auditoria.rav';
            DM.QGeraRelAudit2.Close;
            Dm.QGeraRelAudit2.SQL.Clear;
            DM.QGeraRelAudit2.SQL.Add('select co.con_ean, sum(co.CON_QUANTIDADE), co.con_id, ca.cad_codint, ca.cad_un, ');
            DM.QGeraRelAudit2.SQL.Add('ca.cad_preco, ca.cad_desc, co.are_area, co.sec_secao, co.hor_matricula, co.tonalidade ');
            DM.QGeraRelAudit2.SQL.Add('from contagens co ');
            DM.QGeraRelAudit2.SQL.Add('left join cadastro_conso ca on (ca.cad_ean = co.con_ean) ');
            DM.QGeraRelAudit2.SQL.Add('where (co.con_status = ".") and (co.sec_secao >= :secini) and (co.sec_secao <= :secfim) ');
            DM.QGeraRelAudit2.SQL.Add('group by co.sec_secao, co.are_area , co.con_ean, ca.cad_codint, ca.cad_un, ca.cad_preco, ca.cad_desc, co.con_id, co.hor_matricula, co.tonalidade  ');
            DM.QGeraRelAudit2.SQL.Add('order by co.con_id ');
            Dm.QGeraRelAudit2.Params.ParamByName('secini').Value := Edit1.Text;
            Dm.QGeraRelAudit2.Params.ParamByName('secfim').Value := Edit2.Text;
            Dm.QGeraRelAudit2.Open;
            Dm.RvPrjWis.Open;
            Dm.RvSysWis.DefaultDest := rdPreview;
            Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup] ;
            Dm.RvPrjWis.ExecuteReport('Auditoria');
           end;
        Finally
          //Abrir DataSet Se��o
          Dm.IBSecao.Close;
        //  Dm.IBSecao.Params.ParamByName('secini').Value := Edit1.Text;
        //  Dm.IBSecao.Params.ParamByName('secfin').Value := Edit2.Text;
          Dm.IBSecao.Open;
          Dm.IBSecao.DisableControls;

          //PosicionaSecao(IDSECAO);
          Dm.IBSecao.locate('SEC_ID',IDSECAO,[Lopartialkey,locaseinsensitive]);
          Dm.IBSecao.EnableControls;
        End;
       End
     else
      Begin
       Application.MessageBox('N�o existem se��es para ser gerado um relat�rio de auditoria.','Mensagem do Sistema', MB_ICONERROR + MB_OK);
       Edit1.SetFocus;
      End;
    end;
  If RadioGroup1.ItemIndex = 1 then
   begin
       Dm.QGeraRelAuditConso1.Close;
       Dm.QGeraRelAuditConso1.SQL.Clear;
       StrSql := 'select h.ros_nome, h.ros_matricula, s.sec_secao, c.are_area ';
       StrSql := StrSql + 'from  secao s right join contagens c on(c.sec_secao = s.sec_secao)left join horario h on (h.ros_matricula = s.hor_matricula) ';
       StrSql := StrSql + 'where (sec_status <> "DEL") and (sec_aberta = "F") ';
       StrSql := StrSql + 'and (SEC_SECAO >= ''' + Edit1.text + ''' and SEC_SECAO <= ''' + Edit2.text + ''') ';
       StrSql := StrSql + 'group by s.sec_secao, h.ros_nome, h.ros_matricula, c.are_area ';
       StrSql := StrSql + 'order by s.sec_secao';
       Dm.QGeraRelAuditConso1.SQL.Add(StrSql);
       //DM.QGeraRelAuditConso1.Params.ParamByName('area').Value := DM.IBContagensARE_AREA.Value;
       Dm.QGeraRelAuditConso1.Open;

     if Dm.QGeraRelAuditConso1.RecordCount > 0 then
       Begin
        Try

         If (DM.IBInventarioINV_LOJA.Value = 'CP1') or (DM.IBInventarioINV_LOJA.Value = 'CP2') or (DM.IBInventarioINV_LOJA.Value = 'CP3') then
          begin
            Dm.QGeraRelAuditConso2.close;
            DM.RvPrjWis.Close;
            Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\AuditoriaConsoCPV.rav';
            DM.QGeraRelAuditConso2.Close;
            DM.QGeraRelAuditConso2.SQL.Clear;
            DM.QGeraRelAuditConso2.SQL.Add('select co.con_ean, sum(co.con_quantidade) as qtd, co.sec_secao, ca.cad_desc, ca.cad_codint, co.con_cadcodint, sum(ca .cad_preco) as preco, co.are_area, ca.cad_un, co.hor_matricula, co.tonalidade ');
            DM.QGeraRelAuditConso2.SQL.Add('from contagens co left join cadastro_conso ca on (ca.cad_ean = co.tonalidade) ');
            DM.QGeraRelAuditConso2.SQL.Add('where (co.con_status = ".") and (co.sec_secao >= :secini) and (co.sec_secao <= :secfim) ');
            DM.QGeraRelAuditConso2.SQL.Add('group by co.con_ean, co.sec_secao, ca.cad_desc, ca.cad_codint,  co.are_area, ca.cad_un, co.hor_matricula, co.tonalidade, co.con_cadcodint ');
            DM.QGeraRelAuditConso2.SQL.Add('order by qtd desc ');
            DM.QGeraRelAuditConso2.Params.ParamByName('secini').Value := Edit1.Text;
            DM.QGeraRelAuditConso2.Params.ParamByName('secfim').Value := Edit2.Text;
            //DM.QGeraRelAuditConso2.Params.ParamByName('area').Value := DM.IBContagensARE_AREA.Value;
            Dm.QGeraRelAuditConso2.Open;
            DM.RvPrjWis.Open;
            Dm.RvSysWis.DefaultDest := rdPreview;
            Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
            Dm.RvPrjWis.ExecuteReport('AuditoriaConsoCPV');
          end
         else
          begin
            Dm.QGeraRelAuditConso2.close;
            DM.RvPrjWis.Close;
            Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\AuditoriaConso.rav';
            DM.QGeraRelAuditConso2.Params.ParamByName('secini').Value := Edit1.Text;
            DM.QGeraRelAuditConso2.Params.ParamByName('secfim').Value := Edit2.Text;
            //DM.QGeraRelAuditConso2.Params.ParamByName('area').Value := DM.IBContagensARE_AREA.Value;
            Dm.QGeraRelAuditConso2.Open;
            DM.RvPrjWis.Open;
            Dm.RvSysWis.DefaultDest := rdPreview;
            Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup] ;
            Dm.RvPrjWis.ExecuteReport('AuditoriaConso');
          end;
        Finally
          //Abrir DataSet Se��o
          Dm.IBSecao.Close;
       //   Dm.IBSecao.Params.ParamByName('secini').Value := Edit1.Text;
       //   Dm.IBSecao.Params.ParamByName('secfin').Value := Edit2.Text;
          Dm.IBSecao.Open;
          Dm.IBSecao.DisableControls;

          //PosicionaSecao(IDSECAO);
          Dm.IBSecao.locate('SEC_ID',IDSECAO,[Lopartialkey,locaseinsensitive]);
          Dm.IBSecao.EnableControls;
        End;
       end;
   end;
  end;
end;


end.


