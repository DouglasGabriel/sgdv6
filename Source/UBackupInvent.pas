unit UBackupInvent;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, FileCtrl, ComCtrls, ExtCtrls, Buttons, ZipForge,
  Gauges, IdBaseComponent, IdComponent, IdTCPConnection,
  IdTCPClient, IdFTP, IdTCPServer,
  IdSMTPServer, IdMessageClient, IdSMTP, sGauge,  IdMessage, WinInet, ShellApi,
  sLabel;

type
  TFrmBackupInvent = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    StatusBar1: TStatusBar;
    Panel3: TPanel;
    Label3: TLabel;
    DirectoryListBox1: TDirectoryListBox;
    DriveComboBox1: TDriveComboBox;
    BitBtn1: TBitBtn;
    ZipForge1: TZipForge;
    sLabelFX1: TsLabelFX;
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmBackupInvent: TFrmBackupInvent;

implementation

uses UDm, UFuncProc;

{$R *.dfm}

procedure TFrmBackupInvent.BitBtn1Click(Sender: TObject);
var
Diretorio : String;
Destino, DestinoBanco, OrigemBanco, Origem, OS : String;
DestinoExe, OrigemExe : String;
S, T: TFileStream;
SExe, TExe : TFileStream;
Begin
 //Dm.IBInventario.Open;
 OS := Dm.IBInventarioINV_OS.Value;



  try
   Diretorio := DirectoryListBox1.Directory ;  //Diretório escolhido
   Destino :=  Diretorio + 'Backup_OS' + OS + '\SGD';

   //Zip(Destino,'C:\SGD\','SGD_'+Dm.IBInventarioINV_LOJA.Value+'_'+Dm.IBInventarioINV_NUMLOJA.Value+'_'+Dm.IBInventarioINV_DATAINV.AsString+'.zip','*.*');

   Origem := 'C:\SGD';//'C:\SGD_'+Dm.IBInventarioINV_LOJA.Value+'_'+Dm.IBInventarioINV_NUMLOJA.Value+'_'+Dm.IBInventarioINV_DATAINV.AsString+'.zip';

   CopyDir(Origem,Destino);
  finally
   ShowMessage('Copia Finalizada');
  end;
end;

end.
