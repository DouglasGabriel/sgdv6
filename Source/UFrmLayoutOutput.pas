unit UFrmLayoutOutput;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, DBCtrls, Menus, sLabel;

type
  TFrmLayoutOutput = class(TForm)
    GroupBox1: TGroupBox;
    chkloja: TCheckBox;
    chkcodint: TCheckBox;
    chkean: TCheckBox;
    chkquantidade: TCheckBox;
    chkpreco: TCheckBox;
    chkdata: TCheckBox;
    chkseparador: TCheckBox;
    ooa: TLabel;
    edtsizeloja: TEdit;
    edtordemloja: TEdit;
    aao: TLabel;
    edtsizecodint: TEdit;
    edtsizeean: TEdit;
    edtsizequantidade: TEdit;
    edtsizepreco: TEdit;
    edtsizedata: TEdit;
    edtsizeseparador: TEdit;
    edtordemcodint: TEdit;
    edtordemean: TEdit;
    edtordemquantidade: TEdit;
    edtordempreco: TEdit;
    edtordemdata: TEdit;
    lblcaracterseparador: TLabel;
    EdtCaracterSeparador: TEdit;
    lblidiomadata: TLabel;
    edtidiomadata: TEdit;
    GroupBox2: TGroupBox;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    Label1: TLabel;
    EdtCliente: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    PopupMenu1: TPopupMenu;
    Incluirextenso1: TMenuItem;
    edtqtde_dec: TEdit;
    Label7: TLabel;
    CmbCabecalho: TComboBox;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label20: TLabel;
    ChkFixo: TCheckBox;
    edtsizefixo: TEdit;
    Label21: TLabel;
    edtordemfixo: TEdit;
    edtcaracterfixo: TEdit;
    Label19: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label32: TLabel;
    CmbBreakQtde: TComboBox;
    ChkSetor: TCheckBox;
    EdtSizeSetor: TEdit;
    EdtOrdemSetor: TEdit;
    CheckMarc: TCheckBox;
    EdtOrdemMarc: TEdit;
    EdtSizeMarc: TEdit;
    Label37: TLabel;
    Label38: TLabel;
    GroupBox4: TGroupBox;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    Label39: TLabel;
    CmbSecaoBreakFile: TComboBox;
    CmbSecaoRange: TComboBox;
    CmbSecao: TComboBox;
    Edit1: TEdit;
    Edit2: TEdit;
    EdtOrdemSecaoWis: TEdit;
    EdtOrdemSecaoRange: TEdit;
    CmbUnMed: TComboBox;
    EdtOrdemUnMed: TEdit;
    EdtSizeEnd: TEdit;
    GroupBox3: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label16: TLabel;
    Label33: TLabel;
    EdtNomeArq: TEdit;
    CmbNumLoja: TComboBox;
    cmbExtensao: TComboBox;
    CmbCodInt: TComboBox;
    Label40: TLabel;
    Label41: TLabel;
    CheckLote: TCheckBox;
    EdtSizeLote: TEdit;
    EdtOrdemLote: TEdit;
    Label42: TLabel;
    Label43: TLabel;
    CheckDescricao: TCheckBox;
    EdtSizeDescricao: TEdit;
    EdtOrdemDescricao: TEdit;
    Label44: TLabel;
    Label45: TLabel;
    CmbData: TComboBox;
    Label46: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    Label51: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    Label54: TLabel;
    Label55: TLabel;
    Label56: TLabel;
    Label57: TLabel;
    Label58: TLabel;
    Label59: TLabel;
    cmbNomeLoja: TComboBox;
    sLabelFX1: TsLabelFX;
    procedure chklojaClick(Sender: TObject);
    procedure chkcodintClick(Sender: TObject);
    procedure chkeanClick(Sender: TObject);
    procedure chkquantidadeClick(Sender: TObject);
    procedure chkprecoClick(Sender: TObject);
    procedure chkdataClick(Sender: TObject);
    procedure chkseparadorClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure Incluirextenso1Click(Sender: TObject);
    procedure DBComboBox1Click(Sender: TObject);
    procedure CmbNumLojaClick(Sender: TObject);
    procedure CmbCabecalhoClick(Sender: TObject);
    procedure ChkFixoClick(Sender: TObject);
    procedure CmbSecaoRangeClick(Sender: TObject);
    procedure ChkSetorClick(Sender: TObject);
    procedure CheckMarcClick(Sender: TObject);
    procedure EdtSizeEndExit(Sender: TObject);
    procedure CheckLoteClick(Sender: TObject);
    procedure CheckDescricaoClick(Sender: TObject);
    procedure CmbDataClick(Sender: TObject);
    procedure CmbSecaoBreakFileClick(Sender: TObject);


  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmLayoutOutput: TFrmLayoutOutput;
  estoque, loja: string[10];
  resultadodata: string;

implementation
Uses Udm, UFuncProc;

{$R *.dfm}

procedure TFrmLayoutOutput.chklojaClick(Sender: TObject);
var tamanho, ordem: string;
begin
Tamanho := InputBox('Loja', 'Digite o tamanho do campo loja (em caracteres)','');
Ordem := InputBox('Loja', 'Digite a ordem do campo loja','');
EdtSizeLoja.Text:=Tamanho;
EdtOrdemLoja.Text:=Ordem;

If Tamanho <> '' then
    EdtSizeLoja.Visible:=true
else
    EdtSizeLoja.Visible:=false;


If Ordem <> '' then
    EdtOrdemLoja.Visible:=true
else
    EdtOrdemLoja.Visible:=false;
     

end;

procedure TFrmLayoutOutput.chkcodintClick(Sender: TObject);
var tamanho, ordem, zero, zero1, zero2: string;
begin
Tamanho := InputBox('C�digo Interno', 'Digite o tamanho do campo C�DIGO INTERNO (em caracteres)','');
Ordem := InputBox('C�digo Interno', 'Digite a ordem do campo C�DIGO INTERNO','');
Zero1 := InputBox('C�digo Interno', 'Deseja C�digo Interno(PLU) conforme Cadastro de Produtos?', 'S' );
 if (zero1 = 'N') or (zero1 = 'n') then
  begin
    Zero := InputBox('C�digo Interno', 'Completar C�digo Interno (PLU) com zeros � esquerda?','S');
     if (zero = 'N') or (zero = 'n') then
      begin
        zero2 := InputBox('C�digo Interno', 'Alinhar C�digo Interno (PLU) com espa�o em branco � esquerda?','S');
      end;
  end;

EdtSizeCodInt.Text:=Tamanho;
EdtOrdemCodInt.Text:=Ordem;
If (Zero = 'S') or (Zero = 's') then
  begin
    label19.caption := 'T';
    Label20.Caption := 'Completar com zeros � esquerda';
  end
else
  begin
    label19.caption:='F';
    label20.caption := '';
  end;

if (zero1 = 'S') or (zero1 = 's') then
  begin
    Label55.Caption := 'T';
  end
 else
  begin
    Label55.Caption := 'F';
  end;

if (zero2 = 'S') or (zero2 = 's') then
  begin
    Label57.Caption := 'T';
  end
 else
  begin
    Label57.Caption := 'F';
  end;


If Tamanho <> '' then
    EdtSizeCodInt.Visible:=true
else
    EdtSizeCodInt.Visible:=false;


If Ordem <> '' then
    EdtOrdemCodInt.Visible:=true
else
    EdtOrdemCodInt.Visible:=false;
end;

procedure TFrmLayoutOutput.chkeanClick(Sender: TObject);
var tamanho, ordem, zero, zero1, zero2: string;
begin
Tamanho := InputBox('C�digo EAN', 'Digite o tamanho do campo C�DIGO EAN (em caracteres)','');
Ordem := InputBox('C�digo EAN', 'Digite a ordem do campo EAN','');
Zero1 := InputBox('C�digo EAN', 'Deseja C�digo EAN conforme Cadastro de Produtos?', 'S' );
   if (zero1 = 'N') or (zero1 = 'n') then
  begin
    Zero := InputBox('C�digo EAN', 'Completar EAN com zeros � esquerda?','S');
     if (zero = 'N') or (zero = 'n') then
      begin
        zero2 := InputBox('C�digo EAN', 'Alinhar EAN com espa�o em branco � esquerda?','S');
      end;
  end;


EdtSizeEan.Text:=Tamanho;
EdtOrdemEan.Text:=Ordem;
If (Zero = 'S') or (Zero = 's') then
  begin
    label17.caption := 'T';
    Label18.Caption := 'Completar com zeros � esquerda';
  end
else
  begin
    label17.caption:='F';
    label18.caption := '';
  end;

if (zero1 = 'S') or (zero1 = 's') then
  begin
    Label56.Caption := 'T';
  end
 else
  begin
    Label56.Caption := 'F';
  end;

if (zero2 = 'S') or (zero2 = 's') then
  begin
    Label58.Caption := 'T';
  end
 else
  begin
    Label58.Caption := 'F';
  end;



If Tamanho <> '' then
    EdtSizeEan.Visible:=true
else
    EdtSizeEan.Visible:=false;


If Ordem <> '' then
    EdtOrdemEan.Visible:=true
else
    EdtOrdemEan.Visible:=false;

end;

procedure TFrmLayoutOutput.chkquantidadeClick(Sender: TObject);
var tamanho, ordem, dec, zero, branco, nada: string;
begin
Tamanho := InputBox('Quantidade', 'Digite o tamanho do campo QUANTIDADE (em caracteres)','');
Ordem := InputBox('Quantidade', 'Digite a ordem do campo QUANTIDADE','');
Dec := InputBox('Quantidade', 'Quantidades decimais (S/N)?','');
nada := InputBox('Quantidade', 'Quantidade sem complementos (S/N)?','');

If (nada = 'S') or (nada = 's') then
  begin
    Label22.Caption := 'Sem complementos';
    label23.Caption := '1';
  end
else
  begin
    Zero := InputBox('Quantidade', 'Completar com zeros � esquerda? (S/N)?','');
    If (Zero = 'S') or (Zero = 's') then
      begin
        Label22.caption := 'Completar com zeros � esquerda';
        label23.Caption := '2';
      end
    else
      begin
        branco := InputBox('Quantidade', 'Completar com espa�os em branco � esquerda? (S/N)?','');
        If (branco = 'S') or (branco = 's') then
          begin
            Label22.caption := 'Completar com espa�os em branco � esquerda';
            label23.Caption := '3';
          end;
      end;
  end;


EdtSizeQuantidade.Text:=Tamanho;
EdtOrdemQuantidade.Text:=Ordem;
If (Dec = 'S') or (Dec = 's') then
begin
  EdtQtde_Dec.Text:='T';
  EdtQtde_Dec.Visible := True;
  Label12.Caption :='SIM';
end
else
begin
  EdtQtde_Dec.Text:='F';
  Label12.Caption :='N�O';
end;
If Tamanho <> '' then
    EdtSizeQuantidade.Visible:=true
else
    EdtSizeQuantidade.Visible:=false;


If Ordem <> '' then
    EdtOrdemQuantidade.Visible:=true
else
    EdtOrdemQuantidade.Visible:=false;


end;

procedure TFrmLayoutOutput.chkprecoClick(Sender: TObject);
var tamanho, ordem: string;
begin
Tamanho := InputBox('Pre�o', 'Digite o tamanho do campo PRE�O (em caracteres)','');
Ordem := InputBox('Pre�o', 'Digite a ordem do campo PRE�O','');
EdtSizePreco.Text:=Tamanho;
EdtOrdemPreco.Text:=Ordem;

If Tamanho <> '' then
    EdtSizePreco.Visible:=true
else
    EdtSizePreco.Visible:=false;


If Ordem <> '' then
    EdtOrdemPreco.Visible:=true
else
    EdtOrdemPreco.Visible:=false;


end;

procedure TFrmLayoutOutput.chkdataClick(Sender: TObject);
var tamanho, ordem, idioma: string;
begin
Idioma :=  InputBox('Data', 'DATA em INLG�S? (S/N)','');
Tamanho := InputBox('Data', 'Digite o tamanho do campo DATA (em caracteres)','');
Ordem := InputBox('Data', 'Digite a ordem do campo DATA','');
EdtSizeData.Text:=Tamanho;
EdtOrdemData.Text:=Ordem;

If Tamanho <> '' then
    EdtSizeData.Visible:=true
else
    EdtSizeData.Visible:=false;

If (Idioma = 'S') or (Idioma = 's') then
  Begin
    EdtIdiomaData.Visible:=true;
    EdtIdiomaData.Text:='T';
    Label13.Caption := 'INGL�S';
  end
else
  Begin
    EdtIdiomaData.Visible:=false;
    Label13.Caption := 'PORTUGU�S';
  end;

If Ordem <> '' then
    EdtOrdemData.Visible:=true
else
    EdtOrdemData.Visible:=false;

end;

procedure TFrmLayoutOutput.chkseparadorClick(Sender: TObject);
var caracter, tamanho: string;
begin
caracter := InputBox('Separador', 'Digite o CAR�CTER do campo SEPERADOR(Ex: ;)','');
Tamanho := InputBox('Separador', 'Digite o tamanho do campo SEPARADOR (em caracteres)','');
EdtSizeSeparador.Text:=Tamanho;
EdtCaracterSeparador.Text:=Caracter;

If Tamanho <> '' then
    EdtSizeSeparador.Visible:=true
else
    EdtSizeSeparador.Visible:=false;


If Caracter <> '' then
    EdtCaracterSeparador.Visible:=true
else
    EdtCaracterSeparador.Visible:=false;
If EdtCaracterSeparador.Text = '*' then label14.caption := '0 (Zero)' else label14.caption := EdtCaracterSeparador.text;
end;

procedure TFrmLayoutOutput.SpeedButton2Click(Sender: TObject);
begin
close;
end;

procedure TFrmLayoutOutput.SpeedButton1Click(Sender: TObject);
var verifica : Boolean;
begin
  verifica := False;
  If EdtCliente.Text = '' then Application.MessageBox('Favor preencher a SIGLA do cliente','Mensagem do Sistema', MB_ICONERROR + MB_OK) else
   begin
            Dm.IBLayoutOutput.close;
            Dm.IBLayoutOutput.SelectSQL.Clear;
            Dm.IBLayoutOutput.SelectSQL.Add('Select * from LAYOUT_OUTPUT where CLIENTE = :CLIENTE');
            Dm.IBLayoutOutput.ParamByname('CLIENTE').Value := EdtCliente.Text;
            Dm.IBLayoutOutput.Open;
            Dm.IBInventario.Open;

              If Not Dm.IBLayoutOutput.IsEmpty then Application.MessageBox('Este cliente j� possui um layout definido','Mensagem do Sistema', MB_ICONERROR + MB_OK) else
                begin //4
                  Dm.IBLayoutOutPut.Insert;
                  Dm.IBLayoutOutputCLIENTE.Value:=EdtCliente.text;

                  //LOJA
                  If chkLoja.Checked then
                    Begin
                      Dm.IBLayoutOutputLOJA.Value:='T';
                      Dm.IBLayoutOutputSIZE_LOJA.Value:=EdtSizeLoja.Text;
                      Dm.IBLayoutOutputOR_LOJA.value:=EdtOrdemLoja.Text;
                    end;

                  //COD INT
                  If chkCodInt.Checked then
                    Begin
                      Dm.IBLayoutOutputCOD_INT.Value:='T';
                      Dm.IBLayoutOutputSIZE_CODINT.Value:=EdtSizeCodInt.Text;
                      Dm.IBLayoutOutputOR_COD_INT.value:=EdtOrdemCodInt.Text;
                      Dm.IBLayoutOutputCOD_INT_ZERO.Value:=Label19.caption;
                      DM.IBLayoutOutputCOD_INT_F.Value := Label55.Caption;
                      DM.IBLayoutOutputCOD_INT_SPACE.Value := Label57.Caption;                  
                    end;

                  //EAN
                  If chkEAN.Checked then
                    Begin
                      Dm.IBLayoutOutputEAN.Value:='T';
                      Dm.IBLayoutOutputSIZE_EAN.Value:=EdtSizeEAN.Text;
                      Dm.IBLayoutOutputOR_EAN.value:=EdtOrdemEAN.Text;
                      Dm.IBLayoutOutputCOD_EAN_ZERO.Value:=Label17.caption;
                      DM.IBLayoutOutputCOD_EAN_F.Value := Label56.Caption;
                      DM.IBLayoutOutputCOD_EAN_SPACE.Value := Label58.Caption;
                    end;

                //QTDE
                  If chkQuantidade.Checked then
                    Begin
                      Dm.IBLayoutOutputQtde.Value:='T';
                      Dm.IBLayoutOutputSIZE_Qtde.Value:=EdtSizeQuantidade.Text;
                      Dm.IBLayoutOutputOR_Qtde.value:=EdtOrdemQuantidade.Text;
                      Dm.IBLayoutOutputQtde_Dec.value:=EdtQtde_dec.Text;
                      If Label23.Caption = '1' then Dm.IBLayoutOutputQtde_nada.value:='T';
                      If Label23.Caption = '2' then Dm.IBLayoutOutputQtde_zero.value:='T';
                      If Label23.Caption = '3' then Dm.IBLayoutOutputQtde_branco.value:='T';
                    end;


                //PRECO
                  If chkPreco.Checked then
                    Begin
                      Dm.IBLayoutOutputPRECO.Value:='T';
                      Dm.IBLayoutOutputSIZE_PRECO.Value:=EdtSizePRECO.Text;
                      Dm.IBLayoutOutputOR_PRECO.value:=EdtOrdemPRECO.Text;
                    end;

                //DATA
                  If chkData.Checked then
                    Begin
                      Dm.IBLayoutOutputData_INV.Value:='T';
                      Dm.IBLayoutOutputSIZE_Data.Value:=EdtSizeData.Text;
                      Dm.IBLayoutOutputOR_Data.value:=EdtOrdemData.Text;
                      If EdtIdiomaData.Text = 'T' then Dm.IBLayoutOutputDATA_ENG.Value:='T' else Dm.IBLayoutOutputDATA_ENG.Value:='F';
                    end;

                //SEPARADOR UNIVERSAL
                  If chkSeparador.Checked then
                    Begin
                      Dm.IBLayoutOutputSEPARADOR.Value:='T';
                      Dm.IBLayoutOutputSIZE_SEPARADOR.Value:=EdtSizeSEPARADOR.Text;
                      Dm.IBLayoutOutputTIPO_SEPARADOR.Value:=EdtCaracterSeparador.Text;
                    end;

                //SEPARADOR FIXO
                  If chkFixo.Checked then
                    Begin
                      Dm.IBLayoutOutputFIXO.Value:='T';
                      Dm.IBLayoutOutputOR_FIXO.Value:=EdtOrdemFixo.Text;
                      Dm.IBLayoutOutputTIPO_FIXO.Value:=EdtCaracterFixo.Text;
                      Dm.IBLayoutOutputSIZE_FIXO.Value:=EdtSizeFixo.Text;
                    end;

              //SETOR
                  If chkSetor.Checked then
                    Begin
                      Dm.IBLayoutOutputSETOR.Value:='T';
                      Dm.IBLayoutOutputOR_SETOR.Value:=EdtOrdemSETOR.Text;
                      Dm.IBLayoutOutputSIZE_SETOR.Value:=EdtSizeSETOR.Text;
                      Dm.IBLayoutOutputSETOR_ZERO.Value:=Label43.caption;
                    end;

                //SE��ES OU ENDERE�O
                  If CmbSecao.Text = 'T' then
                      begin
                        Dm.IBLayoutOutputSECAO_WIS.Value := 'T';
                        Dm.IBLayoutOutputOR_SECAOWIS.Value := EdtOrdemSecaoWis.Text;
                        DM.IBLayoutOutputSIZE_ENDERECO.Value := EdtSizeEnd.Text;
                        Dm.IBLayoutOutputENDERECO_ZERO.Value := Label39.Caption;
                      end
                  else
                      Dm.IBLayoutOutputSECAO_WIS.Value := 'F';

               //SE��ES RANGES
                  If CmbSecaoRange.Text = 'T' then
                    begin
                      Dm.IBLayoutOutputSECAO_RANGE_DEPOSITO.Value := Label28.Caption;
                      Dm.IBLayoutOutputSECAO_RANGE_LOJA.Value := Label29.Caption;
                      Dm.IBLayoutOutPutOR_SECAORANGE.Value := EdtOrdemSecaoRange.Text;
                      Dm.IBLayoutOutputSECAO_RANGE.Value := 'T';
                    End;

                //QUEBRAR ARQUIVO LOJA E DEPOSITO
                  If CmbSecaoBreakFile.Text = 'T' then
                   begin
                     Dm.IBLayoutOutputBREAK_FILE.Value := 'T';
                     DM.IBLayoutOutputFILE_GERAL.Value := Label54.Caption;
                   end;


                //NOMEAR SE��ES ESTOQUE
                  Dm.IBLayoutOutputNAME_SECAO_ESTOQUE.Value := Edit1.Text;

               //NOMEAR SE��ES LOJA
                  If Edit2.Text = '*' then
                    Dm.IBLayoutOutputNAME_SECAO_LOJA.Value := ' '
                  else
                  Dm.IBLayoutOutputNAME_SECAO_LOJA.Value := Edit2.Text;

              //CABE�ALHO
                  If CmbCabecalho.Text = 'T' then
                   begin
                    Dm.IBLayoutOutputHEADER.Value := 'T'
                   end;

                   if CmbData.Text = 'T' then
                    begin
                      DM.IBLayoutOutputDATA_FILE.Value := 'T';
                      Dm.IBLayoutOutputDATA_FORMAT.Value := ResultadoData;
                    end;
                   
              //UNIDADE DE MEDIDA
                 If CmbUnMed.Text = 'T' then
                   begin
                     DM.IBLayoutOutputUN.Value := 'T';
                     DM.IBLayoutOutputOR_UN.Value := EdtOrdemUnMed.Text;
                   end;
              //MARCADOR POSITIVO
                 If CheckMarc.Checked then
                   begin
                     DM.IBLayoutOutputMARC_POSITIVO.Value := 'T';
                     DM.IBLayoutOutputOR_MARC.Value := EdtOrdemMarc.Text;
                     DM.IBLayoutOutputSIZE_MARC.Value := EdtSizeMarc.Text;
                   end;
              //LOTE
                 if CheckLote.Checked then
                   begin
                     Dm.IBLayoutOutputLOTE.Value := 'T';
                     DM.IBLayoutOutputOR_LOTE.Value := EdtOrdemLote.Text;
                     DM.IBLayoutOutputSIZE_LOTE.Value := EdtSizeLote.Text;
                     DM.IBLayoutOutputLOTE_ZERO.Value := Label42.Caption;
                   end;

                  //DESCRI��O
                  if CheckDescricao.Checked then
                   begin
                     DM.IBLayoutOutputDESCRICAO.Value := 'T';
                     DM.IBLayoutOutputOR_DESCRICAO.Value := EdtOrdemDescricao.Text;
                     DM.IBLayoutOutputSIZE_DESCRICAO.Value := EdtSizeDescricao.Text
                   end;

                  //NOMENCLATURA

                  If EdtNOmeArq.Text = '' then
                  begin
                   verifica := True;
                   ShowMessage('Favor digitar um nome para o arquivo final. (Ex: ArqFinal)')
                  end
                  else
                    Dm.IBLayoutOutputNOME_ARQUIVO.Value := EdtNomeArq.Text;

                  If (CmbNumLoja.ItemIndex = -1) or (CmbNumLoja.Text = '') then
                    begin
                     verifica := True;
                     ShowMessage('Informar se haver� o n�mero da loja na nomenclatura do arquivo final')
                    end
                  else
                    Dm.IBLayoutOutputNOME_ARQUIVO_LOJA.Value := CmbNumLoja.Text;

                  If (cmbNomeLoja.ItemIndex = -1) or (cmbNomeLoja.Text = '') then
                    begin
                     verifica := True;
                     ShowMessage('Informar se haver� o n�mero da loja na nomenclatura do arquivo final')
                    end
                  else
                    Dm.IBLayoutOutputNOME_ARQUIVO_NOMELOJA.Value := cmbNomeLoja.Text;

                  If CmbExtensao.Text = '' then
                   begin
                    verifica := True;
                    ShowMessage('Favor selecionar uma extens�o');
                   end
                  else
                    Dm.IBLayoutOutputNOME_ARQUIVO_EXTENSAO.Value := CmbExtensao.Text;

                   if CmbData.Text = '' then
                   begin
                     verifica := True;
                     ShowMessage('Informar se haver� data na nomenclatura do arquivo final');
                   end
                   else
                    DM.IBLayoutOutputDATA_FILE.Value := CmbData.Text;

                  If (CmbCodInt.ItemIndex = -1) or (CmbCodInt.Text = '') then
                   begin
                    verifica := True;
                    ShowMessage('Informe se o arquivo final ser� consolidado por C�digo Interno')
                   end
                  else
                    Dm.IBLayoutOutputCODINT_CONSO.Value := CmbCodInt.Text;


                end;


                If (EdtNomeArq.Text = '') and
                   (CmbNumLoja.ItemIndex = -1) or (CmbNumLoja.Text = '') and
                   (cmbNomeLoja.ItemIndex = -1) or (cmbNomeLoja.Text = '') and
                   (CmbExtensao.Text = '') and
                   (CmbCodInt.ItemIndex = -1) or (CmbCodInt.Text = '') then

                   Dm.Transacao.RollbackRetaining
                else
                    Dm.IBLayoutOutput.Post;
                    Dm.Transacao.CommitRetaining;
                    Application.MessageBox('Informa��es salvas','Mensagem do Sistema', MB_ICONINFORMATION + MB_OK);

   end;

end;

procedure TFrmLayoutOutput.Incluirextenso1Click(Sender: TObject);
var add: string;
begin
add := InputBox('Nova extens�o', 'Digite o novo formato da extens�o, sem asterisco. (Ex: doc, xls)','');
cmbExtensao.Items.Add(add);
end;

procedure TFrmLayoutOutput.DBComboBox1Click(Sender: TObject);
begin

If CmbCabecalho.Text = 'T' then
  Label3.Caption := 'SIM' else Label3.Caption := 'N�O';
end;

procedure TFrmLayoutOutput.CmbNumLojaClick(Sender: TObject);
begin
If CmbNumLoja.Text = 'T' then
  Label16.Caption := 'SIM' else Label16.Caption := 'N�O';
end;

procedure TFrmLayoutOutput.CmbCabecalhoClick(Sender: TObject);
begin
If CmbCabecalho.Text = 'T' then
  Label15.Caption := 'SIM' else Label15.Caption := 'N�O';
end;

procedure TFrmLayoutOutput.ChkFixoClick(Sender: TObject);
var caracter, tamanho, ordem : string;
begin
caracter := InputBox('Separador Fixo', 'Digite o CAR�CTER do campo SEPERADOR FIXO(Ex: ;)','');
Tamanho := InputBox('Separador Fixo', 'Digite o tamanho do campo SEPARADOR FIXO (em caracteres)','1');
ordem := InputBox('Separador Fixo', 'Digite a ordem SEPERADOR FIXO','');
EdtSizeFixo.Text:=Tamanho;
EdtCaracterFixo.Text:=Caracter;
EdtOrdemFixo.Text:=Ordem;
end;

procedure TFrmLayoutOutput.CmbSecaoRangeClick(Sender: TObject);
begin
If CmbSecaoRange.Text = 'T' then
  begin
    Estoque := InputBox('Range de Se��es','Digite o valor da maior se��o utilizada no DEP�SITO. (Ex.: 2000)','');
    Loja := InputBox('Range de Se��es','Digite o valor da maior se��o utilizada na �REA DE VENDAS. (Ex.: 9999)','');
    Label28.Caption := Estoque;
    Label29.Caption := Loja;


  end;
end;

procedure TFrmLayoutOutput.ChkSetorClick(Sender: TObject);
var tamanho, ordem, Zero: string;
begin
Tamanho := InputBox('Setor/Estoque', 'Digite o tamanho do campo SETOR/ESTOQUE (em caracteres)','');
Ordem := InputBox('Setor/Estoque', 'Digite a ordem do campo SETOR/ESTOQUE','');
Zero := InputBox('Setor/Estoque', 'Completar SETOR/ESTOQUE com zeros � esquerda?','S');
EdtSizeSetor.Text:=Tamanho;
EdtOrdemSetor.Text:=Ordem;
If (Zero = 'S') or (Zero = 's') then
  begin
    Label43.caption := 'T';
  end
else
  begin
    label43.caption:='F';
  end;


If Tamanho <> '' then
    EdtSizeSetor.Visible:=true
else
    EdtSizeSetor.Visible:=false;


If Ordem <> '' then
    EdtOrdemSetor.Visible:=true
else
    EdtOrdemSetor.Visible:=false;
end;

procedure TFrmLayoutOutput.CheckMarcClick(Sender: TObject);
var tamanho, ordem: string;
begin
Tamanho := InputBox('Marcador', 'Digite o tamanho do campo MARCADOR(em caracteres)','');
Ordem := InputBox('Marcador', 'Digite a ordem do campo MARCADOR','');
EdtSizeMarc.Text:=Tamanho;
EdtOrdemMarc.Text:=Ordem;

If Tamanho <> '' then
    EdtSizeMarc.Visible:=true
else
    EdtSizeMarc.Visible:=false;


If Ordem <> '' then
    EdtOrdemMarc.Visible:=true
else
    EdtOrdemMarc.Visible:=false;
end;


procedure TFrmLayoutOutput.EdtSizeEndExit(Sender: TObject);
var Zero :string;
begin
   Zero := InputBox('ENDERE�O', 'Completar ENDERE�O com zeros � esquerda?','S');
   If (Zero = 'S') or (Zero = 's') then
    begin
     Label39.Caption := 'T';
    end;


end;

procedure TFrmLayoutOutput.CheckLoteClick(Sender: TObject);
var tamanho, ordem, Zero: string;
begin
Tamanho := InputBox('Lote', 'Digite o tamanho do campo LOTE (em caracteres)','');
Ordem := InputBox('Lote', 'Digite a ordem do campo LOTE','');
Zero := InputBox('Lote', 'Completar LOTE com zeros � esquerda?','S');
EdtSizeLote.Text:=Tamanho;
EdtOrdemLote.Text:=Ordem;
If (Zero = 'S') or (Zero = 's') then
  begin
    Label42.caption := 'T';
  end
else
  begin
    label42.caption:='F';
  end;


If Tamanho <> '' then
    EdtSizeLote.Visible:=true
else
    EdtSizeLote.Visible:=false;


If Ordem <> '' then
    EdtOrdemLote.Visible:=true
else
    EdtOrdemLote.Visible:=false;
end;


procedure TFrmLayoutOutput.CheckDescricaoClick(Sender: TObject);
var tamanho, ordem: string;
begin
Tamanho := InputBox('Descri��o', 'Digite o tamanho do campo DESCRI��O (em caracteres)','');
Ordem := InputBox('Descri��o', 'Digite a ordem do campo DESCRI��O','');
EdtSizeDescricao.Text:=Tamanho;
EdtOrdemDescricao.Text:=Ordem;

If Tamanho <> '' then
    EdtSizeDescricao.Visible:=true
else
    EdtSizeDescricao.Visible:=false;


If Ordem <> '' then
    EdtOrdemDescricao.Visible:=true
else
    EdtOrdemDescricao.Visible:=false;

end;

procedure TFrmLayoutOutput.CmbDataClick(Sender: TObject);
var Form1 : string;
begin
  DM.IBInventario.Open;
  DM.IBLayoutOutput.Open;
  DM.IBLayoutOutput.Edit;

   if CmbData.Text = 'T' then
    begin
      Dm.IBLayoutOutputDATA_FILE.Value := 'T';
      Form1 := UpperCase(InputBox('Formato Data', 'Digite o modelo do formato','Ex.: "DDMMAAAA" '));
       if (Form1 <> 'MMDDAA') and (Form1 <> 'DDMMAA') and (Form1 <> 'DDMMAAAA') and (Form1 <> 'MMDDAAAA') and
          (Form1 <> 'MM') and (Form1 <> 'DD') and (Form1 <> 'AA') and (Form1 <> 'AAAA') then
       begin
        Application.MessageBox('Formato inv�lido!','Mensagem do Sistema', MB_ICONERROR + MB_OK);
       end

    end
   else
    begin
      Dm.IBLayoutOutputDATA_FILE.Value := 'F';
    end;
try
If (Form1 = 'MMDDAA') or (Form1 = 'mmddaa') then
  begin
    Label46.caption := 'T';
    ResultadoData := Copy(DM.IBInventarioINV_DATAINV.AsString,4,2)+ Copy(DM.IBInventarioINV_DATAINV.AsString,1,2)+ Copy(DM.IBInventarioINV_DATAINV.AsString,9,2) ;
    DM.IBLayoutOutputDATA_FORMAT.Value := ResultadoData;
  end;

If (Form1 = 'DDMMAA') or (Form1 = 'ddmmaa') then
  begin
    Label47.caption := 'T';
    ResultadoData := Copy(DM.IBInventarioINV_DATAINV.AsString,1,2)+ Copy(DM.IBInventarioINV_DATAINV.AsString,4,2)+ Copy(DM.IBInventarioINV_DATAINV.AsString,9,2);
    DM.IBLayoutOutputDATA_FORMAT.Value := ResultadoData;
  end;

If (Form1 = 'DDMMAAAA') or (Form1 = 'ddmmaaaa') then
  begin
    Label48.caption := 'T';
    ResultadoData := Copy(DM.IBInventarioINV_DATAINV.AsString,1,2)+ Copy(DM.IBInventarioINV_DATAINV.AsString,4,2)+ Copy(DM.IBInventarioINV_DATAINV.AsString,7,4) ;
    DM.IBLayoutOutputDATA_FORMAT.Value := ResultadoData;
  end;

If (Form1 = 'MMDDAAAA') or (Form1 = 'mmddaaaa') then
  begin
     Label49.Caption := 'T';
     ResultadoData := Copy(DM.IBInventarioINV_DATAINV.AsString,4,2)+ Copy(DM.IBInventarioINV_DATAINV.AsString,1,2)+ Copy(DM.IBInventarioINV_DATAINV.AsString,7,4);
     DM.IBLayoutOutputDATA_FORMAT.Value := ResultadoData;
  end;

If (Form1 = 'MM') or (Form1 = 'mm') then
  begin
     Label50.Caption := 'T';
     ResultadoData := Copy(DM.IBInventarioINV_DATAINV.AsString,4,2);
     DM.IBLayoutOutputDATA_FORMAT.Value := ResultadoData;
  end;

If (Form1 = 'DD') or (Form1 = 'dd') then
  begin
    Label51.Caption := 'T';
    ResultadoData := Copy(DM.IBInventarioINV_DATAINV.AsString,1,2);
    DM.IBLayoutOutputDATA_FORMAT.Value := ResultadoData;
  end;

If (Form1 = 'AA') or (Form1 = 'aa') then
  begin
    Label52.Caption := 'T';
    ResultadoData := Copy(DM.IBInventarioINV_DATAINV.AsString,9,2);
    DM.IBLayoutOutputDATA_FORMAT.Value := ResultadoData;
  end;

If (Form1 = 'AAAA') or (Form1 = 'aaaa') then
  begin
    Label53.Caption := 'T';
    ResultadoData := Copy(DM.IBInventarioINV_DATAINV.AsString,7,4);
    DM.IBLayoutOutputDATA_FORMAT.Value := ResultadoData;
  end;

except
  ShowMessage('Erro');
  Dm.Transacao.RollbackRetaining;
end;
end;
procedure TFrmLayoutOutput.CmbSecaoBreakFileClick(Sender: TObject);
var geral: string;
begin

 If CmbSecaoBreakFile.Text = 'T' then
  begin
    geral := InputBox('Quebrar Arquivo', 'Deseja gerar o 3� arquivo final GERAL?','S');
    If (geral = 'S') or (geral = 's') then
      begin
        Label54.caption := 'T';
      end
    else
      begin
        Label54.caption:='F';
      end;

  end;
end;
end.
