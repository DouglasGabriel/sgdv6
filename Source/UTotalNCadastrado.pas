unit UTotalNCadastrado;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.StdCtrls, Vcl.Buttons,
  sLabel;

type
  TFrmNCadastrado = class(TForm)
    EdNCadastrado: TEdit;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    StatusBar1: TStatusBar;
    sLabelFX1: TsLabelFX;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure EdNCadastradoKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmNCadastrado: TFrmNCadastrado;

implementation

{$R *.dfm}

Uses Udm, Udm2;

procedure TFrmNCadastrado.BitBtn1Click(Sender: TObject);
var
  ncad, total : Real;
begin
  ncad := StrToFloat(EdNCadastrado.Text);
  DM.RvPrjWis.SetParam('NCad',FormatFloat('##,###0.000', nCad)) ;

  Total := Dm.QGeraRelFinalLojaQTD_LOJA.Value + Dm.QGeraRelFinalDepositoQTD_DEP.Value + StrToFloat(EdNCadastrado.Text);
  DM.RvPrjWis.SetParam('TotBsi',FormatFloat('##,###0.000', Total)) ;

  FrmNCadastrado.Close;

//  FreeAndNil(FrmNCadastrado);
end;

procedure TFrmNCadastrado.BitBtn2Click(Sender: TObject);
begin
  //Exit;
  FrmNCadastrado.Close;
end;

procedure TFrmNCadastrado.EdNCadastradoKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',',',#8]) then
    key := #0;

  if key in [',','.'] then
    key := FormatSettings.DecimalSeparator;

  if key = FormatSettings.DecimalSeparator then
    if pos(key,TEdit(Sender).Text) <> 0 then
      key := #0;

end;

procedure TFrmNCadastrado.FormShow(Sender: TObject);
begin
  EdNCadastrado.Text := '0,000';
  EdNCadastrado.SetFocus;
end;

end.
