unit USecoes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, Menus, ExtCtrls, Grids, DBGrids, StdCtrls, Buttons,
  Mask, DBCtrls, ShellApi, TeeProcs, TeEngine, Chart, DbChart, Series,
  ImgList, AppEvnts, MidasLib, sSkinManager, sEdit, RpDefine, RpRender,
  RpRenderPDF, RpRenderText, sBitBtn;


type
  TFrmSecoes = class(TForm)
    Panel2: TPanel;
    Label1: TLabel;
    Label9: TLabel;
    Panel3: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    DBText1: TDBText;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    sBitBtn1: TsBitBtn;
    sBitBtn2: TsBitBtn;
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Panel4: TPanel;
    Label5: TLabel;
    Label6: TLabel;
    Panel6: TPanel;
    DBGrid1: TDBGrid;
    sBitBtn4: TsBitBtn;
    Panel5: TPanel;
    RadioGroup1: TRadioGroup;
    procedure sBitBtn4Click(Sender: TObject);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure RadioGroup1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmSecoes: TFrmSecoes;

implementation

{$R *.dfm}

uses UDm, UFuncProc, ULogin, uHorario, uFinalizaInvent, DB, UPesquisa,
  USelecionaSecao, IBCustomDataSet, Math, UAguarde, UDivergencia, UCadInv,
  UFrmProdutividade, UFrmUpdate, Udm2, UBreak;
procedure TFrmSecoes.sBitBtn4Click(Sender: TObject);
begin

Close;
end;

procedure TFrmSecoes.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
Secao, Status, TransID, dup : String;
Id: Integer;
SecaoNova, SecaoAntiga, Matricula: String;
SecaoID, SecTransID, NumCaracteres, secini, secfin, IDSECAO : integer;
volta : boolean;
Begin
  //Pegar valor do ID da Linha
  IDSECAO := DM.IBSecaoSEC_ID.value;

  //Alterar Se��o com tecla de sobe e desce
  If ((Key = VK_DOWN) or (Key = VK_UP))
  and (Dm.DsSecao.State = DsEdit)
  and (Dm.IBSecaoSEC_STATUS.Value = '.')
  and (DM.IBSecaoSTATUS.Value <> 'ABE') then
  Begin
    key := 13;
  End;





  //###N�o deixa apagar registro do DbGrid, e s� coloca o STATUS como DEL###
  If (Shift = [ssCtrl]) and (Key = 46) Then
  Begin
    KEY := 0;
    UfuncProc.sec := true;
    Secao := Dm.IBSecaoSEC_SECAO.Value;
    TransID := inttostr(DM.IBSecaoTRA_ID.value);
    Status := Dm.IBSecaoSEC_ABERTA.Value;
    dup := DM.IBSecaoSEC_DUPLI.Value;

    //###Caso queira tirar de deletado
    If Dm.IBSecaoSEC_STATUS.Value = 'DEL' Then
    Begin
      DM.IBSecao.Edit;
      DM.IBSecaoSEC_STATUS.Value := '.';
      DM.IBSecao.Append;
      DM.IBSecao.Post;
      Dm.Transacao.CommitRetaining;
      //Caso for aceitar uma deletada que era duplicada
      If (DM.IBSecaoSEC_DUPLI.value = 'T') or (DM.IBSecaoSEC_DUPLI.value = '*') then  // or (DM.IBSecaoSEC_DUPLI.value = '*')
      Begin
        //Duplicidade(Secao);
        ColocaSecaoDupli(DM.IBSecaoSEC_ID.Value,Dm.IBSecaoSEC_SECAO.Value);
      End;
      // Tira as contagens de Deletada, caso exita contagens, pois a se��o pode ser uma Se��o Aberta sem contagens
      if  Dm.IBContagens.locate('TRA_ID;SEC_SECAO',VarArrayOf([TransID, Secao]),[Lopartialkey,locaseinsensitive]) = True then
      Begin
        While not (Dm.IBContagens.Eof) and (inttostr(Dm.IBContagensTRA_ID.Value) = TransID) and (Dm.IBContagensSEC_SECAO.Value = Secao)  Do
        Begin
          Dm.IBContagens.Edit;
          Dm.IBContagensCON_STATUS.Value := '.';
          Dm.IBContagens.Post;
          Dm.Transacao.CommitRetaining;
          Dm.IBContagens.Next;
        End;
        Dm.Transacao.CommitRetaining;
      End;
      if (TransID <> '0') then
        GravaAltDbGridSecao(IDSECAO, strtoint(TransID), Dm.IBSecaoHOR_MATRICULA.Value, Secao, Secao);
    End
    else if Dm.IBSecaoSEC_STATUS.Value = '.' then //###Caso queira deletar
    Begin
      DM.IBSecao.Edit;
      DM.IBSecaoSEC_STATUS.Value := 'DEL';

      DM.IBSEcao1.Close;
      DM.IBSEcao1.SelectSQL.Clear;
      DM.IBSEcao1.SelectSQL.Add('select count(*) as qntd from secao where sec_secao = ' + Secao + ' and sec_status = "."');
      DM.IBSEcao1.Open;

      if (TransID <> '0') and (DM.IBSEcao1.FieldByName('qntd').AsInteger = 1) and (Dm.IBSecaoSEC_INESPERADA.Value <> 'T') then
      begin
        DM.IBSecao.Append;
        DM.IBSecaoSEC_SECAO.Value := Secao;
        DM.IBSecaoSEC_ABERTA.Value := 'T';
        Dm.IBSecaoSEC_STATUS.Value := '.';
        Dm.IBSecaoSEC_INESPERADA.Value := 'F';
        Dm.IBSecaoSEC_DUPLI.Value := 'F';
      end;
      //endi
      DM.IBSecao.Post;
      Dm.Transacao.CommitRetaining;

      If DM.IBSecaoSEC_DUPLI.value = 'T' then
      Begin
        Duplicidade(Secao);
      End;
      TiraSecaoDupli(DM.IBSecaoSEC_ID.value,Dm.IBSecaoSEC_SECAO.value);
      // Coloca as contagens em Deletada, caso exita contagens, pois a se��o pode ser uma Se��o Aberta sem contagens
      if  Dm.IBContagens.locate('TRA_ID;SEC_SECAO',VarArrayOf([TransID, Secao]),[Lopartialkey,locaseinsensitive]) = True then
      Begin
        While not (Dm.IBContagens.Eof) and (inttostr(Dm.IBContagensTRA_ID.Value) = TransID) and (Dm.IBContagensSEC_SECAO.Value = Secao)  Do
        Begin
          Dm.IBContagens.Edit;
          Dm.IBContagensCON_STATUS.Value := 'DEL';
          Dm.IBContagens.Post;
          Dm.Transacao.CommitRetaining;
          Dm.IBContagens.Next;
        End;
        Dm.Transacao.CommitRetaining;
      End;
    End;

    UFuncProc.sec := false;
    duplicidade(Dm.IBSecaoSEC_SECAO.Value);
    PosicionaSecao(IDSECAO);

    Dm.QGeraRelFinal.Close;
    Dm.QGeraRelFinal.Open;
    
Dm2.QMap_total.Close;
Dm2.QMap_total.Open;
Dm2.QMap_totalGeral.Close;
Dm2.QMap_totalGeral.Open;
  End;

  //###Imprime relat�rio de auditoria###
  if (key = vk_F1) and (Dm.IBSecaoSEC_STATUS.Value = '.') then
   Begin
    RelAudit(Dm.IBSecaoSEC_ID.value,Dm.IBSecaoSEC_SECAO.Value, Dm.IBSecaoTRA_ID.value);
   End;

  //Chama fun��o aceita duplicidade
  if (key = vk_F4) and (Dm.IBSecaoSEC_DUPLI.Value = 'T')
                 and (Dm.IBSecaoSEC_INESPERADA.Value = 'F') then
  Begin
    Dm.SpAceitaDupli.Close;
    Dm.SpAceitaDupli.ParamByName('SEC_SECAO').Value := Dm.IBSecaoSEC_SECAO.Value;
    Dm.SpAceitaDupli.ExecProc;
    Dm.SpAceitaDupli.Close;
    Dm.Transacao.CommitRetaining;
    PosicionaSecao(IDSECAO);
    //AceitaDuplicidade(Dm.IBSecaoSEC_ID.Value,Dm.IBSecaoSec_Secao.value);
    Dm2.QMap_total.Close;
    Dm2.QMap_total.Open;
    Dm2.QMap_totalGeral.Close;
    Dm2.QMap_totalGeral.Open;
  End;
  //AceitaDuplicidade(inttostr(Dm.IBSecaoTra_ID.value), Dm.IBSecaoSec_Secao.value);



 //Inserta Se��o
  if (key = vk_F5) and (Dm.IBSecaoSEC_INESPERADA.Value = 'T') then
  Begin

    Id := Dm.IBSecaoSEC_ID.Value;
    secini := Dm.IBSecaoSEC_SECAO.AsInteger;
    secfin := Dm.IBSecaoSEC_SECAO.AsInteger;
   //verifica se existem mapeamentos cadastrados que sejam subsequentes desta se��o
    DM.IBMapeamento.Close;
    Dm.IBMapeamento.ParamByName('secini').Value := '0000';
    Dm.IBMapeamento.ParamByName('secfin').Value := secini - 1;
    DM.IBMapeamento.Open;
    Dm.IBMapeamento.Last;
    volta := true;

    while not(Dm.IBMapeamento.Bof) and volta do
    begin
      if ((secini - 1) = DM.IBMapeamentoMAP_SECFIN.AsInteger) then
         begin
          secini := DM.IBMapeamentoMAP_SECINI.AsInteger;
      end
      else
        volta := false;
      Dm.IBMapeamento.Prior;
    end;
    //endwh

    DM.IBMapeamento.Close;
    If (SecFin < 9999) then
    begin
    Dm.IBMapeamento.ParamByName('secini').Value := secfin+1;
    end
    else
    Dm.IBMapeamento.ParamByName('secini').Value := secfin;
    Dm.IBMapeamento.ParamByName('secfin').Value := '9999';
    DM.IBMapeamento.Open;
    Dm.IBMapeamento.First;
    volta := true;

    while not(Dm.IBMapeamento.Eof) and volta do
    begin
      if ((secfin + 1) = DM.IBMapeamentoMAP_SECINI.AsInteger) then
      begin
        secfin := DM.IBMapeamentoMAP_SECFIN.AsInteger;
      end
      else
        volta := false;

      Dm2.QMap_total.Close;
      Dm2.QMap_total.Open;
      Dm2.QMap_totalGeral.Close;
      Dm2.QMap_totalGeral.Open;
      end;
    //endwh
    CadastraMapeamento(IntToStr(SecIni),IntToStr(SecFin), '');
    Dm.IBSecao.locate('SEC_ID',Id,[Lopartialkey,locaseinsensitive]);
    Duplicidade(Secao);

 end;


{
//Inserta Se��o
if (key = vk_F5) and (Dm.IBSecaoSEC_INESPERADA.Value = 'T') then
  Begin
    Id := Dm.IBSecaoSEC_ID.Value;
    CadastraMapeamento(Dm.IBSecaoSEC_SECAO.Value,Dm.IBSecaoSEC_SECAO.Value, '');
    Dm.IBSecao.locate('SEC_ID',Id,[Lopartialkey,locaseinsensitive]);
  end;
}
      Dm2.QMap_total.Close;
      Dm2.QMap_total.Open;
      Dm2.QMap_totalGeral.Close;
      Dm2.QMap_totalGeral.Open;



end;

procedure TFrmSecoes.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
if State = [] then
  begin
    if Dm.IBSecao.RecNo mod 2 = 1 then
      DBGrid1.Canvas.Brush.Color := clSilver
    else
      DBGrid1.Canvas.Brush.Color := clWhite;
  end;
  DBGrid1.DefaultDrawColumnCell(Rect, DataCol, Column, State);
end;

procedure TFrmSecoes.RadioGroup1Click(Sender: TObject);
var StrAberta: string;
begin
// Filtrando Se��es Abertas
If RadioGroup1.ItemIndex = 0 then
  begin
    Dm.IBSecao.Filtered := False;
    Dm.IBSecao.Filter := 'SEC_ABERTA = '+QuotedStr('T')+' AND SEC_INESPERADA = '+QuotedStr('F')+' AND SEC_DUPLI = '+QuotedStr('F')+' AND SEC_STATUS <> '+QuotedStr('DEL')+' ';
    Dm.IBSecao.Filtered := True;
  end
else
  If RadioGroup1.ItemIndex = 1 then
    begin
      Dm.IBSecao.Filtered := False;
      Dm.IBSecao.Filter := 'SEC_ABERTA = '+QuotedStr('F')+' AND SEC_INESPERADA = '+QuotedStr('T')+' AND SEC_DUPLI = '+QuotedStr('F')+' AND SEC_STATUS <> '+QuotedStr('DEL')+' ';
      Dm.IBSecao.Filtered := True;
    End
else
    If RadioGroup1.ItemIndex = 2 then
    begin
      Dm.IBSecao.Filtered := False;
      Dm.IBSecao.Filter := 'SEC_ABERTA = '+QuotedStr('F')+' AND SEC_INESPERADA = '+QuotedStr('F')+' AND SEC_DUPLI = '+QuotedStr('T')+' AND SEC_STATUS <> '+QuotedStr('DEL')+' ';
      Dm.IBSecao.Filtered := True;
    End;


{If RadioGroup1.ItemIndex = 0 then
  begin
    StrAberta := 'select s.controle, s.SEC_SECAO, s.SEC_ABERTA, s.SEC_ID, s.SEC_DUPLI, ';
    StrAberta := StrAberta + 's.SEC_AUDITADA, s.SEC_INESPERADA, s.TRA_ID, s.HOR_MATRICULA, ';
    StrAberta := StrAberta + 's.SEC_STATUS, (CASE when (SEC_STATUS = ''DEL'') then ';
    StrAberta := StrAberta + ' ''DEL'' when (SEC_ABERTA = ''T'') AND (SEC_INESPERADA = ''F'') ';
    StrAberta := StrAberta + ' AND (SEC_DUPLI = ''F'') AND (SEC_STATUS = ''.'')   then ''ABE'' ';
    StrAberta := StrAberta + 'when (SEC_ABERTA = ''F'') AND (SEC_INESPERADA = ''T'') AND ((SEC_DUPLI = ''T'') ';
    StrAberta := StrAberta + ' or (SEC_DUPLI = ''F'') or (SEC_DUPLI = ''*'')) AND (SEC_STATUS = ''.'')   then ';
    StrAberta := StrAberta + ' ''INP'' when (SEC_DUPLI = ''T'') AND (SEC_INESPERADA = ''F'') AND (SEC_STATUS = ''.'') then ';
    StrAberta := StrAberta + ' ''DUP'' when (SEC_DUPLI = ''*'') AND (SEC_INESPERADA = ''F'') AND (SEC_STATUS = ''.'') then ';
    StrAberta := StrAberta + ' ''*'' end) as STATUS, sum(c.CON_QUANTIDADE) as qtd_secao from SECAO s ';
    StrAberta := StrAberta + ' left join contagens c on (c.tra_id = s.tra_id) and (c.sec_secao = s.sec_secao) and (c.con_status = ''.'') ';
    StrAberta := StrAberta + ' where (s.sec_aberta = ''T'') and (s.sec_inesperada = ''F'') and (sec_dupli = ''F'') and (sec_status = ''.'') ';
    StrAberta := StrAberta + ' group by s.SEC_SECAO, s.SEC_ABERTA, s.SEC_ID, s.SEC_DUPLI, ';
    StrAberta := StrAberta + ' s.SEC_INESPERADA, s.TRA_ID, s.HOR_MATRICULA, ';
    StrAberta := StrAberta + ' s.SEC_STATUS, s.SEC_AUDITADA, s.controle ';
    StrAberta := StrAberta + ' order by SEC_status ';


end;
}
end;

procedure TFrmSecoes.FormClose(Sender: TObject; var Action: TCloseAction);
begin
Dm.IBSecao.Filtered := False;
end;

procedure TFrmSecoes.FormShow(Sender: TObject);
begin
    Dm.IBSecao.Filtered := False;
    Dm.IBSecao.Filter := 'SEC_ABERTA = '+QuotedStr('T')+' AND SEC_INESPERADA = '+QuotedStr('F')+' AND SEC_DUPLI = '+QuotedStr('F')+' AND SEC_STATUS <> '+QuotedStr('DEL')+' ';
    Dm.IBSecao.Filtered := True;
end;

end.
