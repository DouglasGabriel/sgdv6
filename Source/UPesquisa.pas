unit UPesquisa;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls, Db, Buttons, DBCtrls, sLabel;

type
  TFrmLocalizar = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    StatusBar1: TStatusBar;
    Panel3: TPanel;
    Label2: TLabel;
    ComboBox1: TComboBox;
    Edit1: TEdit;
    Label3: TLabel;
    BitBtn1: TBitBtn;
    CmbStatus: TComboBox;
    sLabelFX1: TsLabelFX;
    procedure FormShow(Sender: TObject);
    procedure ComboBox1Exit(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ComboBox1Click(Sender: TObject);
    procedure CmbStatusExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmLocalizar: TFrmLocalizar;
  Item_con, Item_sec :Integer;
  Texto_con, Texto_sec :String;
  PosicaoSearch: integer;
implementation

uses UInventario, UDm, UFuncProc;

{$R *.dfm}

procedure TFrmLocalizar.FormShow(Sender: TObject);
begin
if Cod = 1 then
 Begin
  ComboBox1.Items.Add('C�digo Interno');
  ComboBox1.Items.Add('C�digo Ean');
  ComboBox1.Items.Add('Se��o');
  ComboBox1.Items.Add('Lote');
  ComboBox1.Items.Add('Refer�ncia');
  ComboBox1.ItemIndex := PosicaoSearch;
  Edit1.SetFocus;
  Edit1.Text:=LastSearch;
 End;
if Cod = 2 then
 Begin
  ComboBox1.Items.Add('Se��o');
  ComboBox1.Items.Add('Status');
  ComboBox1.Items.Add('Matricula');
  ComboBox1.ItemIndex := 0;
  Edit1.SetFocus;
  Edit1.Text:=LastSearch;
 End;
end;

procedure TFrmLocalizar.ComboBox1Exit(Sender: TObject);
begin
  //Edit1.SetFocus;

end;

procedure TFrmLocalizar.BitBtn1Click(Sender: TObject);
begin
  If cod = 1 then
  begin
     If Combobox1.ItemIndex = 0 then
      begin
        Dm.IBContagens.Close;
        Dm.IBContagens.SelectSQL.Clear;
        Dm.IBContagens.SelectSQL.Add('select c.con_ean,c.con_id,c.are_area,c.con_data,c.con_hora,c.con_quantidade, c.con_quantidadeoriginal,c.hor_matricula, ');
        Dm.IBContagens.SelectSQL.Add('c.sec_secao,c.tra_id,c.con_status,c.rup_deposito,c.rup_loja,c.con_deposito,c.deposito,c.loja,c.con_quantdesc, c.con_bsi, c.con_cliente, ');
        Dm.IBContagens.SelectSQL.Add('c.num_cont,c.tonalidade,c.con_cadcodint,c.con_endereco,( select first 1 ca.cad_desc from cadastro ca where c.con_ean = ca.cad_ean) as cad_desc ');
        Dm.IBContagens.SelectSQL.Add('FROM CONTAGENS c ');
        Dm.IBContagens.SelectSQL.Add('where c.con_cadcodint like :pcon_cadcodint or c.con_ean like :pcon_ean or c.tonalidade like :ptonalidade ');
        Dm.IBContagens.SelectSQL.Add('or c.sec_secao like :psec_secao or c.con_id  like :pcon_id ORDER BY sec_secao, con_id ');
        DM.IBContagens.ParamByName('pcon_cadcodint').AsString   := Edit1.Text;
        DM.IBContagens.ParamByName('pcon_ean').Value            := null;
        DM.IBContagens.ParamByName('psec_secao').Value          := null;
        DM.IBContagens.ParamByName('ptonalidade').Value         := null;
        DM.IBContagens.ParamByName('pcon_id').Value             := null;
        dm.IBContagens.Open;
        if dm.IBContagens.RecordCount > 0 then
        begin
         dm.IBContagens.Open;
        end
        else
        begin
         Application.MessageBox('C�digo interno n�o encontrado', 'Mensagem do Sistema', mb_iconerror + mb_ok);
         DM.IBContagens.Close;
         Dm.IBContagens.SelectSQL.Clear;
         Dm.IBContagens.SelectSQL.Add('select c.con_ean,c.con_id,c.are_area,c.con_data,c.con_hora,c.con_quantidade, c.con_quantidadeoriginal,c.hor_matricula, ');
         Dm.IBContagens.SelectSQL.Add('c.sec_secao,c.tra_id,c.con_status,c.rup_deposito,c.rup_loja,c.con_deposito,c.deposito,c.loja,c.con_quantdesc, c.con_bsi, c.con_cliente, ');
         Dm.IBContagens.SelectSQL.Add('c.num_cont,c.tonalidade,c.con_cadcodint,c.con_endereco,( select first 1 ca.cad_desc from cadastro ca where c.con_ean = ca.cad_ean) as cad_desc ');
         Dm.IBContagens.SelectSQL.Add('FROM CONTAGENS c ORDER BY SEC_SECAO, CON_ID ');
         DM.IBContagens.Open;
        end;
      end;

      If Combobox1.ItemIndex = 1 then
      begin
        Dm.IBContagens.Close;
        Dm.IBContagens.SelectSQL.Clear;
        Dm.IBContagens.SelectSQL.Add('select c.con_ean,c.con_id,c.are_area,c.con_data,c.con_hora,c.con_quantidade, c.con_quantidadeoriginal,c.hor_matricula, ');
        Dm.IBContagens.SelectSQL.Add('c.sec_secao,c.tra_id,c.con_status,c.rup_deposito,c.rup_loja,c.con_deposito,c.deposito,c.loja,c.con_quantdesc, c.con_bsi, c.con_cliente, ');
        Dm.IBContagens.SelectSQL.Add('c.num_cont,c.tonalidade,c.con_cadcodint,c.con_endereco,( select first 1 ca.cad_desc from cadastro ca where c.con_ean = ca.cad_ean) as cad_desc ');
        Dm.IBContagens.SelectSQL.Add('FROM CONTAGENS c ');
        Dm.IBContagens.SelectSQL.Add('where c.con_cadcodint like :pcon_cadcodint or c.con_ean like :pcon_ean or c.tonalidade like :ptonalidade ');
        Dm.IBContagens.SelectSQL.Add('or c.sec_secao like :psec_secao or c.con_id  like :pcon_id ORDER BY sec_secao, con_id ');
        DM.IBContagens.ParamByName('pcon_cadcodint').Value      := null;
        DM.IBContagens.ParamByName('pcon_ean').AsString         := Edit1.Text;
        DM.IBContagens.ParamByName('psec_secao').Value          := null;
        DM.IBContagens.ParamByName('ptonalidade').Value         := null;
        DM.IBContagens.ParamByName('pcon_id').Value             := null;
        dm.IBContagens.Open;

        if dm.IBContagens.RecordCount > 0 then
        begin
         dm.IBContagens.Open;
        end
        else
        begin
         Application.MessageBox('C�digo Ean n�o encontrado', 'Mensagem do Sistema', mb_iconerror + mb_ok);
         DM.IBContagens.Close;
         Dm.IBContagens.SelectSQL.Clear;
         Dm.IBContagens.SelectSQL.Add('select c.con_ean,c.con_id,c.are_area,c.con_data,c.con_hora,c.con_quantidade, c.con_quantidadeoriginal,c.hor_matricula, ');
         Dm.IBContagens.SelectSQL.Add('c.sec_secao,c.tra_id,c.con_status,c.rup_deposito,c.rup_loja,c.con_deposito,c.deposito,c.loja,c.con_quantdesc, c.con_bsi, c.con_cliente, ');
         Dm.IBContagens.SelectSQL.Add('c.num_cont,c.tonalidade,c.con_cadcodint,c.con_endereco,( select first 1 ca.cad_desc from cadastro ca where c.con_ean = ca.cad_ean) as cad_desc ');
         Dm.IBContagens.SelectSQL.Add('FROM CONTAGENS c ORDER BY SEC_SECAO, CON_ID ');
         DM.IBContagens.Open;
        end;
      end;

      If Combobox1.ItemIndex = 2 then
      begin
        Dm.IBContagens.Close;
        Dm.IBContagens.SelectSQL.Clear;
        Dm.IBContagens.SelectSQL.Add('select c.con_ean,c.con_id,c.are_area,c.con_data,c.con_hora,c.con_quantidade, c.con_quantidadeoriginal,c.hor_matricula, ');
        Dm.IBContagens.SelectSQL.Add('c.sec_secao,c.tra_id,c.con_status,c.rup_deposito,c.rup_loja,c.con_deposito,c.deposito,c.loja,c.con_quantdesc, c.con_bsi, c.con_cliente, ');
        Dm.IBContagens.SelectSQL.Add('c.num_cont,c.tonalidade,c.con_cadcodint,c.con_endereco,( select first 1 ca.cad_desc from cadastro ca where c.con_ean = ca.cad_ean) as cad_desc ');
        Dm.IBContagens.SelectSQL.Add('FROM CONTAGENS c ');
        Dm.IBContagens.SelectSQL.Add('where c.con_cadcodint like :pcon_cadcodint or c.con_ean like :pcon_ean or c.tonalidade like :ptonalidade ');
        Dm.IBContagens.SelectSQL.Add('or c.sec_secao like :psec_secao or c.con_id  like :pcon_id ORDER BY sec_secao, con_id ');
        DM.IBContagens.ParamByName('pcon_cadcodint').Value      := null;
        DM.IBContagens.ParamByName('pcon_ean').Value            := null;
        DM.IBContagens.ParamByName('psec_secao').Value          := Edit1.Text;
        DM.IBContagens.ParamByName('ptonalidade').Value         := null;
        DM.IBContagens.ParamByName('pcon_id').Value             := null;
        dm.IBContagens.Open;

        if dm.IBContagens.RecordCount > 0 then
        begin
         dm.IBContagens.Open;
        end
        else
        begin
         Application.MessageBox('Se��o n�o encontrada', 'Mensagem do Sistema', mb_iconerror + mb_ok);
         DM.IBContagens.Close;
         Dm.IBContagens.SelectSQL.Clear;
         Dm.IBContagens.SelectSQL.Add('select c.con_ean,c.con_id,c.are_area,c.con_data,c.con_hora,c.con_quantidade, c.con_quantidadeoriginal,c.hor_matricula, ');
         Dm.IBContagens.SelectSQL.Add('c.sec_secao,c.tra_id,c.con_status,c.rup_deposito,c.rup_loja,c.con_deposito,c.deposito,c.loja,c.con_quantdesc, c.con_bsi, c.con_cliente, ');
         Dm.IBContagens.SelectSQL.Add('c.num_cont,c.tonalidade,c.con_cadcodint,c.con_endereco,( select first 1 ca.cad_desc from cadastro ca where c.con_ean = ca.cad_ean) as cad_desc ');
         Dm.IBContagens.SelectSQL.Add('FROM CONTAGENS c ORDER BY SEC_SECAO, CON_ID ');
         DM.IBContagens.Open;
        end;
      end;

      If Combobox1.ItemIndex = 3 then
      begin
        Dm.IBContagens.Close;
        Dm.IBContagens.SelectSQL.Clear;
        Dm.IBContagens.SelectSQL.Add('select c.con_ean,c.con_id,c.are_area,c.con_data,c.con_hora,c.con_quantidade, c.con_quantidadeoriginal,c.hor_matricula, ');
        Dm.IBContagens.SelectSQL.Add('c.sec_secao,c.tra_id,c.con_status,c.rup_deposito,c.rup_loja,c.con_deposito,c.deposito,c.loja,c.con_quantdesc, c.con_bsi, c.con_cliente, ');
        Dm.IBContagens.SelectSQL.Add('c.num_cont,c.tonalidade,c.con_cadcodint,c.con_endereco,( select first 1 ca.cad_desc from cadastro ca where c.con_ean = ca.cad_ean) as cad_desc ');
        Dm.IBContagens.SelectSQL.Add('FROM CONTAGENS c ');
        Dm.IBContagens.SelectSQL.Add('where c.con_cadcodint like :pcon_cadcodint or c.con_ean like :pcon_ean or c.tonalidade like :ptonalidade ');
        Dm.IBContagens.SelectSQL.Add('or c.sec_secao like :psec_secao or c.con_id  like :pcon_id ORDER BY sec_secao, con_id ');
        DM.IBContagens.ParamByName('pcon_cadcodint').Value      := null;
        DM.IBContagens.ParamByName('pcon_ean').Value            := null;
        DM.IBContagens.ParamByName('psec_secao').Value          := null;
        DM.IBContagens.ParamByName('ptonalidade').Value         := Edit1.Text;
        DM.IBContagens.ParamByName('pcon_id').Value             := null;
        dm.IBContagens.Open;

        if dm.IBContagens.RecordCount > 0 then
        begin
         dm.IBContagens.Open;
        end
        else
        begin
         Application.MessageBox('Lote n�o encontrado', 'Mensagem do Sistema', mb_iconerror + mb_ok);
         DM.IBContagens.Close;
         Dm.IBContagens.SelectSQL.Clear;
         Dm.IBContagens.SelectSQL.Add('select c.con_ean,c.con_id,c.are_area,c.con_data,c.con_hora,c.con_quantidade, c.con_quantidadeoriginal,c.hor_matricula, ');
         Dm.IBContagens.SelectSQL.Add('c.sec_secao,c.tra_id,c.con_status,c.rup_deposito,c.rup_loja,c.con_deposito,c.deposito,c.loja,c.con_quantdesc, c.con_bsi, c.con_cliente, ');
         Dm.IBContagens.SelectSQL.Add('c.num_cont,c.tonalidade,c.con_cadcodint,c.con_endereco,( select first 1 ca.cad_desc from cadastro ca where c.con_ean = ca.cad_ean) as cad_desc ');
         Dm.IBContagens.SelectSQL.Add('FROM CONTAGENS c ORDER BY SEC_SECAO, CON_ID ');
         DM.IBContagens.Open;
        end;
      end;

      if ComboBox1.ItemIndex = 4 then
      begin
        Dm.IBContagens.Close;
        Dm.IBContagens.SelectSQL.Clear;
        Dm.IBContagens.SelectSQL.Add('select c.con_ean,c.con_id,c.are_area,c.con_data,c.con_hora,c.con_quantidade, c.con_quantidadeoriginal,c.hor_matricula, ');
        Dm.IBContagens.SelectSQL.Add('c.sec_secao,c.tra_id,c.con_status,c.rup_deposito,c.rup_loja,c.con_deposito,c.deposito,c.loja,c.con_quantdesc, c.con_bsi, c.con_cliente, ');
        Dm.IBContagens.SelectSQL.Add('c.num_cont,c.tonalidade,c.con_cadcodint,c.con_endereco,( select first 1 ca.cad_desc from cadastro ca where c.con_ean = ca.cad_ean) as cad_desc ');
        Dm.IBContagens.SelectSQL.Add('FROM CONTAGENS c ');
        Dm.IBContagens.SelectSQL.Add('where c.con_cadcodint like :pcon_cadcodint or c.con_ean like :pcon_ean or c.tonalidade like :ptonalidade ');
        Dm.IBContagens.SelectSQL.Add('or c.sec_secao like :psec_secao or c.con_id  like :pcon_id ORDER BY sec_secao, con_id ');
        DM.IBContagens.ParamByName('pcon_cadcodint').Value      := null;
        DM.IBContagens.ParamByName('pcon_ean').Value            := null;
        DM.IBContagens.ParamByName('psec_secao').Value          := null;
        DM.IBContagens.ParamByName('ptonalidade').Value         := null;
        DM.IBContagens.ParamByName('pcon_id').AsString          := Edit1.Text;
        dm.IBContagens.Open;
        
        if dm.IBContagens.RecordCount > 0 then
        begin
         dm.IBContagens.Open;
        end
        else
        begin
         Application.MessageBox('Refer�ncia n�o encontrada', 'Mensagem do Sistema', mb_iconerror + mb_ok);
         DM.IBContagens.Close;
         Dm.IBContagens.SelectSQL.Clear;
         Dm.IBContagens.SelectSQL.Add('select c.con_ean,c.con_id,c.are_area,c.con_data,c.con_hora,c.con_quantidade, c.con_quantidadeoriginal,c.hor_matricula, ');
         Dm.IBContagens.SelectSQL.Add('c.sec_secao,c.tra_id,c.con_status,c.rup_deposito,c.rup_loja,c.con_deposito,c.deposito,c.loja,c.con_quantdesc, c.con_bsi, c.con_cliente, ');
         Dm.IBContagens.SelectSQL.Add('c.num_cont,c.tonalidade,c.con_cadcodint,c.con_endereco,( select first 1 ca.cad_desc from cadastro ca where c.con_ean = ca.cad_ean) as cad_desc ');
         Dm.IBContagens.SelectSQL.Add('FROM CONTAGENS c ORDER BY SEC_SECAO, CON_ID ');
         DM.IBContagens.Open;
        end;
      end;

  end;
  try
     If cod = 2 then
       begin

         If Combobox1.ItemIndex = 0 then
          begin
            if not Dm.IBSecao.Eof then
              begin
                Dm.IBSecao.Filter  := 'SEC_SECAO = '+QuotedStr(Edit1.text);
                Dm.IBSecao.Filtered := False;
                Dm.IBSecao.Locate('SEC_SECAO',edit1.Text,[locaseinsensitive]);
                UInventario.LastSearch:=Edit1.text;
                PosicaoSearch:=0;
                FrmInventario.Label21.Caption := 'Filtro Ligado: "SE��O" ';
                FrmInventario.Label21.Visible := True;
                Close;
              end
            else
            Application.MessageBox('SE��O n�o encontrado', 'Mensagem do Sistema', mb_iconerror + mb_ok);
          end;
         If Combobox1.ItemIndex = 1 then
          begin
            if not Dm.IBSecao.Eof then
              begin

                if CmbStatus.Text = 'DEL' then
                 begin
                   Dm.IBSecao.Filter  := 'SEC_STATUS = '+ QuotedStr('DEL');
                 end
                else
                if CmbStatus.Text = 'ABE' then
                 begin
                   Dm.IBSecao.Filter  := 'SEC_ABERTA = '+ QuotedStr('T') + ' AND SEC_STATUS <> '+ QuotedStr('DEL');
                 end
                else
                if CmbStatus.Text = 'DUP' then
                 begin
                   Dm.IBSecao.Filter  := 'SEC_DUPLI = '+ QuotedStr('T');
                 end
                else
                if CmbStatus.Text = 'OK' then
                 begin
                   Dm.IBSecao.Filter  := 'SEC_INESPERADA <> ' + QuotedStr('T') +' AND SEC_ABERTA <> '+ QuotedStr('T') +' AND SEC_DUPLI <> ' + QuotedStr('T') +' AND SEC_STATUS <> ' + QuotedStr('DEL');
                 end
                else;


                Dm.IBSecao.Filtered := True;
                PosicaoSearch:=1;
                UInventario.LastSearch:=Edit1.text;
                FrmInventario.Label21.Caption := 'Filtro Ligado: "STATUS" ';
                FrmInventario.Label21.Visible := True;
                Close;
              end
            else
            Application.MessageBox('STATUS n�o encontrado', 'Mensagem do Sistema', mb_iconerror + mb_ok);
          end;
         If Combobox1.ItemIndex = 2 then
          begin
            if not Dm.IBSecao.Eof then
              begin

                Dm.IBSecao.Filter  := 'HOR_MATRICULA = '+QuotedStr(Edit1.text);

                Dm.IBSecao.Filtered := True;
                UInventario.LastSearch:=Edit1.text;
                PosicaoSearch:=2;
                FrmInventario.Label21.Caption := 'Filtro Ligado: "MATRICULA" ';
                FrmInventario.Label21.Visible := True;

                Close;
              end
            else
            Application.MessageBox('MATRICULA n�o encontrada', 'Mensagem do Sistema', mb_iconerror + mb_ok);
          end;

       end;
  except;
     Application.MessageBox('Erro ao filtrar, Por gentileza escolha um filtro existente!','Mensagem do Sitema', MB_ICONERROR + MB_OK);
  end

end;




procedure TFrmLocalizar.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
            
begin
 if (Key = 13) then
   BitBtn1.Click;
   
end;


procedure TFrmLocalizar.ComboBox1Click(Sender: TObject);
begin

 if Cod = 2 then
  begin

     if ComboBox1.Text = 'Status' then
      begin

        PosicaoSearch := 1;

        if PosicaoSearch = 1 then
         begin
           Edit1.Visible := False;
           Edit1.Enabled := False;
           CmbStatus.Enabled := True;
           CmbStatus.Visible := True;
         end
      end
     else
      begin
        Edit1.Visible := True;
        Edit1.Enabled := True;
        CmbStatus.Enabled := False;
        CmbStatus.Visible := False;
      end;
  end;

end;

procedure TFrmLocalizar.CmbStatusExit(Sender: TObject);
begin
 if CmbStatus.Text = 'INP' then
  begin
   ShowMessage('Selecione um Status Val�do');
   CmbStatus.Text:= ' ';
  end;
end;

end.
