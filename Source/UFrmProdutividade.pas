unit UFrmProdutividade;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, StdCtrls, Buttons, sSpeedButton, sLabel, sEdit, RpDefine, RpRender,
  RpRenderPDF, RpRenderText, DBCtrls;

type
  TFrmProdutividade = class(TForm)
    GroupBox1: TGroupBox;
    GridProdutividade: TStringGrid;
    GroupBox2: TGroupBox;
    sSpeedButton1: TsSpeedButton;
    EdtTotalHoras: TsEdit;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    EdtTotalPecas: TsEdit;
    sLabel3: TsLabel;
    EdtProdutividade: TsEdit;
    GeraRel: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure sSpeedButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure GridProdutividadeDrawCell(Sender: TObject; ACol,
      ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure GeraRelClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmProdutividade: TFrmProdutividade;

implementation
uses UDm, UFuncProc, UHorario, UInventario;

{$R *.dfm}

procedure TFrmProdutividade.FormCreate(Sender: TObject);
var i, total: integer;
BEGIN
// Ajusta o STringGrid
with GridProdutividade do
Begin
Cells[0,0]:= 'Matr�cula';
ColWidths[0] := 90;
Cells[1,0]:= 'Nome';
ColWidths[1] := 200;
Cells[2,0]:= 'Entrada';
ColWidths[2] := 110;
Cells[3,0]:= 'Sa�da';
ColWidths[3] := 110;
Cells[4,0]:= 'Total Horas';
ColWidths[4] := 60;
Cells[5,0]:= 'Quantidade';
ColWidths[5] := 60;
Cells[6,0]:= 'Produtividade';
ColWidths[6] := 60;

//Atribui os valores da tela de hor�rios no StringGrid

Dm.QProd.Close;
Dm.QProd.Open;
Dm.QProd.First;
Dm.QProd.Last;
total:=Dm.QProd.RecordCount;


Dm.QProd.First;


for i:=1 to total do
  begin
        GridProdutividade.Cells[0,i]:=Dm.QProdHOR_MATRICULA.Value;
        GridProdutividade.Cells[1,i]:=Dm.QProdROS_NOME.Value;
        GridProdutividade.Cells[2,i]:=Dm.QProdHOR_ENTRADA.Value;
        GridProdutividade.Cells[3,i]:=Dm.QProdHOR_SAIDA.Value;
        GridProdutividade.Cells[4,i]:=Dm.QProdHOR_TOTALHORA.Value;
        GridProdutividade.Cells[5,i]:=FormatFloat('###,##',Dm.QProdQTDE.Value);
        Dm.QProd.Next;
        GridProdutividade.RowCount:=GridProdutividade.RowCount+1;
  end;

end;



END;
procedure TFrmProdutividade.sSpeedButton1Click(Sender: TObject);
begin
Close;
end;
            
procedure TFrmProdutividade.FormShow(Sender: TObject);
Var Prod: real;
fim :TDateTime;
begin



    If Not Dm.QProd.IsEmpty then
      begin
          DM.IBHorarios.Open;


          DM.Query1.Close;
          DM.Query1.SQL.Clear;
          DM.Query1.SQL.Add('select * from horario where Hor_saida is null ');
          DM.Query1.Open;

          Dm.Query1.First;


        while not DM.Query1.Eof do
         begin

              Dm.IBHorarios.Edit;
              fim := StrToDateTime(FormatDateTime('dd/mm/yyyy',(Now)))+StrToDateTime(FormatDateTime('hh:mm:ss',(Now)));

              DM.qrySQL.Close;
              DM.qrySQL.SQL.Clear;
              DM.qrySQL.SQL.Add('UPDATE HORARIO SET HOR_TOTALHORA = :TOTALHORA ');
              DM.qrySQL.SQL.Add('WHERE (ROS_NOME = :NOME) AND (ROS_MATRICULA = :MATRICULA) AND (HOR_SAIDA IS NULL) ');
              DM.qrySQL.Params.ParamByName('NOME').Value := DM.Query1.FieldByName('ROS_NOME').Value;
              DM.qrySQL.Params.ParamByName('MATRICULA').Value := DM.Query1.FieldByName('ROS_MATRICULA').Value;
              DM.qrySQL.Params.ParamByName('TOTALHORA').Value := CalculaTotaldeHoras(DM.Query1.FieldByName('HOR_ENTRADA').Value, DateTimeToStr(fim));

              DM.qrySQL.ExecSQL;

              Dm.Transacao.CommitRetaining;

              Dm.Query1.Next;
         end;

          DM.IBHorarios.Close;
          DM.IBHorarios.Open;

          //Calcula Produtividade Individual e inserta no StringGrid
          CalculaProdutividadeIndividual;

          //Calcula o total de horas do inventario
          EdtTotalHoras.Text:=FormatFloat('###,##0.00',TotalHorasInv);

          //Calcula o total de pe�as contadas
          EdtTotalPecas.Text:=FormatFloat('###,##0.00',TotalPecasInv);

          //Calcula a produtividade
          Prod:=TotalPecasInv/TotalHorasInv;
          EdtProdutividade.Text:=FormatFloat('###,##0.00',Prod);
          FrmInventario.EdtProdutividade.Text:=FormatFloat('###,##0.00',Prod);


      end;



end;

procedure TFrmProdutividade.GridProdutividadeDrawCell(Sender: TObject;
  ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
begin

//Centralizar texto na StringGrid
if (Acol=0) and (GridProdutividade.Cells[ACol,ARow]<>'') then
with GridProdutividade.Canvas do
begin
FillRect(Rect);
// CENTRALIZA
TextOut(StrToInt(FloatToStr(Int((Rect.Right-Rect.Left-TextWidth(GridProdutividade.Cells[ACol,ARow]))/2)))+Rect.Left,
Rect.Top+2, GridProdutividade.Cells[ACol,ARow]);
end;
end;

procedure TFrmProdutividade.GeraRelClick(Sender: TObject);
var i : Integer;
begin
 DM.CDSProd.Close;
 Dm.CDSProd.CreateDataSet;
 DM.CDSProd.Open;

    for i:=1 to GridProdutividade.RowCount do
     begin
      DM.CDSProd.Append;
      Dm.CDSProdMATRICULA.AsString    :=GridProdutividade.Cells[0,i];
      DM.CDSProdNOMECONF.AsString     :=GridProdutividade.Cells[1,i];
      Dm.CDSProdTOTAL_HORA.AsString   :=GridProdutividade.Cells[4,i];
      Dm.CDSProdQUANTIDADE.AsString   :=GridProdutividade.Cells[5,i];
      DM.CDSProdPRODUTIVIDADE.AsString:=GridProdutividade.Cells[6,i];
      DM.CDSProd.Post;
     end;

     Dm.RvPrjWis.Close;
     Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Produtividade.rav';
     Dm.RvPrjWis.Open;
     Dm.RvSysWis.DefaultDest := rdPreview;
     Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
     Dm.RvPrjWis.ExecuteReport('Produtividade');


end;

end.


