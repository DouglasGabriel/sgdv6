
unit UFrmHorGlobal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, Mask, DBCtrls, ExtCtrls, IdBaseComponent,
  IdComponent, IdTCPConnection, IdTCPClient, IdTime, IdUDPBase,
  IdUDPClient, IdSNTP, Buttons, strutils, sLabel;

type
  TFrmHorarioGlobal = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    StatusBar1: TStatusBar;
    Panel3: TPanel;
    Label2: TLabel;
    BitBtn1: TBitBtn;
    DateTimePicker1: TDateTimePicker;
    DateTimePicker2: TDateTimePicker;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    DateTimePicker3: TDateTimePicker;
    DateTimePicker4: TDateTimePicker;
    ComboBox1: TComboBox;
    sLabelFX1: TsLabelFX;
    procedure BitBtn1Click(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure CheckBox2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmHorarioGlobal: TFrmHorarioGlobal;

implementation

uses UDm, UInventario, UFuncProc, DB;

{$R *.dfm}

procedure TFrmHorarioGlobal.BitBtn1Click(Sender: TObject);
begin
 If ComboBox1.Text = '' then
  begin
     Application.MessageBox('Preencha o Evento do Conferente!!','Mensagem do Sistema', MB_ICONERROR + MB_OK);
  end
 else
  begin
   //Verifica se hor�rio final � maior que hor�rio inicial
    if (DateTimePicker1.DateTime + DateTimePicker2.DateTime)
    < (DateTimePicker3.DateTime + DateTimePicker4.DateTime) then
     Begin
      try
        try
           if CheckBox1.Checked and CheckBox2.Checked then
            begin
             Dm.qrUpdateHorTotal.Close;
             Dm.qrUpdateHorTotal.ParamByName('pHorEntrada').Value := DateTimeToStr(DateTimePicker1.DateTime + DateTimePicker2.DateTime);
             Dm.qrUpdateHorTotal.ParamByName('pHorSaida').Value   := DateTimeToStr(DateTimePicker3.DateTime + DateTimePicker4.DateTime);
             Dm.qrUpdateHorTotal.ParamByName('pHorEvent').Value   := UpperCase(ComboBox1.Text);
             Dm.qrUpdateHorTotal.ParamByName('pHorTotal').Value   := CalculaTotaldeHorasGeral(Dm.qrUpdateHorTotal.ParamByName('pHorEntrada').Value,Dm.qrUpdateHorTotal.ParamByName('pHorSaida').Value);
             DM.qrUpdateHorTotal.ExecSQL;
            end
           else
            begin
              if CheckBox1.Checked then
               begin
                DM.qrUpdateHorCont.Close;
                DM.qrUpdateHorCont.Open;
                 if DM.qrUpdateHorCont.RecordCount = 0 then
                  begin
                   Dm.qrUpdateHorParcEnt.Close;
                   Dm.qrUpdateHorParcEnt.ParamByName('pHorEntrada').Value := DateTimeToStr(DateTimePicker1.DateTime + DateTimePicker2.DateTime);
                   Dm.qrUpdateHorParcEnt.ParamByName('pHorEvent').Value   := UpperCase(ComboBox1.Text);
                   DM.qrUpdateHorParcEnt.ExecSQL;
                  end
                 else
                  begin
                   Dm.qrUpdateHorEntCalc.Close;
                   Dm.qrUpdateHorEntCalc.ParamByName('pHorEntrada').Value := DateTimeToStr(DateTimePicker1.DateTime + DateTimePicker2.DateTime);
                   Dm.qrUpdateHorEntCalc.ParamByName('pHorEvent').Value   := UpperCase(ComboBox1.Text);
                   Dm.qrUpdateHorEntCalc.ParamByName('pHorTotal').Value   := CalculaTotaldeHorasGeral(Dm.qrUpdateHorEntCalc.ParamByName('pHorEntrada').Value,DM.IBHorariosHOR_SAIDA.Value);
                   DM.qrUpdateHorEntCalc.ExecSQL;
                  end;
               end
              else
               begin
                 Dm.qrUpdateHorParcSai.Close;
                 Dm.qrUpdateHorParcSai.ParamByName('pHorSaida').Value := DateTimeToStr(DateTimePicker3.DateTime + DateTimePicker4.DateTime);
                 Dm.qrUpdateHorParcSai.ParamByName('pHorEvent').Value := UpperCase(ComboBox1.Text);
                 Dm.qrUpdateHorParcSai.ParamByName('pHorTotal').Value := CalculaTotaldeHorasGeral(DM.IBHorariosHOR_ENTRADA.Value,Dm.qrUpdateHorParcSai.ParamByName('pHorSaida').Value);
                 DM.qrUpdateHorParcSai.ExecSQL;
               end;
            end;
        except
          Application.MessageBox('Ocorreu um erro no registro de hor�rio, tente novamente!!','Mensagem do Sistema', MB_ICONERROR + MB_OK);
        end;
      finally
       DM.Transacao.CommitRetaining;
       Dm.IBHorarios.Close;
       DM.IBHorarios.Open;
       Application.MessageBox('Hor�rio global registrado com sucesso!!','Mensagem do Sistema', MB_ICONINFORMATION + MB_OK);
       FrmHorarioGlobal.Close;
      end;
     end
    else
      Application.MessageBox('Horario final n�o pode ser menor ou igual ao inical','Mensagem do Sistema', MB_ICONERROR + MB_OK);
  end
end;

procedure TFrmHorarioGlobal.CheckBox1Click(Sender: TObject);
begin
 IF (CheckBox1.Checked = True) then
   Begin
     DateTimePicker1.Enabled := True;
     DateTimePicker2.Enabled := True;
   End
 else
   Begin
     DateTimePicker1.Enabled := False;
     DateTimePicker2.Enabled := False;
     if (CheckBox2.Checked = True) then
      Begin
        CheckBox2.Checked := False;
        DateTimePicker3.Enabled := False;
        DateTimePicker4.Enabled := False;
      End;
   End;
end;

procedure TFrmHorarioGlobal.CheckBox2Click(Sender: TObject);
begin
 IF (CheckBox2.Checked = True) then
   Begin
     DateTimePicker3.Enabled := True;
     DateTimePicker4.Enabled := True;

   End
 else
   Begin
     DateTimePicker3.Enabled := False;
     DateTimePicker4.Enabled := False;
     
     if (CheckBox1.Checked = True) then
      Begin
        CheckBox1.Checked := False;
        DateTimePicker1.Enabled := False;
        DateTimePicker2.Enabled := False;
      End;
   End;
end;

procedure TFrmHorarioGlobal.FormShow(Sender: TObject);
begin
  DateTimePicker1.DateTime := date;
  DateTimePicker2.DateTime := time;
  DateTimePicker3.DateTime := date;
  DateTimePicker4.DateTime := time;

  //Se tiver hora de entrada ativa checkBox e joga na tela
  If Dm.IBHorariosHOR_ENTRADA.Value <> '' then
  Begin

  
    CheckBox1.Checked := True;
    DateTimePicker1.DateTime := StrToDate(Copy(Dm.IBHorariosHOR_ENTRADA.Value,1,10));
    DateTimePicker2.DateTime := StrToTime(rightstr(Dm.IBHorariosHOR_ENTRADA.Value,8));
  End;

  //Se tiver hora de sa�da ativa checkBox e joga na tela
  If Dm.IBHorariosHOR_SAIDA.Value <> '' then
  Begin
    CheckBox2.Checked := True;
    DateTimePicker3.DateTime := StrToDate(Copy(Dm.IBHorariosHOR_SAIDA.Value,1,10));
    DateTimePicker4.DateTime := StrToTime(rightstr(Dm.IBHorariosHOR_SAIDA.Value,8));
  End;
end;

end.






