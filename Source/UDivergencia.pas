unit UDivergencia;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ComCtrls, ExtCtrls, sBitBtn, RpDefine, RpRender,
  RpRenderPDF, RpRenderText, DBCtrls, sLabel;

type
  TFrmDivergencia = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    StatusBar1: TStatusBar;
    Panel3: TPanel;
    sBitBtn1: TsBitBtn;
    Label2: TLabel;
    Edit1 : TEdit;
    RadioGroup1: TRadioGroup;
    Label3: TLabel;
    CmbSetor: TComboBox;
    CheckBoxDiv: TCheckBox;
    Label4: TLabel;
    CmbSetorFim: TComboBox;
    Edt_ValorFinal: TEdit;
    Lbl_ValorFinal: TLabel;
    CheckBoxCODINT: TCheckBox;
    RvPDF: TRvRenderPDF;
    RgOrdenar: TRadioGroup;
    RvTxt: TRvRenderText;
    sLabelFX1: TsLabelFX;
    procedure FormShow(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure CmbSetorFimClick(Sender: TObject);
    procedure CmbSetorClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmDivergencia: TFrmDivergencia;

implementation

uses UDm, UFuncProc, Udm2;

{$R *.dfm}

procedure TFrmDivergencia.FormShow(Sender: TObject);
begin
 if (dm.IBInventarioINV_LOJA.Text = 'CP1') OR (dm.IBInventarioINV_LOJA.Text = 'CP2') OR (dm.IBInventarioINV_LOJA.Text = 'CP3')then
 begin
   RadioGroup1.Items[0] := 'Valor Quantidade';
 end;

 if (dm.IBInventarioINV_LOJA.Text = 'SBB') then
    TRadioGroup(RadioGroup1.Controls[6]).Visible := True
 Else
    TRadioGroup(RadioGroup1.Controls[6]).Visible := False
end;

procedure TFrmDivergencia.sBitBtn1Click(Sender: TObject);
var cont: Integer;
begin

 Try

  Dm.QDivZerados.Close;
  Dm.QDivZerados.SQL.Clear;
  Dm.QDivZerados.SQL.Add('SELECT CA.CAD_CODINT, CA.CAD_EAN, CA.CAD_SETOR,CA.CAD_UN,CA.CAD_DESC, CA.CAD_PRECO, CA.CAD_QUANTIDADE, ');
  Dm.QDivZerados.SQL.Add('(CASE when ((CA.CAD_PRECO * CA.CAD_QUANTIDADE) < 1) then ((CA.CAD_PRECO * CA.CAD_QUANTIDADE) * -1) else (CAD_PRECO * CA.CAD_QUANTIDADE) end) as DIF_VALOR ');
  Dm.QDivZerados.SQL.Add('from CONTAGENS CO right JOIN CADASTRO CA on (CA.CAD_EAN = CO.CON_EAN) or (CA.CAD_CODINT = CO.CON_CADCODINT) ');
  Dm.QDivZerados.SQL.Add('where (CASE when ((CA.CAD_PRECO * CA.CAD_QUANTIDADE) < 1) then ');
  Dm.QDivZerados.SQL.Add('((CA.CAD_PRECO * CA.CAD_QUANTIDADE) * -1) else (CAD_PRECO * CA.CAD_QUANTIDADE) end) >= :DIF_VALOR and (ca.cad_quantidade > 0) and (co.CON_QUANTIDADE is null) and (ca.cad_Setor >= :cad_Setor) and  (ca.cad_setor <= :cad_setorFim) ');
  Dm.QDivZerados.SQL.Add('GROUP BY CA.CAD_CODINT, CA.CAD_EAN, CA.CAD_DESC, CA.CAD_PRECO, CA.CAD_QUANTIDADE,CA.CAD_SETOR,CA.CAD_UN ');
  Dm.QDivZerados.SQL.Add('ORDER BY DIF_VALOR DESC ');



 {If Dm.IBInventarioINV_LOJA.Value = 'LOP' then
  begin
      Dm.QDivDesc1.Close;
      Dm.QDivDesc2.Close;
      Dm.QDivDesc1.ParamByName('DIF_VALOR').Value := Edit1.Text;
      Dm.QDivDesc1.Open;
      Dm.QDivDesc2.Open;
      Dm.RvPrjWis.Close;
      Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\DivergenciaDesc.rav';
      Dm.RvPrjWis.Open;
      Dm.RvSysWis.DefaultDest := rdPreview;
      Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
      Dm.RvPrjWis.ExecuteReport('DivergenciaDesc')
  end
 else }
   If RadioGroup1.ItemIndex = 3 then
     begin
      try
       if MessageDlg('O relat�rio poder� demorar alguns minutos! Aguarde...', mtConfirmation, [mbOk], 0) = mrOk then
        Screen.Cursor := crSQLWait;

        if DM.IBInventarioINV_LOJA.Value = 'BHC' then
          begin
            Dm.QDivBHC.Close;
            Dm.QDiv2.Close;
            If (Edit1.Text <> '') and (Edt_ValorFinal.Text <> '') then
             begin
              Dm.QDivBHC.ParamByName('VALOR').Value := Edit1.Text;
              Dm.QDivBHC.ParamByName('VALORFIM').Value := Edt_ValorFinal.Text;
                 Dm.QDivBHC.Open;
                 cont := 0;
                       While not DM.QDivBHC.Eof do
                        begin
                          cont := cont + 1;
                          DM.QDivBHC.Next;
                        end;
                Dm.QDiv2.Open;
                Dm.RvPrjWis.Close;
                Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Cega_BHC.rav';
                Dm.RvPrjWis.Open;
                DM.RvPrjWis.SetParam('ValorMin','Total de Itens Divergentes: ' + IntToStr(cont)) ;
                Dm.RvSysWis.DefaultDest := rdPreview;
                Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
                Dm.RvPrjWis.ExecuteReport('Cega_BHC');

                AdicionaLog(DateTimeToStr(Now)+'> Gera��o da diverg�ncia cega realizada com sucesso! - Par�metros utilizados: Valor inicial R$'+FormatFloat('##,###0.00',StrToFloat(Edit1.Text))+'/Valor Final R$'+FormatFloat('##,###0.00',StrToFloat(Edt_ValorFinal.Text))+'/N�mero de itens divergentes: '+IntToStr(cont));
                Screen.Cursor := crDefault;

                // Salvando relat�rio em PDF automaticamente
                Try
                  Dm.RvSysWis.DefaultDest:= rdFile;
                  Dm.RvSysWis.DoNativeOutput:= false;
                  Dm.RvSysWis.RenderObject:= rvPdf;
                  Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Diverg�ncia Cega_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
                  Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
                  Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Cega_BHC.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
                  Dm.RvPrjWis.Engine:= Dm.RvSysWis;
                  Dm.RvPrjWis.Execute;
                Except
                  ShowMessage('Erro ao gerar o relat�rio em PDF');
                end;
             end
            else
            Begin
             ShowMessage('Favor inserir os valores para gerar a Diverg�ncia');
             Screen.Cursor := crDefault;
            End;
          end
        Else
          begin
            Dm.QDiv1.Close;
            Dm.QDiv2.Close;
            If (Edit1.Text <> '') and (Edt_ValorFinal.Text <> '') then
             begin
              Dm.QDiv1.ParamByName('VALOR').Value := Edit1.Text;
              Dm.QDiv1.ParamByName('VALORFIM').Value := Edt_ValorFinal.Text;
                 Dm.QDiv1.Open;
                 cont := 0;
                       While not DM.QDiv1.Eof do
                        begin
                          cont := cont + 1;
                          DM.QDiv1.Next;
                        end;
                Dm.QDiv2.Open;
                Dm.RvPrjWis.Close;
                Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Cega.rav';
                Dm.RvPrjWis.Open;
                DM.RvPrjWis.SetParam('ValorMin','Total de Itens Divergentes: ' + IntToStr(cont)) ;
                Dm.RvSysWis.DefaultDest := rdPreview;
                Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
                Dm.RvPrjWis.ExecuteReport('Cega');

                AdicionaLog(DateTimeToStr(Now)+'> Gera��o da diverg�ncia cega realizada com sucesso! - Par�metros utilizados: Valor inicial R$'+FormatFloat('##,###0.00',StrToFloat(Edit1.Text))+'/Valor Final R$'+FormatFloat('##,###0.00',StrToFloat(Edt_ValorFinal.Text))+'/N�mero de itens divergentes: '+IntToStr(cont));
                Screen.Cursor := crDefault;

                // Salvando relat�rio em PDF automaticamente
                Try
                  Dm.RvSysWis.DefaultDest:= rdFile;
                  Dm.RvSysWis.DoNativeOutput:= false;
                  Dm.RvSysWis.RenderObject:= rvPdf;
                  Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Diverg�ncia Cega_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
                  Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
                  Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Cega.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
                  Dm.RvPrjWis.Engine:= Dm.RvSysWis;
                  Dm.RvPrjWis.Execute;
                Except
                  ShowMessage('Erro ao gerar o relat�rio em PDF');
                end;
             end
            else
            Begin
             ShowMessage('Favor inserir os valores para gerar a Diverg�ncia');
             Screen.Cursor := crDefault;
            End;
          end;
      except
        AdicionaLog(DateTimeToStr(Now)+'> Erro na gera��o da diverg�ncia cega! - Par�metros utilizados: Valor inicial R$'+FormatFloat('##,###0.00',StrToFloat(Edit1.Text))+'/Valor Final R$'+FormatFloat('##,###0.00',StrToFloat(Edt_ValorFinal.Text)));
      end;
     end
   else
    If RadioGroup1.ItemIndex = 0 then
     if RgOrdenar.ItemIndex = 0 then
      begin
        try
        // if MessageDlg('O relat�rio poder� demorar alguns minutos! Aguarde...', mtConfirmation, [mbOk], 0) = mrOk then
          Screen.Cursor:= crSQLWait;

          Dm.QDiv1.Close;
          DM.QDiv1.SQL.Clear;
          If Dm.IBInventarioINV_LOJA.Value = 'BGB' then
            dm.QDiv1.SQL.Add('SELECT First 300 * FROM divergencia ')
          Else
            dm.QDiv1.SQL.Add('SELECT * FROM divergencia ');

          dm.QDiv1.SQL.Add('WHERE ((DIF_VALOR2 * 1) BETWEEN :VALOR AND :VALORFIM) OR ((DIF_VALOR2 * -1) BETWEEN :VALOR AND :VALORFIM) ');
          Dm.QDiv2.Close;
          If (Edit1.Text <> '') and (Edt_ValorFinal.Text <> '') then
           begin
            Dm.QDiv1.ParamByName('VALOR').Value := Edit1.Text;
            Dm.QDiv1.ParamByName('VALORFIM').Value := Edt_ValorFinal.Text;
              Dm.QDiv1.Open;
              cont := 0;
                     While not DM.QDiv1.Eof do
                      begin
                        cont := cont + 1;
                        DM.QDiv1.Next;
                      end;
            Dm.QDiv2.Open;
            if Dm.IBInventarioINV_LOJA.Value <> 'SBB' then
              begin
                Dm.RvPrjWis.Close;
                Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Divergencia.rav';
                Dm.RvPrjWis.Open;
                DM.RvPrjWis.SetParam('ValorMin','Total de Itens Divergentes: ' + IntToStr(cont)) ;
                 Dm.RvSysWis.DefaultDest := rdPreview;
                Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
                Dm.RvPrjWis.ExecuteReport('Divergencia');

                AdicionaLog(DateTimeToStr(Now)+'> Gera��o da diverg�ncia por valor financeiro ordenada por valor realizada com sucesso! - Par�metros utilizados: Valor inicial R$'+FormatFloat('##,###0.00',StrToFloat(Edit1.Text))+'/Valor Final R$'+FormatFloat('##,###0.00',StrToFloat(Edt_ValorFinal.Text))+'/N�mero de itens divergentes: '+IntToStr(cont));
                Screen.Cursor := crDefault;

               // Salvando relat�rio em PDF automaticamente
                Try
                  Dm.RvSysWis.DefaultDest:= rdFile;
                  Dm.RvSysWis.DoNativeOutput:= false;
                  Dm.RvSysWis.RenderObject:= rvPdf;
                  Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Diverg�ncia Valor Financeiro_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
                  Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
                  Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Divergencia.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
                  Dm.RvPrjWis.Engine:= Dm.RvSysWis;
                  Dm.RvPrjWis.Execute;
                Except
                  ShowMessage('Erro ao gerar o relat�rio em PDF');
                end;
              end
            Else
              begin
                Dm.RvPrjWis.Close;
                Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Divergencia_SBB.rav';
                Dm.RvPrjWis.Open;
                DM.RvPrjWis.SetParam('ValorMin','Total de Itens Divergentes: ' + IntToStr(cont)) ;
                 Dm.RvSysWis.DefaultDest := rdPreview;
                Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
                Dm.RvPrjWis.ExecuteReport('Divergencia_SBB');

                AdicionaLog(DateTimeToStr(Now)+'> Gera��o da diverg�ncia por valor financeiro ordenada por valor realizada com sucesso! - Par�metros utilizados: Valor inicial R$'+FormatFloat('##,###0.00',StrToFloat(Edit1.Text))+'/Valor Final R$'+FormatFloat('##,###0.00',StrToFloat(Edt_ValorFinal.Text))+'/N�mero de itens divergentes: '+IntToStr(cont));
                Screen.Cursor := crDefault;

               // Salvando relat�rio em PDF automaticamente
                Try
                  Dm.RvSysWis.DefaultDest:= rdFile;
                  Dm.RvSysWis.DoNativeOutput:= false;
                  Dm.RvSysWis.RenderObject:= rvPdf;
                  Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Divergencia_Valor Financeiro_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
                  Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
                  Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Divergencia_SBB.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
                  Dm.RvPrjWis.Engine:= Dm.RvSysWis;
                  Dm.RvPrjWis.Execute;
                Except
                  ShowMessage('Erro ao gerar o relat�rio em PDF');
                end;

              end;
           end
          else
          Begin
           ShowMessage('Favor inserir os valores para gerar a Diverg�ncia');
           Screen.Cursor := crDefault;
          End;
        except
          AdicionaLog(DateTimeToStr(Now)+'> Erro na gera��o da diverg�ncia por valor financeiro! - Par�metros utilizados: Valor inicial R$'+FormatFloat('##,###0.00',StrToFloat(Edit1.Text))+'/Valor Final R$'+FormatFloat('##,###0.00',StrToFloat(Edt_ValorFinal.Text)));
        end;
      end
     else
      begin
        try
         if MessageDlg('O relat�rio poder� demorar alguns minutos! Aguarde...', mtConfirmation, [mbOk], 0) = mrOk then
          Screen.Cursor:= crSQLWait;

          Dm.QDiv1.Close;
          DM.QDiv1.SQL.Clear;
          dm.QDiv1.SQL.Add('SELECT * FROM divergencia_desc ');
          dm.QDiv1.SQL.Add('WHERE ((DIF_VALOR2 * 1) BETWEEN :VALOR AND :VALORFIM) OR ((DIF_VALOR2 * -1) BETWEEN :VALOR AND :VALORFIM) ');
          Dm.QDiv2.Close;
          If (Edit1.Text <> '') and (Edt_ValorFinal.Text <> '') then
           begin
            Dm.QDiv1.ParamByName('VALOR').Value := Edit1.Text;
            Dm.QDiv1.ParamByName('VALORFIM').Value := Edt_ValorFinal.Text;
              Dm.QDiv1.Open;
              cont := 0;
                     While not DM.QDiv1.Eof do
                      begin
                        cont := cont + 1;
                        DM.QDiv1.Next;
                      end;
              Dm.QDiv2.Open;
              Dm.RvPrjWis.Close;
              Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Divergencia.rav';
              Dm.RvPrjWis.Open;
              DM.RvPrjWis.SetParam('ValorMin','Total de Itens Divergentes: ' + IntToStr(cont)) ;
              Dm.RvSysWis.DefaultDest := rdPreview;
              Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
              Dm.RvPrjWis.ExecuteReport('Divergencia');

              AdicionaLog(DateTimeToStr(Now)+'> Gera��o da diverg�ncia por valor financeiro ordenada por descri��o realizada com sucesso! - Par�metros utilizados: Valor inicial R$'+FormatFloat('##,###0.00',StrToFloat(Edit1.Text))+'/Valor Final R$'+FormatFloat('##,###0.00',StrToFloat(Edt_ValorFinal.Text))+'/N�mero de itens divergentes: '+IntToStr(cont));
              Screen.Cursor := crDefault;

             // Salvando relat�rio em PDF automaticamente
              Try
                Dm.RvSysWis.DefaultDest:= rdFile;
                Dm.RvSysWis.DoNativeOutput:= false;
                Dm.RvSysWis.RenderObject:= rvPdf;
                Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Diverg�ncia Valor Financeiro ordem desc_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
                Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
                Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Divergencia.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
                Dm.RvPrjWis.Engine:= Dm.RvSysWis;
                Dm.RvPrjWis.Execute;
              Except
                ShowMessage('Erro ao gerar o relat�rio em PDF');
              end;

           end
          else
          Begin
           ShowMessage('Favor inserir os valores para gerar a Diverg�ncia');
           Screen.Cursor := crDefault;
          End;
        except
          AdicionaLog(DateTimeToStr(Now)+'> Erro na gera��o da diverg�ncia por valor financeiro! - Par�metros utilizados: Valor inicial R$'+FormatFloat('##,###0.00',StrToFloat(Edit1.Text))+'/Valor Final R$'+FormatFloat('##,###0.00',StrToFloat(Edt_ValorFinal.Text)));
        end;
      end
    else
      If RadioGroup1.ItemIndex = 1 then
       begin
         try
          if MessageDlg('O relat�rio poder� demorar alguns minutos! Aguarde...', mtConfirmation, [mbOk], 0) = mrOk then
           Screen.Cursor := crSQLWait;
           if DM.IBInventarioINV_LOJA.Value = 'DME' then
            begin
              IF CheckBoxDiv.Checked then
               begin
                 Dm.QDiv1.Close;
                 Dm.QDiv1.Sql.Clear;
                 Dm.QDiv1.SQL.Add('SELECT * FROM DIVERGENCIA ');
                 DM.QDiv1.SQL.Add('WHERE (CAD_SETOR >= :CAD_SETOR) and (CAD_SETOR <= :CAD_SETORFIM) and (NUM_LOJA = "S") and (((DIF_VALOR2 * 1) BETWEEN :VALOR AND :VALORFIM) OR ((DIF_VALOR2 * -1) BETWEEN :VALOR AND :VALORFIM)) ');
                 Dm.QDiv1.SQL.Add('ORDER BY DIF_VALOR3 DESC ');
               end
              else
               begin
                 Dm.QDiv1.Close;
                 Dm.QDiv1.Sql.Clear;
                 Dm.QDiv1.SQL.Add('SELECT * FROM DIVERGENCIA ');
                 DM.QDiv1.SQL.Add('WHERE (CAD_SETOR >= :CAD_SETOR) and (CAD_SETOR <= :CAD_SETORFIM) and (((DIF_VALOR2 * 1) BETWEEN :VALOR AND :VALORFIM) OR ((DIF_VALOR2 * -1) BETWEEN :VALOR AND :VALORFIM)) ');
                 Dm.QDiv1.SQL.Add('ORDER BY DIF_VALOR3 DESC ');
               end;

            end
           else
            begin
             Dm.QDiv1.Close;
             Dm.QDiv1.Sql.Clear;
             Dm.QDiv1.SQL.Add('SELECT * FROM DIVERGENCIA ');
             DM.QDiv1.SQL.Add('WHERE (CAD_SETOR >= :CAD_SETOR) and (CAD_SETOR <= :CAD_SETORFIM) and (((DIF_VALOR2 * 1) BETWEEN :VALOR AND :VALORFIM) OR ((DIF_VALOR2 * -1) BETWEEN :VALOR AND :VALORFIM)) ');
             Dm.QDiv1.SQL.Add('ORDER BY DIF_VALOR3 DESC ');
            end;
            If CmbSetor.Text = '' then
               ShowMessage('� preciso selecionar um setor')
            else
             begin
               Dm.QDiv1.ParamByName('CAD_SETOR').Value    := CmbSetor.Text;
               Dm.QDiv1.ParamByName('CAD_SETORFIM').Value := CmbSetorFim.Text;
             end;
             If (Edit1.Text <> '') and (Edt_ValorFinal.Text <> '') then
              begin
               Dm.QDiv1.ParamByName('VALOR').Value := Edit1.Text;
               Dm.QDiv1.ParamByName('VALORFIM').Value := Edt_ValorFinal.Text;

                 Dm.QDiv1.Open;
                 cont := 0;
                    While not DM.QDiv1.Eof do
                     begin
                       cont := cont + 1;
                       DM.QDiv1.Next;
                     end;
                 DM.QDiv2.Close;    
                 Dm.QDiv2.Open;
                 Dm.RvPrjWis.Close;
                 Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Divergencia.rav';
                 Dm.RvPrjWis.Open;
                 DM.RvPrjWis.SetParam('ValorMin','Total de Itens Divergentes: ' + IntToStr(cont)) ;
                 Dm.RvSysWis.DefaultDest := rdPreview;
                 Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
                 Dm.RvPrjWis.ExecuteReport('Divergencia');

                 AdicionaLog(DateTimeToStr(Now)+'> Gera��o da diverg�ncia por valor financeiro e setor realizada com sucesso! - Par�metros utilizados: Valor inicial R$'+FormatFloat('##,###0.00',StrToFloat(Edit1.Text))+'/Valor Final R$'+FormatFloat('##,###0.00',StrToFloat(Edt_ValorFinal.Text))+'/N�mero de itens divergentes: '+IntToStr(cont)+'/Setor inicial: '+CmbSetor.Text+' - Setor Final: '+CmbSetorFim.Text);
                 Screen.Cursor := crDefault;

                 // Salvando relat�rio em PDF automaticamente
                 Try
                   Dm.RvSysWis.DefaultDest:= rdFile;
                   Dm.RvSysWis.DoNativeOutput:= false;
                   Dm.RvSysWis.RenderObject:= rvPdf;
                   Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Diverg�ncia Setor_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
                   Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
                   Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Divergencia.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
                   Dm.RvPrjWis.Engine:= Dm.RvSysWis;
                   Dm.RvPrjWis.Execute;
                 Except
                   ShowMessage('Erro ao gerar o relat�rio em PDF');
                 end;
              end
             else
             Begin
              ShowMessage('Favor inserir os valores para gerar a Diverg�ncia');
              Screen.Cursor := crDefault;
             End;
         except
           AdicionaLog(DateTimeToStr(Now)+'> Erro na gera��o da diverg�ncia por valor financeiro e setor! - Par�metros utilizados: Valor inicial R$'+FormatFloat('##,###0.00',StrToFloat(Edit1.Text))+'/Valor Final R$'+FormatFloat('##,###0.00',StrToFloat(Edt_ValorFinal.Text))+'/Setor inicial: '+CmbSetor.Text+' - Setor Final: '+CmbSetorFim.Text);
         end
       end
      else
        If (RadioGroup1.ItemIndex = 2) and (RgOrdenar.ItemIndex = 0) then
         if not CheckBoxDiv.Checked then
          begin
            If not CheckBoxCODINT.Checked then
             begin
               try
                  if MessageDlg('O relat�rio poder� demorar alguns minutos! Aguarde...', mtConfirmation, [mbOk], 0) = mrOk then
                   Screen.Cursor := crSQLWait;


                  Dm.QDivZerados.Close;
                  Dm.QDivZerados.SQL.Clear;
                  Dm.QDivZerados.SQL.Add(' SELECT CA.CAD_CODINT, CA.CAD_EAN, CA.CAD_SETOR, CA.CAD_UN, CA.CAD_DESC, CA.CAD_PRECO, CA.CAD_QUANTIDADE, ');
                  Dm.QDivZerados.SQL.Add(' (CASE WHEN ((CA.CAD_PRECO * CA.CAD_QUANTIDADE) < 1) THEN ((CA.CAD_PRECO * CA.CAD_QUANTIDADE) * -1) ELSE (CAD_PRECO * CA.CAD_QUANTIDADE) END) AS DIF_VALOR ');
                  Dm.QDivZerados.SQL.Add(' FROM CONTAGENS CO RIGHT JOIN CADASTRO CA ON (CA.CAD_EAN = CO.CON_EAN) WHERE (CASE WHEN ((CA.CAD_PRECO * CA.CAD_QUANTIDADE) < 1) ');
                  Dm.QDivZerados.SQL.Add(' THEN ((CA.CAD_PRECO * CA.CAD_QUANTIDADE) * -1) ELSE (CAD_PRECO * CA.CAD_QUANTIDADE) END) >= :DIF_VALOR ');
                  Dm.QDivZerados.SQL.Add(' AND (CA.CAD_QUANTIDADE > 0) AND (CO.CON_QUANTIDADE IS NULL)  AND (CA.CAD_SETOR >= :CAD_SETOR) AND (CA.CAD_SETOR <= :CAD_SETORFIM) AND (CO.CON_STATUS =  "." OR CO.CON_STATUS IS NULL) ');
                  Dm.QDivZerados.SQL.Add(' GROUP BY CA.CAD_CODINT, CA.CAD_EAN, CA.CAD_DESC, CA.CAD_PRECO, CA.CAD_QUANTIDADE,CA.CAD_SETOR,CA.CAD_UN ');
                  Dm.QDivZerados.SQL.Add(' ORDER BY DIF_VALOR DESC ');
                  if (CmbSetor.Text = '') and (CmbSetorFim.Text = '') then
                    begin
                      ShowMessage('Esse cliente n�o possui setores cadastrados, clique em "SEM DEPARTAMENTO"');
                      Dm.QDivZerados.ParamByName('CAD_SETOR').Value    :=0;
                      Dm.QDivZerados.ParamByName('CAD_SETORFIM').Value :=0;

                    end
                  else
                     begin
                        Dm.QDivZerados.ParamByName('CAD_SETOR').Value    := CmbSetor.Text;
                        Dm.QDivZerados.ParamByName('CAD_SETORFIM').Value := CmbSetorFim.Text;
                     end;
                    If Edit1.Text <> '' then
                     Dm.QDivZerados.ParamByName('DIF_VALOR').Value := Edit1.Text
                    else
                    Dm.QDivZerados.ParamByName('DIF_VALOR').Value :=0;

                    Dm.QDivZerados.Open;
                    Dm.RvPrjWis.Close;
                    Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Rel_DivZerados.rav';
                    Dm.RvPrjWis.Open;
                    Dm.RvSysWis.DefaultDest := rdPreview;
                    Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
                    Dm.RvPrjWis.ExecuteReport('Rel_DivZerados');

                    AdicionaLog(DateTimeToStr(Now)+'> Gera��o de itens zerados por EAN/Departamento realizado com sucesso! - Par�metros utilizados: Valor R$'+FormatFloat('##,###0.00',StrToFloat(Edit1.Text))+'/Setor inicial: '+CmbSetor.Text+' - Setor Final: '+CmbSetorFim.Text);
                    Screen.Cursor := crDefault;

                    // Salvando relat�rio em PDF automaticamente
                    Try
                      Dm.RvSysWis.DefaultDest:= rdFile;
                      Dm.RvSysWis.DoNativeOutput:= false;
                      Dm.RvSysWis.RenderObject:= rvPdf;
                      Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Diverg�ncia Itens N�o Contados_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
                      Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
                      Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Rel_DivZerados.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
                      Dm.RvPrjWis.Engine:= Dm.RvSysWis;
                      Dm.RvPrjWis.Execute;
                    Except
                      ShowMessage('Erro ao gerar o relat�rio em PDF');
                    end;
               except
                 AdicionaLog(DateTimeToStr(Now)+'> Erro na gera��o de itens zerados por EAN/Departamento! - Par�metros utilizados: Valor R$'+FormatFloat('##,###0.00',StrToFloat(Edit1.Text))+'/Setor inicial: '+CmbSetor.Text+' - Setor Final: '+CmbSetorFim.Text);
               end;
             end
            else
             begin
               try
                  if MessageDlg('O relat�rio poder� demorar alguns minutos! Aguarde...', mtConfirmation, [mbOk], 0) = mrOk then
                   Screen.Cursor := crSQLWait;


                  Dm.QDivZeradosCodInt.Close;
                  Dm.QDivZeradosCodInt.SQL.Clear;
                  Dm.QDivZeradosCodInt.SQL.Add('SELECT CA.CAD_CODINT, CA.CAD_SETOR, CA.CAD_UN, CA.CAD_DESC, CA.CAD_PRECO, CA.CAD_QUANTIDADE, ');
                  Dm.QDivZeradosCodInt.SQL.Add('(CASE when ((CA.CAD_PRECO * CA.CAD_QUANTIDADE) < 1) then ((CA.CAD_PRECO * CA.CAD_QUANTIDADE) * -1) ');
                  Dm.QDivZeradosCodInt.SQL.Add('else (CAD_PRECO * CA.CAD_QUANTIDADE) end) as DIF_VALOR ');
                  Dm.QDivZeradosCodInt.SQL.Add(' from CONTAGENS CO right JOIN CADASTRO CA on (CA.CAD_EAN = CO.CON_EAN) ');
                  Dm.QDivZeradosCodInt.SQL.Add('where (CASE when ((CA.CAD_PRECO * CA.CAD_QUANTIDADE) < 1) then ((CA.CAD_PRECO * CA.CAD_QUANTIDADE) * -1) ');
                  Dm.QDivZeradosCodInt.SQL.Add('else (CAD_PRECO * CA.CAD_QUANTIDADE) end) >= :DIF_VALOR and (ca.cad_quantidade > 0) and (co.CON_QUANTIDADE is null) ');
                  Dm.QDivZeradosCodInt.SQL.Add(' AND (CA.CAD_SETOR >= :CAD_SETOR)  AND (CA.CAD_SETOR <= :CAD_SETORFIM) and (CO.CON_STATUS =  "." OR CO.CON_STATUS IS NULL) ');
                  DM.QDivZeradosCodInt.SQL.Add(' GROUP BY CA.CAD_CODINT, CA.CAD_DESC, CA.CAD_PRECO, CA.CAD_QUANTIDADE, CA.CAD_SETOR, CA.CAD_UN ORDER BY DIF_VALOR DESC');

                  if (CmbSetor.Text = '') and (CmbSetorFim.Text = '') then
                    begin
                    ShowMessage('Esse cliente n�o possui setores cadastrados, clique em "SEM DEPARTAMENTO"');
                    Dm.QDivZeradosCodInt.ParamByName('cad_setor').Value    :=0;
                    Dm.QDivZeradosCodInt.ParamByName('CAD_SETORFIM').Value :=0;

                    end
                  else
                     begin
                        Dm.QDivZeradosCodInt.ParamByName('cad_setor').Value    := CmbSetor.Text;
                        Dm.QDivZeradosCodInt.ParamByName('CAD_SETORFIM').Value := CmbSetorFim.Text;
                     end;
                    If Edit1.Text <> '' then
                    Dm.QDivZeradosCodInt.ParamByName('DIF_VALOR').Value := Edit1.Text
                    else
                    Dm.QDivZeradosCodInt.ParamByName('DIF_VALOR').Value :=0;

                    Dm.QDivZeradosCodInt.Open;
                    Dm.RvPrjWis.Close;
                    Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Rel_DivZerados_CODINT.rav';
                    Dm.RvPrjWis.Open;
                    Dm.RvSysWis.DefaultDest := rdPreview;
                    Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
                    Dm.RvPrjWis.ExecuteReport('Rel_DivZerados_CODINT');

                    // Salvando relat�rio em PDF automaticamente
                    Try
                      Dm.RvSysWis.DefaultDest:= rdFile;
                      Dm.RvSysWis.DoNativeOutput:= false;
                      Dm.RvSysWis.RenderObject:= rvPdf;
                      Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Diverg�ncia Itens N�o Contados_Int'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
                      Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
                      Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Rel_DivZerados_CODINT.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
                      Dm.RvPrjWis.Engine:= Dm.RvSysWis;
                      Dm.RvPrjWis.Execute;
                    Except
                      ShowMessage('Erro ao gerar o relat�rio em PDF');
                    end;

                    AdicionaLog(DateTimeToStr(Now)+'> Gera��o de itens zerados por C�DIGO INTERNO/Departamento realizado com sucesso! - Par�metros utilizados: Valor R$'+FormatFloat('##,###0.00',StrToFloat(Edit1.Text))+'/Setor inicial: '+CmbSetor.Text+' - Setor Final: '+CmbSetorFim.Text);
                    Screen.Cursor := crDefault;
               except
                AdicionaLog(DateTimeToStr(Now)+'> Erro na gera��o de itens zerados por C�DIGO INTERNO/Departamento! - Par�metros utilizados: Valor R$'+FormatFloat('##,###0.00',StrToFloat(Edit1.Text))+'/Setor inicial: '+CmbSetor.Text+' - Setor Final: '+CmbSetorFim.Text);
               end
             end;
          end
         else
          begin
           If not CheckBoxCODINT.Checked then
            begin
             try
               if MessageDlg('O relat�rio poder� demorar alguns minutos! Aguarde...', mtConfirmation, [mbOk], 0) = mrOk then
               Screen.Cursor := crSQLWait;

                Dm.QDivZerados.Close;
                Dm.QDivZerados.SQL.Clear;
                Dm.QDivZerados.SQL.Add(' SELECT CA.CAD_CODINT, CA.CAD_EAN, CA.CAD_SETOR, CA.CAD_UN, CA.CAD_DESC, CA.CAD_PRECO, CA.CAD_QUANTIDADE, ');
                Dm.QDivZerados.SQL.Add(' (CASE WHEN ((CA.CAD_PRECO * CA.CAD_QUANTIDADE) < 1) THEN ((CA.CAD_PRECO * CA.CAD_QUANTIDADE) * -1) ELSE (CAD_PRECO * CA.CAD_QUANTIDADE) END) AS DIF_VALOR ');
                Dm.QDivZerados.SQL.Add(' FROM CONTAGENS CO RIGHT JOIN CADASTRO CA ON (CA.CAD_EAN = CO.CON_EAN) WHERE (CASE WHEN ((CA.CAD_PRECO * CA.CAD_QUANTIDADE) < 1) ');
                Dm.QDivZerados.SQL.Add(' THEN ((CA.CAD_PRECO * CA.CAD_QUANTIDADE) * -1) ELSE (CAD_PRECO * CA.CAD_QUANTIDADE) END) >= :DIF_VALOR ');
                Dm.QDivZerados.SQL.Add(' AND (CA.CAD_QUANTIDADE > 0) AND (CO.CON_QUANTIDADE IS NULL) AND (CO.CON_STATUS =  "." OR CO.CON_STATUS IS NULL) ');
                Dm.QDivZerados.SQL.Add(' GROUP BY CA.CAD_CODINT, CA.CAD_EAN, CA.CAD_DESC, CA.CAD_PRECO, CA.CAD_QUANTIDADE,CA.CAD_SETOR,CA.CAD_UN ');
                Dm.QDivZerados.SQL.Add(' ORDER BY DIF_VALOR DESC ');
               If Edit1.Text <> '' then
                Dm.QDivZerados.ParamByName('DIF_VALOR').Value := Edit1.Text
               else
                Dm.QDivZerados.ParamByName('DIF_VALOR').Value := 0;
               Dm.QDivZerados.Open;
               Dm.RvPrjWis.Close;
               Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Rel_DivZerados.rav';
               Dm.RvPrjWis.Open;
               Dm.RvSysWis.DefaultDest := rdPreview;
               Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
               Dm.RvPrjWis.ExecuteReport('Rel_DivZerados');

                // Salvando relat�rio em PDF automaticamente
                Try
                  Dm.RvSysWis.DefaultDest:= rdFile;
                  Dm.RvSysWis.DoNativeOutput:= false;
                  Dm.RvSysWis.RenderObject:= rvPdf;
                  Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Diverg�ncia Itens N�o Contados_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
                  Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
                  Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Rel_DivZerados.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
                  Dm.RvPrjWis.Engine:= Dm.RvSysWis;
                  Dm.RvPrjWis.Execute;
                Except
                  ShowMessage('Erro ao gerar o relat�rio em PDF');
                end;

               AdicionaLog(DateTimeToStr(Now)+'> Gera��o de itens zerados por EAN/Sem Departamento realizado com sucesso! - Par�metros utilizados: Valor R$'+FormatFloat('##,###0.00',StrToFloat(Edit1.Text)));
               Screen.Cursor := crDefault;
             except
              AdicionaLog(DateTimeToStr(Now)+'> Erro na gera��o de itens zerados por EAN/Sem Departamento! - Par�metros utilizados: Valor R$'+FormatFloat('##,###0.00',StrToFloat(Edit1.Text)));
             end;
            end
           else
            begin
             try
                if MessageDlg('O relat�rio poder� demorar alguns minutos! Aguarde...', mtConfirmation, [mbOk], 0) = mrOk then
               Screen.Cursor := crSQLWait;

                Dm.QDivZeradosCodInt.Close;
                Dm.QDivZeradosCodInt.SQL.Clear;
                Dm.QDivZeradosCodInt.SQL.Add('SELECT CA.CAD_CODINT, CA.CAD_SETOR, CA.CAD_UN, CA.CAD_DESC, CA.CAD_PRECO, CA.CAD_QUANTIDADE, ');
                Dm.QDivZeradosCodInt.SQL.Add('(CASE when ((CA.CAD_PRECO * CA.CAD_QUANTIDADE) < 1) then ((CA.CAD_PRECO * CA.CAD_QUANTIDADE) * -1) ');
                Dm.QDivZeradosCodInt.SQL.Add('else (CAD_PRECO * CA.CAD_QUANTIDADE) end) as DIF_VALOR ');
                Dm.QDivZeradosCodInt.SQL.Add(' from CONTAGENS CO right JOIN CADASTRO CA on (CA.CAD_EAN = CO.CON_EAN) ');
                Dm.QDivZeradosCodInt.SQL.Add('where (CASE when ((CA.CAD_PRECO * CA.CAD_QUANTIDADE) < 1) then ((CA.CAD_PRECO * CA.CAD_QUANTIDADE) * -1) ');
                Dm.QDivZeradosCodInt.SQL.Add('else (CAD_PRECO * CA.CAD_QUANTIDADE) end) >= :DIF_VALOR and (ca.cad_quantidade > 0) and (co.CON_QUANTIDADE is null) ');
                Dm.QDivZeradosCodInt.SQL.Add('and (CO.CON_STATUS =  "." OR CO.CON_STATUS IS NULL) ');
                DM.QDivZeradosCodInt.SQL.Add(' GROUP BY CA.CAD_CODINT, CA.CAD_DESC, CA.CAD_PRECO, CA.CAD_QUANTIDADE, CA.CAD_SETOR, CA.CAD_UN ORDER BY DIF_VALOR DESC');
               If Edit1.Text <> '' then
                Dm.QDivZeradosCodInt.ParamByName('DIF_VALOR').Value := Edit1.Text
               else
                Dm.QDivZeradosCodInt.ParamByName('DIF_VALOR').Value := 0;
               Dm.QDivZerados.Open;
               Dm.RvPrjWis.Close;
               Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Rel_DivZerados_CODINT.rav';
               Dm.RvPrjWis.Open;
               Dm.RvSysWis.DefaultDest := rdPreview;
               Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
               Dm.RvPrjWis.ExecuteReport('Rel_DivZerados_CODINT');

                // Salvando relat�rio em PDF automaticamente
              Try
                Dm.RvSysWis.DefaultDest:= rdFile;
                Dm.RvSysWis.DoNativeOutput:= false;
                Dm.RvSysWis.RenderObject:= rvPdf;
                Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Diverg�ncia Itens N�o Contados_Int'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
                Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
                Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Rel_DivZerados_CODINT.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
                Dm.RvPrjWis.Engine:= Dm.RvSysWis;
                Dm.RvPrjWis.Execute;
              Except
                ShowMessage('Erro ao gerar o relat�rio em PDF');
              end;

               AdicionaLog(DateTimeToStr(Now)+'> Gera��o de itens zerados por C�DIGO INTERNO/Sem Departamento realizado com sucesso! - Par�metros utilizados: Valor R$'+FormatFloat('##,###0.00',StrToFloat(Edit1.Text)));
               Screen.Cursor := crDefault;
             except
              AdicionaLog(DateTimeToStr(Now)+'> Erro na gera��o de itens zerados por C�DIGO INTERNO/Sem Departamento! - Par�metros utilizados: Valor R$'+FormatFloat('##,###0.00',StrToFloat(Edit1.Text)));
             end;
            end;
          end
        else
       If (RadioGroup1.ItemIndex = 2) and (RgOrdenar.ItemIndex = 1) then
         if not CheckBoxDiv.Checked then
          begin
            If not CheckBoxCODINT.Checked then
             begin
               try
                  if MessageDlg('O relat�rio poder� demorar alguns minutos! Aguarde...', mtConfirmation, [mbOk], 0) = mrOk then
                   Screen.Cursor := crSQLWait;


                  Dm.QDivZerados.Close;
                  Dm.QDivZerados.SQL.Clear;
                  Dm.QDivZerados.SQL.Add(' SELECT CA.CAD_CODINT, CA.CAD_EAN, CA.CAD_SETOR, CA.CAD_UN, CA.CAD_DESC, CA.CAD_PRECO, CA.CAD_QUANTIDADE, ');
                  Dm.QDivZerados.SQL.Add(' (CASE WHEN ((CA.CAD_PRECO * CA.CAD_QUANTIDADE) < 1) THEN ((CA.CAD_PRECO * CA.CAD_QUANTIDADE) * -1) ELSE (CAD_PRECO * CA.CAD_QUANTIDADE) END) AS DIF_VALOR ');
                  Dm.QDivZerados.SQL.Add(' FROM CONTAGENS CO RIGHT JOIN CADASTRO CA ON (CA.CAD_EAN = CO.CON_EAN) WHERE (CASE WHEN ((CA.CAD_PRECO * CA.CAD_QUANTIDADE) < 1) ');
                  Dm.QDivZerados.SQL.Add(' THEN ((CA.CAD_PRECO * CA.CAD_QUANTIDADE) * -1) ELSE (CAD_PRECO * CA.CAD_QUANTIDADE) END) >= :DIF_VALOR ');
                  Dm.QDivZerados.SQL.Add(' AND (CA.CAD_QUANTIDADE > 0) AND (CO.CON_QUANTIDADE IS NULL)  AND (CA.CAD_SETOR >= :CAD_SETOR) AND (CA.CAD_SETOR <= :CAD_SETORFIM) AND (CO.CON_STATUS =  "." OR CO.CON_STATUS IS NULL) ');
                  Dm.QDivZerados.SQL.Add(' GROUP BY CA.CAD_CODINT, CA.CAD_EAN, CA.CAD_DESC, CA.CAD_PRECO, CA.CAD_QUANTIDADE,CA.CAD_SETOR,CA.CAD_UN ');
                  Dm.QDivZerados.SQL.Add(' ORDER BY CA.CAD_DESC ');
                  if (CmbSetor.Text = '') and (CmbSetorFim.Text = '') then
                    begin
                      ShowMessage('Esse cliente n�o possui setores cadastrados, clique em "SEM DEPARTAMENTO"');
                      Dm.QDivZerados.ParamByName('CAD_SETOR').Value    :=0;
                      Dm.QDivZerados.ParamByName('CAD_SETORFIM').Value :=0;

                    end
                  else
                     begin
                        Dm.QDivZerados.ParamByName('CAD_SETOR').Value    := CmbSetor.Text;
                        Dm.QDivZerados.ParamByName('CAD_SETORFIM').Value := CmbSetorFim.Text;
                     end;
                    If Edit1.Text <> '' then
                     Dm.QDivZerados.ParamByName('DIF_VALOR').Value := Edit1.Text
                    else
                    Dm.QDivZerados.ParamByName('DIF_VALOR').Value :=0;

                    Dm.QDivZerados.Open;
                    Dm.RvPrjWis.Close;
                    Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Rel_DivZerados.rav';
                    Dm.RvPrjWis.Open;
                    Dm.RvSysWis.DefaultDest := rdPreview;
                    Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
                    Dm.RvPrjWis.ExecuteReport('Rel_DivZerados');

                    AdicionaLog(DateTimeToStr(Now)+'> Gera��o de itens zerados por EAN/Departamento ordenado por descri��o realizado com sucesso! - Par�metros utilizados: Valor R$'+FormatFloat('##,###0.00',StrToFloat(Edit1.Text))+'/Setor inicial: '+CmbSetor.Text+' - Setor Final: '+CmbSetorFim.Text);
                    Screen.Cursor := crDefault;

                    // Salvando relat�rio em PDF automaticamente
                    Try
                      Dm.RvSysWis.DefaultDest:= rdFile;
                      Dm.RvSysWis.DoNativeOutput:= false;
                      Dm.RvSysWis.RenderObject:= rvPdf;
                      Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Diverg�ncia Itens N�o Contados ordenado por descri��o_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
                      Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
                      Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Rel_DivZerados.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
                      Dm.RvPrjWis.Engine:= Dm.RvSysWis;
                      Dm.RvPrjWis.Execute;
                    Except
                      ShowMessage('Erro ao gerar o relat�rio em PDF');
                    end;
               except
                 AdicionaLog(DateTimeToStr(Now)+'> Erro na gera��o de itens zerados por EAN/Departamento ordenado por descri��o! - Par�metros utilizados: Valor R$'+FormatFloat('##,###0.00',StrToFloat(Edit1.Text))+'/Setor inicial: '+CmbSetor.Text+' - Setor Final: '+CmbSetorFim.Text);
               end;
             end
            else
             begin
               try
                  if MessageDlg('O relat�rio poder� demorar alguns minutos! Aguarde...', mtConfirmation, [mbOk], 0) = mrOk then
                   Screen.Cursor := crSQLWait;


                  Dm.QDivZeradosCodInt.Close;
                  Dm.QDivZeradosCodInt.SQL.Clear;
                  Dm.QDivZeradosCodInt.SQL.Add('SELECT CA.CAD_CODINT, CA.CAD_SETOR, CA.CAD_UN, CA.CAD_DESC, CA.CAD_PRECO, CA.CAD_QUANTIDADE, ');
                  Dm.QDivZeradosCodInt.SQL.Add('(CASE when ((CA.CAD_PRECO * CA.CAD_QUANTIDADE) < 1) then ((CA.CAD_PRECO * CA.CAD_QUANTIDADE) * -1) ');
                  Dm.QDivZeradosCodInt.SQL.Add('else (CAD_PRECO * CA.CAD_QUANTIDADE) end) as DIF_VALOR ');
                  Dm.QDivZeradosCodInt.SQL.Add(' from CONTAGENS CO right JOIN CADASTRO CA on (CA.CAD_EAN = CO.CON_EAN) ');
                  Dm.QDivZeradosCodInt.SQL.Add('where (CASE when ((CA.CAD_PRECO * CA.CAD_QUANTIDADE) < 1) then ((CA.CAD_PRECO * CA.CAD_QUANTIDADE) * -1) ');
                  Dm.QDivZeradosCodInt.SQL.Add('else (CAD_PRECO * CA.CAD_QUANTIDADE) end) >= :DIF_VALOR and (ca.cad_quantidade > 0) and (co.CON_QUANTIDADE is null) ');
                  Dm.QDivZeradosCodInt.SQL.Add(' AND (CA.CAD_SETOR >= :CAD_SETOR)  AND (CA.CAD_SETOR <= :CAD_SETORFIM) and (CO.CON_STATUS =  "." OR CO.CON_STATUS IS NULL) ');
                  DM.QDivZeradosCodInt.SQL.Add(' GROUP BY CA.CAD_CODINT, CA.CAD_DESC, CA.CAD_PRECO, CA.CAD_QUANTIDADE, CA.CAD_SETOR, CA.CAD_UN ORDER BY CA.CAD_DESC ');

                  if (CmbSetor.Text = '') and (CmbSetorFim.Text = '') then
                    begin
                    ShowMessage('Esse cliente n�o possui setores cadastrados, clique em "SEM DEPARTAMENTO"');
                    Dm.QDivZeradosCodInt.ParamByName('cad_setor').Value    :=0;
                    Dm.QDivZeradosCodInt.ParamByName('CAD_SETORFIM').Value :=0;

                    end
                  else
                     begin
                        Dm.QDivZeradosCodInt.ParamByName('cad_setor').Value    := CmbSetor.Text;
                        Dm.QDivZeradosCodInt.ParamByName('CAD_SETORFIM').Value := CmbSetorFim.Text;
                     end;
                    If Edit1.Text <> '' then
                    Dm.QDivZeradosCodInt.ParamByName('DIF_VALOR').Value := Edit1.Text
                    else
                    Dm.QDivZeradosCodInt.ParamByName('DIF_VALOR').Value :=0;

                    Dm.QDivZeradosCodInt.Open;
                    Dm.RvPrjWis.Close;
                    Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Rel_DivZerados_CODINT.rav';
                    Dm.RvPrjWis.Open;
                    Dm.RvSysWis.DefaultDest := rdPreview;
                    Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
                    Dm.RvPrjWis.ExecuteReport('Rel_DivZerados_CODINT');

                    // Salvando relat�rio em PDF automaticamente
                    Try
                      Dm.RvSysWis.DefaultDest:= rdFile;
                      Dm.RvSysWis.DoNativeOutput:= false;
                      Dm.RvSysWis.RenderObject:= rvPdf;
                      Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Diverg�ncia Itens N�o Contados ordenado por descri��o_Int'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
                      Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
                      Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Rel_DivZerados_CODINT.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
                      Dm.RvPrjWis.Engine:= Dm.RvSysWis;
                      Dm.RvPrjWis.Execute;
                    Except
                      ShowMessage('Erro ao gerar o relat�rio em PDF');
                    end;

                    AdicionaLog(DateTimeToStr(Now)+'> Gera��o de itens zerados por C�DIGO INTERNO/Departamento ordenado por descri��o realizado com sucesso! - Par�metros utilizados: Valor R$'+FormatFloat('##,###0.00',StrToFloat(Edit1.Text))+'/Setor inicial: '+CmbSetor.Text+' - Setor Final: '+CmbSetorFim.Text);
                    Screen.Cursor := crDefault;
               except
                AdicionaLog(DateTimeToStr(Now)+'> Erro na gera��o de itens zerados por C�DIGO INTERNO/Departamento ordenado por descri��o! - Par�metros utilizados: Valor R$'+FormatFloat('##,###0.00',StrToFloat(Edit1.Text))+'/Setor inicial: '+CmbSetor.Text+' - Setor Final: '+CmbSetorFim.Text);
               end
             end;
          end
         else
          begin
           If not CheckBoxCODINT.Checked then
            begin
             try
               if MessageDlg('O relat�rio poder� demorar alguns minutos! Aguarde...', mtConfirmation, [mbOk], 0) = mrOk then
               Screen.Cursor := crSQLWait;

                Dm.QDivZerados.Close;
                Dm.QDivZerados.SQL.Clear;
                Dm.QDivZerados.SQL.Add(' SELECT CA.CAD_CODINT, CA.CAD_EAN, CA.CAD_SETOR, CA.CAD_UN, CA.CAD_DESC, CA.CAD_PRECO, CA.CAD_QUANTIDADE, ');
                Dm.QDivZerados.SQL.Add(' (CASE WHEN ((CA.CAD_PRECO * CA.CAD_QUANTIDADE) < 1) THEN ((CA.CAD_PRECO * CA.CAD_QUANTIDADE) * -1) ELSE (CAD_PRECO * CA.CAD_QUANTIDADE) END) AS DIF_VALOR ');
                Dm.QDivZerados.SQL.Add(' FROM CONTAGENS CO RIGHT JOIN CADASTRO CA ON (CA.CAD_EAN = CO.CON_EAN) WHERE (CASE WHEN ((CA.CAD_PRECO * CA.CAD_QUANTIDADE) < 1) ');
                Dm.QDivZerados.SQL.Add(' THEN ((CA.CAD_PRECO * CA.CAD_QUANTIDADE) * -1) ELSE (CAD_PRECO * CA.CAD_QUANTIDADE) END) >= :DIF_VALOR ');
                Dm.QDivZerados.SQL.Add(' AND (CA.CAD_QUANTIDADE > 0) AND (CO.CON_QUANTIDADE IS NULL) AND (CO.CON_STATUS =  "." OR CO.CON_STATUS IS NULL) ');
                Dm.QDivZerados.SQL.Add(' GROUP BY CA.CAD_CODINT, CA.CAD_EAN, CA.CAD_DESC, CA.CAD_PRECO, CA.CAD_QUANTIDADE,CA.CAD_SETOR,CA.CAD_UN ');
                Dm.QDivZerados.SQL.Add(' ORDER BY CA.CAD_DESC ');
               If Edit1.Text <> '' then
                Dm.QDivZerados.ParamByName('DIF_VALOR').Value := Edit1.Text
               else
                Dm.QDivZerados.ParamByName('DIF_VALOR').Value := 0;
               Dm.QDivZerados.Open;
               Dm.RvPrjWis.Close;
               Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Rel_DivZerados.rav';
               Dm.RvPrjWis.Open;
               Dm.RvSysWis.DefaultDest := rdPreview;
               Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
               Dm.RvPrjWis.ExecuteReport('Rel_DivZerados');

                // Salvando relat�rio em PDF automaticamente
                Try
                  Dm.RvSysWis.DefaultDest:= rdFile;
                  Dm.RvSysWis.DoNativeOutput:= false;
                  Dm.RvSysWis.RenderObject:= rvPdf;
                  Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Diverg�ncia Itens N�o Contados ordenado por descri��o_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
                  Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
                  Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Rel_DivZerados.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
                  Dm.RvPrjWis.Engine:= Dm.RvSysWis;
                  Dm.RvPrjWis.Execute;
                Except
                  ShowMessage('Erro ao gerar o relat�rio em PDF');
                end;

               AdicionaLog(DateTimeToStr(Now)+'> Gera��o de itens zerados por EAN/Sem Departamento ordenado por descri��o realizado com sucesso! - Par�metros utilizados: Valor R$'+FormatFloat('##,###0.00',StrToFloat(Edit1.Text)));
               Screen.Cursor := crDefault;
             except
              AdicionaLog(DateTimeToStr(Now)+'> Erro na gera��o de itens zerados por EAN/Sem Departamento ordenado por descri��o! - Par�metros utilizados: Valor R$'+FormatFloat('##,###0.00',StrToFloat(Edit1.Text)));
             end;
            end
           else
            begin
             try
                if MessageDlg('O relat�rio poder� demorar alguns minutos! Aguarde...', mtConfirmation, [mbOk], 0) = mrOk then
               Screen.Cursor := crSQLWait;

                Dm.QDivZeradosCodInt.Close;
                Dm.QDivZeradosCodInt.SQL.Clear;
                Dm.QDivZeradosCodInt.SQL.Add('SELECT CA.CAD_CODINT, CA.CAD_SETOR, CA.CAD_UN, CA.CAD_DESC, CA.CAD_PRECO, CA.CAD_QUANTIDADE, ');
                Dm.QDivZeradosCodInt.SQL.Add('(CASE when ((CA.CAD_PRECO * CA.CAD_QUANTIDADE) < 1) then ((CA.CAD_PRECO * CA.CAD_QUANTIDADE) * -1) ');
                Dm.QDivZeradosCodInt.SQL.Add('else (CAD_PRECO * CA.CAD_QUANTIDADE) end) as DIF_VALOR ');
                Dm.QDivZeradosCodInt.SQL.Add(' from CONTAGENS CO right JOIN CADASTRO CA on (CA.CAD_EAN = CO.CON_EAN) ');
                Dm.QDivZeradosCodInt.SQL.Add('where (CASE when ((CA.CAD_PRECO * CA.CAD_QUANTIDADE) < 1) then ((CA.CAD_PRECO * CA.CAD_QUANTIDADE) * -1) ');
                Dm.QDivZeradosCodInt.SQL.Add('else (CAD_PRECO * CA.CAD_QUANTIDADE) end) >= :DIF_VALOR and (ca.cad_quantidade > 0) and (co.CON_QUANTIDADE is null) ');
                Dm.QDivZeradosCodInt.SQL.Add('and (CO.CON_STATUS =  "." OR CO.CON_STATUS IS NULL) ');
                DM.QDivZeradosCodInt.SQL.Add(' GROUP BY CA.CAD_CODINT, CA.CAD_DESC, CA.CAD_PRECO, CA.CAD_QUANTIDADE, CA.CAD_SETOR, CA.CAD_UN ORDER BY CA.CAD_DESC');
               If Edit1.Text <> '' then
                Dm.QDivZeradosCodInt.ParamByName('DIF_VALOR').Value := Edit1.Text
               else
                Dm.QDivZeradosCodInt.ParamByName('DIF_VALOR').Value := 0;
               Dm.QDivZerados.Open;
               Dm.RvPrjWis.Close;
               Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Rel_DivZerados_CODINT.rav';
               Dm.RvPrjWis.Open;
               Dm.RvSysWis.DefaultDest := rdPreview;
               Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
               Dm.RvPrjWis.ExecuteReport('Rel_DivZerados_CODINT');

                // Salvando relat�rio em PDF automaticamente
              Try
                Dm.RvSysWis.DefaultDest:= rdFile;
                Dm.RvSysWis.DoNativeOutput:= false;
                Dm.RvSysWis.RenderObject:= rvPdf;
                Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Diverg�ncia Itens N�o Contados ordenado por descri��o_Int'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
                Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
                Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Rel_DivZerados_CODINT.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
                Dm.RvPrjWis.Engine:= Dm.RvSysWis;
                Dm.RvPrjWis.Execute;
              Except
                ShowMessage('Erro ao gerar o relat�rio em PDF');
              end;

               AdicionaLog(DateTimeToStr(Now)+'> Gera��o de itens zerados por C�DIGO INTERNO/Sem Departamento ordenado por descri��o realizado com sucesso! - Par�metros utilizados: Valor R$'+FormatFloat('##,###0.00',StrToFloat(Edit1.Text)));
               Screen.Cursor := crDefault;
             except
              AdicionaLog(DateTimeToStr(Now)+'> Erro na gera��o de itens zerados por C�DIGO INTERNO/Sem Departamento ordenado por descri��o! - Par�metros utilizados: Valor R$'+FormatFloat('##,###0.00',StrToFloat(Edit1.Text)));
             end;
            end;
          end
       else
     If RadioGroup1.ItemIndex = 4 then
        try
         if MessageDlg('O relat�rio poder� demorar alguns minutos! Aguarde...', mtConfirmation, [mbOk], 0) = mrOk then
          Screen.Cursor:= crSQLWait;

          Dm.QDivLote.Close;
          Dm.QDivLote2.Close;
          If (Edit1.Text <> '') and (Edt_ValorFinal.Text <> '') then
           begin
            Dm.QDivLote.ParamByName('VALOR').Value := Edit1.Text;
            Dm.QDivLote.ParamByName('VALORFIM').Value := Edt_ValorFinal.Text;
              Dm.QDivLote.Open;
              cont := 0;
                     While not DM.QDivLote.Eof do
                      begin
                        cont := cont + 1;
                        DM.QDivLote.Next;
                      end;
              Dm.QDivLote2.Open;
              Dm.RvPrjWis.Close;
              Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Divergencia_Lote.rav';
              Dm.RvPrjWis.Open;
              DM.RvPrjWis.SetParam('ValorMin','Total de Itens Divergentes: ' + IntToStr(cont)) ;
              Dm.RvSysWis.DefaultDest := rdPreview;
              Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
              Dm.RvPrjWis.ExecuteReport('Divergencia_Lote');

              AdicionaLog(DateTimeToStr(Now)+'> Gera��o da diverg�ncia por valor financeiro realizada com sucesso! - Par�metros utilizados: Valor inicial R$'+FormatFloat('##,###0.00',StrToFloat(Edit1.Text))+'/Valor Final R$'+FormatFloat('##,###0.00',StrToFloat(Edt_ValorFinal.Text))+'/N�mero de itens divergentes: '+IntToStr(cont));
              Screen.Cursor := crDefault;

             // Salvando relat�rio em PDF automaticamente
              Try
                Dm.RvSysWis.DefaultDest:= rdFile;
                Dm.RvSysWis.DoNativeOutput:= false;
                Dm.RvSysWis.RenderObject:= rvPdf;
                Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Diverg�ncia Lote_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
                Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
                Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Divergencia_Lote.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
                Dm.RvPrjWis.Engine:= Dm.RvSysWis;
                Dm.RvPrjWis.Execute;
              Except
                ShowMessage('Erro ao gerar o relat�rio em PDF');
              end;

           end
          else
          Begin
           ShowMessage('Favor inserir os valores para gerar a Diverg�ncia');
           Screen.Cursor := crDefault;
          End;
        except
          AdicionaLog(DateTimeToStr(Now)+'> Erro na gera��o da diverg�ncia por Lote! - Par�metros utilizados: Valor inicial R$'+FormatFloat('##,###0.00',StrToFloat(Edit1.Text))+'/Valor Final R$'+FormatFloat('##,###0.00',StrToFloat(Edt_ValorFinal.Text)));
        end
      Else
        begin
          If RadioGroup1.ItemIndex = 5 then
           begin
             try

                if MessageDlg('O relat�rio poder� demorar alguns minutos! Aguarde...', mtConfirmation, [mbOk], 0) = mrOk then
                 Screen.Cursor := crSQLWait;

                 Dm2.QDivSemSaldoCadastro.Close;

                 Dm2.QDivSemSaldoCadastro.SQL.Clear;

                 if (not CheckBoxDiv.Checked) and (CmbSetor.Text = '') and (CmbSetorFim.Text = '') Then
                 begin
                   ShowMessage('Esse cliente n�o possui setores cadastrados, clique em "SEM DEPARTAMENTO"');
                   Screen.Cursor := crDefault;
                 end
                 else
                   begin
                   if CheckBoxDiv.Checked then
                     begin
                        Dm2.QDivSemSaldoCadastro.SQL.Add('SELECT CA.CAD_CODINT, '+
                                                  'CA.CAD_EAN, '+
                                                  'CO.QTD, CA.CAD_SETOR, CA.CAD_UN, CA.CAD_DESC, CA.CAD_PRECO, SUM(CA.CAD_QUANTIDADE) AS CAD_QUANTIDADE, ' +
                                                  '(CASE when ((CO.QTD - SUM(CA.CAD_QUANTIDADE)) IS NULL) then '+
                                                  '((0 - SUM(CA.CAD_QUANTIDADE)) * -1) '+
                                                  'else '+
                                                  '(CO.QTD - SUM(CA.CAD_QUANTIDADE)) end) as DIF_QTD, '+
                                                  '(CASE when ((CA.CAD_PRECO * (CO.QTD - SUM(CA.CAD_QUANTIDADE))) is null) then '+
                                                  '((CA.CAD_PRECO * (SUM(CA.CAD_QUANTIDADE))) * 1) '+
                                                  'else '+
                                                  '(CA.CAD_PRECO * (CO.QTD - SUM(CA.CAD_QUANTIDADE)) * 1) end) as DIF_VALOR2 '+
                                                  'from CONTAGENS_DIV CO '+
                                                  'INNER JOIN CADASTRO CA on (CA.CAD_CODINT = CO.CAD_CODINT) '+
                                                  'WHERE (CASE when ((CA.CAD_PRECO * CO.QTD) < 1) then '+
                                                  '((CA.CAD_PRECO * CO.QTD) * -1) '+
                                                  'else (CAD_PRECO * CO.QTD) end) >= :DIF_VALOR '+
                                                  'AND (CO.QTD IS NOT NULL) '+
                                                  'GROUP BY CA.CAD_CODINT, '+
                                                  'CO.QTD, CA.CAD_DESC, CA.CAD_PRECO, CA.CAD_SETOR, CA.CAD_UN, CA.CAD_EAN ' +
                                                  'HAVING ((SUM(CA.CAD_QUANTIDADE)) = 0) '+
                                                  'ORDER BY CAD_SETOR, DIF_VALOR2 DESC');
                      // Dm2.QDivSemSaldoCadastro.ParamByName('CAD_SETOR').Value    :=0;
                      // Dm2.QDivSemSaldoCadastro.ParamByName('CAD_SETORFIM').Value :=0;
                     end
                     Else
                     begin
                        Dm2.QDivSemSaldoCadastro.SQL.Add('SELECT CA.CAD_CODINT, '+
                                                  'CA.CAD_EAN, '+
                                                  'CO.QTD, CA.CAD_SETOR, CA.CAD_UN, CA.CAD_DESC, CA.CAD_PRECO, SUM(CA.CAD_QUANTIDADE) AS CAD_QUANTIDADE, ' +
                                                  '(CASE when ((CO.QTD - SUM(CA.CAD_QUANTIDADE)) IS NULL) then '+
                                                  '((0 - SUM(CA.CAD_QUANTIDADE)) * -1) '+
                                                  'else '+
                                                  '(CO.QTD - SUM(CA.CAD_QUANTIDADE)) end) as DIF_QTD, '+
                                                  '(CASE when ((CA.CAD_PRECO * (CO.QTD - SUM(CA.CAD_QUANTIDADE))) is null) then '+
                                                  '((CA.CAD_PRECO * (SUM(CA.CAD_QUANTIDADE))) * 1) '+
                                                  'else '+
                                                  '(CA.CAD_PRECO * (CO.QTD - SUM(CA.CAD_QUANTIDADE)) * 1) end) as DIF_VALOR2 '+
                                                  'from CONTAGENS_DIV CO '+
                                                  'INNER JOIN CADASTRO CA on (CA.CAD_CODINT = CO.CAD_CODINT) '+
                                                  'WHERE (CASE when ((CA.CAD_PRECO * CO.QTD) < 1) then '+
                                                  '((CA.CAD_PRECO * CO.QTD) * -1) '+
                                                  'else (CAD_PRECO * CO.QTD) end) >= :DIF_VALOR '+
                                                  'AND (CO.QTD IS NOT NULL) '+
                                                  'and (ca.cad_setor >=:cad_setor) '+
                                                  'and (ca.cad_setor <=:cad_setorfim) ' +
                                                  'GROUP BY CA.CAD_CODINT, '+
                                                  'CO.QTD, CA.CAD_DESC, CA.CAD_PRECO, CA.CAD_SETOR, CA.CAD_UN, CA.CAD_EAN ' +
                                                  'HAVING ((SUM(CA.CAD_QUANTIDADE)) = 0) '+
                                                  'ORDER BY CAD_SETOR, DIF_VALOR2 DESC');

                       Dm2.QDivSemSaldoCadastro.ParamByName('CAD_SETOR').Value    := CmbSetor.Text;
                       Dm2.QDivSemSaldoCadastro.ParamByName('CAD_SETORFIM').Value := CmbSetorFim.Text;
                     end;
                     If Edit1.Text <> '' then
                       Dm2.QDivSemSaldoCadastro.ParamByName('DIF_VALOR').Value := Edit1.Text
                     else
                       Dm2.QDivSemSaldoCadastro.ParamByName('DIF_VALOR').Value :=0;

                     Dm2.QDivSemSaldoCadastro.Open;
                     Dm.RvPrjWis.Close;
                     Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Divergencia_SemSaldoCadastro.rav';
                     Dm.RvPrjWis.Open;
                     Dm.RvSysWis.DefaultDest := rdPreview;
                     Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
                     Dm.RvPrjWis.ExecuteReport('Rel_DivSemSaldoCadastro');

                     //AdicionaLog(DateTimeToStr(Now)+'> Gera��o de itens zerados por EAN/Departamento realizado com sucesso! - Par�metros utilizados: Valor R$'+FormatFloat('##,###0.00',StrToFloat(Edit1.Text))+'/Setor inicial: '+CmbSetor.Text+' - Setor Final: '+CmbSetorFim.Text);
                     Screen.Cursor := crDefault;

                    // Salvando relat�rio em PDF E TXT automaticamente
                     Try
                       Dm.RvSysWis.DefaultDest:= rdFile;
                       Dm.RvSysWis.DoNativeOutput:= false;
                       Dm.RvSysWis.RenderObject:= rvtxt;
                       if CheckBoxDiv.Checked then
                         Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Divergencia_ItensSobra_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString + '.txt' //+'_'+Dm2.QDivSemSaldoCadastro.ParamByName('CAD_SETORFIM').Value+')'+'.txt'; //caminho onde vai gerar o arquivo pdf
                       Else
                         Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Divergencia_ItensSobra_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'_'+ StringReplace(Dm2.QDivSemSaldoCadastro.ParamByName('CAD_SETOR').Value,'*', '', [rfReplaceAll, rfIgnoreCase]) + '.txt'; //+'_'+Dm2.QDivSemSaldoCadastro.ParamByName('CAD_SETORFIM').Value+')'+'.txt'; //caminho onde vai gerar o arquivo pdf
                       Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
                       Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Divergencia_SemSaldoCadastro.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
                       Dm.RvPrjWis.Engine:= Dm.RvSysWis;
                       Dm.RvPrjWis.Execute;
                     Except
                       ShowMessage('Erro ao gerar o relat�rio em TXT');
                     end;

                     Try
                       Dm.RvSysWis.DefaultDest:= rdFile;
                       Dm.RvSysWis.DoNativeOutput:= false;
                       Dm.RvSysWis.RenderObject:= rvPdf;
                       if CheckBoxDiv.Checked then
                         Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Divergencia_ItensSobra_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString + '.pdf' //+'_'+Dm2.QDivSemSaldoCadastro.ParamByName('CAD_SETORFIM').Value+')'+'.txt'; //caminho onde vai gerar o arquivo pdf
                       Else
                         Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Divergencia_ItensSobra_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'_' + StringReplace(Dm2.QDivSemSaldoCadastro.ParamByName('CAD_SETOR').Value,'*', '', [rfReplaceAll, rfIgnoreCase]) + '.pdf'; //+'_'+Dm2.QDivSemSaldoCadastro.ParamByName('CAD_SETORFIM').Value+')'+'.txt'; //caminho onde vai gerar o arquivo pdf
                       Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
                       Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Divergencia_SemSaldoCadastro.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
                       Dm.RvPrjWis.Engine:= Dm.RvSysWis;
                       Dm.RvPrjWis.Execute;
                     Except
                       ShowMessage('Erro ao gerar o relat�rio em PDF');
                     end;
                   end;
                // end;
              except
                //AdicionaLog(DateTimeToStr(Now)+'> Erro na gera��o de itens zerados por EAN/Departamento! - Par�metros utilizados: Valor R$'+FormatFloat('##,###0.00',StrToFloat(Edit1.Text))+'/Setor inicial: '+CmbSetor.Text+' - Setor Final: '+CmbSetorFim.Text);
              end;
           end
       // end
        Else
          begin
            If RadioGroup1.ItemIndex = 6 then
              begin
                try
                 if MessageDlg('O relat�rio poder� demorar alguns minutos! Aguarde...', mtConfirmation, [mbOk], 0) = mrOk then
                  Screen.Cursor:= crSQLWait;

                  Dm.QDiv1.Close;
                  DM.QDiv1.SQL.Clear;
                  dm.QDiv1.SQL.Add('SELECT * FROM divergencia ');
                  Dm.QDiv1.Open;

                  cont := 0;

                  While not DM.QDiv1.Eof do
                    begin
                      cont := cont + 1;
                      DM.QDiv1.Next;
                    end;
                    //  Dm.QDiv2.Open;
                  Dm.RvPrjWis.Close;
                  Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Divergencia_REsumo.rav';
                  Dm.RvPrjWis.Open;
                  DM.RvPrjWis.SetParam('ValorMin','Total de Itens Divergentes: ' + IntToStr(cont)) ;
                //  DM.RvPrjWis.SetParam('EAN',Dm.QDiv1EAN.Value) ;
                  Dm.RvSysWis.DefaultDest := rdPreview;
                  Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
                  Dm.RvPrjWis.ExecuteReport('Divergencia_Resumo');

              //    AdicionaLog(DateTimeToStr(Now)+'> Gera��o da diverg�ncia por valor financeiro ordenada por valor realizada com sucesso! - Par�metros utilizados: Valor inicial R$'+FormatFloat('##,###0.00',StrToFloat(Edit1.Text))+'/Valor Final R$'+FormatFloat('##,###0.00',StrToFloat(Edt_ValorFinal.Text))+'/N�mero de itens divergentes: '+IntToStr(cont));
                  Screen.Cursor := crDefault;

                 // Salvando relat�rio em PDF automaticamente
                  Try
                    Dm.RvSysWis.DefaultDest:= rdFile;
                    Dm.RvSysWis.DoNativeOutput:= false;
                    Dm.RvSysWis.RenderObject:= rvPdf;
                    Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Diverg�ncia_Resumo '+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
                    Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
                    Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Divergencia_Resumo.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
                    Dm.RvPrjWis.Engine:= Dm.RvSysWis;
                    Dm.RvPrjWis.Execute;
                  Except
                    ShowMessage('Erro ao gerar o relat�rio em PDF');
                  end;

                except
                  AdicionaLog(DateTimeToStr(Now)+'> Erro na gera��o da diverg�ncia por valor financeiro! - Par�metros utilizados: Valor inicial R$'+FormatFloat('##,###0.00',StrToFloat(Edit1.Text))+'/Valor Final R$'+FormatFloat('##,###0.00',StrToFloat(Edt_ValorFinal.Text)));
                end;
          end
     end;
   end;
 Except
  ShowMEssage('Erro ao gerar a diverg�ncia');
 end;
end;


procedure TFrmDivergencia.RadioGroup1Click(Sender: TObject);
begin
If RadioGroup1.ItemIndex = 0 then
  begin
    If  (dm.IBInventarioINV_LOJA.Text = 'CP1') OR (dm.IBInventarioINV_LOJA.Text = 'CP2') OR (dm.IBInventarioINV_LOJA.Text = 'CP3')  then
    begin
      Edt_ValorFinal.Visible:= true; // Desabilita o a caixa de texto valor final
      Lbl_ValorFinal.Visible:= True; // Desabilita a o label valor final
      Label2.Visible:=True;
      label3.Visible:=False;
      Label4.Visible:=False;
      Edit1.Visible := True;
      CmbSetor.Visible := False;
      CmbSetorFim.Visible:=False;
      CheckBoxDiv.Visible := False;
      CheckBoxCODINT.Visible := False;
      Label2.Caption := 'Valor MIN. Quantidade: '  ;
      Lbl_ValorFinal.Caption := 'Valor MAX. Quantidade: '  ;
      RgOrdenar.Visible := False;
    end
      else
        if dm.IBInventarioINV_LOJA.Text = 'BGB' Then
        begin
           Edt_ValorFinal.Text := '99999999999';
           Edit1.Text := '1';
           Edt_ValorFinal.Visible:= False; // Desabilita o a caixa de texto valor final
           Lbl_ValorFinal.Visible:= False; // Desabilita a o label valor final
           Label2.Visible:=False;
           label3.Visible:=False;
           Label4.Visible:=False;
           Edit1.Visible := False;
           CmbSetor.Visible := False;
           CmbSetorFim.Visible:=False;
           CheckBoxDiv.Visible := False;
           CheckBoxCODINT.Visible := False;
           Label2.Caption := 'Valor MIN. Financeiro (R$): '  ;
           Lbl_ValorFinal.Caption := 'Valor MAX. Financeiro (R$): ';
           RgOrdenar.Visible := True;
          End

        Else
          begin
           Edt_ValorFinal.Visible:= True; // Desabilita o a caixa de texto valor final
           Lbl_ValorFinal.Visible:= True; // Desabilita a o label valor final
           Label2.Visible:=True;
           label3.Visible:=False;
           Label4.Visible:=False;
           Edit1.Visible := True;
           CmbSetor.Visible := False;
           CmbSetorFim.Visible:=False;
           CheckBoxDiv.Visible := False;
           CheckBoxCODINT.Visible := False;
           Label2.Caption := 'Valor MIN. Financeiro (R$): '  ;
           Lbl_ValorFinal.Caption := 'Valor MAX. Financeiro (R$): ';
           RgOrdenar.Visible := True;
          End
    end
else
 If RadioGroup1.ItemIndex = 1 then
    begin
      Edt_ValorFinal.Visible:=True; // Desabilita o a caixa de texto valor final
      Lbl_ValorFinal.Visible:=True; // Desabilita a o label valor final
      Label2.Visible:=True;
      label3.Visible:=True;
      Label4.Visible:=True;
      CmbSetor.Visible := true;
      CmbSetorFim.Visible:= True;
     if DM.IBInventarioINV_LOJA.Value = 'DME' then
      begin
        CheckBoxDiv.Visible := True;
        CheckBoxDiv.Caption := 'CONTROLADOS';
      end
     else
      begin
        CheckBoxDiv.Visible := False;
        CheckBoxDiv.Caption := 'SEM DEPARTAMENTO';
      end;
      CheckBoxCODINT.Visible := False;
      Edit1.Visible := true;
      Label2.Caption := 'Valor Financeiro (R$): ';
      Lbl_ValorFinal.Caption := 'Valor MAX. Financeiro (R$): '  ;
      CmbSetor.Items.Clear;
      CmbSetorFim.Items.Clear;
      Dm.Qsetor.Close;
      Dm.Qsetor.Open;
      Dm.Qsetor.First;
      Try While Not Dm.Qsetor.Eof do
        begin
          CmbSetor.Items.Add(Dm.QsetorCAD_SETOR.Value);
          CmbSetorFim.Items.Add(Dm.QsetorCAD_SETOR.Value);
          Dm.Qsetor.Next;
        End;
      Except
      End;
      RgOrdenar.Visible := False;
    End
 Else
  If (RadioGroup1.ItemIndex = 2) or (RadioGroup1.ItemIndex = 5) Then
    Begin

      Edt_ValorFinal.Visible:=False; // Desabilita o a caixa de texto valor final
      Lbl_ValorFinal.Visible:=False; // Desabilita a o label valor final

     CheckBoxDiv.Visible := True;
     CheckBoxCODINT.Visible := True;
     RgOrdenar.Visible := True;
     If RadioGroup1.ItemIndex = 5 then
       CheckBoxCODINT.Visible := False;

        if CheckBoxDiv.Checked then
         begin
         Label2.Caption := 'Valor Financeiro (R$): ';
         CmbSetor.Visible := False;
         Label3.Visible := False;
         end
        else
          Label2.Caption := 'Valor Financeiro (R$): ';
          Label2.visible:=True;
          Label3.Visible:=True;
          Label4.Visible:=True;
          Edit1.Visible:=True;
          CmbSetor.Visible:=True;
          CmbSetorFim.Visible:=True;
          CmbSetor.Items.Clear;
          CmbSetorFim.Items.Clear;
          Dm.Qsetor.Close;
          Dm.Qsetor.Open;
          Dm.Qsetor.First;
      Try While Not Dm.Qsetor.Eof do
        begin
          CmbSetor.Items.Add(Dm.QsetorCAD_SETOR.Value);
          CmbSetorFim.Items.Add(Dm.QsetorCAD_SETOR.Value);
          Dm.Qsetor.Next;
        End;
      Except
      End;
    End
  else
If RadioGroup1.ItemIndex = 3 then
  begin
    Edt_ValorFinal.Visible:=True;// Desabilita o a caixa de texto valor final
    Lbl_ValorFinal.Visible:=True;// Desabilita a o label valor final
    Label2.Visible:=True;
    label3.Visible:=False;
    Label4.Visible:=False;
    Edit1.Visible := True;
    CmbSetor.Visible := False;
    CmbSetorFim.Visible:=False;
    CheckBoxDiv.Visible := False;
    CheckBoxCODINT.Visible := False;
    Label2.Caption := 'Diverg�ncia Cega Min. (R$): ';
    Lbl_ValorFinal.Caption:= 'Diverg�ncia Cega M�x. (R$): ';
    RgOrdenar.Visible := False;
  End
else
   {If RadioGroup1.ItemIndex = 4 then
  begin
    Edt_ValorFinal.Visible:=True;// habilita a caixa de texto do valor final para a diverg�ncia por endere�o
    Lbl_ValorFinal.Visible:=True; // habilita o label valor final para a diverg�ncia por endere�o
    Label2.Visible:=True;
    label3.Visible:=False;
    Label4.Visible:=False;
    Edit1.Visible := True;
    CmbSetor.Visible := False;
    CmbSetorFim.Visible:=False;
    CheckBoxDiv.Visible := False;
    CheckBoxCODINT.Visible := False;
    Label2.Caption := 'Valor da Diverg�ncia Min. :';
  End }
  If RadioGroup1.ItemIndex = 4 then
  begin
   Edt_ValorFinal.Visible:= True; // Desabilita o a caixa de texto valor final
   Lbl_ValorFinal.Visible:= True; // Desabilita a o label valor final
   Label2.Visible:=True;
   label3.Visible:=False;
   Label4.Visible:=False;
   Edit1.Visible := True;
   CmbSetor.Visible := False;
   CmbSetorFim.Visible:=False;
   CheckBoxDiv.Visible := False;
   CheckBoxCODINT.Visible := False;
   Label2.Caption := 'Valor MIN. Financeiro (R$): '  ;
   Lbl_ValorFinal.Caption := 'Valor MAX. Financeiro (R$): ';
   RgOrdenar.Visible := False;
  End
Else
  begin
    If RadioGroup1.ItemIndex = 6 then
    begin
      Edt_ValorFinal.Visible:= false; // Desabilita o a caixa de texto valor final
      Lbl_ValorFinal.Visible:= false; // Desabilita a o label valor final
      Label2.Visible:=False;
      label3.Visible:=False;
      Label4.Visible:=False;
      Edit1.Visible := False;
      CmbSetor.Visible := False;
      CmbSetorFim.Visible:=False;
      CheckBoxDiv.Visible := False;
      CheckBoxCODINT.Visible := False;
      RgOrdenar.Visible := False;
    end
  end;
end;

procedure TFrmDivergencia.CmbSetorFimClick(Sender: TObject);
begin
      if CmbSetorFim.Text < CmbSetor.Text   then
          begin
           ShowMessage('O Setor final n�o pode ser menor que o setor inicial');
          end;
end;


procedure TFrmDivergencia.CmbSetorClick(Sender: TObject);
begin
      if (CmbSetorFim.Text <> '') and (CmbSetorFim.Text < CmbSetor.Text) then
          begin
           ShowMessage('O Setor final n�o pode ser menor que o setor inicial');
          end;
end;

end.

procedure TFrmDivergencia.CmbSetorClick(Sender: TObject);
begin
      if (CmbSetorFim.Text <> '') and (CmbSetorFim.Text < CmbSetor.Text) then
          begin
           ShowMessage('O Setor final n�o pode ser menor que o setor inicial');
          end;
end;

end.
//////////DIVERGENCIA ENDERE�O////////////////////
{If RadioGroup1.ItemIndex = 4 then
              begin
               if MessageDlg('O relat�rio poder� demorar alguns minutos! Aguarde...', mtConfirmation, [mbOk], 0) = mrOk then
                Screen.Cursor := crSQLWait;


                DM.QDivEndereco.Close;
                If (Edit1.Text <> '') and (Edt_ValorFinal.Text <> '') then
                  begin
                    Dm.QDivEndereco.ParamByName('DIF_VALOR').Value      := Edit1.Text;
                    Dm.QDivEndereco.ParamByName('DIF_VALORFINAL').Value := Edt_ValorFinal.Text;
                  end
                else
                  begin
                    Dm.QDivEndereco.ParamByName('DIF_VALOR').Value := 1;
                    Dm.QDivEndereco.ParamByName('DIF_VALORFINAL').Value := 100;
                  end;

                    Dm.QDivEndereco.Open;
                    Dm.RvPrjWis.Close;
                    Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\DivergenciaEnd.rav';
                    Dm.RvPrjWis.Open;
                    //DM.RvPrjWis.SetParam('ValorMin','VLR. MIN. : R$ ' + edit1.Text);
                    //DM.RvPrjWis.SetParam('ValorMax','VLR. MAX. : R$ ' + Edt_ValorFinal.Text);
                    Dm.RvSysWis.DefaultDest := rdPreview;
                    Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
                    Dm.RvPrjWis.ExecuteReport('DivergenciaEnd');

                 Screen.Cursor := crDefault;
              end;}


