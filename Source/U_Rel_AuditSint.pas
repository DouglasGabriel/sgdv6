unit U_Rel_AuditSint;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DBCtrls, FileCtrl, Buttons, ExtCtrls, ComCtrls, RpRave,
  DB, RpCon, RpConDS, RpBase, RpSystem, RpDefine, Grids, DBGrids, RpRender,
  RpRenderPDF, RpRenderText,IBQuery;

type
  TFrmAudit_Sint = class(TForm)
    RadioGroup1: TRadioGroup;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    SpeedButton1: TSpeedButton;
    Edit1: TEdit;
    btnimportar: TButton;
    OpenDialog1: TOpenDialog;
    GroupBox2: TGroupBox;
    DBGrid1: TDBGrid;
    Edit2: TEdit;
    RadioGroup2: TRadioGroup;
    GroupBox3: TGroupBox;
    BtnGeraRel: TButton;
    ListBox1: TListBox;
    Edit3: TEdit;
    DBGrid2: TDBGrid;
    rvTxt: TRvRenderText;
    BtnSelectall: TButton;
    BtnClear: TButton;
    procedure SpeedButton1Click(Sender: TObject);
    procedure btnimportarClick(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Edit2Change(Sender: TObject);
    procedure Edit3Change(Sender: TObject);
    procedure RadioGroup2Click(Sender: TObject);
    procedure DBGrid2DblClick(Sender: TObject);
    procedure ListBox1DblClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure BtnGeraRelClick(Sender: TObject);
    procedure BtnClearClick(Sender: TObject);
    procedure BtnSelectallClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmAudit_Sint: TFrmAudit_Sint;

implementation

uses UFuncProc,UDm, Math;
{$R *.dfm}

procedure TFrmAudit_Sint.SpeedButton1Click(Sender: TObject);
begin
  OpenDialog1.InitialDir := ExtractFilePath(Application.ExeName);
  if OpenDialog1.Execute then
  Edit1.Text := OpenDialog1.FileName;
end;

procedure TFrmAudit_Sint.btnimportarClick(Sender: TObject);
var nomearq: string;
    Txt: Textfile;
    Entrada: string;
begin
 case RadioGroup1.ItemIndex  of
    0:
      if Edit1.Text = '' then
      begin
        Application.MessageBox('Aten��o, arquivo para importa��o n�o informado',' Aten��o.:',MB_APPLMODAL + MB_OK + MB_ICONWARNING );
        Exit;
      end
      else
      begin
       case RadioGroup2.ItemIndex  of
        0:
        begin
          if MessageDlg('Aten��o, arquivo Importado para relat�rio de c�digos internos. Deseja realmente realizar essa importa��o!?', mtConfirmation, [mbYes,mbNo], 0) = mrYes then
          begin
           nomearq := Edit1.Text;
           DM.QAuditSint.Close;
           AssignFile(Txt,nomearq); // NOME do arquivo texto
           Reset(Txt);
           While not Eoln(Txt) do
           Begin
            Readln(Txt,Entrada);
            DM.QAuditSint.ParamByName('FILTRO').Value := Entrada;
            DM.QAuditSint.ExecSQL;
           End;
             Dm.Transacao.CommitRetaining;
             CloseFile(Txt);
             Dm.QAuditSint.close;
             Application.MessageBox('Importa��o conclu�da, o relat�rio j� pode ser gerado!','Mensagem do Sistema', MB_ICONINFORMATION+ MB_OK);
          end
          else
          begin
           Exit;
          end;  
        end;

        1:
        begin
          if MessageDlg('Aten��o, arquivo Importado para relat�rio de se��es. Deseja realmente realizar essa importa��o!?', mtConfirmation, [mbYes,mbNo], 0) = mrYes then
          begin
           nomearq := Edit1.Text;
           DM.QAuditSint.Close;
           AssignFile(Txt,nomearq); // NOME do arquivo texto
           Reset(Txt);
           While not Eoln(Txt) do
           Begin
            Readln(Txt,Entrada);
            DM.QAuditSint.ParamByName('FILTRO').Value := Entrada;
            DM.QAuditSint.ExecSQL;
           End;
             Dm.Transacao.CommitRetaining;
             CloseFile(Txt);
             Dm.QAuditSint.close;
             Application.MessageBox('Importa��o conclu�da, o relat�rio j� pode ser gerado!','Mensagem do Sistema', MB_ICONINFORMATION+ MB_OK);
          end
          else
          begin
           Exit;
          end;  
        end;
       end;


      end;
 end;
end;


procedure TFrmAudit_Sint.RadioGroup1Click(Sender: TObject);
begin
  case RadioGroup1.ItemIndex  of
    0:
    begin
     GroupBox1.Visible:= True;
     GroupBox2.Visible:=False;
     ListBox1.Clear;
    end;
    1:
    begin
     GroupBox1.Visible:= False;
     GroupBox2.Visible:= True;
     ListBox1.Clear;
    end;
  end;
end;

procedure TFrmAudit_Sint.FormCreate(Sender: TObject);
begin
  dm.QInterno.Close;
  DM.QInterno.Open;
  DM.QFiltroSecao.Close;
  DM.QFiltroSecao.Open;
  GroupBox2.Visible := False;
end;

procedure TFrmAudit_Sint.Edit2Change(Sender: TObject);
begin
 Dm.QInterno.locate('INTERNO',Edit2.Text,[locaseinsensitive,loPartialKey]);
end;

procedure TFrmAudit_Sint.Edit3Change(Sender: TObject);
begin
 Dm.QFiltroSecao.locate('SEC_SECAO',Edit3.Text,[locaseinsensitive,loPartialKey]);
end;

procedure TFrmAudit_Sint.RadioGroup2Click(Sender: TObject);
begin
  case RadioGroup2.ItemIndex  of
    0:
    begin
     DBGrid1.Visible := True;
     Edit2.Visible := True;
     DBGrid2.Visible := False;
     Edit3.Visible:= False;
     ListBox1.Clear;
     DM.QInterno.Close;
     DM.QInterno.Open
    end;
    1:
    begin
     DBGrid1.Visible := False;
     Edit2.Visible := False;
     DBGrid2.Visible := True;
     Edit3.Visible:= True;
     ListBox1.Clear;
     DM.QFiltroSecao.Close;
     DM.QFiltroSecao.Open;
    end;
  end;
end;

procedure TFrmAudit_Sint.DBGrid2DblClick(Sender: TObject);
begin
 if ListBox1.Items.IndexOf(DBGrid2.Columns[0].Field.Text) = -1 then
  begin
   ListBox1.Items.Add(DBGrid2.Columns[0].Field.Text);
   Edit3.Clear;
  end
 else
  begin
   ShowMessage('Item j� adicionado, por favor digite outro item');
   Edit3.Clear;
  end;  


end;

procedure TFrmAudit_Sint.ListBox1DblClick(Sender: TObject);
begin
 ListBox1.DeleteSelected;
end;

procedure TFrmAudit_Sint.DBGrid1DblClick(Sender: TObject);
begin
 if ListBox1.Items.IndexOf(DBGrid1.Columns[0].Field.Text) = -1 then
  begin
   ListBox1.Items.Add(DBGrid1.Columns[0].Field.Text);
   Edit2.Clear;
  end
 else
  begin
   ShowMessage('Item j� adicionado, por favor digite outro item');
   Edit2.Clear;
  end;

end;

procedure TFrmAudit_Sint.BtnGeraRelClick(Sender: TObject);
var nomearq: string;
    Txt: Textfile;
    Entrada: string;
begin
  case RadioGroup2.ItemIndex  of
    0:
    begin

       ListBox1.Items.SaveToFile('C:\SGD\Relatorios\Audit_Sint_Internos.txt');
       nomearq := ('C:\SGD\Relatorios\Audit_Sint_Internos.txt');
       DM.QAuditSint.Close;
       AssignFile(Txt,nomearq); // NOME do arquivo texto
       Reset(Txt);
       While not Eoln(Txt) do
       Begin
        Readln(Txt,Entrada);
        DM.QAuditSint.ParamByName('FILTRO').Value := Entrada;
        DM.QAuditSint.ExecSQL;
       End;
       Dm.Transacao.CommitRetaining;
       CloseFile(Txt);
       Dm.QAuditSint.close;
       DM.QAuditSintInt.Close;
       DM.QAuditSintInt.Open;
       if DM.QAuditSintInt.RecordCount <> 0 then
       begin
        GeraRelAuditSintInt;
       end
       else
       begin
        Application.MessageBox('Aten��o, os c�digos selecionados n�o foram contados. Selecione os c�digos novamente!','Mensagem do Sistema', MB_ICONWARNING + MB_OK);
       end;



    end;
    1:
    begin
       ListBox1.Items.SaveToFile('C:\SGD\Relatorios\Audit_Sint_Se��es.txt');
       nomearq := ('C:\SGD\Relatorios\Audit_Sint_Se��es.txt');
       DM.QAuditSint.Close;
       AssignFile(Txt,nomearq); // NOME do arquivo texto
       Reset(Txt);
       While not Eoln(Txt) do
       Begin
        Readln(Txt,Entrada);
        DM.QAuditSint.ParamByName('FILTRO').Value := Entrada;
        DM.QAuditSint.ExecSQL;
       End;
       Dm.Transacao.CommitRetaining;
       CloseFile(Txt);
       Dm.QAuditSint.close;
       DM.QAuditSintSecao.Close;
       DM.QAuditSintSecao.Open;
       if DM.QAuditSintSecao.RecordCount <> 0 then
       begin
        GeraRelAuditSintSecao
       end
       else
       begin
        Application.MessageBox('Aten��o, as Se��es selecionadas n�o foram contadas. Selecione as Se��es novamente!','Mensagem do Sistema', MB_ICONWARNING + MB_OK);
       end;
    end;
  end;
end;
procedure TFrmAudit_Sint.BtnClearClick(Sender: TObject);
begin
 ListBox1.Clear;
 DM.QInterno.Close;
 dm.QInterno.Open;
 DM.QFiltroSecao.Close;
 DM.QFiltroSecao.Open;
end;

procedure TFrmAudit_Sint.BtnSelectallClick(Sender: TObject);
begin
 case RadioGroup2.ItemIndex  of
   0:
   begin
    dm.QInterno.First;
    while not DM.QInterno.Eof do
    begin
     ListBox1.Items.Add(DM.QInternoINTERNO.AsString);
     DM.QInterno.Next;
    end;
   end;
    1:
   begin
    DM.QFiltroSecao.First;
    while not DM.QFiltroSecao.Eof do
    begin
     ListBox1.Items.Add(DM.QFiltroSecaoSEC_SECAO.AsString);
     DM.QFiltroSecao.Next;
    end;
   end;
 end;
end;

end.
