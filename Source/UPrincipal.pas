unit UPrincipal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, Menus, Buttons, StdCtrls, ImgList, acPNG,
  jpeg, System.ImageList;

type
  TFrmPrincipal = class(TForm)
    MainMenu1: TMainMenu;
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    OpenDialog1: TOpenDialog;
    Atualizao1: TMenuItem;
    MasterLibrary1: TMenuItem;
    Panel2: TPanel;
    btnCadastro: TSpeedButton;
    btnImport: TSpeedButton;
    btnAbrir: TSpeedButton;
    btnRestore: TSpeedButton;
    btnBackup: TSpeedButton;
    btnClose: TSpeedButton;
    Label1: TLabel;
    Label2: TLabel;
    Image: TImageList;
    Layout1: TMenuItem;
    CriarumNovoLayoutdeEntrada1: TMenuItem;
    AdicionarNovoLayoutdeSada1: TMenuItem;
    EditarumLayoutexistente1: TMenuItem;
    N1: TMenuItem;
    MostrartodososLayouts1: TMenuItem;
    Configurao1: TMenuItem;
    ConfigurarservidorFTP1: TMenuItem;
    ImageLogo: TImage;
    procedure CadastrarNovoInventrio1Click(Sender: TObject);
    procedure AbrirInventrio1Click(Sender: TObject);
    procedure ImportarCadastro1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure RestaurarTransmisses1Click(Sender: TObject);
    procedure BackupInventrio1Click(Sender: TObject);
    procedure MasterLibrary1Click(Sender: TObject);
    procedure btnCadastroClick(Sender: TObject);
    procedure btnImportClick(Sender: TObject);
    procedure btnAbrirClick(Sender: TObject);
    procedure btnRestoreClick(Sender: TObject);
    procedure btnBackupClick(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure CriarumNovoLayoutdeEntrada1Click(Sender: TObject);
    procedure AdicionarNovoLayoutdeSada1Click(Sender: TObject);
    procedure EditarumLayoutexistente1Click(Sender: TObject);
    procedure ConfigurarservidorFTP1Click(Sender: TObject);
    procedure MostrartodososLayouts1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmPrincipal: TFrmPrincipal;

implementation

uses UCadInv, UInventario, UImportaCad, ULogin, UDm, URestTransm,
  UBackupInvent, UFuncProc, uFrmMasterLibrary, ULayout, UFrmLayoutOutput,
  UFrmLayoutExiste, UFrmConfigSMTP, UFrmUpdate, UFrmAllLayouts;

{$R *.dfm}

procedure TFrmPrincipal.CadastrarNovoInventrio1Click(Sender: TObject);
begin
  FrmCadInv := TFrmCadInv.Create(FrmCadInv);
  try
   FrmCadInv.ShowModal;
  finally
   FrmCadInv.Release;
   FrmCadInv := nil;
  end;
end;
procedure TFrmPrincipal.AbrirInventrio1Click(Sender: TObject);
begin
 FrmInventario := TFrmInventario.Create(FrmInventario);
 try
   //Abrir DataSet Mapeamento
   Dm.IBMapeamento.ParamByName('secini').Value := '0000';
   Dm.IBMapeamento.ParamByName('secfin').Value := '9999';
   Dm.IBMapeamento.Open;
   //Abrir DataSet Se��o
   DM.QIBSecao.Active := true;
 //  Dm.IBSecao.Params.ParamByName('secini').Value := '0';
  // Dm.IBSecao.Params.ParamByName('secfin').Value := 'ZZZZZZZZZZZZZZZZZZZZZZZZZ';
   Dm.IBSecao.Open;
   //Abrir Transmiss�es
   Dm.IBTransmissao.Open;
   //Abrir Area
   Dm.IBArea.Open;
   //Abrir Contagens
   Dm.IBContagens.Open;
  // Dm.CdsContagens.Open;
   //Abrir tabela Roster
   Dm.IBRoster.Open;
   //Abrir tabela Horario
   Dm.IBHorarios.Open;

   FrmInventario.ShowModal;

 finally
   FrmInventario.Release;
   FrmInventario := nil;   
 end;

end;

procedure TFrmPrincipal.ImportarCadastro1Click(Sender: TObject);
begin
FrmImportaCad := TFrmImportaCad.Create(FrmImportaCad);
try
  FrmImportaCad.ShowModal;
finally
  FrmImportaCad.Release;
  FrmImportaCad := nil;
end;


end;

procedure TFrmPrincipal.FormShow(Sender: TObject);
var 
  FlDt: Integer; 
begin 

  FlDt := FileAge(ParamStr(0));
  Label2.Caption := 'Criado em: '+DateToStr(FileDateToDateTime(FlDt));
//Abre tabela de Inventarios
 Dm.IBInventario.Open;
//Procedure para verificar se existem todos os diret�rios do sistema. Caso n�o exista � criado
 CriaDirSgd;
 Label1.Caption := 'v'+VersaoSGD;

 //Label1.Caption := 'V3.9.2.1';

 //Se n�vel do usu�rio OPER
 If (NivelUsu = 2) then
    Begin
     BtnCadastro.Enabled := False; //Menu de Cadastro de Invent�rio
     BtnRestore.Enabled := False; //Menu de Restaurar Invent�rio
     MainMenu1.Items[0].Enabled := false;
     MainMenu1.Items[1].Enabled := false;
     MainMenu1.Items[2].Enabled := false;
     If (Dm.IBInventarioINV_CADASTRO.Value = 'T') or (DM.IBInventarioINV_LOJA.Value = 'NFE') then
       Begin
         BtnAbrir.Enabled := True; //Menu de Abrir Invent�rio
         BtnBackup.Enabled := True; //Menu de Backup de Invent�rio

       End;
    End;

 //Se n�vel do usu�rio ADMIN
 If (NivelUsu = 1) then
    Begin
     BtnCadastro.Enabled := true; //Menu de Cadastro de Invent�rio
     BtnRestore.Enabled := true; //Menu de Restaurar Invent�rio
     MainMenu1.Items[0].Enabled := false;
     MainMenu1.Items[1].Enabled := false;
     MainMenu1.Items[2].Enabled := false;

      If (Dm.IBInventarioINV_CADASTRO.Value = 'T') or (DM.IBInventarioINV_LOJA.Value = 'NFE') then
       Begin
         BtnAbrir.Enabled := True; //Menu de Abrir Invent�rio
         BtnBackup.Enabled := True; //Menu de Backup de Invent�rio

       End;
    End;
 //Se n�vel do usu�rio MASTER
  If (NivelUsu = 3) then
    Begin
     BtnCadastro.Enabled := true; //Menu de Cadastro de Invent�rio
     BtnRestore.Enabled := true; //Menu de Restaurar Invent�rio
     MainMenu1.Items[0].Enabled := true;
     MainMenu1.Items[1].Enabled := true;
     MainMenu1.Items[2].Enabled := true;

      If (Dm.IBInventarioINV_CADASTRO.Value = 'T') or (DM.IBInventarioINV_LOJA.Value = 'NFE') then
       Begin
         BtnAbrir.Enabled := True; //Menu de Abrir Invent�rio
         BtnBackup.Enabled := True; //Menu de Backup de Invent�rio
         
       End;
    End;

 //Verifica se cadastro j� foi importado para habilitar menu de Invent�rio
  Dm.IBInventario.Open;
  if (Dm.IBInventarioINV_CADASTRO.AsBoolean = False) AND (DM.IBInventarioINV_LOJA.Value <> 'NFE')  then
    Begin
     BtnAbrir.Enabled := False;
    End;
  Dm.IBInventario.Close;

  //Preenche StatusBar
  StatusBar1.Panels[0].Text := 'Usu�rio Conectado = ' + DM.QUsuarioUSU_NOME.Value;
  StatusBar1.Panels[1].Text := '[' +Datetostr(Date) + ']';
end;

procedure TFrmPrincipal.N1Click(Sender: TObject);
begin
 Close;
end;

procedure TFrmPrincipal.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 close;
 frmLogin.Close;

// FrmPrincipal.Release;
// FrmPrincipal := Nil;
// FrmLogin.Release;
// FrmLogin := Nil;

end;

procedure TFrmPrincipal.RestaurarTransmisses1Click(Sender: TObject);
begin
 FrmRestTram := TFrmRestTram.Create(FrmRestTram);
 try
   FrmRestTram.ShowModal;
 finally
   FrmRestTram.Release;
   FrmRestTram := nil;
 end;
end;

procedure TFrmPrincipal.BackupInventrio1Click(Sender: TObject);
begin
  FrmBackupInvent := TFrmBackupInvent.Create(FrmRestTram);
 try
   FrmBackupInvent.ShowModal;
 finally
   FrmBackupInvent.Release;
   FrmBackupInvent := nil;
 end;
end;

procedure TFrmPrincipal.MasterLibrary1Click(Sender: TObject);
begin
  FrmMasterLibrary := TFrmMasterLibrary.Create(FrmPrincipal);
  try
   FrmMasterLibrary.ShowModal;
  finally
   FrmMasterLibrary.Release;
   FrmMasterLibrary := nil;
  end;
end;


procedure TFrmPrincipal.btnCadastroClick(Sender: TObject);
begin
  FrmCadInv := TFrmCadInv.Create(FrmPrincipal);
  try
   FrmCadInv.ShowModal;
  finally
   FrmCadInv.Release;
   FrmCadInv := nil;
  end;
end;

procedure TFrmPrincipal.btnImportClick(Sender: TObject);
begin
FrmImportaCad := TFrmImportaCad.Create(FrmPrincipal);
try
  FrmImportaCad.ShowModal;
finally
  FrmImportaCad.Release;
  FrmImportaCad := nil;
end;
end;

procedure TFrmPrincipal.btnRestoreClick(Sender: TObject);
begin
 FrmRestTram := TFrmRestTram.Create(FrmPrincipal);
 try
   FrmRestTram.ShowModal;
 finally
   FrmRestTram.Release;
   FrmRestTram := nil;
 end;
end;

procedure TFrmPrincipal.btnBackupClick(Sender: TObject);
begin
  FrmBackupInvent := TFrmBackupInvent.Create(FrmPrincipal);
 try
   FrmBackupInvent.ShowModal;
 finally
   FrmBackupInvent.Release;
   FrmBackupInvent := nil;
 end;
end;

procedure TFrmPrincipal.btnCloseClick(Sender: TObject);
begin
Application.Terminate;
end;


procedure TFrmPrincipal.btnAbrirClick(Sender: TObject);
begin
 FrmInventario := TFrmInventario.Create(FrmInventario);
 try
   //Abrir DataSet Mapeamento
   Dm.IBMapeamento.ParamByName('secini').Value := '0000';
   Dm.IBMapeamento.ParamByName('secfin').Value := '9999';
   Dm.IBMapeamento.Open;

   //Abrir DataSet Se��o
   //DM.QIBSecao.Active := true;
  // Dm.IBSecao.Params.ParamByName('secini').Value := '0';
  // Dm.IBSecao.Params.ParamByName('secfin').Value := 'ZZZZZZZZZZZZZZZZZZZZZZZZZ';
   Dm.IBSecao.Open;

   //Abrir Transmiss�es
   Dm.IBTransmissao.Open;

   //Abrir Area
   Dm.IBArea.Open;

   //Abrir Contagens
   Dm.IBContagens.Open;

  // Dm.CdsContagens.Open;
   //Abrir tabela Roster
   Dm.IBRoster.Open;

   //Abrir tabela Horario
   Dm.IBHorarios.Open;


   FrmInventario.ShowModal;


 finally
   FrmInventario.Release;
   FrmInventario := nil;
   FrmPrincipal.Release;
   FrmPrincipal := nil;
 end;

end;

procedure TFrmPrincipal.CriarumNovoLayoutdeEntrada1Click(Sender: TObject);
begin
  FrmLayout := TFrmLayout.Create(FrmLayout);
  try
   FrmLayout.ShowModal;
  finally
   FrmLayout.Release;
   FrmLayout := nil;
  end;
end;

procedure TFrmPrincipal.AdicionarNovoLayoutdeSada1Click(Sender: TObject);
begin
  FrmLayoutOutput := TFrmLayoutOutput.Create(FrmLayoutOutput);
  try
   FrmLayoutOutput.ShowModal;
  finally
   FrmLayoutOutput.Release;
   FrmLayoutOutput := nil;
  end;
end;

procedure TFrmPrincipal.EditarumLayoutexistente1Click(Sender: TObject);
begin
  FrmLayoutExiste := TFrmLayoutExiste.Create(FrmLayoutExiste);
  try
   FrmLayoutExiste.ShowModal;
  finally
   FrmLayoutExiste.Release;
   FrmLayoutExiste := nil;
  end;
end;


procedure TFrmPrincipal.ConfigurarservidorFTP1Click(Sender: TObject);
begin
 FrmConfigSMTP := TFrmConfigSMTP.Create(FrmPrincipal);
  try
   FrmConfigSMTP.ShowModal;
  finally
   FrmConfigSMTP.Release;
   FrmConfigSMTP := nil;
  end;
end;

procedure TFrmPrincipal.MostrartodososLayouts1Click(Sender: TObject);
begin
  FrmAllLayouts := TFrmAllLayouts.Create(FrmAllLayouts);
  try
   FrmAllLayouts.ShowModal;
  finally
   FrmAllLayouts.Release;
   FrmAllLayouts := nil;
  end;
end;

end.
