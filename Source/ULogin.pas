  {*------------------------------------------------------------------------------

  Primeiro formu�rio, para login do usu�rio
  @author  Felipe Gabriel
  @version Revis�o 3.5.3.0 - 17/07/2012
  @todo
  @comment
-------------------------------------------------------------------------------}          
           
unit ULogin;

interface

uses
  sSkinManager, Vcl.StdCtrls, Vcl.Buttons, Vcl.Graphics, Vcl.Controls,
  Vcl.ExtCtrls, System.Classes, Vcl.Forms, System.SysUtils, windows, acPNG;

type

{* -----
Nome completo da classe do formul�rio
----- }
    TFrmLogin = class(TForm)

    {* -----
Painel que integra o layout do SGD
----- }
    Panel1: TPanel;

    {* -----
Painel que integra o layout do SGD
----- }
    Panel2: TPanel;

    {* -----
Imagem BMP que simula a figura de um usu�rio.
----- }
    Image1: TImage;

    {* -----
Painel que integra o layout do SGD
----- }
    Panel3: TPanel;

    {* -----
Label "Usu�rio"
----- }
    Label1: TLabel;

    {* -----
Edit para preenchimento do nome do Usu�rio
-----}
    EdtUsuario: TEdit;

    {* -----
Label "Senha"
----- }
    Label2: TLabel;

    {* -----
Edit para preenchimento da Senha do Usu�rio
----- }
    EdtSenha: TEdit;

    {* -----
Bot�o "Enviar"
----- }
    BitBtn1: TBitBtn;
    
    {* -----
Bot�o "Fechar"
----- }
    BitBtn2: TBitBtn;

    {* -----
Skin "Interface WLM"
----- }
    sSkinManager1: TsSkinManager;

    {* -----
Evento OnClick do bot�o "Enviar"
----- }
    procedure BitBtn1Click(Sender: TObject);

    {* -----
Evento OnClick do bot�o "FECHAR"
----- }
    procedure BitBtn2Click(Sender: TObject);

    {* -----
Evento KeyDown do Edit "SENHA"
----- }
    procedure EdtSenhaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

     {* -----
Evento KeyDown do Edit "USU�RIO"
----- }
    procedure EdtUsuarioKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private

    { Private declarations }
  public

    { Public declarations }
  end;

var
  {* -----
Nome do Formul�rio
----- }

  FrmLogin: TFrmLogin;
  {* -----
Vari�vel que define o nivel do usu�rio e seus acessos no sistema
----- }

    NivelUsu : Integer;

implementation

uses UDm, UPrincipal, UImportaCad, UFuncProc;

{$R *.dfm}

{*------------------------------------------------------------------------------
  @param Sender  Bot�o "Enviar" - Ele verifica a vericidade do usu�rio e libera a entrada no programa
-------------------------------------------------------------------------------}    
procedure TFrmLogin.BitBtn1Click(Sender: TObject);
begin
  /// A Query QUsuario recebe os parametros digitados nos edits LOGIN e SENHA
  DM.QUsuario.Close;
  DM.QUsuario.ParamByName('USUARIO').Value := EdtUsuario.Text;
  DM.QUsuario.ParamByName('SENHA').Value := EdtSenha.Text;
  DM.QUsuario.Open;

  /// Compara o que foi digitado com o que est� cadastrado no banco de dados e
  /// Libera o usu�rio com suas permiss�es ou apresenta mensagem de erro de login na tela
  if DM.QUsuario.RecordCount = 1 then
  Begin
    IF FrmPrincipal = Nil then
    Begin
      FrmLogin.Hide;
      FrmPrincipal := TFrmPrincipal.Create(Self);
      NivelUsu := strtoint(Dm.QUsuarioUSU_NIVEL.Value);
      FrmPrincipal.ShowModal;
      FrmPrincipal.Free;
    end
    else
    Begin
      FrmPrincipal.Release;
      FrmPrincipal := Nil;
      FrmLogin.Hide;
      FrmPrincipal := TFrmPrincipal.Create(Self);
      NivelUsu := strtoint(Dm.QUsuarioUSU_NIVEL.Value);
      FrmPrincipal.ShowModal;
      FrmPrincipal.Free;
    End;
  End
  else
  Begin
    Application.MessageBox('Usu�rio ou senha inv�lidos','Mensagem do Sistema', MB_ICONERROR + MB_OK);
    EdtUsuario.SetFocus;
  End;
end;

{*------------------------------------------------------------------------------
  @param Sender  Bot�o "FECHAR" - Ele cancela a entrada no programa
-------------------------------------------------------------------------------}
procedure TFrmLogin.BitBtn2Click(Sender: TObject);
begin
  /// Fecha a aplica��o
  close;
end;

{*------------------------------------------------------------------------------
  @param Sender  Edit "SENHA" - Ele recebe o valor referente a senha
-------------------------------------------------------------------------------}
procedure TFrmLogin.EdtSenhaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 /// Quando apertado a tecla ENTER simula um click no bot�o "ENVIAR"
 if (Key = 13) then                               
   BitBtn1.Click;
end;
                            
{*------------------------------------------------------------------------------
  @param Sender  Edit "USU�RIO" - Ele recebe o valor referente ao Usu�rio
-------------------------------------------------------------------------------}
procedure TFrmLogin.EdtUsuarioKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 /// Quando apertado a tecla ENTER seta o foco para o EDIT "SENHA"
 if (Key = 13) then
   EdtSenha.SetFocus;
end;

end.
