unit UCadInv;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Mask, DBCtrls, DBActns, ActnList,
  XPStyleActnCtrls, ActnMan, ImgList, ToolWin, ActnCtrls, ActnMenus, Grids,
  DBGrids, StdStyleActnCtrls, ComCtrls, ShellApi, Db, IBServices, sEdit, System.Actions,
  System.ImageList, sLabel;

type
  TFrmCadInv = class(TForm)
    Panel1: TPanel;                             
    Panel2: TPanel;
    ImageList1: TImageList;
    ActionManager1: TActionManager;
    DataSetInsert1: TDataSetInsert;
    DataSetDelete1: TDataSetDelete;
    DataSetEdit1: TDataSetEdit;
    DataSetPost1: TDataSetPost;
    DataSetCancel1: TDataSetCancel;
    ActionToolBar1: TActionToolBar;
    StatusBar1: TStatusBar;
    Panel3: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    DBEdit5: TDBEdit;
    Label4: TLabel;
    DBEdit3: TDBEdit;
    Label8: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    Panel4: TPanel;
    DBGrid1: TDBGrid;
    DateTimePicker1: TDateTimePicker;
    DBEdit1: TDBEdit;
    IBRestoreService1: TIBRestoreService;
    IBBackupService1: TIBBackupService;
    cbxCliente: TDBEdit;
    Label10: TLabel;
    DBEdit2: TDBEdit;
    DBEdit7: TDBEdit;
    Label12: TLabel;
    sLabelFX1: TsLabelFX;
    procedure FormShow(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure Action5Execute(Sender: TObject);
    procedure Action2Execute(Sender: TObject);
    procedure Action3Execute(Sender: TObject);
    procedure Action4Execute(Sender: TObject);
    procedure DataSetEdit1Execute(Sender: TObject);
    procedure DataSetCancel1Execute(Sender: TObject);
    procedure DataSetPost1Execute(Sender: TObject);
    procedure DataSetInsert1Execute(Sender: TObject);
    procedure DataSetDelete1Execute(Sender: TObject);
    procedure DateTimePicker1Exit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmCadInv: TFrmCadInv;

implementation

uses UDm, UFuncProc, UPrincipal, UFrmBkp;

{$R *.dfm}

procedure TFrmCadInv.FormShow(Sender: TObject);
begin
  //Abre tabela de Inventarios
  Dm.IBInventario.Open;

  //Deixar Edit desabilitados antes de incluir um novo invent�rio
  DbEdit1.Enabled := False;
  DbEdit2.Enabled := False;
  cbxCliente.Enabled := False;
  DBEdit3.Enabled := False;
  DBEdit4.Enabled := False;
  DBEdit5.Enabled := False;
  DBEdit6.Enabled := False;
  DBEdit7.Enabled := False;
  DateTimePicker1.Enabled := False;

end;

procedure TFrmCadInv.Action1Execute(Sender: TObject);
begin
  Dm.IBInventario.Edit;
end;

procedure TFrmCadInv.Action5Execute(Sender: TObject);
begin
 Dm.IBInventario.Insert;
end;

procedure TFrmCadInv.Action2Execute(Sender: TObject);
begin
  Dm.IBInventario.Cancel;
end;

procedure TFrmCadInv.Action3Execute(Sender: TObject);
begin
  Dm.IBInventario.Delete;
end;

procedure TFrmCadInv.Action4Execute(Sender: TObject);
begin
    Dm.IBInventario.Post;
end;

procedure TFrmCadInv.DataSetEdit1Execute(Sender: TObject);
begin

if Dm.IBInventario.RecordCount > 0 then
 Begin
  Dm.IBInventario.Edit;
  //Habilitar os Edit para Inser��o
  DbEdit1.Enabled := True;
  cbxCliente.Enabled := True;
  DBEdit2.Enabled := True;
  DBEdit3.Enabled := True;
  DBEdit4.Enabled := True;
  DBEdit5.Enabled := True;
  DBEdit6.Enabled := True;
  DBEdit7.Enabled := True;
  DateTimePicker1.Enabled := True;
  DbEdit1.SetFocus;
 End
else
Application.MessageBox('N�o existe nenhum invent�rio cadastrado para ser alterado', 'Mensagem do Sistema', MB_ICONERROR + MB_OK);

end;

procedure TFrmCadInv.DataSetCancel1Execute(Sender: TObject);
begin
  dm.IBInventario.Cancel;

  //Desabilita os Edit para Inser��o
  DbEdit1.Enabled := False;
  cbxCliente.Enabled := False;
  DBEdit3.Enabled := False;
  DBEdit4.Enabled := False;
  DBEdit5.Enabled := False;
  DBEdit6.Enabled := False;
  DBEdit7.Enabled := False;
  DateTimePicker1.Enabled := False;
end;

procedure TFrmCadInv.DataSetPost1Execute(Sender: TObject);
begin
  if (RetiraSpace(DbEdit1.Text) = '') or (cbxCliente.Text = '') or (DBEdit3.Text = '') then
  Begin
    Application.MessageBox('� obrigat�rio o preenchimento dos Campos OS, Cliente e N� de Loja', 'Mensagem do Sistema', MB_ICONERROR + MB_OK);
    DBEdit1.SetFocus;
  End
  else if (Length(RetiraSpace(DBEdit1.Text)) <> 13) then
  Begin
    Application.MessageBox('O n�mero da OS deve conter 13 d�gitos', 'Mensagem do Sistema', MB_ICONERROR + MB_OK);
    DBEdit1.SetFocus;
  End
  else
  Begin
    DBEdit1.SetFocus;
    Dm.IBInventarioINV_DATAINV.AsString;
    if DM.IBInventarioINV_DATAINV.AsString = '' then
      DM.IBInventarioINV_DATAINV.Value := Date;
    Dm.IBInventario.Post;
    Dm.Transacao.CommitRetaining;

    //Desabilitar os Edit para Inser��o
    DbEdit1.Enabled := False;
    cbxCliente.Enabled := False;
    DBEdit3.Enabled := False;
    DBEdit4.Enabled := False;
    DBEdit5.Enabled := False;
    DBEdit6.Enabled := False;
    DBEdit7.Enabled := False;
    DateTimePicker1.Enabled := False;
  End;
End;

procedure TFrmCadInv.DataSetInsert1Execute(Sender: TObject);
begin
//if (Dm.DsInventario.State = DsInsert) then
//ShowMessage('Voc� j� est� incluindo uma cadastro. N�o pode ser cadastrado dois invent�rios');
//else
//Begin
//Oculta os arquivos da pasta IGT_CE, exceto IgtCeSync
//FileSetAttr('C:\SGD\IGT_CE\IgtCESync\CfgIgtCe.exe', FileGetAttr('C:\SGD\IGT_CE\IgtCeSync\CfgIgtCe.exe') or faHidden);
If (DM.IBInventario.RecordCount = 0) and (Dm.DsInventario.State = DsBrowse) then
Begin
  Dm.IBInventario.Insert;

//Verifica se existe todos os diret�rios do sistema. Se n�o tiver � criado
CriaDirSgd;

//Habilitar os Edit para Inser��o
  DbEdit1.Enabled := True;
  DbEdit2.Enabled := True;
  cbxCliente.Enabled := True;
  DBEdit3.Enabled := True;
  DBEdit4.Enabled := True;
  DBEdit5.Enabled := True;
  DBEdit6.Enabled := True;
  DBEdit7.Enabled := True;
  DateTimePicker1.Enabled := True;

  //Grava False (Inesperado, Duplicidade, Cadastro, aberto) e Status = Novo
  DM.IBInventarioINV_CADASTRO.AsBoolean := False;
  DM.IBInventarioINV_SECABERTA.AsBoolean := False;
  DM.IBInventarioINV_SECDUPLI.AsBoolean := False;
  DM.IBInventarioINV_SECINESPERADA.AsBoolean := False;
  Dm.IBInventarioINV_STATUS.Value := 'NOVO';
  //Coloca data do dia do objeto de data
  DateTimePicker1.DateTime := Date;
  DbEdit1.SetFocus;
End
else
    Application.MessageBox('O sistema s� permite o gerenciamento de um �nico invent�rio', 'Mensagem do Sistema', MB_ICONERROR + MB_OK);

end;

procedure TFrmCadInv.DataSetDelete1Execute(Sender: TObject);
var
Result: integer;
Cont: Integer;
begin

//Mensagem de confirma��o. retorna 6 para sim e 7 para n�o
Result := Application.messageBox('Deseja Excluir o invent�rio Inteiro? Ser� deletada todas ' +#13 +
                                 'as informa��es do invent�rio. Fa�a um backup antes. ','Confirma��o',mb_YesNo+mb_IconInformation+mb_DefButton2);

if Result = 6 then
Begin
  // Executar Procedures para deletar as tabelas e zerar os Gerators
  DM.SpDeletaBanco.ExecProc;
  DM.SpZeraGenrerator.ExecProc;
  Dm.Transacao.CommitRetaining;
  Dm.IBInventario.Close;
  Dm.IBInventario.Open;
  ApagaDir('C:\SGD\Arquivo Final\');
  ApagaDir('C:\SGD\Cadastro\');
  ApagaDir('C:\SGD\Horas Conferentes\');
  ApagaDir('C:\SGD\Relatorios\');
  ApagaDir('C:\SGD\Transmissao\');
  ApagaDir('C:\SGD\Upload\');
  CreateDir('C:\SGD\IGT_CE\TempIni\');
  MoverArq('C:\SGD\IGT_CE\IgtCeSinc\*.ini','C:\SGD\IGT_CE\TempIni\');
  ApagaDir('C:\SGD\IGT_CE\IgtCeSinc\');
  CreateDir('C:\SGD\IGT_CE\IgtCeSinc\');
  MoverArq('C:\SGD\IGT_CE\TempIni\*.ini','C:\SGD\IGT_CE\IgtCeSinc\');
  ApagaDir('C:\SGD\IGT_CE\TempIni\');
  FrmPrincipal.btnAbrir.Enabled:= False; //N�o deixa abrir um invent�rio sem cadastrar
  FrmPrincipal.btnRestore.Enabled:= False;
  FrmPrincipal.btnBackup.Enabled := False;

    //Deleta todas as pastas de transmiss�o dos coletores dentro do IGET_CE

  Application.MessageBox('Cadastro de invent�rio exclu�do', 'Mensagem do Sistema', MB_ICONINFORMATION + MB_OK);

  //Desativa conex�o com o Banco
 // Dm.Conexao.Connected := False;

// Try
//  ExecutarEEsperar('c:\Sgd\Dados\BACKUP.BAT');
// Finally
  //deleta o arquivo de banco antigo
//  DeleteFile('c:\Sgd\Dados\BD_SGD.gdb');
// End;

// Try
//  ExecutarEEsperar('c:\Sgd\Dados\RESTAURA.BAT');
// Finally
  //deleta o arquivo GBK GERADO NO BACKUP
//   DeleteFile('c:\Sgd\Dados\BD_SGD.GBK');
// End;
end;
end;
procedure TFrmCadInv.DateTimePicker1Exit(Sender: TObject);
begin
 //Grava Data do invent�rio no Banco
 DM.IBInventarioINV_DATAINV.Value := FrmCadInv.DateTimePicker1.DateTime;
end;

end.
