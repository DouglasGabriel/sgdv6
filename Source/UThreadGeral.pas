unit UThreadGeral;

interface

uses Classes, ComCtrls, Controls, SysUtils; 

type 
  TAnimacao = class(TThread)
  private 
    { Private declarations } 
    procedure InicializaAnimacao;
    procedure RodaAnimacao;
    procedure EncerraAnimacao(Sender : TObject);
  protected 
    procedure Execute; override;
  public 
    constructor Create(CreateSuspended : boolean); 
  end; 

var 
   Anim : TAnimate;

implementation 

uses UImportaCad;

{ TAnimacao }
constructor TAnimacao.Create(CreateSuspended : boolean);
begin 
   inherited Create(CreateSuspended); 
   OnTerminate := EncerraAnimacao;  // OnTerminate � executado quando � encerrado a thread. 
   FreeOnTerminate := True; // determina se a thread � destruido automaticamente. 
   Priority := tpNormal;    // prioridade da thread em rela��o aos demais processos. 
   Synchronize(InicializaAnimacao); // inicializa o Animate.
end; 

procedure TAnimacao.EncerraAnimacao(Sender : TObject);
begin
   FreeAndNil(Anim); // destroi e libera o progressbar.
end;

procedure TAnimacao.Execute;
TotaldeLinhas: integer;
begin  // enquanto a execu��o n�o for for�ado a terminar
TotaldeLinhas:=QuantasLinhasTxt('C:\SGD\Cadastro\Cadastro_prod.txt');
FrmAguarde.Gauge.MaxValue:=TotaldeLInhas;
   while FrmImportaCad.Gauge.Progress <> FrmImportaCad.Gauge.MaxValue do   // (atraves do evento OnTerminate da TThreadSQL).
     Synchronize(RodaAnimacao);             // preenche o progressbar.
end;

// inicializa o progressbar
procedure TAnimacao.InicializaAnimacao;
begin
   Anim := TAnimate.Create(nil);  // inst�ncia um objeto do tipo TAnimate.
   with Anim do
    begin
       Parent := frmImportaCad.Animate1;
       Active := False;
       Visible := false;
       StartFrame := 1;
       StopFrame := 50;
       Repetitions := -1;
       CommonAvi := aviCopyFile;
       Transparent := true;
    end;
end;

// preenche o progressbar.
procedure TAnimacao.RodaAnimacao;
begin
   with Anim do
    begin
       Visible := True;
       Active := True;
    end;
end;

end.
