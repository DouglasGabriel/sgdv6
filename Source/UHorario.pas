unit UHorario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, Mask, DBCtrls, ExtCtrls, IdBaseComponent,
  IdComponent, IdTCPConnection, IdTCPClient, IdTime, IdUDPBase,
  IdUDPClient, IdSNTP, Buttons, strutils, sLabel;

type
  TFrmHorario = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    StatusBar1: TStatusBar;
    Panel3: TPanel;
    Label4: TLabel;
    Label7: TLabel;
    DBEdit3: TDBEdit;
    DBEdit6: TDBEdit;
    BitBtn1: TBitBtn;
    DateTimePicker1: TDateTimePicker;
    DateTimePicker2: TDateTimePicker;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    DateTimePicker3: TDateTimePicker;
    DateTimePicker4: TDateTimePicker;
    Label2: TLabel;
    ComboBox1: TComboBox;
    sLabelFX1: TsLabelFX;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure CheckBox2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmHorario: TFrmHorario;
 

implementation

uses UDm, UInventario, UFuncProc, DB;

{$R *.dfm}

procedure TFrmHorario.BitBtn1Click(Sender: TObject);
var dia, mes, ano, hora, minuto, segundo, dia1, mes1, ano1, hora1, minuto1, segundo1, total, total1, dia2, mes2, ano2, hora2, minuto2, segundo2, total2 :string;
var id_conf:Integer;
begin
 Dm.IBHorarios.Edit;
 DM.IBHorariosHOR_SAIDA.Value := '';
 DM.IBHorariosHOR_TOTALHORA.Value := '';
 DM.IBHorarios.Post;
 DM.Transacao.CommitRetaining;
 DM.IBHorarios.Edit;
 id_conf:= DM.IBHorariosHOR_ID.Value;

 //Verifica e grava no banco os horarios de entrada e saida. Calcula a diferen�a de horas.
 If (CheckBox1.Checked = True) and (CheckBox2.Checked = True) Then
   Begin
     //Verifica se hor�rio final � maior que hor�rio inicial
     if (DateTimePicker1.DateTime + DateTimePicker2.DateTime)
     < (DateTimePicker3.DateTime + DateTimePicker4.DateTime) then
       Begin

         DM.qrySQL.Close;
         DM.qrySQL.SQL.Clear;
         DM.qrySQL.SQL.Add('select HOR_ENTRADA, HOR_SAIDA from horario where (ros_nome = :ros_nome) and (ros_matricula = :ros_matricula) and (hor_entrada <> '') and (hor_saida <> '') ');
         DM.qrySQL.Params.ParamByName('ROS_NOME').Value      :=  DBEdit6.Text;
         Dm.qrySQL.Params.ParamByName('ROS_MATRICULA').Value :=  DBEdit3.Text;
        // DM.qrySQL.Params.ParamByName('HOR_ENTRADA').Value   :=
         DM.qrySQL.Open;

         If DM.qrySQL.RecordCount > 0 then
          begin

             dia    :=  Copy(DM.qrySQL.FieldByName('HOR_ENTRADA').Value,1,2);
             mes    :=  Copy(DM.qrySQL.FieldByName('HOR_ENTRADA').Value,4,2);
             ano    :=  Copy(DM.qrySQL.FieldByName('HOR_ENTRADA').Value,7,4);
             hora   :=  Copy(DM.qrySQL.FieldByName('HOR_ENTRADA').Value,12,2);
             minuto :=  Copy(DM.qrySQL.FieldByName('HOR_ENTRADA').Value,15,2);
             segundo:=  Copy(DM.qrySQL.FieldByName('HOR_ENTRADA').Value,18,2);
             total  := dia+mes+ano+hora+minuto+segundo;

            If (DM.qrySQL.FieldByName('HOR_SAIDA').Value = null) or (DM.qrySQL.FieldByName('HOR_SAIDA').Value = '') then
             begin
                 dia1    :=  Copy(DateTimeToStr(DateTimePicker3.DateTime + DateTimePicker4.DateTime),1,2);
                 mes1    :=  Copy(DateTimeToStr(DateTimePicker3.DateTime + DateTimePicker4.DateTime),4,2);
                 ano1    :=  Copy(DateTimeToStr(DateTimePicker3.DateTime + DateTimePicker4.DateTime),7,4);
                 hora1   :=  Copy(DateTimeToStr(DateTimePicker3.DateTime + DateTimePicker4.DateTime),12,2);
                 minuto1 :=  Copy(DateTimeToStr(DateTimePicker3.DateTime + DateTimePicker4.DateTime),15,2);
                 segundo1:=  Copy(DateTimeToStr(DateTimePicker3.DateTime + DateTimePicker4.DateTime),18,2);
                 total1  :=  dia1+mes1+ano1+hora1+minuto1+segundo1;
             end
            else
             begin
                dia1    :=  Copy(DM.qrySQL.FieldByName('HOR_SAIDA').Value,1,2);
                mes1    :=  Copy(DM.qrySQL.FieldByName('HOR_SAIDA').Value,4,2);
                ano1    :=  Copy(DM.qrySQL.FieldByName('HOR_SAIDA').Value,7,4);
                hora1   :=  Copy(DM.qrySQL.FieldByName('HOR_SAIDA').Value,12,2);
                minuto1 :=  Copy(DM.qrySQL.FieldByName('HOR_SAIDA').Value,15,2);
                segundo1:=  Copy(DM.qrySQL.FieldByName('HOR_SAIDA').Value,18,2);
                total1  :=   dia1+mes1+ano1+hora1+minuto1+segundo1;
             end;


             dia2    :=  Copy(DateTimeToStr(DateTimePicker1.DateTime + DateTimePicker2.DateTime),1,2);
             mes2    :=  Copy(DateTimeToStr(DateTimePicker1.DateTime + DateTimePicker2.DateTime),4,2);
             ano2    :=  Copy(DateTimeToStr(DateTimePicker1.DateTime + DateTimePicker2.DateTime),7,4);
             hora2   :=  Copy(DateTimeToStr(DateTimePicker1.DateTime + DateTimePicker2.DateTime),12,2);
             minuto2 :=  Copy(DateTimeToStr(DateTimePicker1.DateTime + DateTimePicker2.DateTime),15,2);
             segundo2:=  Copy(DateTimeToStr(DateTimePicker1.DateTime + DateTimePicker2.DateTime),18,2);
             total2  :=  dia2+mes2+ano2+hora2+minuto2+segundo2;


            If total2 > total1 then
              begin

                 If total2 >= total1 then
                   begin

                         DM.Query1.Close;
                         DM.Query1.SQL.Clear;
                         Dm.Query1.SQL.Add('select max(hor_saida) as hor_saida from horario where (ros_nome = :ros_nome) and (ros_matricula = :ros_matricula) ');
                         DM.Query1.Params.ParamByName('ROS_NOME').Value      :=  DBEdit6.Text;
                         Dm.Query1.Params.ParamByName('ROS_MATRICULA').Value :=  DBEdit3.Text;
                         Dm.Query1.Open;

                         IF (DM.Query1.FieldByName('HOR_SAIDA').Value) >=  DateTimeToStr(DateTimePicker1.DateTime + DateTimePicker2.DateTime) then
                          begin
                            Application.MessageBox('Esse intervalo de tempo j� foi registrado, por gentileza registrar novamente!','Mensagem do Sistema', MB_ICONERROR + MB_OK);
                            FrmHorario.Close;
                          end
                         else
                         If ComboBox1.Text = '' then
                          begin
                            Application.MessageBox('Preencha o Evento do Conferente!!','Mensagem do Sistema', MB_ICONERROR + MB_OK);
                            FrmHorario.Close;
                          end
                         else
                          begin
                             Dm.IBHorariosHOR_ENTRADA.Value := DateTimeToStr(DateTimePicker1.DateTime + DateTimePicker2.DateTime);
                             Dm.IBHorariosHOR_SAIDA.Value := DateTimeToStr(DateTimePicker3.DateTime + DateTimePicker4.DateTime);
                             Dm.IBHorariosHOR_TOTALHORA.Value :=CalculaTotaldeHoras(Dm.IBHorariosHOR_ENTRADA.Value,Dm.IBHorariosHOR_SAIDA.Value);

                            { if DM.IBHorariosHOR_SAIDA.Value = '' then
                              begin
                               fim := StrToDateTime(FormatDateTime('dd/mm/yyyy',(Now)))+StrToDateTime(FormatDateTime('hh:mm:ss',(Now)));
                               Dm.IBHorariosHOR_TOTALHORA.Value :=CalculaTotaldeHoras(Dm.IBHorariosHOR_ENTRADA.Value, DateTimeToStr(fim));
                              end
                             else
                              begin
                               Dm.IBHorariosHOR_TOTALHORA.Value :=CalculaTotaldeHoras(Dm.IBHorariosHOR_ENTRADA.Value,Dm.IBHorariosHOR_SAIDA.Value);
                              end;   }

                             DM.IBHorariosEVENT.Value := UpperCase(ComboBox1.Text);
                             FrmHorario.Close;
                          end;
                   end

              end
            else
               begin
                  DM.Query1.Close;
                  DM.Query1.SQL.Clear;
                  Dm.Query1.SQL.Add('select max(hor_saida) as hor_saida from horario where (ros_nome = :ros_nome) and (ros_matricula = :ros_matricula) ');
                  DM.Query1.Params.ParamByName('ROS_NOME').Value      :=  DBEdit6.Text;
                  Dm.Query1.Params.ParamByName('ROS_MATRICULA').Value :=  DBEdit3.Text;
                  Dm.Query1.Open;

                  IF (DM.Query1.FieldByName('HOR_SAIDA').Value) >=  DateTimeToStr(DateTimePicker1.DateTime + DateTimePicker2.DateTime) then
                   begin
                     Application.MessageBox('Esse intervalo de tempo j� foi registrado, por gentileza registrar novamente!','Mensagem do Sistema', MB_ICONERROR + MB_OK);
                     FrmHorario.Close;
                   end
                  else
                  If ComboBox1.Text = '' then
                   begin
                     Application.MessageBox('Preencha o Evento do Conferente!!','Mensagem do Sistema', MB_ICONERROR + MB_OK);
                     FrmHorario.Close;
                   end
                  else
                   begin
                      Dm.IBHorariosHOR_ENTRADA.Value := DateTimeToStr(DateTimePicker1.DateTime + DateTimePicker2.DateTime);
                      Dm.IBHorariosHOR_SAIDA.Value := DateTimeToStr(DateTimePicker3.DateTime + DateTimePicker4.DateTime);
                      Dm.IBHorariosHOR_TOTALHORA.Value :=CalculaTotaldeHoras(Dm.IBHorariosHOR_ENTRADA.Value,Dm.IBHorariosHOR_SAIDA.Value);

                     { if DM.IBHorariosHOR_SAIDA.Value = '' then
                       begin
                        fim := StrToDateTime(FormatDateTime('dd/mm/yyyy',(Now)))+StrToDateTime(FormatDateTime('hh:mm:ss',(Now)));
                        Dm.IBHorariosHOR_TOTALHORA.Value :=CalculaTotaldeHoras(Dm.IBHorariosHOR_ENTRADA.Value, DateTimeToStr(fim));
                       end
                      else
                       begin
                        Dm.IBHorariosHOR_TOTALHORA.Value :=CalculaTotaldeHoras(Dm.IBHorariosHOR_ENTRADA.Value,Dm.IBHorariosHOR_SAIDA.Value);
                       end;   }

                      DM.IBHorariosEVENT.Value := UpperCase(ComboBox1.Text);
                      FrmHorario.Close;

                   end
                
               end;
          end
         else
          If ComboBox1.Text = '' then
           begin
              Application.MessageBox('Preencha o Evento do Conferente!!','Mensagem do Sistema', MB_ICONERROR + MB_OK);
           end
          else
           begin
            
            Dm.IBHorariosHOR_ENTRADA.Value := DateTimeToStr(DateTimePicker1.DateTime + DateTimePicker2.DateTime);
            Dm.IBHorariosHOR_SAIDA.Value := DateTimeToStr(DateTimePicker3.DateTime + DateTimePicker4.DateTime);
            Dm.IBHorariosHOR_TOTALHORA.Value :=CalculaTotaldeHoras(Dm.IBHorariosHOR_ENTRADA.Value,Dm.IBHorariosHOR_SAIDA.Value);

            { if DM.IBHorariosHOR_SAIDA.Value = '' then
               begin
                fim := StrToDateTime(FormatDateTime('dd/mm/yyyy',(Now)))+StrToDateTime(FormatDateTime('hh:mm:ss',(Now)));
                Dm.IBHorariosHOR_TOTALHORA.Value :=CalculaTotaldeHoras(Dm.IBHorariosHOR_ENTRADA.Value, DateTimeToStr(fim));
               end
             else
              begin
               Dm.IBHorariosHOR_TOTALHORA.Value :=CalculaTotaldeHoras(Dm.IBHorariosHOR_ENTRADA.Value,Dm.IBHorariosHOR_SAIDA.Value);
              end;

            Dm.IBHorariosHOR_TOTALHORA.Value :=CalculaTotaldeHoras(Dm.IBHorariosHOR_ENTRADA.Value,Dm.IBHorariosHOR_SAIDA.Value);  }

            DM.IBHorariosEVENT.Value := UpperCase(ComboBox1.Text);
            FrmHorario.Close;
           end;


       End
     else
        Application.MessageBox('Horario final n�o pode ser menor ou igual ao inical','Mensagem do Sistema', MB_ICONERROR + MB_OK);
   End
 Else
 If (CheckBox1.Checked = True) and (CheckBox2.Checked = False) then
   Begin

           DM.Query1.Close;
           DM.Query1.SQL.Clear;
           Dm.Query1.SQL.Add('select max(hor_saida) as hor_saida from horario where (ros_nome = :ros_nome) and (ros_matricula = :ros_matricula) ');
           DM.Query1.Params.ParamByName('ROS_NOME').Value      :=  DBEdit6.Text;
           Dm.Query1.Params.ParamByName('ROS_MATRICULA').Value :=  DBEdit3.Text;
           Dm.Query1.Open;

           DM.qrySQL.Close;
           DM.qrySQL.SQL.Clear;
           DM.qrySQL.SQL.Add('select HOR_ENTRADA, HOR_SAIDA from horario where (ros_nome = :ros_nome) and (ros_matricula = :ros_matricula) and (hor_entrada <> '') and (hor_saida <> '') ');
           DM.qrySQL.Params.ParamByName('ROS_NOME').Value      :=  DBEdit6.Text;
           Dm.qrySQL.Params.ParamByName('ROS_MATRICULA').Value :=  DBEdit3.Text;
           DM.qrySQL.Open;

     If DM.qrySQL.RecordCount > 0 then
      begin

          IF (DM.Query1.FieldByName('HOR_SAIDA').Value) >=  DateTimeToStr(DateTimePicker1.DateTime + DateTimePicker2.DateTime) then
           begin
             Application.MessageBox('Esse hor�rio incial j� foi registrado em outro intervalo de tempo, por gentileza registrar novamente!','Mensagem do Sistema', MB_ICONERROR + MB_OK);
             FrmHorario.Close;
           end
          else
           If ComboBox1.Text = '' then
            begin
              Application.MessageBox('Preencha o Evento do Conferente!!','Mensagem do Sistema', MB_ICONERROR + MB_OK);
              FrmHorario.Close;
            end
           else
           begin
              Dm.IBHorariosHOR_ENTRADA.Value := DateTimeToStr(DateTimePicker1.DateTime + DateTimePicker2.DateTime);
              Dm.IBHorariosHOR_SAIDA.Value := '';
              Dm.IBHorariosHOR_TOTALHORA.Value := '';

            {  if DM.IBHorariosHOR_SAIDA.Value = '' then
                begin
                 fim := StrToDateTime(FormatDateTime('dd/mm/yyyy',(Now)))+StrToDateTime(FormatDateTime('hh:mm:ss',(Now)));
                 Dm.IBHorariosHOR_TOTALHORA.Value :=CalculaTotaldeHoras(Dm.IBHorariosHOR_ENTRADA.Value, DateTimeToStr(fim));
                end;  }

              DM.IBHorariosEVENT.Value := UpperCase(ComboBox1.Text);
              FrmHorario.Close;
           end;
      End
     else
      If ComboBox1.Text = '' then
        begin
           Application.MessageBox('Preencha o Evento do Conferente!!','Mensagem do Sistema', MB_ICONERROR + MB_OK);
        end
      else
       begin
          Dm.IBHorariosHOR_ENTRADA.Value := DateTimeToStr(DateTimePicker1.DateTime + DateTimePicker2.DateTime);
          Dm.IBHorariosHOR_SAIDA.Value := '';
          Dm.IBHorariosHOR_TOTALHORA.Value := '';

          { if DM.IBHorariosHOR_SAIDA.Value = '' then
            begin
             fim := StrToDateTime(FormatDateTime('dd/mm/yyyy',(Now)))+StrToDateTime(FormatDateTime('hh:mm:ss',(Now)));
             Dm.IBHorariosHOR_TOTALHORA.Value :=CalculaTotaldeHoras(Dm.IBHorariosHOR_ENTRADA.Value, DateTimeToStr(fim));

            end;           }

          DM.IBHorariosEVENT.Value := UpperCase(ComboBox1.Text);
          FrmHorario.Close;
       end;

   end

 Else
 If (CheckBox1.Checked = False) and (CheckBox2.Checked = False) then
   Begin
     Dm.IBHorariosHOR_ENTRADA.Value := '';
     Dm.IBHorariosHOR_SAIDA.Value := '';
     Dm.IBHorariosHOR_TOTALHORA.Value := '';
     DM.IBHorariosEVENT.Value := '';
     FrmHorario.Close;

   End;
 Dm.IBHorarios.Post;
 Dm.Transacao.CommitRetaining;
 FrmInventario.EdtHoras.Text := TotalHoras;
 DM.IBHorarios.Locate('HOR_ID',id_conf,[loPartialKey, loCaseInsensitive])
end;

procedure TFrmHorario.FormShow(Sender: TObject);
begin
  DateTimePicker1.DateTime := date;
  DateTimePicker2.DateTime := time;
  DateTimePicker3.DateTime := date;
  DateTimePicker4.DateTime := time;

  //Se tiver hora de entrada ativa checkBox e joga na tela
  If Dm.IBHorariosHOR_ENTRADA.Value <> '' then
  Begin

  
    CheckBox1.Checked := True;
    DateTimePicker1.DateTime := StrToDate(Copy(Dm.IBHorariosHOR_ENTRADA.Value,1,10));
    DateTimePicker2.DateTime := StrToTime(rightstr(Dm.IBHorariosHOR_ENTRADA.Value,8));
  End;

  //Se tiver hora de sa�da ativa checkBox e joga na tela
  If Dm.IBHorariosHOR_SAIDA.Value <> '' then
  Begin
    CheckBox2.Checked := True;
    DateTimePicker3.DateTime := StrToDate(Copy(Dm.IBHorariosHOR_SAIDA.Value,1,10));
    DateTimePicker4.DateTime := StrToTime(rightstr(Dm.IBHorariosHOR_SAIDA.Value,8));
  End;
end;

procedure TFrmHorario.CheckBox1Click(Sender: TObject);
begin
 IF (CheckBox1.Checked = True) then
   Begin
     DateTimePicker1.Enabled := True;
     DateTimePicker2.Enabled := True;
   End
 else
   Begin
     DateTimePicker1.Enabled := False;
     DateTimePicker2.Enabled := False;
     if (CheckBox2.Checked = True) then
      Begin
        CheckBox2.Checked := False;
        DateTimePicker3.Enabled := False;
        DateTimePicker4.Enabled := False;
      End;
   End;
end;

procedure TFrmHorario.CheckBox2Click(Sender: TObject);
begin
 IF (CheckBox2.Checked = True) then
   Begin
     if (CheckBox1.Checked = True) then
       Begin
        DateTimePicker3.Enabled := True;
        DateTimePicker4.Enabled := True;
       End
     Else
      Begin
        Application.MessageBox('N�o � poss�vel digitar o hor�rio de sa�da sem ter aberto o de entrada','Mensagem do Sistema', MB_ICONERROR + MB_OK);
        CheckBox2.Checked := False;
      End;
   End
 else
   Begin
     DateTimePicker3.Enabled := False;
     DateTimePicker4.Enabled := False;
   End;
end;

end.
