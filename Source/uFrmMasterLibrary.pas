unit uFrmMasterLibrary;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons, Gauges, ComCtrls, Mask;

type
  TFrmMasterLibrary = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    btnImpCadProd: TBitBtn;
    barra: TStatusBar;
    OpenDialog1: TOpenDialog;
    gauge: TGauge;
    Label1: TLabel;
    lbl_import: TLabel;
    Gauge1: TGauge;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    procedure btnImpCadProdClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmMasterLibrary: TFrmMasterLibrary;
  Atual: integer;

implementation

uses UDm;

{$R *.dfm}


 procedure TFrmMasterLibrary.btnImpCadProdClick(Sender: TObject);
Var
EAN: string[3];
CodEan: boolean;
aContLInhas: TStringList;
 Txt : Textfile;
 Entrada : String;
 Total_Aprov, Total_Old, Total_atual: integer;
 begin
 gauge1.visible:=false;

      //Importa Arquivo TXT para tabela CASAEVIDEOLJ_CAD
     //Vincula o caixa de dialogo, iniciando na primeira linha
  if OpenDialog1.Execute then
    Begin
      AssignFile(Txt,(OpenDialog1.FileName));
      Reset(Txt);
      Screen.Cursor:=crSqlWait;
      aContLInhas := TStringList.Create;
        try
          aContLInhas.LoadFromFile(OpenDialog1.FileName);
        finally
          Gauge.MaxValue:=aContLinhas.Count;
          Gauge.MinValue:=0;
          aContLinhas.Free;

       //Importar TXT
       Dm.IBEanConso.Close;
       DM.IBEanConso.Open;
       DM.IBEanConso.First;
       DM.IBEanConso.Last;
       Total_Old:=Dm.IBEanConso.RecordCount;

       Dm.IBEan.Close;
       DM.IBEan.Open;
       DM.IBEan.First;
       DM.IBEan.Last;

       while not Eoln(Txt) do
        Begin
          Readln(Txt,Entrada);
          DM.IBEan.Insert;
          EAN := Copy(Entrada,1,3);
          Try
            If EAN = '789' then
              begin
                Dm.IBEanEAN.Value := Copy(Entrada,1,13);
                DM.IBEanDESCRICAO.Value := Copy(Entrada,14,35);
                gauge.Visible:=true;
                Gauge.Progress:=Gauge.Progress + 1;
              end
            else
            finally
          end;

          gauge.Visible:=true;
          Gauge.Progress:=Gauge.Progress + 1;
        End;

     DM.IBEan.Post;
     DM.Transacao.CommitRetaining;
     lbl_import.Caption:=IntToStr(Dm.IBEan.RecordCount);
 end;
 //CONSOLIDAR

 Dm.QEanDeleta.Close;
 Dm.QEanDeleta.SQL.Clear;
 Dm.QEanDeleta.SQL.Add('DELETE FROM EAN_CONSO');
 Dm.QEanDeleta.Open;


  DM.QEanConso.Open;
  DM.QEanConso.First;
  Dm.IBEanConso.Open;

  While not DM.QEanConso.Eof do
   Begin
    Dm.IBEanConso.Insert;
    Dm.IBEanConsoEAN_CONSO.Value := DM.QEanConsoEAN.value;
    Dm.IBEanConsoDESCRICAO.Value := DM.QEanConsoDESCRICAO.value;
    DM.QEanConso.Next;
   end;
    Dm.IBEanConso.Post;

    DM.Transacao.CommitRetaining;


    Total_Aprov:=(Atual - Dm.IBEanConso.RecordCount);
    Label3.Caption:= IntToStr(Atual - Total_Aprov);
    Label7.Caption:=IntToStr(Dm.IBEanConso.RecordCount);
Dm.IBEanConso.Close;
Dm.IBEanConso.Open;
Dm.IBEanConso.First;
Dm.IBEanConso.Last;
Total_Atual:=Dm.IBEanConso.RecordCount;

Total_Aprov:=Total_Atual - Total_old;

Barra.Panels[0].Text:='Base de dados com '+IntToStr(Dm.IBEanConso.RecordCount)+' c�digos EANs cadastrados.';

  if Gauge.Progress = Gauge.MaxValue then
    Application.MessageBox('Arquivo importado com sucesso!', 'Mensagem do Sistema', mb_iconinformation + mb_ok);

    Screen.Cursor:=crDefault;
    gauge.Visible:=false;
    DM.IBEan.Close;
    Label3.caption:=IntToStr(Total_Aprov);
end;
end;


procedure TFrmMasterLibrary.FormShow(Sender: TObject);
begin
Dm.IBEanConso.Open;
Dm.IBEanConso.First;
Dm.IBEanConso.Last;
Atual := Dm.IBEanConso.RecordCount;
Barra.Panels[0].Text:='Base de dados com '+IntToStr(Dm.IBEanConso.RecordCount)+' c�digos EANs cadastrados.';
end;

end.






