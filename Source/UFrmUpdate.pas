unit UFrmUpdate;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, Gauges, IdBaseComponent, IdComponent, IdTCPConnection,
  IdTCPClient, IdFTP, ExtCtrls, StdCtrls, ZipForge, IdTCPServer,
  IdSMTPServer, IdMessageClient, IdSMTP, sGauge,  IdMessage, WinInet, ShellApi,
  IdIOHandler, IdIOHandlerSocket, IdSSLOpenSSL, IdServerIOHandler,
  Buttons, IdIOHandlerStack, IdSSL, IdExplicitTLSClientServerBase, IdAttachmentFile,
  IdSMTPBase, IdText;
type
  TFrmUpdate = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Image1: TImage;
    Memo1: TMemo;
    StatusBar1: TStatusBar;
    gauge: TsGauge;
    Memo2: TMemo;
    ZipForge1: TZipForge;
    IdSMTP1: TIdSMTP;
    IdSSLIOHandlerSocketOpenSSL1: TIdSSLIOHandlerSocketOpenSSL;
    procedure FormActivate(Sender: TObject);

  private
    { Private declarations }

  public
    { Public declarations }
  end;

var
  FrmUpdate: TFrmUpdate;

implementation
Uses Udm, UFuncProc;

{$R *.dfm}











procedure TFrmUpdate.FormActivate(Sender: TObject);
Var ArqRelat: String;
Mensagem: TIdMessage;
Arquivos: TIdAttachmentFile;
DadosInv: TSHFileOpStruct;
IdSSl     : TIdSSLIOHandlerSocketOpenSSL;
IdSmtp    : TIdSMTP;
begin
Dm.IBMAIL.Open;
Dm.IBInventario.Open;

// criando os componemtes em tempo de execu��o
Arquivos := TIdAttachmentFile.Create(nil);
Mensagem := TIdMessage.Create(nil);
//ServerSMTP  := TIdSMTP.Create(nil);


Screen.Cursor:=crSqlWait;
Memo1.Lines.Add('-----------------------------------------------------------------------------');
Memo1.Lines.Add('');
Memo1.Lines.Add('INFORMA��ES SOBRE O INVENT�RIO');
Memo1.Lines.Add('');
Memo1.Lines.Add('O.S: '+Dm.IBInventarioINV_OS.Value);
Memo1.Lines.Add('Data do Invent�rio: '+Dm.IBInventarioINV_DATAINV.AsString);
Memo1.Lines.Add('Cliente: '+Dm.IBInventarioINV_LOJA.Value);
Memo1.Lines.Add('Loja: '+Dm.IBInventarioINV_NUMLOJA.Value);
Memo1.Lines.Add('L�der do Invent�rio: '+Dm.IBInventarioINV_GESTOR.Value);
Memo1.Lines.Add('Telefone: '+Dm.IBInventarioINV_TELEFONE.Value);
Memo1.Lines.Add('Endere�o: '+Dm.IBInventarioINV_ENDERECO.Value);
Memo1.Lines.Add('Gerente da Loja: '+Dm.IBInventarioINV_Gerente.Value);
Memo1.Lines.Add('Status do Invent�rio: '+Dm.IBInventarioINV_STATUS.Value);
Memo1.Lines.Add('-----------------------------------------------------------------------------');
Memo1.Lines.Add('');

Memo1.Lines.Add('PREPARANDO OS RELAT�RIOS');
Memo1.Lines.Add('');

Memo1.Lines.Add(DateTimeToStr(Now)+' - Status: Deletando o diret�rio C:\SGD\Upload');
Try
  If DirectoryExists ('C:\SGD\Upload\') then
    begin
      ApagaDir('C:\SGD\Upload');
      Memo1.Lines.Add(DateTimeToStr(Now)+' - Status: Diret�rio C:\SGD\Upload deletado com sucesso.');
    End;
except
  Memo1.Lines.Add(DateTimeToStr(Now)+' - Status: Erro ao deletar o diret�rio C:\SGD\Upload');
end;

Memo1.Lines.Add(DateTimeToStr(Now)+' - Status: Criando as estruturas de diret�rios.');
Try
  If Not DirectoryExists ('C:\SGD\Upload\') then
    begin
      CreateDir('C:\SGD\Upload\');
      Memo1.Lines.Add(DateTimeToStr(Now)+' - Status: Diret�rio C:\SGD\Upload criado com sucesso.');
      CreateDir('C:\SGD\Upload\Relatorios\');
      Memo1.Lines.Add(DateTimeToStr(Now)+' - Status: Diret�rio C:\SGD\Upload\Relatorios criado com sucesso.');
      CreateDir('C:\SGD\Upload\Arquivo Final');
      Memo1.Lines.Add(DateTimeToStr(Now)+' - Status: Diret�rio C:\SGD\Upload\Arquivo Final criado com sucesso.');
      CreateDir('C:\SGD\Upload\Horas Conferentes');
      Memo1.Lines.Add(DateTimeToStr(Now)+' - Status: Diret�rio C:\SGD\Upload\Horas Conferentes criado com sucesso.');
       CreateDir('C:\SGD\Upload\Transmissao');
      Memo1.Lines.Add(DateTimeToStr(Now)+' - Status: Diret�rio C:\SGD\Upload\Transmissao criado com sucesso.');
    end;
Except
      Memo1.Lines.Add(DateTimeToStr(Now)+' - Status: Erro ao criar estrutura de diret�rios.');
End;

//Copiando arquivos do diretorio RELATORIOS para o diret�rio C:\SGD\UPLOAD
Memo1.Lines.Add(DateTimeToStr(Now)+' - Status: Copiando arquivos do diret�rio C:\SGD\Relatorios para C:\SGD\Upload\Relatorios');
Try
FillChar(DadosInv,SizeOf(DadosInv), 0);
with DadosInv do
  begin
    wFunc := FO_COPY;
    pFrom := PChar('C:\SGD\RELATORIOS\*.*');
    pTo   := PChar('C:\SGD\UPLOAD\RELATORIOS\');
    fFlags:= FOF_ALLOWUNDO;
  end;
SHFileOperation(DadosInv);
Memo1.Lines.Add(DateTimeToStr(Now)+' - Status: Arquivos copiados com sucesso.');
Except
Memo1.Lines.Add(DateTimeToStr(Now)+' - Status: Erro ao copiar os arquivos do diret�rio C:\SGD\Relatorios para C:\SGD\Upload\Relatorios');
End;

Try
//Copiando arquivos do diretorio RELATORIOS para o diret�rio C:\SGD\UPLOAD
Memo1.Lines.Add(DateTimeToStr(Now)+' - Status: Copiando arquivos do diret�rio C:\SGD\Horas Conferentes para C:\SGD\Upload\Horas Conferentes');
FillChar(DadosInv,SizeOf(DadosInv), 0);
with DadosInv do
  begin
    wFunc := FO_COPY;
    pFrom := PChar('C:\SGD\HORAS CONFERENTES\*.*');
    pTo   := PChar('C:\SGD\UPLOAD\HORAS CONFERENTES\');
    fFlags:= FOF_ALLOWUNDO;
  end;
SHFileOperation(DadosInv);
Memo1.Lines.Add(DateTimeToStr(Now)+' - Status: Arquivos copiados com sucesso.');
Except
Memo1.Lines.Add(DateTimeToStr(Now)+' - Status: Erro ao copiar os arquivos do diret�rio C:\SGD\Horas Conferentes para C:\SGD\Upload\Horas Conferentes');
End;

Try
//Copiando arquivos do diretorio RELATORIOS para o diret�rio C:\SGD\UPLOAD
Memo1.Lines.Add(DateTimeToStr(Now)+' - Status: Copiando arquivos do diret�rio C:\SGD\Transmissao para C:\SGD\Upload\Transmissao');
FillChar(DadosInv,SizeOf(DadosInv), 0);
with DadosInv do
  begin
    wFunc := FO_COPY;
    pFrom := PChar('C:\SGD\TRANSMISSAO\*.*');
    pTo   := PChar('C:\SGD\UPLOAD\TRANSMISSAO\');
    fFlags:= FOF_ALLOWUNDO;
  end;
SHFileOperation(DadosInv);
Memo1.Lines.Add(DateTimeToStr(Now)+' - Status: Arquivos copiados com sucesso.');
Except
Memo1.Lines.Add(DateTimeToStr(Now)+' - Status: Erro ao copiar os arquivos do diret�rio C:\SGD\Transmissao para C:\SGD\Upload\Transmissao');
End;

Try
//Copiando arquivos do diretorio RELATORIOS para o diret�rio C:\SGD\UPLOAD
Memo1.Lines.Add(DateTimeToStr(Now)+' - Status: Copiando arquivos do diret�rio C:\SGD\Horas Conferentes para C:\SGD\Upload\Horas Conferentes');
FillChar(DadosInv,SizeOf(DadosInv), 0);
with DadosInv do
  begin
    wFunc := FO_COPY;
    pFrom := PChar('C:\SGD\ARQUIVO FINAL\*.*');
    pTo   := PChar('C:\SGD\UPLOAD\ARQUIVO FINAL\');
    fFlags:= FOF_ALLOWUNDO;
  end;
SHFileOperation(DadosInv);
Memo1.Lines.Add(DateTimeToStr(Now)+' - Status: Arquivos copiados com sucesso.');
Except
Memo1.Lines.Add(DateTimeToStr(Now)+' - Status: Erro ao copiar os arquivos do diret�rio C:\SGD\Arquivo Final para C:\SGD\Upload\Arquivo Final');
End;

//Compactando os relat�rios
    Memo1.Lines.Add(DateTimeToStr(Now)+' - Status: In�cio da compacta��o dos diret�rios');
    Memo1.Lines.Add(DateTimeToStr(Now)+' - Status: Compactando o diret�rio C:\SGD\Upload');
    Memo1.Lines.Add(DateTimeToStr(Now)+' - Status: Arquivo Dados_'+Dm.IBInventarioINV_LOJA.Value+'_'+Dm.IBInventarioINV_NUMLOJA.Value+'.zip'+' criado com sucesso');
    Memo1.Lines.Add('');
    Memo1.Lines.Add('-----------------------------------------------------------------------------');
    Memo1.Lines.Add('PREPARANDO O ENVIO DO E-MAIL');
    Memo1.Lines.Add('');
    Memo1.Lines.Add(DateTimeToStr(Now)+' - Status: E-mail enviado ao destinat�rios '+Dm.IBMailDESTINATARIOS.Value+' com sucesso!');
    Memo1.Lines.Add('');
    Memo1.Lines.Add('-----------------------------------------------------------------------------');
    Memo1.Lines.SaveToFile('C:\SGD\Upload\SGD_Log.Txt');
    Zip('C:\SGD\','C:\SGD\Upload','Dados_'+Dm.IBInventarioINV_LOJA.Value+'_'+Dm.IBInventarioINV_NUMLOJA.Value+'.zip','*.*');
    ArqRelat:='Dados_'+Dm.IBInventarioINV_LOJA.Value+'_'+Dm.IBInventarioINV_NUMLOJA.Value+'.zip';

    //Tratando os arquivos anexos
    Mensagem.MessageParts.Clear;
    Arquivos.create(Mensagem.MessageParts, TFileName(ArqRelat));


     //Destinatarios
    Mensagem.CharSet:='iso-8859-1';
    Mensagem.From.Name := Dm.IBmailNOME_EXIBICAO.AsString; // Nome do Remetente
    Mensagem.From.Address := Dm.IBmailSERVERUSER.AsString; // E-mail do Remetente = email valido...
    Mensagem.Recipients.EMailAddresses := Dm.IBMailDESTINATARIOS.AsString;
    Mensagem.Priority := mpHigh;
    Mensagem.Subject := 'Loja '+Dm.IBInventarioINV_LOJA.AsString+' #'+Dm.IBInventarioINV_NUMLOJA.AsString+' '+FormatDateTime('dd/mm/yyyy',(Now));
    Mensagem.Body:=(Memo2.Lines);
    Mensagem.ContentType := 'multipart/related'; //'text/html'; //mensagem em html
    Mensagem.ReceiptRecipient.Text := Mensagem.From.Text; // confirma��o de leitura



  try
    IdSMTP1 := TIdSMTP.Create(nil);
    IdSSLIOHandlerSocketOpenSSL1 := TIdSSLIOHandlerSocketOpenSSL.Create(nil);

    Screen.Cursor    := crHourGlass;
    IdSMTP1.Host     := Dm.IBMailSERVERSMTP.AsString;
    IdSMTP1.Username := Dm.IBMailSERVERUSER.AsString;
    IdSMTP1.Password := Dm.IBMailSERVERPASS.AsString;
    IdSMTP1.Port     := Dm.IBMailPORTA_SMTP.Value;

    IdSMTP1.AuthType:= satDefault;
    IdSMTP1.IOHandler := IdSSLIOHandlerSocketOpenSSL1;
    IdSMTP1.UseTLS := utUseRequireTLS;

    if NOT IdSMTP1.Connected then

    IdSMTP1.Connect;
    IdSMTP1.Authenticate;

    if IdSMTP1.Connected then
     begin
      ShowMessage('CONECTADO> Aguarde o envio do email!');
      IdSMTP1.Send(Mensagem);
      Application.ProcessMessages;
     end
    else
      ShowMessage('DESCONECTADO> Conex�o FALHOU!');
  Except
    on E:Exception do
      ShowMessage(e.Message);
  end;

  IdSMTP1.Disconnect;
  Screen.Cursor:= crDefault;

Application.MessageBox('Email enviado com sucesso!', 'Mensagem do Sistema', MB_ICONINFORMATION + MB_OK);
Screen.Cursor:=crDefault;
end;

end.









