unit UFrmProdQuant;

interface

uses
 Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
 Dialogs, StdCtrls, Buttons, ComCtrls, ExtCtrls, sBitBtn, RpDefine, RpRender,
 RpRenderPDF, RpRenderText, DBCtrls, sLabel;



type
  TFrmProdQuant = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    StatusBar1: TStatusBar;
    Panel3: TPanel;
    Label2: TLabel;
    Lbl_ValorFinal: TLabel;
    Edit1: TEdit;
    sBitBtn1: TsBitBtn;
    Edit2: TEdit;
    rvPdf: TRvRenderPDF;
    rvTxt: TRvRenderText;
    sLabelFX1: TsLabelFX;
    procedure sBitBtn1Click(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure Edit2KeyPress(Sender: TObject; var Key: Char);
    procedure Edit1Exit(Sender: TObject);
    procedure Edit2Exit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmProdQuant: TFrmProdQuant;

implementation

uses UDm;

{$R *.dfm}

procedure TFrmProdQuant.sBitBtn1Click(Sender: TObject);
begin
 Try
  try

    If (Edit1.Text <> '') and  (Edit2.Text <> '') then
      begin
       if StrToInt(Edit2.Text) < StrToInt(Edit1.Text) then
        begin
         ShowMessage('A Quantidade inicial deve ser menor que quantidade final. Digite um intervalo v�lido');
         Edit1.Clear;
         Edit2.Clear;
         Edit1.SetFocus;
        end
       else
        begin
          Dm.QRelProdQuant.Close;
          Dm.QRelProdQuant.ParamByName('QTDINI').Value := Edit1.Text;
          Dm.QRelProdQuant.ParamByName('QTDFIM').Value := Edit2.Text;
          Dm.QRelProdQuant.Open;

          if Dm.QRelProdQuant.RecordCount > 0 then
            begin
             Dm.RvPrjWis.Close;
             Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\AuditoriaEanValor.rav';
             Dm.RvPrjWis.Open;
             Dm.RvSysWis.DefaultDest := rdPreview;
             Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
             Dm.RvPrjWis.ExecuteReport('RelEanValor');

              // Salvando relat�rio em PDF automaticamente
              Try
                Dm.RvSysWis.DefaultDest:= rdFile;
                Dm.RvSysWis.DoNativeOutput:= false;
                Dm.RvSysWis.RenderObject:= rvPdf;
                Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Relatorio de Auditoria por faixa de Quantidade_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
                Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
                Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\AuditoriaEanValor.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
                Dm.RvPrjWis.Engine:= Dm.RvSysWis;
                Dm.RvPrjWis.Execute;
              Except
                ShowMessage('Erro ao gerar o relat�rio em PDF');
              end;

              // Salvando relat�rio em TXT automaticamente
              Try
                Dm.RvSysWis.DefaultDest:= rdFile;
                Dm.RvSysWis.DoNativeOutput:= false;
                Dm.RvSysWis.RenderObject:= rvTxt;
                Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Relatorio de Auditoria por faixa de Quantidade_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.txt'; //caminho onde vai gerar o arquivo pdf
                Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
                Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\AuditoriaEanValor.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
                Dm.RvPrjWis.Engine:= Dm.RvSysWis;
                Dm.RvPrjWis.Execute;
              except
                ShowMessage('Erro ao gerar o relat�rio em TXT');
              end;

            end
          else
            begin
             Application.MessageBox('N�o existem produtos nessa faixa de quantidade','Mensagem do sistema', MB_ICONEXCLAMATION+ MB_OK);
            end;
        end;
      end
    else
      Begin
       ShowMessage('N�o foi informado o intervalo de quantidades. O sistema ir� atribuir quantidades de 1 at� 100');
       Dm.QRelProdQuant.Close;
       Dm.QRelProdQuant.ParamByName('QTDINI').Value := 1;
       Dm.QRelProdQuant.ParamByName('QTDFIM').Value := 100;
       Dm.QRelProdQuant.Open;

        if Dm.QRelProdQuant.RecordCount > 0 then
         begin
          Dm.RvPrjWis.Close;
          Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\AuditoriaEanValor.rav';
          Dm.RvPrjWis.Open;
          Dm.RvSysWis.DefaultDest := rdPreview;
          Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
          Dm.RvPrjWis.ExecuteReport('RelEanValor');

                      // Salvando relat�rio em PDF automaticamente
            Try
              Dm.RvSysWis.DefaultDest:= rdFile;
              Dm.RvSysWis.DoNativeOutput:= false;
              Dm.RvSysWis.RenderObject:= rvPdf;
              Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Relatorio de Auditoria por faixa de Quantidade_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
              Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
              Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\AuditoriaEanValor.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
              Dm.RvPrjWis.Engine:= Dm.RvSysWis;
              Dm.RvPrjWis.Execute;
            Except
              ShowMessage('Erro ao gerar o relat�rio em PDF');
            end;

            // Salvando relat�rio em TXT automaticamente
            Try
              Dm.RvSysWis.DefaultDest:= rdFile;
              Dm.RvSysWis.DoNativeOutput:= false;
              Dm.RvSysWis.RenderObject:= rvTxt;
              Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Relatorio de Auditoria por faixa de Quantidade_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.txt'; //caminho onde vai gerar o arquivo pdf
              Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
              Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\AuditoriaEanValor.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
              Dm.RvPrjWis.Engine:= Dm.RvSysWis;
              Dm.RvPrjWis.Execute;
            except
              ShowMessage('Erro ao gerar o relat�rio em TXT');
            end;

         end
        else
            begin
             Application.MessageBox('N�o existem produtos nessa faixa de quantidade','Mensagem do sistema', MB_ICONEXCLAMATION+ MB_OK);
            end;
      end;

  except
     Application.MessageBox('Erro ao gerar relat�rio','Mensagem do sistema', MB_ICONERROR+ MB_OK);
  end;

 Finally
   Dm.QRelProdQuant.Close;
 End;
end;

procedure TFrmProdQuant.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
 if not (key in ['0'..'9',#8]) then
         begin
          key:=#0;
          beep;
         end;
end;

procedure TFrmProdQuant.Edit2KeyPress(Sender: TObject; var Key: Char);
begin
 if not (key in ['0'..'9',#8]) then
   begin
     key:=#0;
     beep;
   end;
end;

procedure TFrmProdQuant.Edit1Exit(Sender: TObject);
begin
 if not (Edit2.Text = '') then
  begin
   if StrToInt(Edit2.Text) < StrToInt(Edit1.Text) then
      begin
       ShowMessage('A Quantidade inicial deve ser menor que quantidade final. Digite um intervalo v�lido');
       Edit1.Clear;
       Edit2.Clear;
       Edit1.SetFocus;
      end
  end;
end;

procedure TFrmProdQuant.Edit2Exit(Sender: TObject);
begin
 if (StrToInt(Edit2.Text) < StrToInt(Edit1.Text)) then
    begin
     ShowMessage('A Quantidade inicial deve ser menor que quantidade final. Digite um intervalo v�lido');
     Edit1.Clear;
     Edit2.Clear;
     Edit1.SetFocus;
    end
end;

end.
