unit URestTransm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, ComCtrls, StdCtrls, Mask, DBCtrls, ExtCtrls,
  Buttons, CheckLst, db, FileCtrl, sLabel;

type
  TFrmRestTram = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    StatusBar1: TStatusBar;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    BitBtn1: TBitBtn;
    CheckListBox1: TCheckListBox;
    BitBtn2: TBitBtn;
    GroupBox2: TGroupBox;
    DBGrid1: TDBGrid;
    DirectoryListBox1: TDirectoryListBox;
    Label2: TLabel;
    DBText1: TDBText;
    Label3: TLabel;
    DriveComboBox1: TDriveComboBox;
    sLabelFX1: TsLabelFX;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public

{ Public declarations }
  end;

var
  FrmRestTram: TFrmRestTram;

implementation

uses UFuncProc, UDm;

{$R *.dfm}





procedure TFrmRestTram.BitBtn1Click(Sender: TObject);
var
i: integer;
begin
  i := 0;
   For i:= CheckListBox1.Items.Count -1 downto 0 do
     CheckListBox1.Items.Delete(i);

  CheckListBox1.Perform(LB_DIR, DDL_ARCHIVE or
  DDL_DIRECTORY,LongInt(DirectoryListBox1.Directory + '\*.txt'));
End;

procedure TFrmRestTram.BitBtn2Click(Sender: TObject);
var
i:integer;
TransID: integer;
Transtexto: String[5];
OS : String[13];
ID: integer;
Begin
  For i:= CheckListBox1.Items.Count -1 downto 0 do
    If CheckListBox1.Checked[i] = True then
    Begin
      Transtexto := Copy(CheckListBox1.Items.Strings[i],7,11);
      TransID := strtoint(Transtexto);
      OS := Copy(CheckListBox1.Items.Strings[i],15,27);
      If OS = Dm.IBInventarioINV_OS.Value then
      Begin
        If Dm.IBTransmissao.locate('TRA_ID',inttostr(TransID),[Lopartialkey,locaseinsensitive]) = True then
          ShowMessage('N�o � poss�vel importar a transmiss�o ' + inttostr(TransID) + ' porque ela j� est� importada')
        Else
         begin
           IF DM.IBInventarioINV_LOJA.Value ='CLB' then
            ImportaDadosCLB(RetiraSpace(DirectoryListBox1.Directory + '\' + CheckListBox1.Items.Strings[i]),0)
           else
           IF DM.IBInventarioINV_LOJA.Value ='GST' then
            ImportaDadosGST(RetiraSpace(DirectoryListBox1.Directory + '\' + CheckListBox1.Items.Strings[i]),0)
           else  
           IF DM.IBInventarioINV_LOJA.Value ='CTD' then
            ImportaDadosCTD(RetiraSpace(DirectoryListBox1.Directory + '\' + CheckListBox1.Items.Strings[i]),0)
           else
           IF DM.IBInventarioINV_LOJA.Value ='ONF' then
            ImportaDadosONF(RetiraSpace(DirectoryListBox1.Directory + '\' + CheckListBox1.Items.Strings[i]),0)
           else
           IF DM.IBInventarioINV_LOJA.Value ='OCD' then
            ImportaDadosOCD(RetiraSpace(DirectoryListBox1.Directory + '\' + CheckListBox1.Items.Strings[i]),0)
           else
           IF DM.IBInventarioINV_LOJA.Value ='ONP' then
            ImportaDadosONP(RetiraSpace(DirectoryListBox1.Directory + '\' + CheckListBox1.Items.Strings[i]),0)
           else
           IF DM.IBInventarioINV_LOJA.Value ='CIC' then
            ImportaDadosCIC(RetiraSpace(DirectoryListBox1.Directory + '\' + CheckListBox1.Items.Strings[i]),0)
           else
           IF DM.IBInventarioINV_LOJA.Value ='MRG' then
            ImportaDadosMRG(RetiraSpace(DirectoryListBox1.Directory + '\' + CheckListBox1.Items.Strings[i]),0)
           else
           IF DM.IBInventarioINV_LOJA.Value ='SCH' then
            ImportaDadosSCH(RetiraSpace(DirectoryListBox1.Directory + '\' + CheckListBox1.Items.Strings[i]),0)
           else
           IF (DM.IBInventarioINV_LOJA.Value ='PDA') or (DM.IBInventarioINV_LOJA.Value ='ASA') then
            ImportaDadosPDA(RetiraSpace(DirectoryListBox1.Directory + '\' + CheckListBox1.Items.Strings[i]),0)
           else
           IF (DM.IBInventarioINV_LOJA.Value ='CP1') then
            ImportaDadosCP1(RetiraSpace(DirectoryListBox1.Directory + '\' + CheckListBox1.Items.Strings[i]),0)
           else
           IF (DM.IBInventarioINV_LOJA.Value ='CP2') then
            ImportaDadosCP2(RetiraSpace(DirectoryListBox1.Directory + '\' + CheckListBox1.Items.Strings[i]),0)
           else
           IF (DM.IBInventarioINV_LOJA.Value ='CP3') then
            ImportaDadosCP3(RetiraSpace(DirectoryListBox1.Directory + '\' + CheckListBox1.Items.Strings[i]),0)
           else
           IF (DM.IBInventarioINV_LOJA.Value ='LVC') then
            ImportaDadosLVC(RetiraSpace(DirectoryListBox1.Directory + '\' + CheckListBox1.Items.Strings[i]),0)
           else
           IF (DM.IBInventarioINV_LOJA.Value ='SAR') then
            ImportaDadosSAR(RetiraSpace(DirectoryListBox1.Directory + '\' + CheckListBox1.Items.Strings[i]),0)
           else
           IF (DM.IBInventarioINV_LOJA.Value ='TNT') then
            ImportaDadosTNT(RetiraSpace(DirectoryListBox1.Directory + '\' + CheckListBox1.Items.Strings[i]),0)
           else
           if (DM.IBInventarioINV_LOJA.Value ='DIA') then
            ImportaDadosDIA(RetiraSpace(DirectoryListBox1.Directory + '\' + CheckListBox1.Items.Strings[i]),0)
           else
           if (DM.IBInventarioINV_LOJA.Value ='LMD') then
            ImportaDadosLMD(RetiraSpace(DirectoryListBox1.Directory + '\' + CheckListBox1.Items.Strings[i]),0)
           else
           IF DM.IBInventarioINV_LOJA.Value ='FRN' then
            ImportaDadosFRN(RetiraSpace(DirectoryListBox1.Directory + '\' + CheckListBox1.Items.Strings[i]),0)
           else
           IF DM.IBInventarioINV_LOJA.Value ='SAF' then
            ImportaDadosSAF(RetiraSpace(DirectoryListBox1.Directory + '\' + CheckListBox1.Items.Strings[i]),0)
           else
           IF DM.IBInventarioINV_LOJA.Value ='RLD' then
            ImportaDadosRLD(RetiraSpace(DirectoryListBox1.Directory + '\' + CheckListBox1.Items.Strings[i]),0)
           else
           IF DM.IBInventarioINV_LOJA.Value ='AND' then
            ImportaDadosAND(RetiraSpace(DirectoryListBox1.Directory + '\' + CheckListBox1.Items.Strings[i]),0)
           else
           IF DM.IBInventarioINV_LOJA.Value ='MRS' then
            ImportaDadosMRS(RetiraSpace(DirectoryListBox1.Directory + '\' + CheckListBox1.Items.Strings[i]),0)
           else
           IF DM.IBInventarioINV_LOJA.Value ='CEA' then
            ImportaDadosCEA(RetiraSpace(DirectoryListBox1.Directory + '\' + CheckListBox1.Items.Strings[i]),0)
           else
           IF DM.IBInventarioINV_LOJA.Value ='PRN' then
            ImportaDadosPRN(RetiraSpace(DirectoryListBox1.Directory + '\' + CheckListBox1.Items.Strings[i]),0)
           else
           IF DM.IBInventarioINV_LOJA.Value ='WIS' then
            ImportaDadosWIS(RetiraSpace(DirectoryListBox1.Directory + '\' + CheckListBox1.Items.Strings[i]),0)
           else
            ImportaDados(RetiraSpace(DirectoryListBox1.Directory + '\' + CheckListBox1.Items.Strings[i]),0)
         end;

      End
      Else
        ShowMessage('A transmiss�o ' + CheckListBox1.Items.Strings[i] + #13 +
                    ' '  + #13 +
                    'n�o pertence a OS ' + Dm.IBInventarioINV_OS.Value );
    End;


    For i:= CheckListBox1.Items.Count -1 downto 0 do
    CheckListBox1.Checked[i] := False;

    Dm.IBTransmissao.Close;
    Dm.IBTransmissao.Open;
    Dm.IBTransmissao.First;

    //Alterar o generator para o maior valor que tiver de ID na tabela transmiss�o
    ID := 0;
    While not Dm.IBTransmissao.Eof Do
    Begin
      if ID < Dm.IBTransmissaoTRA_ID.Value then
        ID := Dm.IBTransmissaoTRA_ID.Value;
      Dm.IBTransmissao.Next;
    End;

    Dm.QAlteraValorGenTransID.Close;
    DM.QAlteraValorGenTransID.SQL.Add('SET GENERATOR TRANSMISSAO_ID TO ' + INTTOSTR(ID));
    DM.QAlteraValorGenTransID.Open;
    Dm.QAlteraValorGenTransID.Close;
    DM.Transacao.CommitRetaining;

    Application.MessageBox('Restaura��o conclu�da com �xito. Ser� preciso refazer todos os ajustes e corre��es.','Mensagem do Sistema', MB_ICONINFORMATION + MB_OK);
    ShowMessage('');

end;


procedure TFrmRestTram.FormShow(Sender: TObject);
begin
 Dm.IBInventario.Open;
 Dm.IBTransmissao.Open;
 Dm.IBHorarios.Open;
 Dm.IBContagens.Open;
 //Dm.CdsContagens.Open;
 Dm.IBArea.Open;
 Dm.IBSecao.Open;
end;

end.
