unit UFrmAllLayouts;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, StdCtrls, Buttons, sBitBtn, Menus;

type
  TFrmAllLayouts = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox3: TGroupBox;
    sBitBtn1: TsBitBtn;
    PopInput: TPopupMenu;
    Excluir1: TMenuItem;
    GridLayout: TStringGrid;
    ExcluiresteLAYOUTDESADA1: TMenuItem;
    procedure sBitBtn1Click(Sender: TObject);
    procedure Excluir1Click(Sender: TObject);
    procedure GridLayoutDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure ExcluiresteLAYOUTDESADA1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmAllLayouts: TFrmAllLayouts;
  ClienteInput, ClienteOutput, DelOkInput, DelOkOutput: String;
  totalInput: integer;

implementation
Uses Udm, UFuncProc, ULogin;

{$R *.dfm}

procedure TFrmAllLayouts.sBitBtn1Click(Sender: TObject);
begin
close;
end;

procedure TFrmAllLayouts.Excluir1Click(Sender: TObject);
var resposta: integer;
begin
Resposta:= Application.MessageBox(PChar(ClienteInput),'Aviso',mb_YesNo+mb_IconInformation);
  case Resposta Of
    6: Begin
         Try
            Dm.IBLayoutInput.Delete;
            Dm.Transacao.CommitRetaining;
            MessageBox(Application.Handle,Pchar(DelOkInput), 'Mensagem do Sistema', MB_OK+MB_ICONINFORMATION);
            // todos os layouts de sa�da
            DM.IBLayoutOutput.Close;
            DM.IBLayoutOutput.SelectSql.Clear;
            DM.IBLayoutOutput.SelectSql.Add('SELECT * FROM LAYOUT_OUTPUT');
            Dm.IBLayoutOutput.Open;

            //todos os layouts de entrada
            DM.IBLayoutInput.Close;
            DM.IBLayoutInput.SelectSql.Clear;
            DM.IBLayoutInput.SelectSql.Add('SELECT * FROM LAYOUT_INPUT');
            Dm.IBLayoutInput.Open;
        except
            ShowMessage('Erro ao deletar o cliente '+Dm.IBLayoutInputCLIENTE.Value+#13+'Favor tentar novamente');
            Dm.Transacao.RollbackRetaining;
        end;
      end;
    7: Dm.Transacao.RollbackRetaining;
  end;

end;


procedure TFrmAllLayouts.GridLayoutDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var Tmp: string;
begin
{centralizar texto no grid}
if ACol = 0 then
  begin
    Tmp := GridLayout.Cells[ACol,ARow];
    GridLayout.Canvas.FillRect(Rect);
    DrawText(GridLayout.Canvas.Handle,PChar(Tmp),-1,Rect,DT_CENTER);
  end;

if ACol = 0 then
    GridLayout.Canvas.Font.Style := [fsBold];
  end;

procedure TFrmAllLayouts.FormShow(Sender: TObject);
var lin: integer;

begin// todos os layouts de sa�da

DM.IBLayoutOutput.Close;
DM.IBLayoutOutput.SelectSql.Clear;
DM.IBLayoutOutput.SelectSql.Add('SELECT * FROM LAYOUT_OUTPUT');
Dm.IBLayoutOutput.Open;

//todos os layouts de entrada
DM.IBLayoutInput.Close;
DM.IBLayoutInput.SelectSql.Clear;
DM.IBLayoutInput.SelectSql.Add('SELECT * FROM LAYOUT_INPUT');
Dm.IBLayoutInput.Open;

{Retorna a quantidade de clientes cadastrados na tabela LayoutInput}
      DM.IBLayoutInput.Close;
      DM.IBLayoutInput.Open;
      DM.IBLayoutInput.First;
      DM.IBLayoutInput.Last;
      TotalInput := DM.IBLayoutInput.RecordCount;
      DM.IBLayoutInput.First;

{Ajusta o STringGrid}
    with GridLayout do
      Begin
        Align:=alCLient;
        ScrollBars:=ssVertical;
        RowCount:=1;
        ColCount:=3;
        Cells[0,0]:= 'Cliente';
        ColWidths[0] := 90;
        Cells[1,0]:= 'Layout de Entrada';
        ColWidths[1] := 100;
        Cells[2,0]:= 'Layout de Sa�da';
        ColWidths[2] := 100;
        
      End;



{Grid recebe todos os clientes}
    for lin:=1 to totalInput do
      begin
        GridLayout.Cells[0,lin]:=Dm.IBLayoutInputCLIENTE.Value;
        GridLayout.RowCount:=GridLayout.RowCount+1;
        Dm.IBLayoutInput.Next;

      end;

{Procura na coluna CLIENTE, c�lula x a sigla do cliente e verifica se h� algum
layout de entrada gravado, dependendo do resultado escreve SIM ou NAO na coluna LAYOUT DE ENTRADA}
    For lin:=1 to totalInput do
      begin
        DM.IBLayoutInput.Close;
        DM.IBLayoutInput.SelectSql.Clear;
        DM.IBLayoutInput.SelectSql.Add('SELECT * FROM LAYOUT_INPUT WHERE CLIENTE = :CLIENTE');
        DM.IBLayoutInput.ParamByName('CLIENTE').Value:=GridLayout.Cells[0,lin];
        Dm.IBLayoutInput.Open;
        If Dm.IBLayoutInput.IsEmpty then
           GridLayout.Cells[1,lin]:='N�O' else GridLayout.Cells[1,lin]:='SIM';
        Dm.IBLayoutInput.Next;
      end;

{Procura na coluna CLIENTE, c�lula x a sigla do cliente e verifica se h� algum
layout de Sa�da gravado, dependendo do resultado escreve SIM ou NAO na coluna LAYOUT DE SA�DA}
    For lin:=1 to totalInput do
      begin
        DM.IBLayoutOutput.Close;
        DM.IBLayoutOutput.SelectSql.Clear;
        DM.IBLayoutOutput.SelectSql.Add('SELECT * FROM LAYOUT_OUTPUT WHERE CLIENTE = :CLIENTE');
        DM.IBLayoutOutput.ParamByName('CLIENTE').Value:=GridLayout.Cells[0,lin];
        Dm.IBLayoutOutput.Open;
        If Dm.IBLayoutOutput.IsEmpty then
           GridLayout.Cells[2,lin]:='N�O' else GridLayout.Cells[2,lin]:='SIM';
        Dm.IBLayoutOutput.Next;
      end;



{Atribui as mensagens de confirma��o nas vari�veis para serem passadas ao Application.Message}
ClienteInput:='Deseja realmente EXCLUIR o Layout do cliente '+Dm.IBLayoutInputCLIENTE.Value+'?';
DelOkInput:='Cliente '+Dm.IBLayoutInputCLIENTE.Value+' exclu�do com �xito!';
ClienteOutput:='Deseja realmente EXCLUIR o Layout do cliente '+Dm.IBLayoutOutputCLIENTE.Value+'?';
DelOkOutput:='Cliente '+Dm.IBLayoutOutputCLIENTE.Value+' exclu�do com �xito!';


end;

procedure TFrmAllLayouts.ExcluiresteLAYOUTDESADA1Click(Sender: TObject);
var resposta: integer;
begin
Resposta:= Application.MessageBox(PChar(ClienteOutput),'Aviso',mb_YesNoCancel+mb_IconInformation);
  case Resposta Of
    2: ShowMessage('Exclus�o CANCELADA');
    6: Begin
         Try
            Dm.IBLayoutOutput.Delete;
            Dm.Transacao.CommitRetaining;
            MessageBox(Application.Handle,Pchar(DelOkOutput), 'Mensagem do Sistema', MB_OK+MB_ICONINFORMATION);
        except
            ShowMessage('Erro ao deletar o cliente '+Dm.IBLayoutOutputCLIENTE.Value+#13+'Favor tentar novamente');
            Dm.Transacao.RollbackRetaining;
        end;
      end;
    7: Dm.Transacao.RollbackRetaining;
  end;

end;

end.
