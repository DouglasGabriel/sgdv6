object Dm2: TDm2
  OldCreateOrder = False
  Height = 884
  Width = 1342
  object QMap_total: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    ForcedRefresh = True
    BufferChunks = 100000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select * from TOTAL_MAP')
    Left = 24
    Top = 8
    object QMap_totalMAP_SECINI: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'MAP_SECINI'
      Origin = 'TOTAL_MAP.MAP_SECINI'
      ProviderFlags = []
      ReadOnly = True
      Size = 4
    end
    object QMap_totalMAP_DESC: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'MAP_DESC'
      Origin = 'TOTAL_MAP.MAP_DESC'
      ProviderFlags = []
      ReadOnly = True
      Size = 30
    end
    object QMap_totalMAP_TOTAL: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'MAP_TOTAL'
      Origin = 'TOTAL_MAP.MAP_TOTAL'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object dsMap_Total: TDataSource
    DataSet = QMap_total
    Left = 96
    Top = 8
  end
  object QMap_TotalGeral: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    ForcedRefresh = True
    BufferChunks = 100000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT SUM(MAP_TOTAL) FROM TOTAL_MAP')
    Left = 978
    Top = 4
    object QMap_TotalGeralSUM: TIntegerField
      FieldName = 'SUM'
      ProviderFlags = []
    end
  end
  object dsMap_TotalGeral: TDataSource
    DataSet = QMap_TotalGeral
    Left = 1072
    Top = 8
  end
  object dsHor_break: TDataSource
    Left = 818
    Top = 4
  end
  object spUpdate_Hor_Sec: TIBStoredProc
    ForcedRefresh = True
    StoredProcName = 'ATUALIZA_MATRICULA'
    Left = 168
    Top = 480
    ParamData = <
      item
        DataType = ftString
        Name = 'NEWMATRICULA'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'OLDMATRICULA'
        ParamType = ptInput
      end>
  end
  object QGeral: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    Left = 607
    Top = 7
  end
  object QSetor: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 100000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'Select cad_id,  Cad_Setor, Cad_setdesc from cadastro'
      'order by cad_setor')
    Left = 657
    Top = 7
    object QSetorCAD_SETOR: TIBStringField
      FieldName = 'CAD_SETOR'
      Origin = 'CADASTRO.CAD_SETOR'
      Size = 5
    end
    object QSetorCAD_ID: TIntegerField
      FieldName = 'CAD_ID'
      Origin = 'CADASTRO.CAD_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object QSetorCAD_SETDESC: TIBStringField
      FieldName = 'CAD_SETDESC'
      Origin = 'CADASTRO.CAD_SETDESC'
      Size = 100
    end
  end
  object dsSetor: TDataSource
    DataSet = QSetor
    Left = 657
    Top = 7
  end
  object sp_atualiza_contagens: TIBStoredProc
    Left = 280
    Top = 480
  end
  object dsGeral: TDataSource
    DataSet = QGeral
    Left = 607
    Top = 7
  end
  object QRelSecaoAlterada: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      
        'select s.sec_secao, s.hor_matricula, s.sec_status, h.ros_nome fr' +
        'om secao s left join horario h on (s.hor_matricula = h.ros_matri' +
        'cula)'
      'where sec_status = '#39'DEL'#39)
    Left = 40
    Top = 256
    object QRelSecaoAlteradaSEC_SECAO: TIBStringField
      FieldName = 'SEC_SECAO'
      Origin = 'SECAO.SEC_SECAO'
      Size = 25
    end
    object QRelSecaoAlteradaHOR_MATRICULA: TIBStringField
      FieldName = 'HOR_MATRICULA'
      Origin = 'SECAO.HOR_MATRICULA'
      Size = 11
    end
    object QRelSecaoAlteradaSEC_STATUS: TIBStringField
      FieldName = 'SEC_STATUS'
      Origin = 'SECAO.SEC_STATUS'
      Size = 3
    end
    object QRelSecaoAlteradaROS_NOME: TIBStringField
      FieldName = 'ROS_NOME'
      Origin = 'HORARIO.ROS_NOME'
      Size = 50
    end
  end
  object RvSecaoAlterada: TRvDataSetConnection
    RuntimeVisibility = rtDeveloper
    DataSet = QRelSecaoAlterada
    Left = 40
    Top = 256
  end
  object QRelContagensDeletadas: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      
        'select CD.CAD_CODINT, c.CON_EAN, c.CON_QUANTIDADE, c.HOR_MATRICU' +
        'LA, c.SEC_SECAO, CD.CAD_DESC'
      
        'FROM CONTAGENS C LEFT JOIN CADASTRO_CONSO CD ON (C.CON_EAN = CD.' +
        'CAD_EAN)'
      'where c.con_status = '#39'DEL'#39
      'ORDER BY CD.CAD_CODINT DESC, CD.CAD_EAN DESC, C.CON_QUANTIDADE')
    Left = 160
    Top = 256
    object QRelContagensDeletadasCAD_CODINT: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'CAD_CODINT'
      Origin = 'CADASTRO_CONSO.CAD_CODINT'
      ProviderFlags = []
      ReadOnly = True
    end
    object QRelContagensDeletadasCON_EAN: TIBStringField
      FieldName = 'CON_EAN'
      Origin = 'CONTAGENS.CON_EAN'
      Size = 25
    end
    object QRelContagensDeletadasCON_QUANTIDADE: TFloatField
      FieldName = 'CON_QUANTIDADE'
      Origin = 'CONTAGENS.CON_QUANTIDADE'
    end
    object QRelContagensDeletadasHOR_MATRICULA: TIBStringField
      FieldName = 'HOR_MATRICULA'
      Origin = 'CONTAGENS.HOR_MATRICULA'
      Size = 11
    end
    object QRelContagensDeletadasSEC_SECAO: TIBStringField
      FieldName = 'SEC_SECAO'
      Origin = 'CONTAGENS.SEC_SECAO'
      Size = 25
    end
    object QRelContagensDeletadasCAD_DESC: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'CAD_DESC'
      Origin = 'CADASTRO_CONSO.CAD_DESC'
      ProviderFlags = []
      ReadOnly = True
      Size = 100
    end
  end
  object RvContagensDeletadas: TRvDataSetConnection
    RuntimeVisibility = rtDeveloper
    DataSet = QRelContagensDeletadas
    Left = 160
    Top = 256
  end
  object spAtualiza_Secao_Status: TIBStoredProc
    StoredProcName = 'ATUALIZA_SECAO_STATUS'
    Left = 48
    Top = 480
    ParamData = <
      item
        DataType = ftInteger
        Name = 'TRA_ID'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'SECAO_NOVA'
        ParamType = ptInput
      end>
  end
  object spVerifica_Horario: TIBStoredProc
    ForcedRefresh = True
    StoredProcName = 'VERIFICA_HORARIO'
    Left = 384
    Top = 488
    object spVerifica_HorarioRESULT_SAIDA: TIntegerField
      FieldName = 'RESULT_SAIDA'
      Origin = 'VERIFICA_HORARIO.RESULT_SAIDA'
    end
    object spVerifica_HorarioRESULT_ENTRADA: TIntegerField
      FieldName = 'RESULT_ENTRADA'
      Origin = 'VERIFICA_HORARIO.RESULT_ENTRADA'
    end
  end
  object QGeraArqFinalCPV: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      
        ' select C.CAD_CODINT AS SKU, C.CAD_EAN AS LOTE, C.CAD_PRECOTXT A' +
        'S VALIDADE, sum(D.CON_QUANTIDADE)'
      'from CONTAGENS D'
      'inner JOIN CADASTRO_CONSO C ON (C.CAD_EAN = D.CON_EAN)'
      '  where CON_STATUS =  '#39'.'#39' or CON_STATUS is Null'
      '  GROUP BY C.CAD_EAN, C.CAD_CODINT, C.CAD_PRECOTXT')
    Left = 424
    Top = 64
    object QGeraArqFinalCPVSKU: TIBStringField
      FieldName = 'SKU'
      ProviderFlags = []
    end
    object QGeraArqFinalCPVLOTE: TIBStringField
      FieldName = 'LOTE'
      ProviderFlags = []
      Size = 25
    end
    object QGeraArqFinalCPVVALIDADE: TIBStringField
      FieldName = 'VALIDADE'
      ProviderFlags = []
      Size = 15
    end
    object QGeraArqFinalCPVSUM: TFloatField
      FieldName = 'SUM'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalLVC: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      ' select C.CAD_CODINT, sum(D.CON_QUANTIDADE), d.sec_secao'
      'from CONTAGENS D'
      'inner JOIN CADASTRO_CONSO C ON (C.CAD_EAN = D.CON_EAN)'
      '  where CON_STATUS =  '#39'.'#39' or CON_STATUS is Null'
      '  GROUP BY 3,1')
    Left = 920
    Top = 64
    object QGeraArqFinalLVCCAD_CODINT: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'CAD_CODINT'
      Origin = 'CADASTRO_CONSO.CAD_CODINT'
      ProviderFlags = []
      ReadOnly = True
    end
    object QGeraArqFinalLVCSUM: TFloatField
      FieldName = 'SUM'
      ProviderFlags = []
    end
    object QGeraArqFinalLVCSEC_SECAO: TIBStringField
      FieldName = 'SEC_SECAO'
      Origin = 'CONTAGENS.SEC_SECAO'
      Size = 25
    end
  end
  object QGeraArqFinalRCH: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = False
    Left = 232
    Top = 64
    object QGeraArqFinalRCHSISTEMA: TIBStringField
      FieldName = 'SISTEMA'
      Origin = 'ENDERECO.SISTEMA'
      Size = 3
    end
    object QGeraArqFinalRCHTIPO: TIBStringField
      FieldName = 'TIPO'
      Origin = 'ENDERECO.TIPO'
      Size = 3
    end
    object QGeraArqFinalRCHCON_ENDERECO: TIBStringField
      FieldName = 'CON_ENDERECO'
      Origin = 'CONTAGENS.CON_ENDERECO'
      Size = 50
    end
    object QGeraArqFinalRCHCON_EAN: TIBStringField
      FieldName = 'CON_EAN'
      Origin = 'CONTAGENS.CON_EAN'
      Size = 25
    end
    object QGeraArqFinalRCHSUM: TFloatField
      FieldName = 'SUM'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalSAR: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      
        ' select C.CAD_CODINT, D.SEC_SECAO,  C.CAD_EAN, sum(D.CON_QUANTID' +
        'ADE)'
      'from CONTAGENS D'
      'inner JOIN CADASTRO_CONSO C ON (C.CAD_EAN = D.CON_EAN)'
      'where CON_STATUS =  '#39'.'#39' or CON_STATUS is Null'
      '  GROUP BY 1,3,2')
    Left = 728
    Top = 64
    object QGeraArqFinalSARCAD_CODINT: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'CAD_CODINT'
      Origin = 'CADASTRO_CONSO.CAD_CODINT'
      ProviderFlags = []
      ReadOnly = True
    end
    object QGeraArqFinalSARCAD_EAN: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'CAD_EAN'
      Origin = 'CADASTRO_CONSO.CAD_EAN'
      ProviderFlags = []
      ReadOnly = True
      Size = 25
    end
    object QGeraArqFinalSARSUM: TFloatField
      FieldName = 'SUM'
      ProviderFlags = []
    end
    object QGeraArqFinalSARSEC_SECAO: TIBStringField
      FieldName = 'SEC_SECAO'
      Origin = 'CONTAGENS.SEC_SECAO'
      Size = 25
    end
  end
  object QryCadastroTNT: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 10000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      
        'SELECT CAD_ID, CAD_NUMLOJA, CAD_DEPOSITO, CAD_DATA, CAD_NUMINV, ' +
        'CAD_REFINV, CAD_EXERC, CAD_DOCINV, CAD_NUMITEM, CAD_DIGCODINT, C' +
        'AD_CODINT, CAD_TIPOESTOQUE, CAD_LOTE, CAD_EAN, CAD_DESC, CAD_UN,' +
        ' CAD_GRUPOCOM, CAD_QUANTIDADE, CAD_PRECO FROM CADASTRO'
      'ORDER BY CAD_EAN, CAD_CODINT')
    Left = 40
    Top = 181
    object QryCadastroTNTCAD_ID: TIntegerField
      FieldName = 'CAD_ID'
      Origin = 'CADASTRO.CAD_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object QryCadastroTNTCAD_NUMLOJA: TIBStringField
      FieldName = 'CAD_NUMLOJA'
      Origin = 'CADASTRO.CAD_NUMLOJA'
      Size = 4
    end
    object QryCadastroTNTCAD_DEPOSITO: TIBStringField
      FieldName = 'CAD_DEPOSITO'
      Origin = 'CADASTRO.CAD_DEPOSITO'
      Size = 25
    end
    object QryCadastroTNTCAD_DATA: TIBStringField
      FieldName = 'CAD_DATA'
      Origin = 'CADASTRO.CAD_DATA'
      Size = 8
    end
    object QryCadastroTNTCAD_NUMINV: TIBStringField
      FieldName = 'CAD_NUMINV'
      Origin = 'CADASTRO.CAD_NUMINV'
    end
    object QryCadastroTNTCAD_REFINV: TIBStringField
      FieldName = 'CAD_REFINV'
      Origin = 'CADASTRO.CAD_REFINV'
    end
    object QryCadastroTNTCAD_EXERC: TIBStringField
      FieldName = 'CAD_EXERC'
      Origin = 'CADASTRO.CAD_EXERC'
      Size = 4
    end
    object QryCadastroTNTCAD_DOCINV: TIBStringField
      FieldName = 'CAD_DOCINV'
      Origin = 'CADASTRO.CAD_DOCINV'
      Size = 10
    end
    object QryCadastroTNTCAD_NUMITEM: TIBStringField
      FieldName = 'CAD_NUMITEM'
      Origin = 'CADASTRO.CAD_NUMITEM'
      Size = 5
    end
    object QryCadastroTNTCAD_DIGCODINT: TIBStringField
      FieldName = 'CAD_DIGCODINT'
      Origin = 'CADASTRO.CAD_DIGCODINT'
      Size = 5
    end
    object QryCadastroTNTCAD_CODINT: TIBStringField
      FieldName = 'CAD_CODINT'
      Origin = 'CADASTRO.CAD_CODINT'
    end
    object QryCadastroTNTCAD_TIPOESTOQUE: TIBStringField
      FieldName = 'CAD_TIPOESTOQUE'
      Origin = 'CADASTRO.CAD_TIPOESTOQUE'
      Size = 3
    end
    object QryCadastroTNTCAD_LOTE: TIBStringField
      FieldName = 'CAD_LOTE'
      Origin = 'CADASTRO.CAD_LOTE'
      Size = 25
    end
    object QryCadastroTNTCAD_EAN: TIBStringField
      FieldName = 'CAD_EAN'
      Origin = 'CADASTRO.CAD_EAN'
      Size = 25
    end
    object QryCadastroTNTCAD_DESC: TIBStringField
      FieldName = 'CAD_DESC'
      Origin = 'CADASTRO.CAD_DESC'
      Size = 100
    end
    object QryCadastroTNTCAD_GRUPOCOM: TIBStringField
      FieldName = 'CAD_GRUPOCOM'
      Origin = 'CADASTRO.CAD_GRUPOCOM'
      Size = 3
    end
    object QryCadastroTNTCAD_QUANTIDADE: TFloatField
      FieldName = 'CAD_QUANTIDADE'
      Origin = 'CADASTRO.CAD_QUANTIDADE'
    end
    object QryCadastroTNTCAD_PRECO: TFloatField
      FieldName = 'CAD_PRECO'
      Origin = 'CADASTRO.CAD_PRECO'
    end
    object QryCadastroTNTCAD_UN: TIBStringField
      FieldName = 'CAD_UN'
      Origin = 'CADASTRO.CAD_UN'
      Size = 5
    end
  end
  object DsCadastroTNT: TDataSource
    DataSet = CDSImportaCadastroTNT
    Left = 264
    Top = 181
  end
  object CDSImportaCadastroTNT: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DsImportaCadastroTNT'
    Left = 152
    Top = 181
    object CDSImportaCadastroTNTCAD_ID: TIntegerField
      FieldName = 'CAD_ID'
      Required = True
    end
    object CDSImportaCadastroTNTCAD_NUMLOJA: TWideStringField
      FieldName = 'CAD_NUMLOJA'
      Size = 4
    end
    object CDSImportaCadastroTNTCAD_DEPOSITO: TWideStringField
      FieldName = 'CAD_DEPOSITO'
      Size = 25
    end
    object CDSImportaCadastroTNTCAD_DATA: TWideStringField
      FieldName = 'CAD_DATA'
      Size = 8
    end
    object CDSImportaCadastroTNTCAD_NUMINV: TWideStringField
      FieldName = 'CAD_NUMINV'
    end
    object CDSImportaCadastroTNTCAD_REFINV: TWideStringField
      FieldName = 'CAD_REFINV'
    end
    object CDSImportaCadastroTNTCAD_EXERC: TWideStringField
      FieldName = 'CAD_EXERC'
      Size = 4
    end
    object CDSImportaCadastroTNTCAD_DOCINV: TWideStringField
      FieldName = 'CAD_DOCINV'
      Size = 10
    end
    object CDSImportaCadastroTNTCAD_NUMITEM: TWideStringField
      FieldName = 'CAD_NUMITEM'
      Size = 5
    end
    object CDSImportaCadastroTNTCAD_DIGCODINT: TWideStringField
      FieldName = 'CAD_DIGCODINT'
      Size = 5
    end
    object CDSImportaCadastroTNTCAD_CODINT: TWideStringField
      FieldName = 'CAD_CODINT'
    end
    object CDSImportaCadastroTNTCAD_TIPOESTOQUE: TWideStringField
      FieldName = 'CAD_TIPOESTOQUE'
      Size = 3
    end
    object CDSImportaCadastroTNTCAD_LOTE: TWideStringField
      FieldName = 'CAD_LOTE'
      Size = 25
    end
    object CDSImportaCadastroTNTCAD_EAN: TWideStringField
      FieldName = 'CAD_EAN'
      Size = 25
    end
    object CDSImportaCadastroTNTCAD_DESC: TWideStringField
      FieldName = 'CAD_DESC'
      Size = 100
    end
    object CDSImportaCadastroTNTCAD_GRUPOCOM: TWideStringField
      FieldName = 'CAD_GRUPOCOM'
      Size = 3
    end
    object CDSImportaCadastroTNTCAD_QUANTIDADE: TFloatField
      FieldName = 'CAD_QUANTIDADE'
    end
    object CDSImportaCadastroTNTCAD_PRECO: TFloatField
      FieldName = 'CAD_PRECO'
    end
    object CDSImportaCadastroTNTCAD_UN: TWideStringField
      FieldName = 'CAD_UN'
      Size = 5
    end
  end
  object DsImportaCadastroTNT: TDataSetProvider
    DataSet = QryCadastroTNT
    Options = [poAllowCommandText]
    UpdateMode = upWhereKeyOnly
    Left = 344
    Top = 181
  end
  object QGeraArqFinalTNT: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      
        'SELECT C.CAD_NUMLOJA, C.CAD_DEPOSITO, C.CAD_DATA, C.CAD_NUMINV, ' +
        'C.CAD_REFINV, C.CAD_EXERC,'
      
        'C.CAD_DOCINV, C.CAD_NUMITEM, C.CAD_DIGCODINT, C.CAD_CODINT, C.CA' +
        'D_TIPOESTOQUE, C.CAD_LOTE, C.CAD_EAN,'
      
        'C.CAD_DESC, C.CAD_UN, C.CAD_GRUPOCOM, C.CAD_QUANTIDADE, C.CAD_PR' +
        'ECO,D.ARE_AREA, SUM(D.CON_QUANTIDADE) as QTY'
      
        'FROM Contagens d right JOIN Cadastro c ON (D.CON_EAN = C.CAD_EAN' +
        ')'
      'WHERE CON_STATUS = '#39'.'#39' or CON_STATUS IS NULL'
      
        'GROUP BY  C.CAD_NUMLOJA, C.CAD_DEPOSITO, C.CAD_DATA, C.CAD_NUMIN' +
        'V, C.CAD_REFINV, C.CAD_EXERC,'
      
        'C.CAD_DOCINV, C.CAD_NUMITEM, C.CAD_DIGCODINT, C.CAD_CODINT, C.CA' +
        'D_TIPOESTOQUE, C.CAD_LOTE, C.CAD_EAN,'
      
        'C.CAD_DESC, C.CAD_UN, C.CAD_GRUPOCOM, C.CAD_QUANTIDADE, C.CAD_PR' +
        'ECO,D.ARE_AREA'
      
        'ORDER BY C.CAD_NUMLOJA, C.CAD_DEPOSITO, C.CAD_DATA, C.CAD_NUMINV' +
        ', C.CAD_REFINV, C.CAD_EXERC,'
      
        'C.CAD_DOCINV, C.CAD_NUMITEM, C.CAD_DIGCODINT, C.CAD_CODINT, C.CA' +
        'D_TIPOESTOQUE, C.CAD_LOTE, C.CAD_EAN,'
      
        'C.CAD_DESC, C.CAD_UN, C.CAD_GRUPOCOM, C.CAD_QUANTIDADE, C.CAD_PR' +
        'ECO,D.ARE_AREA'
      ''
      '')
    Left = 456
    Top = 181
    object QGeraArqFinalTNTCAD_NUMLOJA: TIBStringField
      FieldName = 'CAD_NUMLOJA'
      Origin = 'CADASTRO.CAD_NUMLOJA'
      Size = 4
    end
    object QGeraArqFinalTNTCAD_DEPOSITO: TIBStringField
      FieldName = 'CAD_DEPOSITO'
      Origin = 'CADASTRO.CAD_DEPOSITO'
      Size = 25
    end
    object QGeraArqFinalTNTCAD_DATA: TIBStringField
      FieldName = 'CAD_DATA'
      Origin = 'CADASTRO.CAD_DATA'
      Size = 8
    end
    object QGeraArqFinalTNTCAD_NUMINV: TIBStringField
      FieldName = 'CAD_NUMINV'
      Origin = 'CADASTRO.CAD_NUMINV'
    end
    object QGeraArqFinalTNTCAD_REFINV: TIBStringField
      FieldName = 'CAD_REFINV'
      Origin = 'CADASTRO.CAD_REFINV'
    end
    object QGeraArqFinalTNTCAD_EXERC: TIBStringField
      FieldName = 'CAD_EXERC'
      Origin = 'CADASTRO.CAD_EXERC'
      Size = 4
    end
    object QGeraArqFinalTNTCAD_DOCINV: TIBStringField
      FieldName = 'CAD_DOCINV'
      Origin = 'CADASTRO.CAD_DOCINV'
      Size = 10
    end
    object QGeraArqFinalTNTCAD_NUMITEM: TIBStringField
      FieldName = 'CAD_NUMITEM'
      Origin = 'CADASTRO.CAD_NUMITEM'
      Size = 5
    end
    object QGeraArqFinalTNTCAD_DIGCODINT: TIBStringField
      FieldName = 'CAD_DIGCODINT'
      Origin = 'CADASTRO.CAD_DIGCODINT'
      Size = 5
    end
    object QGeraArqFinalTNTCAD_CODINT: TIBStringField
      FieldName = 'CAD_CODINT'
      Origin = 'CADASTRO.CAD_CODINT'
    end
    object QGeraArqFinalTNTCAD_TIPOESTOQUE: TIBStringField
      FieldName = 'CAD_TIPOESTOQUE'
      Origin = 'CADASTRO.CAD_TIPOESTOQUE'
      Size = 3
    end
    object QGeraArqFinalTNTCAD_LOTE: TIBStringField
      FieldName = 'CAD_LOTE'
      Origin = 'CADASTRO.CAD_LOTE'
      Size = 25
    end
    object QGeraArqFinalTNTCAD_EAN: TIBStringField
      FieldName = 'CAD_EAN'
      Origin = 'CADASTRO.CAD_EAN'
      Size = 25
    end
    object QGeraArqFinalTNTCAD_DESC: TIBStringField
      FieldName = 'CAD_DESC'
      Origin = 'CADASTRO.CAD_DESC'
      Size = 100
    end
    object QGeraArqFinalTNTCAD_GRUPOCOM: TIBStringField
      FieldName = 'CAD_GRUPOCOM'
      Origin = 'CADASTRO.CAD_GRUPOCOM'
      Size = 3
    end
    object QGeraArqFinalTNTCAD_QUANTIDADE: TFloatField
      FieldName = 'CAD_QUANTIDADE'
      Origin = 'CADASTRO.CAD_QUANTIDADE'
    end
    object QGeraArqFinalTNTCAD_PRECO: TFloatField
      FieldName = 'CAD_PRECO'
      Origin = 'CADASTRO.CAD_PRECO'
    end
    object QGeraArqFinalTNTARE_AREA: TIBStringField
      FieldName = 'ARE_AREA'
      Origin = 'CONTAGENS.ARE_AREA'
      Size = 25
    end
    object QGeraArqFinalTNTQTY: TFloatField
      FieldName = 'QTY'
      ProviderFlags = []
    end
    object QGeraArqFinalTNTCAD_UN: TIBStringField
      FieldName = 'CAD_UN'
      Origin = 'CADASTRO.CAD_UN'
      Size = 5
    end
  end
  object QEventoCont: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select HOR_TOTALHORA from horario where (event = '#39'CONTAGEM'#39')')
    Left = 176
    Top = 8
    object QEventoContHOR_TOTALHORA: TIBStringField
      FieldName = 'HOR_TOTALHORA'
      Origin = 'HORARIO.HOR_TOTALHORA'
      Size = 10
    end
  end
  object QEventoAudit: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select HOR_TOTALHORA from horario where (event = '#39'AUDITORIA'#39')')
    Left = 256
    Top = 8
    object QEventoAuditHOR_TOTALHORA: TIBStringField
      FieldName = 'HOR_TOTALHORA'
      Origin = 'HORARIO.HOR_TOTALHORA'
      Size = 10
    end
  end
  object QEventoDiv: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select HOR_TOTALHORA from horario where (event = '#39'DIVERGENCIA'#39')')
    Left = 336
    Top = 8
    object QEventoDivHOR_TOTALHORA: TIBStringField
      FieldName = 'HOR_TOTALHORA'
      Origin = 'HORARIO.HOR_TOTALHORA'
      Size = 10
    end
  end
  object QEventoHI: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select HOR_TOTALHORA from horario where (event = '#39'H.I.'#39')')
    Left = 400
    Top = 8
  end
  object QEventoRef: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select HOR_TOTALHORA from horario where (event = '#39'REFEICAO'#39')')
    Left = 469
    Top = 8
    object QEventoRefHOR_TOTALHORA: TIBStringField
      FieldName = 'HOR_TOTALHORA'
      Origin = 'HORARIO.HOR_TOTALHORA'
      Size = 10
    end
  end
  object QDivDesc1: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      
        'SELECT CO.QTD, CA.CAD_CODINT, CA.CAD_DESC, CA.CAD_PRECO, CA.CAD_' +
        'QUANTIDADE, CO.CON_EAN, CO.SEC_SECAO,'
      ''
      '        (CASE'
      '         when ((CO.QTD - CA.CAD_QUANTIDADE) < 1) then'
      '              ((CO.QTD - CA.CAD_QUANTIDADE) * -1)'
      '         else'
      '             (CO.QTD - CA.CAD_QUANTIDADE) end) as DIF_QTD,'
      ''
      '        (CASE'
      
        '         when ((CA.CAD_PRECO * (CO.QTD - CA.CAD_QUANTIDADE)) < 1' +
        ') then'
      
        '              ((CA.CAD_PRECO * (CO.QTD - CA.CAD_QUANTIDADE)) * -' +
        '1)'
      '         else'
      
        '             (CA.CAD_PRECO * (CO.QTD - CA.CAD_QUANTIDADE)) end) ' +
        'as DIF_VALOR,'
      '             '
      '        (CASE'
      
        '         when ((CA.CAD_PRECO * (CO.QTD - CA.CAD_QUANTIDADE)) is ' +
        'not null) then'
      
        '              ((CA.CAD_PRECO * (CO.QTD - CA.CAD_QUANTIDADE)) * 1' +
        ')'
      '         else'
      
        '             (CA.CAD_PRECO * (CO.QTD - CA.CAD_QUANTIDADE) * 1) e' +
        'nd) as DIF_VALOR2'
      '         '
      '         '
      '  from CONTAGENS_CONSO_GERAL CO'
      '  INNER join CADASTRO_CONSO CA on (CA.CAD_EAN = CO.CON_EAN)'
      ''
      ''
      '   where (CO.QTD >= :DIF_QTD)'
      ''
      '  ORDER BY DIF_VALOR DESC, CA.CAD_DESC'
      '')
    Left = 464
    Top = 256
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'DIF_QTD'
        ParamType = ptUnknown
      end>
    object QDivDesc1QTD: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'QTD'
      Origin = 'CONTAGENS_CONSO_GERAL.QTD'
      ProviderFlags = []
      ReadOnly = True
      DisplayFormat = '##,###0.000'
    end
    object QDivDesc1CAD_PRECO: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'CAD_PRECO'
      Origin = 'CADASTRO_CONSO.CAD_PRECO'
      ProviderFlags = []
      ReadOnly = True
      DisplayFormat = 'R$ ##,##0.000'
    end
    object QDivDesc1CAD_QUANTIDADE: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'CAD_QUANTIDADE'
      Origin = 'CADASTRO_CONSO.CAD_QUANTIDADE'
      ProviderFlags = []
      ReadOnly = True
      DisplayFormat = '##,###0.000'
    end
    object QDivDesc1DIF_QTD: TFloatField
      FieldName = 'DIF_QTD'
      ProviderFlags = []
      DisplayFormat = '##,###0.000'
    end
    object QDivDesc1DIF_VALOR: TFloatField
      FieldName = 'DIF_VALOR'
      ProviderFlags = []
      DisplayFormat = 'R$ ##,##0.000'
    end
    object QDivDesc1DIF_VALOR2: TFloatField
      FieldName = 'DIF_VALOR2'
      ProviderFlags = []
      DisplayFormat = 'R$ ##,##0.000'
    end
    object QDivDesc1CAD_DESC: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'CAD_DESC'
      Origin = 'CADASTRO_CONSO.CAD_DESC'
      ProviderFlags = []
      ReadOnly = True
      Size = 100
    end
    object QDivDesc1SEC_SECAO: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'SEC_SECAO'
      Origin = 'CONTAGENS_CONSO_GERAL.SEC_SECAO'
      ProviderFlags = []
      ReadOnly = True
      Size = 25
    end
    object QDivDesc1CAD_CODINT: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'CAD_CODINT'
      Origin = 'CADASTRO_CONSO.CAD_CODINT'
      ProviderFlags = []
      ReadOnly = True
    end
    object QDivDesc1CON_EAN: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'CON_EAN'
      Origin = 'CONTAGENS_CONSO_GERAL.CON_EAN'
      ProviderFlags = []
      ReadOnly = True
      Size = 25
    end
  end
  object QDivDesc2: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      
        'select CAD_CODINT, SEC_SECAO, SUM(CON_QUANTIDADE), CAD_EAN_ORIG ' +
        'AS CON_EAN'
      '    from contagens'
      '      LEFT JOIN CADASTRO_CONSO ON (CAD_EAN = CON_EAN)'
      '      where CON_STATUS =  '#39'.'#39' or CON_STATUS is Null'
      '          group by SEC_SECAO, CAD_EAN_ORIG, CAD_CODINT')
    Left = 536
    Top = 256
    object QDivDesc2SEC_SECAO: TIBStringField
      FieldName = 'SEC_SECAO'
      Origin = 'CONTAGENS.SEC_SECAO'
      Size = 4
    end
    object QDivDesc2SUM: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'SUM'
      ProviderFlags = []
      DisplayFormat = '##,###0.000'
    end
    object QDivDesc2CAD_CODINT: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'CAD_CODINT'
      Origin = 'CADASTRO_CONSO.CAD_CODINT'
      ProviderFlags = []
      ReadOnly = True
    end
    object QDivDesc2CON_EAN: TIBStringField
      FieldName = 'CON_EAN'
      ProviderFlags = []
      Size = 25
    end
  end
  object RvDivDesc12: TRvDataSetConnection
    RuntimeVisibility = rtDeveloper
    DataSet = QDivDesc1
    Left = 464
    Top = 256
  end
  object RvDivDesc22: TRvDataSetConnection
    RuntimeVisibility = rtDeveloper
    DataSet = QDivDesc2
    Left = 536
    Top = 256
  end
  object QDivZerados: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      
        'SELECT CO.QTD, CA.CAD_CODINT,  CA.CAD_DESC, CA.CAD_QUANTIDADE, C' +
        'A.CAD_SETOR,(CA.CAD_PRECO), CA.CAD_EAN, CA.CAD_UN'
      
        'FROM CONTAGENS_CONSO CO RIGHT join CADASTRO_CONSO CA on (CA.CAD_' +
        'CODINT = CO.CAD_CODINT)'
      
        'where (CA.CAD_QUANTIDADE  >= :CAD_QUANTIDADE) and (CO.QTD IS NUL' +
        'L) and (ca.cad_quantidade > 0)'
      
        'ORDER BY CA.CAD_PRECO DESC, CA.CAD_SETOR, CA.CAD_CODINT DESC, CA' +
        '.CAD_UN')
    Left = 616
    Top = 256
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'CAD_QUANTIDADE'
        ParamType = ptUnknown
      end>
    object QDivZeradosQTD: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'QTD'
      Origin = 'CONTAGENS_DIV.QTD'
      ReadOnly = True
      DisplayFormat = '##,###0.000'
    end
    object QDivZeradosCAD_CODINT: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'CAD_CODINT'
      Origin = 'CADASTRO_CONSO.CAD_CODINT'
      ReadOnly = True
    end
    object QDivZeradosCAD_QUANTIDADE: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'CAD_QUANTIDADE'
      Origin = 'CADASTRO_CONSO.CAD_QUANTIDADE'
      ReadOnly = True
      DisplayFormat = '##,###0.000'
    end
    object QDivZeradosCAD_SETOR: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'CAD_SETOR'
      Origin = 'CADASTRO_CONSO.CAD_SETOR'
      ReadOnly = True
      Size = 5
    end
    object QDivZeradosCAD_PRECO: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'CAD_PRECO'
      Origin = 'CADASTRO_CONSO.CAD_PRECO'
      ReadOnly = True
      DisplayFormat = 'R$ ##,##0.00'
    end
    object QDivZeradosCAD_EAN: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'CAD_EAN'
      Origin = 'CADASTRO_CONSO.CAD_EAN'
      ReadOnly = True
      Size = 25
    end
    object QDivZeradosCAD_UN: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'CAD_UN'
      Origin = 'CADASTRO_CONSO.CAD_UN'
      ProviderFlags = []
      ReadOnly = True
      Size = 5
    end
    object QDivZeradosCAD_DESC: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'CAD_DESC'
      Origin = 'CADASTRO_CONSO.CAD_DESC'
      ProviderFlags = []
      ReadOnly = True
      Size = 100
    end
  end
  object RvDivZerados2: TRvDataSetConnection
    RuntimeVisibility = rtDeveloper
    DataSet = QDivZerados
    Left = 616
    Top = 256
  end
  object QDiv1: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT * FROM DIVERGENCIA'
      ' WHERE ((DIF_QTD * 1) BETWEEN :VALOR and :VALORFIM) OR'
      '       ((DIF_QTD * -1) BETWEEN :VALOR and :VALORFIM)')
    Left = 440
    Top = 312
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'VALOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'VALORFIM'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'VALOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'VALORFIM'
        ParamType = ptUnknown
      end>
    object QDiv1CAD_CODINT: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'CAD_CODINT'
      Origin = 'DIVERGENCIA.CAD_CODINT'
      ProviderFlags = []
      ReadOnly = True
      Size = 32
    end
    object QDiv1QTD: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'QTD'
      Origin = 'DIVERGENCIA.QTD'
      ProviderFlags = []
      ReadOnly = True
      DisplayFormat = '##,###0.000'
    end
    object QDiv1CAD_SETOR: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'CAD_SETOR'
      Origin = 'DIVERGENCIA.CAD_SETOR'
      ProviderFlags = []
      ReadOnly = True
    end
    object QDiv1CAD_SETDESC: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'CAD_SETDESC'
      Origin = 'DIVERGENCIA.CAD_SETDESC'
      ProviderFlags = []
      ReadOnly = True
      Size = 100
    end
    object QDiv1CAD_UN: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'CAD_UN'
      Origin = 'DIVERGENCIA.CAD_UN'
      ProviderFlags = []
      ReadOnly = True
      Size = 5
    end
    object QDiv1CAD_DESC: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'CAD_DESC'
      Origin = 'DIVERGENCIA.CAD_DESC'
      ProviderFlags = []
      ReadOnly = True
      Size = 100
    end
    object QDiv1CAD_PRECO: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'CAD_PRECO'
      Origin = 'DIVERGENCIA.CAD_PRECO'
      ProviderFlags = []
      ReadOnly = True
      DisplayFormat = 'R$ ##,###0.00'
    end
    object QDiv1CAD_QUANTIDADE: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'CAD_QUANTIDADE'
      Origin = 'DIVERGENCIA.CAD_QUANTIDADE'
      ProviderFlags = []
      ReadOnly = True
      DisplayFormat = '##,###0.000'
    end
    object QDiv1DIF_QTD: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'DIF_QTD'
      Origin = 'DIVERGENCIA.DIF_QTD'
      ProviderFlags = []
      ReadOnly = True
      DisplayFormat = '##,###0.000'
    end
    object QDiv1DIF_VALOR2: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'DIF_VALOR2'
      Origin = 'DIVERGENCIA.DIF_VALOR2'
      ProviderFlags = []
      ReadOnly = True
      DisplayFormat = 'R$ ##,###0.00'
    end
    object QDiv1NUM_LOJA: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'NUM_LOJA'
      Origin = 'DIVERGENCIA.NUM_LOJA'
      ProviderFlags = []
      ReadOnly = True
      Size = 10
    end
    object QDiv1DIF_VALOR3: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'DIF_VALOR3'
      Origin = 'DIVERGENCIA.DIF_VALOR3'
      ProviderFlags = []
      ReadOnly = True
      DisplayFormat = 'R$ ##,###0.00'
    end
  end
  object RvDiv12: TRvDataSetConnection
    RuntimeVisibility = rtDeveloper
    DataSet = QDiv1
    Left = 440
    Top = 312
  end
  object RvDiv22: TRvDataSetConnection
    RuntimeVisibility = rtDeveloper
    DataSet = QDiv2
    Left = 512
    Top = 309
  end
  object QDiv2: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      ' select'
      '       (CASE'
      '         when (SUM(CON_QUANTIDADE) = 0) THEN 0'
      '          else SUM(CON_QUANTIDADE) end) as QTD,'
      '       (select CAD_CODINT'
      '             FROM cadastro'
      '              WHERE (CAD_EAN = CON_EAN)'
      '              GROUP by CAD_CODINT) AS CAD_CODINT,'
      '       (select CAD_UN'
      '             FROM cadastro'
      '              WHERE (CAD_EAN = CON_EAN)'
      '              GROUP by CAD_UN) AS CAD_UN,'
      '            SEC_SECAO,'
      '            CON_ENDERECO,'
      '            CON_EAN'
      '  from contagens'
      '        WHERE(CON_STATUS =  '#39'.'#39')'
      '         or  (CON_STATUS is Null)'
      '      group by 2,3,4,5,6')
    Left = 512
    Top = 309
    object QDiv2QTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
      DisplayFormat = '##,###0.000'
    end
    object QDiv2CAD_CODINT: TIBStringField
      FieldName = 'CAD_CODINT'
      ProviderFlags = []
      Size = 35
    end
    object QDiv2CAD_UN: TIBStringField
      FieldName = 'CAD_UN'
      ProviderFlags = []
      Size = 5
    end
    object QDiv2SEC_SECAO: TIBStringField
      FieldName = 'SEC_SECAO'
      Origin = 'CONTAGENS.SEC_SECAO'
      Size = 25
    end
    object QDiv2CON_ENDERECO: TIBStringField
      FieldName = 'CON_ENDERECO'
      Origin = 'CONTAGENS.CON_ENDERECO'
      Size = 25
    end
    object QDiv2CON_EAN: TIBStringField
      FieldName = 'CON_EAN'
      Origin = 'CONTAGENS.CON_EAN'
      Size = 35
    end
  end
  object QDivEndereco: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      
        'SELECT CO.QTD, C.CON_EAN, C.SEC_SECAO, CA.CAD_UN, CA.CAD_DESC, C' +
        '.ARE_AREA, CA.CAD_PRECO, CA.CAD_QUANTIDADE,'
      ''
      '        (CASE'
      '         when ((CO.QTD - CA.CAD_QUANTIDADE) < 1) then'
      '              ((CO.QTD - CA.CAD_QUANTIDADE) * -1)'
      '         else'
      '             (CO.QTD - CA.CAD_QUANTIDADE) end) as DIF_QTD'
      '         '
      '  from CONTAGENS_DIV CO'
      
        '  INNER join CADASTRO_CONSO CA ON (CA.CAD_CODINT = CO.CAD_CODINT' +
        ') INNER JOIN CONTAGENS_CONSO C ON (CO.CAD_CODINT = C.CAD_CODINT)'
      ''
      
        '         WHERE (ca.cad_quantidade >= :dif_qtd) and (ca.cad_quant' +
        'idade <= :dif_qtdfinal)  AND (CO.QTD <> CA.CAD_QUANTIDADE)'
      ''
      
        '  GROUP BY C.SEC_SECAO,CO.QTD, CA.CAD_DESC, CA.CAD_PRECO, CA.CAD' +
        '_QUANTIDADE,CA.CAD_UN, C.ARE_AREA, C.CON_EAN'
      '  ORDER BY  C.SEC_SECAO, C.ARE_AREA')
    Left = 756
    Top = 255
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'dif_qtd'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'dif_qtdfinal'
        ParamType = ptUnknown
      end>
    object QDivEnderecoSEC_SECAO: TIBStringField
      FieldName = 'SEC_SECAO'
      Origin = 'CONTAGENS.SEC_SECAO'
      Size = 25
    end
    object QDivEnderecoQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
      DisplayFormat = '##,###0.000'
    end
    object QDivEnderecoCAD_UN: TIBStringField
      DisplayWidth = 5
      FieldKind = fkInternalCalc
      FieldName = 'CAD_UN'
      Origin = 'CADASTRO_CONSO.CAD_UN'
      ProviderFlags = []
      ReadOnly = True
      Size = 5
    end
    object QDivEnderecoCAD_PRECO: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'CAD_PRECO'
      Origin = 'CADASTRO_CONSO.CAD_PRECO'
      ProviderFlags = []
      ReadOnly = True
      DisplayFormat = 'R$ ##,##0.00'
    end
    object QDivEnderecoARE_AREA: TIBStringField
      FieldName = 'ARE_AREA'
      Origin = 'CONTAGENS.ARE_AREA'
      Size = 25
    end
    object QDivEnderecoCAD_QUANTIDADE: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'CAD_QUANTIDADE'
      Origin = 'CADASTRO_CONSO.CAD_QUANTIDADE'
      ProviderFlags = []
      ReadOnly = True
      DisplayFormat = '##,###0.000'
    end
    object QDivEnderecoDIF_QTD: TFloatField
      FieldName = 'DIF_QTD'
      ProviderFlags = []
      DisplayFormat = '##,###0.000'
    end
    object QDivEnderecoCON_EAN: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'CON_EAN'
      Origin = 'CONTAGENS_CONSO.CON_EAN'
      ProviderFlags = []
      ReadOnly = True
      Size = 25
    end
    object QDivEnderecoCAD_DESC: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'CAD_DESC'
      Origin = 'CADASTRO_CONSO.CAD_DESC'
      ProviderFlags = []
      ReadOnly = True
      Size = 100
    end
  end
  object RvDivEndereco2: TRvDataSetConnection
    RuntimeVisibility = rtDeveloper
    DataSet = QDivEndereco
    Left = 756
    Top = 255
  end
  object QDiv3: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      
        'SELECT CA.CAD_CODINT, SUM(CO.QTD) as qtd1, CA.CAD_SETOR,CA.CAD_U' +
        'N,CA.CAD_DESC, CA.CAD_PRECO, CA.CAD_QUANTIDADE,'
      ''
      '        (CASE'
      '         when ((CO.QTD - CA.CAD_QUANTIDADE) < 1) then'
      '              ((CO.QTD - CA.CAD_QUANTIDADE) * -1)'
      '         else'
      '             (CO.QTD - CA.CAD_QUANTIDADE) end) as DIF_QTD,'
      ''
      '        (CASE'
      
        '         when ((CA.CAD_PRECO * (CO.QTD - CA.CAD_QUANTIDADE)) < 1' +
        ') then'
      
        '              ((CA.CAD_PRECO * (CO.QTD - CA.CAD_QUANTIDADE)) * -' +
        '1)'
      '         else'
      
        '             (CA.CAD_PRECO * (CO.QTD - CA.CAD_QUANTIDADE)) end) ' +
        'as DIF_VALOR,'
      '             '
      '        (CASE'
      
        '         when ((CA.CAD_PRECO * (CO.QTD - CA.CAD_QUANTIDADE)) is ' +
        'not null) then'
      
        '              ((CA.CAD_PRECO * (CO.QTD - CA.CAD_QUANTIDADE)) * 1' +
        ')'
      '         else'
      
        '             (CA.CAD_PRECO * (CO.QTD - CA.CAD_QUANTIDADE) * 1) e' +
        'nd) as DIF_VALOR2'
      '         '
      '   from CONTAGENS_DIV CO'
      
        '  INNER JOIN CADASTRO_CONSO CA on (CA.CAD_CODINT = CO.CAD_CODINT' +
        ') '
      ''
      '  where (CO.QTD >= :DIF_QTD)'
      
        '  GROUP BY CA.CAD_CODINT,  CO.QTD, CA.CAD_DESC, CA.CAD_PRECO, CA' +
        '.CAD_QUANTIDADE,CA.CAD_SETOR,CA.CAD_UN'
      '  ORDER BY CA.CAD_CODINT,  DIF_VALOR DESC, CA.CAD_PRECO DESC'
      ''
      '')
    Left = 684
    Top = 256
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'DIF_QTD'
        ParamType = ptUnknown
      end>
    object QDiv3QTD1: TFloatField
      FieldName = 'QTD1'
      ProviderFlags = []
    end
    object QDiv3CAD_SETOR: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'CAD_SETOR'
      Origin = 'CADASTRO_CONSO.CAD_SETOR'
      ProviderFlags = []
      ReadOnly = True
      Size = 5
    end
    object QDiv3CAD_UN: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'CAD_UN'
      Origin = 'CADASTRO_CONSO.CAD_UN'
      ProviderFlags = []
      ReadOnly = True
      Size = 2
    end
    object QDiv3CAD_PRECO: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'CAD_PRECO'
      Origin = 'CADASTRO_CONSO.CAD_PRECO'
      ProviderFlags = []
      ReadOnly = True
    end
    object QDiv3CAD_QUANTIDADE: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'CAD_QUANTIDADE'
      Origin = 'CADASTRO_CONSO.CAD_QUANTIDADE'
      ProviderFlags = []
      ReadOnly = True
    end
    object QDiv3DIF_QTD: TFloatField
      FieldName = 'DIF_QTD'
      ProviderFlags = []
    end
    object QDiv3DIF_VALOR: TFloatField
      FieldName = 'DIF_VALOR'
      ProviderFlags = []
    end
    object QDiv3DIF_VALOR2: TFloatField
      FieldName = 'DIF_VALOR2'
      ProviderFlags = []
    end
    object QDiv3CAD_DESC: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'CAD_DESC'
      Origin = 'CADASTRO_CONSO.CAD_DESC'
      ProviderFlags = []
      ReadOnly = True
      Size = 100
    end
    object QDiv3CAD_CODINT: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'CAD_CODINT'
      Origin = 'CADASTRO_CONSO.CAD_CODINT'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object RvDiv32: TRvDataSetConnection
    RuntimeVisibility = rtDeveloper
    DataSet = QDiv3
    Left = 684
    Top = 256
  end
  object QRelatorioAuditForcada: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select h.ros_nome, h.ros_matricula, s.sec_secao, c.are_area'
      
        'from  secao s left join horario h left join contagens c on (h.ro' +
        's_matricula = s.hor_matricula)'
      'on (s.sec_secao = c.sec_secao)'
      '')
    Left = 968
    Top = 312
    object QRelatorioAuditForcadaROS_NOME: TIBStringField
      FieldName = 'ROS_NOME'
      Origin = 'HORARIO.ROS_NOME'
      Size = 50
    end
    object QRelatorioAuditForcadaROS_MATRICULA: TIBStringField
      FieldName = 'ROS_MATRICULA'
      Origin = 'HORARIO.ROS_MATRICULA'
      Size = 11
    end
    object QRelatorioAuditForcadaSEC_SECAO: TIBStringField
      FieldName = 'SEC_SECAO'
      Origin = 'SECAO.SEC_SECAO'
      Size = 25
    end
    object QRelatorioAuditForcadaARE_AREA: TIBStringField
      FieldName = 'ARE_AREA'
      Origin = 'CONTAGENS.ARE_AREA'
      Size = 25
    end
  end
  object QRelatorioAuditForcada2: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      
        'select co.con_ean,co.qtd, co.sec_secao, ca.cad_desc, ca.cad_codi' +
        'nt,  co.preco,  co.are_area, ca.cad_un'
      'from contagens_conso co'
      
        'left join cadastro_conso ca on (ca.cad_ean = co.con_ean) where  ' +
        '(co.preco >= :preco) '
      'order by co.preco desc')
    Left = 48
    Top = 308
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'preco'
        ParamType = ptUnknown
      end>
    object QRelatorioAuditForcada2CON_EAN: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'CON_EAN'
      Origin = 'CONTAGENS_CONSO.CON_EAN'
      ProviderFlags = []
      ReadOnly = True
      Size = 25
    end
    object QRelatorioAuditForcada2QTD: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'QTD'
      Origin = 'CONTAGENS_CONSO.QTD'
      ProviderFlags = []
      ReadOnly = True
      DisplayFormat = '##,###0.000'
    end
    object QRelatorioAuditForcada2SEC_SECAO: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'SEC_SECAO'
      Origin = 'CONTAGENS_CONSO.SEC_SECAO'
      ProviderFlags = []
      ReadOnly = True
      Size = 25
    end
    object QRelatorioAuditForcada2CAD_CODINT: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'CAD_CODINT'
      Origin = 'CADASTRO_CONSO.CAD_CODINT'
      ProviderFlags = []
      ReadOnly = True
    end
    object QRelatorioAuditForcada2PRECO: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'PRECO'
      Origin = 'CONTAGENS_CONSO.PRECO'
      ProviderFlags = []
      ReadOnly = True
      DisplayFormat = 'R$ ##,###0.00'
    end
    object QRelatorioAuditForcada2ARE_AREA: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'ARE_AREA'
      Origin = 'CONTAGENS_CONSO.ARE_AREA'
      ProviderFlags = []
      ReadOnly = True
      Size = 25
    end
    object QRelatorioAuditForcada2CAD_UN: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'CAD_UN'
      Origin = 'CADASTRO_CONSO.CAD_UN'
      ProviderFlags = []
      ReadOnly = True
      Size = 2
    end
    object QRelatorioAuditForcada2CAD_DESC: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'CAD_DESC'
      Origin = 'CADASTRO_CONSO.CAD_DESC'
      ProviderFlags = []
      ReadOnly = True
      Size = 100
    end
  end
  object RvRelatorioAuditForcada: TRvDataSetConnection
    RuntimeVisibility = rtDeveloper
    DataSet = QRelatorioAuditForcada
    Left = 872
    Top = 256
  end
  object RvRelatorioAuditForcada2: TRvDataSetConnection
    RuntimeVisibility = rtDeveloper
    DataSet = QRelatorioAuditForcada2
    Left = 968
    Top = 312
  end
  object DsCadastroDIA: TDataSource
    DataSet = CDSImportaCadastroDIA
    Left = 544
    Top = 181
  end
  object CDSImportaCadastroDIA: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DsImportaCadastroDIA'
    Left = 648
    Top = 181
    object CDSImportaCadastroDIACAD_ID: TIntegerField
      FieldName = 'CAD_ID'
      Required = True
    end
    object CDSImportaCadastroDIACAD_CODINT: TStringField
      FieldName = 'CAD_CODINT'
    end
    object CDSImportaCadastroDIACAD_DESC: TStringField
      FieldName = 'CAD_DESC'
      Size = 100
    end
    object CDSImportaCadastroDIACAD_EAN: TStringField
      FieldName = 'CAD_EAN'
      Size = 25
    end
    object CDSImportaCadastroDIACAD_QUANTIDADE: TFloatField
      FieldName = 'CAD_QUANTIDADE'
    end
    object CDSImportaCadastroDIACAD_PRECO: TFloatField
      FieldName = 'CAD_PRECO'
    end
    object CDSImportaCadastroDIACAD_PRECOTXT: TStringField
      FieldName = 'CAD_PRECOTXT'
      Size = 15
    end
    object CDSImportaCadastroDIACAD_EAN_ORIG: TStringField
      FieldName = 'CAD_EAN_ORIG'
      Size = 25
    end
    object CDSImportaCadastroDIACAD_SETOR: TStringField
      FieldName = 'CAD_SETOR'
      Size = 5
    end
    object CDSImportaCadastroDIACAD_SETDESC: TStringField
      FieldName = 'CAD_SETDESC'
      Size = 100
    end
  end
  object QryCadastroDIA: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT CAD_ID, CAD_CODINT, CAD_DESC, CAD_EAN, '
      
        'CAD_QUANTIDADE, CAD_PRECO, CAD_PRECOTXT, CAD_EAN_ORIG, CAD_SETOR' +
        ', CAD_SETDESC  FROM CADASTRO'
      'ORDER BY CAD_EAN, CAD_CODINT')
    Left = 760
    Top = 192
    object QryCadastroDIACAD_ID: TIntegerField
      FieldName = 'CAD_ID'
      Origin = 'CADASTRO.CAD_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object QryCadastroDIACAD_CODINT: TIBStringField
      FieldName = 'CAD_CODINT'
      Origin = 'CADASTRO.CAD_CODINT'
    end
    object QryCadastroDIACAD_DESC: TIBStringField
      FieldName = 'CAD_DESC'
      Origin = 'CADASTRO.CAD_DESC'
      Size = 100
    end
    object QryCadastroDIACAD_EAN: TIBStringField
      FieldName = 'CAD_EAN'
      Origin = 'CADASTRO.CAD_EAN'
      Size = 25
    end
    object QryCadastroDIACAD_QUANTIDADE: TFloatField
      FieldName = 'CAD_QUANTIDADE'
      Origin = 'CADASTRO.CAD_QUANTIDADE'
    end
    object QryCadastroDIACAD_PRECO: TFloatField
      FieldName = 'CAD_PRECO'
      Origin = 'CADASTRO.CAD_PRECO'
    end
    object QryCadastroDIACAD_PRECOTXT: TIBStringField
      FieldName = 'CAD_PRECOTXT'
      Origin = 'CADASTRO.CAD_PRECOTXT'
      Size = 15
    end
    object QryCadastroDIACAD_EAN_ORIG: TIBStringField
      FieldName = 'CAD_EAN_ORIG'
      Origin = 'CADASTRO.CAD_EAN_ORIG'
      Size = 25
    end
    object QryCadastroDIACAD_SETOR: TIBStringField
      FieldName = 'CAD_SETOR'
      Origin = 'CADASTRO.CAD_SETOR'
      Size = 5
    end
    object QryCadastroDIACAD_SETDESC: TIBStringField
      FieldName = 'CAD_SETDESC'
      Origin = 'CADASTRO.CAD_SETDESC'
      Size = 100
    end
  end
  object DsImportaCadastroDIA: TDataSetProvider
    DataSet = QryCadastroDIA
    Options = [poAllowCommandText]
    UpdateMode = upWhereKeyOnly
    Left = 856
    Top = 192
  end
  object QGeraArqFinalDIA: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT C.CAD_EAN, SUM(D.CON_QUANTIDADE) as QTY'
      'FROM Contagens d left JOIN Cadastro c ON (D.CON_EAN = C.CAD_EAN)'
      'WHERE CON_STATUS = '#39'.'#39' or CON_STATUS IS NULL'
      'GROUP BY C.CAD_EAN'
      'ORDER BY C.CAD_EAN')
    Left = 960
    Top = 192
    object QGeraArqFinalDIACAD_EAN: TIBStringField
      FieldName = 'CAD_EAN'
      Origin = 'CADASTRO.CAD_EAN'
      Size = 25
    end
    object QGeraArqFinalDIAQTY: TFloatField
      FieldName = 'QTY'
      ProviderFlags = []
    end
  end
  object QEventoMap: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      
        'select HOR_TOTALHORA from horario where (event = '#39'MAPEANDO/ EQUI' +
        'PAMENT'#39')')
    Left = 541
    Top = 8
    object QEventoMapHOR_TOTALHORA: TIBStringField
      FieldName = 'HOR_TOTALHORA'
      Origin = 'HORARIO.HOR_TOTALHORA'
      Size = 10
    end
  end
  object QGeraArqFinalFRN: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      
        'select D.CON_EAN, C.CAD_CODINT, C.CAD_DESC, C.CAD_DEPOSITO, SUM(' +
        'D.CON_QUANTIDADE) AS QTD, SUM(C.CAD_PRECOTXT) AS PRECO'
      'from CONTAGENS D'
      'inner JOIN CADASTRO C ON (C.CAD_EAN = D.CON_EAN)'
      '  where CON_STATUS =  '#39'.'#39' or CON_STATUS is Null'
      '  GROUP BY 1,2,3,4')
    Left = 732
    Top = 5
    object QGeraArqFinalFRNCON_EAN: TIBStringField
      FieldName = 'CON_EAN'
      Origin = 'CONTAGENS.CON_EAN'
      Size = 25
    end
    object QGeraArqFinalFRNCAD_CODINT: TIBStringField
      FieldName = 'CAD_CODINT'
      Origin = 'CADASTRO.CAD_CODINT'
    end
    object QGeraArqFinalFRNCAD_DESC: TIBStringField
      FieldName = 'CAD_DESC'
      Origin = 'CADASTRO.CAD_DESC'
      Size = 100
    end
    object QGeraArqFinalFRNCAD_DEPOSITO: TIBStringField
      FieldName = 'CAD_DEPOSITO'
      Origin = 'CADASTRO.CAD_DEPOSITO'
      Size = 25
    end
    object QGeraArqFinalFRNQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
    object QGeraArqFinalFRNPRECO: TFloatField
      FieldName = 'PRECO'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalSAF: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      
        'select D.CON_EAN, C.CAD_CODINT, C.CAD_DESC, (C.CAD_PRECO) AS PRE' +
        'CO, SUM(D.CON_QUANTIDADE) AS QTD'
      'from CONTAGENS D'
      'inner JOIN CADASTRO C ON (C.CAD_EAN = D.CON_EAN)'
      '  where CON_STATUS =  '#39'.'#39' or CON_STATUS is Null'
      '  GROUP BY 1,2,3,4')
    Left = 336
    Top = 64
    object QGeraArqFinalSAFCON_EAN: TIBStringField
      FieldName = 'CON_EAN'
      Origin = 'CONTAGENS.CON_EAN'
      Size = 25
    end
    object QGeraArqFinalSAFCAD_CODINT: TIBStringField
      FieldName = 'CAD_CODINT'
      Origin = 'CADASTRO.CAD_CODINT'
    end
    object QGeraArqFinalSAFCAD_DESC: TIBStringField
      FieldName = 'CAD_DESC'
      Origin = 'CADASTRO.CAD_DESC'
      Size = 100
    end
    object QGeraArqFinalSAFPRECO: TFloatField
      FieldName = 'PRECO'
      ProviderFlags = []
    end
    object QGeraArqFinalSAFQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalNAT: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT co.con_cadcodint as cod_int,'
      '       coalesce(sum(co.con_quantidade),0) as Qtd'
      ' FROM contagens co'
      'INNER JOIN cadastro ca'
      '   on co.con_ean = ca.cad_ean'
      'WHERE co.con_status = '#39'.'#39
      '   or co.con_status is null'
      'group by 1')
    Left = 32
    Top = 64
    object QGeraArqFinalNATCOD_INT: TIBStringField
      FieldName = 'COD_INT'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalNATQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalFUT: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select D.CON_EAN, SUM(D.CON_QUANTIDADE) AS QTD'
      'from CONTAGENS D'
      'inner JOIN CADASTRO C ON (C.CAD_EAN = D.CON_EAN)'
      '  where CON_STATUS =  '#39'.'#39' or CON_STATUS is Null'
      '  GROUP BY 1')
    Left = 136
    Top = 64
    object QGeraArqFinalFUTCON_EAN: TIBStringField
      FieldName = 'CON_EAN'
      Origin = 'CONTAGENS.CON_EAN'
      Size = 25
    end
    object QGeraArqFinalFUTQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalDVL_Dep: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select D.CON_EAN,'
      '       SUM(D.CON_QUANTIDADE) AS QTD'
      'from CONTAGENS D'
      'inner JOIN CADASTRO C ON (C.CAD_EAN = D.CON_EAN)'
      '  where (CON_STATUS =  '#39'.'#39' or CON_STATUS is Null)'
      '   and EXISTS (select 1'
      '               from mapeamento m'
      '              WHERE d.sec_secao Between m.map_secini'
      '                                    and m.map_secfin'
      '                and m.deposito = '#39'X'#39')'
      ' group by 1')
    Left = 48
    Top = 600
    object QGeraArqFinalDVL_DepCON_EAN: TIBStringField
      FieldName = 'CON_EAN'
      Origin = 'CONTAGENS.CON_EAN'
      Size = 25
    end
    object QGeraArqFinalDVL_DepQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalCAM: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      
        'select C.CAD_EAN, C.CAD_CODINT, C.CAD_PRECO, SUM(D.CON_QUANTIDAD' +
        'E) AS QTD, C.CAD_SETOR'
      'from CONTAGENS D'
      'right JOIN CADASTRO C ON (C.CAD_EAN = D.CON_EAN)'
      '  where CON_STATUS =  '#39'.'#39' or CON_STATUS is Null'
      '  GROUP BY 1,2,3,5'
      '  ORDER BY C.CAD_SETOR')
    Left = 824
    Top = 64
    object QGeraArqFinalCAMCAD_EAN: TIBStringField
      FieldName = 'CAD_EAN'
      Origin = 'CADASTRO.CAD_EAN'
      Size = 25
    end
    object QGeraArqFinalCAMCAD_CODINT: TIBStringField
      FieldName = 'CAD_CODINT'
      Origin = 'CADASTRO.CAD_CODINT'
    end
    object QGeraArqFinalCAMCAD_PRECO: TFloatField
      FieldName = 'CAD_PRECO'
      Origin = 'CADASTRO.CAD_PRECO'
    end
    object QGeraArqFinalCAMQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
    object QGeraArqFinalCAMCAD_SETOR: TIBStringField
      FieldName = 'CAD_SETOR'
      Origin = 'CADASTRO.CAD_SETOR'
    end
  end
  object QGeraArqFinalRLD: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      
        'select C.CAD_REFINV, C.CAD_NUMLOJA, C.CAD_DOCINV, C.CAD_EXERC, C' +
        '.CAD_CODINT, C.CAD_EAN, C.CAD_UN, SUM(D.CON_QUANTIDADE) AS QTD'
      'from CONTAGENS D'
      'RIGHT JOIN CADASTRO C ON (C.CAD_EAN = D.CON_EAN)'
      '  where CON_STATUS =  '#39'.'#39' or CON_STATUS is Null'
      '  GROUP BY 1,2,3,4,5,6,7')
    Left = 1168
    Top = 8
    object QGeraArqFinalRLDCAD_REFINV: TIBStringField
      FieldName = 'CAD_REFINV'
      Origin = 'CADASTRO.CAD_REFINV'
    end
    object QGeraArqFinalRLDCAD_NUMLOJA: TIBStringField
      FieldName = 'CAD_NUMLOJA'
      Origin = 'CADASTRO.CAD_NUMLOJA'
      Size = 4
    end
    object QGeraArqFinalRLDCAD_DOCINV: TIBStringField
      FieldName = 'CAD_DOCINV'
      Origin = 'CADASTRO.CAD_DOCINV'
      Size = 10
    end
    object QGeraArqFinalRLDCAD_EXERC: TIBStringField
      FieldName = 'CAD_EXERC'
      Origin = 'CADASTRO.CAD_EXERC'
      Size = 4
    end
    object QGeraArqFinalRLDCAD_CODINT: TIBStringField
      FieldName = 'CAD_CODINT'
      Origin = 'CADASTRO.CAD_CODINT'
    end
    object QGeraArqFinalRLDCAD_EAN: TIBStringField
      FieldName = 'CAD_EAN'
      Origin = 'CADASTRO.CAD_EAN'
      Size = 25
    end
    object QGeraArqFinalRLDCAD_UN: TIBStringField
      FieldName = 'CAD_UN'
      Origin = 'CADASTRO.CAD_UN'
      Size = 5
    end
    object QGeraArqFinalRLDQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalAND: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select D.CON_EAN, SUM(D.CON_QUANTIDADE) AS QTD'
      'from CONTAGENS D'
      'inner JOIN CADASTRO C ON (C.CAD_EAN = D.CON_EAN)'
      '  where CON_STATUS =  '#39'.'#39' or CON_STATUS is Null'
      '  GROUP BY 1')
    Left = 1264
    Top = 8
    object QGeraArqFinalANDCON_EAN: TIBStringField
      FieldName = 'CON_EAN'
      Origin = 'CONTAGENS.CON_EAN'
      Size = 25
    end
    object QGeraArqFinalANDQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QueryAcuracy: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select COUNT(s.sec_secao) as qtd_secao, s.sec_auditada'
      
        'from secao s left join mapeamento m on (s.sec_secao between m.ma' +
        'p_secini and m.map_secfin)'
      
        'where (s.sec_auditada = "N") and (s.sec_secao between :secini an' +
        'd :secfin)'
      'group by s.sec_auditada')
    Left = 160
    Top = 316
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'secini'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'secfin'
        ParamType = ptUnknown
      end>
    object QueryAcuracyQTD_SECAO: TIntegerField
      FieldName = 'QTD_SECAO'
      ProviderFlags = []
    end
    object QueryAcuracySEC_AUDITADA: TIBStringField
      FieldName = 'SEC_AUDITADA'
      Origin = 'SECAO.SEC_AUDITADA'
      Size = 1
    end
  end
  object QueryAcuracyAudit: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select COUNT(s.sec_secao) as qtd_secao, s.sec_auditada'
      
        'from secao s left join mapeamento m on (s.sec_secao between m.ma' +
        'p_secini and m.map_secfin)'
      
        'where (s.sec_auditada = "S") and (s.sec_secao between :secini an' +
        'd :secfin)'
      'group by s.sec_auditada')
    Left = 272
    Top = 256
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'secini'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'secfin'
        ParamType = ptUnknown
      end>
    object QueryAcuracyAuditQTD_SECAO: TIntegerField
      FieldName = 'QTD_SECAO'
      ProviderFlags = []
    end
    object QueryAcuracyAuditSEC_AUDITADA: TIBStringField
      FieldName = 'SEC_AUDITADA'
      Origin = 'SECAO.SEC_AUDITADA'
      Size = 1
    end
  end
  object RvAcuracy: TRvDataSetConnection
    RuntimeVisibility = rtDeveloper
    DataSet = QueryAcuracy
    Left = 160
    Top = 316
  end
  object r: TRvDataSetConnection
    RuntimeVisibility = rtDeveloper
    DataSet = QueryAcuracyAudit
    Left = 272
    Top = 256
  end
  object QueryAcuracyTotal: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select COUNT(s.sec_secao) as qtd_secao'
      
        'from secao s left join mapeamento m on (s.sec_secao between m.ma' +
        'p_secini and m.map_secfin)'
      
        'where (s.sec_auditada <> '#39#39') and (s.sec_secao between :secini an' +
        'd :secfin)')
    Left = 368
    Top = 256
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'secini'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'secfin'
        ParamType = ptUnknown
      end>
    object QueryAcuracyTotalQTD_SECAO: TIntegerField
      FieldName = 'QTD_SECAO'
      ProviderFlags = []
    end
  end
  object RvAcuracyTotal: TRvDataSetConnection
    RuntimeVisibility = rtDeveloper
    DataSet = QueryAcuracyTotal
    Left = 368
    Top = 256
  end
  object QGeraArqFinalCEAHEAD: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select CAD_NUMLOJA, CAD_DOCINV FROM CADASTRO'
      'GROUP BY 1,2')
    Left = 616
    Top = 64
    object QGeraArqFinalCEAHEADCAD_NUMLOJA: TIBStringField
      FieldName = 'CAD_NUMLOJA'
      Origin = 'CADASTRO.CAD_NUMLOJA'
      Size = 10
    end
    object QGeraArqFinalCEAHEADCAD_DOCINV: TIBStringField
      FieldName = 'CAD_DOCINV'
      Origin = 'CADASTRO.CAD_DOCINV'
      Size = 10
    end
  end
  object QGeraArqFinalCEA: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select D.SEC_SECAO, D.CON_EAN, SUM(D.CON_QUANTIDADE) AS QTD,'
      'D.TONALIDADE'
      'from CONTAGENS D'
      'left JOIN CADASTRO C ON (C.CAD_EAN = D.CON_EAN)'
      '  where CON_STATUS =  '#39'.'#39' or CON_STATUS is Null'
      '  GROUP BY 1,2,4')
    Left = 32
    Top = 121
    object QGeraArqFinalCEASEC_SECAO: TIBStringField
      FieldName = 'SEC_SECAO'
      Origin = 'CONTAGENS.SEC_SECAO'
      Size = 25
    end
    object QGeraArqFinalCEACON_EAN: TIBStringField
      FieldName = 'CON_EAN'
      Origin = 'CONTAGENS.CON_EAN'
      Size = 25
    end
    object QGeraArqFinalCEAQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
    object QGeraArqFinalCEATONALIDADE: TIBStringField
      FieldName = 'TONALIDADE'
      Origin = 'CONTAGENS.TONALIDADE'
    end
  end
  object QGeraArqFinalPRN: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select D.CON_EAN, SUM(D.CON_QUANTIDADE) AS QTD, C.CAD_EXERC'
      'from CONTAGENS D'
      'inner JOIN CADASTRO C ON (C.CAD_EAN = D.CON_EAN)'
      '  where CON_STATUS =  '#39'.'#39' or CON_STATUS is Null'
      '  GROUP BY 1,3')
    Left = 1133
    Top = 65
    object QGeraArqFinalPRNCON_EAN: TIBStringField
      FieldName = 'CON_EAN'
      Origin = 'CONTAGENS.CON_EAN'
      Size = 25
    end
    object QGeraArqFinalPRNQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
    object QGeraArqFinalPRNCAD_EXERC: TIBStringField
      FieldName = 'CAD_EXERC'
      Origin = 'CADASTRO.CAD_EXERC'
      Size = 4
    end
  end
  object qImportaCadCEA: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT CAD_DOCINV, '
      '               CAD_REFINV, '
      '               CAD_NUMLOJA, '
      '               CAD_SETDESC, '
      '               CAD_EAN, '
      '               CAD_EAN_ORIG,'
      '               CAD_CODINT,  '
      '               CAD_DESC, '
      '               CAD_QUANTIDADE, '
      '              CAD_LOTE'
      ' FROM CADASTRO ')
    Left = 128
    Top = 121
    object qImportaCadCEACAD_DOCINV: TIBStringField
      FieldName = 'CAD_DOCINV'
      Origin = 'CADASTRO.CAD_DOCINV'
      Size = 10
    end
    object qImportaCadCEACAD_REFINV: TIBStringField
      FieldName = 'CAD_REFINV'
      Origin = 'CADASTRO.CAD_REFINV'
    end
    object qImportaCadCEACAD_NUMLOJA: TIBStringField
      FieldName = 'CAD_NUMLOJA'
      Origin = 'CADASTRO.CAD_NUMLOJA'
      Size = 10
    end
    object qImportaCadCEACAD_SETDESC: TIBStringField
      FieldName = 'CAD_SETDESC'
      Origin = 'CADASTRO.CAD_SETDESC'
      Size = 100
    end
    object qImportaCadCEACAD_EAN: TIBStringField
      FieldName = 'CAD_EAN'
      Origin = 'CADASTRO.CAD_EAN'
      Size = 25
    end
    object qImportaCadCEACAD_EAN_ORIG: TIBStringField
      FieldName = 'CAD_EAN_ORIG'
      Origin = 'CADASTRO.CAD_EAN_ORIG'
      Size = 25
    end
    object qImportaCadCEACAD_CODINT: TIBStringField
      FieldName = 'CAD_CODINT'
      Origin = 'CADASTRO.CAD_CODINT'
    end
    object qImportaCadCEACAD_DESC: TIBStringField
      FieldName = 'CAD_DESC'
      Origin = 'CADASTRO.CAD_DESC'
      Size = 100
    end
    object qImportaCadCEACAD_QUANTIDADE: TFloatField
      FieldName = 'CAD_QUANTIDADE'
      Origin = 'CADASTRO.CAD_QUANTIDADE'
    end
    object qImportaCadCEACAD_LOTE: TIBStringField
      FieldName = 'CAD_LOTE'
      Origin = 'CADASTRO.CAD_LOTE'
      Size = 25
    end
  end
  object DsImportaCadCEA: TDataSource
    DataSet = CDSImportaCadCEA
    Left = 432
    Top = 121
  end
  object CDSImportaCadCEA: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DSPImportaCadCEA'
    Left = 328
    Top = 121
    object CDSImportaCadCEACAD_DOCINV: TStringField
      FieldName = 'CAD_DOCINV'
      Origin = 'CADASTRO.CAD_DOCINV'
      Size = 10
    end
    object CDSImportaCadCEACAD_REFINV: TStringField
      FieldName = 'CAD_REFINV'
      Origin = 'CADASTRO.CAD_REFINV'
    end
    object CDSImportaCadCEACAD_NUMLOJA: TStringField
      FieldName = 'CAD_NUMLOJA'
      Origin = 'CADASTRO.CAD_NUMLOJA'
      Size = 10
    end
    object CDSImportaCadCEACAD_SETDESC: TStringField
      FieldName = 'CAD_SETDESC'
      Origin = 'CADASTRO.CAD_SETDESC'
      Size = 100
    end
    object CDSImportaCadCEACAD_EAN: TStringField
      FieldName = 'CAD_EAN'
      Origin = 'CADASTRO.CAD_EAN'
      Size = 25
    end
    object CDSImportaCadCEACAD_EAN_ORIG: TStringField
      FieldName = 'CAD_EAN_ORIG'
      Origin = 'CADASTRO.CAD_EAN_ORIG'
      Size = 25
    end
    object CDSImportaCadCEACAD_CODINT: TStringField
      FieldName = 'CAD_CODINT'
      Origin = 'CADASTRO.CAD_CODINT'
    end
    object CDSImportaCadCEACAD_DESC: TStringField
      FieldName = 'CAD_DESC'
      Origin = 'CADASTRO.CAD_DESC'
      Size = 100
    end
    object CDSImportaCadCEACAD_QUANTIDADE: TFloatField
      FieldName = 'CAD_QUANTIDADE'
      Origin = 'CADASTRO.CAD_QUANTIDADE'
    end
    object CDSImportaCadCEACAD_LOTE: TStringField
      FieldName = 'CAD_LOTE'
      Origin = 'CADASTRO.CAD_LOTE'
      Size = 25
    end
  end
  object DSPImportaCadCEA: TDataSetProvider
    DataSet = qImportaCadCEA
    Options = [poAllowCommandText]
    UpdateMode = upWhereKeyOnly
    Left = 224
    Top = 121
  end
  object DSPImportaCadCEAAux: TDataSetProvider
    DataSet = qImportaCadCEAAux
    Options = [poAllowCommandText]
    UpdateMode = upWhereKeyOnly
    Left = 685
    Top = 121
  end
  object qImportaCadCEAAux: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    Left = 741
    Top = 122
    object qImportaCadCEAAuxUIN: TStringField
      FieldName = 'UIN'
    end
    object qImportaCadCEAAuxITEM_ID: TStringField
      FieldName = 'ITEM_ID'
      Size = 25
    end
    object qImportaCadCEAAuxITEM_DESC: TStringField
      FieldName = 'ITEM_DESC'
      Size = 50
    end
    object qImportaCadCEAAuxITEM_SNAPSHOT: TFloatField
      FieldName = 'ITEM_SNAPSHOT'
    end
    object qImportaCadCEAAuxSTOCK_COUNT_ID: TStringField
      FieldName = 'STOCK_COUNT_ID'
    end
    object qImportaCadCEAAuxCOUNT_ID: TStringField
      FieldName = 'COUNT_ID'
      Size = 10
    end
    object qImportaCadCEAAuxSTORE_ID: TStringField
      FieldName = 'STORE_ID'
      Size = 10
    end
    object qImportaCadCEAAuxDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 30
    end
  end
  object XMLTransProvCEA: TXMLTransformProvider
    XMLDataFile = 'C:\SGD\Cadastro\cadastro_prod.xml'
    Left = 1005
    Top = 121
  end
  object CDSImportaCadCEAAuxIBQuery: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT CAD_DOCINV, '
      '               CAD_REFINV, '
      '               CAD_NUMLOJA, '
      '               CAD_SETDESC, '
      '               CAD_EAN, '
      '               CAD_EAN_ORIG,'
      '               CAD_CODINT,  '
      '               CAD_DESC, '
      '               CAD_QUANTIDADE, '
      '              CAD_LOTE'
      ' FROM CADASTRO ')
    Left = 877
    Top = 121
    object CDSImportaCadCEAAuxIBStringField: TIBStringField
      FieldName = 'CAD_DOCINV'
      Origin = 'CADASTRO.CAD_DOCINV'
      Size = 10
    end
    object CDSImportaCadCEAAuxIBStringField2: TIBStringField
      FieldName = 'CAD_REFINV'
      Origin = 'CADASTRO.CAD_REFINV'
    end
    object CDSImportaCadCEAAuxIBStringField3: TIBStringField
      FieldName = 'CAD_NUMLOJA'
      Origin = 'CADASTRO.CAD_NUMLOJA'
      Size = 10
    end
    object CDSImportaCadCEAAuxIBStringField4: TIBStringField
      FieldName = 'CAD_SETDESC'
      Origin = 'CADASTRO.CAD_SETDESC'
      Size = 30
    end
    object CDSImportaCadCEAAuxIBStringField5: TIBStringField
      FieldName = 'CAD_EAN'
      Origin = 'CADASTRO.CAD_EAN'
      Size = 25
    end
    object CDSImportaCadCEAAuxIBStringField6: TIBStringField
      FieldName = 'CAD_EAN_ORIG'
      Origin = 'CADASTRO.CAD_EAN_ORIG'
      Size = 25
    end
    object CDSImportaCadCEAAuxIBStringField7: TIBStringField
      FieldName = 'CAD_CODINT'
      Origin = 'CADASTRO.CAD_CODINT'
    end
    object CDSImportaCadCEAAuxIBStringField8: TIBStringField
      FieldName = 'CAD_DESC'
      Origin = 'CADASTRO.CAD_DESC'
      Size = 50
    end
    object CDSImportaCadCEAAuxFloatField: TFloatField
      FieldName = 'CAD_QUANTIDADE'
      Origin = 'CADASTRO.CAD_QUANTIDADE'
    end
    object CDSImportaCadCEAAuxIBStringField9: TIBStringField
      FieldName = 'CAD_LOTE'
      Origin = 'CADASTRO.CAD_LOTE'
      Size = 25
    end
  end
  object CDSImportaCadCEAAux: TClientDataSet
    Aggregates = <>
    FileName = 'C:\SGD\Cadastro\cadastro_prod.xml'
    Params = <>
    ProviderName = 'DSPImportaCadCEAAux'
    Left = 544
    Top = 120
    object CDSImportaCadCEAAuxUIN: TStringField
      FieldName = 'UIN'
    end
    object CDSImportaCadCEAAuxITEM_ID: TStringField
      FieldName = 'ITEM_ID'
      Size = 25
    end
    object CDSImportaCadCEAAuxITEM_DESC: TStringField
      FieldName = 'ITEM_DESC'
      Size = 50
    end
    object CDSImportaCadCEAAuxITEM_SNAPSHOT: TFloatField
      FieldName = 'ITEM_SNAPSHOT'
    end
    object CDSImportaCadCEAAuxSTOCK_COUNT_ID: TStringField
      FieldName = 'STOCK_COUNT_ID'
    end
    object CDSImportaCadCEAAuxCOUNT_ID: TStringField
      FieldName = 'COUNT_ID'
      Size = 10
    end
    object CDSImportaCadCEAAuxSTORE_ID: TStringField
      FieldName = 'STORE_ID'
      Size = 10
    end
    object CDSImportaCadCEAAuxDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 30
    end
  end
  object QRupturaLoja: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'update CONTAGENS'
      '   set RUP_LOJA = '#39'X'#39
      '    where CON_STATUS = '#39'.'#39' AND'
      '          CON_EAN IN (select c.con_ean'
      '                       from contagens c'
      
        '                        INNER JOIN ruptura r on (c.SEC_SECAO BET' +
        'WEEN r.rup_deposito AND r.rup_lojafim)'
      '                        where (c.con_status = ".")'
      
        '                          and (c.SEC_SECAO BETWEEN :rup_loja AND' +
        ' :rup_lojafim))')
    Left = 1107
    Top = 425
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'rup_loja'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'rup_lojafim'
        ParamType = ptUnknown
      end>
  end
  object QRupturaDeposito: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'update CONTAGENS'
      '   set RUP_DEPOSITO = '#39'X'#39
      '    where CON_STATUS = '#39'.'#39' AND'
      '          CON_EAN IN (select c.con_ean'
      '                       from contagens c'
      
        '                        INNER JOIN ruptura r on (c.SEC_SECAO BET' +
        'WEEN r.rup_deposito AND r.rup_lojafim)'
      '                        where (c.con_status = ".")'
      
        '                          and (c.SEC_SECAO BETWEEN :rup_deposito' +
        ' AND :rup_depositofim))')
    Left = 328
    Top = 421
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'rup_deposito'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'rup_depositofim'
        ParamType = ptUnknown
      end>
  end
  object QRupturaRange: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      
        'insert into ruptura (rup_deposito, rup_depositofim, rup_loja, Ru' +
        'p_lojafim)'
      'values (:DepIni, :Depfim, :Rupini, :RupFim)')
    Left = 32
    Top = 416
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'DepIni'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'Depfim'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'Rupini'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'RupFim'
        ParamType = ptUnknown
      end>
  end
  object QRupturaTable: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select distinct r.rup_deposito,'
      '                r.rup_depositofim,'
      '                r.rup_loja,'
      '                r.rup_lojafim'
      ' from ruptura r')
    Left = 128
    Top = 368
    object QRupturaTableRUP_DEPOSITO: TIBStringField
      FieldName = 'RUP_DEPOSITO'
      Origin = 'RUPTURA.RUP_DEPOSITO'
      Size = 5
    end
    object QRupturaTableRUP_DEPOSITOFIM: TIBStringField
      FieldName = 'RUP_DEPOSITOFIM'
      Origin = 'RUPTURA.RUP_DEPOSITOFIM'
      Size = 5
    end
    object QRupturaTableRUP_LOJA: TIBStringField
      FieldName = 'RUP_LOJA'
      Origin = 'RUPTURA.RUP_LOJA'
      Size = 5
    end
    object QRupturaTableRUP_LOJAFIM: TIBStringField
      FieldName = 'RUP_LOJAFIM'
      Origin = 'RUPTURA.RUP_LOJAFIM'
      Size = 5
    end
  end
  object QRelRuptura: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select co.con_ean,'
      '   sum(co.CON_QUANTIDADE),'
      '   ca.cad_codint,'
      '   ca.cad_un,'
      '   ca.cad_preco,'
      '   ca.cad_desc,'
      '   ca.cad_setor,'
      '   co.sec_secao '
      'from contagens co'
      ' INNER join cadastro ca on (ca.cad_ean = co.con_ean)'
      '  where (co.rup_deposito is null) and (co.rup_loja = "X")'
      '  group by co.con_ean,'
      '           ca.cad_codint,'
      '           co.sec_secao, '
      '           ca.cad_setor,'
      '           ca.cad_un,'
      '           ca.cad_preco,'
      '           ca.cad_desc'
      'order by ca.cad_preco desc')
    Left = 997
    Top = 490
    object QRelRupturaCON_EAN: TIBStringField
      FieldName = 'CON_EAN'
      Origin = 'CONTAGENS.CON_EAN'
      Size = 25
    end
    object QRelRupturaSUM: TFloatField
      FieldName = 'SUM'
      ProviderFlags = []
      DisplayFormat = '##,###0.000'
    end
    object QRelRupturaCAD_CODINT: TIBStringField
      FieldName = 'CAD_CODINT'
      Origin = 'CADASTRO.CAD_CODINT'
    end
    object QRelRupturaCAD_UN: TIBStringField
      FieldName = 'CAD_UN'
      Origin = 'CADASTRO.CAD_UN'
      Size = 5
    end
    object QRelRupturaCAD_PRECO: TFloatField
      FieldName = 'CAD_PRECO'
      Origin = 'CADASTRO.CAD_PRECO'
      DisplayFormat = 'R$ ##,###0.00'
    end
    object QRelRupturaCAD_DESC: TIBStringField
      FieldName = 'CAD_DESC'
      Origin = 'CADASTRO.CAD_DESC'
      Size = 100
    end
    object QRelRupturaCAD_SETOR: TIBStringField
      FieldName = 'CAD_SETOR'
      Origin = 'CADASTRO.CAD_SETOR'
    end
    object QRelRupturaSEC_SECAO: TIBStringField
      FieldName = 'SEC_SECAO'
      Origin = 'CONTAGENS.SEC_SECAO'
      Size = 25
    end
  end
  object RvRelRuptura: TRvDataSetConnection
    RuntimeVisibility = rtDeveloper
    DataSet = QRelRuptura
    Left = 995
    Top = 489
  end
  object qGeraArqFinalPET_AVARIA: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT COD_INT,'
      '       SUM(QTD) AS QTD,'
      '       ARMAZEM,'
      '       NUM_LOJA'
      '           FROM arq_pet'
      '            GROUP BY 1,3,4')
    Left = 1072
    Top = 192
    object qGeraArqFinalPET_AVARIACOD_INT: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'COD_INT'
      Origin = 'ARQ_PET.COD_INT'
      ProviderFlags = []
      ReadOnly = True
    end
    object qGeraArqFinalPET_AVARIAQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
    object qGeraArqFinalPET_AVARIAARMAZEM: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'ARMAZEM'
      Origin = 'ARQ_PET.ARMAZEM'
      ProviderFlags = []
      ReadOnly = True
      FixedChar = True
      Size = 2
    end
    object qGeraArqFinalPET_AVARIANUM_LOJA: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'NUM_LOJA'
      Origin = 'ARQ_PET.NUM_LOJA'
      ProviderFlags = []
      ReadOnly = True
      Size = 10
    end
  end
  object qGeraArqFinalPET_VENDA: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT COD_INT,'
      '       SUM(QTD) AS QTD,'
      '       ARMAZEM,'
      '       NUM_LOJA'
      '           FROM arq_pet2'
      '            GROUP BY 1,3,4')
    Left = 1208
    Top = 192
    object qGeraArqFinalPET_VENDACOD_INT: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'COD_INT'
      Origin = 'ARQ_PET2.COD_INT'
      ProviderFlags = []
      ReadOnly = True
    end
    object qGeraArqFinalPET_VENDAQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
    object qGeraArqFinalPET_VENDAARMAZEM: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'ARMAZEM'
      Origin = 'ARQ_PET2.ARMAZEM'
      ProviderFlags = []
      ReadOnly = True
      FixedChar = True
      Size = 2
    end
    object qGeraArqFinalPET_VENDANUM_LOJA: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'NUM_LOJA'
      Origin = 'ARQ_PET2.NUM_LOJA'
      ProviderFlags = []
      ReadOnly = True
      Size = 10
    end
  end
  object QRupturaLojaInt: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'update CONTAGENS co'
      'set co.rup_loja = '#39'X'#39
      'where co.con_status = '#39'.'#39
      'and co.con_cadcodint in (select c.con_cadcodint'
      '                           from contagens c'
      '                         inner join ruptura r'
      
        '                                 on (c.sec_secao between r.rup_d' +
        'eposito'
      
        '                                                    and r.rup_lo' +
        'jafim)'
      '                          where (c.con_status = ".")'
      '                            and (c.sec_secao between :rup_loja'
      
        '                                                and :rup_lojafim' +
        '))')
    Left = 232
    Top = 421
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'rup_loja'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'rup_lojafim'
        ParamType = ptUnknown
      end>
  end
  object QRupturaDepositoInt: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'update CONTAGENS co'
      'set co.rup_deposito = '#39'X'#39
      'where co.con_status = '#39'.'#39
      'and  co.con_cadcodint in (select c.con_cadcodint'
      '                            from contagens c'
      '                          inner join ruptura r'
      '                             on (c.sec_secao between'
      
        '                                r.rup_deposito and r.rup_lojafim' +
        ')'
      '                          where (c.con_status = ".")'
      
        '                            and (c.sec_secao between :rup_deposi' +
        'to'
      
        '                                                and :rup_deposit' +
        'ofim))')
    Left = 1007
    Top = 423
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'rup_deposito'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'rup_depositofim'
        ParamType = ptUnknown
      end>
  end
  object QRelRupturaInt: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select max(co.con_ean),'
      '   sum(co.CON_QUANTIDADE),'
      '   ca.cad_codint,'
      '   ca.cad_un,'
      '   ca.cad_preco,'
      '   ca.cad_desc,'
      '   ca.cad_Setor,'
      '   co.sec_secao'
      'from contagens co'
      ' INNER join cadastro ca on (ca.cad_codint = co.con_cadcodint)'
      '  where (co.rup_deposito is null) and (co.rup_loja = "X")'
      '     and ca.rdb$db_key = (select max(ca2.rdb$db_key)'
      '                        from cadastro ca2'
      '                       where ca2.cad_ean = co.con_ean)'
      '  group by ca.cad_codint,'
      '                 co.sec_secao,'
      '                 ca.cad_setor,'
      '                 ca.cad_un,'
      '                 ca.cad_preco,'
      '                 cad_desc'
      'order by ca.cad_preco desc')
    Left = 1096
    Top = 488
    object QRelRupturaIntSUM: TFloatField
      FieldName = 'SUM'
      ProviderFlags = []
      DisplayFormat = '##,###0.000'
    end
    object QRelRupturaIntCAD_CODINT: TIBStringField
      FieldName = 'CAD_CODINT'
      Origin = 'CADASTRO.CAD_CODINT'
    end
    object QRelRupturaIntCAD_UN: TIBStringField
      FieldName = 'CAD_UN'
      Origin = 'CADASTRO.CAD_UN'
      Size = 5
    end
    object QRelRupturaIntCAD_PRECO: TFloatField
      FieldName = 'CAD_PRECO'
      Origin = 'CADASTRO.CAD_PRECO'
      DisplayFormat = 'R$ ##,###0.00'
    end
    object QRelRupturaIntCAD_DESC: TIBStringField
      FieldName = 'CAD_DESC'
      Origin = 'CADASTRO.CAD_DESC'
      Size = 100
    end
    object QRelRupturaIntMAX: TIBStringField
      FieldName = 'MAX'
      ProviderFlags = []
      Size = 25
    end
    object QRelRupturaIntCAD_SETOR: TIBStringField
      FieldName = 'CAD_SETOR'
      Origin = 'CADASTRO.CAD_SETOR'
    end
    object QRelRupturaIntSEC_SECAO: TIBStringField
      FieldName = 'SEC_SECAO'
      Origin = 'CONTAGENS.SEC_SECAO'
      Size = 25
    end
  end
  object RvRelRupturaInt: TRvDataSetConnection
    RuntimeVisibility = rtDeveloper
    DataSet = QRelRupturaInt
    Left = 1099
    Top = 489
  end
  object UpdateRupLoja: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'update contagens set rup_loja = null'
      'where rup_loja is not null')
    Left = 208
    Top = 368
  end
  object UpdateRupDeposito: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'update contagens set rup_deposito = null'
      'where rup_deposito is not null')
    Left = 312
    Top = 368
  end
  object QGeraArqFinalDEC: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select con_ean as EAN,'
      '       sum(con_quantidade) as QTD'
      '        from contagens'
      '         left join cadastro on (cad_ean = con_ean)'
      '          where con_status = '#39'.'#39' or con_status is null'
      '          group by 1')
    Left = 1240
    Top = 64
    object QGeraArqFinalDECEAN: TIBStringField
      FieldName = 'EAN'
      ProviderFlags = []
      Size = 25
    end
    object QGeraArqFinalDECQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalNFe: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select distinct nfe'
      ' from contagens')
    Left = 520
    Top = 368
    object QGeraArqFinalNFeNFE: TIBStringField
      FieldName = 'NFE'
      Origin = 'CONTAGENS.NFE'
      Size = 100
    end
  end
  object QGeraArqFinalBAR: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      ' SELECT CA.CAD_CODINT AS INTERNO,'
      '       '#39'99'#39' AS LOCAL,'
      '       '#39'PA'#39' AS TIPO,'
      '       '#39'000000001'#39' AS NUM_DOC,'
      '       SUM(CO.CON_QUANTIDADE) AS QTD'
      '        FROM CONTAGENS CO'
      '         INNER JOIN CADASTRO CA ON (CA.CAD_EAN = CO.CON_EAN)'
      '          where (CO.CON_STATUS = '#39'.'#39' or CO.CON_STATUS IS NULL)'
      '          GROUP BY 1,2,3,4')
    Left = 48
    Top = 536
    object QGeraArqFinalBARINTERNO: TIBStringField
      FieldName = 'INTERNO'
      ProviderFlags = []
    end
    object QGeraArqFinalBARTIPO: TIBStringField
      FieldName = 'TIPO'
      ProviderFlags = []
      FixedChar = True
      Size = 2
    end
    object QGeraArqFinalBARNUM_DOC: TIBStringField
      FieldName = 'NUM_DOC'
      ProviderFlags = []
      FixedChar = True
      Size = 9
    end
    object QGeraArqFinalBARQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
    object QGeraArqFinalBARLOCAL: TIBStringField
      FieldName = 'LOCAL'
      ProviderFlags = []
      FixedChar = True
      Size = 2
    end
  end
  object QGeraArqFinalDVL_Loja: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select c.con_ean,'
      '       sum(c.con_quantidade) as qtd_loja'
      'from contagens c'
      'inner join cadastro ca'
      '        on c.con_ean = ca.cad_ean'
      'WHERE (c.con_status = '#39'.'#39
      '   or c.con_status is null)'
      '  and EXISTS (select 1'
      '               from mapeamento m'
      '              WHERE c.sec_secao Between m.map_secini'
      '                                    and m.map_secfin'
      '                and m.loja = '#39'X'#39')'
      'group by 1')
    Left = 173
    Top = 601
    object QGeraArqFinalDVL_LojaCON_EAN: TIBStringField
      FieldName = 'CON_EAN'
      Origin = 'CONTAGENS.CON_EAN'
      Size = 25
    end
    object QGeraArqFinalDVL_LojaQTD_LOJA: TFloatField
      FieldName = 'QTD_LOJA'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalDVL: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select c.con_ean,'
      '       sum(c.con_quantidade) as qtd_geral'
      'from contagens c'
      'inner join cadastro ca'
      '        on c.con_ean = ca.cad_ean'
      'WHERE (c.con_status = '#39'.'#39
      '   or c.con_status is null)'
      'group by 1')
    Left = 279
    Top = 601
    object QGeraArqFinalDVLCON_EAN: TIBStringField
      FieldName = 'CON_EAN'
      Origin = 'CONTAGENS.CON_EAN'
      Size = 25
    end
    object QGeraArqFinalDVLQTD_GERAL: TFloatField
      FieldName = 'QTD_GERAL'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalMAX: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT CO.SEC_SECAO,'
      '       '#39'12:00'#39' AS HORA,'
      '       CO.CON_EAN,'
      '       SUM(CO.CON_QUANTIDADE) AS QTD'
      '        FROM CONTAGENS CO'
      '         INNER JOIN CADASTRO CA ON (CA.CAD_EAN = CO.CON_EAN)'
      '          WHERE (CO.CON_STATUS = '#39'.'#39' or CO.CON_STATUS IS NULL)'
      '           GROUP BY 1,2,3')
    Left = 400
    Top = 616
    object QGeraArqFinalMAXSEC_SECAO: TIBStringField
      FieldName = 'SEC_SECAO'
      Origin = 'CONTAGENS.SEC_SECAO'
      Size = 25
    end
    object QGeraArqFinalMAXHORA: TIBStringField
      FieldName = 'HORA'
      ProviderFlags = []
      FixedChar = True
      Size = 5
    end
    object QGeraArqFinalMAXCON_EAN: TIBStringField
      FieldName = 'CON_EAN'
      Origin = 'CONTAGENS.CON_EAN'
      Size = 25
    end
    object QGeraArqFinalMAXQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object IBQuery1: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select HOR_TOTALHORA from horario where (event = '#39'CONTAGEM'#39')')
    Left = 184
    Top = 8
    object IBStringField1: TIBStringField
      FieldName = 'HOR_TOTALHORA'
      Origin = 'HORARIO.HOR_TOTALHORA'
      Size = 10
    end
  end
  object IBQuery2: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select HOR_TOTALHORA from horario where (event = '#39'CONTAGEM'#39')')
    Left = 192
    Top = 8
    object IBStringField2: TIBStringField
      FieldName = 'HOR_TOTALHORA'
      Origin = 'HORARIO.HOR_TOTALHORA'
      Size = 10
    end
  end
  object QGeraArqFinalSEB: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT '
      '        COALESCE(CO.CON_EAN, CA.CAD_EAN) AS CON_EAN,'
      '        CA.CAD_PRECOTXT,'
      '        COALESCE(SUM(CO.CON_QUANTIDADE),0) AS QTD'
      '        FROM CONTAGENS CO'
      '         INNER JOIN CADASTRO CA ON (CA.CAD_EAN = CO.CON_EAN)'
      '          WHERE (CO.CON_STATUS = '#39'.'#39' or CO.CON_STATUS IS NULL)'
      '           GROUP BY 1,2')
    Left = 424
    Top = 424
    object QGeraArqFinalSEBCAD_PRECOTXT: TIBStringField
      FieldName = 'CAD_PRECOTXT'
      Origin = 'CADASTRO.CAD_PRECOTXT'
      Size = 15
    end
    object QGeraArqFinalSEBQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
    object QGeraArqFinalSEBCON_EAN: TIBStringField
      FieldName = 'CON_EAN'
      ProviderFlags = []
      Size = 35
    end
  end
  object QGeraArqFinalYKS: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT '
      '        CO.CON_EAN,'
      '        COALESCE(SUM(CO.CON_QUANTIDADE),0) AS QTD'
      '        FROM CONTAGENS CO'
      'WHERE (CO.CON_STATUS = '#39'.'#39' or CO.CON_STATUS IS NULL)'
      'GROUP BY 1')
    Left = 616
    Top = 368
    object QGeraArqFinalYKSCON_EAN: TIBStringField
      FieldName = 'CON_EAN'
      Origin = 'CONTAGENS.CON_EAN'
      Size = 25
    end
    object QGeraArqFinalYKSQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalYOG: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT '
      '        COALESCE(CO.CON_EAN, CA.CAD_EAN) AS CON_CADCODINT,'
      '        COALESCE(SUM(CO.CON_QUANTIDADE),0) AS QTD'
      '        FROM CONTAGENS CO'
      '         INNER JOIN CADASTRO CA ON (CA.CAD_EAN = CO.CON_EAN)'
      '          WHERE (CO.CON_STATUS = '#39'.'#39' or CO.CON_STATUS IS NULL)'
      '           GROUP BY 1')
    Left = 1096
    Top = 312
    object QGeraArqFinalYOGCON_CADCODINT: TIBStringField
      FieldName = 'CON_CADCODINT'
      ProviderFlags = []
      Size = 25
    end
    object QGeraArqFinalYOGQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QImportaEndereco: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'Select * From PRODUTO')
    Left = 1187
    Top = 369
  end
  object QImportaLote: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    Left = 1192
    Top = 312
  end
  object QGeraArqFinalSVC: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT '
      '             CO.CON_EAN,'
      '             COALESCE(SUM(CO.CON_QUANTIDADE),0) AS QTD'
      '    FROM CONTAGENS CO'
      '   INNER JOIN CADASTRO CA ON (CA.CAD_EAN = CO.CON_EAN)'
      ' WHERE (CO.CON_STATUS = '#39'.'#39' or CO.CON_STATUS IS NULL)'
      '  GROUP BY 1')
    Left = 624
    Top = 424
    object QGeraArqFinalSVCCON_EAN: TIBStringField
      FieldName = 'CON_EAN'
      Origin = 'CONTAGENS.CON_EAN'
      Size = 25
    end
    object QGeraArqFinalSVCQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalNIK: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT COALESCE(CO.CON_EAN, CA.CAD_EAN) AS CON_CADEAN,'
      '       COALESCE(SUM(CO.CON_QUANTIDADE),0) AS QTD'
      '  FROM CONTAGENS CO'
      ' INNER JOIN CADASTRO CA ON (CA.CAD_EAN = CO.CON_EAN)'
      ' WHERE (CO.CON_STATUS = '#39'.'#39' or CO.CON_STATUS IS NULL)'
      ' GROUP BY 1')
    Left = 528
    Top = 424
    object QGeraArqFinalNIKCON_CADEAN: TIBStringField
      FieldName = 'CON_CADEAN'
      ProviderFlags = []
      Size = 25
    end
    object QGeraArqFinalNIKQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalROS: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT  CO.CON_EAN as CODEAN,'
      '                COALESCE(SUM(CO.CON_QUANTIDADE),0) AS QTD,'
      '                CAD_GRUPOCOM as TIPOPESO'
      '    FROM CONTAGENS CO'
      '   INNER JOIN CADASTRO CA ON (CA.CAD_EAN = CO.CON_EAN)'
      ' WHERE (CO.CON_STATUS = '#39'.'#39' or CO.CON_STATUS IS NULL)'
      '  GROUP BY CO.CON_EAN,'
      '                      TIPOPESO')
    Left = 816
    Top = 368
    object QGeraArqFinalROSCODEAN: TIBStringField
      FieldName = 'CODEAN'
      ProviderFlags = []
      Size = 25
    end
    object QGeraArqFinalROSQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
    object QGeraArqFinalROSTIPOPESO: TIBStringField
      FieldName = 'TIPOPESO'
      ProviderFlags = []
      Size = 3
    end
  end
  object QGeraArqFinalROS_Loja: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT'
      '    coalesce(co.con_ean,ca.cad_ean) as CodEan,'
      '    coalesce(sum(co.con_quantidade),0) as Qtd,'
      '    CAD_GRUPOCOM as TIPOPESO'
      ' FROM CONTAGENS CO'
      'inner join cadastro ca'
      '   on co.con_ean = ca.cad_ean'
      'WHERE (CO.CON_STATUS = '#39'.'#39
      '   OR CO.CON_STATUS IS NULL)'
      '  AND exists (select 1'
      '                   from secao se'
      '                   where se.sec_secao = co.sec_secao'
      '                     and se.loja = '#39'X'#39')'
      '  GROUP BY CODEAN,'
      '                      TIPOPESO'
      ''
      'ORDER BY 1')
    Left = 936
    Top = 368
    object QGeraArqFinalROS_LojaCODEAN: TIBStringField
      FieldName = 'CODEAN'
      ProviderFlags = []
      Size = 25
    end
    object QGeraArqFinalROS_LojaQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
    object QGeraArqFinalROS_LojaTIPOPESO: TIBStringField
      FieldName = 'TIPOPESO'
      ProviderFlags = []
      Size = 3
    end
  end
  object QGeraArqFinalROS_Dep: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT'
      '    coalesce(co.con_ean,ca.cad_ean) as CodEan,'
      '    coalesce(sum(co.con_quantidade),0) as Qtd,'
      '    CAD_GRUPOCOM as TIPOPESO'
      ' FROM CONTAGENS CO'
      'inner join cadastro ca'
      '   on co.con_ean = ca.cad_ean'
      'WHERE (CO.CON_STATUS = '#39'.'#39
      '   OR CO.CON_STATUS IS NULL)'
      '  AND exists (select 1'
      '                   from secao se'
      '                   where se.sec_secao = co.sec_secao'
      '                     and se.deposito = '#39'X'#39')'
      'GROUP BY CodEan,'
      '                    TIPOPESO'
      'ORDER BY 1')
    Left = 1064
    Top = 368
    object QGeraArqFinalROS_DepCODEAN: TIBStringField
      FieldName = 'CODEAN'
      ProviderFlags = []
      Size = 25
    end
    object QGeraArqFinalROS_DepQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
    object QGeraArqFinalROS_DepTIPOPESO: TIBStringField
      FieldName = 'TIPOPESO'
      ProviderFlags = []
      Size = 3
    end
  end
  object QGeraArqFinalCEC: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      ''
      'SELECT '
      
        '        COALESCE(CO.CON_CADCODINT, CA.CAD_CODINT) AS CON_CADCODI' +
        'NT,'
      '        CA.CAD_DESC,'
      '        CA.CAD_SETOR,'
      '        COALESCE(SUM(CO.CON_QUANTIDADE),0) AS QTD'
      '        FROM CONTAGENS CO'
      
        '         INNER JOIN CADASTRO CA ON (CA.CAD_CODINT = CO.CON_CADCO' +
        'DINT)'
      '          WHERE (CO.CON_STATUS = '#39'.'#39' or CO.CON_STATUS IS NULL)'
      '           GROUP BY 1,2,3')
    Left = 32
    Top = 356
    object QGeraArqFinalCECCON_CADCODINT: TIBStringField
      FieldName = 'CON_CADCODINT'
      ProviderFlags = []
    end
    object QGeraArqFinalCECCAD_DESC: TIBStringField
      FieldName = 'CAD_DESC'
      Origin = 'CADASTRO.CAD_DESC'
      Size = 100
    end
    object QGeraArqFinalCECCAD_SETOR: TIBStringField
      FieldName = 'CAD_SETOR'
      Origin = 'CADASTRO.CAD_SETOR'
    end
    object QGeraArqFinalCECQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalBGB: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT '
      '        COALESCE(CO.CON_CADCODINT, CA.CAD_CODINT) AS CAD_CODINT,'
      '        COALESCE(SUM(CO.CON_QUANTIDADE),0) AS QTD'
      'FROM CONTAGENS CO'
      '         RIGHT JOIN CADASTRO CA ON (CA.CAD_EAN = CO.CON_EAN)'
      'WHERE (CO.CON_STATUS = '#39'.'#39' or CO.CON_STATUS IS NULL)'
      'GROUP BY 1')
    Left = 847
    Top = 311
    object QGeraArqFinalBGBCAD_CODINT: TIBStringField
      FieldName = 'CAD_CODINT'
      ProviderFlags = []
    end
    object QGeraArqFinalBGBQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object qGeraArqFinalBGB_Diferenca: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select CA.CAD_CODINT AS CAD_CODINT,'
      '       SUM(CA.CAD_QUANTIDADE * -1) AS QTD'
      '  from cadastro ca'
      ' where not exists (select 1'
      '                     from contagens co'
      '                    where co.con_ean = ca.cad_ean'
      '                      and co.con_status = '#39'.'#39
      '                      and exists (select 1'
      '                                    from secao se'
      
        '                                   where se.sec_secao = co.sec_s' +
        'ecao'
      '                                     and se.sec_status = '#39'.'#39'))'
      ' group by ca.cad_codint'
      'union'
      'select ca.cad_codint as CAD_CODINT,'
      '       sum(co.con_quantidade) - ca.cad_quantidade as QTD'
      '  from contagens co,'
      '       cadastro ca'
      ' where ca.cad_ean = co.con_ean'
      '   and co.con_status = '#39'.'#39
      '   and exists (select 1'
      '                 from secao se'
      '                where se.sec_secao = co.sec_secao'
      '                  and se.sec_status = '#39'.'#39')'
      ' group by ca.cad_codint, ca.cad_quantidade')
    Left = 727
    Top = 311
    object qGeraArqFinalBGB_DiferencaCAD_CODINT: TIBStringField
      FieldName = 'CAD_CODINT'
      ProviderFlags = []
    end
    object qGeraArqFinalBGB_DiferencaQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalWAL: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      '  SELECT CO.CON_CADCODINT,'
      '         CO.CON_ENDERECO,'
      '         CA.CAD_PRECO,'
      '         COALESCE(SUM(CO.CON_QUANTIDADE),0) AS QTD'
      '    FROM CONTAGENS CO'
      
        '         INNER JOIN CADASTRO CA ON (CA.CAD_CODINT = CO.CON_CADCO' +
        'DINT)'
      '   WHERE (CO.CON_STATUS = '#39'.'#39' or CO.CON_STATUS IS NULL)'
      'GROUP BY 1,2,3')
    Left = 128
    Top = 424
    object QGeraArqFinalWALCON_CADCODINT: TIBStringField
      FieldName = 'CON_CADCODINT'
      Origin = 'CONTAGENS.CON_CADCODINT'
    end
    object QGeraArqFinalWALCON_ENDERECO: TIBStringField
      FieldName = 'CON_ENDERECO'
      Origin = 'CONTAGENS.CON_ENDERECO'
      Size = 25
    end
    object QGeraArqFinalWALCAD_PRECO: TFloatField
      FieldName = 'CAD_PRECO'
      Origin = 'CADASTRO.CAD_PRECO'
    end
    object QGeraArqFinalWALQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalGLM: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT CO.CON_CADCODINT,'
      '       COALESCE(SUM(CO.CON_QUANTIDADE),0) AS QTD'
      '  FROM CONTAGENS CO'
      ' WHERE (CO.CON_STATUS = '#39'.'#39' or CO.CON_STATUS IS NULL)'
      ' GROUP BY 1')
    Left = 599
    Top = 311
    object QGeraArqFinalGLMCON_CADCODINT: TIBStringField
      FieldName = 'CON_CADCODINT'
      Origin = 'CONTAGENS.CON_CADCODINT'
    end
    object QGeraArqFinalGLMQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalCLS: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select ca.cad_refinv as codigo_prod,'
      '       coalesce(co.con_ean, ca.cad_ean) as con_ean,'
      '       coalesce(co.con_cadcodint, ca.cad_codint) as cod_interno,'
      '       ca.cad_desc,'
      '       ca.cad_setor as descr_cor,'
      '       ca.cad_preco,'
      '       coalesce(sum(co.con_quantidade),0) as qtd,'
      '       ca.cad_docinv as desc_unidade,'
      '       ca.cad_numinv as desc_tipoprod,'
      '       ca.cad_data as codigo_cor,'
      '       ca.CAD_NUMITEM as sequenciador'
      '  from contagens co'
      '  right join cadastro ca on (ca.cad_ean = co.con_ean)'
      ' where (co.con_status = '#39'.'#39' or co.con_status is null)'
      ' group by 2,1,3,4,5,6,8,9,10,11'
      ' order by 2')
    Left = 416
    Top = 368
    object QGeraArqFinalCLSCODIGO_PROD: TIBStringField
      FieldName = 'CODIGO_PROD'
      ProviderFlags = []
    end
    object QGeraArqFinalCLSCON_EAN: TIBStringField
      FieldName = 'CON_EAN'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalCLSCOD_INTERNO: TIBStringField
      FieldName = 'COD_INTERNO'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalCLSCAD_DESC: TIBStringField
      FieldName = 'CAD_DESC'
      Origin = 'CADASTRO.CAD_DESC'
      Size = 100
    end
    object QGeraArqFinalCLSDESCR_COR: TIBStringField
      FieldName = 'DESCR_COR'
      ProviderFlags = []
    end
    object QGeraArqFinalCLSCAD_PRECO: TFloatField
      FieldName = 'CAD_PRECO'
      Origin = 'CADASTRO.CAD_PRECO'
    end
    object QGeraArqFinalCLSQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
    object QGeraArqFinalCLSDESC_UNIDADE: TIBStringField
      FieldName = 'DESC_UNIDADE'
      ProviderFlags = []
      Size = 10
    end
    object QGeraArqFinalCLSDESC_TIPOPROD: TIBStringField
      FieldName = 'DESC_TIPOPROD'
      ProviderFlags = []
    end
    object QGeraArqFinalCLSCODIGO_COR: TIBStringField
      FieldName = 'CODIGO_COR'
      ProviderFlags = []
      Size = 8
    end
    object QGeraArqFinalCLSSEQUENCIADOR: TIBStringField
      FieldName = 'SEQUENCIADOR'
      ProviderFlags = []
      Size = 5
    end
  end
  object QGeraArqFinalJFL: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      '  select coalesce(ca.cad_ean,co.con_ean) as con_ean,'
      '         ca.cad_desc,'
      '         coalesce(sum(co.con_quantidade),0) as qtd'
      '    from contagens co'
      '         right join cadastro ca on (ca.cad_ean = co.con_ean)'
      '   where (co.con_status = '#39'.'#39' or co.con_status is null)'
      'group by 1,2')
    Left = 248
    Top = 312
    object QGeraArqFinalJFLCON_EAN: TIBStringField
      FieldName = 'CON_EAN'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalJFLCAD_DESC: TIBStringField
      FieldName = 'CAD_DESC'
      Origin = 'CADASTRO.CAD_DESC'
      Size = 100
    end
    object QGeraArqFinalJFLQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalDCS: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT '
      '        CO.CON_EAN,'
      '        COALESCE(SUM(CO.CON_QUANTIDADE),0) AS QTD'
      '        FROM CONTAGENS CO'
      'WHERE (CO.CON_STATUS = '#39'.'#39' or CO.CON_STATUS IS NULL)'
      'GROUP BY 1')
    Left = 352
    Top = 312
    object QGeraArqFinalDCSCON_EAN: TIBStringField
      FieldName = 'CON_EAN'
      Origin = 'CONTAGENS.CON_EAN'
      Size = 35
    end
    object QGeraArqFinalDCSQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalNCL: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      ' select coalesce(co.con_cadcodint,ca.cad_codint) as interno,'
      '       ca.cad_desc as descricao,'
      '       ca.cad_un,'
      '       coalesce(co.con_ean, ca.cad_ean) as ean,'
      '       sum(con_quantidade) as qtd'
      '  from contagens co'
      ' right join cadastro ca on co.con_ean  = ca.cad_ean'
      ' where (con_status =  '#39'.'#39
      '    or con_status is null)'
      ' group by ean,'
      '          interno,'
      '          --co.con_cadcodint,'
      '          ca.cad_desc,'
      '          ca.cad_un'
      ' order by interno')
    Left = 1080
    Top = 248
    object QGeraArqFinalNCLINTERNO: TIBStringField
      FieldName = 'INTERNO'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalNCLDESCRICAO: TIBStringField
      FieldName = 'DESCRICAO'
      ProviderFlags = []
      Size = 100
    end
    object QGeraArqFinalNCLCAD_UN: TIBStringField
      FieldName = 'CAD_UN'
      Origin = 'CADASTRO.CAD_UN'
      Size = 5
    end
    object QGeraArqFinalNCLEAN: TIBStringField
      FieldName = 'EAN'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalNCLQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalLOC: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      ' select con_ean as ean,'
      '        con_cadcodint as cod_artigo,'
      '       sum(con_quantidade) as qtd'
      '  from contagens co'
      ' where (con_status =  '#39'.'#39
      '    or con_status is null)'
      ' group by ean,'
      '          cod_artigo'
      ' order by ean')
    Left = 984
    Top = 248
    object QGeraArqFinalLOCEAN: TIBStringField
      FieldName = 'EAN'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalLOCCOD_ARTIGO: TIBStringField
      FieldName = 'COD_ARTIGO'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalLOCQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalNTR: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      ' select coalesce(co.con_cadcodint,ca.cad_codint) as interno,'
      '       ca.cad_desc as descricao,'
      '       ca.cad_un,'
      '       coalesce(co.con_ean, ca.cad_ean) as ean,'
      '       sum(con_quantidade) as qtd'
      '  from contagens co'
      ' right join cadastro ca on co.con_ean  = ca.cad_ean'
      ' where (con_status =  '#39'.'#39
      '    or con_status is null)'
      ' group by ean,'
      '          interno,'
      '          --co.con_cadcodint,'
      '          ca.cad_desc,'
      '          ca.cad_un'
      ' order by interno')
    Left = 1184
    Top = 248
    object QGeraArqFinalNTRINTERNO: TIBStringField
      FieldName = 'INTERNO'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalNTRDESCRICAO: TIBStringField
      FieldName = 'DESCRICAO'
      ProviderFlags = []
      Size = 100
    end
    object QGeraArqFinalNTRCAD_UN: TIBStringField
      FieldName = 'CAD_UN'
      Origin = 'CADASTRO.CAD_UN'
      Size = 5
    end
    object QGeraArqFinalNTREAN: TIBStringField
      FieldName = 'EAN'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalNTRQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalSDD: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT '
      '        CO.CON_EAN,'
      '        COALESCE(SUM(CO.CON_QUANTIDADE),0) AS QTD'
      '        FROM CONTAGENS CO'
      'WHERE (CO.CON_STATUS = '#39'.'#39' or CO.CON_STATUS IS NULL)'
      'GROUP BY 1')
    Left = 156
    Top = 545
    object QGeraArqFinalSDDCON_EAN: TIBStringField
      FieldName = 'CON_EAN'
      Origin = 'CONTAGENS.CON_EAN'
      Size = 35
    end
    object QGeraArqFinalSDDQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalEDR: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select coalesce(co.con_ean, ca.cad_ean) as con_ean,'
      '       ca.cad_codint as cod_int,'
      '       coalesce(sum(co.con_quantidade),0) as qtd,'
      '       ca.cad_desc as descricao'
      '  from contagens co'
      ' Right join cadastro ca on (ca.cad_ean = co.con_ean)'
      ' where (co.con_status = '#39'.'#39' or co.con_status is null)'
      ' group by 1,2,4')
    Left = 480
    Top = 488
    object QGeraArqFinalEDRCON_EAN: TIBStringField
      FieldName = 'CON_EAN'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalEDRCOD_INT: TIBStringField
      FieldName = 'COD_INT'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalEDRQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
    object QGeraArqFinalEDRDESCRICAO: TIBStringField
      FieldName = 'DESCRICAO'
      ProviderFlags = []
      Size = 100
    end
  end
  object IBConsultaLoteCEC: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'Select cad_Lote'
      '  From cadastro'
      'where cad_ean = :cad_ean'
      '    and cad_codint = :cad_codint')
    Left = 712
    Top = 368
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'cad_ean'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'cad_codint'
        ParamType = ptUnknown
      end>
  end
  object QGeraArqFinalDFY: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select a.codigo_interno as SAC,'
      '       b.quantidade as QTD'
      '   from (select sum(ca.cad_quantidade) as quantidade,'
      '                ca.CAD_CODINT as codigo_interno'
      '           from cadastro ca'
      '          group by ca.CAD_CODINT) A,'
      '        (select sum(co.con_quantidade) as quantidade,'
      '                co.con_cadcodint as codigo_interno'
      '           from contagens co'
      '          where co.con_status = '#39'.'#39
      '       group by co.con_cadcodint) B'
      ' where a.codigo_interno = b.codigo_interno'
      'union'
      'select ca.CAD_CODINT as SAC,'
      '       0 as quantidade_contagem'
      '  from cadastro ca'
      ' where not exists (select 1'
      '                     from contagens co'
      '                    where co.con_cadcodint = ca.cad_codint'
      '                      and co.con_status = '#39'.'#39')'
      ' group by ca.CAD_CODINT'
      'union'
      'select co.CON_CADCODINT as SAC,'
      '       sum(co.con_quantidade) as quantidade_contagem'
      '  from contagens co'
      ' where not exists (select 1'
      '                     from cadastro ca'
      '                    where ca.cad_codint = co.con_cadcodint)'
      '   and co.con_status = '#39'.'#39
      ' group by  co.CON_CADCODINT')
    Left = 592
    Top = 488
    object QGeraArqFinalDFYSAC: TIBStringField
      FieldName = 'SAC'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalDFYQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QDivQtd: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'Select *'
      'from (select a.ean as ean,'
      '       a.codigo_interno as codigo_interno,'
      '       a.descricao as descricao,'
      '       a.quantidade as quantidade_cadastro,'
      '       b.quantidade as quantidade_contagem,'
      '       a.unidade,'
      '       b.quantidade - a.quantidade  as divergencia'
      '   from (select sum(ca.cad_quantidade) as quantidade,'
      '                ca.cad_ean as ean,'
      '                ca.cad_desc as descricao,'
      '                ca.CAD_CODINT as codigo_interno,'
      '                ca.cad_un as unidade'
      '           from cadastro ca'
      '          group by ca.cad_ean,'
      '                ca.cad_desc,'
      '                ca.CAD_CODINT,'
      '                ca.cad_un) A,'
      '        (select sum(co.con_quantidade) as quantidade,'
      '                co.con_ean as ean,'
      '                co.con_cadcodint as codigo_interno'
      '           from contagens co'
      '          where co.con_status = '#39'.'#39
      '       group by co.con_ean,'
      '                co.con_cadcodint) B'
      ' where a.ean = b.ean'
      '   and a.quantidade <> b.quantidade'
      'union'
      'select ca.cad_ean as ean,'
      '       ca.CAD_CODINT as codigo_interno,'
      '       ca.CAD_DESC as descricao,'
      '       sum(ca.cad_quantidade) as quantidade_cadastro,'
      '       0 as quantidade_contagem,'
      '       ca.cad_un as unidade,'
      '       sum(ca.cad_quantidade) * -1 as divergencia'
      '  from cadastro ca'
      ' where not exists (select 1'
      '                     from contagens co'
      '                    where co.con_ean = ca.cad_ean'
      '                      and co.con_status = '#39'.'#39')'
      ' group by ca.cad_ean,'
      '       ca.CAD_CODINT,'
      '       ca.CAD_DESC,'
      '       ca.cad_un'
      'union'
      'select co.con_ean as ean,'
      '       co.CON_CADCODINT as codigo_interno,'
      '       '#39'N'#195'O CADASTRADO'#39' as descricao,'
      '       0 as quantidade_cadastro,'
      '       sum(co.con_quantidade) as quantidade_contagem,'
      '       null as unidade,'
      '       sum(co.con_quantidade) as divergencia'
      '  from contagens co'
      ' where not exists (select 1'
      '                     from cadastro ca'
      '                    where ca.cad_ean = co.con_ean)'
      '   and co.con_status = '#39'.'#39
      ' group by co.con_ean,'
      '          co.CON_CADCODINT)'
      'where abs(divergencia) >= :divergencia'
      'order by ean,'
      '         codigo_interno')
    Left = 232
    Top = 544
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'divergencia'
        ParamType = ptUnknown
      end>
    object QDivQtdEAN: TIBStringField
      FieldName = 'EAN'
      ProviderFlags = []
      Size = 35
    end
    object QDivQtdCODIGO_INTERNO: TIBStringField
      FieldName = 'CODIGO_INTERNO'
      ProviderFlags = []
      Size = 35
    end
    object QDivQtdDESCRICAO: TIBStringField
      FieldName = 'DESCRICAO'
      ProviderFlags = []
      Size = 100
    end
    object QDivQtdQUANTIDADE_CADASTRO: TFloatField
      FieldName = 'QUANTIDADE_CADASTRO'
      ProviderFlags = []
      DisplayFormat = '##,###0.000'
    end
    object QDivQtdQUANTIDADE_CONTAGEM: TFloatField
      FieldName = 'QUANTIDADE_CONTAGEM'
      ProviderFlags = []
      DisplayFormat = '##,###0.000'
    end
    object QDivQtdDIVERGENCIA: TFloatField
      FieldName = 'DIVERGENCIA'
      ProviderFlags = []
      DisplayFormat = '##,###0.000'
    end
    object QDivQtdUNIDADE: TIBStringField
      FieldName = 'UNIDADE'
      ProviderFlags = []
      Size = 5
    end
  end
  object RvDivQTD: TRvDataSetConnection
    RuntimeVisibility = rtDeveloper
    DataSet = QDivQtd
    Left = 256
    Top = 544
  end
  object QDivQtd1: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      ' select'
      '       (CASE'
      '         when (SUM(CON_QUANTIDADE) = 0) THEN 0'
      '          else SUM(CON_QUANTIDADE) end) as QTD,'
      '       (select CAD_UN'
      '             FROM cadastro'
      '              WHERE (CAD_EAN = CON_EAN)'
      '              GROUP by CAD_UN) AS CAD_UN,'
      '            SEC_SECAO,'
      '            CON_ENDERECO,'
      '            CON_EAN'
      '  from contagens'
      '        WHERE(CON_STATUS =  '#39'.'#39')'
      '         or  (CON_STATUS is Null)'
      '      group by 2,3,4,5')
    Left = 312
    Top = 544
    object QDivQtd1QTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
      DisplayFormat = '##,###0.000'
    end
    object QDivQtd1CAD_UN: TIBStringField
      FieldName = 'CAD_UN'
      ProviderFlags = []
      Size = 5
    end
    object QDivQtd1SEC_SECAO: TIBStringField
      FieldName = 'SEC_SECAO'
      Origin = 'CONTAGENS.SEC_SECAO'
      Size = 25
    end
    object QDivQtd1CON_ENDERECO: TIBStringField
      FieldName = 'CON_ENDERECO'
      Origin = 'CONTAGENS.CON_ENDERECO'
      Size = 25
    end
    object QDivQtd1CON_EAN: TIBStringField
      FieldName = 'CON_EAN'
      Origin = 'CONTAGENS.CON_EAN'
      Size = 35
    end
  end
  object RvDivQtd1: TRvDataSetConnection
    RuntimeVisibility = rtDeveloper
    DataSet = QDivQtd1
    Left = 328
    Top = 544
  end
  object RvDivSemSaldoCadastro: TRvDataSetConnection
    RuntimeVisibility = rtDeveloper
    DataSet = QDivSemSaldoCadastro
    Left = 406
    Top = 547
  end
  object QDivSemSaldoCadastro: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT CA.CAD_CODINT,'
      '       CA.CAD_EAN,'
      '       CO.QTD,'
      '       CA.CAD_SETOR,'
      '       CA.CAD_UN,'
      '       CA.CAD_DESC,'
      '       CA.CAD_PRECO,'
      '       SUM(CA.CAD_QUANTIDADE) AS CAD_QUANTIDADE,'
      '       (CASE'
      '        when ((CO.QTD - SUM(CA.CAD_QUANTIDADE)) IS NULL) then'
      '             ((0 - SUM(CA.CAD_QUANTIDADE)) * -1)'
      '        else'
      '             (CO.QTD - SUM(CA.CAD_QUANTIDADE)) end) as DIF_QTD,'
      '       (CASE'
      
        '        when ((CA.CAD_PRECO * (CO.QTD - SUM(CA.CAD_QUANTIDADE)))' +
        ' is null) then'
      '             ((CA.CAD_PRECO * (SUM(CA.CAD_QUANTIDADE))) * 1)'
      '        else'
      
        '            (CA.CAD_PRECO * (CO.QTD - SUM(CA.CAD_QUANTIDADE)) * ' +
        '1) end) as DIF_VALOR2'
      'from CONTAGENS_DIV CO'
      'INNER JOIN CADASTRO CA on (CA.CAD_CODINT = CO.CAD_CODINT)'
      'WHERE (CASE when ((CA.CAD_PRECO * CO.QTD) < 1) then'
      '                 ((CA.CAD_PRECO * CO.QTD) * -1)'
      '            else (CAD_PRECO * CO.QTD) end) >= :DIF_VALOR'
      'AND (CO.QTD IS NOT NULL)'
      'and (ca.cad_setor >=:cad_setor)'
      'and (ca.cad_setor <=:cad_setorfim)'
      'GROUP BY CA.CAD_CODINT,'
      '         CO.QTD,'
      '         CA.CAD_DESC,'
      '         CA.CAD_PRECO,'
      '         CA.CAD_SETOR,'
      '         CA.CAD_UN,'
      '         CA.CAD_EAN'
      'HAVING ((SUM(CA.CAD_QUANTIDADE)) = 0)'
      'ORDER BY CAD_SETOR, DIF_VALOR2 DESC'
      ''
      ''
      '')
    Left = 408
    Top = 547
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'DIF_VALOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'cad_setor'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'cad_setorfim'
        ParamType = ptUnknown
      end>
    object QDivSemSaldoCadastroCAD_CODINT: TIBStringField
      FieldName = 'CAD_CODINT'
      Origin = 'CADASTRO.CAD_CODINT'
    end
    object QDivSemSaldoCadastroCAD_EAN: TIBStringField
      FieldName = 'CAD_EAN'
      Origin = 'CADASTRO.CAD_EAN'
      Size = 25
    end
    object QDivSemSaldoCadastroQTD: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'QTD'
      Origin = 'CONTAGENS_DIV.QTD'
      ProviderFlags = []
      ReadOnly = True
      DisplayFormat = '##,###0.000'
    end
    object QDivSemSaldoCadastroCAD_SETOR: TIBStringField
      FieldName = 'CAD_SETOR'
      Origin = 'CADASTRO.CAD_SETOR'
    end
    object QDivSemSaldoCadastroCAD_UN: TIBStringField
      FieldName = 'CAD_UN'
      Origin = 'CADASTRO.CAD_UN'
      Size = 5
    end
    object QDivSemSaldoCadastroCAD_DESC: TIBStringField
      FieldName = 'CAD_DESC'
      Origin = 'CADASTRO.CAD_DESC'
      Size = 50
    end
    object QDivSemSaldoCadastroCAD_PRECO: TFloatField
      FieldName = 'CAD_PRECO'
      Origin = 'CADASTRO.CAD_PRECO'
      DisplayFormat = 'R$ ##,###0.00'
    end
    object QDivSemSaldoCadastroCAD_QUANTIDADE: TFloatField
      FieldName = 'CAD_QUANTIDADE'
      ProviderFlags = []
      DisplayFormat = '##,###0.000'
    end
    object QDivSemSaldoCadastroDIF_QTD: TFloatField
      FieldName = 'DIF_QTD'
      ProviderFlags = []
      DisplayFormat = '##,###0.000'
    end
    object QDivSemSaldoCadastroDIF_VALOR2: TFloatField
      FieldName = 'DIF_VALOR2'
      ProviderFlags = []
      DisplayFormat = 'R$ ##,###0.00'
    end
  end
  object QGeraArqFinalBVL: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select coalesce(co.con_ean, ca.cad_ean) as con_ean,'
      '       ca.cad_codint as cod_int,'
      '       coalesce(sum(co.con_quantidade),0) as qtd,'
      '       ca.cad_desc as descricao'
      '  from contagens co'
      ' inner join cadastro ca on (ca.cad_ean = co.con_ean)'
      ' where (co.con_status = '#39'.'#39' or co.con_status is null)'
      ' group by 1,2,4')
    Left = 688
    Top = 488
    object QGeraArqFinalBVLCON_EAN: TIBStringField
      FieldName = 'CON_EAN'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalBVLCOD_INT: TIBStringField
      FieldName = 'COD_INT'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalBVLQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
    object QGeraArqFinalBVLDESCRICAO: TIBStringField
      FieldName = 'DESCRICAO'
      ProviderFlags = []
      Size = 100
    end
  end
  object QGeraArqFinalCND: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select coalesce(co.con_ean, ca.cad_ean) as con_ean,'
      '       coalesce(sum(co.con_quantidade),0) as qtd_contagem,'
      '       ca.cad_preco,'
      
        '       round(coalesce(sum(co.con_quantidade),0)) as qtd_arredond' +
        'ada'
      '  from contagens co'
      'right join cadastro ca on (ca.cad_ean = co.con_ean)'
      ' where (co.con_status = '#39'.'#39' or co.con_status is null)'
      ' group by 1,3')
    Left = 736
    Top = 424
    object QGeraArqFinalCNDCON_EAN: TIBStringField
      FieldName = 'CON_EAN'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalCNDQTD_CONTAGEM: TFloatField
      FieldName = 'QTD_CONTAGEM'
      ProviderFlags = []
    end
    object QGeraArqFinalCNDCAD_PRECO: TFloatField
      FieldName = 'CAD_PRECO'
      Origin = 'CADASTRO.CAD_PRECO'
    end
    object QGeraArqFinalCNDQTD_ARREDONDADA: TFloatField
      FieldName = 'QTD_ARREDONDADA'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalKTX: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select cad_numloja,'
      'coalesce(co.con_ean, ca.cad_ean) as con_ean,'
      '          cad_desc, '
      '       coalesce(sum(co.con_quantidade),0) as qtd'
      '  from contagens co'
      ' right join cadastro ca on (ca.cad_ean = co.con_ean)'
      ' where (co.con_status = '#39'.'#39' or co.con_status is null)'
      ' group by 2,1,3')
    Left = 792
    Top = 488
    object QGeraArqFinalKTXCAD_NUMLOJA: TIBStringField
      FieldName = 'CAD_NUMLOJA'
      Origin = 'CADASTRO.CAD_NUMLOJA'
      Size = 10
    end
    object QGeraArqFinalKTXCON_EAN: TIBStringField
      FieldName = 'CON_EAN'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalKTXCAD_DESC: TIBStringField
      FieldName = 'CAD_DESC'
      Origin = 'CADASTRO.CAD_DESC'
      Size = 100
    end
    object QGeraArqFinalKTXQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalSLL: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select a.sec_secao,'
      '       b.con_ean as ean,'
      '       b.qtd,'
      '       a.Total_Sec'
      
        '  from (Select co1.sec_secao, coalesce(sum(co1.con_quantidade),0' +
        ') as Total_Sec'
      '          From contagens co1'
      '         where (co1.con_status = '#39'.'#39' or co1.con_status is null)'
      '         group by sec_secao) A,'
      '       (Select co2.sec_secao, co2.con_ean,'
      '               coalesce(sum(co2.con_quantidade),0) as qtd'
      '          From contagens co2'
      '          left join cadastro ca on (co2.con_ean = ca.cad_ean)'
      '         where (co2.con_status = '#39'.'#39' or co2.con_status is null)'
      '         group by sec_secao, con_ean) B'
      '  where a.sec_secao = b.sec_secao'
      '  group by 2,1,3,4'
      ' order by ean, sec_secao')
    Left = 520
    Top = 547
    object QGeraArqFinalSLLSEC_SECAO: TIBStringField
      FieldName = 'SEC_SECAO'
      ProviderFlags = []
      Size = 25
    end
    object QGeraArqFinalSLLEAN: TIBStringField
      FieldName = 'EAN'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalSLLQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
    object QGeraArqFinalSLLTOTAL_SEC: TFloatField
      FieldName = 'TOTAL_SEC'
      ProviderFlags = []
    end
  end
  object QVerificaSecao: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 100000
    CachedUpdates = False
    ParamCheck = True
    Left = 512
    Top = 64
  end
  object QGeraFinalCEA_Loja: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT SUM(C.CON_QUANTIDADE) AS QTD_LOJA'
      'FROM CONTAGENS C'
      
        'INNER JOIN MAPEAMENTO M ON (C.SEC_SECAO BETWEEN M.MAP_SECINI AND' +
        ' M.MAP_SECFIN)'
      'WHERE M.LOJA = '#39'X'#39
      'AND (C.CON_STATUS = '#39'.'#39
      '  OR C.CON_STATUS IS NULL)')
    Left = 620
    Top = 550
    object QGeraFinalCEA_LojaQTD_LOJA: TFloatField
      FieldName = 'QTD_LOJA'
      ProviderFlags = []
      DisplayFormat = '##,###0.000'
    end
  end
  object RvGeraFinalCEA_Loja: TRvDataSetConnection
    RuntimeVisibility = rtDeveloper
    DataSet = QGeraFinalCEA_Loja
    Left = 624
    Top = 550
  end
  object QGeraFinalCEA_DEP: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT SUM(C.CON_QUANTIDADE) AS QTD_DEPOSITO'
      'FROM CONTAGENS C'
      
        'INNER JOIN MAPEAMENTO M ON (C.SEC_SECAO BETWEEN M.MAP_SECINI AND' +
        ' M.MAP_SECFIN)'
      'WHERE M.DEPOSITO = '#39'X'#39
      'AND (C.CON_STATUS = '#39'.'#39
      '  OR C.CON_STATUS IS NULL)')
    Left = 740
    Top = 550
    object QGeraFinalCEA_DEPQTD_DEPOSITO: TFloatField
      FieldName = 'QTD_DEPOSITO'
      ProviderFlags = []
      DisplayFormat = '##,###0.000'
    end
  end
  object RvGeraFinalCEA_DEP: TRvDataSetConnection
    RuntimeVisibility = rtDeveloper
    DataSet = QGeraFinalCEA_DEP
    Left = 738
    Top = 550
  end
  object QTotalPecas_CEA: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT SUM(CO.CON_QUANTIDADE) AS QTD'
      ' FROM CONTAGENS CO'
      'WHERE (CO.CON_STATUS = '#39'.'#39
      '  OR CO.CON_STATUS IS NULL)')
    Left = 900
    Top = 491
    object QTotalPecas_CEAQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
      DisplayFormat = '##,###0.000'
    end
  end
  object RvTotalPecas_CEA: TRvDataSetConnection
    RuntimeVisibility = rtDeveloper
    DataSet = QTotalPecas_CEA
    Left = 898
    Top = 491
  end
  object QBuscaMapeamento: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      '  SELECT FIRST(1) mapeamento.map_desc'
      '    FROM mapeamento'
      '   WHERE :secao'
      ' BETWEEN mapeamento.map_secini_int'
      '     AND mapeamento.map_secfin_int')
    Left = 1027
    Top = 60
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'secao'
        ParamType = ptUnknown
      end>
    object QBuscaMapeamentoMAP_DESC: TIBStringField
      FieldName = 'MAP_DESC'
      Origin = 'MAPEAMENTO.MAP_DESC'
      Size = 30
    end
  end
  object QGeraArqFinalSMN: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT coalesce(CO.con_ean, ca.cad_ean) as ean ,'
      '       Coalesce(SUM(CO.CON_QUANTIDADE),0) AS QTD'
      '  FROM CONTAGENS CO'
      
        ' right JOIN CADASTRO CA ON (upper(CA.CAD_EAN) = upper(CO.CON_EAN' +
        '))'
      ' WHERE (CO.CON_STATUS = '#39'.'#39' or CO.CON_STATUS IS NULL)'
      ' GROUP BY cad_codint, ean'
      ' order by ean')
    Left = 512
    Top = 608
    object QGeraArqFinalSMNEAN: TIBStringField
      FieldName = 'EAN'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalSMNQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QProdutividade: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'Select t.Tra_coletor as Coletor,'
      '       substring(t.Tra_horario from 9 for 6) as Horario,'
      '       t.hor_Matricula as Matricula,'
      '       h.Ros_Nome as Conferente,'
      '       sum(c.con_quantidade) as Qtd,'
      '       c.sec_secao as Secao'
      '  From Contagens C'
      ' inner join transmissao t on t.tra_id = c.tra_id'
      ' inner join Horario H on t.hor_matricula = h.ros_matricula'
      ' group by t.Tra_coletor,'
      '          t.Tra_horario,'
      '          c.sec_secao,'
      '          t.hor_Matricula,'
      '          h.Ros_Nome'
      ' order by t.Tra_Coletor,'
      '          t.Tra_horario')
    Left = 894
    Top = 4
  end
  object QGeraArqFinalANL: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT co.con_ean as ean ,'
      '             SUM(CO.CON_QUANTIDADE) AS QTD'
      '  FROM CONTAGENS CO'
      ' WHERE (CO.CON_STATUS = '#39'.'#39' or CO.CON_STATUS IS NULL)'
      ' GROUP BY ean'
      ' ORDER BY ean')
    Left = 616
    Top = 608
    object QGeraArqFinalANLEAN: TIBStringField
      FieldName = 'EAN'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalANLQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalFRM: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT co.con_ean as ean ,'
      '             SUM(CO.CON_QUANTIDADE) AS QTD'
      '  FROM CONTAGENS CO'
      ' WHERE (CO.CON_STATUS = '#39'.'#39' or CO.CON_STATUS IS NULL)'
      ' GROUP BY ean'
      ' ORDER BY ean')
    Left = 719
    Top = 608
    object QGeraArqFinalFRMEAN: TIBStringField
      FieldName = 'EAN'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalFRMQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalFAB: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT co.con_ean as ean ,'
      '             SUM(CO.CON_QUANTIDADE) AS QTD'
      '  FROM CONTAGENS CO'
      ' WHERE (CO.CON_STATUS = '#39'.'#39' or CO.CON_STATUS IS NULL)'
      ' GROUP BY ean'
      ' ORDER BY ean')
    Left = 815
    Top = 608
    object QGeraArqFinalFABEAN: TIBStringField
      FieldName = 'EAN'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalFABQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalABR: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT co.con_ean as ean ,'
      '             SUM(CO.CON_QUANTIDADE) AS QTD'
      '  FROM CONTAGENS CO'
      ' WHERE (CO.CON_STATUS = '#39'.'#39' or CO.CON_STATUS IS NULL)'
      ' GROUP BY ean'
      ' ORDER BY ean')
    Left = 919
    Top = 608
    object QGeraArqFinalABREAN: TIBStringField
      FieldName = 'EAN'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalABRQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalFYI: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT co.con_ean as ean ,'
      '             SUM(CO.CON_QUANTIDADE) AS QTD'
      '  FROM CONTAGENS CO'
      ' WHERE (CO.CON_STATUS = '#39'.'#39' or CO.CON_STATUS IS NULL)'
      ' GROUP BY ean'
      ' ORDER BY ean')
    Left = 1023
    Top = 608
    object QGeraArqFinalFYIEAN: TIBStringField
      FieldName = 'EAN'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalFYIQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalMBZ_Dep: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT coalesce(co.con_ean,ca.cad_ean) as ean,'
      '             coalesce(sum(co.con_quantidade),0) as qtd,'
      '             ca.cad_numloja,'
      '             co.hor_matricula as matricula'
      '  FROM contagens co'
      ' INNER JOIN cadastro ca on (ca.cad_ean = co.con_ean)'
      ' WHERE (con_status =  '#39'.'#39' or con_status is null)'
      '   AND EXISTS (SELECT 1'
      '               FROM mapeamento m'
      '              WHERE co.sec_secao BETWEEN m.map_secini'
      '                and m.map_secfin'
      '                and m.deposito = '#39'X'#39')'
      ' GROUP BY 1,3,4')
    Left = 861
    Top = 550
    object QGeraArqFinalMBZ_DepEAN: TIBStringField
      FieldName = 'EAN'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalMBZ_DepQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
    object QGeraArqFinalMBZ_DepCAD_NUMLOJA: TIBStringField
      FieldName = 'CAD_NUMLOJA'
      Origin = 'CADASTRO.CAD_NUMLOJA'
      Size = 10
    end
    object QGeraArqFinalMBZ_DepMATRICULA: TIBStringField
      FieldName = 'MATRICULA'
      ProviderFlags = []
      Size = 11
    end
  end
  object QGeraArqFinalMBZ_Loja: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT coalesce(co.con_ean,ca.cad_ean) as ean,'
      '             coalesce(sum(co.con_quantidade),0) as qtd,'
      '             ca.cad_numloja,'
      '             co.hor_matricula as matricula'
      '  FROM contagens co'
      ' INNER JOIN cadastro ca on (ca.cad_ean = co.con_ean)'
      ' WHERE (con_status =  '#39'.'#39' or con_status is null)'
      '   AND EXISTS (SELECT 1'
      '               FROM mapeamento m'
      '              WHERE co.sec_secao BETWEEN m.map_secini'
      '                and m.map_secfin'
      '                and m.loja = '#39'X'#39')'
      ' GROUP BY 1,3,4')
    Left = 981
    Top = 550
    object QGeraArqFinalMBZ_LojaEAN: TIBStringField
      FieldName = 'EAN'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalMBZ_LojaQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
    object QGeraArqFinalMBZ_LojaCAD_NUMLOJA: TIBStringField
      FieldName = 'CAD_NUMLOJA'
      Origin = 'CADASTRO.CAD_NUMLOJA'
      Size = 10
    end
    object QGeraArqFinalMBZ_LojaMATRICULA: TIBStringField
      FieldName = 'MATRICULA'
      ProviderFlags = []
      Size = 11
    end
  end
  object QGeraArqFinalTUM: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select co.con_ean,'
      '       co.con_cadcodint,'
      '       ca.cad_preco,'
      '       sum(co.con_quantidade) as qtd,'
      '       co.sec_secao, '
      '       ca.cad_tipoestoque as tonalidade,'
      '       ca.cad_lote as desc_tonalidade,'
      '       ca.cad_grupocom as bitola,'
      '       ca.cad_numinv as desc_bitola,'
      '       ca.cad_digcodint as data_fab,'
      '       ca.cad_data as data_fabricacao,'
      '       ca.cad_exerc as ean_zero'
      '  from contagens co'
      ' inner join cadastro ca on (ca.cad_ean = co.con_ean)'
      ' where (co.con_status = '#39'.'#39' or co.con_status is null)'
      ' group by co.con_cadcodint,'
      '          co.con_ean,'
      '          co.sec_secao,'
      '          ca.cad_preco,'
      '          tonalidade,'
      '          desc_tonalidade,'
      '          bitola,'
      '          desc_bitola,'
      '          data_fab,'
      '          data_fabricacao,'
      '          ean_zero')
    Left = 844
    Top = 424
    object QGeraArqFinalTUMCON_EAN: TIBStringField
      FieldName = 'CON_EAN'
      Origin = 'CONTAGENS.CON_EAN'
      Size = 35
    end
    object QGeraArqFinalTUMCON_CADCODINT: TIBStringField
      FieldName = 'CON_CADCODINT'
      Origin = 'CONTAGENS.CON_CADCODINT'
      Size = 35
    end
    object QGeraArqFinalTUMCAD_PRECO: TFloatField
      FieldName = 'CAD_PRECO'
      Origin = 'CADASTRO.CAD_PRECO'
    end
    object QGeraArqFinalTUMQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
    object QGeraArqFinalTUMSEC_SECAO: TIBStringField
      FieldName = 'SEC_SECAO'
      Origin = 'CONTAGENS.SEC_SECAO'
      Size = 25
    end
    object QGeraArqFinalTUMTONALIDADE: TIBStringField
      FieldName = 'TONALIDADE'
      ProviderFlags = []
      Size = 3
    end
    object QGeraArqFinalTUMDESC_TONALIDADE: TIBStringField
      FieldName = 'DESC_TONALIDADE'
      ProviderFlags = []
      Size = 25
    end
    object QGeraArqFinalTUMBITOLA: TIBStringField
      FieldName = 'BITOLA'
      ProviderFlags = []
      Size = 3
    end
    object QGeraArqFinalTUMDESC_BITOLA: TIBStringField
      FieldName = 'DESC_BITOLA'
      ProviderFlags = []
    end
    object QGeraArqFinalTUMDATA_FAB: TIBStringField
      FieldName = 'DATA_FAB'
      ProviderFlags = []
      Size = 5
    end
    object QGeraArqFinalTUMDATA_FABRICACAO: TIBStringField
      FieldName = 'DATA_FABRICACAO'
      ProviderFlags = []
      Size = 8
    end
    object QGeraArqFinalTUMEAN_ZERO: TIBStringField
      FieldName = 'EAN_ZERO'
      ProviderFlags = []
      Size = 4
    end
  end
  object QGeraArqFinalMRT: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT co.con_ean as ean ,'
      '             co.sec_secao,'
      '             SUM(CO.CON_QUANTIDADE) AS QTD'
      '  FROM CONTAGENS CO'
      ' WHERE (CO.CON_STATUS = '#39'.'#39' or CO.CON_STATUS IS NULL)'
      ' GROUP BY ean, co.sec_secao'
      ' ORDER BY ean')
    Left = 1111
    Top = 560
    object QGeraArqFinalMRTEAN: TIBStringField
      FieldName = 'EAN'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalMRTSEC_SECAO: TIBStringField
      FieldName = 'SEC_SECAO'
      Origin = 'CONTAGENS.SEC_SECAO'
      Size = 25
    end
    object QGeraArqFinalMRTQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalLUB: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT co.con_ean as ean ,'
      '             SUM(CO.CON_QUANTIDADE) AS QTD'
      '  FROM CONTAGENS CO'
      ' WHERE (CO.CON_STATUS = '#39'.'#39' or CO.CON_STATUS IS NULL)'
      ' GROUP BY ean'
      ' ORDER BY ean')
    Left = 1119
    Top = 616
    object QGeraArqFinalLUBEAN: TIBStringField
      FieldName = 'EAN'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalLUBQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalLLB: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT co.con_ean as ean ,'
      '             SUM(CO.CON_QUANTIDADE) AS QTD'
      '  FROM CONTAGENS CO'
      ' WHERE (CO.CON_STATUS = '#39'.'#39' or CO.CON_STATUS IS NULL)'
      ' GROUP BY ean'
      ' ORDER BY ean')
    Left = 1191
    Top = 424
    object QGeraArqFinalLLBEAN: TIBStringField
      FieldName = 'EAN'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalLLBQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalMKR: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select  ca.cad_numloja, '
      '           ca.cad_setor as grupo_artigo,'
      '           co.con_ean, '
      '           co.con_cadcodint as codigo_artigo,'
      '           ca.cad_preco_custo * 1000 as custo,'
      '           (sum(co.con_quantidade) * 1000) as qtd         '
      '  from contagens co'
      '         inner join cadastro ca on (ca.cad_ean = co.con_ean)'
      '          where (co.con_status = '#39'.'#39' or co.con_status is null)'
      '           group by 1,2,3,4,5')
    Left = 1200
    Top = 480
    object QGeraArqFinalMKRGRUPO_ARTIGO: TIBStringField
      FieldName = 'GRUPO_ARTIGO'
      ProviderFlags = []
      Size = 3
    end
    object QGeraArqFinalMKRCON_EAN: TIBStringField
      FieldName = 'CON_EAN'
      Origin = 'CONTAGENS.CON_EAN'
      Size = 35
    end
    object QGeraArqFinalMKRCODIGO_ARTIGO: TIBStringField
      FieldName = 'CODIGO_ARTIGO'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalMKRCUSTO: TFloatField
      FieldName = 'CUSTO'
      ProviderFlags = []
    end
    object QGeraArqFinalMKRQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
    object QGeraArqFinalMKRCAD_NUMLOJA: TIBStringField
      FieldName = 'CAD_NUMLOJA'
      Origin = 'CADASTRO.CAD_NUMLOJA'
      Size = 10
    end
  end
  object QGeraArqFinalIDL: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT coalesce(CO.con_ean, ca.cad_ean) as ean ,'
      
        '             coalesce(co.con_cadcodint, ca.cad_codint) as intern' +
        'o,'
      '             ca.cad_preco as preco,'
      '             coalesce(SUM(CO.CON_QUANTIDADE),0) AS QTD'
      'FROM CONTAGENS CO'
      'right JOIN CADASTRO CA ON (CA.CAD_EAN = CO.CON_EAN)'
      'WHERE (CO.CON_STATUS = '#39'.'#39' or CO.CON_STATUS IS NULL)'
      'GROUP BY ean, interno, preco'
      'order by ean')
    Left = 1216
    Top = 552
    object QGeraArqFinalIDLEAN: TIBStringField
      FieldName = 'EAN'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalIDLINTERNO: TIBStringField
      FieldName = 'INTERNO'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalIDLPRECO: TFloatField
      FieldName = 'PRECO'
      ProviderFlags = []
    end
    object QGeraArqFinalIDLQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalLVL: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT CO.con_ean as ean ,'
      '             SUM(CO.CON_QUANTIDADE) AS QTD'
      'FROM CONTAGENS CO'
      'WHERE (CO.CON_STATUS = '#39'.'#39' or CO.CON_STATUS IS NULL)'
      'GROUP BY ean'
      'order by ean')
    Left = 1224
    Top = 608
    object QGeraArqFinalLVLEAN: TIBStringField
      FieldName = 'EAN'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalLVLQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalBHC: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT ca.cad_numloja, '
      
        '             coalesce(co.con_cadcodint, ca.cad_codint) as intern' +
        'o,'
      '             coalesce(SUM(CO.CON_QUANTIDADE),0) AS QTD,'
      '             ca.cad_lote,'
      '             ca.cad_preco_custo as multiplo_venda '
      'FROM CONTAGENS CO'
      'right JOIN CADASTRO CA ON (CA.CAD_EAN = CO.CON_EAN)'
      'WHERE (CO.CON_STATUS = '#39'.'#39' or CO.CON_STATUS IS NULL)'
      ' and ca.rdb$db_key = (select max(ca2.rdb$db_key)'
      '                        from cadastro ca2'
      '                       where ca2.cad_ean = co.con_ean)'
      'GROUP BY interno, ca.cad_lote, ca.cad_numloja, multiplo_venda'
      'order by interno')
    Left = 1088
    Top = 664
    object QGeraArqFinalBHCCAD_NUMLOJA: TIBStringField
      FieldName = 'CAD_NUMLOJA'
      Origin = 'CADASTRO.CAD_NUMLOJA'
      Size = 10
    end
    object QGeraArqFinalBHCINTERNO: TIBStringField
      FieldName = 'INTERNO'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalBHCQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
    object QGeraArqFinalBHCCAD_LOTE: TIBStringField
      FieldName = 'CAD_LOTE'
      Origin = 'CADASTRO.CAD_LOTE'
      Size = 25
    end
    object QGeraArqFinalBHCMULTIPLO_VENDA: TFloatField
      FieldName = 'MULTIPLO_VENDA'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalPMT: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select co.con_ean as ean ,'
      '       coalesce(sum(co.con_quantidade),0) as qtd'
      '  from contagens co'
      ' where (co.con_status = '#39'.'#39' or co.con_status is null)'
      ' group by ean'
      ' order by ean')
    Left = 24
    Top = 664
    object QGeraArqFinalPMTEAN: TIBStringField
      FieldName = 'EAN'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalPMTQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object IBImpCadastro: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'Select cad_numloja,'
      '           cad_ean,'
      '           cad_ean_orig,'
      '           cad_codint,'
      '           cad_desc,'
      '           cad_quantidade,'
      '           cad_preco,'
      '           cad_preco_custo as multiplo_venda,'
      '           cad_lote, '
      '           cad_numinv as SKU'
      'From cadastro'
      'where cad_numinv = :SKU'
      '')
    Left = 1115
    Top = 124
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'SKU'
        ParamType = ptUnknown
      end>
    object IBImpCadastroCAD_EAN: TIBStringField
      FieldName = 'CAD_EAN'
      Origin = 'CADASTRO.CAD_EAN'
      Size = 35
    end
    object IBImpCadastroCAD_EAN_ORIG: TIBStringField
      FieldName = 'CAD_EAN_ORIG'
      Origin = 'CADASTRO.CAD_EAN_ORIG'
      Size = 35
    end
    object IBImpCadastroCAD_CODINT: TIBStringField
      FieldName = 'CAD_CODINT'
      Origin = 'CADASTRO.CAD_CODINT'
      Size = 35
    end
    object IBImpCadastroCAD_DESC: TIBStringField
      FieldName = 'CAD_DESC'
      Origin = 'CADASTRO.CAD_DESC'
      Size = 100
    end
    object IBImpCadastroCAD_QUANTIDADE: TFloatField
      FieldName = 'CAD_QUANTIDADE'
      Origin = 'CADASTRO.CAD_QUANTIDADE'
    end
    object IBImpCadastroCAD_PRECO: TFloatField
      FieldName = 'CAD_PRECO'
      Origin = 'CADASTRO.CAD_PRECO'
    end
    object IBImpCadastroMULTIPLO_VENDA: TFloatField
      FieldName = 'MULTIPLO_VENDA'
      Origin = 'CADASTRO.CAD_PRECO_CUSTO'
    end
    object IBImpCadastroCAD_LOTE: TIBStringField
      FieldName = 'CAD_LOTE'
      Origin = 'CADASTRO.CAD_LOTE'
      Size = 25
    end
    object IBImpCadastroCAD_NUMLOJA: TIBStringField
      FieldName = 'CAD_NUMLOJA'
      Origin = 'CADASTRO.CAD_NUMLOJA'
      Size = 10
    end
    object IBImpCadastroSKU: TIBStringField
      FieldName = 'SKU'
      Origin = 'CADASTRO.CAD_NUMINV'
    end
  end
  object QGeraArqFinalPSL: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT co.con_ean as ean ,'
      '             SUM(CO.CON_QUANTIDADE) AS QTD'
      '  FROM CONTAGENS CO'
      ' WHERE (CO.CON_STATUS = '#39'.'#39' or CO.CON_STATUS IS NULL)'
      ' GROUP BY ean'
      ' ORDER BY ean')
    Left = 127
    Top = 664
    object QGeraArqFinalPSLEAN: TIBStringField
      FieldName = 'EAN'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalPSLQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object IBControleSecCont: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'Select coalesce(count(distinct(sec_secao)),0) as sec_contada'
      '  From secao'
      ' where ((sec_status is null) or (sec_status = '#39'.'#39'))')
    Left = 423
    Top = 664
    object IBControleSecContSEC_CONTADA: TIntegerField
      FieldName = 'SEC_CONTADA'
      ProviderFlags = []
    end
  end
  object IBControleSecAudit: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'Select coalesce(count(1),0) as sec_auditada'
      '  From secao'
      ' where sec_auditada = '#39'S'#39
      '   and ((sec_status is null) or (sec_status = '#39'.'#39'))')
    Left = 527
    Top = 664
    object IBControleSecAuditSEC_AUDITADA: TIntegerField
      FieldName = 'SEC_AUDITADA'
      ProviderFlags = []
    end
  end
  object IBControlePecasCont: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'Select coalesce(sum(con_quantidade),0) as pecas_contadas'
      '  From contagens'
      ' where ((con_status is null) or (con_status = '#39'.'#39'))')
    Left = 639
    Top = 664
    object IBControlePecasContPECAS_CONTADAS: TFloatField
      FieldName = 'PECAS_CONTADAS'
      ProviderFlags = []
    end
  end
  object IBControlePecasAudit: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'Select coalesce(sum(con_quantidade),0) as pecas_auditadas'
      '  From secao s'
      ' inner join contagens c'
      '    on (s.sec_secao = c.sec_secao)'
      '  and sec_auditada = '#39'S'#39
      '   and ((c.con_status = '#39'.'#39') or (c.con_status is null))')
    Left = 751
    Top = 664
    object IBControlePecasAuditPECAS_AUDITADAS: TFloatField
      FieldName = 'PECAS_AUDITADAS'
      ProviderFlags = []
    end
  end
  object IBControleSecCorrigida: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'Select coalesce(count(distinct(c.Sec_secao)),0) as sec_corrigida'
      '  From contagens c'
      '  inner join secao s on (c.sec_secao = s.sec_secao)'
      '    and c.con_quantidadeoriginal <> c.con_quantidade'
      '     and ((s.sec_status is null) or (s.sec_status = '#39'.'#39'))'
      '     and s.sec_auditada = '#39'S'#39
      '')
    Left = 863
    Top = 664
    object IBControleSecCorrigidaSEC_CORRIGIDA: TIntegerField
      FieldName = 'SEC_CORRIGIDA'
      ProviderFlags = []
    end
  end
  object RvControleSecCont: TRvDataSetConnection
    RuntimeVisibility = rtDeveloper
    DataSet = IBControleSecCont
    Left = 419
    Top = 707
  end
  object RvControleSecAudit: TRvDataSetConnection
    RuntimeVisibility = rtDeveloper
    DataSet = IBControleSecAudit
    Left = 531
    Top = 707
  end
  object RvControlePecasCont: TRvDataSetConnection
    RuntimeVisibility = rtDeveloper
    DataSet = IBControlePecasCont
    Left = 643
    Top = 707
  end
  object RvControlePecasAudit: TRvDataSetConnection
    RuntimeVisibility = rtDeveloper
    DataSet = IBControlePecasAudit
    Left = 771
    Top = 707
  end
  object RvControleSecCorrigida: TRvDataSetConnection
    RuntimeVisibility = rtDeveloper
    DataSet = IBControleSecCorrigida
    Left = 867
    Top = 707
  end
  object IBControleTotalErros: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'Select count(c.con_bsi) as BSI,'
      '       count(c.con_cliente) as cliente,'
      '       (count(c.con_bsi) / count(1)) as porcent_BSI,'
      '       (count(c.con_cliente) / count(1)) as porcent_Cliente'
      '  From Contagens c'
      '  inner join secao s on (c.sec_secao = s.sec_secao)'
      '   and c.con_quantidade <> c.con_quantidadeoriginal'
      
        '   and ((s.sec_status <> '#39'DEL'#39') or (s.sec_status = '#39'.'#39') or (s.se' +
        'c_status is null))'
      '   and s.sec_auditada = '#39'S'#39)
    Left = 973
    Top = 664
    object IBControleTotalErrosBSI: TIntegerField
      FieldName = 'BSI'
      ProviderFlags = []
    end
    object IBControleTotalErrosCLIENTE: TIntegerField
      FieldName = 'CLIENTE'
      ProviderFlags = []
    end
    object IBControleTotalErrosPORCENT_BSI: TFloatField
      FieldName = 'PORCENT_BSI'
      ProviderFlags = []
      DisplayFormat = ' ##,###0.000'
    end
    object IBControleTotalErrosPORCENT_CLIENTE: TFloatField
      FieldName = 'PORCENT_CLIENTE'
      ProviderFlags = []
      DisplayFormat = ' ##,###0.000'
    end
  end
  object RvControleTotalErros: TRvDataSetConnection
    RuntimeVisibility = rtDeveloper
    DataSet = IBControleTotalErros
    Left = 977
    Top = 707
  end
  object QGeraArqFinalSET: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT co.con_ean as ean ,'
      '             SUM(CO.CON_QUANTIDADE) AS QTD'
      '  FROM CONTAGENS CO'
      ' inner join cadastro ca on (co.con_ean = ca.cad_ean)'
      '   and (CO.CON_STATUS = '#39'.'#39' or CO.CON_STATUS IS NULL)'
      ' GROUP BY ean'
      ' ORDER BY ean')
    Left = 223
    Top = 664
    object QGeraArqFinalSETEAN: TIBStringField
      FieldName = 'EAN'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalSETQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalSEP: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT co.con_Cadcodint as interno,'
      '             ca.cad_desc as descricao, '
      '             SUM(CO.CON_QUANTIDADE) AS QTD'
      '  FROM CONTAGENS CO'
      ' inner join cadastro ca on (co.con_ean = ca.cad_ean)'
      '   and (CO.CON_STATUS = '#39'.'#39' or CO.CON_STATUS IS NULL)'
      ' GROUP BY interno, descricao'
      ' ORDER BY interno')
    Left = 317
    Top = 664
    object QGeraArqFinalSEPINTERNO: TIBStringField
      FieldName = 'INTERNO'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalSEPDESCRICAO: TIBStringField
      FieldName = 'DESCRICAO'
      ProviderFlags = []
      Size = 100
    end
    object QGeraArqFinalSEPQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalADA: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'Select c.con_cadcodint as interno,'
      '       sum(c.con_quantidade) as qtd,'
      '       c.sec_secao as secao'
      '  From contagens c'
      '  left join cadastro ca on (c.con_ean = ca.cad_ean)'
      ' where (c.con_status is null)'
      '    or (c.con_status = '#39'.'#39')'
      '    and exists (select 1'
      '                  From secao s'
      '                 where s.sec_secao = c.sec_secao)'
      '  group by c.con_cadcodint,'
      '                 c.sec_secao'
      'union'
      'Select cad_codint as interno,'
      '       0 as qtd,'
      '       '#39' '#39' as secao'
      '  From cadastro ca'
      ' where not exists (select 1'
      '                     From contagens c'
      '                    where ca.cad_ean = c.con_ean)'
      '  group by ca.cad_codint')
    Left = 40
    Top = 722
    object QGeraArqFinalADAQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
    object QGeraArqFinalADASECAO: TIBStringField
      FieldName = 'SECAO'
      ProviderFlags = []
      Size = 25
    end
    object QGeraArqFinalADAINTERNO: TIBStringField
      FieldName = 'INTERNO'
      ProviderFlags = []
      Size = 35
    end
  end
  object QGeraArqFinalADA_Loja: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select c.con_cadcodint as interno,'
      '       sum(c.con_quantidade) as qtd_loja,'
      '       c.sec_secao '
      'from contagens c'
      'inner join cadastro ca'
      '        on c.con_ean = ca.cad_ean'
      'WHERE (c.con_status = '#39'.'#39
      '   or c.con_status is null)'
      '  and EXISTS (select 1'
      '               from mapeamento m'
      '              WHERE c.sec_secao Between m.map_secini'
      '                                    and m.map_secfin'
      '                and m.loja = '#39'X'#39')'
      'group by c.con_cadcodint,'
      '               c.sec_secao ')
    Left = 120
    Top = 722
    object QGeraArqFinalADA_LojaQTD_LOJA: TFloatField
      FieldName = 'QTD_LOJA'
      ProviderFlags = []
    end
    object QGeraArqFinalADA_LojaSEC_SECAO: TIBStringField
      FieldName = 'SEC_SECAO'
      Origin = 'CONTAGENS.SEC_SECAO'
      Size = 25
    end
    object QGeraArqFinalADA_LojaINTERNO: TIBStringField
      FieldName = 'INTERNO'
      ProviderFlags = []
      Size = 35
    end
  end
  object QGeraArqFinalADA_Dep: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select D.CON_CADCODINT as interno,'
      '       SUM(D.CON_QUANTIDADE) AS QTD,'
      '       d.sec_secao'
      'from CONTAGENS D'
      'inner JOIN CADASTRO C ON (C.CAD_EAN = D.CON_EAN)'
      '  where (CON_STATUS =  '#39'.'#39' or CON_STATUS is Null)'
      '   and EXISTS (select 1'
      '               from mapeamento m'
      '              WHERE d.sec_secao Between m.map_secini'
      '                                    and m.map_secfin'
      '                and m.deposito = '#39'X'#39')'
      ' group by d.CON_CADCODINT,'
      '                d.sec_Secao')
    Left = 208
    Top = 722
    object QGeraArqFinalADA_DepQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
    object QGeraArqFinalADA_DepSEC_SECAO: TIBStringField
      FieldName = 'SEC_SECAO'
      Origin = 'CONTAGENS.SEC_SECAO'
      Size = 25
    end
    object QGeraArqFinalADA_DepINTERNO: TIBStringField
      FieldName = 'INTERNO'
      ProviderFlags = []
      Size = 35
    end
  end
  object QGeraArqFinalSSP_Dep: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select D.CON_EAN,'
      '         SUM(D.CON_QUANTIDADE) AS QTD'
      'from CONTAGENS D'
      'inner JOIN CADASTRO C ON (C.CAD_EAN = D.CON_EAN)'
      '  where (CON_STATUS =  '#39'.'#39' or CON_STATUS is Null)'
      '   and EXISTS (select 1'
      '               from mapeamento m'
      '              WHERE d.sec_secao Between m.map_secini'
      '                                    and m.map_secfin'
      '                and m.deposito = '#39'X'#39')'
      ' group by 1')
    Left = 1136
    Top = 728
    object QGeraArqFinalSSP_DepCON_EAN: TIBStringField
      FieldName = 'CON_EAN'
      Origin = 'CONTAGENS.CON_EAN'
      Size = 35
    end
    object QGeraArqFinalSSP_DepQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalSSP_LOJA: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select c.con_ean,'
      '       sum(c.con_quantidade) as qtd'
      'from contagens c'
      'inner join cadastro ca'
      '        on c.con_ean = ca.cad_ean'
      'WHERE (c.con_status = '#39'.'#39
      '   or c.con_status is null)'
      '  and EXISTS (select 1'
      '               from mapeamento m'
      '              WHERE c.sec_secao Between m.map_secini'
      '                                    and m.map_secfin'
      '                and m.loja = '#39'X'#39')'
      'group by 1')
    Left = 1245
    Top = 729
    object QGeraArqFinalSSP_LOJACON_EAN: TIBStringField
      FieldName = 'CON_EAN'
      Origin = 'CONTAGENS.CON_EAN'
      Size = 35
    end
    object QGeraArqFinalSSP_LOJAQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalNSD: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT co.con_ean as ean,'
      '             co.con_cadcodint as interno,'
      '             SUM(CO.CON_QUANTIDADE) AS QTD,'
      '             co.sec_secao as secao   '
      'FROM CONTAGENS CO '
      '  inner join cadastro ca on (co.con_ean = ca.cad_ean)'
      'WHERE (CO.CON_STATUS = '#39'.'#39' or CO.CON_STATUS IS NULL)'
      'GROUP BY ean,'
      '                  interno,'
      '                  secao'
      'order by ean')
    Left = 1184
    Top = 664
    object QGeraArqFinalNSDEAN: TIBStringField
      FieldName = 'EAN'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalNSDINTERNO: TIBStringField
      FieldName = 'INTERNO'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalNSDQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
    object QGeraArqFinalNSDSECAO: TIBStringField
      FieldName = 'SECAO'
      ProviderFlags = []
      Size = 25
    end
  end
  object QGeraArqFinalPET: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select co.con_cadcodint as interno ,'
      '       coalesce(sum(co.con_quantidade),0) as qtd'
      '  from contagens co'
      ' inner join cadastro ca on (ca.cad_codint = co.con_cadcodint)'
      ' where (co.con_status = '#39'.'#39' or co.con_status is null)'
      '           and ca.rdb$db_key = (select max(ca2.rdb$db_key)'
      '                                  from cadastro ca2'
      
        '                                 where ca2.cad_codint = co.con_c' +
        'adcodint)'
      ' group by interno'
      '-- order by interno'
      'union'
      'Select cad_codint as interno,'
      '       0 as qtd'
      '  From cadastro'
      ' where not exists (Select 1'
      '                     From contagens'
      '                     where con_cadcodint = cad_codint'
      '                       and con_status = '#39'.'#39')')
    Left = 312
    Top = 728
    object QGeraArqFinalPETINTERNO: TIBStringField
      FieldName = 'INTERNO'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalPETQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalPET_Ini: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select co.con_cadcodint as interno,'
      '               co.con_ean as ean,'
      '               ca.cad_Desc as descricao,'
      '               ca.cad_preco as preco,'
      '               coalesce(sum(co.con_quantidade),0) as qtd,'
      '               (Select sum(con_quantidade)'
      '                 From contagens co1'
      '                where co1.con_cadcodint = co.con_cadcodint'
      
        '                  and (co1.con_status = '#39'.'#39') or (co1.con_status ' +
        'is null)) as qtd_int'
      '          from contagens co'
      
        '         right join cadastro ca on (ca.cad_codint = co.con_cadco' +
        'dint)'
      '         where (co.con_status = '#39'.'#39') or (co.con_status is null)'
      '           and ca.rdb$db_key = (select max(ca2.rdb$db_key)'
      '                                  from cadastro ca2'
      
        '                                 where ca2.cad_codint = co.con_c' +
        'adcodint)'
      '         group by 1,2,3,4'
      '      --   order by interno'
      'union'
      'Select cad_codint as interno,'
      '       cad_ean as ean,'
      '       cad_Desc as descricao,'
      '       cad_preco as preco,'
      '       0 as qtd,'
      '       0 as qtd_int'
      '  From cadastro'
      ' where not exists (Select 1'
      '                     From contagens'
      '                     where con_ean = cad_ean'
      '                       and con_status = '#39'.'#39')')
    Left = 432
    Top = 752
    object QGeraArqFinalPET_IniINTERNO: TIBStringField
      FieldName = 'INTERNO'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalPET_IniEAN: TIBStringField
      FieldName = 'EAN'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalPET_IniDESCRICAO: TIBStringField
      FieldName = 'DESCRICAO'
      ProviderFlags = []
      Size = 100
    end
    object QGeraArqFinalPET_IniPRECO: TFloatField
      FieldName = 'PRECO'
      ProviderFlags = []
    end
    object QGeraArqFinalPET_IniQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
    object QGeraArqFinalPET_IniQTD_INT: TFloatField
      FieldName = 'QTD_INT'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalLNG: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select co.con_ean as ean ,'
      '       coalesce(sum(co.con_quantidade),0) as qtd,'
      '       ca.cad_desc'
      '  from contagens co '
      'inner join cadastro ca on (co.con_ean = ca.cad_ean)'
      ' where (co.con_status = '#39'.'#39' or co.con_status is null)'
      ' group by ean, ca.cad_desc'
      ' order by ean')
    Left = 552
    Top = 752
    object QGeraArqFinalLNGEAN: TIBStringField
      FieldName = 'EAN'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalLNGQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
    object QGeraArqFinalLNGCAD_DESC: TIBStringField
      FieldName = 'CAD_DESC'
      Origin = 'CADASTRO.CAD_DESC'
      Size = 100
    end
  end
  object QGeraArqFinalFMS: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'Select con_ean,'
      '       sum(con_quantidade) as qtd'
      '  From contagens'
      ' inner join cadastro on (cad_ean = con_ean)'
      ' group by con_ean'
      ' order by con_ean')
    Left = 656
    Top = 760
    object QGeraArqFinalFMSCON_EAN: TIBStringField
      FieldName = 'CON_EAN'
      Origin = 'CONTAGENS.CON_EAN'
      Size = 35
    end
    object QGeraArqFinalFMSQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalRPL: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'Select con_ean,'
      '       sum(con_quantidade) as qtd'
      '  From contagens'
      ' inner join cadastro on (cad_ean = con_ean)'
      ' group by con_ean'
      ' order by con_ean')
    Left = 760
    Top = 760
    object QGeraArqFinalRPLCON_EAN: TIBStringField
      FieldName = 'CON_EAN'
      Origin = 'CONTAGENS.CON_EAN'
      Size = 35
    end
    object QGeraArqFinalRPLQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalSED: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'Select ca.cad_numloja,'
      '       coalesce(co.con_ean, ca.cad_ean) as ISBN,'
      '      coalesce(co.con_cadcodint, ca.cad_codint) as produto,'
      '       coalesce(co.con_deposito, ca.cad_deposito) as endereco,'
      '       ca.cad_Desc as descricao,'
      '       sum(coalesce(co.con_quantidade, 0)) as qtd'
      '  From contagens co'
      ' right join cadastro ca on (ca.cad_ean = co.con_ean)'
      ' WHERE CON_STATUS =  '#39'.'#39
      '    OR CON_STATUS is Null'
      ' group by endereco, produto, ISBN, descricao, ca.cad_numloja')
    Left = 24
    Top = 773
    object QGeraArqFinalSEDCAD_NUMLOJA: TIBStringField
      FieldName = 'CAD_NUMLOJA'
      Origin = 'CADASTRO.CAD_NUMLOJA'
      Size = 10
    end
    object QGeraArqFinalSEDISBN: TIBStringField
      FieldName = 'ISBN'
      ProviderFlags = []
      Size = 25
    end
    object QGeraArqFinalSEDPRODUTO: TIBStringField
      FieldName = 'PRODUTO'
      ProviderFlags = []
    end
    object QGeraArqFinalSEDENDERECO: TIBStringField
      FieldName = 'ENDERECO'
      ProviderFlags = []
      Size = 25
    end
    object QGeraArqFinalSEDDESCRICAO: TIBStringField
      FieldName = 'DESCRICAO'
      ProviderFlags = []
      Size = 100
    end
    object QGeraArqFinalSEDQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalSED_Int: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'Select ca.cad_numloja,'
      '       coalesce(co.con_ean, ca.cad_ean) as ISBN,'
      '       coalesce(co.con_cadcodint, ca.cad_codint) as produto,'
      '       ca.cad_Desc as descricao,'
      '       sum(coalesce(co.con_quantidade, 0)) as qtd'
      '  From contagens co'
      ' inner join cadastro ca on (ca.cad_ean = co.con_ean)'
      ' WHERE CON_STATUS =  '#39'.'#39
      '    OR CON_STATUS is Null'
      ' group by produto, ISBN, descricao, ca.cad_numloja')
    Left = 136
    Top = 773
    object QGeraArqFinalSED_IntCAD_NUMLOJA: TIBStringField
      FieldName = 'CAD_NUMLOJA'
      Origin = 'CADASTRO.CAD_NUMLOJA'
      Size = 10
    end
    object QGeraArqFinalSED_IntISBN: TIBStringField
      FieldName = 'ISBN'
      ProviderFlags = []
      Size = 25
    end
    object QGeraArqFinalSED_IntPRODUTO: TIBStringField
      FieldName = 'PRODUTO'
      ProviderFlags = []
    end
    object QGeraArqFinalSED_IntDESCRICAO: TIBStringField
      FieldName = 'DESCRICAO'
      ProviderFlags = []
      Size = 100
    end
    object QGeraArqFinalSED_IntQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalCPS: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'Select con_ean,'
      '          con_deposito,'
      '       sum(con_quantidade) as qtd'
      '  From contagens'
      ' inner join cadastro on (cad_ean = con_ean)'
      ' group by con_ean, con_deposito'
      ' order by con_ean')
    Left = 1160
    Top = 787
    object QGeraArqFinalCPSCON_EAN: TIBStringField
      FieldName = 'CON_EAN'
      Origin = 'CONTAGENS.CON_EAN'
      Size = 35
    end
    object QGeraArqFinalCPSQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
    object QGeraArqFinalCPSCON_DEPOSITO: TIBStringField
      FieldName = 'CON_DEPOSITO'
      Origin = 'CONTAGENS.CON_DEPOSITO'
      Size = 25
    end
  end
  object QGeraArqFinalANC: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select coalesce(co.con_ean, ca.cad_ean) as ean ,'
      '       coalesce(co.con_cadcodint, ca.cad_codint) as interno,'
      '           coalesce(sum(co.con_quantidade),0) as qtd,'
      '           ca.cad_desc as desc,'
      '           ca.cad_numinv as handle'
      '  from contagens co '
      'right join cadastro ca on (co.con_ean = ca.cad_ean)'
      ' where (co.con_status = '#39'.'#39' or co.con_status is null)'
      ' group by interno, ean, ca.cad_desc, ca.cad_numinv'
      ' order by ca.cad_numinv')
    Left = 864
    Top = 781
    object QGeraArqFinalANCEAN: TIBStringField
      FieldName = 'EAN'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalANCINTERNO: TIBStringField
      FieldName = 'INTERNO'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalANCQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
    object QGeraArqFinalANCDESC: TIBStringField
      FieldName = 'DESC'
      ProviderFlags = []
      Size = 100
    end
    object QGeraArqFinalANCHANDLE: TIBStringField
      FieldName = 'HANDLE'
      ProviderFlags = []
    end
  end
  object QDeletaDupl: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'delete from Cadastro'
      'where cad_ean in'
      '  (select cad_ean from cadastro'
      '   group by cad_ean'
      '   having Count(cad_ean) >1)')
    Left = 976
    Top = 757
    object IBStringField4: TIBStringField
      FieldName = 'EAN'
      ProviderFlags = []
      Size = 35
    end
    object IBStringField5: TIBStringField
      FieldName = 'INTERNO'
      ProviderFlags = []
      Size = 35
    end
    object FloatField2: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
    object IBStringField6: TIBStringField
      FieldName = 'DESC'
      ProviderFlags = []
      Size = 100
    end
    object IBStringField7: TIBStringField
      FieldName = 'HANDLE'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalPNB: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 50
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'Select con_ean,'
      '       sum(con_quantidade) as qtd'
      '  From contagens'
      ' inner join cadastro on cad_ean = con_ean'
      '  and ((con_status = '#39'.'#39') or (con_status is null))'
      ' group by con_ean'
      ' order by con_ean')
    Left = 1048
    Top = 784
    object QGeraArqFinalPNBCON_EAN: TIBStringField
      FieldName = 'CON_EAN'
      Origin = 'CONTAGENS.CON_EAN'
      Size = 35
    end
    object QGeraArqFinalPNBQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalOUT: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'Select con_ean,'
      '           sum(con_quantidade) as Qtd'
      '  From contagens'
      ' inner join cadastro on (cad_ean = con_ean)'
      ' group by con_ean'
      ' order by con_ean')
    Left = 1256
    Top = 787
    object QGeraArqFinalOUTCON_EAN: TIBStringField
      FieldName = 'CON_EAN'
      Origin = 'CONTAGENS.CON_EAN'
      Size = 35
    end
    object QGeraArqFinalOUTQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalDSP: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select'
      '   (CASE'
      '     when co.con_cadcodint is null then'
      '          ca.cad_codint'
      '         else'
      '          co.con_cadcodint end) as INTERNO,'
      '   (CASE'
      '     when MAX(CO.con_ean) is null then'
      '          MAX(ca.cad_ean)'
      '         else'
      '          MAX(CO.con_ean) end) as EAN,'
      '       ca.cad_desc,'
      '   (CASE'
      '     when SUM(co.con_quantidade) is null then'
      '          0'
      '         else'
      '          SUM(co.con_quantidade) end) as QTD'
      'FROM CONTAGENS CO'
      'left JOIN CADASTRO CA ON (CA.CAD_EAN = CO.CON_EAN)'
      'WHERE CO.con_status = '#39'.'#39' or CO.con_status is null'
      'GROUP by 1,3')
    Left = 312
    Top = 800
    object QGeraArqFinalDSPINTERNO: TIBStringField
      FieldName = 'INTERNO'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalDSPEAN: TIBStringField
      FieldName = 'EAN'
      ProviderFlags = []
      Size = 35
    end
    object QGeraArqFinalDSPCAD_DESC: TIBStringField
      FieldName = 'CAD_DESC'
      Origin = 'CADASTRO.CAD_DESC'
      Size = 100
    end
    object QGeraArqFinalDSPQTD: TFloatField
      FieldName = 'QTD'
      ProviderFlags = []
    end
  end
  object QGeraArqFinalSAR2: TIBQuery
    Database = DM.Conexao
    Transaction = DM.Transacao
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      
        'select C.CAD_CODINT, D.SEC_SECAO,  C.CAD_EAN, sum(D.CON_QUANTIDA' +
        'DE)'
      'from CONTAGENS D'
      'inner JOIN CADASTRO_CONSO C ON (C.CAD_EAN = D.CON_EAN)'
      
        '  where (d.sec_secao between 9901 and 9950) and CON_STATUS =  '#39'.' +
        #39' or CON_STATUS is Null'
      '  GROUP BY 1,3,2')
    Left = 672
    Top = 64
    object QGeraArqFinalSAR2CAD_CODINT: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'CAD_CODINT'
      Origin = 'CADASTRO_CONSO.CAD_CODINT'
      ProviderFlags = []
      ReadOnly = True
      Size = 35
    end
    object QGeraArqFinalSAR2SEC_SECAO: TIBStringField
      FieldName = 'SEC_SECAO'
      Origin = 'CONTAGENS.SEC_SECAO'
      Size = 25
    end
    object QGeraArqFinalSAR2CAD_EAN: TIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'CAD_EAN'
      Origin = 'CADASTRO_CONSO.CAD_EAN'
      ProviderFlags = []
      ReadOnly = True
      Size = 35
    end
    object QGeraArqFinalSAR2SUM: TFloatField
      FieldName = 'SUM'
      ProviderFlags = []
      ReadOnly = True
    end
  end
end
