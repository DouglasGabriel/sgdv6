unit UInventario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, Menus, ExtCtrls, Grids, DBGrids, StdCtrls, Buttons,
  Mask, DBCtrls, ShellApi, TeeProcs, TeEngine, Chart, DbChart, Series,
  ImgList, AppEvnts, MidasLib, sSkinManager, sEdit, RpDefine, RpRender,
  RpRenderPDF, RpRenderText, sBitBtn, DBClient, VclTee.TeeGDIPlus, sGauge,
  Data.DB, System.ImageList;

type
  TFrmInventario = class(TForm)
    MainMenu1: TMainMenu;
    Opes1: TMenuItem;
    Fechar1: TMenuItem;
    Timer1: TTimer;
    DesabilitarimportaoAutomatica1: TMenuItem;
    N1: TMenuItem;
    Relatrios1: TMenuItem;
    FinalizarInventrio1: TMenuItem;
    RelatriosdeAuditoria1: TMenuItem;
    Panel10: TPanel;
    GeraArquivoFinal1: TMenuItem;
    RelatrioTotaldePeaseFinanceiro1: TMenuItem;
    xtcomtotaldeHorasdosConferente1: TMenuItem;
    Ajuda1: TMenuItem;
    AtalhosdoSistema1: TMenuItem;
    RelatriodePsicount1: TMenuItem;
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Label11: TLabel;
    DBText1: TDBText;
    Label13: TLabel;
    DBText4: TDBText;
    Label12: TLabel;
    DBText3: TDBText;
    DBText2: TDBText;
    Panel2: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel3: TPanel;
    Panel4: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    Panel15: TPanel;
    DBChart1: TDBChart;
    Series1: TPieSeries;
    TabSheet2: TTabSheet;
    Panel6: TPanel;
    Panel9: TPanel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    Label14: TLabel;
    DBGrid6: TDBGrid;
    DBGrid7: TDBGrid;
    BitBtn3: TBitBtn;
    Edit4: TEdit;
    TabSheet3: TTabSheet;
    Panel7: TPanel;
    Panel8: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel11: TPanel;
    DBGrid1: TDBGrid;
    TabSheet4: TTabSheet;
    Panel12: TPanel;
    DBGrid2: TDBGrid;
    TabSheet5: TTabSheet;
    Panel13: TPanel;
    DBGrid3: TDBGrid;
    Panel16: TPanel;
    TabSheet7: TTabSheet;
    Panel17: TPanel;
    RelatriodeDuplicidades1: TMenuItem;
    RelatriodeSeesAbertas1: TMenuItem;
    DBGrid4: TDBGrid;
    RelatriodeSeesInesperadas1: TMenuItem;
    ImageList1: TImageList;
    RelatriodeItensAlteradosdepoisdedescarregados1: TMenuItem;
    ApplicationEvents1: TApplicationEvents;
    BitBtn4: TBitBtn;
    GerarDivergncia1: TMenuItem;
    RelatriodeProdutosNoCadastrados1: TMenuItem;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    EdtHoras: TsEdit;
    EdtProdutividade: TsEdit;
    EdtQtde: TsEdit;
    Label18: TLabel;
    EdtPessoas: TsEdit;
    Label19: TLabel;
    DBEdit8: TDBEdit;
    RvPDF: TRvRenderPDF;
    RvTxt: TRvRenderText;
    RelatriodeCdigosEANePLU1: TMenuItem;
    ransmitirBancodeDados1: TMenuItem;
    Timer2: TTimer;
    sBitBtn1: TsBitBtn;
    sBitBtn2: TsBitBtn;
    RelatriodeSetorConsolidado1: TMenuItem;
    DBGrid5: TDBGrid;
    CheckBox1: TCheckBox;
    Geratxtconfernciaseo1: TMenuItem;
    Label20: TLabel;
    RelatriodeEndereosnocadastrados1: TMenuItem;
    Label21: TLabel;
    RelatriodeSeesDeletadas1: TMenuItem;
    Label22: TLabel;
    Edit5: TEdit;
    RelatriodeRuptura1: TMenuItem;
    GerarDivergnciaQuantidade1: TMenuItem;
    RelatriodeAuditoriaForada1: TMenuItem;
    ProgressBarTrans: TProgressBar;
    GerarArquivodeTransmisses1: TMenuItem;
    mniRelatriodeQuantidades11: TMenuItem;
    btnHoraGlobal: TBitBtn;
    RelatriodeAuditoriaFaixadeQuantidade1: TMenuItem;
    LabelTotalPecas: TLabel;
    LabelTotalFin: TLabel;
    Rel_Acu_Colaborador: TMenuItem;
    RelatriodeAuditoriaFaixadeQuantidadeSEOEAN1: TMenuItem;
    btnImport: TSpeedButton;
    Label23: TLabel;
    Label24: TLabel;
    DBEdit9: TDBEdit;
    Relatriodeprodutospareados1: TMenuItem;
    RelatriodeAuditoriaSint1: TMenuItem;
    GerarArquivoTcnicoOnofre1: TMenuItem;
    Produtividade1: TMenuItem;
    RelatriodeAcuracia1: TMenuItem;
    PProgress: TPanel;
    gauge: TsGauge;
    Label25: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure Edit1Exit(Sender: TObject);
    procedure Edit2Exit(Sender: TObject);
    procedure TabSheet3Show(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure TabSheet5Show(Sender: TObject);
    procedure Fechar1Click(Sender: TObject);
    procedure DBGrid5KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure BitBtn3Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure FinalizarInventrio1Click(Sender: TObject);
    procedure GeraArquivoFinal1Click(Sender: TObject);
    procedure DBGrid3KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure RelatrioTotaldePeaseFinanceiro1Click(Sender: TObject);
    procedure xtcomtotaldeHorasdosConferente1Click(Sender: TObject);
    procedure TabSheet4Show(Sender: TObject);
    procedure RelatriosdeAuditoria1Click(Sender: TObject);
    procedure AtalhosdoSistema1Click(Sender: TObject);
    procedure TabSheet1Show(Sender: TObject);
    procedure DesabilitarimportaoAutomatica1Click(Sender: TObject);
    procedure RelatriodePsicount1Click(Sender: TObject);
    procedure TabSheet2Show(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure RelatriodeDuplicidades1Click(Sender: TObject);
    procedure RelatriodeSeesAbertas1Click(Sender: TObject);
    procedure _S(Sender: TObject; var Key: Char);
    procedure DBGrid3KeyPress(Sender: TObject; var Key: Char);
    procedure Edit4Change(Sender: TObject);
    procedure TabSheet7Show(Sender: TObject);
    procedure RelatriodeSeesInesperadas1Click(Sender: TObject);
    procedure DBGrid3CellClick(Column: TColumn);
    procedure DBGrid5CellClick(Column: TColumn);
    procedure RelatriodeItensAlteradosdepoisdedescarregados1Click(Sender: TObject);
    procedure DBGrid5ColExit(Sender: TObject);
    procedure TabSheet7Exit(Sender: TObject);
    procedure DBGrid5Exit(Sender: TObject);
    procedure ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);
    procedure BitBtn4Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure GerarDivergncia1Click(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure RelatriodeProdutosNoCadastrados1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure RelatriodeCdigosEANePLU1Click(Sender: TObject);
    procedure DBGrid6DrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure DBGrid7DrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure DBGrid3DrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure DBGrid4DrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure DBGrid5DrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure ransmitirBancodeDados1Click(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure sBitBtn2Click(Sender: TObject);
    procedure RelatriodeSetorConsolidado1Click(Sender: TObject);
    procedure RelatriodeAuditoriaConsolidado1Click(Sender: TObject);
    procedure DBGrid5TitleClick(Column: TColumn);
    procedure DBGrid2TitleClick(Column: TColumn);
    procedure DBGrid3TitleClick(Column: TColumn);
    procedure CheckBox1Click(Sender: TObject);
    procedure Geratxtconfernciaseo1Click(Sender: TObject);
    procedure RelatriodeEndereosnocadastrados1Click(Sender: TObject);
    procedure RelatriodeSeesDeletadas1Click(Sender: TObject);
    procedure DBGrid3MouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure DBGrid1MouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure Edit5Change(Sender: TObject);
    procedure RelatriodeRuptura1Click(Sender: TObject);
    procedure GerarDivergnciaQuantidade1Click(Sender: TObject);
    procedure RelatriodeAuditoriaForada1Click(Sender: TObject);
    procedure DBGrid7CellClick(Column: TColumn);
    procedure DBGrid7KeyPress(Sender: TObject; var Key: Char);
    procedure Edit4KeyPress(Sender: TObject; var Key: Char);
    procedure Edit5KeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure StatusBar1DrawPanel(StatusBar: TStatusBar; Panel: TStatusPanel; const Rect: TRect);
    procedure GerarArquivodeTransmisses1Click(Sender: TObject);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure mniRelatriodeQuantidades11Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnHoraGlobalClick(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure Edit2Change(Sender: TObject);
    procedure RelatriodeAuditoriaFaixadeQuantidade1Click(Sender: TObject);
    procedure Rel_Acu_ColaboradorClick(Sender: TObject);
    procedure DBGrid6KeyPress(Sender: TObject; var Key: Char);
    procedure DBGrid6KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure RelatriodeAuditoriaFaixadeQuantidadeSEOEAN1Click(Sender: TObject);
    procedure btnImportClick(Sender: TObject);
    procedure DBEdit9Click(Sender: TObject);
    procedure DBEdit9Exit(Sender: TObject);
    procedure TabSheet1Exit(Sender: TObject);
    procedure DBGrid1KeyPress(Sender: TObject; var Key: Char);
    procedure Relatriodeprodutospareados1Click(Sender: TObject);
    procedure RelatriodeAuditoriaSint1Click(Sender: TObject);
    procedure GerarArquivoTcnicoOnofre1Click(Sender: TObject);
    procedure Produtividade1Click(Sender: TObject);
    procedure RelatriodeAcuracia1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }

    click: Boolean;

  end;

var
  FrmInventario: TFrmInventario;
  IDSECAO: Integer;
  IDCONTAGEM: Integer;
  Cod: Integer;
  LastSearch: string;
  LastItem: Integer;
  ClicadoGridMapX: Integer;
  sec: Boolean;

  // SecaoGlobal: String[4];
  // TransIdGlobal: Integer;
  // SecIdGlobal: Integer;
implementation

uses UDm, UFuncProc, ULogin, uHorario, uFinalizaInvent, UPesquisa,
  USelecionaSecao, IBCustomDataSet, Math, UAguarde, UDivergencia, UCadInv,
  UFrmProdutividade, UFrmUpdate, Udm2, UImportaCad, U_RelEAN, URuptura,
  UDivergenciaQtd, UFrmHorGlobal, UFrmProdQuant, URelAuditQtdSecEan,
  UFrmAuditQtdSecEanReal, U_Rel_AuditSint, UTotalNCadastrado;

{$R *.dfm}

procedure TFrmInventario.BitBtn1Click(Sender: TObject);
Begin
  Edit2.Enabled := True;

  If Length(Edit1.Text) < 4 then // Verifica se os campos n�o est�o em Branco
  begin
    Application.MessageBox('A se��o inicial deve ter no m�nimo 4 digitos', 'Mensagem do Sistema', MB_ICONERROR + MB_OK);
  end
  else If Length(Edit2.Text) < 4 then // Verifica se os campos n�o est�o em Branco
  begin
    Application.MessageBox('A se��o final deve ter no m�nimo 4 digitos', 'Mensagem do Sistema', MB_ICONERROR + MB_OK);
  end
  else
  begin
    If ((Edit1.Text <> '') and (Edit2.Text <> '')) then // Verifica se os campos n�o est�o em Branco
    Begin
      DM.qrySQL.Close;
      DM.qrySQL.SQL.Clear;
      DM.qrySQL.SQL.Add('SELECT * FROM LAYOUT_INPUT WHERE CLIENTE = :CLIENTE');
      DM.qrySQL.ParamByName('CLIENTE').AsString := DM.IBInventarioINV_LOJA.Value;
      DM.qrySQL.Open;

      // if DM.qrySQL.FieldByName('ENDERECO').AsString = 'T' then
      // begin
      // Try
      // Timer1.Enabled := False;
      // AbreTeladeAguarde('Aguarde!!! Cadastrando Se��es');
      // CadastraMapeamentoEnd(Edit1.Text, Edit2.Text, Edit3.Text);
      // Finally
      // Timer1.Enabled := True;
      // FechaTeladeAguarde;
      // End;
      // end
      // else
      // begin
      If (Edit1.Text <= Edit2.Text) and ((strtoint(Edit1.Text) >= 0000) and (strtoint(Edit2.Text) >= 0000)) then
      // Verifica se Se��o Final � Maior que Se��o Inicial
      Begin
        Try
          Timer1.Enabled := False;
          AbreTeladeAguarde('Aguarde!!! Cadastrando Se��es');

          CadastraMapeamento(AjustaStrEsq(Edit1.Text, 4, '0'), AjustaStrEsq(Edit2.Text, 4, '0'), Edit3.Text);
        Finally
          Timer1.Enabled := True;
          FechaTeladeAguarde;
        End;
      end
      Else // Caso Se��o Final � Maior que Se��o Inicial
        Application.MessageBox('A se��o inicial deve ser menor que a se��o final ', 'Mensagem do Sistema',
          MB_ICONERROR + MB_OK);
      // end;

    End
    Else
      Application.MessageBox('� obrigat�rio o preenchimento do valor na Se��o Incial e Final', 'Mensagem do Sistema',
        MB_ICONERROR + MB_OK);
  end;
  // Zerar campo
  Edit1.Text := '';
  Edit2.Text := '';
  Edit3.Text := '';
  Edit1.SetFocus;
  DM.IBMapeamento.Close;
  DM.IBMapeamento.ParamByName('SecIni').Value := '0';
  DM.IBMapeamento.ParamByName('SecFin').Value := 'ZZZZZZZZZZZZZZZZZZZZZZZZZ';
  DM.IBMapeamento.Open;
  DM.IBMapeamento.First;
  DM.IBSecao.Close;
  DM.IBSecao.Open;
End;

procedure TFrmInventario.BitBtn2Click(Sender: TObject);
begin
  if DM.IBMapeamento.RecordCount > 0 then
  Begin
    Try
      AbreTeladeAguarde('Aguarde!!! Excluindo Se��es Abertas');
      DeletaSecao(DM.IBMapeamentoMAP_SECINI.Value, DM.IBMapeamentoMAP_SECFIN.Value);
      DM.IBMapeamento.Delete;
      DM.Transacao.CommitRetaining;
      PosicionaSecao(IDSECAO);
    Finally
      FechaTeladeAguarde;
    End;
  End;
end;

procedure TFrmInventario.Edit1Exit(Sender: TObject);
begin
  DM.qrySQL.Close;
  DM.qrySQL.SQL.Clear;
  DM.qrySQL.SQL.Add('SELECT * FROM LAYOUT_INPUT WHERE CLIENTE = :CLIENTE');
  DM.qrySQL.ParamByName('CLIENTE').AsString := DM.IBInventarioINV_LOJA.Value;
  DM.qrySQL.Open;

  // if DM.qrySQL.FieldByName('ENDERECO').AsString <> 'T' then
  // begin
  If IntegerIsStr(Edit1.Text) then
  Begin
    Edit1.Text := '';
    Edit1.SetFocus;
    Application.MessageBox('� obrigat�rio o preenchimento do valor na Se��o', 'Mensagem do Sistema',
      MB_ICONERROR + MB_OK);
  End;
  // end
  // else
  // begin
  // Edit2.Text := Edit1.Text;
  // Edit2.Enabled := False;
  // end;

End;

procedure TFrmInventario.Edit2Exit(Sender: TObject);
begin
  DM.qrySQL.Close;
  DM.qrySQL.SQL.Clear;
  DM.qrySQL.SQL.Add('SELECT * FROM LAYOUT_INPUT WHERE CLIENTE = :CLIENTE');
  DM.qrySQL.ParamByName('CLIENTE').AsString := DM.IBInventarioINV_LOJA.Value;
  DM.qrySQL.Open;

  // if DM.qrySQL.FieldByName('ENDERECO').AsString <> 'T' then
  // begin
  If IntegerIsStr(Edit2.Text) then
  Begin
    Edit2.Text := '';
    Edit2.SetFocus;
    Application.MessageBox('S� � permitido abrir se��es com o valores num�ricos', 'Mensagem do Sistema',
      MB_ICONERROR + MB_OK);
  End;
  // end;
End;

procedure TFrmInventario.TabSheet3Show(Sender: TObject);
begin
  DBGrid1.Refresh;
  if Panel7.Enabled = True then
    Edit1.SetFocus;
end;

procedure TFrmInventario.Timer1Timer(Sender: TObject);
Var
  Origem, Origem2, Destino1, Destino2: String;
  TransID, OS: String;
  Cont: Integer;
  DataHora, NomedaTransmissao1, NomedaTransmissao2, NomeArqBackup: String;
  S, T: TFileStream;
  SR: TSearchRec;
  I: Integer;
  Local1, Local2: string;
Begin
  Timer1.Enabled := False; // Desativa o Timer para n�o rodar ao mesmo tempo que j� estiver rodando
  Cont := 1650;

  // Corre da pasta 1650 a 2999 dentro do diret�rio 'C:\SGD\IGT_CE\IgtCeSinc\' para ver qual pasta existe
  // if Cont <= 2999 then
  // begin
  // Corre da pasta 1650 a 2999 dentro do diret�rio 'C:\SGD\IGT_CE\IgtCeSinc\' para ver qual pasta existe
  While Cont <= 2999 Do
  Begin
    Origem := 'C:\SGD\IGT_CE\IgtCeSinc\' + AjustaStrEsq(inttostr(Cont), 4, '0') + '\invent.txt';
    // Origem do arquivo "invent.txt"

    // Verica se o arquivo existe dentro da pasta
    If FileExists(Origem) then // Caso exista o arquivo
    Begin
      AbreTeladeAguarde('Aguarde!!! Importando Transmiss�o do Coletor ' + inttostr(Cont) + '.');
      Try // ##01## Para verificar algum problema na transmiss�o do arquivo para pasta C:\SGD\Transmissao\

        // Copia arquivo da pasta 'C:\SGD\IGT_CE\IgtCeSinc\XXXX' para 'C:\SGD\Transmissao\'
        S := TFileStream.Create(Origem, fmOpenRead);

        // Gera ID da Transmiss�o
        Try
          DM.QCriaTransID.Open;
          TransID := AjustaStrEsq(inttostr(DM.QCriaTransIDGEN_ID.Value), 5, '0');
          DM.Transacao.CommitRetaining;
          DM.QCriaTransID.Close;
        Except
          Application.MessageBox('Problema na gera��o do Id da transmiss�o', 'Mensagem do Sistema',
            MB_ICONERROR + MB_OK);
        End;

        // N�mero da OS
        OS := DM.IBInventarioINV_OS.Value;

        Destino1 := 'C:\SGD\Transmissao\Trans_' + TransID + '_OS' + OS + '.txt'; // Destino para onde ser� copiado
        Try // ##02##
          T := TFileStream.Create(Destino1, fmOpenWrite or fmCreate);
          Try // ##03##
            T.CopyFrom(S, S.Size)
          Finally // ##03##
            T.Free
          End
          /// /##03## Finaliza 2� Try
        Finally // ##02##
          S.Free
        End;
        /// /##02## Finaliza 1� Try
      Except // ##01##
        Application.MessageBox('N�o foi copiado o arquivo para pasta "C:\SGD\Transmissao\" contate o suporte',
          'Mensagem do Sistema', MB_ICONERROR + MB_OK);
      End; // ##01##

      Try
        // Verifica se existe o diret�rios 'I:\N� OS'', sen�o � criado
        Try // ##04## Try para verifica se ocorre erro na cria��o do diret�rio dentro do PEN-DRIVE
          If not FileExists('I:\BKP_SGD\OS_' + DM.IBInventarioINV_OS.Value) then // Caso exista o arquivo
            CreateDir('I:\OS_' + DM.IBInventarioINV_OS.Value);
        Except // ##04##
          Application.MessageBox('N�o foi poss�vel criar o diret�rio dentro PEN-DRIVE', 'Mensagem do Sistema',
            MB_ICONERROR + MB_OK);
        End; // ##04##
      Finally
        // Copia arquivo da pasta 'C:\SGD\IGT_CE\IgtCeSinc\XXXX' para 'I:\N� OS''
        Try // ##05## Try para verificar se ocorre problema na copia do arquivo para o PEN-DRIVE
          S := TFileStream.Create(Origem, fmOpenRead);
          Destino2 := 'I:\OS_' + DM.IBInventarioINV_OS.Value + '\Trans_' + TransID + '_OS' + OS + '.txt';
          // Destino para onde ser� copiado
          Try // ##06##
            T := TFileStream.Create(Destino2, fmOpenWrite or fmCreate);
            Try // ##07##
              T.CopyFrom(S, S.Size)
            Finally // ##07##
              T.Free
            End // ##07##
          Finally // ##06##
            S.Free
          End; // ##06##
        Except // ##05##
          Application.MessageBox
            ('Houve problema na copia da transmiss�o para o PEN-DRIVE. Essa transmiss�o ser� importada, ' + #13 +
            'por�m voc� est� sem o backup completo das transmiss�es. Colocar o PEN-DRIVE e copiar da pasta ' + #13 +
            'c:\SGD\Transmiss�es todas as transmiss�es, para caso ocorra algum porblema com o PC',
            'Mensagem do Sistema', MB_ICONEXCLAMATION + MB_OK);
        End; // ##05##
      End;

      Try // ##10##
        // Depois de copiado para pasta transmiss�o renomeio arquivo invent.txt da pasta do IGT e apaga o invent.txt
        // Pega Data e Hora da Transmiss�o do Arquivo
        DataHora := RetirarBarras(Retirar2Pontos(RetiraSpace(DataHoraArquivo(Origem))));
        NomeArqBackup := Copy(Origem, 1, 35) + '_' + DataHora + '.txt';
        RenameFile(Origem, NomeArqBackup);
        DeleteFile(Origem);
      Except // ##10##
        Application.MessageBox('N�o foi poss�vel deletar o invent.txt da pasta do IGT', 'Mensagem do Sistema',
          MB_ICONERROR + MB_OK);
      End; // ##10##

      Try // ##11##
        // Chama Fun��o que Importa os dados para para as tabelas
        IF DBEdit2.Text = 'CLB' then
          ImportaDadosCLB(Destino1, Cont)
        else IF DBEdit2.Text = 'GST' then
          ImportaDadosGST(Destino1, Cont)
        else IF DBEdit2.Text = 'CTD' then
          ImportaDadosCTD(Destino1, Cont)
        else IF ((DBEdit2.Text = 'ONF') or (DBEdit2.Text = 'OEL')) then
          ImportaDadosONF(Destino1, Cont)
        else IF DBEdit2.Text = 'OCD' then
          ImportaDadosOCD(Destino1, Cont)
        else IF DBEdit2.Text = 'ONP' then
          ImportaDadosONP(Destino1, Cont)
        else IF DBEdit2.Text = 'SCH' then
          ImportaDadosSCH(Destino1, Cont)
        else IF (DBEdit2.Text = 'PDA') or (DBEdit2.Text = 'ASA') then
          ImportaDadosPDA(Destino1, Cont)
        else IF (DBEdit2.Text = 'CP1') then
          ImportaDadosCP1(Destino1, Cont)
        else IF (DBEdit2.Text = 'CP2') then
          ImportaDadosCP2(Destino1, Cont)
        else IF (DBEdit2.Text = 'CP3') then
          ImportaDadosCP3(Destino1, Cont)
        else IF (DBEdit2.Text = 'LVC') then
          ImportaDadosLVC(Destino1, Cont)
        else IF (DBEdit2.Text = 'SAR') or (DBEdit2.Text = 'SRR') then
          ImportaDadosSAR(Destino1, Cont)
        else IF (DBEdit2.Text = 'TNT') then
          ImportaDadosTNT(Destino1, Cont)
        else if (DBEdit2.Text = 'DIA') then
          ImportaDadosDIA(Destino1, Cont)
        else if (DBEdit2.Text = 'LMD') then
          ImportaDadosLMD(Destino1, Cont)
        else IF DBEdit2.Text = 'FRN' then
          ImportaDadosFRN(Destino1, Cont)
        else IF DBEdit2.Text = 'SAF' then
          ImportaDadosSAF(Destino1, Cont)
        else IF DBEdit2.Text = 'RLD' then
          ImportaDadosRLD(Destino1, Cont)
        else IF DBEdit2.Text = 'AND' then
          ImportaDadosAND(Destino1, Cont)
        else IF DBEdit2.Text = 'MRS' then
          ImportaDadosMRS(Destino1, Cont)
        else IF DBEdit2.Text = 'CEA' then
          ImportaDadosCEA(Destino1, Cont)
        else IF DBEdit2.Text = 'PRN' then
          ImportaDadosPRN(Destino1, Cont)
        else IF DBEdit2.Text = 'WIS' then
          ImportaDadosWIS(Destino1, Cont)
        else IF DBEdit2.Text = 'PET' then
          ImportaDadosPET(Destino1, Cont)
        else IF DBEdit2.Text = 'MRG' then
          ImportaDadosMRG(Destino1, Cont)
        else IF DBEdit2.Text = 'CIC' then
          ImportaDadosCIC(Destino1, Cont)
        else IF DBEdit2.Text = 'SMP' then
          ImportaDadosSMP(Destino1, Cont)
        else IF DBEdit2.Text = 'SMK' then
          ImportaDadosSMP(Destino1, Cont)
        else IF DBEdit2.Text = 'SMD' then
          ImportaDadosSMP(Destino1, Cont)
        else IF DBEdit2.Text = 'VLG' then
          ImportaDadosVLG(Destino1, Cont)
        else IF DBEdit2.Text = 'DLG' then
          ImportaDadosDLG(Destino1, Cont)
        else IF DBEdit2.Text = 'LOP' then
          ImportaDadosLOP(Destino1, Cont)
        else IF DBEdit2.Text = 'TMC' then
          ImportaDadosTMC(Destino1, Cont)
        else IF DBEdit2.Text = 'NFE' then
          ImportaDadosNFE(Destino1, Cont)
        else IF DBEdit2.Text = 'BAR' then
          ImportaDadosBAR(Destino1, Cont)
        else IF DBEdit2.Text = 'MAX' then
          ImportaDadosMAX(Destino1, Cont)
        else IF DBEdit2.Text = 'SEB' then
          ImportaDadosSEB(Destino1, Cont)
        else IF DBEdit2.Text = 'YKS' then
          ImportaDadosYKS(Destino1, Cont)
        else IF DBEdit2.Text = 'ROS' then
          ImportaDadosROS(Destino1, Cont)
        else IF DBEdit2.Text = 'BGB' then
          ImportaDadosBGB(Destino1, Cont)
        else IF DBEdit2.Text = 'WAL' then
          ImportaDadosWAL(Destino1, Cont)
        else IF DBEdit2.Text = 'BBL' then
          ImportaDadosBBL(Destino1, Cont)
        else IF DBEdit2.Text = 'GLM' then
          ImportaDadosGLM(Destino1, Cont)
        else IF DBEdit2.Text = 'CLS' then
          ImportaDadosCLS(Destino1, Cont)
        else IF DBEdit2.Text = 'ALX' then
          ImportaDadosALX(Destino1, Cont)
        else IF DBEdit2.Text = 'PLT' then
          ImportaDadosPLT(Destino1, Cont)
        else IF DBEdit2.Text = 'CBT' then
          ImportaDadosCBT(Destino1, Cont)
        else IF DBEdit2.Text = 'MRD' then
          ImportaDadosMRD(Destino1, Cont)
        else IF DBEdit2.Text = 'NCL' then
          ImportaDadosNCL(Destino1, Cont)
        else IF DBEdit2.Text = 'LOC' then
          ImportaDadosLOC(Destino1, Cont)
        else IF DBEdit2.Text = 'GRL' then
          ImportaDadosGRL(Destino1, Cont)
        else if DBEdit2.Text = 'NTR' then
          ImportaDadosNTR(Destino1, Cont)
        else if DBEdit2.Text = 'EDR' then
          ImportaDadosEDR(Destino1, Cont)
        else if DBEdit2.Text = 'DFY' then
          ImportaDadosDFY(Destino1, Cont)
        else if DBEdit2.Text = 'BVL' then
          ImportaDadosBVL(Destino1, Cont)
        else if DBEdit2.Text = 'CND' then
          ImportaDadosCND(Destino1, Cont)
        else if DBEdit2.Text = 'SLL' then
          ImportaDadosSLL(Destino1, Cont)
        else if DBEdit2.Text = 'SMN' then
          ImportaDadosSMN(Destino1, Cont)
        else if DBEdit2.Text = 'ANL' then
          ImportaDadosANL(Destino1, Cont)
        else if DBEdit2.Text = 'FRM' then
          ImportaDadosANL(Destino1, Cont)
        else if DBEdit2.Text = 'FAB' then
          ImportaDadosFAB(Destino1, Cont)
        else if DBEdit2.Text = 'ABR' then
          ImportaDadosABR(Destino1, Cont)
        else if DBEdit2.Text = 'FYI' then
          ImportaDadosFYI(Destino1, Cont)
        else if DBEdit2.Text = 'MBZ' then
          ImportaDadosMBZ(Destino1, Cont)
        else if (DBEdit2.Text = 'LUB') or (DBEdit2.Text = 'EME') then
          ImportaDadosLUB(Destino1, Cont)
        else if (DBEdit2.Text = 'LLB') or (DBEdit2.Text = 'BBO') or (DBEdit2.Text = 'JHJ') or
                (DBEdit2.Text = 'NOR') or (DBEdit2.Text = 'RSC') Then
          ImportaDadosLLB(Destino1, Cont)
        else if (DBEdit2.Text = 'MKR') then
          ImportaDadosMKR(Destino1, Cont)
        else if (DBEdit2.Text = 'LVL') then
          ImportaDadosLVL(Destino1, Cont)
        else if (DBEdit2.Text = 'BHC') then
          ImportaDadosBHC(Destino1, Cont)
        else if (DBEdit2.Text = 'PMT') then
          ImportaDadosPMT(Destino1, Cont)
        else if (DBEdit2.Text = 'GDS') then
          ImportaDadosGDS(Destino1, Cont)
        else if (DBEdit2.Text = 'PSL') then
          ImportaDadosPSL(Destino1, Cont)
        else if (DBEdit2.Text = 'SET') then
          ImportaDadosSET(Destino1, Cont)
        else if (DBEdit2.Text = 'SEP') then
          ImportaDadosSEP(Destino1, Cont)
        else if (DBEdit2.Text = 'RCH') then
          ImportaDadosRCH(Destino1, Cont)
        else if (DBEdit2.Text = 'MCP') then
          ImportaDadosMCP(Destino1, Cont)
        else if (DBEdit2.Text = 'MCF') then
          ImportaDadosMCF(Destino1, Cont)
        else if (DBEdit2.Text = 'FMS') then
          ImportaDadosFMS(Destino1, Cont)
        else if (DBEdit2.Text = 'TNG') then
          ImportaDadosTNG(Destino1, Cont)
        else if (DBEdit2.Text = 'SED') then
          ImportaDadosSED(Destino1, Cont)
        else if (DBEdit2.Text = 'ANC') then
          ImportaDadosANC(Destino1, Cont)
        else if (DBEdit2.Text = 'PNB') then
          ImportaDadosPNB(Destino1, Cont)
        else if (DBEdit2.Text = 'CPS') then
          ImportaDadosCPS(Destino1, Cont)
        else if (DBEdit2.Text = 'DSP') then
          ImportaDadosDSP(Destino1, Cont)
        else if (DBEdit2.Text = 'OUT') then
          ImportaDadosOUT(Destino1, Cont)
       // else if (DBEdit2.Text = 'NSD') then
       //   ImportaDadosNSD(Destino1, Cont)
        else
        begin
          ImportaDados(Destino1, Cont);
        end;
      Except // ##11##
        Application.MessageBox
          ('N�o foi poss�vel chamar a fun��o que importa os dados para o SGD, POR FAVOR TRANSMITA NOVAMENTE!!',
          'Mensagem do Sistema', MB_ICONERROR + MB_OK);
      End; // ##11##
      FechaTeladeAguarde;
    End; // Fechar o If Caso encontre a pasta

    { //Fecha o While que corre as pastas para ver se encontra o arquivo
      Origem2 := 'Computador\Coletor\\\TT09313\Invent.txt';
      If FileExists(Origem2) then;
      begin
      Origem2 := 'Computador\Coletor\\\TT09313\Invent.txt';;
      CreateDir('C:\SGD\IGT_CE\IgtCeSinc\2000');
      Local2  := 'C:\SGD\IGT_CE\IgtCeSinc\2000\invent.txt';
      if not CopyFile(PChar(Origem2), PChar(Local2), true) then
      ShowMessage('Erro ao copiar ' + Origem2 + ' para ' + Local2);
      end;
      Origem2 := 'Computador\WindowsCE\\\TT09313\Invent.txt';
      If FileExists(Origem2) then; //Origem do arquivo SGDMOBILE.) then //Caso exista o arquivo
      Begin
      CreateDir('C:\SGD\IGT_CE\IgtCeSinc\2000');
      CopyDir(Origem2,'C:\SGD\IGT_CE\IgtCeSinc\2000');
      AbreTeladeAguarde('Aguarde!!! Importando Transmiss�o do Coletor ' + inttostr(Cont) + '.');
      Try // ##01## Para verificar algum problema na transmiss�o do arquivo para pasta C:\SGD\Transmissao\

      //Copia arquivo da pasta 'C:\SGD\IGT_CE\IgtCeSinc\XXXX' para 'C:\SGD\Transmissao\'
      S := TFileStream.Create(Origem2, fmOpenRead);

      //Gera ID da Transmiss�o
      Try
      Dm.QCriaTransID.Open;
      TransID := AjustaStrEsq(inttostr(Dm.QCriaTransIDGEN_ID.Value),5,'0');
      Dm.Transacao.CommitRetaining;
      Dm.QCriaTransID.Close;
      Except
      Application.MessageBox('Problema na gera��o do Id da transmiss�o','Mensagem do Sistema', MB_ICONERROR+ MB_OK);
      End;

      //N�mero da OS
      OS := Dm.IBInventarioINV_OS.Value;

      Destino1 := 'C:\SGD\Transmissao\Trans_' + TransID + '_OS' + OS + '.txt' ;   //Destino para onde ser� copiado
      Try //##02##
      T := TFileStream.Create( Destino1, fmOpenWrite or fmCreate );
      Try //##03##
      T.CopyFrom(S, S.Size )
      Finally //##03##
      T.Free
      End ////##03## Finaliza 2� Try
      Finally //##02##
      S.Free
      End;////##02## Finaliza 1� Try
      Except //##01##
      Application.MessageBox('N�o foi copiado o arquivo para pasta "C:\SGD\Transmissao\" contate o suporte','Mensagem do Sistema', MB_ICONERROR+ MB_OK);
      End; //##01##

      Try
      //Verifica se existe o diret�rios 'I:\N� OS'', sen�o � criado
      Try //##04## Try para verifica se ocorre erro na cria��o do diret�rio dentro do PEN-DRIVE
      If not FileExists('I:\BKP_SGD\OS_' + Dm.IBInventarioINV_OS.Value) then //Caso exista o arquivo
      CreateDir('I:\OS_' + Dm.IBInventarioINV_OS.Value);
      Except//##04##
      Application.MessageBox('N�o foi poss�vel criar o diret�rio dentro PEN-DRIVE','Mensagem do Sistema', MB_ICONERROR+ MB_OK);
      End;//##04##
      Finally
      //Copia arquivo da pasta 'C:\SGD\IGT_CE\IgtCeSinc\XXXX' para 'I:\N� OS''
      Try//##05## Try para verificar se ocorre problema na copia do arquivo para o PEN-DRIVE
      S := TFileStream.Create(Origem2, fmOpenRead);
      Destino2 := 'I:\OS_' + Dm.IBInventarioINV_OS.Value + '\Trans_' + TransID + '_OS' + OS + '.txt';   //Destino para onde ser� copiado
      Try//##06##
      T := TFileStream.Create( Destino2, fmOpenWrite or fmCreate );
      Try//##07##
      T.CopyFrom(S, S.Size )
      Finally//##07##
      T.Free
      End //##07##
      Finally//##06##
      S.Free
      End;//##06##
      Except//##05##
      Application.MessageBox('Houve problema na copia da transmiss�o para o PEN-DRIVE. Essa transmiss�o ser� importada, ' + #13 +
      'por�m voc� est� sem o backup completo das transmiss�es. Colocar o PEN-DRIVE e copiar da pasta ' + #13 +
      'c:\SGD\Transmiss�es todas as transmiss�es, para caso ocorra algum porblema com o PC','Mensagem do Sistema', MB_ICONEXCLAMATION+ MB_OK);
      End;//##05##
      End;

      Try//##10##
      //Depois de copiado para pasta transmiss�o renomeio arquivo invent.txt da pasta do IGT e apaga o invent.txt
      //Pega Data e Hora da Transmiss�o do Arquivo
      DataHora := RetirarBarras(Retirar2Pontos(RetiraSpace(DataHoraArquivo(Origem2))));
      NomeArqBackup := Copy(Origem2,1,35) + '_' + DataHora + '.txt';
      RenameFile(Origem2, NomeArqBackup);
      DeleteFile(Origem2);
      Except //##10##
      Application.MessageBox('N�o foi poss�vel deletar o invent.txt da pasta do IGT','Mensagem do Sistema', MB_ICONERROR+ MB_OK);
      End; //##10##

      Try //##11##
      //Chama Fun��o que Importa os dados para para as tabelas
      //   IF DbEdit2.Text ='LOP' then
      //     ImportaDadosLOP(Destino1, Cont)
      //   else
      IF DbEdit2.Text ='CLB' then
      ImportaDadosCLB(Destino1, Cont)
      else
      IF DbEdit2.Text ='ONF' then
      ImportaDadosONF(Destino1, Cont)
      else
      IF DbEdit2.Text ='OCD' then
      ImportaDadosOCD(Destino1, Cont)
      else
      IF DbEdit2.Text ='ONP' then
      ImportaDadosONP(Destino1, Cont)
      else
      IF DbEdit2.Text ='SCH' then
      ImportaDadosSCH(Destino1, Cont)
      else
      IF (DbEdit2.Text ='PDA') or (DbEdit2.Text ='ASA') then
      ImportaDadosPDA(Destino1, Cont)
      else
      IF (DbEdit2.Text ='CP1') then
      ImportaDadosCP1(Destino1, Cont)
      else
      IF (DbEdit2.Text ='CP2') then
      ImportaDadosCP2(Destino1, Cont)
      else
      IF (DbEdit2.Text ='CP3') then
      ImportaDadosCP3(Destino1, Cont)
      else
      IF (DbEdit2.Text ='LVC') then
      ImportaDadosLVC(Destino1, Cont)
      else
      IF (DbEdit2.Text ='SAR') then
      ImportaDadosSAR(Destino1, Cont)
      else
      IF (DbEdit2.Text ='TNT') then
      ImportaDadosTNT(Destino1, Cont)
      else
      if (DBEdit2.Text ='DIA') then
      ImportaDadosDIA(Destino1, Cont)
      else
      if (DBEdit2.Text ='LMD') then
      ImportaDadosLMD(Destino1, Cont)
      else
      IF DbEdit2.Text ='FRN' then
      ImportaDadosFRN(Destino1, Cont)
      else
      IF DbEdit2.Text ='SAF' then
      ImportaDadosSAF(Destino1, Cont)
      else
      IF DbEdit2.Text ='RLD' then
      ImportaDadosRLD(Destino1, Cont)
      else
      IF DbEdit2.Text ='AND' then
      ImportaDadosAND(Destino1, Cont)
      else
      IF DbEdit2.Text ='MRS' then
      ImportaDadosMRS(Destino1, Cont)
      else
      IF DbEdit2.Text ='CEA' then
      ImportaDadosCEA(Destino1, Cont)
      else
      IF DbEdit2.Text ='PRN' then
      ImportaDadosPRN(Destino1, Cont)
      else
      IF DbEdit2.Text ='WIS' then
      ImportaDadosWIS(Destino1, Cont)
      else
      IF DbEdit2.Text ='PET' then
      ImportaDadosPET(Destino1, Cont)
      else
      IF DbEdit2.Text ='MRG' then
      ImportaDadosMRG(Destino1, Cont)
      else
      begin
      ImportaDados(Destino1, Cont);
      end;
      Except//##11##
      Application.MessageBox('N�o foi poss�vel chamar a fun��o que importa os dados para o SGD, POR FAVOR TRANSMITA NOVAMENTE!!','Mensagem do Sistema', MB_ICONERROR+ MB_OK);
      End;//##11##
      FechaTeladeAguarde;
      End;  //Fechar o If Caso encontre a pasta }
    Cont := Cont + 1;
  End; // Fecha o While que corre as pastas para ver se encontra o arquivo



  // end
  // else
  // begin
  // Application.MessageBox('O coletor descarregado est� fora do range de patrim�nio, verifique o n�mero do coletor e altere '+#13+ 'o n�mero da pasta no caminho C:\SGD\IGT_CE\IgtCeSinc para que o sistema importe as transmiss�es','Mensagem do Sistema', MB_ICONERROR + MB_OK)
  // end;

  Timer1.Enabled := True; // Ativa o Timer pois j� finalizou alguma importa��o
  // Abrir query que gera total de pe�as e valor financeiro

End;

procedure TFrmInventario.TabSheet5Show(Sender: TObject);
begin

  AtualizaInv;
  DM.IBSecao.Refresh;

  // DBGrid3.Refresh;
  if Panel12.Enabled = True then
    DBGrid3.SetFocus;
end;

procedure TFrmInventario.Fechar1Click(Sender: TObject);
begin
  Close;
end;

procedure TFrmInventario.DBGrid5KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  Secao, Status, TransID, AREA: String;
  ID: Integer;
  StrSql: String;
  SecTransId, SecaoId: Integer;
  matricula, SecaoAntiga, SecaoNova: string;
  NumCaracteres: Integer;
Begin
  // N�O DEIXA DELETAR A LINHA DIRETO DO BANCO
  if (Shift = [ssShift]) or (Shift = [ssAlt]) or (Shift = [ssShift, ssCtrl]) or (Shift = [ssShift, ssAlt]) or
    (Shift = [ssAlt, ssCtrl]) or (Shift = [ssShift, ssAlt, ssCtrl]) then
    Abort;

  // N�O DEIXA INSERIR LINHAS EM BRANCO NO GRIG;
  if (Key in [VK_DOWN]) and (DBGrid5.DataSource.DataSet.RecNo = DBGrid5.DataSource.DataSet.RecordCount) then
    Abort;

  // Pega valor do ID da linha
  IDCONTAGEM := DM.IBContagensCON_ID.Value;

  case Key of
    9:
      Key := 13;
  end;

  if (Key = 33) and (DM.DsContagens.State = Dsedit) then
  begin
    DM.IBContagens.Cancel;
    PosicionaContagem(IDCONTAGEM);
    // DM.IBContagens.Locate('CON_ID',IDCONTAGEM, [loPartialKey, loCaseInsensitive]);
    MouseShowCursor(True);
  end;

  if (Key = 34) and (DM.DsContagens.State = Dsedit) then
  begin
    DM.IBContagens.Cancel;
    PosicionaContagem(IDCONTAGEM);
    // DM.IBContagens.Locate('CON_ID',IDCONTAGEM, [loPartialKey, loCaseInsensitive]);
    MouseShowCursor(True);
  end;

  // Alterar Se��o com tecla de sobe e desce
  If ((Key = VK_DOWN) or (Key = VK_UP)) and (DM.DsContagens.State = Dsedit) then
  Begin
    Key := 13;
  End;

  // ###N�o deixa apagar registro do DbGrid, e s� coloca o STATUS como DEL###
  If (Shift = [ssCtrl]) and (Key = 46) and (DM.IBInventarioINV_STATUS.Value <> 'FINALIZADO') Then
  Begin
    // Dm.IBContagens.DisableControls;
    ID := DM.IBContagensCON_ID.Value;
    Key := 0;
    DM.SpDelGridContagem.Close;
    DM.SpDelGridContagem.ParamByName('IDCON').Value := DM.IBContagensCON_ID.Value;
    DM.SpDelGridContagem.ParamByName('TRANSID').Value := DM.IBContagensTRA_ID.Value;
    DM.SpDelGridContagem.ParamByName('SECSECAO').Value := DM.IBContagensSEC_SECAO.Value;
    DM.SpDelGridContagem.ParamByName('STATUS').Value := DM.IBContagensCON_STATUS.Value;
    DM.SpDelGridContagem.ExecProc;
    Application.ProcessMessages;
    // Fechar e abre as contagens
    DM.IBContagens.Close;
    // Dm.CdsContagens.Close;
    DM.IBContagens.Open;
    // Dm.CdsContagens.Open;

    DM.IBContagens.locate('CON_ID', ID, [Lopartialkey, locaseinsensitive]);

    AtualizaInv;
    // evandro novo total de pe�as
    If (DM.IBInventarioINV_LOJA.Value = 'PDA') or (DM.IBInventarioINV_LOJA.Value = 'ASA') then
    Begin
      DM.QGeraRelFinal.Close;
      DM.QGeraRelFinal.SQL.Clear;
      DM.QGeraRelFinal.SQL.Add
        ('SELECT SUM(ROUND(D.QTD)) AS QTD,SUM(D.PRECO) AS PRECO,SUM(PRECO*100) AS PRECO_PORCENT,SUM((ROUND(D.QTD))*100) AS QTD_PORCENT FROM CONTAGENS_CONSO D INNER JOIN CADASTRO C ON (C.CAD_EAN = D.CON_EAN) WHERE C.CAD_CODINT IS NOT NULL ');
      DM.QGeraRelFinal.Open;
      LabelTotalPecas.Caption := FormatFloat('##,###0.000', DM.QGeraRelFinal.FieldByName('QTD').Value);
      LabelTotalFin.Caption := FormatFloat('R$##,###0.00', DM.QGeraRelFinal.FieldByName('PRECO').Value);
    end;

  End
  else If (Shift = [ssCtrl]) and (Key = 46) and (DM.IBInventarioINV_STATUS.Value = 'FINALIZADO') Then
  begin
    Key := 13;
    ShowMessage('Imposs�vel alterar as contagens com o invent�rio FINALIZADO' + #13 + 'Favor, reabrir o invent�rio!');
  end;

  // ###Imprime relat�rio de auditoria###
  If Key = vk_F1 then // Se precionado a tecla F1
  Begin
    TRY
      DM.IBContagens.DisableControls;
      ID := DM.IBContagensCON_ID.Value;
      Secao := DM.IBContagensSEC_SECAO.Value;
      AREA := DM.IBContagensARE_AREA.Value;
      // TransID := IntToStr(Dm.IBContagensTRA_ID.value);
      DM.IBSecao.locate('TRA_ID;SEC_SECAO', VarArrayOf([TransID, Secao]), [Lopartialkey, locaseinsensitive]);
      If DM.IBSecaoSEC_STATUS.Value = '.' then

        RelAudit(DM.IBSecaoSEC_ID.Value, DM.IBSecaoSEC_SECAO.Value, DM.IBSecaoTRA_ID.Value,
          DM.qrySQL.FieldByName('ARE_AREA').Value);

      DM.IBContagens.locate('CON_ID;ARE_AREA', VarArrayOf([ID, AREA]), [Lopartialkey, locaseinsensitive]);
      // Dm.IBContagens.locate('CON_ID',ID,[Lopartialkey,locaseinsensitive]);
      DM.IBContagens.EnableControls;
      Application.ProcessMessages;
    except
      ShowMessage('Erro');
    End;
  end;
  // Chama tela de Consulta
  if Key = vk_F3 then
  Begin
    Cod := 1;
    DM.IBContagens.Close;
    DM.IBContagens.SelectSQL.Clear;
    DM.IBContagens.SelectSQL.Add('select c.con_ean, ');
    DM.IBContagens.SelectSQL.Add('c.con_id, ');
    DM.IBContagens.SelectSQL.Add('c.are_area, ');
    DM.IBContagens.SelectSQL.Add('c.con_data, ');
    DM.IBContagens.SelectSQL.Add('c.con_hora, ');
    DM.IBContagens.SelectSQL.Add('c.con_quantidade, ');
    DM.IBContagens.SelectSQL.Add('c.con_quantidadeoriginal, ');
    DM.IBContagens.SelectSQL.Add('c.hor_matricula, ');
    DM.IBContagens.SelectSQL.Add('c.sec_secao, ');
    DM.IBContagens.SelectSQL.Add('c.tra_id, ');
    DM.IBContagens.SelectSQL.Add('c.con_status, ');
    DM.IBContagens.SelectSQL.Add('c.rup_deposito, ');
    DM.IBContagens.SelectSQL.Add('c.rup_loja, ');
    DM.IBContagens.SelectSQL.Add('c.con_deposito, ');
    DM.IBContagens.SelectSQL.Add('c.deposito, ');
    DM.IBContagens.SelectSQL.Add('c.loja, ');
    DM.IBContagens.SelectSQL.Add('c.con_quantdesc, ');
    DM.IBContagens.SelectSQL.Add('c.num_cont, ');
    DM.IBContagens.SelectSQL.Add('c.tonalidade, ');
    DM.IBContagens.SelectSQL.Add('c.con_cadcodint, ');
    DM.IBContagens.SelectSQL.Add('c.con_endereco, ');
    DM.IBContagens.SelectSQL.Add('c.con_bsi, ');
    DM.IBContagens.SelectSQL.Add('c.con_cliente, ');
    DM.IBContagens.SelectSQL.Add('( select first 1 ca.cad_desc ');
    DM.IBContagens.SelectSQL.Add('from cadastro ca ');
    DM.IBContagens.SelectSQL.Add('where c.con_ean = ca.cad_ean) as cad_desc ');
    DM.IBContagens.SelectSQL.Add('FROM CONTAGENS c ');
    DM.IBContagens.SelectSQL.Add('ORDER BY SEC_SECAO, CON_ID ');
    DM.IBContagens.Open;
    FrmLocalizar := TFrmLocalizar.Create(FrmLocalizar);
    try
      FrmLocalizar.ShowModal;
    finally
      FrmLocalizar.Release;
      FrmLocalizar := nil;
    end;
  End;

  If (Shift = [ssCtrl]) and (Key = 65) and (Key = 46) Then
  begin
    DM.SpDeletaBanco.Open;
  end;
end;

procedure TFrmInventario.BitBtn3Click(Sender: TObject);
var
  Txt: Textfile;
  Entrada: string;
Begin
  // Deletar Tabela de Cadastro
  if FileExists('C:\SGD\Roster\roster.ros') then
  Begin
    DM.QDeletaRoster.Open;
    DM.Transacao.CommitRetaining;
    DM.QDeletaRoster.Close;

    DM.IBRoster.Open;
    AssignFile(Txt, ('C:\SGD\Roster\roster.ros')); // NOME do arquivo texto
    Reset(Txt);

    While not Eoln(Txt) do
    Begin
      Readln(Txt, Entrada);
      DM.IBRoster.Insert;
      DM.IBRosterROS_MATRICULA.Value := Copy(Entrada, 14, 11);
      DM.IBRosterROS_NOME.Value := Copy(Entrada, 26, 29);
    End;
    DM.IBRoster.Post;
    DM.Transacao.CommitRetaining;
    DM.IBRoster.First;
    CloseFile(Txt);
    DM.IBRoster.Close;
    DM.IBRoster.Open;
    Application.MessageBox('Importa��o conclu�da', 'Mensagem do Sistema', MB_ICONINFORMATION + MB_OK);
    Edit4.SetFocus;
  End
  else
    Application.MessageBox('O arquivo roster.ros n�o se encontra na pasta C:\SGD\Roster\', 'Mensagem do Sistema',
      MB_ICONERROR + MB_OK);
end;

procedure TFrmInventario.SpeedButton1Click(Sender: TObject);
begin
  // Transferir dados da tabela roster para a tabela de hor�rios, caso tenha algum registro
  If DM.IBRoster.RecordCount > 0 then
  Begin

    DM.IBHorarios.Insert;
    DM.IBHorariosROS_MATRICULA.Value := DM.IBRosterROS_MATRICULA.Value;
    DM.IBHorariosROS_NOME.Value := DM.IBRosterROS_NOME.Value;
    DM.IBHorariosHOR_ENTRADA.Value := '';
    DM.IBHorariosHOR_SAIDA.Value := '';
    DM.IBHorarios.Post;
    DM.Transacao.CommitRetaining;
    DBGrid7.SetFocus;

    DM.IBHorarios.Close;
    DM.IBHorarios.Open;

    // ATUALIZA O EDIT DE QUANTAS PESSOAS EST�O CADASTRADAS NO INVET�RIO
    DM.Query1.Close;
    DM.Query1.SQL.Clear;
    DM.Query1.SQL.Add('select ROS_NOME, ROS_MATRICULA from horario ');
    DM.Query1.SQL.Add('group by ROS_MATRICULA, ROS_NOME ');
    DM.Query1.Open;

    DM.Query1.First;
    DM.Query1.Last;
    EdtPessoas.Text := inttostr(DM.Query1.RecordCount);

  End;
end;

procedure TFrmInventario.SpeedButton2Click(Sender: TObject);
begin
  // Exluir registro de hor�rio caso tenha algum registro na tabela
  If DM.IBHorarios.RecordCount > 0 then
  Begin
    DM.IBHorarios.Delete;
    DM.Transacao.CommitRetaining;
  End;
End;

procedure TFrmInventario.FinalizarInventrio1Click(Sender: TObject);
begin
  Application.CreateForm(TFrmFinalizaInvent, FrmFinalizaInvent); // cria o form
  FrmFinalizaInvent.ShowModal;
  FreeAndNil(FrmFinalizaInvent); // libera o form da memoria
end;

{ procedure TFrmInventario.GeraArquivoFinal1Click(Sender: TObject);
  Var
  Arquivo1, Arquivo2: TStringList;
  DDia, DMes, DAno: String[2];
  DData: String;
  Cont, cont2 : integer;
  Begin

  try
  if Dm.QUsuarioUSU_NOME.Value = 'OPERADOR' then
  begin

  DM.qArqFinalDUP.Close;
  DM.qArqFinalDUP.Open;

  If ((DM.IBInventarioINV_LOJA.Value = 'CIC') and (DM.qArqFinalDUP.RecordCount > 0)) then
  GeraArqFinalCIC()
  else
  If ((DM.IBInventarioINV_LOJA.Value = 'ANA') and (DM.qArqFinalDUP.RecordCount > 0)) then
  GeraArqFinalANA()
  else
  if DM.qArqFinalDUP.RecordCount > 0 then
  begin
  Application.MessageBox('O arquivo de cadastro do cliente cont�m itens duplicados.'+#13+'                     O arquivo final n�o ser� gerado!'+#13+'     Comunique o respons�vel do cliente e informe o erro.','Mensagem do Sistema', MB_ICONWARNING + MB_OK);
  AdicionaLog(DateTimeToStr(Now)+'> Erro na gera��o do arquivo final - Cadastro do Cliente duplicado! ');
  end
  else
  begin
  if (DM.IBInventarioINV_LOJA.Value = 'CLB') then
  GeraArqFinalCLB()
  else
  if (DM.IBInventarioINV_LOJA.Value = 'SAV') then
  GeraArqFinalSAV()
  else
  if (DM.IBInventarioINV_LOJA.Value = 'PPS') then
  GeraArqFinalPPS()
  else
  if (DM.IBInventarioINV_LOJA.Value = 'JAP') then
  GeraArqFinalJAP()
  else
  if (DM.IBInventarioINV_LOJA.Value = 'KRI') then
  GeraArqFinalKRI()
  else
  if (DM.IBInventarioINV_LOJA.Value = 'CTD') then
  GeraArqFinalCTD()
  else
  if (DM.IBInventarioINV_LOJA.Value = 'MRG') then
  GeraArqFinalMRG()
  else
  if (DM.IBInventarioINV_LOJA.Value = 'ASA') then
  GeraArqFinalASA()
  else
  if (DM.IBInventarioINV_LOJA.Value = 'CSV') then
  GeraArqFinalCSV()
  else
  if (DM.IBInventarioINV_LOJA.Value = 'FST') then
  GeraArqFinalFST()
  else
  if (DM.IBInventarioINV_LOJA.Value = 'DGA') then
  GeraArqFinalDGA()
  else
  if (DM.IBInventarioINV_LOJA.Value = 'COV') or (DM.IBInventarioINV_LOJA.Value = 'KIP') or (DM.IBInventarioINV_LOJA.Value = 'SAI') then
  GeraArqFinalCOV()
  else
  if (DM.IBInventarioINV_LOJA.Value = 'PDA') then
  GeraArqFinalPDA()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'MRS') then
  GeraArqFinalMRS()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'GST') then
  GeraArqFinalGST()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'LUB') or (DM.IBInventarioINV_LOJA.Value = 'COR') or (DM.IBInventarioINV_LOJA.Value = 'EME') then
  GeraArqFinalGEP()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'CP1') or (DM.IBInventarioINV_LOJA.Value = 'CP2') then
  GeraArqFinalCPV()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'CP3') then
  GeraArqFinalCP3()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'LVC') then
  GeraArqFinalLVC()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'SAR') then
  GeraArqFinalSAR()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'TNT') then
  GeraArqFinalTNT()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'FRN') then
  GeraArqFinalFRN()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'DIA') then
  GeraArqFinalDIA()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'SAF') then
  GeraArqFinalSAF()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'NAT') then
  GeraArqFinalNAT()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'FUT') then
  GeraArqFinalFUT()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'DVL') then
  GeraArqFinalDVL()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'LOP') then
  GeraArqFinalLOP()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'CAM') then
  GeraArqFinalCAM()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'RLD') then
  GeraArqFinalRLD()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'AND') then
  GeraArqFinalAND()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'CEA') then
  GeraArqFinalCEA()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'PRN') then
  GeraArqFinalPRN()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'WIS') then
  GeraArqFinalWIS()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'ONF') OR (DM.IBInventarioINV_LOJA.Value = 'OEL')then
  GeraArqFinalONF()
  else
  if (DM.IBInventarioINV_LOJA.Value = 'OCD') or (DM.IBInventarioINV_LOJA.Value = 'ONP') then
  GeraArqFinalOCD()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'PET') then
  GeraArqFinalPET()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'SMP') then
  GeraArqFinalSMP()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'SMK') then
  GeraArqFinalSMP()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'SMD') then
  GeraArqFinalSMP()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'VLG') then
  GeraArqFinalVLG()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'DLG') then
  GeraArqFinalDLG()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'STM') then
  GeraArqFinalSTM()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'MRN') then
  GeraArqFinalMRN()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'RNI') then
  GeraArqFinalRNI()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'TMC') then
  GeraArqFinalTMC()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'DEC') then
  GeraArqFinalDEC()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'NFE') then
  GeraArqFinalNFe()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'BAR') then
  GeraArqFinalBAR()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'MAX') then
  GeraArqFinalMAX()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'SEB') then
  GeraArqFinalSEB()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'YKS') then
  GeraArqFinalYKS()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'YOG') then
  GeraArqFinalYOG()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'SVC') then
  GeraArqFinalSVC()

  else
  If (DM.IBInventarioINV_LOJA.Value = 'NIK') then
  GeraArqFinalNIK()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'ROS') then
  GeraArqFinalROS()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'BGB') then
  GeraArqFinalBGB()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'WAL') then
  GeraArqFinalWAL()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'GLM') then
  GeraArqFinalGLM()

  else

  with DM.Query1 do
  begin
  Close;
  SQL.Clear;
  SQL.Add('SELECT * FROM LAYOUT_OUTPUT ');
  SQL.Add('WHERE CLIENTE = :CLIENTE');
  Params.ParamByName('CLIENTE').Value := DM.IBInventarioINV_LOJA.Value;
  Open;

  if (DM.Query1.FieldByName('BREAK_FILE').Value = 'T') and (DM.Query1.FieldByName('FILE_GERAL').Value <> 'T') then
  begin
  GeraArqFinalDEPOSITO();
  GeraArqFinalLOJA();
  end
  else
  if (DM.Query1.FieldByName('BREAK_FILE').Value = 'T') and (DM.Query1.FieldByName('FILE_GERAL').Value = 'T') then
  begin
  GeraArqFinalDEPOSITO();
  GeraArqFinalLOJA();
  GeraArqFinalONF_CD();
  end


  else
  begin
  GeraArqFinalONF_CD();
  end;
  end;


  AdicionaLog(DateTimeToStr(Now)+'> Gera��o do arquivo final realizado com sucesso! - Usu�rio: OPERADOR ');
  Screen.Cursor:=CrDefault;
  end;
  end
  else
  if (Dm.QUsuarioUSU_NOME.Value = 'ADMINISTRADOR DO SISTEMA') or (Dm.QUsuarioUSU_NOME.Value = 'ADMINISTRADOR MASTER') or (Dm.QUsuarioUSU_NOME.Value = 'LIDER DO INVENTARIO') then
  begin
  DM.qArqFinalDUP.Close;
  DM.qArqFinalDUP.Open;

  if (DM.IBInventarioINV_LOJA.Value = 'CLB') then
  GeraArqFinalCLB()
  else
  if (DM.IBInventarioINV_LOJA.Value = 'SAV') then
  GeraArqFinalSAV()
  else
  if (DM.IBInventarioINV_LOJA.Value = 'PPS') then
  GeraArqFinalPPS()
  else
  if (DM.IBInventarioINV_LOJA.Value = 'JAP') then
  GeraArqFinalJAP()
  else
  if (DM.IBInventarioINV_LOJA.Value = 'KRI') then
  GeraArqFinalKRI()
  else
  if (DM.IBInventarioINV_LOJA.Value = 'CTD') then
  GeraArqFinalCTD()
  else
  if (DM.IBInventarioINV_LOJA.Value = 'MRG') then
  GeraArqFinalMRG()
  else
  if (DM.IBInventarioINV_LOJA.Value = 'ASA') then
  GeraArqFinalASA()
  else
  if (DM.IBInventarioINV_LOJA.Value = 'CSV') then
  GeraArqFinalCSV()
  else
  if (DM.IBInventarioINV_LOJA.Value = 'FST') then
  GeraArqFinalFST()
  else
  if (DM.IBInventarioINV_LOJA.Value = 'DGA') then
  GeraArqFinalDGA()
  else
  if (DM.IBInventarioINV_LOJA.Value = 'COV') or (DM.IBInventarioINV_LOJA.Value = 'KIP') or (DM.IBInventarioINV_LOJA.Value = 'SAI') then
  GeraArqFinalCOV()
  else
  if (DM.IBInventarioINV_LOJA.Value = 'PDA') then
  GeraArqFinalPDA()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'MRS') then
  GeraArqFinalMRS()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'GST') then
  GeraArqFinalGST()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'LUB') or (DM.IBInventarioINV_LOJA.Value = 'COR') or (DM.IBInventarioINV_LOJA.Value = 'EME') then
  GeraArqFinalGEP()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'CP1') or (DM.IBInventarioINV_LOJA.Value = 'CP2') then
  GeraArqFinalCPV()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'CP3') then
  GeraArqFinalCP3()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'LVC') then
  GeraArqFinalLVC()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'SAR') then
  GeraArqFinalSAR()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'TNT') then
  GeraArqFinalTNT()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'FRN') then
  GeraArqFinalFRN()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'DIA') then
  GeraArqFinalDIA()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'SAF') then
  GeraArqFinalSAF()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'NAT') then
  GeraArqFinalNAT()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'FUT') then
  GeraArqFinalFUT()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'DVL') then
  GeraArqFinalDVL()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'CAM') then
  GeraArqFinalCAM()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'RLD') then
  GeraArqFinalRLD()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'AND') then
  GeraArqFinalAND()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'CEA') then
  GeraArqFinalCEA()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'PRN') then
  GeraArqFinalPRN()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'WIS') then
  GeraArqFinalWIS()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'ONF') OR (DM.IBInventarioINV_LOJA.Value = 'OEL')then
  GeraArqFinalONF()
  else
  if (DM.IBInventarioINV_LOJA.Value = 'OCD') or (DM.IBInventarioINV_LOJA.Value = 'ONP') then
  GeraArqFinalOCD()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'CIC') then
  GeraArqFinalCIC()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'PET') then
  GeraArqFinalPET()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'SMP') then
  GeraArqFinalSMP()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'SMK') then
  GeraArqFinalSMP()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'SMD') then
  GeraArqFinalSMP()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'VLG') then
  GeraArqFinalVLG()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'DLG') then
  GeraArqFinalDLG()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'STM') then
  GeraArqFinalSTM()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'LOP') then
  GeraArqFinalLOP()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'MRN') then
  GeraArqFinalMRN()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'RNI') then
  GeraArqFinalRNI()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'TMC') then
  GeraArqFinalTMC()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'DEC') then
  GeraArqFinalDEC()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'NFE') then
  GeraArqFinalNFe()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'BAR') then
  GeraArqFinalBAR()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'MAX') then
  GeraArqFinalMAX()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'SEB') then
  GeraArqFinalSEB()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'YKS') then
  GeraArqFinalYKS()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'YOG') then
  GeraArqFinalYOG()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'SVC') then
  GeraArqFinalSVC()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'ROS') then
  GeraArqFinalROS()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'BGB') then
  GeraArqFinalBGB()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'WAL') then
  GeraArqFinalWAL()
  else
  If (DM.IBInventarioINV_LOJA.Value = 'GLM') then
  GeraArqFinalGLM()
  else
  with DM.Query1 do
  begin
  Close;
  SQL.Clear;
  SQL.Add('SELECT * FROM LAYOUT_OUTPUT ');
  SQL.Add('WHERE CLIENTE = :CLIENTE');
  Params.ParamByName('CLIENTE').Value := DM.IBInventarioINV_LOJA.Value;
  Open;
  if (DM.Query1.FieldByName('BREAK_FILE').Value = 'T') and (DM.Query1.FieldByName('FILE_GERAL').Value <> 'T') then
  begin
  GeraArqFinalDEPOSITO();
  GeraArqFinalLOJA();
  end
  else
  if (DM.Query1.FieldByName('BREAK_FILE').Value = 'T') and (DM.Query1.FieldByName('FILE_GERAL').Value = 'T') then
  begin
  GeraArqFinalDEPOSITO();
  GeraArqFinalLOJA();
  GeraArqFinalONF_CD();
  end
  else
  begin
  GeraArqFinalONF_CD();
  end;

  end;
  AdicionaLog(DateTimeToStr(Now)+'> Gera��o do arquivo final realizado com sucesso! - Usu�rio: ADMINISTTRADOR ');
  //Screen.Cursor:=CrDefault;
  end
  except
  Application.MessageBox('Erro ao gerar o arquivo final!','Mensagem do Sistema', MB_ICONERROR + MB_OK);
  AdicionaLog(DateTimeToStr(Now)+'> Erro na gera��o do arquivo final! - Usu�rio: '+Dm.QUsuarioUSU_NOME.Value);
  Screen.Cursor:=CrDefault;
  end;

  end; }

procedure TFrmInventario.GeraArquivoFinal1Click(Sender: TObject);
var
  Arquivo1, Arquivo2: TStringList;
  DDia, DMes, DAno: String[2];
  DData: String;
  Cont, cont2: Integer;
begin
  try
    if DM.QUsuarioUSU_NOME.Value = 'OPERADOR' then
    begin
      DM.qArqFinalDUP.Close;
      DM.qArqFinalDUP.Open;

      { Clientes CIC e ANA podem ter EANs duplicados no cadastro - Leandro (10/12/2014) }
      if (DM.IBInventarioINV_LOJA.Value = 'DSP') then
        GeraArqFinalDSP()
      else
      if DM.IBInventarioINV_LOJA.Value = 'CIC' then
        GeraArqFinalCIC()
      else if DM.IBInventarioINV_LOJA.Value = 'ANA' then
        GeraArqFinalANA()
      else if DM.IBInventarioINV_LOJA.Value = 'BHC' then
        GeraArqFinalBHC()
      else if (DM.IBInventarioINV_LOJA.Value = 'TNT') then
          GeraArqFinalTNT()
       else if (DM.IBInventarioINV_LOJA.Value = 'NSD') then
          GeraArqFinalNSD()
      else if not DM.qArqFinalDUP.IsEmpty then
      begin
        Application.MessageBox('O arquivo de cadastro do cliente cont�m itens duplicados.' + #13 +
          '                     O arquivo final n�o ser� gerado!' + #13 +
          '     Comunique o respons�vel do cliente e informe o erro.', 'Mensagem do Sistema', MB_ICONWARNING + MB_OK);
        AdicionaLog(DateTimeToStr(Now) + '> Erro na gera��o do arquivo final - Cadastro do Cliente duplicado!');
      end

      else
      begin
        if (DM.IBInventarioINV_LOJA.Value = 'CLB') then
          GeraArqFinalCLB()
        else if (DM.IBInventarioINV_LOJA.Value = 'SAV') then
          GeraArqFinalSAV()
        else if (DM.IBInventarioINV_LOJA.Value = 'PPS') then
          GeraArqFinalPPS()
        else if (DM.IBInventarioINV_LOJA.Value = 'JAP') then
          GeraArqFinalJAP()
        else if (DM.IBInventarioINV_LOJA.Value = 'KRI') then
          GeraArqFinalKRI()
        else if (DM.IBInventarioINV_LOJA.Value = 'CTD') then
          GeraArqFinalCTD()
        else if (DM.IBInventarioINV_LOJA.Value = 'MRG') then
          GeraArqFinalMRG()
        else if (DM.IBInventarioINV_LOJA.Value = 'ASA') then
          GeraArqFinalASA()
        else if (DM.IBInventarioINV_LOJA.Value = 'CSV') then
          GeraArqFinalCSV()
        else if (DM.IBInventarioINV_LOJA.Value = 'FST') then
          GeraArqFinalFST()
        else if (DM.IBInventarioINV_LOJA.Value = 'DGA') then
          GeraArqFinalDGA()
        else if (DM.IBInventarioINV_LOJA.Value = 'COV') or (DM.IBInventarioINV_LOJA.Value = 'KIP') or
          (DM.IBInventarioINV_LOJA.Value = 'SAI') then
          GeraArqFinalCOV()
          // else if (DM.IBInventarioINV_LOJA.Value = 'PDA') then GeraArqFinalPDA()
        else if (DM.IBInventarioINV_LOJA.Value = 'MRS') then
          GeraArqFinalMRS()
        else if (DM.IBInventarioINV_LOJA.Value = 'GST') then
          GeraArqFinalGST()
        else if (DM.IBInventarioINV_LOJA.Value = 'LUB') or (DM.IBInventarioINV_LOJA.Value = 'COR') or
          (DM.IBInventarioINV_LOJA.Value = 'EME') then
          GeraArqFinalGEP()
        else if (DM.IBInventarioINV_LOJA.Value = 'CP1') or (DM.IBInventarioINV_LOJA.Value = 'CP2') then
          GeraArqFinalCPV()
        else if (DM.IBInventarioINV_LOJA.Value = 'CP3') then
          GeraArqFinalCP3()
        else if (DM.IBInventarioINV_LOJA.Value = 'LVC') then
          GeraArqFinalLVC()
        else if (DM.IBInventarioINV_LOJA.Value = 'SAR') or (DM.IBInventarioINV_LOJA.Value = 'SRR') then
          GeraArqFinalSAR()
        else if (DM.IBInventarioINV_LOJA.Value = 'TNT') then
          GeraArqFinalTNT()
        else if (DM.IBInventarioINV_LOJA.Value = 'FRN') then
          GeraArqFinalFRN()
        else if (DM.IBInventarioINV_LOJA.Value = 'DIA') then
          GeraArqFinalDIA()
        else if (DM.IBInventarioINV_LOJA.Value = 'SAF') then
          GeraArqFinalSAF()
        else if (DM.IBInventarioINV_LOJA.Value = 'NAT') then
          GeraArqFinalNAT()
        else if (DM.IBInventarioINV_LOJA.Value = 'FUT') then
          GeraArqFinalFUT()
        else if (DM.IBInventarioINV_LOJA.Value = 'DVL') then
          GeraArqFinalDVL()
        else if (DM.IBInventarioINV_LOJA.Value = 'LOP') then
          GeraArqFinalLOP()
        else if (DM.IBInventarioINV_LOJA.Value = 'CAM') then
          GeraArqFinalCAM()
        else if (DM.IBInventarioINV_LOJA.Value = 'RLD') then
          GeraArqFinalRLD()
        else if (DM.IBInventarioINV_LOJA.Value = 'AND') then
          GeraArqFinalAND()
        else if (DM.IBInventarioINV_LOJA.Value = 'CEA') then
          GeraArqFinalCEA()
        else if (DM.IBInventarioINV_LOJA.Value = 'PRN') then
          GeraArqFinalPRN()
        else if (DM.IBInventarioINV_LOJA.Value = 'WIS') then
          GeraArqFinalWIS()
        else if (DM.IBInventarioINV_LOJA.Value = 'ONF') or (DM.IBInventarioINV_LOJA.Value = 'OEL') then
          GeraArqFinalONF()
        else if (DM.IBInventarioINV_LOJA.Value = 'OCD') or (DM.IBInventarioINV_LOJA.Value = 'ONP') then
          GeraArqFinalOCD()
        else if (DM.IBInventarioINV_LOJA.Value = 'PET') then
          GeraArqFinalPET()
        else if (DM.IBInventarioINV_LOJA.Value = 'SMP') then
          GeraArqFinalSMP()
        else if (DM.IBInventarioINV_LOJA.Value = 'SMK') then
          GeraArqFinalSMP()
        else if (DM.IBInventarioINV_LOJA.Value = 'SMD') then
          GeraArqFinalSMP()
        else if (DM.IBInventarioINV_LOJA.Value = 'VLG') then
          GeraArqFinalVLG()
        else if (DM.IBInventarioINV_LOJA.Value = 'DLG') then
          GeraArqFinalDLG()
        else if (DM.IBInventarioINV_LOJA.Value = 'STM') or (DM.IBInventarioINV_LOJA.Value = 'ETY') then
          GeraArqFinalSTM()
        else if (DM.IBInventarioINV_LOJA.Value = 'MRN') then
          GeraArqFinalMRN()
        else if (DM.IBInventarioINV_LOJA.Value = 'RNI') then
          GeraArqFinalRNI()
        else if (DM.IBInventarioINV_LOJA.Value = 'TMC') then
          GeraArqFinalTMC()
        else if (DM.IBInventarioINV_LOJA.Value = 'DEC') then
          GeraArqFinalDEC()
        else if (DM.IBInventarioINV_LOJA.Value = 'NFE') then
          GeraArqFinalNFe()
        else if (DM.IBInventarioINV_LOJA.Value = 'BAR') then
          GeraArqFinalBAR()
        else if (DM.IBInventarioINV_LOJA.Value = 'MAX') then
          GeraArqFinalMAX()
        else if (DM.IBInventarioINV_LOJA.Value = 'SEB') then
          GeraArqFinalSEB()
        else if (DM.IBInventarioINV_LOJA.Value = 'YKS') then
          GeraArqFinalYKS()
        else if (DM.IBInventarioINV_LOJA.Value = 'YOG') then
          GeraArqFinalYOG()
        else if (DM.IBInventarioINV_LOJA.Value = 'SVC') then
          GeraArqFinalSVC()
        else if (DM.IBInventarioINV_LOJA.Value = 'NIK') then
          GeraArqFinalNIK()
        else if (DM.IBInventarioINV_LOJA.Value = 'ROS') then
          GeraArqFinalROS()
        else if (DM.IBInventarioINV_LOJA.Value = 'BGB') then
          GeraArqFinalBGB()
        else if (DM.IBInventarioINV_LOJA.Value = 'WAL') then
          GeraArqFinalWAL()
        else if (DM.IBInventarioINV_LOJA.Value = 'GLM') then
          GeraArqFinalGLM()
        else if (DM.IBInventarioINV_LOJA.Value = 'CLS') then
          GeraArqFinalCLS()
        else if (DM.IBInventarioINV_LOJA.Value = 'JFL') then
          GeraArqFinalJFL()
        else if (DM.IBInventarioINV_LOJA.Value = 'DCS') then
          GeraArqFinalDCS()
        else if (DM.IBInventarioINV_LOJA.Value = 'NCL') then
          GeraArqFinalNCL()
        else if (DM.IBInventarioINV_LOJA.Value = 'LOC') then
          GeraArqFinalLOC()
        else if (DM.IBInventarioINV_LOJA.Value = 'NTR') then
          GeraArqFinalNTR()
        else if (DM.IBInventarioINV_LOJA.Value = 'SDD') then
          GeraArqFinalSDD()
        else if (DM.IBInventarioINV_LOJA.Value = 'EDR') then
          GeraArqFinalEDR()
        else if (DM.IBInventarioINV_LOJA.Value = 'DFY') then
          GeraArqFinalDFY()
        else if (DM.IBInventarioINV_LOJA.Value = 'BVL') then
          GeraArqFinalBVL()
        else if (DM.IBInventarioINV_LOJA.Value = 'CND') then
          GeraArqFinalCND()
        else if (DM.IBInventarioINV_LOJA.Value = 'KTX') then
          GeraArqFinalKTX()
        else if (DM.IBInventarioINV_LOJA.Value = 'SLL') then
          GeraArqFinalSLL()
        else if (DM.IBInventarioINV_LOJA.Value = 'SMN') then
          GeraArqFinalSMN()
        else if (DM.IBInventarioINV_LOJA.Value = 'ANL') then
          GeraArqFinalANL()
        else if (DM.IBInventarioINV_LOJA.Value = 'FRM') then
          GeraArqFinalFRM()
        else if (DM.IBInventarioINV_LOJA.Value = 'FAB') then
          GeraArqFinalFAB()
        else if (DM.IBInventarioINV_LOJA.Value = 'ABR') then
          GeraArqFinalABR()
        else if (DM.IBInventarioINV_LOJA.Value = 'FYI') then
          GeraArqFinalFYI()
        else if (DM.IBInventarioINV_LOJA.Value = 'MBZ') then
          GeraArqFinalMBZ()
        else if (DM.IBInventarioINV_LOJA.Value = 'MRT') then
          GeraArqFinalMRT()
        else if (DM.IBInventarioINV_LOJA.Value = 'LLB') or
                (DM.IBInventarioINV_LOJA.Value = 'BBO') or
                (DM.IBInventarioINV_LOJA.Value = 'JHJ') or
                (DM.IBInventarioINV_LOJA.Value = 'NOR') or
                (DM.IBInventarioINV_LOJA.Value = 'RSC') then
          GeraArqFinalLLB()
        else if (DM.IBInventarioINV_LOJA.Value = 'MKR') then
          GeraArqFinalMKR()
        else if (DM.IBInventarioINV_LOJA.Value = 'IDL') then
          GeraArqFinalIDL()
        else if (DM.IBInventarioINV_LOJA.Value = 'LVL') then
          GeraArqFinalLVL()
        else if (DM.IBInventarioINV_LOJA.Value = 'PMT') then
          GeraArqFinalPMT()
        else if (DM.IBInventarioINV_LOJA.Value = 'PSL') then
          GeraArqFinalPSL()
        else if (DM.IBInventarioINV_LOJA.Value = 'SET') then
          GeraArqFinalSET()
        else if (DM.IBInventarioINV_LOJA.Value = 'SEP') then
          GeraArqFinalSEP()
        else if (DM.IBInventarioINV_LOJA.Value = 'ADA') then
          GeraArqFinalADA()
        else if (DM.IBInventarioINV_LOJA.Value = 'SSP') then
          GeraArqFinalSSP()
        else if (DM.IBInventarioINV_LOJA.Value = 'LNG') then
          GeraArqFinalLNG()
        else if (DM.IBInventarioINV_LOJA.Value = 'FMS') then
          GeraArqFinalFMS()
        else if (DM.IBInventarioINV_LOJA.Value = 'RPL') then
          GeraArqFinalRPL()
        else if (DM.IBInventarioINV_LOJA.Value = 'SED') then
          GeraArqFinalSED()
        else if (DM.IBInventarioINV_LOJA.Value = 'ANC') then
          GeraArqFinalANC()
        else if (DM.IBInventarioINV_LOJA.Value = 'PNB') then
          GeraArqFinalPNB()
        else if (DM.IBInventarioINV_LOJA.Value = 'CPS') then
          GeraArqFinalCPS()
        else if (DM.IBInventarioINV_LOJA.Value = 'OUT') then
          GeraArqFinalOUT()
        else
          with DM.Query1 do
          begin
            Close;
            SQL.Clear;
            SQL.Add('SELECT * FROM LAYOUT_OUTPUT ');
            SQL.Add('WHERE CLIENTE = :CLIENTE');
            Params.ParamByName('CLIENTE').Value := DM.IBInventarioINV_LOJA.Value;
            Open;

            if (DM.Query1.FieldByName('BREAK_FILE').Value = 'T') and (DM.Query1.FieldByName('FILE_GERAL').Value <> 'T')
            then
            begin
              GeraArqFinalDEPOSITO();
              GeraArqFinalLOJA();
            end

            else if (DM.Query1.FieldByName('BREAK_FILE').Value = 'T') and
              (DM.Query1.FieldByName('FILE_GERAL').Value = 'T') then
            begin
              GeraArqFinalDEPOSITO();
              GeraArqFinalLOJA();
              GeraArqFinalONF_CD();
            end

            else
            begin
              GeraArqFinalONF_CD();
            end;
          end;

        AdicionaLog(DateTimeToStr(Now) + '> Gera��o do arquivo final realizado com sucesso! - Usu�rio: OPERADOR ');
        Screen.Cursor := CrDefault;
      end;
    end

    else if (DM.QUsuarioUSU_NOME.Value = 'ADMINISTRADOR DO SISTEMA') or
      (DM.QUsuarioUSU_NOME.Value = 'ADMINISTRADOR MASTER') or (DM.QUsuarioUSU_NOME.Value = 'LIDER DO INVENTARIO') then
    begin
      DM.qArqFinalDUP.Close;
      DM.qArqFinalDUP.Open;
      if not DM.qArqFinalDUP.IsEmpty then
      begin
        Application.MessageBox('O arquivo de cadastro do cliente cont�m itens duplicados.' + #13 +
          '                     O arquivo final n�o ser� gerado!' + #13 +
          '     Comunique o respons�vel do cliente e informe o erro.', 'Mensagem do Sistema', MB_ICONWARNING + MB_OK);
        AdicionaLog(DateTimeToStr(Now) + '> Erro na gera��o do arquivo final - Cadastro do Cliente duplicado!');
      end

      Else If (DM.IBInventarioINV_LOJA.Value = 'CLB') then
        GeraArqFinalCLB()
      else if (DM.IBInventarioINV_LOJA.Value = 'SAV') then
        GeraArqFinalSAV()
      else if (DM.IBInventarioINV_LOJA.Value = 'PPS') then
        GeraArqFinalPPS()
      else if (DM.IBInventarioINV_LOJA.Value = 'JAP') then
        GeraArqFinalJAP()
      else if (DM.IBInventarioINV_LOJA.Value = 'KRI') then
        GeraArqFinalKRI()
      else if (DM.IBInventarioINV_LOJA.Value = 'CTD') then
        GeraArqFinalCTD()
      else if (DM.IBInventarioINV_LOJA.Value = 'MRG') then
        GeraArqFinalMRG()
      else if (DM.IBInventarioINV_LOJA.Value = 'ASA') then
        GeraArqFinalASA()
      else if (DM.IBInventarioINV_LOJA.Value = 'CSV') then
        GeraArqFinalCSV()
      else if (DM.IBInventarioINV_LOJA.Value = 'FST') then
        GeraArqFinalFST()
      else if (DM.IBInventarioINV_LOJA.Value = 'DGA') then
        GeraArqFinalDGA()
      else if (DM.IBInventarioINV_LOJA.Value = 'COV') or (DM.IBInventarioINV_LOJA.Value = 'KIP') or
        (DM.IBInventarioINV_LOJA.Value = 'SAI') then
        GeraArqFinalCOV()
        // else if (DM.IBInventarioINV_LOJA.Value = 'PDA') then GeraArqFinalPDA()
      else if (DM.IBInventarioINV_LOJA.Value = 'MRS') then
        GeraArqFinalMRS()
      else if (DM.IBInventarioINV_LOJA.Value = 'GST') then
        GeraArqFinalGST()
      else if (DM.IBInventarioINV_LOJA.Value = 'LUB') or (DM.IBInventarioINV_LOJA.Value = 'COR') or
        (DM.IBInventarioINV_LOJA.Value = 'EME') then
        GeraArqFinalGEP()
      else if (DM.IBInventarioINV_LOJA.Value = 'CP1') or (DM.IBInventarioINV_LOJA.Value = 'CP2') then
        GeraArqFinalCPV()
      else if (DM.IBInventarioINV_LOJA.Value = 'CP3') then
        GeraArqFinalCP3()
      else if (DM.IBInventarioINV_LOJA.Value = 'LVC') then
        GeraArqFinalLVC()
      else if (DM.IBInventarioINV_LOJA.Value = 'SAR') or (DM.IBInventarioINV_LOJA.Value = 'SRR') then
        GeraArqFinalSAR()
      else if (DM.IBInventarioINV_LOJA.Value = 'TNT') then
        GeraArqFinalTNT()
      else if (DM.IBInventarioINV_LOJA.Value = 'FRN') then
        GeraArqFinalFRN()
      else if (DM.IBInventarioINV_LOJA.Value = 'DIA') then
        GeraArqFinalDIA()
      else if (DM.IBInventarioINV_LOJA.Value = 'SAF') then
        GeraArqFinalSAF()
      else if (DM.IBInventarioINV_LOJA.Value = 'NAT') then
        GeraArqFinalNAT()
      else if (DM.IBInventarioINV_LOJA.Value = 'FUT') then
        GeraArqFinalFUT()
      else if (DM.IBInventarioINV_LOJA.Value = 'DVL') then
        GeraArqFinalDVL()
      else if (DM.IBInventarioINV_LOJA.Value = 'CAM') then
        GeraArqFinalCAM()
      else if (DM.IBInventarioINV_LOJA.Value = 'RLD') then
        GeraArqFinalRLD()
      else if (DM.IBInventarioINV_LOJA.Value = 'AND') then
        GeraArqFinalAND()
      else if (DM.IBInventarioINV_LOJA.Value = 'CEA') then
        GeraArqFinalCEA()
      else if (DM.IBInventarioINV_LOJA.Value = 'PRN') then
        GeraArqFinalPRN()
      else if (DM.IBInventarioINV_LOJA.Value = 'WIS') then
        GeraArqFinalWIS()
      else if (DM.IBInventarioINV_LOJA.Value = 'ONF') or (DM.IBInventarioINV_LOJA.Value = 'OEL') then
        GeraArqFinalONF()
      else if (DM.IBInventarioINV_LOJA.Value = 'OCD') or (DM.IBInventarioINV_LOJA.Value = 'ONP') then
        GeraArqFinalOCD()
      else if (DM.IBInventarioINV_LOJA.Value = 'CIC') then
        GeraArqFinalCIC()
      else if (DM.IBInventarioINV_LOJA.Value = 'PET') then
        GeraArqFinalPET()
      else if (DM.IBInventarioINV_LOJA.Value = 'SMP') then
        GeraArqFinalSMP()
      else if (DM.IBInventarioINV_LOJA.Value = 'SMK') then
        GeraArqFinalSMP()
      else if (DM.IBInventarioINV_LOJA.Value = 'SMD') then
        GeraArqFinalSMP()
      else if (DM.IBInventarioINV_LOJA.Value = 'VLG') then
        GeraArqFinalVLG()
      else if (DM.IBInventarioINV_LOJA.Value = 'DLG') then
        GeraArqFinalDLG()
      else if (DM.IBInventarioINV_LOJA.Value = 'STM') or (DM.IBInventarioINV_LOJA.Value = 'ETY') then
        GeraArqFinalSTM()
      else if (DM.IBInventarioINV_LOJA.Value = 'LOP') then
        GeraArqFinalLOP()
      else if (DM.IBInventarioINV_LOJA.Value = 'MRN') then
        GeraArqFinalMRN()
      else if (DM.IBInventarioINV_LOJA.Value = 'RNI') then
        GeraArqFinalRNI()
      else if (DM.IBInventarioINV_LOJA.Value = 'TMC') then
        GeraArqFinalTMC()
      else if (DM.IBInventarioINV_LOJA.Value = 'DEC') then
        GeraArqFinalDEC()
      else if (DM.IBInventarioINV_LOJA.Value = 'NFE') then
        GeraArqFinalNFe()
      else if (DM.IBInventarioINV_LOJA.Value = 'BAR') then
        GeraArqFinalBAR()
      else if (DM.IBInventarioINV_LOJA.Value = 'MAX') then
        GeraArqFinalMAX()
      else if (DM.IBInventarioINV_LOJA.Value = 'SEB') then
        GeraArqFinalSEB()
      else if (DM.IBInventarioINV_LOJA.Value = 'YKS') then
        GeraArqFinalYKS()
      else if (DM.IBInventarioINV_LOJA.Value = 'YOG') then
        GeraArqFinalYOG()
      else if (DM.IBInventarioINV_LOJA.Value = 'SVC') then
        GeraArqFinalSVC()
      else if (DM.IBInventarioINV_LOJA.Value = 'ROS') then
        GeraArqFinalROS()
      else if (DM.IBInventarioINV_LOJA.Value = 'BGB') then
        GeraArqFinalBGB()
      else if (DM.IBInventarioINV_LOJA.Value = 'WAL') then
        GeraArqFinalWAL()
      else if (DM.IBInventarioINV_LOJA.Value = 'GLM') then
        GeraArqFinalGLM()
      else if (DM.IBInventarioINV_LOJA.Value = 'CLS') then
        GeraArqFinalCLS()
      else if (DM.IBInventarioINV_LOJA.Value = 'JFL') then
        GeraArqFinalJFL()
      else if (DM.IBInventarioINV_LOJA.Value = 'DCS') then
        GeraArqFinalDCS()
      else if (DM.IBInventarioINV_LOJA.Value = 'NCL') then
        GeraArqFinalNCL()
      else if (DM.IBInventarioINV_LOJA.Value = 'LOC') then
        GeraArqFinalLOC()
      else if (DM.IBInventarioINV_LOJA.Value = 'NTR') then
        GeraArqFinalNTR()
      else if (DM.IBInventarioINV_LOJA.Value = 'SDD') then
        GeraArqFinalSDD()
      else if (DM.IBInventarioINV_LOJA.Value = 'EDR') then
        GeraArqFinalEDR()
      else if (DM.IBInventarioINV_LOJA.Value = 'DFY') then
        GeraArqFinalDFY()
      else if (DM.IBInventarioINV_LOJA.Value = 'BVL') then
        GeraArqFinalBVL()
      else if (DM.IBInventarioINV_LOJA.Value = 'CND') then
        GeraArqFinalCND()
      else if (DM.IBInventarioINV_LOJA.Value = 'KTX') then
        GeraArqFinalKTX()
      else if (DM.IBInventarioINV_LOJA.Value = 'SLL') then
        GeraArqFinalSLL()
      else if (DM.IBInventarioINV_LOJA.Value = 'SMN') then
        GeraArqFinalSMN()
      else if (DM.IBInventarioINV_LOJA.Value = 'ANL') then
        GeraArqFinalANL()
      else if (DM.IBInventarioINV_LOJA.Value = 'FRM') then
        GeraArqFinalFRM()
      else if (DM.IBInventarioINV_LOJA.Value = 'FAB') then
        GeraArqFinalFAB()
      else if (DM.IBInventarioINV_LOJA.Value = 'ABR') then
        GeraArqFinalABR()
      else if (DM.IBInventarioINV_LOJA.Value = 'FYI') then
        GeraArqFinalFYI()
      else if (DM.IBInventarioINV_LOJA.Value = 'MRT') then
        GeraArqFinalMRT()
        else if (DM.IBInventarioINV_LOJA.Value = 'LLB') or
                (DM.IBInventarioINV_LOJA.Value = 'BBO') or
                (DM.IBInventarioINV_LOJA.Value = 'JHJ') or
                (DM.IBInventarioINV_LOJA.Value = 'NOR') or
                (DM.IBInventarioINV_LOJA.Value = 'RSC') then
          GeraArqFinalLLB()
        else if (DM.IBInventarioINV_LOJA.Value = 'MKR') then
          GeraArqFinalMKR()
        else if (DM.IBInventarioINV_LOJA.Value = 'IDL') then
          GeraArqFinalIDL()
        else if (DM.IBInventarioINV_LOJA.Value = 'LVL') then
          GeraArqFinalLVL()
        else if (DM.IBInventarioINV_LOJA.Value = 'PMT') then
          GeraArqFinalPMT()
        else if (DM.IBInventarioINV_LOJA.Value = 'PSL') then
          GeraArqFinalPSL()
        else if (DM.IBInventarioINV_LOJA.Value = 'SET') then
          GeraArqFinalSET()
        else if (DM.IBInventarioINV_LOJA.Value = 'SEP') then
          GeraArqFinalSEP()
        else if (DM.IBInventarioINV_LOJA.Value = 'ADA') then
          GeraArqFinalADA()
        else if (DM.IBInventarioINV_LOJA.Value = 'SSP') then
          GeraArqFinalSSP()
        else if (DM.IBInventarioINV_LOJA.Value = 'NSD') then
          GeraArqFinalNSD()
        else if (DM.IBInventarioINV_LOJA.Value = 'LNG') then
          GeraArqFinalLNG()
        else if (DM.IBInventarioINV_LOJA.Value = 'FMS') then
          GeraArqFinalFMS()
        else if (DM.IBInventarioINV_LOJA.Value = 'RPL') then
          GeraArqFinalRPL()
        else if (DM.IBInventarioINV_LOJA.Value = 'SED') then
          GeraArqFinalSED()
        else if (DM.IBInventarioINV_LOJA.Value = 'ANC') then
          GeraArqFinalANC()
        else if (DM.IBInventarioINV_LOJA.Value = 'PNB') then
          GeraArqFinalPNB()
        else if (DM.IBInventarioINV_LOJA.Value = 'CPS') then
          GeraArqFinalCPS()
        else if (DM.IBInventarioINV_LOJA.Value = 'OUT') then
          GeraArqFinalOUT()
      else
      begin
        with DM.Query1 do
        begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT * FROM LAYOUT_OUTPUT ');
          SQL.Add('WHERE CLIENTE = :CLIENTE');
          Params.ParamByName('CLIENTE').Value := DM.IBInventarioINV_LOJA.Value;
          Open;

          if (DM.Query1.FieldByName('BREAK_FILE').Value = 'T') and (DM.Query1.FieldByName('FILE_GERAL').Value <> 'T')
          then
          begin
            GeraArqFinalDEPOSITO();
            GeraArqFinalLOJA();
          end

          else if (DM.Query1.FieldByName('BREAK_FILE').Value = 'T') and (DM.Query1.FieldByName('FILE_GERAL').Value = 'T')
          then
          begin
            GeraArqFinalDEPOSITO();
            GeraArqFinalLOJA();
            GeraArqFinalONF_CD();
          end

          else
          begin
            GeraArqFinalONF_CD();
          end;
        end;
      end;

      AdicionaLog(DateTimeToStr(Now) + '> Gera��o do arquivo final realizado com sucesso! - Usu�rio: ADMINISTTRADOR ');
      Screen.Cursor := CrDefault;
    end;
  except on E: exception do
    begin
    ShowMessage('Erro ao gerar o arquivo final!  Mensagem do Sistema - Erro: ' + e.message);
//    Application.MessageBox('Erro ao gerar o arquivo final!', 'Mensagem do Sistema - Erro: ' + e.message, MB_ICONERROR + MB_OK);
    AdicionaLog(DateTimeToStr(Now) + '> Erro na gera��o do arquivo final! - Usu�rio: ' + DM.QUsuarioUSU_NOME.Value);
    Screen.Cursor := CrDefault;
    end;
  end;
end;

procedure TFrmInventario.DBGrid3KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  Secao, Status, TransID, dup: String;
  ID, Id_secao: Integer;
  SecaoNova, SecaoAntiga, matricula: String;
  SecaoId, SecTransId, NumCaracteres, secini, secfin: Integer;
  volta: Boolean;
  Dupli: Integer;
Begin
  // Pegar valor do ID da Linha

  // N�O DEIXA DELETAR A LINHA DIRETO DO BANCO
  if (Shift = [ssShift]) or (Shift = [ssAlt]) or (Shift = [ssShift, ssCtrl]) or (Shift = [ssShift, ssAlt]) or
    (Shift = [ssAlt, ssCtrl]) or (Shift = [ssShift, ssAlt, ssCtrl]) then
    Abort;

  IDSECAO := DM.IBSecaoSEC_ID.Value;
  // N�O DEIXA INSERIR LINHAS EM BRANCO NO GRIG;
  if (Key in [VK_DOWN]) and (DBGrid3.DataSource.DataSet.RecNo = DBGrid3.DataSource.DataSet.RecordCount) then
    Abort;

  // Alterar Se��o com tecla de sobe e desce
  If ((Key = VK_DOWN) or (Key = VK_UP) or (Key = VK_PRIOR) or (Key = VK_NEXT)) and (DM.DsSecao.State = Dsedit) and
    (DM.IBSecaoSEC_STATUS.Value = '.') and (DM.IBSecaoSTATUS.Value <> 'ABE') then
  Begin
    Key := 13;
  End;

  // ###N�o deixa apagar registro do DbGrid, e s� coloca o STATUS como DEL###
  If (Shift = [ssCtrl]) and (Key = 46) Then
  Begin
    if CheckBox1.Checked = True then
    begin
      CheckBox1.Checked := False;
    end;

    Key := 0;
    UFuncProc.sec := True;
    SecaoAntiga := DM.IBSecaoSEC_SECAO.OldValue;
    Secao := DM.IBSecaoSEC_SECAO.Value;
    TransID := inttostr(DM.IBSecaoTRA_ID.Value);
    Status := DM.IBSecaoSEC_ABERTA.Value;
    dup := DM.IBSecaoSEC_DUPLI.Value;

    // ###Caso queira tirar de deletado
    If DM.IBSecaoSEC_STATUS.Value = 'DEL' Then
    Begin

      DM.SpDelContagem.Close;
      DM.SpDelContagem.ParamByName('TRA_ID').Value := TransID;
      DM.SpDelContagem.ParamByName('TRANSID').Value := DM.IBSecaoTRA_ID.AsInteger;
      DM.SpDelContagem.ParamByName('SECSECAO').Value := DM.IBSecaoSEC_SECAO.Value;
      DM.SpDelContagem.ParamByName('STATUS').Value := 'DEL';

      DM.SpDelContagem.ExecProc;

      SecaoId := -1;
      DM.SpAlteraSec.Close;
      DM.SpAlteraSec.ParamByName('NOVASEC').Value := SecaoNova;
      DM.SpAlteraSec.ParamByName('VELHASEC').Value := SecaoAntiga;
      DM.SpAlteraSec.ParamByName('TRANSID').Value := SecTransId;
      DM.SpAlteraSec.ParamByName('ID_SECAO').Value := SecaoId;

      DM.SpAlteraSec.ExecProc;

      DM.Transacao.CommitRetaining;

      Duplicidade(Secao);

      PosicionaSecao(IDSECAO);

      AtualizaInv;
      // evandro novo total de pe�as
      If (DM.IBInventarioINV_LOJA.Value = 'PDA') or (DM.IBInventarioINV_LOJA.Value = 'ASA') then
      Begin
        DM.QGeraRelFinal.Close;
        DM.QGeraRelFinal.SQL.Clear;
        DM.QGeraRelFinal.SQL.Add
          ('SELECT SUM(ROUND(D.QTD)) AS QTD,SUM(D.PRECO) AS PRECO,SUM(PRECO*100) AS PRECO_PORCENT,SUM((ROUND(D.QTD))*100) AS QTD_PORCENT FROM CONTAGENS_CONSO D INNER JOIN CADASTRO C ON (C.CAD_EAN = D.CON_EAN) WHERE C.CAD_CODINT IS NOT NULL');
        DM.QGeraRelFinal.Open;
        LabelTotalPecas.Caption := FormatFloat('##,###0.000', DM.QGeraRelFinal.FieldByName('QTD').Value);
        LabelTotalFin.Caption := FormatFloat('R$##,###0.00', DM.QGeraRelFinal.FieldByName('PRECO').Value);
      end;

    End
    else if DM.IBSecaoSEC_STATUS.Value = '.' then // ###Caso queira deletar
    Begin

      DM.SpDelContagem.Close;
      DM.SpDelContagem.ParamByName('TRA_ID').Value := TransID;
      DM.SpDelContagem.ParamByName('TRANSID').Value := DM.IBSecaoTRA_ID.AsInteger;
      DM.SpDelContagem.ParamByName('SECSECAO').Value := DM.IBSecaoSEC_SECAO.Value;
      DM.SpDelContagem.ParamByName('STATUS').Value := '.';

      DM.SpDelContagem.ExecProc;
      If NOT((DM.IBSecaoSEC_DUPLI.Value = 'T') or (DM.IBSecaoSEC_DUPLI.Value = '*')) then
      Begin
        If (DM.IBSecaoSEC_DUPLI.Value = 'T') or (DM.IBSecaoSEC_ABERTA.Value = 'F') and
          (DM.ibSecaoSEC_INESPERADA.Value = 'F') then
        Begin
          DM.qrySQL.Close;
          DM.qrySQL.SQL.Clear;
          DM.qrySQL.SQL.Add('Insert into SECAO (SEC_SECAO, SEC_ABERTA, SEC_INESPERADA, SEC_DUPLI, SEC_STATUS) ');
          DM.qrySQL.SQL.Add('values (:SEC_SECAO, :SEC_ABERTA, :SEC_INESPERADA, :SEC_DUPLI, :SEC_STATUS) ');
          DM.qrySQL.ParamByName('SEC_SECAO').Value := Secao;
          DM.qrySQL.ParamByName('SEC_ABERTA').Value := 'T';
          DM.qrySQL.ParamByName('SEC_INESPERADA').Value := 'F';
          DM.qrySQL.ParamByName('SEC_DUPLI').Value := 'F';
          DM.qrySQL.ParamByName('SEC_STATUS').Value := '.';
          DM.qrySQL.ExecSQL;
          Application.ProcessMessages;
        END
        ELSE
        BEGIN
          DM.qrySQL.Close;
          DM.qrySQL.SQL.Clear;
          DM.qrySQL.SQL.Add('UPDATE SECAO SET SEC_STATUS = :SEC_STATUS ');
          DM.qrySQL.SQL.Add('WHERE SEC_SECAO = :SEC_SECAO AND SEC_ID = :SEC_ID ');
          DM.qrySQL.ParamByName('SEC_SECAO').Value := Secao;
          DM.qrySQL.ParamByName('SEC_ID').Value := DM.IBSecaoSEC_ID.AsInteger;
          DM.qrySQL.ParamByName('SEC_STATUS').Value := 'DEL';
          DM.qrySQL.ExecSQL;
          Application.ProcessMessages;
        end;
      end
      else

        DM.Transacao.CommitRetaining;

      Duplicidade(Secao);

      PosicionaSecao(IDSECAO);

      AtualizaInv;
      // evandro novo total de pe�as
      If (DM.IBInventarioINV_LOJA.Value = 'PDA') or (DM.IBInventarioINV_LOJA.Value = 'ASA') then
      Begin
        DM.QGeraRelFinal.Close;
        DM.QGeraRelFinal.SQL.Clear;
        DM.QGeraRelFinal.SQL.Add
          ('SELECT SUM(ROUND(D.QTD)) AS QTD,SUM(D.PRECO) AS PRECO,SUM(PRECO*100) AS PRECO_PORCENT,SUM((ROUND(D.QTD))*100) AS QTD_PORCENT FROM CONTAGENS_CONSO D INNER JOIN CADASTRO C ON (C.CAD_EAN = D.CON_EAN) WHERE C.CAD_CODINT IS NOT NULL ');
        DM.QGeraRelFinal.Open;
        LabelTotalPecas.Caption := FormatFloat('##,###0.000', DM.QGeraRelFinal.FieldByName('QTD').Value);
        LabelTotalFin.Caption := FormatFloat('R$##,###0.00', DM.QGeraRelFinal.FieldByName('PRECO').Value);
      end;
    end;
  End;

  // ###Imprime relat�rio de auditoria###
  if (Key = vk_F1) and (DM.IBSecaoSEC_STATUS.Value = '.') then
  Begin
    DM.qrySQL.Close;
    DM.qrySQL.SQL.Clear;
    DM.qrySQL.SQL.Add('select are_area, tra_id from contagens ');
    DM.qrySQL.SQL.Add('where (TRA_ID = :TransID) and (SEC_SECAO = :secao) and (HOR_MATRICULA = :HOR_MATRICULA)');
    DM.qrySQL.SQL.Add('group by 1, 2 ');
    DM.qrySQL.Params.ParamByName('Secao').Value := DM.IBSecaoSEC_SECAO.Value;
    DM.qrySQL.Params.ParamByName('TransID').Value := DM.IBSecaoTRA_ID.Value;
    DM.qrySQL.Params.ParamByName('HOR_MATRICULA').Value := DM.IBSecaoHOR_MATRICULA.Value;
    DM.qrySQL.Open;

    RelAudit(DM.IBSecaoSEC_ID.Value, DM.IBSecaoSEC_SECAO.Value, DM.qrySQL.FieldByName('TRA_ID').Value,
      DM.qrySQL.FieldByName('ARE_AREA').Value);
  End;

  // ###Imprime relat�rio de auditoria consolidada###
  if (Key = vk_F2) and (DM.IBSecaoSEC_STATUS.Value = '.') then
  Begin
    DM.qrySQL.Close;
    DM.qrySQL.SQL.Clear;
    DM.qrySQL.SQL.Add('select are_area, tra_id from contagens ');
    DM.qrySQL.SQL.Add('where (TRA_ID = :TransID) and (SEC_SECAO = :secao) and (HOR_MATRICULA = :HOR_MATRICULA) ');
    DM.qrySQL.SQL.Add('group by 1, 2 ');
    DM.qrySQL.Params.ParamByName('Secao').Value := DM.IBSecaoSEC_SECAO.Value;
    DM.qrySQL.Params.ParamByName('TransID').Value := DM.IBSecaoTRA_ID.Value;
    DM.qrySQL.Params.ParamByName('HOR_MATRICULA').Value := DM.IBSecaoHOR_MATRICULA.Value;
    DM.qrySQL.Open;

    RelAuditConso(DM.IBSecaoSEC_ID.Value, DM.IBSecaoSEC_SECAO.Value, DM.qrySQL.FieldByName('TRA_ID').Value,
      DM.qrySQL.FieldByName('ARE_AREA').Value);
    DM.IBSecao.Edit;
    DM.IBSecaoSEC_AUDITADA.Value := 'S';
  End;

  // Chama fun��o aceita duplicidade
  if (Key = vk_F4) and (DM.IBSecaoSEC_DUPLI.Value = 'T') and (DM.ibSecaoSEC_INESPERADA.Value = 'F') then
  Begin
    Application.ProcessMessages;
    DM.SpAceitaDupli.Close;
    DM.SpAceitaDupli.ParamByName('SEC_SECAO').Value := DM.IBSecaoSEC_SECAO.Value;
    DM.SpAceitaDupli.ExecProc;
    DM.SpAceitaDupli.Close;
    DM.Transacao.CommitRetaining;
    DM.IBSecao.Refresh;
    // PosicionaSecao(IDSECAO);

  End;
  // AceitaDuplicidade(inttostr(Dm.IBSecaoTra_ID.value), Dm.IBSecaoSec_Secao.value);
  // AceitaDuplicidade(Dm.IBSecaoSEC_ID.Value,Dm.IBSecaoSec_Secao.value);
  // Chama tela de consulta
  if Key = vk_F3 then
  Begin
    Cod := 2;

    DM.IBSecao.Filtered := False;
    Label21.Visible := False;
    Application.CreateForm(TFrmLocalizar, FrmLocalizar); // cria o form
    FrmLocalizar.ShowModal;
    FreeAndNil(FrmLocalizar); // libera o form da memoria
  End;

  // Inserta Se��o
  if (Key = vk_F5) and (DM.ibSecaoSEC_INESPERADA.Value = 'T') then
  Begin
    With DM.qrySQL do
    begin
      Close;
      SQL.Clear;
      SQL.Add('SELECT ENDERECO FROM LAYOUT_INPUT WHERE CLIENTE = :CLIENTE');
      ParamByName('CLIENTE').Value := DM.IBInventarioINV_LOJA.Value;
      Open;
    end;

    // if DM.qrySQL.FieldByName('ENDERECO').asString <> 'T' then
    // Begin

    ID := DM.IBSecaoSEC_ID.Value;
    secini := DM.IBSecaoSEC_SECAO.AsInteger;
    secfin := DM.IBSecaoSEC_SECAO.AsInteger;
    // verifica se existem mapeamentos cadastrados que sejam subsequentes desta se��o
    DM.IBMapeamento.Close;
    DM.IBMapeamento.ParamByName('secini').Value := '0';
    DM.IBMapeamento.ParamByName('secfin').Value := secini - 1;
    DM.IBMapeamento.Open;
    DM.IBMapeamento.Last;
    volta := True;
    while not(DM.IBMapeamento.Bof) and volta do
    begin

      if ((secini - 1) = DM.IBMapeamentoMAP_SECFIN.AsInteger) then
      begin
        secini := DM.IBMapeamentoMAP_SECINI.AsInteger;
      end
      else
        volta := False;
      DM.IBMapeamento.Prior;
    end;
    // endwh

    DM.IBMapeamento.Close;
    If (secfin < 9999) then
    begin
      DM.IBMapeamento.ParamByName('secini').Value := secfin + 1;
    end
    else
      DM.IBMapeamento.ParamByName('secini').Value := secfin;
    DM.IBMapeamento.ParamByName('secfin').Value := '9999';
    DM.IBMapeamento.Open;
    DM.IBMapeamento.First;
    volta := True;

    while not(DM.IBMapeamento.Eof) and volta do
    begin
      if ((secfin + 1) = DM.IBMapeamentoMAP_SECINI.AsInteger) then
      begin
        secfin := DM.IBMapeamentoMAP_SECFIN.AsInteger;
      end
      else

        volta := False;

      // Dm2.QMap_total.Close;
      // Dm2.QMap_total.Open;
      // Dm2.QMap_totalGeral.Close;
      // Dm2.QMap_totalGeral.Open;
    end;
    //

    CadastraMapeamento(inttostr(secini), inttostr(secfin), '');

    DM.IBSecao.Refresh;
    // PosicionaSecao(IDSECAO);
    Application.ProcessMessages;
    // end;
    // else
    // begin

    // CadastraMapeamentoEnd(Dm.IBSecaoSEC_SECAO.AsString, Dm.IBSecaoSEC_SECAO.AsString, '');
    // Application.ProcessMessages;
    // PosicionaSecao(IDSECAO);
    // end;
  End;


  // Dm2.QMap_total.Close;

  // Dm2.QMap_total.Open;

  // Dm2.QMap_totalGeral.Close;

  // Dm2.QMap_totalGeral.Open;

END;

procedure TFrmInventario.RelatrioTotaldePeaseFinanceiro1Click(Sender: TObject);
begin
  Application.CreateForm(TFrmNCadastrado, FrmNCadastrado); // cria o form
//  FreeAndNil(FrmDivergencia); // libera o form da memoria


  If (DM.IBInventarioINV_LOJA.Value = 'PDA') Or (DM.IBInventarioINV_LOJA.Value = 'COV') or
    (DM.IBInventarioINV_LOJA.Value = 'ASA') then
  begin
    // Timer2.Enabled:=false;
    DM.QGeraRelFinal.Close;
    DM.QGeraRelFinal.SQL.Clear;
    DM.QGeraRelFinal.SQL.Add
      (' SELECT SUM(ROUND(QTD)) AS QTD,SUM(PRECO) as PRECO,SUM(PRECO*100) AS PRECO_PORCENT,SUM(((ROUND(QTD))*100)) AS QTD_PORCENT FROM CONTAGENS_CONSO_GERAL WHERE CAD_CODINT IS NOT NULL');
    DM.QGeraRelFinal.Open;
    DM.QGeraRelFinalDeposito.Close;
    DM.QGeraRelFinalDeposito.SQL.Clear;
    DM.QGeraRelFinalDeposito.SQL.Add
      (' SELECT SUM(ROUND(C.QTD)) AS QTD_DEP,SUM(C.PRECO) as PRECO_DEP,SUM(C.PRECO*100) AS PRECO_PORCENT_DEP FROM CONTAGENS_CONSO_GERAL C INNER JOIN MAPEAMENTO M ON (C.SEC_SECAO BETWEEN M.MAP_SECINI AND M.MAP_SECFIN) ');
    DM.QGeraRelFinalDeposito.SQL.Add(' WHERE (M.DEPOSITO = "X") AND (C.CAD_CODINT IS NOT NULL)');
    DM.QGeraRelFinalDeposito.Open;
    DM.QGeraRelFinalLoja.Close;
    DM.QGeraRelFinalLoja.SQL.Clear;
    DM.QGeraRelFinalLoja.SQL.Add
      (' SELECT SUM(ROUND(C.QTD)) AS QTD_LOJA,SUM(C.PRECO) as PRECO_LOJA,SUM(C.PRECO*100) AS PRECO_PORCENT_LOJA FROM CONTAGENS_CONSO_GERAL C INNER JOIN MAPEAMENTO M ON (C.SEC_SECAO BETWEEN M.MAP_SECINI AND M.MAP_SECFIN) ');
    DM.QGeraRelFinalLoja.SQL.Add(' WHERE (M.LOJA = "X") AND (C.CAD_CODINT IS NOT NULL) ');
    DM.QGeraRelFinalLoja.Open;

    DM.RvPrjWis.Close;
    DM.RvPrjWis.projectfile := 'C:\SGD\Rave\Final.rav';
    DM.RvPrjWis.Open;
    FrmNCadastrado.ShowModal;
    DM.RvSysWis.DefaultDest := rdPreview;
    DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups + [ssAllowSetup];
    DM.RvPrjWis.ExecuteReport('Final');

    // Timer2.Enabled:=true;
  end
  else If (DM.IBInventarioINV_LOJA.Value = 'RLD') then
  begin
    // Timer2.Enabled:=false;
    DM.QGeraRelFinal.Close;
    DM.QGeraRelFinal.Open;
    DM.RvPrjWis.Close;
    DM.RvPrjWis.projectfile := 'C:\SGD\Rave\Final_RLD.rav';
    DM.RvPrjWis.Open;
    DM.RvSysWis.DefaultDest := rdPreview;
    DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups + [ssAllowSetup];
    DM.RvPrjWis.ExecuteReport('Final_RLD');

    Try
      DM.RvSysWis.DefaultDest := rdFile;
      DM.RvSysWis.DoNativeOutput := False;
      DM.RvSysWis.RenderObject := RvPDF;
      DM.RvSysWis.OutputFileName := 'C:\SGD\Relatorios\Relatorio Total de Pecas e Financeiro_' +
        DM.IBInventarioINV_LOJA.AsString + '_' + DM.IBInventarioINV_NUMLOJA.AsString + '.pdf';
      // caminho onde vai gerar o arquivo pdf
      DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
      DM.RvPrjWis.projectfile := 'C:\SGD\Rave\Final_RLD.rav';
      // caminho onde esta o arquivo que ele vai exportar para pdf
      DM.RvPrjWis.Engine := DM.RvSysWis;
      DM.RvPrjWis.Execute;
    Except
      ShowMessage('Erro ao gerar o relat�rio em PDF');
    end;

    // Salvando relat�rio em TXT automaticamente
    Try
      DM.RvSysWis.DefaultDest := rdFile;
      DM.RvSysWis.DoNativeOutput := False;
      DM.RvSysWis.RenderObject := RvTxt;
      DM.RvSysWis.OutputFileName := 'C:\SGD\Relatorios\Relatorio Total de Pecas e Financeiro_' +
        DM.IBInventarioINV_LOJA.AsString + '_' + DM.IBInventarioINV_NUMLOJA.AsString + '.txt';
      // caminho onde vai gerar o arquivo pdf
      DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
      DM.RvPrjWis.projectfile := 'C:\SGD\Rave\Final_RLD.rav';
      // caminho onde esta o arquivo que ele vai exportar para pdf
      DM.RvPrjWis.Engine := DM.RvSysWis;
      DM.RvPrjWis.Execute;
    except
      ShowMessage('Erro ao gerar o relat�rio em TXT');
    end;

    // Timer2.Enabled:=true;
  end
  else If (DM.IBInventarioINV_LOJA.Value = 'CEA') then
  begin
    // Timer2.Enabled:=false;
    Dm2.QGeraFinalCEA_Loja.Close;
    Dm2.QGeraFinalCEA_Loja.Open;
    Dm2.QGeraFinalCEA_Dep.Close;
    Dm2.QGeraFinalCEA_Dep.Open;
    Dm2.QTotalPecas_CEA.Close;
    Dm2.QTotalPecas_CEA.Open;
    DM.RvPrjWis.Close;
    DM.RvPrjWis.projectfile := 'C:\SGD\Rave\Final_CEA.rav';
    DM.RvPrjWis.Open;
    DM.RvSysWis.DefaultDest := rdPreview;
    DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups + [ssAllowSetup];
    DM.RvPrjWis.ExecuteReport('Final_CEA');

    Try
      DM.RvSysWis.DefaultDest := rdFile;
      DM.RvSysWis.DoNativeOutput := False;
      DM.RvSysWis.RenderObject := RvPDF;
      DM.RvSysWis.OutputFileName := 'C:\SGD\Relatorios\Relatorio Total de Pecas e Financeiro_' +
        DM.IBInventarioINV_LOJA.AsString + '_' + DM.IBInventarioINV_NUMLOJA.AsString + '.pdf';
      // caminho onde vai gerar o arquivo pdf
      DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
      DM.RvPrjWis.projectfile := 'C:\SGD\Rave\Final_CEA.rav';
      // caminho onde esta o arquivo que ele vai exportar para pdf
      DM.RvPrjWis.Engine := DM.RvSysWis;
      DM.RvPrjWis.Execute;
    Except
      ShowMessage('Erro ao gerar o relat�rio em PDF');
    end;

    // Salvando relat�rio em TXT automaticamente
    Try
      DM.RvSysWis.DefaultDest := rdFile;
      DM.RvSysWis.DoNativeOutput := False;
      DM.RvSysWis.RenderObject := RvTxt;
      DM.RvSysWis.OutputFileName := 'C:\SGD\Relatorios\Relatorio Total de Pecas e Financeiro_' +
        DM.IBInventarioINV_LOJA.AsString + '_' + DM.IBInventarioINV_NUMLOJA.AsString + '.txt';
      // caminho onde vai gerar o arquivo pdf
      DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
      DM.RvPrjWis.projectfile := 'C:\SGD\Rave\Final_CEA.rav';
      // caminho onde esta o arquivo que ele vai exportar para pdf
      DM.RvPrjWis.Engine := DM.RvSysWis;
      DM.RvPrjWis.Execute;
    except
      ShowMessage('Erro ao gerar o relat�rio em TXT');
    end;

    // Timer2.Enabled:=true;
  end
  else
    if Dm.IBInventarioINV_LOJA.Value = 'BHC' then
      begin
        DM.QGeraRelFinal.Close;
        DM.QGeraRelFinal.SQL.Clear;
        Dm.QGeraRelFinal.SQL.Text := 'SELECT SUM(QTD) AS QTD, ' +
                                     '       SUM(PRECO) as PRECO, ' +
                                     '       SUM(PRECO*100) AS PRECO_PORCENT, ' +
                                     '       SUM(QTD*100) AS QTD_PORCENT ' +
                                     '  FROM CONTAGENS_CONSO_GERAL_BHC ' +
                                     ' WHERE CAD_CODINT IS NOT NULL';
        DM.QGeraRelFinal.Open;

        DM.QGeraRelFinalDeposito.Close;
        DM.QGeraRelFinalDeposito.SQL.Clear;
        DM.QGeraRelFinalDeposito.SQL.Text := 'SELECT SUM(C.QTD) AS QTD_DEP, ' +
                                             '       SUM(C.PRECO) as PRECO_DEP, ' +
                                             '       SUM(C.PRECO*100) AS PRECO_PORCENT_DEP ' +
                                             '  FROM CONTAGENS_CONSO_GERAL_BHC C ' +
                                             ' INNER JOIN MAPEAMENTO M ON (C.SEC_SECAO BETWEEN M.MAP_SECINI AND M.MAP_SECFIN) ' +
                                             ' WHERE M.DEPOSITO = ''X'' ' +
                                             '   AND C.CAD_CODINT IS NOT NULL' ;
        DM.QGeraRelFinalDeposito.Open;


        DM.QGeraRelFinalLoja.Close;
        DM.QGeraRelFinalLoja.SQL.Clear;
        DM.QGeraRelFinalLoja.SQL.Text := 'SELECT SUM(C.QTD) AS QTD_LOJA, ' +
                                         '       SUM(C.PRECO) AS PRECO_LOJA, ' +
                                         '       SUM(C.PRECO*100) AS PRECO_PORCENT_LOJA ' +
                                         '  FROM CONTAGENS_CONSO_GERAL_BHC C ' +
                                         ' INNER JOIN MAPEAMENTO M ON (C.SEC_SECAO BETWEEN M.MAP_SECINI AND M.MAP_SECFIN) ' +
                                         ' WHERE M.LOJA = ''X'' ' +
                                         '   AND C.CAD_CODINT IS NOT NULL';
        DM.QGeraRelFinalLoja.Open;

        DM.RvPrjWis.Close;
        DM.RvPrjWis.projectfile := 'C:\SGD\Rave\Final_BHC.rav';
        DM.RvPrjWis.Open;
  //      FrmNCadastrado.ShowModal;
        DM.RvSysWis.DefaultDest := rdPreview;
        DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups + [ssAllowSetup];
        DM.RvPrjWis.ExecuteReport('Final_BHC');

        // Salvando relat�rio em PDF automaticamente
        Try
          DM.RvSysWis.DefaultDest := rdFile;
          DM.RvSysWis.DoNativeOutput := False;
          DM.RvSysWis.RenderObject := RvPDF;
          DM.RvSysWis.OutputFileName := 'C:\SGD\Relatorios\Relatorio Total de Pecas e Financeiro_' +
            DM.IBInventarioINV_LOJA.AsString + '_' + DM.IBInventarioINV_NUMLOJA.AsString + '.pdf';
          // caminho onde vai gerar o arquivo pdf
          DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
          DM.RvPrjWis.projectfile := 'C:\SGD\Rave\Final_BHC.rav'; // caminho onde esta o arquivo que ele vai exportar para pdf
          DM.RvPrjWis.Engine := DM.RvSysWis;
          DM.RvPrjWis.Execute;
        Except
          ShowMessage('Erro ao gerar o relat�rio em PDF');
        end;

        // Salvando relat�rio em TXT automaticamente
        Try
          DM.RvSysWis.DefaultDest := rdFile;
          DM.RvSysWis.DoNativeOutput := False;
          DM.RvSysWis.RenderObject := RvTxt;
          DM.RvSysWis.OutputFileName := 'C:\SGD\Relatorios\Relatorio Total de Pecas e Financeiro_' +
            DM.IBInventarioINV_LOJA.AsString + '_' + DM.IBInventarioINV_NUMLOJA.AsString + '.txt';
          // caminho onde vai gerar o arquivo pdf
          DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
          DM.RvPrjWis.projectfile := 'C:\SGD\Rave\Final_BHC.rav'; // caminho onde esta o arquivo que ele vai exportar para pdf
          DM.RvPrjWis.Engine := DM.RvSysWis;
          DM.RvPrjWis.Execute;
        except
          ShowMessage('Erro ao gerar o relat�rio em TXT');
        end;
      end
    Else
      begin
        DM.QGeraRelFinal.Close;
        DM.QGeraRelFinal.Open;
        DM.QGeraRelFinalDeposito.Close;
        DM.QGeraRelFinalDeposito.Open;
        DM.QGeraRelFinalLoja.Close;
        DM.QGeraRelFinalLoja.Open;
        DM.RvPrjWis.Close;
        if Dm.IBInventarioINV_LOJA.Value = 'DME' then
          begin
            DM.RvPrjWis.projectfile := 'C:\SGD\Rave\Final.rav';
            DM.RvPrjWis.Open;
            DM.RvSysWis.DefaultDest := rdPreview;
            DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups + [ssAllowSetup];
            DM.RvPrjWis.ExecuteReport('Final');
          end
        Else
          begin
            DM.RvPrjWis.projectfile := 'C:\SGD\Rave\Final_NCad.rav';
            DM.RvPrjWis.Open;
            FrmNCadastrado.ShowModal;
            DM.RvSysWis.DefaultDest := rdPreview;
            DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups + [ssAllowSetup];
            DM.RvPrjWis.ExecuteReport('Final_NCad');
          end;
      end;

      // Salvando relat�rio em PDF automaticamente
      Try
        DM.RvSysWis.DefaultDest := rdFile;
        DM.RvSysWis.DoNativeOutput := False;
        DM.RvSysWis.RenderObject := RvPDF;
        DM.RvSysWis.OutputFileName := 'C:\SGD\Relatorios\Relatorio Total de Pecas e Financeiro_' +
          DM.IBInventarioINV_LOJA.AsString + '_' + DM.IBInventarioINV_NUMLOJA.AsString + '.pdf';
        // caminho onde vai gerar o arquivo pdf
        DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
        if Dm.IBInventarioINV_LOJA.Value = 'DME' Then
          DM.RvPrjWis.projectfile := 'C:\SGD\Rave\Final.rav' // caminho onde esta o arquivo que ele vai exportar para pdf
        Else
          DM.RvPrjWis.projectfile := 'C:\SGD\Rave\Final_NCad.rav'; // caminho onde esta o arquivo que ele vai exportar para pdf
        DM.RvPrjWis.Engine := DM.RvSysWis;
        DM.RvPrjWis.Execute;
      Except
        ShowMessage('Erro ao gerar o relat�rio em PDF');
      end;

      // Salvando relat�rio em TXT automaticamente
      Try
        DM.RvSysWis.DefaultDest := rdFile;
        DM.RvSysWis.DoNativeOutput := False;
        DM.RvSysWis.RenderObject := RvTxt;
        DM.RvSysWis.OutputFileName := 'C:\SGD\Relatorios\Relatorio Total de Pecas e Financeiro_' +
          DM.IBInventarioINV_LOJA.AsString + '_' + DM.IBInventarioINV_NUMLOJA.AsString + '.txt';
        // caminho onde vai gerar o arquivo pdf
        DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
        if Dm.IBInventarioINV_LOJA.Value = 'DME' Then
          DM.RvPrjWis.projectfile := 'C:\SGD\Rave\Final.rav' // caminho onde esta o arquivo que ele vai exportar para pdf
        Else
          DM.RvPrjWis.projectfile := 'C:\SGD\Rave\Final_NCad.rav';
        DM.RvPrjWis.Engine := DM.RvSysWis;
        DM.RvPrjWis.Execute;
      except
        ShowMessage('Erro ao gerar o relat�rio em TXT');
      end;
       { Application.CreateForm(TFrmTotalPecasFinanceiro, FrmTotalPecasFinanceiro); // cria o form
         FrmTotalPecasFinanceiro.ShowModal;
         FreeAndNil(FrmTotalPecasFinanceiro); // libera o form da memoria }
  //    end;
  //end;
end;

procedure TFrmInventario.xtcomtotaldeHorasdosConferente1Click(Sender: TObject);
Var
  Arquivo1: TStringList;
  contagem, contagem2, auditoria, auditoria2, divergencia, divergencia2, hi, hi2, refeicao, refeicao2, Map, Map2,
    Viagem, Viagem2: Integer;
  valor, valor1, valor2, valor3, valor4, valor5, valor6, ttotal: string;
Begin

  contagem := 0;
  contagem2 := 0;
  auditoria := 0;
  auditoria2 := 0;
  divergencia := 0;
  divergencia2 := 0;
  hi := 0;
  hi2 := 0;
  refeicao := 0;
  refeicao2 := 0;
  Map := 0;
  Map2 := 0;
  Viagem := 0;
  Viagem2 := 0;

  DM.IBHorarios.Close;
  DM.IBHorarios.Open;
  DM.IBHorarios.First;

  Arquivo1 := TStringList.Create; // Instancia a variavel Arquivo

  // GRAVA LINHAS NOS ARQUIVOS

  DM.Query1.Close;
  DM.Query1.SQL.Clear;
  DM.Query1.SQL.Add
    ('SELECT * FROM HORARIO WHERE (HOR_TOTALHORA IS NULL) OR (HOR_SAIDA IS NULL) OR (HOR_ENTRADA IS NULL) ');
  DM.Query1.Open;

  if DM.Query1.RecordCount = 0 then
  begin
    While Not DM.IBHorarios.Eof Do
    Begin
      Arquivo1.Add(DM.IBHorariosROS_MATRICULA.Value + '   ' + DM.IBHorariosHOR_ENTRADA.Value + '   ' +
        DM.IBHorariosHOR_SAIDA.Value + '   ' + DM.IBHorariosHOR_TOTALHORA.Value + '  ' +
        AjustaStrDir(DM.IBHorariosROS_NOME.AsString, 25, ' ') + '  ' + AjustaStrDir(DM.IBHorariosEVENT.AsString,
        15, ' '));
      DM.IBHorarios.Next;
    End;
    Arquivo1.Add('');

    { Dm2.QEventoCont.Close;
      Dm2.QEventoCont.Open;
      Dm2.QEventoCont.First;
      DM2.QEventoCont.Last;

      Dm2.QEventoCont.First; }

    DM.QrySql2.Close;
    DM.QrySql2.SQL.Clear;
    DM.QrySql2.SQL.Add('SELECT HOR_TOTALHORA FROM HORARIO ');
    DM.QrySql2.SQL.Add('WHERE (EVENT = "CONTAGEM")');
    DM.QrySql2.Open;
    DM.QrySql2.First;
    DM.QrySql2.Last;

    If DM.QrySql2.RecordCount > 0 then
    begin
      DM.QrySql2.First;
      while not DM.QrySql2.Eof do
      begin
        contagem := contagem + strtoint(Copy(DM.QrySql2.FieldByName('HOR_TOTALHORA').Value, 1, 2));
        contagem2 := contagem2 + strtoint(Copy(DM.QrySql2.FieldByName('HOR_TOTALHORA').Value, 4, 2));
        if contagem2 > 59 then
        begin
          contagem2 := contagem2 - 60;
          contagem := contagem + 1;
        end;
        DM.QrySql2.Next;
      end;

    end;

    valor := AjustaStrEsq(inttostr(contagem), 4, ' ') + ':' + AjustaStrEsq(inttostr(contagem2), 2, '0');
    Arquivo1.Add('Total de horas CONTAGEM:       ' + valor);

    { Dm2.QEventoAudit.Close;
      Dm2.QEventoAudit.Open;
      Dm2.QEventoAudit.First;
      Dm2.QEventoAudit.Last;

      Dm2.QEventoAudit.First; }

    DM.qrySQL.Close;
    DM.qrySQL.SQL.Clear;
    DM.qrySQL.SQL.Add('SELECT HOR_TOTALHORA FROM HORARIO ');
    DM.qrySQL.SQL.Add('WHERE (EVENT = "AUDITORIA")');
    DM.qrySQL.Open;
    DM.qrySQL.First;
    DM.qrySQL.Last;

    DM.qrySQL.First;
    If DM.qrySQL.RecordCount > 0 then
    begin
      while not DM.qrySQL.Eof do
      begin
        auditoria := auditoria + strtoint(Copy(DM.qrySQL.FieldByName('HOR_TOTALHORA').Value, 1, 2));
        auditoria2 := auditoria2 + strtoint(Copy(DM.qrySQL.FieldByName('HOR_TOTALHORA').Value, 4, 2));
        if auditoria2 > 59 then
        begin
          auditoria2 := auditoria2 - 60;
          auditoria := auditoria + 1;
        end;
        DM.qrySQL.Next;
      end;

    end;

    valor1 := AjustaStrEsq(inttostr(auditoria), 4, ' ') + ':' + AjustaStrEsq(inttostr(auditoria2), 2, '0');
    Arquivo1.Add('Total de horas AUDITORIA:      ' + valor1);

    { Dm2.QEventoDiv.Close;
      Dm2.QEventoDiv.Open;
      Dm2.QEventoDiv.First;
      Dm2.QEventoDiv.Last;

      Dm2.QEventoDiv.First; }

    DM.qrySQL.Close;
    DM.qrySQL.SQL.Clear;
    DM.qrySQL.SQL.Add('SELECT HOR_TOTALHORA FROM HORARIO ');
    DM.qrySQL.SQL.Add('WHERE (EVENT = "DIVERGENCIA")');
    DM.qrySQL.Open;
    DM.qrySQL.First;
    DM.qrySQL.Last;

    DM.qrySQL.First;
    If DM.qrySQL.RecordCount > 0 then
    begin
      while not DM.qrySQL.Eof do
      begin
        divergencia := divergencia + strtoint(Copy(DM.qrySQL.FieldByName('HOR_TOTALHORA').Value, 1, 2));
        divergencia2 := divergencia2 + strtoint(Copy(DM.qrySQL.FieldByName('HOR_TOTALHORA').Value, 4, 2));
        if divergencia2 > 59 then
        begin
          divergencia2 := divergencia2 - 60;
          divergencia := divergencia + 1;
        end;
        DM.qrySQL.Next;
      end;

    end;

    valor2 := AjustaStrEsq(inttostr(divergencia), 4, ' ') + ':' + AjustaStrEsq(inttostr(divergencia2), 2, '0');
    Arquivo1.Add('Total de horas DIVERG�NCIA:    ' + valor2);

    { Dm2.QEventoHI.Close;
      Dm2.QEventoHI.Open;
      DM2.QEventoHI.First;
      Dm2.QEventoHI.Last;

      Dm2.QEventoHI.First; }

    DM.qrySQL.Close;
    DM.qrySQL.SQL.Clear;
    DM.qrySQL.SQL.Add('SELECT HOR_TOTALHORA FROM HORARIO ');
    DM.qrySQL.SQL.Add('WHERE (EVENT = "H.I.")');
    DM.qrySQL.Open;
    DM.qrySQL.First;
    DM.qrySQL.Last;

    DM.qrySQL.First;
    If DM.qrySQL.RecordCount > 0 then
    begin
      while not DM.qrySQL.Eof do
      begin
        hi := hi + strtoint(Copy(DM.qrySQL.FieldByName('HOR_TOTALHORA').Value, 1, 2));
        hi2 := hi2 + strtoint(Copy(DM.qrySQL.FieldByName('HOR_TOTALHORA').Value, 4, 2));
        if hi2 > 59 then
        begin
          hi2 := hi2 - 60;
          hi := hi + 1;
        end;
        DM.qrySQL.Next;
      end;

    end;

    valor3 := AjustaStrEsq(inttostr(hi), 4, ' ') + ':' + AjustaStrEsq(inttostr(hi2), 2, '0');
    Arquivo1.Add('Total de horas IMPRODUTIVAS:   ' + valor3);

    { Dm2.QEventoRef.Close;
      Dm2.QEventoRef.Open;
      Dm2.QEventoRef.First;
      Dm2.QEventoRef.Last;

      Dm2.QEventoRef.First; }

    DM.qrySQL.Close;
    DM.qrySQL.SQL.Clear;
    DM.qrySQL.SQL.Add('SELECT HOR_TOTALHORA FROM HORARIO ');
    DM.qrySQL.SQL.Add('WHERE (EVENT = "REFEICAO")');
    DM.qrySQL.Open;
    DM.qrySQL.First;
    DM.qrySQL.Last;

    DM.qrySQL.First;
    If DM.qrySQL.RecordCount > 0 then
    begin
      while not DM.qrySQL.Eof do
      begin
        refeicao := refeicao + strtoint(Copy(DM.qrySQL.FieldByName('HOR_TOTALHORA').Value, 1, 2));
        refeicao2 := refeicao2 + strtoint(Copy(DM.qrySQL.FieldByName('HOR_TOTALHORA').Value, 4, 2));
        if refeicao2 > 59 then
        begin
          refeicao2 := refeicao2 - 60;
          refeicao := refeicao + 1;
        end;
        DM.qrySQL.Next;
      end;

    end;

    valor4 := AjustaStrEsq(inttostr(refeicao), 4, ' ') + ':' + AjustaStrEsq(inttostr(refeicao2), 2, '0');
    Arquivo1.Add('Total de horas REFEI��O:       ' + valor4);

    DM.QrySql2.Close;
    DM.QrySql2.SQL.Clear;
    DM.QrySql2.SQL.Add('SELECT HOR_TOTALHORA FROM HORARIO ');
    DM.QrySql2.SQL.Add('WHERE (EVENT = "MAPEAR/EQUIPAMENTO")');
    DM.QrySql2.Open;
    DM.QrySql2.First;
    DM.QrySql2.Last;

    If DM.QrySql2.RecordCount > 0 then
    begin
      DM.QrySql2.First;
      while not DM.QrySql2.Eof do
      begin
        Map := Map + strtoint(Copy(DM.QrySql2.FieldByName('HOR_TOTALHORA').Value, 1, 2));
        Map2 := Map2 + strtoint(Copy(DM.QrySql2.FieldByName('HOR_TOTALHORA').Value, 4, 2));
        if Map2 > 59 then
        begin
          Map2 := Map2 - 60;
          Map := Map + 1;
        end;
        DM.QrySql2.Next;
      end;

    end;

    valor5 := AjustaStrEsq(inttostr(Map), 4, ' ') + ':' + AjustaStrEsq(inttostr(Map2), 2, '0');
    Arquivo1.Add('Total de horas MAPEANDO/EQUIP.:' + valor5);

    DM.qrySQL.Close;
    DM.qrySQL.SQL.Clear;
    DM.qrySQL.SQL.Add('SELECT HOR_TOTALHORA FROM HORARIO ');
    DM.qrySQL.SQL.Add('WHERE (EVENT = "VIAGEM")');
    DM.qrySQL.Open;
    DM.qrySQL.First;
    DM.qrySQL.Last;

    DM.qrySQL.First;
    If DM.qrySQL.RecordCount > 0 then
    begin
      while not DM.qrySQL.Eof do
      begin
        Viagem := Viagem + strtoint(Copy(DM.qrySQL.FieldByName('HOR_TOTALHORA').Value, 1, 2));
        Viagem2 := Viagem2 + strtoint(Copy(DM.qrySQL.FieldByName('HOR_TOTALHORA').Value, 4, 2));
        if Viagem2 > 59 then
        begin
          Viagem2 := Viagem2 - 60;
          Viagem := Viagem + 1;
        end;
        DM.qrySQL.Next;
      end;

    end;

    valor6 := AjustaStrEsq(inttostr(Viagem), 4, ' ') + ':' + AjustaStrEsq(inttostr(Viagem2), 2, '0');
    Arquivo1.Add('Total de horas Viagem:         ' + valor6);

    Arquivo1.Add('');
    Arquivo1.Add('');
    ttotal := TotalHoras;
    Arquivo1.Add('Total de Horas: ' + ttotal + '     Total Pessoas: ' + EdtPessoas.Text);
    Arquivo1.SaveToFile('C:\SGD\Horas Conferentes\Horas_OS ' + DM.IBInventarioINV_OS.Value + '.TXT');
    FreeAndNil(Arquivo1);
    Application.MessageBox('Arquivo de horas gerados com sucesso', 'Mensagem do sistema', MB_ICONINFORMATION + MB_OK);
  end
  else
  begin
    Application.MessageBox('Existem hor�rios abertos, por gentileza feche todos os hor�rios para gerar o arquivo!',
      'Mensagem do Sistema', MB_ICONERROR + MB_OK);
  end;

end;

procedure TFrmInventario.TabSheet4Show(Sender: TObject);
begin
  if Panel11.Enabled = True then
    DBGrid2.SetFocus;

  // Abre e fecha Transmiss�es
  DM.IBTransmissao.Close;
  DM.IBTransmissao.Open;
end;

procedure TFrmInventario.RelatriosdeAuditoria1Click(Sender: TObject);
begin
  Application.CreateForm(TFrmSelSecao, FrmSelSecao); // cria o form
  FrmSelSecao.ShowModal;
  FreeAndNil(FrmSelSecao); // libera o form da memoria
end;

procedure TFrmInventario.AtalhosdoSistema1Click(Sender: TObject);
begin
  ShowMessage('Tela de Se��es: ' + #13 + 'F1 = Imprimir relat�rio de Auditoria ' + #13 + 'F3 = Tela de Pesquisa ' + #13
    + 'F4 = Aceita Duplicidade ' + #13 + 'F5 = Insere se��o no mapeamento ' + #13 + 'Ctrl + Del = Deleta Se��o ' + #13 +
    'Alt + A = Ativa visualiza��o apenas das contagens da se��o selecionada ' + #13 + ' ' + #13 + 'Tela de Contagem: ' +
    #13 + 'F3 = Tela de Pesquisa ' + #13 + 'Ctrl + Del = Deleta Contagem ');

end;

procedure TFrmInventario.TabSheet1Show(Sender: TObject);
begin
  If Panel4.Enabled = True then
    Panel4.SetFocus;

  DM.QGrafico.Close;
  DM.QGrafico.Open;
  DM.IBInventario.Open;
  DM.IBInventario.Edit;
end;

procedure TFrmInventario.DesabilitarimportaoAutomatica1Click(Sender: TObject);
begin
  If Timer1.Enabled = True then
  Begin
    Timer1.Enabled := False;
    MainMenu1.Items[0].Items[1].Caption := 'Habilitar Importa��o das Transmiss�es';
    Application.MessageBox('O sistema n�o importar� mais as coletas descarregadas pelo coletor', 'Mensagem do sistema',
      MB_ICONEXCLAMATION + MB_OK);
  End
  Else
  Begin
    Timer1.Enabled := True;
    MainMenu1.Items[0].Items[1].Caption := 'Desabilitar Importa��o das Transmiss�es';
    Application.MessageBox('O sistema importar� automaticamente as coletas descarregadas pelo coletor',
      'Mensagem do sistema', MB_ICONEXCLAMATION + MB_OK);

  End;
end;

procedure TFrmInventario.RelatriodePsicount1Click(Sender: TObject);
begin
  Try
    DM.QRelPiceCount.Close;
    DM.QRelPiceCount.Open;
    if DM.QRelPiceCount.RecordCount > 0 then
    Begin
      DM.RvPrjWis.Close;
      DM.RvPrjWis.projectfile := 'C:\SGD\Rave\PiceCount.rav';
      DM.RvPrjWis.Open;
      DM.RvSysWis.DefaultDest := rdPreview;
      DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
      DM.RvPrjWis.ExecuteReport('PiceCount');
      // Salvando relat�rio em PDF automaticamente
      Try
        DM.RvSysWis.DefaultDest := rdFile;
        DM.RvSysWis.DoNativeOutput := False;
        DM.RvSysWis.RenderObject := RvPDF;
        DM.RvSysWis.OutputFileName := 'C:\SGD\Relatorios\Relatorio PieceCount_' + DM.IBInventarioINV_LOJA.AsString + '_'
          + DM.IBInventarioINV_NUMLOJA.AsString + '.pdf'; // caminho onde vai gerar o arquivo pdf
        DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
        DM.RvPrjWis.projectfile := 'C:\SGD\Rave\PiceCount.rav';
        // caminho onde esta o arquivo que ele vai exportar para pdf
        DM.RvPrjWis.Engine := DM.RvSysWis;
        DM.RvPrjWis.Execute;
      Except
        ShowMessage('Erro ao gerar o relat�rio em PDF');
      end;
      // Salvando relat�rio em TXT automaticamente

      Try
        DM.RvSysWis.DefaultDest := rdFile;
        DM.RvSysWis.DoNativeOutput := False;
        DM.RvSysWis.RenderObject := RvTxt;
        DM.RvSysWis.OutputFileName := 'C:\SGD\Relatorios\Relatorio PieceCount_' + DM.IBInventarioINV_LOJA.AsString + '_'
          + DM.IBInventarioINV_NUMLOJA.AsString + '.txt'; // caminho onde vai gerar o arquivo pdf
        DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
        DM.RvPrjWis.projectfile := 'C:\SGD\Rave\PiceCount.rav';
        // caminho onde esta o arquivo que ele vai exportar para pdf
        DM.RvPrjWis.Engine := DM.RvSysWis;
        DM.RvPrjWis.Execute;
      except
        ShowMessage('Erro ao gerar o relat�rio em TXT');
      end;
    end

    else
      Application.MessageBox('N�o existem se��es contadas', 'Mensagem do sistema', MB_ICONERROR + MB_OK);
  Finally
    DM.QRelPiceCount.Close;
  End;
end;

procedure TFrmInventario.TabSheet2Show(Sender: TObject);
begin

  if Panel9.Enabled = True then
    Edit4.SetFocus;

end;

procedure TFrmInventario.FormActivate(Sender: TObject);
begin
  // Alinha Panel
  AlinharPanel(Panel10, Panel1, True);

end;

procedure TFrmInventario.RelatriodeDuplicidades1Click(Sender: TObject);
begin
  Try
    DM.QRelDupli.Close;
    DM.QRelDupli.Open;
    if DM.QRelDupli.RecordCount > 0 then
    begin
      DM.RvPrjWis.Close;
      DM.RvPrjWis.projectfile := 'C:\SGD\Rave\Duplicidade.rav';
      DM.RvPrjWis.Open;
      DM.RvSysWis.DefaultDest := rdPreview;
      DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
      DM.RvPrjWis.ExecuteReport('Duplicidade');
      // Salvando relat�rio em PDF automaticamente
      Try
        DM.RvSysWis.DefaultDest := rdFile;
        DM.RvSysWis.DoNativeOutput := False;
        DM.RvSysWis.RenderObject := RvPDF;
        DM.RvSysWis.OutputFileName := 'C:\SGD\Relatorios\Relatorio de Duplicidades_' + DM.IBInventarioINV_LOJA.AsString
          + '_' + DM.IBInventarioINV_NUMLOJA.AsString + '.pdf'; // caminho onde vai gerar o arquivo pdf
        DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
        DM.RvPrjWis.projectfile := 'C:\SGD\Rave\Duplicidade.rav';
        // caminho onde esta o arquivo que ele vai exportar para pdf
        DM.RvPrjWis.Engine := DM.RvSysWis;
        DM.RvPrjWis.Execute;
      Except
        ShowMessage('Erro ao gerar o relat�rio em PDF');
      end;
      // Salvando relat�rio em TXT automaticamente

      Try
        DM.RvSysWis.DefaultDest := rdFile;
        DM.RvSysWis.DoNativeOutput := False;
        DM.RvSysWis.RenderObject := RvTxt;
        DM.RvSysWis.OutputFileName := 'C:\SGD\Relatorios\Relatorio de Duplicidades_' + DM.IBInventarioINV_LOJA.AsString
          + '_' + DM.IBInventarioINV_NUMLOJA.AsString + '.txt'; // caminho onde vai gerar o arquivo pdf
        DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
        DM.RvPrjWis.projectfile := 'C:\SGD\Rave\Duplicidade.rav';
        // caminho onde esta o arquivo que ele vai exportar para pdf
        DM.RvPrjWis.Engine := DM.RvSysWis;
        DM.RvPrjWis.Execute;
      except
        ShowMessage('Erro ao gerar o relat�rio em TXT');
      end;
    end
    else
      Application.MessageBox('N�o existem se��es duplicadas', 'Mensagem do sistema', MB_ICONEXCLAMATION + MB_OK);
  Finally
    DM.QRelDupli.Close;
  End;

end;

procedure TFrmInventario.RelatriodeSeesAbertas1Click(Sender: TObject);
begin
  Try
    DM.QRelAberta.Close;
    DM.QRelAberta.Open;
    if DM.QRelAberta.RecordCount > 0 then
    begin
      DM.RvPrjWis.Close;
      DM.RvPrjWis.projectfile := 'C:\SGD\Rave\Abertas.rav';
      DM.RvPrjWis.Open;
      DM.RvSysWis.DefaultDest := rdPreview;
      DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
      DM.RvPrjWis.ExecuteReport('Abertas');
      // Salvando relat�rio em PDF automaticamente
      Try
        DM.RvSysWis.DefaultDest := rdFile;
        DM.RvSysWis.DoNativeOutput := False;
        DM.RvSysWis.RenderObject := RvPDF;
        DM.RvSysWis.OutputFileName := 'C:\SGD\Relatorios\Relatorio de Secoes Abertas_' +
          DM.IBInventarioINV_LOJA.AsString + '_' + DM.IBInventarioINV_NUMLOJA.AsString + '.pdf';
        // caminho onde vai gerar o arquivo pdf
        DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
        DM.RvPrjWis.projectfile := 'C:\SGD\Rave\Abertas.rav';
        // caminho onde esta o arquivo que ele vai exportar para pdf
        DM.RvPrjWis.Engine := DM.RvSysWis;
        DM.RvPrjWis.Execute;
      Except
        ShowMessage('Erro ao gerar o relat�rio em PDF');
      end;
      // Salvando relat�rio em TXT automaticamente

      Try
        DM.RvSysWis.DefaultDest := rdFile;
        DM.RvSysWis.DoNativeOutput := False;
        DM.RvSysWis.RenderObject := RvTxt;
        DM.RvSysWis.OutputFileName := 'C:\SGD\Relatorios\Relatorio de Secoes Abertas_' +
          DM.IBInventarioINV_LOJA.AsString + '_' + DM.IBInventarioINV_NUMLOJA.AsString + '.txt';
        // caminho onde vai gerar o arquivo pdf
        DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
        DM.RvPrjWis.projectfile := 'C:\SGD\Rave\Abertas.rav';
        // caminho onde esta o arquivo que ele vai exportar para pdf
        DM.RvPrjWis.Engine := DM.RvSysWis;
        DM.RvPrjWis.Execute;
      except
        ShowMessage('Erro ao gerar o relat�rio em TXT');
      end;
    end
    else
      Application.MessageBox('N�o existem se��es abertas', 'Mensagem do sistema', MB_ICONEXCLAMATION + MB_OK);
  Finally
    DM.QRelAberta.Close;
  End;

end;

procedure TFrmInventario._S(Sender: TObject; var Key: Char);
var
  SecaoNova, SecaoAntiga, matricula: String;
  SecaoId, SecTransId, tinhaDUP, Id_secao: Integer;
  NumCaracteres: Integer;
  Secao, AREA: string;
  Inesperada: String[1];
begin
  // N�O DEIXA DELETAR A LINHA DIRETO DO BANCO

  IDCONTAGEM := DM.IBContagensCON_ID.Value;
  MouseShowCursor(False);

  if (Key = #9) and (DM.DsContagens.State <> Dsedit) then
  begin
    MouseShowCursor(True);
  end;
  // Quando � alterado uma quantidade e pressionado ESC,volta a quantidade original
  if (Key = #27) and (DM.DsContagens.State <> Dsedit) then
  begin
    DM.IBContagens.Cancel;
    PosicionaContagem(IDCONTAGEM);
    MouseShowCursor(True);
  end;
  if (Key = #27) and (DM.DsContagens.State = Dsedit) then
  begin
    MouseShowCursor(True);
  end;

  // Quando � alterado uma quantidade e pressionado ENTER,grava a nova quantidade
  if (Key = #13) and (DM.DsContagens.State = Dsedit) and (DM.IBContagensCON_STATUS.Value <> 'DEL') then
  Begin
    GravaAltDbGridContagem;
    DM.IBContagens.locate('CON_ID', IDCONTAGEM, [Lopartialkey, locaseinsensitive]);
    MouseShowCursor(True);

    // evandro novo total de pe�as
    If (DM.IBInventarioINV_LOJA.Value = 'PDA') or (DM.IBInventarioINV_LOJA.Value = 'ASA') then
    Begin
      DM.QGeraRelFinal.Close;
      DM.QGeraRelFinal.SQL.Clear;
      DM.QGeraRelFinal.SQL.Add
        ('SELECT SUM(ROUND(D.QTD)) AS QTD,SUM(D.PRECO) AS PRECO,SUM(PRECO*100) AS PRECO_PORCENT,SUM((ROUND(D.QTD))*100) AS QTD_PORCENT FROM CONTAGENS_CONSO D INNER JOIN CADASTRO C ON (C.CAD_EAN = D.CON_EAN) WHERE C.CAD_CODINT IS NOT NULL');
      DM.QGeraRelFinal.Open;
      LabelTotalPecas.Caption := FormatFloat('##,###0.000', DM.QGeraRelFinal.FieldByName('QTD').Value);
      LabelTotalFin.Caption := FormatFloat('R$##,###0.00', DM.QGeraRelFinal.FieldByName('PRECO').Value);
    end;
  End;

  if (DBGrid5.SelectedField.FieldName = 'SEC_SECAO') THEN
    Key := AnsiUpperCase(Key)[Length(Key)];

  { If (Key = #27) then
    begin

    end; }

  // Alterar Se��o com tecla Enter

  If (FrmInventario.DBGrid5.Fields[3].NewValue <> FrmInventario.DBGrid5.Fields[3].OldValue) then
  Begin
    // CASO CLIENTE N�O TENHA ENDERE�O, A SE��O ACEITARA SOMENTE NUMEROS
    with DM.qrySQL do
    begin
      Close;
      SQL.Clear;
      SQL.Add('SELECT * FROM LAYOUT_INPUT');
      SQL.Add('WHERE CLIENTE = :CLIENTE');
      Params.ParamByName('CLIENTE').Value := FrmInventario.DBEdit2.Text;
      Open;
    end;

    // if DM.qrySQL.FieldByName('ENDERECO').Value <> 'T' then
    // begin
    if not(Key in ['0' .. '9', Chr(8)]) then
      Key := #0;
    // end;

    DM.IBSecao.Open;

    DM.IBSecao.Edit;

    SecTransId := DM.IBContagensTRA_ID.Value;
    SecaoAntiga := DM.IBContagensSEC_SECAO.OldValue;
    SecaoId := DM.IBContagensCON_ID.Value;
    matricula := DM.IBContagensHOR_MATRICULA.Value;
    SecaoNova := (DM.IBContagensSEC_SECAO.Value);
    NumCaracteres := Length(SecaoNova);
    Secao := (SecaoNova);

    With DM.qrySQL do
    begin
      Close;
      SQL.Clear;
      SQL.Add('SELECT * FROM LAYOUT_INPUT WHERE CLIENTE = :CLIENTE');
      ParamByName('CLIENTE').Value := DM.IBInventarioINV_LOJA.Value;
      Open;
    end;

    // if DM.qrySQL.FieldByName('ENDERECO').asString <> 'T' then
    // Begin
    If (NumCaracteres <> 4) or (Secao < '0001') or (BoolToStr(IntOK) = '-1') then
    Begin
      DM.Transacao.RollbackRetaining;
      DM.IBSecao.Cancel;
      MouseShowCursor(True);
    End
    else
    begin
      try
        SecaoNova := DM.IBSecaoSEC_SECAO.Value;

        DM.IBSecao.Post;

        // ShowMessage(SecaoAntiga);
        // ShowMessage(SecaoNova);
        DM.SpAlteraSec.Close;
        DM.SpAlteraSec.ParamByName('NOVASEC').Value := SecaoNova;
        DM.SpAlteraSec.ParamByName('VELHASEC').Value := SecaoAntiga;
        DM.SpAlteraSec.ParamByName('TRANSID').Value := SecTransId;
        DM.SpAlteraSec.ParamByName('ID_SECAO').Value := SecaoId;

        DM.SpAlteraSec.ExecProc;
        Application.ProcessMessages;
        // GravaAltDbGridSecao(SecaoID, SecTransID, Matricula, SecaoAntiga, SecaoNova);

        CriaSecaoNova(SecaoNova, SecaoAntiga);

        Duplicidade(SecaoAntiga);

        Duplicidade(SecaoNova);

        PosicionaSecao(IDSECAO);

        AtualizaInv;

        DM.Transacao.CommitRetaining;

        MouseShowCursor(True);
        Application.ProcessMessages;
        // end;
      except
        ShowMessage('Erro na altera��o, por favor tente novamente');
        DM.Transacao.RollbackRetaining;
        DM.IBSecao.Edit;
        DM.IBSecaoSEC_SECAO.Value := SecaoAntiga;
        MouseShowCursor(True);
      end;

    end;
    // end;

    { Else
      begin
      try
      SecaoNova := Dm.IBSecaoSEC_SECAO.value;

      Dm.IBSecao.Post;

      //ShowMessage(SecaoAntiga);
      //ShowMessage(SecaoNova);
      Dm.SpAlteraSec.Close;
      Dm.SpAlteraSec.ParamByName('NOVASEC').Value := SecaoNova;
      Dm.SpAlteraSec.ParamByName('VELHASEC').Value := SecaoAntiga;
      Dm.SpAlteraSec.ParamByName('TRANSID').Value := SecTransID;
      Dm.SpAlteraSec.ParamByName('ID_SECAO').Value := SecaoID;

      Dm.SpAlteraSec.ExecProc;
      Application.ProcessMessages;
      //GravaAltDbGridSecao(SecaoID, SecTransID, Matricula, SecaoAntiga, SecaoNova);

      CriaSecaoNova(SecaoNova, SecaoAntiga);

      Duplicidade(SecaoAntiga);

      Duplicidade(SecaoNova);

      PosicionaSecao(IDSECAO);

      AtualizaInv;

      Dm.Transacao.CommitRetaining;
      Application.ProcessMessages;
      MouseShowCursor(True);
      // end;
      except
      ShowMessage('Erro na altera��o, por favor tente novamente');
      Dm.Transacao.RollbackRetaining;
      DM.IBSecao.Edit;
      DM.IBSecaoSEC_SECAO.Value := SecaoAntiga;
      MouseShowCursor(True);
      end;

      end; }

    // #######Importa da Query AREAS
    Try

      DM.QGeraArea.Close;
      DM.QGeraArea.ParamByName('TransID').Value := SecTransId;
      DM.QGeraArea.Open;
      DM.QGeraArea.First;

      While not DM.QGeraArea.Eof do
      Begin
        DM.IBArea.Insert;
        DM.IBAreaARE_AREA.Value := DM.QGeraAreaARE_AREA.Value;
        DM.IBAreaSEC_SECAO.Value := DM.QGeraAreaSEC_SECAO.Value;
        DM.IBAreaTRA_ID.Value := DM.QGeraAreaTRA_ID.Value;
        DM.IBArea.Post;
        DM.QGeraArea.Next;
        Application.ProcessMessages;
      End;
      DM.Transacao.CommitRetaining;
      DM.QGeraArea.Close;
    Except
      ShowMessage('Problema na importa��o das areas');
    end;
    // ShowMessage('secao');
    // #######Importa da Query SE��ES
    Try

      DM.QGeraSecao.Close;
      DM.QGeraSecao.ParamByName('TransID').Value := SecTransId;
      DM.QGeraSecao.Open;
      DM.QGeraSecao.First;

      While not DM.QGeraSecao.Eof do
      Begin

        DM.QVerificaSecMap.Close;
        Secao := DM.QGeraSecaoSEC_SECAO.Value;
        AREA := DM.QGeraSecaoARE_AREA.Value;
        tinhaDUP := 0;
        // Se Se��o j� tiver cadastrada
        If DM.IBSecao.locate('SEC_SECAO', Secao, [Lopartialkey, locaseinsensitive]) = True then
        Begin

          // Se tiver como aberta
          If DM.IBSecaoSEC_ABERTA.Value = 'T' then
          Begin

            DM.IBSecao.Edit;
            DM.IBSecaoSEC_ABERTA.Value := 'F';
            DM.IBSecaoSEC_DUPLI.Value := 'F';
            DM.ibSecaoSEC_INESPERADA.Value := 'F';
            DM.IBSecaoSEC_AUDITADA.Value := 'N';
            DM.IBSecaoTRA_ID.Value := DM.QGeraSecaoTRA_ID.Value;
            DM.IBSecaoHOR_MATRICULA.Value := DM.QGeraSecaoHOR_MATRICULA.Value;
            DM.IBSecao.Post;
            DM.Transacao.CommitRetaining;
          End
          else // se n�o tiver como ABERTA
          Begin
            DM.QVerificaSecMap.Close;
            DM.QVerificaSecMap.ParamByName('secao').Value := Secao;
            DM.QVerificaSecMap.Open;

            if DM.QVerificaSecMap.RecordCount > 0 then
              Inesperada := 'F'
            else
              Inesperada := 'T';

            // Roda todas as se��es iguais
            While not(DM.IBSecao.Eof) and (DM.IBSecaoSEC_SECAO.Value = Secao) do
            Begin
              // Caso seja uma se��o de uma mesma transmiss�o n�o roda as areas para achar DUPLI
              If not(DM.IBSecaoTRA_ID.Value = SecTransId) then
              Begin
                While not(DM.IBArea.Eof) do
                Begin
                  If (DM.IBAreaARE_AREA.Value = AREA) then
                  // and (Dm.IBSecaoSEC_STATUS.Value = '.') then
                  Begin
                    DM.IBSecao.Edit;
                    DM.IBSecaoSEC_DUPLI.Value := 'T';
                    DM.IBSecao.Post;
                    if (DM.IBSecaoSEC_STATUS.Value = '.') then
                      tinhaDUP := 1
                    else
                      tinhaDUP := 2;
                    Break;
                  End;
                  DM.IBArea.Next;
                End;
              End;
              DM.IBSecao.Next;
            End;
            // Verifica se j� esta cadastrado uma se��o com o mesmo TransId, caso n�o esteja cadastra
            If DM.IBSecao.locate('Tra_ID;Sec_Secao', VarArrayOf([(SecTransId), Secao]),
              [Lopartialkey, locaseinsensitive]) = False Then
            Begin
              // evandro
              { Dm.qrySQL.Close;
                DM.qrySQL.SQL.Clear;
                DM.qrySQL.SQL.Add('select coalesce(max(sec_id),0) as ID from secao');
                DM.qrySQL.Open;
                Id_secao := 0;
                Id_secao := DM.qrySQL.FieldByName('ID').AsInteger + 1; }

              sec := True;
              DM.IBSecao.Append;
              // DM.IBSecaoSEC_ID.Value := id_secao;
              DM.IBSecaoSEC_SECAO.Value := Secao;
              DM.IBSecaoSEC_ABERTA.Value := 'F';
              DM.ibSecaoSEC_INESPERADA.Value := Inesperada;
              DM.IBSecaoSEC_AUDITADA.Value := 'N';

              if tinhaDUP = 1 then
                DM.IBSecaoSEC_DUPLI.Value := 'T'
              else if tinhaDUP = 2 then
                DM.IBSecaoSEC_DUPLI.Value := '*'
              else
                DM.IBSecaoSEC_DUPLI.Value := 'F';
              DM.IBSecaoTRA_ID.Value := DM.QGeraSecaoTRA_ID.Value;
              DM.IBSecaoHOR_MATRICULA.Value := DM.QGeraSecaoHOR_MATRICULA.Value;
              DM.IBSecaoSEC_STATUS.Value := '.';
              DM.IBSecao.Post;
              sec := False;
            End;
          End;
        End
        Else // Se n�o tiver a se��o na tabela
        Begin

          // evandro
          { Dm.qrySQL.Close;
            DM.qrySQL.SQL.Clear;
            DM.qrySQL.SQL.Add('select coalesce(max(sec_id),0) as ID from secao');
            DM.qrySQL.Open;
            Id_secao := 0;
            Id_secao := DM.qrySQL.FieldByName('ID').AsInteger + 1; }

          sec := True;
          DM.IBSecao.Append;
          // DM.IBSecaoSEC_ID.Value := id_secao;
          DM.IBSecaoSEC_SECAO.Value := Secao;
          DM.IBSecaoSEC_ABERTA.Value := 'F';
          DM.ibSecaoSEC_INESPERADA.Value := 'T';
          DM.IBSecaoSEC_DUPLI.Value := 'F';
          DM.IBSecaoSEC_AUDITADA.Value := 'N';
          DM.IBSecaoTRA_ID.Value := DM.QGeraSecaoTRA_ID.Value;
          DM.IBSecaoHOR_MATRICULA.Value := DM.QGeraSecaoHOR_MATRICULA.Value;
          DM.IBSecaoSEC_STATUS.Value := '.';
          DM.IBSecao.Post;
          DM.Transacao.CommitRetaining;
          sec := False;

        End;
        DM.SpAlteraSec.Close;
        DM.SpAlteraSec.ParamByName('NOVASEC').Value := Secao;
        DM.SpAlteraSec.ParamByName('VELHASEC').Value := Secao;
        DM.SpAlteraSec.ParamByName('TRANSID').Value := DM.QGeraSecaoTRA_ID.Value;
        DM.SpAlteraSec.ParamByName('ID_SECAO').Value := DM.IBSecaoSEC_ID.Value;
        DM.SpAlteraSec.ExecProc;
        Application.ProcessMessages;
        DM.QGeraSecao.Next;

        Duplicidade(Secao);

      End;
      DM.QGeraSecao.Close;

      PosicionaSecao(IDSECAO);

      AtualizaInv;
      // DM.IBSecao.Close;
      // DM.IBSecao.ParamByName('SECINI').Value := '0000';
      // DM.IBSecao.ParamByName('SECFIN').Value := '9999';
      // Dm.IBSecao.Open;
      // ShowMessage('final');
      DM.Transacao.CommitRetaining;
    Except
      ShowMessage('Problema na importa��o das se��es');
    end;
    DM.IBContagens.locate('CON_ID', IDCONTAGEM, [Lopartialkey, locaseinsensitive]);
  end
  else if (DM.IBSecao.State in [Dsedit, dsinsert]) and (Key = #13) then
  Begin
    DM.IBSecao.Cancel;
    MouseShowCursor(True);
    DM.IBContagens.locate('CON_ID', IDCONTAGEM, [Lopartialkey, locaseinsensitive]);
  End;

End;

procedure TFrmInventario.DBGrid3KeyPress(Sender: TObject; var Key: Char);
var
  SecaoNova, SecaoAntiga, matricula: String;
  SecaoId, SecTransId: Integer;
  NumCaracteres: Integer;
  Secao: string;
begin

  // N�O DEIXA DELETAR A LINHA DIRETO DO BANCO
  if (not(Char(Key) in ['0' .. '9', '', #127, #32, #13, #8, #27])) then
    Abort;

  MouseShowCursor(False);

  if (DBGrid3.SelectedField.FieldName = 'SEC_SECAO') THEN
    Key := AnsiUpperCase(Key)[Length(Key)];

  If (Key = #27) then
  begin
    MouseShowCursor(True);
  end;

  // Alterar Se��o com tecla Enter
  If (Key = #13) and (DM.DsSecao.State = Dsedit) and (DM.IBSecaoSEC_STATUS.Value = '.') and
    (DM.IBSecaoSTATUS.Value <> 'ABE') then
  Begin

    DM.IBSecao.Open;

    DM.IBSecao.Edit;

    SecTransId := DM.IBSecaoTRA_ID.Value;

    SecaoAntiga := DM.IBSecaoSEC_SECAO.OldValue;

    SecaoId := DM.IBSecaoSEC_ID.Value;

    matricula := DM.IBSecaoHOR_MATRICULA.Value;

    SecaoNova := (DM.IBSecaoSEC_SECAO.Value);

    NumCaracteres := Length(SecaoNova);

    Secao := (SecaoNova);

    MouseShowCursor(True);

    with DM.qrySQL do
    begin
      Close;
      SQL.Clear;
      SQL.Add('SELECT SEC_SECAO, TRA_ID FROM SECAO ');
      SQL.Add('WHERE SEC_SECAO = :SEC_SECAO AND TRA_ID = :TRA_ID');
      ParamByName('SEC_SECAO').Value := Secao;
      ParamByName('TRA_ID').Value := SecTransId;
      Open;
    end;

    if DM.qrySQL.RecordCount = 0 THEN
    BEGIN
      if MessageDlg('Voc� esta prestes a alterar SE��O: ' + SecaoAntiga + ' para SE��O: ' + SecaoNova + #13 +
        '           Deseja realmente realizar essa altera��o!?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        With DM.qrySQL do
        begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT * FROM LAYOUT_INPUT WHERE CLIENTE = :CLIENTE');
          ParamByName('CLIENTE').Value := DM.IBInventarioINV_LOJA.Value;
          Open;
        end;


        // if DM.qrySQL.FieldByName('ENDERECO').asString <> 'T' then
        // Begin

        If (NumCaracteres <> 4) or (Secao < '0001') or (BoolToStr(IntOK) = '-1') then
        Begin
          DM.Transacao.RollbackRetaining;
          DM.IBSecao.Cancel;
          MouseShowCursor(True);
        End
        else
        begin
          try

            SecaoNova := DM.IBSecaoSEC_SECAO.Value;

            { DM.Query1.Close;
              DM.Query1.SQL.Clear;
              Dm.Query1.SQL.Add('UPDATE SECAO SET SEC_SECAO = :SEC_SECAO ');
              DM.Query1.SQL.Add('WHERE SEC_ID = :SEC_ID AND TRA_ID = :TRA_ID');
              DM.Query1.Params.ParamByName('SEC_ID').Value := SecaoID;
              DM.Query1.Params.ParamByName('TRA_ID').Value := SecTransID;
              DM.Query1.Params.ParamByName('SEC_SECAO').Value := SecaoNova;
              DM.Query1.ExecSQL; }

            // ShowMessage(SecaoAntiga);
            // ShowMessage(SecaoNova);
            try

              { ShowMessage('vai come�ar a executar procedure');
                Dm.SpAlteraSec.Close;
                Dm.SpAlteraSec.ParamByName('NOVASEC').Value := UpperCase(SecaoNova);
                Dm.SpAlteraSec.ParamByName('VELHASEC').Value := SecaoAntiga;
                Dm.SpAlteraSec.ParamByName('TRANSID').Value := SecTransID;
                Dm.SpAlteraSec.ParamByName('ID_SECAO').Value := SecaoID;
                ShowMessage('passou os parametros');

                Dm.SpAlteraSec.ExecProc; }

              DM.SpAlteraSec.Close;
              DM.SpAlteraSec.ParamByName('NOVASEC').Value := UpperCase(SecaoNova);
              DM.SpAlteraSec.ParamByName('VELHASEC').Value := SecaoAntiga;
              DM.SpAlteraSec.ParamByName('TRANSID').Value := SecTransId;
              DM.SpAlteraSec.ParamByName('ID_SECAO').Value := SecaoId;

              DM.SpAlteraSec.ExecProc;

            except
              ShowMessage('Erro na execu��o de Procedure, por favor tente novamente');
            end;

            // GravaAltDbGridSecao(SecaoID, SecTransID, Matricula, SecaoAntiga, SecaoNova);

            Duplicidade(SecaoAntiga);

            Duplicidade(SecaoNova);

            PosicionaSecao(IDSECAO);

            DM.Transacao.CommitRetaining;

            MouseShowCursor(True);
            // end;
          except
            ShowMessage('Erro na altera��o, por favor tente novamente');
            DM.Transacao.RollbackRetaining;
            DM.IBSecao.Edit;
            DM.IBSecaoSEC_SECAO.Value := SecaoAntiga;
            MouseShowCursor(True);
          end;

        end;
        // end;
        { Else
          begin
          try
          SecaoNova := Dm.IBSecaoSEC_SECAO.value;

          Dm.IBSecao.Post;

          //ShowMessage(SecaoAntiga);
          //ShowMessage(SecaoNova);
          Dm.SpAlteraSec.Close;
          Dm.SpAlteraSec.ParamByName('NOVASEC').Value := SecaoNova;
          Dm.SpAlteraSec.ParamByName('VELHASEC').Value := SecaoAntiga;
          Dm.SpAlteraSec.ParamByName('TRANSID').Value := SecTransID;
          Dm.SpAlteraSec.ParamByName('ID_SECAO').Value := SecaoID;

          Dm.SpAlteraSec.ExecProc;

          GravaAltDbGridSecao(SecaoID, SecTransID, Matricula, SecaoAntiga, SecaoNova);

          Duplicidade(SecaoAntiga);

          Duplicidade(SecaoNova);

          PosicionaSecao(IDSECAO);

          Dm.Transacao.CommitRetaining;

          MouseShowCursor(True);
          // end;
          except
          ShowMessage('Erro na altera��o, por favor tente novamente');
          Dm.Transacao.RollbackRetaining;
          DM.IBSecao.Edit;
          DM.IBSecaoSEC_SECAO.Value := SecaoAntiga;
          MouseShowCursor(True);
          end;
          end; }

      end
      else
      begin
        DM.IBSecao.Cancel;
      end;
    end
    else
    begin
      DM.IBSecao.Cancel;
      Application.MessageBox
        ('      N�o � possivel alterar essa SE��O para este n�mero, j� existe uma                                        SE��O com essa TRANSMISS�O!'
        + #13 + '                            Por gentileza, altere para outro n�mero.', 'Mensagem do Sistema',
        MB_ICONERROR + MB_OK);
      DM.Transacao.RollbackRetaining;
      PosicionaSecao(IDSECAO);
    end;

  end
  else if (DM.IBSecao.State in [Dsedit, dsinsert]) and (Key = #13) then
  Begin
    DM.IBSecao.Cancel;
    MouseShowCursor(True);
  End;

  // CASO CLIENTE N�O TENHA ENDERE�O, A SE��O ACEITARA SOMENTE NUMEROS

  with DM.qrySQL do
  begin
    Close;
    SQL.Clear;
    SQL.Add('SELECT * FROM LAYOUT_INPUT');
    SQL.Add('WHERE CLIENTE = :CLIENTE');
    Params.ParamByName('CLIENTE').Value := DBEdit2.Text;
    Open;
  end;

  // if DM.qrySQL.FieldByName('ENDERECO').Value <> 'T' then
  // begin
  if not(Key in ['0' .. '9', Chr(8)]) then
    Key := #0;
  // end;

end;

procedure TFrmInventario.Edit4Change(Sender: TObject);
begin
  DM.IBRoster.locate('ROS_MATRICULA', Edit4.Text, [Lopartialkey, locaseinsensitive]);
end;

procedure TFrmInventario.TabSheet7Show(Sender: TObject);
begin
  // Dm.IBContagens.Close;
  // Dm.CdsContagens.Close;
  // Dm.IBContagens.Open;
  // Dm.CdsContagens.Open;

  // FrmInventario.DBGrid5.Refresh;
  // PosicionaSecao(IDSECAO);

  PosicionaContagem(IDCONTAGEM);

  AtualizaInv;

  // Dm.IBContagens.locate('CON_ID',IDCONTAGEM,[Lopartialkey,locaseinsensitive]);

  if Panel17.Enabled = True then
    DBGrid5.SetFocus;
end;

procedure TFrmInventario.RelatriodeSeesInesperadas1Click(Sender: TObject);
begin
  Try
    DM.QRelInesperadas.Close;
    DM.QRelInesperadas.Open;
    if DM.QRelInesperadas.RecordCount > 0 then
    begin
      DM.RvPrjWis.Close;
      DM.RvPrjWis.projectfile := 'C:\SGD\Rave\Inesperadas.rav';
      DM.RvPrjWis.Open;
      DM.RvSysWis.DefaultDest := rdPreview;
      DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
      DM.RvPrjWis.ExecuteReport('Inesperadas');
      // Salvando relat�rio em PDF automaticamente
      Try
        DM.RvSysWis.DefaultDest := rdFile;
        DM.RvSysWis.DoNativeOutput := False;
        DM.RvSysWis.RenderObject := RvPDF;
        DM.RvSysWis.OutputFileName := 'C:\SGD\Relatorios\Relatorio de Secoes Inesperadas_' +
          DM.IBInventarioINV_LOJA.AsString + '_' + DM.IBInventarioINV_NUMLOJA.AsString + '.pdf';
        // caminho onde vai gerar o arquivo pdf
        DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
        DM.RvPrjWis.projectfile := 'C:\SGD\Rave\Inesperadas.rav';
        // caminho onde esta o arquivo que ele vai exportar para pdf
        DM.RvPrjWis.Engine := DM.RvSysWis;
        DM.RvPrjWis.Execute;
      Except
        ShowMessage('Erro ao gerar o relat�rio em PDF');
      end;
      // Salvando relat�rio em TXT automaticamente

      Try
        DM.RvSysWis.DefaultDest := rdFile;
        DM.RvSysWis.DoNativeOutput := False;
        DM.RvSysWis.RenderObject := RvTxt;
        DM.RvSysWis.OutputFileName := 'C:\SGD\Relatorios\Relatorio de Secoes Inesperadas_' +
          DM.IBInventarioINV_LOJA.AsString + '_' + DM.IBInventarioINV_NUMLOJA.AsString + '.txt';
        // caminho onde vai gerar o arquivo pdf
        DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
        DM.RvPrjWis.projectfile := 'C:\SGD\Rave\Inesperadas.rav';
        // caminho onde esta o arquivo que ele vai exportar para pdf
        DM.RvPrjWis.Engine := DM.RvSysWis;
        DM.RvPrjWis.Execute;
      except
        ShowMessage('Erro ao gerar o relat�rio em TXT');
      end;
    end
    else
      Application.MessageBox('N�o existem se��es inesperadas', 'Mensagem do sistema', MB_ICONEXCLAMATION + MB_OK);

  Finally
    DM.QRelInesperadas.Close;
  End;
end;

procedure TFrmInventario.DBGrid3CellClick(Column: TColumn);
var
  Coluna, SomaXColunas: Integer;
begin
  // Pegar valor do ID da Linha
  IDSECAO := DM.IBSecaoSEC_ID.Value;

  DM.IBSecao.Open;

  Coluna := -1;
  SomaXColunas := 0;

  while ClicadoGridMapX > SomaXColunas do
  begin
    Coluna := Coluna + 1;
    SomaXColunas := SomaXColunas + DBGrid3.Columns[Coluna].Width;
  end;

  If Coluna = 2 then
  begin
    DM.IBSecao.Edit;
    if DM.IBSecaoSEC_OK.Value = 'X' then
    begin
      DM.IBSecaoSEC_OK.Value := '';
    end
    else
      DM.IBSecaoSEC_OK.Value := 'X';
    DM.IBSecao.Post;

  end;

  Application.ProcessMessages;
  DM.Transacao.CommitRetaining;

end;

procedure TFrmInventario.DBGrid5CellClick(Column: TColumn);
var
  Coluna, SomaXColunas: Integer;
begin
  If click = True then
    click := False
  else
    click := True;
  // Pega valor do ID da linha
  IDCONTAGEM := DM.IBContagensCON_ID.Value;
  // Dm.IBContagens.Refresh;
  DM.IBArea.Close;
  DM.IBArea.Open;

    if DBGrid5.SelectedField.FieldName = 'CON_CLIENTE' Then
//    If Coluna = 13 then    //Cliente
    begin
      DM.IBContagens.Edit;
      if DM.IBContagensCON_CLIENTE.Value = 'X' then
        DM.IBContagensCON_CLIENTE.Value := ''
      Else
        begin
          DM.IBContagensCON_BSI.Value := '';
          DM.IBContagensCON_CLIENTE.Value := 'X';
        end;
      DM.IBContagens.Post;
    end;

    if DBGrid5.SelectedField.FieldName = 'CON_BSI' Then
//    If Coluna = 12 then    //BSI
    begin
      DM.IBContagens.Edit;
      if DM.IBContagensCON_BSI.Value = 'X' then
        DM.IBContagensCON_BSI.Value := ''
      Else
        begin
          DM.IBContagensCON_CLIENTE.Value := '';
          DM.IBContagensCON_BSI.Value := 'X';
        end;
      DM.IBContagens.Post;
    end;

 // Application.ProcessMessages;
  DM.Transacao.CommitRetaining;


end;

procedure TFrmInventario.RelatriodeItensAlteradosdepoisdedescarregados1Click(Sender: TObject);
begin
  Try
    DM.QRelItensAlterados.Close;
    DM.QRelItensAlterados.Open;
    if DM.QRelItensAlterados.RecordCount > 0 then
    begin
      DM.RvPrjWis.Close;
      DM.RvPrjWis.projectfile := 'C:\SGD\Rave\ItensAlterados.rav';
      DM.RvPrjWis.Open;
      DM.RvSysWis.DefaultDest := rdPreview;
      DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
      DM.RvPrjWis.ExecuteReport('ItensAlterados');
      // Salvando relat�rio em PDF automaticamente
      Try
        DM.RvSysWis.DefaultDest := rdFile;
        DM.RvSysWis.DoNativeOutput := False;
        DM.RvSysWis.RenderObject := RvPDF;
        DM.RvSysWis.OutputFileName := 'C:\SGD\Relatorios\Relatorio de Quantidades Alteradas_' +
          DM.IBInventarioINV_LOJA.AsString + '_' + DM.IBInventarioINV_NUMLOJA.AsString + '.pdf';
        // caminho onde vai gerar o arquivo pdf
        DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
        DM.RvPrjWis.projectfile := 'C:\SGD\Rave\ItensAlterados.rav';
        // caminho onde esta o arquivo que ele vai exportar para pdf
        DM.RvPrjWis.Engine := DM.RvSysWis;
        DM.RvPrjWis.Execute;
      Except
        ShowMessage('Erro ao gerar o relat�rio em PDF');
      end;
      // Salvando relat�rio em TXT automaticamente

      Try
        DM.RvSysWis.DefaultDest := rdFile;
        DM.RvSysWis.DoNativeOutput := False;
        DM.RvSysWis.RenderObject := RvTxt;
        DM.RvSysWis.OutputFileName := 'C:\SGD\Relatorios\Relatorio de Quantidades Alteradas_' +
          DM.IBInventarioINV_LOJA.AsString + '_' + DM.IBInventarioINV_NUMLOJA.AsString + '.txt';
        // caminho onde vai gerar o arquivo pdf
        DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
        DM.RvPrjWis.projectfile := 'C:\SGD\Rave\ItensAlterados.rav';
        // caminho onde esta o arquivo que ele vai exportar para pdf
        DM.RvPrjWis.Engine := DM.RvSysWis;
        DM.RvPrjWis.Execute;
      except
        ShowMessage('Erro ao gerar o relat�rio em TXT');
      end;
    end
    else
      Application.MessageBox('N�o existem quantidades alteradas', 'Mensagem do sistema', MB_ICONEXCLAMATION + MB_OK);
  Finally
    DM.QRelItensAlterados.Close;
  End;

end;

procedure TFrmInventario.DBGrid5ColExit(Sender: TObject);
begin
  { if (Dm.DsContagens.State = Dsedit) then
    GravaAltDbGridContagem;
    Dm.QGeraRelFinal.Close;
    Dm.QGeraRelFinal.Open;
    Dm.IBArea.Close;
    Dm.IBArea.Open;
  }
end;

procedure TFrmInventario.TabSheet7Exit(Sender: TObject);
begin
  { if (Dm.DsContagens.State = Dsedit) then
    GravaAltDbGridContagem; }
end;

procedure TFrmInventario.DBGrid5Exit(Sender: TObject);
begin

end;
{ if (Dm.DsContagens.State = Dsedit) then
  GravaAltDbGridContagem;
}

procedure TFrmInventario.ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);
var
  I: SmallInt;
begin
  // Fazer funcionar o Scroll do Mouse
  if Msg.message = WM_MOUSEWHEEL then
  begin
    Msg.message := WM_KEYDOWN;
    Msg.lParam := 0;
    I := HiWord(Msg.wParam);
    if I > 0 then
      Msg.wParam := VK_UP
    else
      Msg.wParam := VK_DOWN;
    Handled := False;
  end;
end;

procedure TFrmInventario.BitBtn4Click(Sender: TObject);
begin
  If DM.IBHorarios.RecordCount > 0 then
  Begin
    Application.CreateForm(TFrmHorario, FrmHorario); // cria o form
    FrmHorario.ShowModal;
    FreeAndNil(FrmHorario); // libera o form da memoria

  End
  Else
    Application.MessageBox('N�o existe nenhum conferente para ser aberto o hor�rio', 'Mensagem do sistema',
      MB_ICONERROR + MB_OK);

end;

procedure TFrmInventario.FormShow(Sender: TObject);
var
  prod: real;
begin
  DM.IBInventario.Open;

  { If (Dm.IBInventarioINV_LOJA.Value = 'PDA') Or (Dm.IBInventarioINV_LOJA.Value = 'COV') or (Dm.IBInventarioINV_LOJA.Value = 'ASA')then
    begin
    Dm.QGeraRelFinal.close;
    Dm.QGeraRelFinal.SQL.Clear;
    Dm.QGeraRelFinal.Sql.Add('SELECT Round(SUM(QTD)) AS QTD, SUM(PRECO) AS PRECO ,SUM(PRECO*100) AS PRECO_PORCENT FROM CONTAGENS_CONSO');
    Dm.QGeraRelFinal.Open;
    end
    else
    begin
    Dm.QGeraRelFinal.close;
    Dm.QGeraRelFinal.SQL.Clear;
    Dm.QGeraRelFinal.Sql.Add('SELECT SUM(QTD) AS QTD, SUM(PRECO) AS PRECO ,SUM(PRECO*100) AS PRECO_PORCENT FROM CONTAGENS_CONSO_GERAL');
    Dm.QGeraRelFinal.Open;
    end; }

  // Busca o total de pessoas no invent�rio que j� fizeram ao menos uma transmissao

  DM.Query1.Close;
  DM.Query1.SQL.Clear;
  DM.Query1.SQL.Add('select ROS_NOME, ROS_MATRICULA from horario ');
  DM.Query1.SQL.Add('group by ROS_MATRICULA, ROS_NOME ');
  DM.Query1.Open;

  DM.Query1.First;
  DM.Query1.Last;
  EdtPessoas.Text := inttostr(DM.Query1.RecordCount);

  { Dm.IBHorarios.First;
    Dm.IBHorarios.Last;
    EdtPessoas.Text:=IntToStr(Dm.IBHorarios.RecordCount); }

  try
    // Busca o total de horas do form produtividade
    // EdtHoras.Text:=FormatFloat('##,###0.00',(TotalHoras));
    EdtHoras.Text := TotalHoras;
    // Busca a quantidade do form produtividade
    EdtQtde.Text := FormatFloat('###,##0.00', DM.QGeraRelFinalQTD.Value);
    // Busca a produtividade do form produtividade

    DM.qrHorProd.Close;
    DM.qrHorProd.Open;
    prod := TotalPecasInv / TotalHorasInv;

    EdtProdutividade.Text := FormatFloat('###,##0.00', prod)
  except
    EdtHoras.Text := '0';
    EdtQtde.Text := '0';
    EdtProdutividade.Text := '0';
    prod := 0;
  end;

  // CASO CLIENTE N�O TENHA ENDERE�O, O TAMANHO M�XIMO DA SE��O SER� 4
  DM.qrySQL.Close;
  DM.qrySQL.SQL.Clear;
  DM.qrySQL.SQL.Add('SELECT * FROM LAYOUT_INPUT');
  DM.qrySQL.SQL.Add('WHERE CLIENTE = :CLIENTE');
  DM.qrySQL.ParamByName('CLIENTE').Value := DBEdit2.Text;
  DM.qrySQL.Open;

  { if DM.qrySQL.FieldByName('ENDERECO').Value = 'T' then
    begin
    Edit1.MaxLength := 25;
    Edit2.MaxLength := 25;
    end
    else
    begin }
  Edit1.MaxLength := 4;
  Edit2.MaxLength := 4;
  // end;

  // VERIFICA SE CLIENTE TEM ENDERE�O E LOTE AO INV�S DE SE��O E AREA
  DM.qrySQL.Close;
  DM.qrySQL.SQL.Clear;
  DM.qrySQL.SQL.Add('SELECT * FROM LAYOUT_INPUT');
  DM.qrySQL.SQL.Add('WHERE CLIENTE = :CLIENTE');
  DM.qrySQL.ParamByName('CLIENTE').Value := DBEdit2.Text;
  DM.qrySQL.Open;

  { if DM.qrySQL.FieldByName('ENDERECO').Value = 'T' then
    begin
    FrmInventario.DBGrid3.Columns[1].Title.Caption := 'Endere�o';
    FrmInventario.DBGrid5.Columns[3].Title.Caption := 'Endere�o';
    FrmInventario.DBGrid5.Columns[4].Title.Caption := 'Lote';
    FrmInventario.TabSheet5.Caption := '&Endere�o';
    FrmInventario.DBGrid1.Columns[0].Title.Caption := 'Endere�o Inicial';
    FrmInventario.DBGrid1.Columns[1].Title.Caption := 'Endere�o Final';
    FrmInventario.DBGrid1.Columns[2].Title.Caption := 'Descri��o do Endere�o';
    FrmInventario.Label1.Caption := 'End. Inicial';
    FrmInventario.Label2.Caption := 'End. Final';
    FrmInventario.Label3.Caption := 'Descri��o do Endere�o';
    FrmInventario.DBChart1.Title.Text.Clear;
    FrmInventario.DBChart1.Title.Text.Add('Total de Endere�os Contados');
    RelatriodeEndereosnocadastrados1.Enabled := True;

    Dm.QGrafico.Close;
    DM.QGrafico.SQL.Clear;
    DM.QGrafico.SQL.Add('select SEC_ABERTA,CASE SEC_ABERTA ');
    DM.QGrafico.SQL.Add('WHEN "T" THEN "ENDERE�OS ABERTOS" ');
    DM.QGrafico.SQL.Add('WHEN "F" THEN "ENDERE�OS CONTADOS" ');
    DM.QGrafico.SQL.Add('END AS DESCRI, ');
    DM.QGrafico.SQL.Add('CASE SEC_ABERTA ');
    DM.QGrafico.SQL.Add('WHEN "T" THEN COUNT(*) ');
    Dm.QGrafico.SQL.Add('WHEN "F" THEN COUNT(*) ');
    Dm.QGrafico.SQL.Add('END AS QUANTIDADE ');
    Dm.QGrafico.SQL.Add('FROM SECAO ');
    Dm.QGrafico.SQL.Add('WHERE (SEC_STATUS = '+ QuotedStr('.') +') ');
    Dm.QGrafico.SQL.Add('GROUP BY SEC_ABERTA ');
    DM.QGrafico.Open;
    end; }

  // VERIFICA SE HAVERA QUEBRA DE ARQUIVO, SE "SIM" ELE HABILITA CHECK DO GRID 3
  with DM.Query1 do
  begin
    Close;
    SQL.Clear;
    SQL.Add('SELECT * FROM LAYOUT_OUTPUT ');
    SQL.Add('WHERE CLIENTE = :CLIENTE');
    Params.ParamByName('CLIENTE').Value := DM.IBInventarioINV_LOJA.Value;
    Open;
  end;

  // Evandro 17/10/2013
  { if DM.Query1.FieldByName('BREAK_FILE').Value = 'T' then
    begin
    DBGrid1.Columns[3].Visible := True;
    DBGrid1.Columns[4].Visible := True;
    end
    else
    begin
    DBGrid1.Columns[3].Visible := False;
    DBGrid1.Columns[4].Visible := False;
    //DBGrid3.Columns[6].Width := 173;
    //DBGrid3.Columns[5].Width := 209;
    end; }

  // Preenche StatusBar
  StatusBar1.Panels[0].Text := 'Usu�rio Conectado = ' + DM.QUsuarioUSU_NOME.Value;
  StatusBar1.Panels[1].Text := '[' + Datetostr(Date) + ']';

  // Abrir Invent�rios

  // Verificar se invent�rio esta finalizado e mudar menu
  if DM.IBInventarioINV_STATUS.Value = 'FINALIZADO' then
    FrmInventario.MainMenu1.Items[0].Items[0].Caption := 'Reabrir Invent�rio';

  // Abrir no 1� Page Control
  FrmInventario.PageControl1.ActivePageIndex := 0;

  // Altera o title do grid5 (contagens) para as colunas EAN e o C�digo Interno
  // quando o cliente for a Coopervision, pois eles trabalham apenas com LOTE E SKU

  If DM.IBInventarioINV_LOJA.Value = 'CPV' then
  Begin
    FrmInventario.DBGrid5.Columns[5].Title.Caption := 'SKU'; // no lugar do C�digo Interno
    FrmInventario.DBGrid5.Columns[6].Title.Caption := 'LOTE'; // no lugar do EAN
  end;

  If (DM.IBInventarioINV_LOJA.Value = 'PDA') or (DM.IBInventarioINV_LOJA.Value = 'ASA') then
  Begin
    LabelTotalPecas.Visible := True;
    LabelTotalFin.Visible := True;
    DBText3.Visible := False;
    DBText4.Visible := False;
    try
      DM.QGeraRelFinal.Close;
      DM.QGeraRelFinal.SQL.Clear;
      DM.QGeraRelFinal.SQL.Add
        ('SELECT coalesce(sum(round(D.QTD)),0) AS QTD, coalesce(SUM(D.PRECO),0) AS PRECO, coalesce(SUM(PRECO*100),0) AS PRECO_PORCENT, coalesce(SUM(round((D.QTD))*100), 0) AS QTD_PORCENT ');
      DM.QGeraRelFinal.SQL.Add('FROM CONTAGENS_CONSO D INNER JOIN CADASTRO C ON (C.CAD_EAN = D.CON_EAN) WHERE C.CAD_CODINT IS NOT NULL');
      DM.QGeraRelFinal.Open;
      LabelTotalPecas.Caption := FormatFloat('##,###0.000', DM.QGeraRelFinal.FieldByName('QTD').Value);
      LabelTotalFin.Caption := FormatFloat('R$##,###0.00', DM.QGeraRelFinal.FieldByName('PRECO').Value);
    except
    end;
  end;

  // Caso invent�rio esteja finalizado ativa o menu para gera��o do arquivo final
  if DM.IBInventarioINV_STATUS.Value = 'FINALIZADO' then
    MainMenu1.Items[1].Items[1].Enabled := True;

  // Habilita o menu de Ativar e dasativar importa��o das transmiss�es para o adminitrador do sistema
  If NivelUsu = 1 then
    MainMenu1.Items[0].Items[1].Enabled := True;

  If (NivelUsu = 1) AND (DM.IBInventarioINV_STATUS.Value = 'FINALIZADO') then
  begin
    MainMenu1.Items[1].Items[7].Enabled := True;
    MainMenu1.Items[1].Items[8].Enabled := True;
  end;

  // Caso o invent�rio estaja finalizado desabilita alguns Panels e alguns menus
  If DM.IBInventarioINV_STATUS.Value = 'FINALIZADO' then
  Begin
    MainMenu1.Items[1].Items[3].Enabled := True;
    MainMenu1.Items[1].Items[4].Enabled := True;
    MainMenu1.Items[1].Items[5].Enabled := True;
    MainMenu1.Items[1].Items[6].Enabled := True;

    If NivelUsu = 1 then
    begin
      MainMenu1.Items[1].Items[7].Enabled := True;
      MainMenu1.Items[1].Items[8].Enabled := True;
    end;
    Panel4.Enabled := False;
    Panel15.Enabled := False;
    Panel9.Enabled := False;
    Panel7.Enabled := False;
    Panel8.Enabled := False;
    Panel11.Enabled := False;
    Panel12.Enabled := False;
    Panel13.Enabled := False;
  End;

end;

procedure TFrmInventario.GerarDivergncia1Click(Sender: TObject);
begin
  Application.CreateForm(TFrmDivergencia, FrmDivergencia); // cria o form
  FrmDivergencia.ShowModal;
  FreeAndNil(FrmDivergencia); // libera o form da memoria
end;

procedure TFrmInventario.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
  DM.qrySQL.Close;
  DM.qrySQL.SQL.Clear;
  DM.qrySQL.SQL.Add('SELECT * FROM LAYOUT_INPUT WHERE CLIENTE = :CLIENTE');
  DM.qrySQL.ParamByName('CLIENTE').AsString := DM.IBInventarioINV_LOJA.Value;
  DM.qrySQL.Open;

  // if DM.qrySQL.FieldByName('ENDERECO').AsString <> 'T' then
  // begin
  if not(Key in ['0' .. '9', ',', #8]) then
  begin
    Key := #0;
    beep;
  end;
  // end;
end;

procedure TFrmInventario.RelatriodeProdutosNoCadastrados1Click(Sender: TObject);
begin
  DM.QProdNaoCadastrado.Close;
  DM.QProdNaoCadastrado.Open;
  if DM.QProdNaoCadastrado.RecordCount > 0 then
  begin
    DM.RvPrjWis.Close;
    DM.RvPrjWis.projectfile := 'C:\SGD\Rave\NaoCadastrados.rav';
    DM.RvPrjWis.Open;
    DM.RvSysWis.DefaultDest := rdPreview;
    DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
    DM.RvPrjWis.ExecuteReport('NaoCadastrados');
    // Salvando relat�rio em PDF automaticamente
    Try
      DM.RvSysWis.DefaultDest := rdFile;
      DM.RvSysWis.DoNativeOutput := False;
      DM.RvSysWis.RenderObject := RvPDF;
      DM.RvSysWis.OutputFileName := 'C:\SGD\Relatorios\Relatorio de Produtos Nao Cadastrados_' +
        DM.IBInventarioINV_LOJA.AsString + '_' + DM.IBInventarioINV_NUMLOJA.AsString + '.pdf';
      // caminho onde vai gerar o arquivo pdf
      DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
      DM.RvPrjWis.projectfile := 'C:\SGD\Rave\NaoCadastrados.rav';
      // caminho onde esta o arquivo que ele vai exportar para pdf
      DM.RvPrjWis.Engine := DM.RvSysWis;
      DM.RvPrjWis.Execute;
    Except
      ShowMessage('Erro ao gerar o relat�rio em PDF');
    end;
    // Salvando relat�rio em TXT automaticamente

    Try
      DM.RvSysWis.DefaultDest := rdFile;
      DM.RvSysWis.DoNativeOutput := False;
      DM.RvSysWis.RenderObject := RvTxt;
      DM.RvSysWis.OutputFileName := 'C:\SGD\Relatorios\Relatorio de Produtos Nao Cadastrados_' +
        DM.IBInventarioINV_LOJA.AsString + '_' + DM.IBInventarioINV_NUMLOJA.AsString + '.txt';
      // caminho onde vai gerar o arquivo pdf
      DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
      DM.RvPrjWis.projectfile := 'C:\SGD\Rave\NaoCadastrados.rav';
      // caminho onde esta o arquivo que ele vai exportar para pdf
      DM.RvPrjWis.Engine := DM.RvSysWis;
      DM.RvPrjWis.Execute;
    except
      ShowMessage('Erro ao gerar o relat�rio em TXT');
    end;
  end
  else
    Application.MessageBox('Todos os produtos est�o cadastrados', 'Mensagem do sistema', MB_ICONEXCLAMATION + MB_OK);

End;

procedure TFrmInventario.PageControl1Change(Sender: TObject);
var
  prod: real;
begin
  AtualizaInv;
end;

procedure TFrmInventario.Produtividade1Click(Sender: TObject);
var
  arq: TStringList;
begin
  try
    arq := TStringList.Create;
    Dm2.QProdutividade.Close;
    Dm2.QProdutividade.Open;
    Dm2.QProdutividade.First;
    arq.Add('Coletor;Hora Transmissao;Matricula;Conferente;Quantidade;Secao;Descricao');
    while not Dm2.QProdutividade.Eof do
    begin
      Dm2.QBuscaMapeamento.Close;
      Dm2.QBuscaMapeamento.ParamByName('secao').Value := Dm2.QProdutividade.FieldByName('secao').Value;
      Dm2.QBuscaMapeamento.Open;

      arq.Add(Dm2.QProdutividade.FieldByName('Coletor').Value + ';' + Dm2.QProdutividade.FieldByName('Horario').AsString
        + ';' + Dm2.QProdutividade.FieldByName('Matricula').Value + ';' + Dm2.QProdutividade.FieldByName('Conferente')
        .Value + ';' + FloatToStr(Dm2.QProdutividade.FieldByName('Qtd').AsFloat) + ';' +
        Dm2.QProdutividade.FieldByName('Secao').Value + ';' + Dm2.QBuscaMapeamento.FieldByName('map_desc').AsString);
      Dm2.QProdutividade.Next;
    end;
    arq.SaveToFile('c:\SGD\Produtividade.csv');
    arq.Free;
    ShowMessage('Relat�rio gerado com sucesso!');
  Except
    on E: Exception do
    begin
      ShowMessage('Erro ao gerar relat�rio -  Erro: ' + E.message);
      Dm2.QProdutividade.Close;
    end;
  end;
end;

procedure TFrmInventario.RelatriodeCdigosEANePLU1Click(Sender: TObject);
var
  Ean, CodInt: string;
begin
  Application.CreateForm(TFrmRelCodigo, FrmRelCodigo); // cria o form
  FrmRelCodigo.ShowModal;
  FreeAndNil(FrmRelCodigo); // libera o form da memoria

end;

procedure TFrmInventario.DBGrid6DrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  { Procedure que altera a cor da linha selecionada no DBGrid }
  { Altera a cor do fundo da linha selecionada no DBGrid }
  if gdSelected in State then
    with (Sender as TDBGrid).Canvas do
    begin
      Brush.Color := clMoneyGreen; { Cor de fundo }
      FillRect(Rect);
    end;

  TDBGrid(Sender).DefaultDrawDataCell(Rect, TDBGrid(Sender).Columns[DataCol].field, State);

end;

procedure TFrmInventario.DBGrid7DrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  { Procedure que altera a cor da linha selecionada no DBGrid }
  { Altera a cor do fundo da linha selecionada no DBGrid }
  if gdSelected in State then
    with (Sender as TDBGrid).Canvas do
    begin
      Brush.Color := clMoneyGreen; { Cor de fundo }
      FillRect(Rect);
    end;

  TDBGrid(Sender).DefaultDrawDataCell(Rect, TDBGrid(Sender).Columns[DataCol].field, State);

end;

procedure TFrmInventario.DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  check: Integer;
  r: TRect;
begin

  // Desenha um checkbox no dbgrid
  if (Column.FieldName = 'DEPOSITO') then
  begin
    DBGrid1.Canvas.FillRect(Rect);
    check := 0;
    if DM.IBMapeamentoDEPOSITO.AsString = 'X' then
      check := DFCS_CHECKED
    else
      check := 0;
    r := Rect;
    InflateRect(r, -2, -2); { Diminue o tamanho do CheckBox }
    DrawFrameControl(DBGrid1.Canvas.Handle, r, DFC_BUTTON, DFCS_BUTTONCHECK or check);
  end;

  if (Column.FieldName = 'LOJA') then
  begin
    DBGrid1.Canvas.FillRect(Rect);
    check := 0;
    if DM.IBMapeamentoLOJA.AsString = 'X' then
      check := DFCS_CHECKED
    else
      check := 0;
    r := Rect;
    InflateRect(r, -2, -2); { Diminue o tamanho do CheckBox }
    DrawFrameControl(DBGrid1.Canvas.Handle, r, DFC_BUTTON, DFCS_BUTTONCHECK or check);
  end;

  { Procedure que altera a cor da linha selecionada no DBGrid }
  { Altera a cor do fundo da linha selecionada no DBGrid }
  // if gdSelected in State then
  // with (Sender as TDBGrid).Canvas do
  // begin
  // Brush.Color:=clblue; {Cor de fundo}
  // FillRect(Rect);
  // end;

  // TDbGrid(Sender).DefaultDrawDataCell(Rect, TDbGrid(Sender).columns[datacol].field, State);

end;

procedure TFrmInventario.DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  { Procedure que altera a cor da linha selecionada no DBGrid }
  { Altera a cor do fundo da linha selecionada no DBGrid }
  if gdSelected in State then
    with (Sender as TDBGrid).Canvas do
    begin
      Brush.Color := clMOneyGreen; { Cor de fundo }
      FillRect(Rect);
    end;

  TDBGrid(Sender).DefaultDrawDataCell(Rect, TDBGrid(Sender).Columns[DataCol].field, State);

end;

procedure TFrmInventario.DBGrid3DrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  check: Integer;
  r: TRect;
begin

  // Desenha um checkbox no dbgrid
  if (Column.FieldName = 'SEC_OK') then
  begin
    DBGrid3.Canvas.FillRect(Rect);
    check := 0;
    if DM.IBSecaoSEC_OK.AsString = 'X' then
      check := DFCS_CHECKED
    else
      check := 0;
    r := Rect;
    InflateRect(r, -2, -2); { Diminue o tamanho do CheckBox }
    DrawFrameControl(DBGrid3.Canvas.Handle, r, DFC_BUTTON, DFCS_BUTTONCHECK or check);
  end;

  { Procedure que altera a cor da linha selecionada no DBGrid
    Altera a cor do fundo da linha selecionada no DBGrid
    if gdSelected in State then
    with (Sender as TDBGrid).Canvas do
    begin
    Brush.Color:=clBlue; {Cor de fundo
    FillRect(Rect);
    end;

    TDbGrid(Sender).DefaultDrawDataCell(Rect, TDbGrid(Sender).columns[datacol].field, State);
  }
end;

procedure TFrmInventario.DBGrid4DrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  { Procedure que altera a cor da linha selecionada no DBGrid }
  { Altera a cor do fundo da linha selecionada no DBGrid }
  if gdSelected in State then
    with (Sender as TDBGrid).Canvas do
    begin
      Brush.Color := clMoneyGreen; { Cor de fundo }
      FillRect(Rect);
    end;

  TDBGrid(Sender).DefaultDrawDataCell(Rect, TDBGrid(Sender).Columns[DataCol].field, State);

end;

procedure TFrmInventario.DBGrid5DrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  check: Integer;
  r: TRect;
begin
  { Procedure que altera a cor da linha selecionada no DBGrid }
  { Altera a cor do fundo da linha selecionada no DBGrid }
  if gdSelected in State then
    with (Sender as TDBGrid).Canvas do
    begin
      Brush.Color := clMoneyGreen; { Cor de fundo }
      FillRect(Rect);
    end;

  TDBGrid(Sender).DefaultDrawDataCell(Rect, TDBGrid(Sender).Columns[DataCol].field, State);

  if (Column.FieldName = 'CON_BSI') then
  begin
    DBGrid5.Canvas.FillRect(Rect);
    check := 0;
    if DM.IBContagensCON_BSI.AsString = 'X' then
      check := DFCS_CHECKED
    else
      check := 0;
    r := Rect;
    InflateRect(r, -2, -2); { Diminue o tamanho do CheckBox }
    DrawFrameControl(DBGrid5.Canvas.Handle, r, DFC_BUTTON, DFCS_BUTTONCHECK or check);
  end;

  if (Column.FieldName = 'CON_CLIENTE') then
  begin
    DBGrid5.Canvas.FillRect(Rect);
    check := 0;
    if DM.IBContagensCON_CLIENTE.AsString = 'X' then
      check := DFCS_CHECKED
    else
      check := 0;
    r := Rect;
    InflateRect(r, -2, -2); { Diminue o tamanho do CheckBox }
    DrawFrameControl(DBGrid5.Canvas.Handle, r, DFC_BUTTON, DFCS_BUTTONCHECK or check);
  end;

end;

procedure TFrmInventario.ransmitirBancodeDados1Click(Sender: TObject);
begin
  Application.CreateForm(TFrmUpdate, FrmUpdate); // cria o form
  FrmUpdate.ShowModal;
  FreeAndNil(FrmUpdate); // libera o form da memoria
end;

procedure TFrmInventario.Timer2Timer(Sender: TObject);
begin
  { If (Dm.IBInventarioINV_LOJA.Value = 'PDA') or (Dm.IBInventarioINV_LOJA.Value = 'COV') or (Dm.IBInventarioINV_LOJA.Value = 'ASA') then
    begin
    Dm.QGeraRelFinal.Close;
    Dm.QGeraRelFinal.Sql.Clear;
    Dm.QGeraRelFinal.Sql.Add('SELECT round(SUM(QTD)) AS QTD, SUM(PRECO) AS PRECO ,SUM(PRECO*100) AS PRECO_PORCENT FROM CONTAGENS_CONSO');
    Dm.QGeraRelFinal.Open;
    end
    Else
    begin
    Dm.QGeraRelFinal.Close;
    Dm.QGeraRelFinal.Sql.Clear;
    Dm.QGeraRelFinal.Sql.Add('select sum(preco) as preco, sum(QTD) as QTD ,SUM(PRECO*100) AS PRECO_PORCENT from CONTAGENS_CONSO_GERAL');
    Dm.QGeraRelFinal.Open;

    end; }

end;

procedure TFrmInventario.sBitBtn1Click(Sender: TObject);
begin
  AdicionarConferente;
end;

procedure TFrmInventario.sBitBtn2Click(Sender: TObject);
begin

  DM.Query1.Close;
  DM.Query1.SQL.Clear;
  DM.Query1.SQL.Add('SELECT * FROM HORARIO ');
  DM.Query1.SQL.Add('WHERE HOR_ENTRADA IS NULL ');
  DM.Query1.Open;
  DM.Query1.First;

  if DM.Query1.RecordCount > 0 then
  begin

    while NOT DM.Query1.Eof do
    begin
      ShowMessage('N�o foi dado a entrada de hor�rio do Conferente ' + #13 + DM.Query1.FieldByName('ROS_NOME').AsString
        + #13 + 'Por favor informe o hor�rio para calcularmos a Produtividade');
      DM.Query1.Next;
    end;
  end
  else
  begin
    Application.CreateForm(TFrmProdutividade, FrmProdutividade); // cria o form
    FrmProdutividade.ShowModal;
    FreeAndNil(FrmProdutividade); // libera o form da memoria
  end;
end;

procedure TFrmInventario.RelatriodeSetorConsolidado1Click(Sender: TObject);
begin
  Try
    DM.QRelCLBLoja.Close;
    DM.QRelCLBLoja.Open;
    DM.QRelCLBDeposito.Close;
    DM.QRelCLBDeposito.Open;
    DM.QRelCLBVitrine.Close;
    DM.QRelCLBVitrine.Open;
    DM.QRelCLBTotal.Close;
    DM.QRelCLBTotal.Open;
    if DM.QRelCLBTotalSum.Value > 0 then
    begin
      DM.RvPrjWis.projectfile := 'C:\SGD\Rave\Rel_CLBSetorizado.rav';
      DM.RvSysWis.DefaultDest := rdPreview;
      DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
      DM.RvPrjWis.ExecuteReport('Rel_CLBSetorizado');
      // Salvando relat�rio em PDF automaticamente
      Try
        DM.RvSysWis.DefaultDest := rdFile;
        DM.RvSysWis.DoNativeOutput := False;
        DM.RvSysWis.RenderObject := RvPDF;
        DM.RvSysWis.OutputFileName := 'C:\SGD\Relatorios\Relatorio CLB Setorizado_' + DM.IBInventarioINV_LOJA.AsString +
          '_' + DM.IBInventarioINV_NUMLOJA.AsString + '.pdf'; // caminho onde vai gerar o arquivo pdf
        DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
        DM.RvPrjWis.projectfile := 'C:\SGD\Rave\Rel_CLBSetorizado.rav';
        // caminho onde esta o arquivo que ele vai exportar para pdf
        DM.RvPrjWis.Engine := DM.RvSysWis;
        DM.RvPrjWis.Execute;
      Except
        ShowMessage('Erro ao gerar o relat�rio em PDF');
      end;
      // Salvando relat�rio em TXT automaticamente

      Try
        DM.RvSysWis.DefaultDest := rdFile;
        DM.RvSysWis.DoNativeOutput := False;
        DM.RvSysWis.RenderObject := RvTxt;
        DM.RvSysWis.OutputFileName := 'C:\SGD\Relatorios\Relatorio CLB Setorizado_' + DM.IBInventarioINV_LOJA.AsString +
          '_' + DM.IBInventarioINV_NUMLOJA.AsString + '.txt'; // caminho onde vai gerar o arquivo pdf
        DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
        DM.RvPrjWis.projectfile := 'C:\SGD\Rave\Rel_CLBSetorizado.rav';
        // caminho onde esta o arquivo que ele vai exportar para pdf
        DM.RvPrjWis.Engine := DM.RvSysWis;
        DM.RvPrjWis.Execute;
      except
        ShowMessage('Erro ao gerar o relat�rio em TXT');
      end;
    end
    else
      Application.MessageBox('N�o existem contagens para gerar o relat�rio', 'Mensagem do sistema',
        MB_ICONEXCLAMATION + MB_OK);
  Finally
    DM.QRelCLBTotal.Close;
  End;
end;

procedure TFrmInventario.RelatriodeAcuracia1Click(Sender: TObject);
begin
  Try
    begin
      DM2.IBControleSecCont.Close;


//      if DM.QAcu_Colaborador.RecordCount > 0 then
//      Begin
        DM.RvPrjWis.Close;
        DM.RvPrjWis.projectfile := 'C:\SGD\Rave\ControleAudit.rav';
        DM.RvPrjWis.Open;
        DM.RvSysWis.DefaultDest := rdPreview;
        DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
        DM.RvPrjWis.ExecuteReport('ControleAudit');
        // Salvando relat�rio em PDF automaticamente
        Try
          DM.RvSysWis.DefaultDest := rdFile;
          DM.RvSysWis.DoNativeOutput := False;
          DM.RvSysWis.RenderObject := RvPDF;
          DM.RvSysWis.OutputFileName := 'C:\SGD\Relatorios\ControleAudit_' +
            DM.IBInventarioINV_LOJA.AsString + '_' + DM.IBInventarioINV_NUMLOJA.AsString + '.pdf';
          // caminho onde vai gerar o arquivo pdf
          DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
          DM.RvPrjWis.projectfile := 'C:\SGD\Rave\ControleAudit.rav';
          // caminho onde esta o arquivo que ele vai exportar para pdf
          DM.RvPrjWis.Engine := DM.RvSysWis;
          DM.RvPrjWis.Execute;
        Except
          ShowMessage('Erro ao gerar o relat�rio em PDF');
        end;
      end

    //  else
    //    Application.MessageBox('N�o existem colaboradores cadastrados', 'Mensagem do sistema', MB_ICONERROR + MB_OK);
    //End;
  Finally
      DM2.IBControleSecCont.Close;
      DM2.IBControleSecAudit.Close;
      DM2.IBControlePecasCont.Close;
      DM2.IBControlePecasAudit.Close;
      DM2.IBControleSecCorrigida.Close;
  End;
end;

procedure TFrmInventario.RelatriodeAuditoriaConsolidado1Click(Sender: TObject);
var
  StrSql: string;
begin
End;

procedure TFrmInventario.DBGrid5TitleClick(Column: TColumn);
var
  campo: string;
  query: TStringList;
begin

  if DM.IBContagens.SelectSQL.Text <> '' then
  begin
    query := TStringList.Create();

    query.Text := DM.IBContagens.SelectSQL.Text;

    query.Delete(query.Count - 1);
  end;

  // Grid_Ordena_e_PintaTitulo(DBGrid5, Column, DM.CdsContagens);

  If click = True then
  begin
    click := False;
    campo := Column.FieldName; // CAMPO RECEBE O NOME DA COLUNA CLICADA,
    Application.ProcessMessages; // para considerar algo que aconte�a no dbgrid durante a entrada nesta procedure
    DM.IBContagens.SelectSQL.Clear; // LIMPA A QUERY
    query.Add('ORDER BY ' + campo);
    DM.IBContagens.SelectSQL.Text := query.Text;
    DM.IBContagens.Open; // ABRE A QUERY COM A ORDEM ESCOLHIDA.
    Column.Font.Color := clBlue; // COLOCAR A COLUNA NA COR DESEJADA
  end
  else
  begin
    click := True;
    campo := Column.FieldName; // CAMPO RECEBE O NOME DA COLUNA CLICADA,
    Application.ProcessMessages; // para considerar algo que aconte�a no dbgrid durante a entrada nesta procedure
    DM.IBContagens.SelectSQL.Clear; // LIMPA A QUERY
    query.Add('ORDER BY ' + campo);
    DM.IBContagens.SelectSQL.Text := query.Text;
    DM.IBContagens.Open; // ABRE A QUERY COM A ORDEM ESCOLHIDA.
    Column.Font.Color := clBlue; // COLOCAR A COLUNA NA COR DESEJADA
  end;


  // Grid_Ordena_e_PintaTitulo(DBGrid5, Column, DM.CdsContagens);

  { If click = true then
    begin
    Click := false;
    Campo := Column.FieldName; // CAMPO RECEBE O NOME DA COLUNA CLICADA,
    Application.ProcessMessages; // para considerar algo que aconte�a no dbgrid durante a entrada nesta procedure
    Dm.IBContagens.SelectSql.Clear; // LIMPA A QUERY
    Dm.IBContagens.SelectSql.Add('select c.CON_EAN, c.CON_ID, c.ARE_AREA, c.CON_DATA, c.CON_HORA, c.CON_QUANTIDADE, c.CON_QUANTIDADEORIGINAL, c.HOR_MATRICULA, c.SEC_SECAO, c.TRA_ID, c.CON_STATUS, ');
    Dm.IBContagens.SelectSQL.Add('c.RUP_DEPOSITO, c.RUP_LOJA, c.CON_DEPOSITO, c.DEPOSITO, c.LOJA, c.CON_QUANTDESC, c.NUM_CONT, c.TONALIDADE, c.CON_CADCODINT, c.CON_ENDERECO, ca.CAD_DESC ');
    Dm.IBContagens.SelectSql.Add('FROM CONTAGENS C INNER JOIN CADASTRO CA ON (CA.CAD_EAN = C.CON_EAN) ');
    Dm.IBContagens.SelectSql.Add('ORDER BY '+campo); // ESCREVE O SELECT COM O ORDER BY
    Dm.IBContagens.Open; // ABRE A QUERY COM A ORDEM ESCOLHIDA.
    Column.Font.Color:= clBlue; // COLOCAR A COLUNA NA COR DESEJADA
    end
    else
    begin
    click := true;
    Campo := Column.FieldName; // CAMPO RECEBE O NOME DA COLUNA CLICADA,
    Application.ProcessMessages; // para considerar algo que aconte�a no dbgrid durante a entrada nesta procedure
    Dm.IBContagens.SelectSql.Clear; // LIMPA A QUERY
    Dm.IBContagens.SelectSql.Add('select c.CON_EAN, c.CON_ID, c.ARE_AREA, c.CON_DATA, c.CON_HORA, c.CON_QUANTIDADE, c.CON_QUANTIDADEORIGINAL, c.HOR_MATRICULA, c.SEC_SECAO, c.TRA_ID, c.CON_STATUS, ');
    Dm.IBContagens.SelectSQL.Add('c.RUP_DEPOSITO, c.RUP_LOJA, c.CON_DEPOSITO, c.DEPOSITO, c.LOJA, c.CON_QUANTDESC, c.NUM_CONT, c.TONALIDADE, c.CON_CADCODINT, c.CON_ENDERECO, ca.CAD_DESC ');
    Dm.IBContagens.SelectSql.Add('FROM CONTAGENS C INNER JOIN CADASTRO CA ON (CA.CAD_EAN = C.CON_EAN) ');
    Dm.IBContagens.SelectSql.Add('ORDER BY '+campo+ ' DESC'); // ESCREVE O SELECT COM O ORDER BY
    Dm.IBContagens.Open; // ABRE A QUERY COM A ORDEM ESCOLHIDA.
    Column.Font.Color:= clBlue; // COLOCAR A COLUNA NA COR DESEJADA
    end; }

end;

procedure TFrmInventario.DBGrid2TitleClick(Column: TColumn);
var
  campo: string;
begin
  Try
    If click = True then
    begin
      click := False;
      campo := Column.FieldName; // CAMPO RECEBE O NOME DA COLUNA CLICADA,
      Application.ProcessMessages; // para considerar algo que aconte�a no dbgrid durante a entrada nesta procedure
      DM.IBTransmissao.SelectSQL.Clear; // LIMPA A QUERY
      DM.IBTransmissao.SelectSQL.Add('select * from transmissao ');
      DM.IBTransmissao.SelectSQL.Add('ORDER BY ' + campo); // ESCREVE O SELECT COM O ORDER BY
      DM.IBTransmissao.Open; // ABRE A QUERY COM A ORDEM ESCOLHIDA.
      Column.Font.Color := clBlue; // COLOCAR A COLUNA NA COR DESEJADA
    end
    else
    begin
      click := True;
      campo := Column.FieldName; // CAMPO RECEBE O NOME DA COLUNA CLICADA,
      Application.ProcessMessages; // para considerar algo que aconte�a no dbgrid durante a entrada nesta procedure
      DM.IBTransmissao.SelectSQL.Clear; // LIMPA A QUERY
      DM.IBTransmissao.SelectSQL.Add('select * from transmissao ');
      DM.IBTransmissao.SelectSQL.Add('ORDER BY ' + campo + ' DESC'); // ESCREVE O SELECT COM O ORDER BY
      DM.IBTransmissao.Open; // ABRE A QUERY COM A ORDEM ESCOLHIDA.
      Column.Font.Color := clBlue; // COLOCAR A COLUNA NA COR DESEJADA
    end;
  except
  end;
end;

procedure TFrmInventario.DBGrid3TitleClick(Column: TColumn);
var
  indice: string;
  existe: Boolean;
  clientdataset_idx: tclientdataset;
begin
  Try
    // Grid_Ordena_e_PintaTitulo(DBGrid3, Column, DM.IBSecao);
    clientdataset_idx := tclientdataset(Column.Grid.DataSource.DataSet);

    // Column.FieldName

    if clientdataset_idx.IndexFieldNames = Column.FieldName then
    begin
      indice := AnsiUpperCase(Column.FieldName);
      try
        clientdataset_idx.IndexDefs.Find(indice);
        existe := True;
      except
        existe := False;
      end;

      if not existe then
        with clientdataset_idx.IndexDefs.AddIndexDef do
        begin
          Name := indice;
          Fields := Column.FieldName;
          Options := [ixDescending];
        end;
      clientdataset_idx.IndexName := indice;
      Column.Font.Color := clBlue; // COLOCAR A COLUNA NA COR DESEJADA
    end
    else
      clientdataset_idx.IndexFieldNames := Column.FieldName;
    Column.Font.Color := clBlue; // COLOCAR A COLUNA NA COR DESEJADA

  Except
  End;
End;

procedure TFrmInventario.CheckBox1Click(Sender: TObject);
begin
  If not CheckBox1.Checked then
  begin
    DM.IBContagens.Close;
    DM.IBContagens.SelectSQL.Clear;
    DM.IBContagens.SelectSQL.Add
      ('select c.CON_EAN, c.CON_ID, c.ARE_AREA, c.CON_DATA, c.CON_HORA, c.CON_QUANTIDADE, c.CON_QUANTIDADEORIGINAL, c.HOR_MATRICULA, c.SEC_SECAO, c.TRA_ID, c.CON_STATUS, ');
    DM.IBContagens.SelectSQL.Add
      ('c.RUP_DEPOSITO, c.RUP_LOJA, c.CON_DEPOSITO, c.DEPOSITO, c.LOJA, c.CON_QUANTDESC, c.NUM_CONT, c.TONALIDADE, c.CON_CADCODINT, c.CON_ENDERECO, ca.CAD_DESC, c.con_bsi, c.con_cliente ');
    DM.IBContagens.SelectSQL.Add('FROM CONTAGENS C LEFT JOIN CADASTRO CA ON (CA.CAD_EAN = C.CON_EAN) ');
    DM.IBContagens.SelectSQL.Add('ORDER BY SEC_SECAO, CON_ID ');
    DM.IBContagens.Close;
    // Dm.CdsContagens.Close;
    DM.IBContagens.Open;
    // Dm.CdsContagens.Open;
  end;
  If CheckBox1.Checked then
  begin
    DM.IBContagens.Close;
    DM.IBContagens.SelectSQL.Clear;
    DM.IBContagens.SelectSQL.Add
      ('select c.CON_EAN, c.CON_ID, c.ARE_AREA, c.CON_DATA, c.CON_HORA, c.CON_QUANTIDADE, c.CON_QUANTIDADEORIGINAL, c.HOR_MATRICULA, c.SEC_SECAO, c.TRA_ID, c.CON_STATUS, ');
    DM.IBContagens.SelectSQL.Add
      ('c.RUP_DEPOSITO, c.RUP_LOJA, c.CON_DEPOSITO, c.DEPOSITO, c.LOJA, c.CON_QUANTDESC, c.NUM_CONT, c.TONALIDADE, c.CON_CADCODINT, c.CON_ENDERECO, ca.CAD_DESC, c.con_bsi, c.con_cliente ');
    DM.IBContagens.SelectSQL.Add
      ('FROM CONTAGENS C LEFT JOIN CADASTRO CA ON (CA.CAD_EAN = C.CON_EAN) where (SEC_SECAO = :SEC_SECAO) and (TRA_ID = :TRA_ID)');
    DM.IBContagens.SelectSQL.Add('ORDER BY SEC_SECAO, CON_ID');
    DM.IBContagens.Close;
    // Dm.CdsContagens.Close;
    DM.IBContagens.Open;
    // Dm.CdsContagens.Open;
  end;
end;

procedure TFrmInventario.Geratxtconfernciaseo1Click(Sender: TObject);
begin
  GeraTxtConfSecao;
end;

procedure TFrmInventario.RelatriodeEndereosnocadastrados1Click(Sender: TObject);
begin
  DM.QEndNaoCadastrados.Close;
  DM.QEndNaoCadastrados.Open;
  if DM.QEndNaoCadastrados.RecordCount > 0 then
  begin
    DM.RvPrjWis.Close;
    DM.RvPrjWis.projectfile := 'C:\SGD\Rave\RelEndNaoCadastrado.rav';
    DM.RvPrjWis.Open;
    DM.RvSysWis.DefaultDest := rdPreview;
    DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
    DM.RvPrjWis.ExecuteReport('RelEndNaoCadastrado')
  end;

end;

procedure TFrmInventario.RelatriodeSeesDeletadas1Click(Sender: TObject);
begin
  Try
    DM.QRelDeletadas.Close;
    DM.QRelDeletadas.Open;
    if DM.QRelDeletadas.RecordCount > 0 then
    begin
      DM.RvPrjWis.Close;
      DM.RvPrjWis.projectfile := 'C:\SGD\Rave\Deletadas.rav';
      DM.RvPrjWis.Open;
      DM.RvSysWis.DefaultDest := rdPreview;
      DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
      DM.RvPrjWis.ExecuteReport('Deletadas');
      // Salvando relat�rio em PDF automaticamente
      Try
        DM.RvSysWis.DefaultDest := rdFile;
        DM.RvSysWis.DoNativeOutput := False;
        DM.RvSysWis.RenderObject := RvPDF;
        DM.RvSysWis.OutputFileName := 'C:\SGD\Relatorios\Relatorio de Deletadas_' + DM.IBInventarioINV_LOJA.AsString +
          '_' + DM.IBInventarioINV_NUMLOJA.AsString + '.pdf'; // caminho onde vai gerar o arquivo pdf
        DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
        DM.RvPrjWis.projectfile := 'C:\SGD\Rave\Deletadas.rav';
        // caminho onde esta o arquivo que ele vai exportar para pdf
        DM.RvPrjWis.Engine := DM.RvSysWis;
        DM.RvPrjWis.Execute;
      Except
        ShowMessage('Erro ao gerar o relat�rio em PDF');
      end;
      // Salvando relat�rio em TXT automaticamente

      Try
        DM.RvSysWis.DefaultDest := rdFile;
        DM.RvSysWis.DoNativeOutput := False;
        DM.RvSysWis.RenderObject := RvTxt;
        DM.RvSysWis.OutputFileName := 'C:\SGD\Relatorios\Relatorio de Deletadas_' + DM.IBInventarioINV_LOJA.AsString +
          '_' + DM.IBInventarioINV_NUMLOJA.AsString + '.txt'; // caminho onde vai gerar o arquivo pdf
        DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
        DM.RvPrjWis.projectfile := 'C:\SGD\Rave\Deletadas.rav';
        // caminho onde esta o arquivo que ele vai exportar para pdf
        DM.RvPrjWis.Engine := DM.RvSysWis;
        DM.RvPrjWis.Execute;
      except
        ShowMessage('Erro ao gerar o relat�rio em TXT');
      end;
    end
    else
      Application.MessageBox('N�o existem se��es Deletadas', 'Mensagem do sistema', MB_ICONEXCLAMATION + MB_OK);
  Finally
    DM.QRelDeletadas.Close;
  End;

end;

procedure TFrmInventario.DBGrid3MouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  ClicadoGridMapX := X;
end;

procedure TFrmInventario.DBGrid1CellClick(Column: TColumn);
var
  Coluna, SomaXColunas: Integer;
begin
  // Pegar valor do ID da Linha
  IDSECAO := DM.IBSecaoSEC_ID.Value;

  DM.IBMapeamento.Open;

  Coluna := -1;
  SomaXColunas := 0;

  while ClicadoGridMapX > SomaXColunas do
  begin
    Coluna := Coluna + 1;
    SomaXColunas := SomaXColunas + DBGrid1.Columns[Coluna].Width;
  end;

  If Coluna = 3 then
  begin
    DM.IBMapeamento.Open;
    DM.IBMapeamento.Edit;
    DM.IBSecao.Open;
    DM.IBSecao.Edit;

    if DM.IBMapeamentoDEPOSITO.Value = 'X' then
    begin
      DM.IBMapeamentoDEPOSITO.Value := '';

      DM.Query1.Close;
      DM.Query1.SQL.Clear;
      DM.Query1.SQL.Add('SELECT * FROM MAPEAMENTO ');
      DM.Query1.SQL.Add('where (Map_Secini between :secini and :secfin) ');
      DM.Query1.SQL.Add('or (Map_Secfin between :secini and :secfin) ');
      DM.Query1.SQL.Add('order by MAP_SECINI ');
      DM.Query1.Params.ParamByName('SECINI').Value := DM.IBMapeamentoMAP_SECINI.Value;
      DM.Query1.Params.ParamByName('SECFIN').Value := DM.IBMapeamentoMAP_SECFIN.Value;
      DM.Query1.Open;

      If (DM.IBSecao.locate('SEC_SECAO', DM.Query1.FieldByName('MAP_SECINI').Value, [Lopartialkey, locaseinsensitive])
        = True) then
      Begin // 2

        DM.IBSecao.Edit;
        DM.qrySQL.Close;
        DM.qrySQL.SQL.Clear;
        DM.qrySQL.SQL.Add('Update SECAO set DEPOSITO = :DEPOSITO ');
        DM.qrySQL.SQL.Add(' where SEC_SECAO BETWEEN :MAP_SECINI AND :MAP_SECFIN ');
        DM.qrySQL.Params.ParamByName('MAP_SECINI').Value := DM.IBMapeamentoMAP_SECINI.Value;
        DM.qrySQL.Params.ParamByName('MAP_SECFIN').Value := DM.IBMapeamentoMAP_SECFIN.Value;
        DM.qrySQL.Params.ParamByName('DEPOSITO').Value := null;
        DM.qrySQL.ExecSQL;

      End;
      DM.IBMapeamento.Post;
      DM.IBSecao.Post;
    end
    else
    begin

      DM.IBMapeamentoDEPOSITO.Value := 'X';
      DM.IBMapeamentoLOJA.Value := ' ';

      DM.Query1.Close;
      DM.Query1.SQL.Clear;
      DM.Query1.SQL.Add('SELECT * FROM MAPEAMENTO ');
      DM.Query1.SQL.Add('where (Map_Secini between :secini and :secfin) ');
      DM.Query1.SQL.Add('or (Map_Secfin between :secini and :secfin) ');
      DM.Query1.SQL.Add('order by MAP_SECINI ');
      DM.Query1.Params.ParamByName('SECINI').Value := DM.IBMapeamentoMAP_SECINI.Value;
      DM.Query1.Params.ParamByName('SECFIN').Value := DM.IBMapeamentoMAP_SECFIN.Value;
      DM.Query1.Open;

      If (DM.IBSecao.locate('SEC_SECAO', DM.Query1.FieldByName('MAP_SECINI').Value, [Lopartialkey, locaseinsensitive])
        = True) then
      Begin // 2

        DM.IBSecao.Edit;
        DM.qrySQL.Close;
        DM.qrySQL.SQL.Clear;
        DM.qrySQL.SQL.Add('Update SECAO set LOJA = :LOJA, DEPOSITO = :DEPOSITO ');
        DM.qrySQL.SQL.Add(' where SEC_SECAO BETWEEN :MAP_SECINI AND :MAP_SECFIN ');
        DM.qrySQL.Params.ParamByName('MAP_SECINI').Value := DM.IBMapeamentoMAP_SECINI.Value;
        DM.qrySQL.Params.ParamByName('MAP_SECFIN').Value := DM.IBMapeamentoMAP_SECFIN.Value;
        DM.qrySQL.Params.ParamByName('DEPOSITO').Value := 'X';
        DM.qrySQL.Params.ParamByName('LOJA').Value := '';
        DM.qrySQL.ExecSQL;

      End;
      DM.IBMapeamento.Post;
      DM.IBSecao.Post;
    end;

  end;

  If Coluna = 4 then
  begin
    DM.IBMapeamento.Open;
    DM.IBMapeamento.Edit;
    DM.IBSecao.Open;
    DM.IBSecao.Edit;
    if DM.IBMapeamentoLOJA.Value = 'X' then
    begin
      DM.IBMapeamentoLOJA.Value := '';

      DM.Query1.Close;
      DM.Query1.SQL.Clear;
      DM.Query1.SQL.Add('SELECT * FROM MAPEAMENTO ');
      DM.Query1.SQL.Add('where (Map_Secini between :secini and :secfin) ');
      DM.Query1.SQL.Add('or (Map_Secfin between :secini and :secfin) ');
      DM.Query1.SQL.Add('order by MAP_SECINI ');
      DM.Query1.Params.ParamByName('SECINI').Value := DM.IBMapeamentoMAP_SECINI.Value;
      DM.Query1.Params.ParamByName('SECFIN').Value := DM.IBMapeamentoMAP_SECFIN.Value;
      DM.Query1.Open;

      If (DM.IBSecao.locate('SEC_SECAO', DM.Query1.FieldByName('MAP_SECINI').Value, [Lopartialkey, locaseinsensitive])
        = True) then
      Begin // 2

        DM.IBSecao.Edit;
        DM.qrySQL.Close;
        DM.qrySQL.SQL.Clear;
        DM.qrySQL.SQL.Add('Update SECAO set LOJA = :LOJA ');
        DM.qrySQL.SQL.Add(' where SEC_SECAO BETWEEN :MAP_SECINI AND :MAP_SECFIN ');
        DM.qrySQL.Params.ParamByName('MAP_SECINI').Value := DM.IBMapeamentoMAP_SECINI.Value;
        DM.qrySQL.Params.ParamByName('MAP_SECFIN').Value := DM.IBMapeamentoMAP_SECFIN.Value;
        DM.qrySQL.Params.ParamByName('LOJA').Value := null;
        DM.qrySQL.ExecSQL;

      End;
      DM.IBMapeamento.Post;
      DM.IBSecao.Post;
    end
    else
    begin

      DM.IBMapeamentoLOJA.Value := 'X';
      DM.IBMapeamentoDEPOSITO.Value := ' ';

      DM.Query1.Close;
      DM.Query1.SQL.Clear;
      DM.Query1.SQL.Add('SELECT * FROM MAPEAMENTO ');
      DM.Query1.SQL.Add('where (Map_Secini between :secini and :secfin) ');
      DM.Query1.SQL.Add('or (Map_Secfin between :secini and :secfin) ');
      DM.Query1.SQL.Add('order by MAP_SECINI ');
      DM.Query1.Params.ParamByName('SECINI').Value := DM.IBMapeamentoMAP_SECINI.Value;
      DM.Query1.Params.ParamByName('SECFIN').Value := DM.IBMapeamentoMAP_SECFIN.Value;
      DM.Query1.Open;

      If (DM.IBSecao.locate('SEC_SECAO', DM.Query1.FieldByName('MAP_SECINI').Value, [Lopartialkey, locaseinsensitive])
        = True) then
      Begin // 2

        DM.IBSecao.Edit;
        DM.qrySQL.Close;
        DM.qrySQL.SQL.Clear;
        DM.qrySQL.SQL.Add('Update SECAO set LOJA = :LOJA, DEPOSITO = :DEPOSITO ');
        DM.qrySQL.SQL.Add(' where SEC_SECAO BETWEEN :MAP_SECINI AND :MAP_SECFIN ');
        DM.qrySQL.Params.ParamByName('MAP_SECINI').Value := DM.IBMapeamentoMAP_SECINI.Value;
        DM.qrySQL.Params.ParamByName('MAP_SECFIN').Value := DM.IBMapeamentoMAP_SECFIN.Value;
        DM.qrySQL.Params.ParamByName('LOJA').Value := 'X';
        DM.qrySQL.Params.ParamByName('DEPOSITO').Value := '';
        DM.qrySQL.ExecSQL;

      End;
      DM.IBMapeamento.Post;
      DM.IBSecao.Post;
    end;
  end;
  DM.Transacao.CommitRetaining;
end;

procedure TFrmInventario.DBGrid1MouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  ClicadoGridMapX := X;
end;

procedure TFrmInventario.Edit5Change(Sender: TObject);
begin
  DM.IBRoster.locate('ROS_NOME', Edit5.Text, [Lopartialkey, locaseinsensitive]);
end;

procedure TFrmInventario.RelatriodeRuptura1Click(Sender: TObject);
begin
  Application.CreateForm(TFrmRuptura, FrmRuptura); // cria o form
  FrmRuptura.ShowModal;
  FreeAndNil(FrmRuptura); // libera o form da memoria

end;

procedure TFrmInventario.GerarDivergnciaQuantidade1Click(Sender: TObject);
begin
  Application.CreateForm(TFrmDivergenciaQTD, FrmDivergenciaQTD); // cria o form
  FrmDivergenciaQTD.ShowModal;
  FreeAndNil(FrmDivergenciaQTD); // libera o form da memoria

end;

procedure TFrmInventario.RelatriodeAuditoriaForada1Click(Sender: TObject);
var
  StrSql: string;
begin
  DM.IBLayoutInput.Open;
  DM.Query1.Close;
  DM.Query1.SQL.Clear;
  DM.Query1.SQL.Add('SELECT * FROM LAYOUT_INPUT ');
  DM.Query1.SQL.Add('WHERE CLIENTE = :CLIENTE');
  DM.Query1.Params.ParamByName('CLIENTE').Value := DM.IBInventarioINV_LOJA.AsString;
  DM.Query1.Open;

  if (DM.Query1.FieldByName('REL_AUDIT_VALOR').Value = 'T') then
  begin
    Dm2.QRelatorioAuditForcada.Close;
    Dm2.QRelatorioAuditForcada.SQL.Clear;
    StrSql := 'select h.ros_nome, h.ros_matricula, s.sec_secao, c.are_area ';
    StrSql := StrSql +
      'from  secao s join contagens_conso c on (c.sec_secao = s.sec_secao) and (c.preco >= :preco) left join horario h on (h.ros_matricula = s.hor_matricula) ';
    StrSql := StrSql + 'where (sec_status <> "DEL") and (sec_aberta = "F") ';
    StrSql := StrSql + ' and (c.are_area = c.are_area) ';
    StrSql := StrSql + 'group by s.sec_secao, h.ros_nome, h.ros_matricula, c.are_area ';
    StrSql := StrSql + 'order by s.sec_secao';
    Dm2.QRelatorioAuditForcada.SQL.Add(StrSql);
    Dm2.QRelatorioAuditForcada.Params.ParamByName('preco').Value := DM.Query1.FieldByName('REL_AUDIT_VALOR2').AsString;
    Dm2.QRelatorioAuditForcada.Open;
    Dm2.QRelatorioAuditForcada.Close;
    Dm2.QRelatorioAuditForcada.Open;

    if Dm2.QRelatorioAuditForcada.RecordCount > 0 then
    Begin
      Try
        Try
          Dm2.QRelatorioAuditForcada2.Close;
          Dm2.QRelatorioAuditForcada2.SQL.Clear;
          Dm2.QRelatorioAuditForcada2.SQL.Add
            ('select co.con_ean,co.qtd, co.sec_secao, ca.cad_desc, ca.cad_codint,  co.preco,  co.are_area, ca.cad_un ');
          Dm2.QRelatorioAuditForcada2.SQL.Add('from contagens_conso co ');
          Dm2.QRelatorioAuditForcada2.SQL.Add
            ('left join cadastro_conso ca on (ca.cad_ean = co.con_ean) where  (co.preco >= :preco)  ');
          Dm2.QRelatorioAuditForcada2.SQL.Add('order by co.preco desc ');
          Dm2.QRelatorioAuditForcada2.Params.ParamByName('preco').Value :=
            DM.Query1.FieldByName('REL_AUDIT_VALOR2').AsString;
          Dm2.QRelatorioAuditForcada2.Open;
          DM.RvPrjWis.Close;
          DM.RvPrjWis.projectfile := 'C:\SGD\Rave\AuditoriaForcada.rav';
          DM.RvPrjWis.Open;
          DM.RvSysWis.DefaultDest := rdPreview;
          DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
          DM.RvPrjWis.ExecuteReport('AuditoriaForcada');

          // Salvando relat�rio em PDF automaticamente
          Try
            DM.RvPrjWis.Close;
            DM.RvSysWis.DefaultDest := rdFile;
            DM.RvSysWis.DoNativeOutput := False;
            DM.RvSysWis.RenderObject := RvPDF;
            DM.RvSysWis.OutputFileName := 'C:\SGD\Relatorios\Relatorio Auditoria For�ada por Valor_' +
              DM.IBInventarioINV_LOJA.AsString + '_' + DM.IBInventarioINV_NUMLOJA.AsString + '.pdf';
            // caminho onde vai gerar o arquivo pdf
            DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
            DM.RvPrjWis.projectfile := 'C:\SGD\Rave\AuditoriaForcada.rav';
            // caminho onde esta o arquivo que ele vai exportar para pdf
            DM.RvPrjWis.Engine := DM.RvSysWis;
            DM.RvPrjWis.Open;
            DM.RvPrjWis.Execute;
          Except
            ShowMessage('Erro ao salvar o relat�rio em PDF');
          end;

          // Salvando relat�rio em TXT automaticamente
          Try
            DM.RvPrjWis.Close;
            DM.RvSysWis.DefaultDest := rdFile;
            DM.RvSysWis.DoNativeOutput := False;
            DM.RvSysWis.RenderObject := RvTxt;
            DM.RvSysWis.OutputFileName := 'C:\SGD\Relatorios\Relatorio Auditoria For�ada por Valor_' +
              DM.IBInventarioINV_LOJA.AsString + '_' + DM.IBInventarioINV_NUMLOJA.AsString + '.txt';
            // caminho onde vai gerar o arquivo pdf
            DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
            DM.RvPrjWis.projectfile := 'C:\SGD\Rave\AuditoriaForcada.rav';
            // caminho onde esta o arquivo que ele vai exportar para pdf
            DM.RvPrjWis.Engine := DM.RvSysWis;
            DM.RvPrjWis.Open;
            DM.RvPrjWis.Execute;
          except
            ShowMessage('Erro ao salvar o relat�rio em TXT');
          end;

        except
          Application.MessageBox('Erro ao gerar Auditoria For�ada', 'Mensagem do Sistema', MB_ICONERROR + MB_OK);
        end;

      finally
        DM.IBLayoutInput.Edit;
        DM.qrySQL.Close;
        DM.qrySQL.SQL.Clear;
        DM.qrySQL.SQL.Add('UPDATE LAYOUT_INPUT SET REL_IMPRESSO = :OK ');
        DM.qrySQL.SQL.Add('WHERE CLIENTE = :CLIENTE ');
        DM.qrySQL.Params.ParamByName('OK').Value := 'X';
        DM.qrySQL.Params.ParamByName('CLIENTE').Value := DM.IBInventarioINV_LOJA.Value;
        DM.qrySQL.ExecSQL;
        DM.Transacao.CommitRetaining;
        ShowMessage('Auditoria For�ada por valor R$ gerada com sucesso!');
      end;

    end
    else
    begin
      DM.IBLayoutInput.Edit;
      DM.qrySQL.Close;
      DM.qrySQL.SQL.Clear;
      DM.qrySQL.SQL.Add('UPDATE LAYOUT_INPUT SET REL_IMPRESSO = :OK ');
      DM.qrySQL.SQL.Add('WHERE CLIENTE = :CLIENTE ');
      DM.qrySQL.Params.ParamByName('OK').Value := 'X';
      DM.qrySQL.Params.ParamByName('CLIENTE').Value := DM.IBInventarioINV_LOJA.Value;
      DM.qrySQL.ExecSQL;
      DM.Transacao.CommitRetaining;
      Application.MessageBox('N�o existem VALORES para o relat�rio ser gerado!', 'Mensagem do Sistema',
        MB_ICONERROR + MB_OK);
    end;
  end;

  if (DM.Query1.FieldByName('REL_AUDIT_QUANT').Value = 'T') then
  begin
    Dm2.QRelatorioAuditForcada.Close;
    Dm2.QRelatorioAuditForcada.SQL.Clear;
    StrSql := 'select h.ros_nome, h.ros_matricula, s.sec_secao, c.are_area ';
    StrSql := StrSql +
      'from  secao s join contagens_conso c on (c.sec_secao = s.sec_secao) and (c.qtd >= :qtd) left join horario h on (h.ros_matricula = s.hor_matricula) ';
    StrSql := StrSql + 'where (sec_status <> "DEL") and (sec_aberta = "F") ';
    StrSql := StrSql + ' and (c.are_area = c.are_area) ';
    StrSql := StrSql + 'group by s.sec_secao, h.ros_nome, h.ros_matricula, c.are_area ';
    StrSql := StrSql + 'order by s.sec_secao';
    Dm2.QRelatorioAuditForcada.SQL.Add(StrSql);
    Dm2.QRelatorioAuditForcada.Params.ParamByName('qtd').Value := DM.Query1.FieldByName('REL_AUDIT_QUANT2').AsString;
    Dm2.QRelatorioAuditForcada.Open;
    Dm2.QRelatorioAuditForcada.Close;
    Dm2.QRelatorioAuditForcada.Open;

    if Dm2.QRelatorioAuditForcada.RecordCount > 0 then
    Begin
      Try
        Try
          Dm2.QRelatorioAuditForcada2.Close;
          Dm2.QRelatorioAuditForcada2.SQL.Clear;
          Dm2.QRelatorioAuditForcada2.SQL.Add
            ('select co.con_ean,co.qtd, co.sec_secao, ca.cad_desc, ca.cad_codint,  co.preco,  co.are_area, ca.cad_un ');
          Dm2.QRelatorioAuditForcada2.SQL.Add('from contagens_conso co ');
          Dm2.QRelatorioAuditForcada2.SQL.Add
            ('left join cadastro_conso ca on (ca.cad_ean = co.con_ean) where  (co.qtd >= :qtd)  ');
          Dm2.QRelatorioAuditForcada2.SQL.Add('order by co.qtd desc ');
          Dm2.QRelatorioAuditForcada2.Params.ParamByName('qtd').Value :=
            DM.Query1.FieldByName('REL_AUDIT_QUANT2').AsString;
          Dm2.QRelatorioAuditForcada2.Open;
          DM.RvPrjWis.Close;
          DM.RvPrjWis.projectfile := 'C:\SGD\Rave\AuditoriaForcada2.rav';
          DM.RvPrjWis.Open;
          DM.RvSysWis.DefaultDest := rdPreview;
          DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
          DM.RvPrjWis.ExecuteReport('AuditoriaForcada');

          // Salvando relat�rio em PDF automaticamente
          Try
            DM.RvPrjWis.Close;
            DM.RvSysWis.DefaultDest := rdFile;
            DM.RvSysWis.DoNativeOutput := False;
            DM.RvSysWis.RenderObject := RvPDF;
            DM.RvSysWis.OutputFileName := 'C:\SGD\Relatorios\Relatorio Auditoria For�ada por Quantidade_' +
              DM.IBInventarioINV_LOJA.AsString + '_' + DM.IBInventarioINV_NUMLOJA.AsString + '.pdf';
            // caminho onde vai gerar o arquivo pdf
            DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
            DM.RvPrjWis.projectfile := 'C:\SGD\Rave\AuditoriaForcada2.rav';
            // caminho onde esta o arquivo que ele vai exportar para pdf
            DM.RvPrjWis.Engine := DM.RvSysWis;
            DM.RvPrjWis.Open;
            DM.RvPrjWis.Execute;
          Except
            ShowMessage('Erro ao gravar o relat�rio em PDF');
          end;

          // Salvando relat�rio em TXT automaticamente
          Try
            DM.RvPrjWis.Close;
            DM.RvSysWis.DefaultDest := rdFile;
            DM.RvSysWis.DoNativeOutput := False;
            DM.RvSysWis.RenderObject := RvTxt;
            DM.RvSysWis.OutputFileName := 'C:\SGD\Relatorios\Relatorio Auditoria For�ada por Quantidade_' +
              DM.IBInventarioINV_LOJA.AsString + '_' + DM.IBInventarioINV_NUMLOJA.AsString + '.txt';
            // caminho onde vai gerar o arquivo pdf
            DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
            DM.RvPrjWis.projectfile := 'C:\SGD\Rave\AuditoriaForcada2.rav';
            // caminho onde esta o arquivo que ele vai exportar para pdf
            DM.RvPrjWis.Engine := DM.RvSysWis;
            DM.RvPrjWis.Open;
            DM.RvPrjWis.Execute;
          except
            ShowMessage('Erro ao salvar o relat�rio em TXT');
          end;

        except
          Application.MessageBox('Erro ao gerar Auditoria For�ada', 'Mensagem do Sistema', MB_ICONERROR + MB_OK);
        end;

      finally
        DM.IBLayoutInput.Edit;
        DM.qrySQL.Close;
        DM.qrySQL.SQL.Clear;
        DM.qrySQL.SQL.Add('UPDATE LAYOUT_INPUT SET REL_IMPRESSO = :OK ');
        DM.qrySQL.SQL.Add('WHERE CLIENTE = :CLIENTE ');
        DM.qrySQL.Params.ParamByName('OK').Value := 'X';
        DM.qrySQL.Params.ParamByName('CLIENTE').Value := DM.IBInventarioINV_LOJA.Value;
        DM.qrySQL.ExecSQL;
        DM.Transacao.CommitRetaining;
        ShowMessage('Auditoria For�ada por quantidade gerada com sucesso!');
      end;

    end
    else
    begin
      DM.IBLayoutInput.Edit;
      DM.qrySQL.Close;
      DM.qrySQL.SQL.Clear;
      DM.qrySQL.SQL.Add('UPDATE LAYOUT_INPUT SET REL_IMPRESSO = :OK ');
      DM.qrySQL.SQL.Add('WHERE CLIENTE = :CLIENTE ');
      DM.qrySQL.Params.ParamByName('OK').Value := 'X';
      DM.qrySQL.Params.ParamByName('CLIENTE').Value := DM.IBInventarioINV_LOJA.Value;
      DM.qrySQL.ExecSQL;
      DM.Transacao.CommitRetaining;
      Application.MessageBox('N�o existem VALORES para o relat�rio ser gerado!', 'Mensagem do Sistema',
        MB_ICONERROR + MB_OK);
    end;
  end
  else
  begin
    Application.MessageBox
      ('N�o existe valor pr�-determinado para gerar a Auditoria For�ada, Entre em contato com o Administrador do Sistema',
      'Mensagem do Sistema', MB_ICONERROR + MB_OK);
  end;

end;

procedure TFrmInventario.DBGrid7CellClick(Column: TColumn);
var
  id_conf: Integer;
begin
  id_conf := DM.IBHorariosHOR_ID.Value;
  DM.IBHorarios.locate('HOR_ID', id_conf, [Lopartialkey, locaseinsensitive])
end;

procedure TFrmInventario.DBGrid7KeyPress(Sender: TObject; var Key: Char);
var
  IDHORARIO: Integer;
begin
  DM.IBHorarios.Open;
  DM.IBHorarios.Edit;
  IDHORARIO := DM.IBHorariosHOR_ID.Value;
  if DBGrid7.SelectedIndex = 0 then
  begin
    if not(Key in ['0' .. '9', #8]) and not(Key = #13) then
    begin
      // ShowMessage('Digite apenas n�meros sendo que a matricula deve conter 11 digitos');
      Key := #0;
      // DBGrid7.Fields[0].Value := DBGrid7.Fields[0].OldValue;
    end
    else
    begin
      If (Key = #13) then
      begin
        If (DBGrid7.Fields[0].OldValue <> DBGrid7.Fields[0].NewValue) then
        begin
          if (Length(DBGrid7.Fields[0].NewValue) > 0) and (Length(DBGrid7.Fields[0].NewValue) < 11) then
          begin
            ShowMessage('A MATRICULA deve conter 11 n�meros!' + #13 + 'Digite novamente');
            DBGrid7.Fields[0].Value := DBGrid7.Fields[0].OldValue;
          end
          else
          begin
            if MessageDlg('Deseja realmente realizar a altera��o!?', mtConfirmation, [mbOK, mbCancel], 0) = MrOK then
            begin
              DM.IBHorarios.Open;
              DM.IBHorarios.Edit;
              DM.IBHorarios.Post;
              DM.Transacao.CommitRetaining;
              DM.IBHorarios.locate('HOR_ID', IDHORARIO, [Lopartialkey, locaseinsensitive]);
            end
            else
            begin
              DM.Transacao.RollbackRetaining;
              DM.IBHorarios.Close;
              DM.IBHorarios.Open;
              DM.IBHorarios.locate('HOR_ID', IDHORARIO, [Lopartialkey, locaseinsensitive]);
            end;
          end;
        end;
      end;
    end;
  end;
  if DBGrid7.SelectedIndex = 1 then
  begin
    if not(Key in ['A' .. 'Z', #8, #32]) and not(Key in ['a' .. 'z', #8]) and not(Key = #13) then
    begin
      Key := #0 // DBGrid7.Fields[1].Value := DBGrid7.Fields[1].OldValue;
    end
    else
    begin
      If (Key = #13) then
      begin
        if (DBGrid7.Fields[1].OldValue <> DBGrid7.Fields[1].NewValue) then
        begin
          if MessageDlg('Deseja realmente realizar a altera��o!?', mtConfirmation, [mbOK, mbCancel], 0) = MrOK then
          begin
            DM.IBHorarios.Open;
            DM.IBHorarios.Edit;
            DM.IBHorarios.Post;
            DM.Transacao.CommitRetaining;
            DM.IBHorarios.locate('HOR_ID', IDHORARIO, [Lopartialkey, locaseinsensitive]);
          end
          else
          begin
            DM.Transacao.RollbackRetaining;
            DM.IBHorarios.Close;
            DM.IBHorarios.Open;
            DM.IBHorarios.locate('HOR_ID', IDHORARIO, [Lopartialkey, locaseinsensitive]);
          end;
        end;
      end;
    end;
  end;
  if DBGrid7.SelectedIndex = 2 then
  begin
    DBGrid7.Fields[2].ReadOnly := True;
  end;
  if DBGrid7.SelectedIndex = 3 then
  begin
    DBGrid7.Fields[3].ReadOnly := True;
  end;
  if DBGrid7.SelectedIndex = 4 then
  begin
    DBGrid7.Fields[4].ReadOnly := True;
  end;
  if DBGrid7.SelectedIndex = 5 then
  begin
    DBGrid7.Fields[5].ReadOnly := True;
  end;

end;

procedure TFrmInventario.Edit4KeyPress(Sender: TObject; var Key: Char);
begin
  If not(Key in ['0' .. '9', #08, #13]) then
    Key := #0;
end;

procedure TFrmInventario.Edit5KeyPress(Sender: TObject; var Key: Char);
begin
  if not(Key in ['A' .. 'Z', #8, #13, #32]) and not(Key in ['a' .. 'z', #8, #13, #32]) then
    Key := #0;
end;

procedure TFrmInventario.FormCreate(Sender: TObject);
var
  ProgressBarStyle: Integer;

begin

  StatusBar1.Panels[2].Style := psOwnerDraw;

  ProgressBarTrans.Parent := StatusBar1;

  ProgressBarStyle := GetWindowLong(ProgressBarTrans.Handle,

    GWL_EXSTYLE);

  ProgressBarStyle := ProgressBarStyle - WS_EX_STATICEDGE;

  SetWindowLong(ProgressBarTrans.Handle, GWL_EXSTYLE,

    ProgressBarStyle);

end;

procedure TFrmInventario.StatusBar1DrawPanel(StatusBar: TStatusBar; Panel: TStatusPanel; const Rect: TRect);
begin

  if Panel = StatusBar1.Panels[2] then

    with ProgressBarTrans do

    begin

      Top := Rect.Top;

      Left := Rect.Left;

      Width := Rect.Right - Rect.Left - 15;

      Height := Rect.Bottom - Rect.Top;

    end;

end;

procedure TFrmInventario.GerarArquivodeTransmisses1Click(Sender: TObject);
Var
  Arquivo1: TStringList;
  DDia, DMes, DAno: Word;
  DData: TDateTime;
  Cont: Integer;
begin
  DM.QGeraRelTrans.Open;
  DM.QGeraRelTrans.First;

  Arquivo1 := TStringList.Create; // Instancia a variavel Arquivo

  DM.QGeraRelTrans.Last;
  Cont := DM.QGeraRelTrans.RecordCount;
  DM.QGeraRelTrans.First;

  Arquivo1.Add('MATRICULA' + ';' + 'NOME' + ';' + 'HORA DE ENTRADA' + ';' + 'PRIMEIRA TRANSMISS�O' + ';' +
    'HORARIO DE SAIDA' + ';' + '�LTIMA TRANSMISS�O');
  Arquivo1.Add(' ' + ';' + ' ' + ';' + ' ' + ';' + ' ' + ';' + ' ' + ';' + ' ');

  While Not DM.QGeraRelTrans.Eof Do
  Begin
    Arquivo1.Add(DM.QGeraRelTransHOR_MATRICULA.Value + ';' + DM.QGeraRelTransROS_NOME.AsString + ';' +
      DM.QGeraRelTransHOR_ENTRADA.Value + ';' + Copy(DM.QGeraRelTransTRANS_FIRST.AsString, 1, 2) + '/' +
      Copy(DM.QGeraRelTransTRANS_FIRST.AsString, 3, 2) + '/' + Copy(DM.QGeraRelTransTRANS_FIRST.AsString, 5, 4) + ' ' +
      Copy(DM.QGeraRelTransTRANS_FIRST.AsString, 9, 2) + ':' + Copy(DM.QGeraRelTransTRANS_FIRST.AsString, 11, 2) + ':' +
      Copy(DM.QGeraRelTransTRANS_FIRST.AsString, 13, 2) + ';' + DM.QGeraRelTransHOR_SAIDA.AsString + ';' +
      Copy(DM.QGeraRelTransTRANS_LAST.AsString, 1, 2) + '/' + Copy(DM.QGeraRelTransTRANS_LAST.AsString, 3, 2) + '/' +
      Copy(DM.QGeraRelTransTRANS_LAST.AsString, 5, 4) + ' ' + Copy(DM.QGeraRelTransTRANS_LAST.AsString, 9, 2) + ':' +
      Copy(DM.QGeraRelTransTRANS_LAST.AsString, 11, 2) + ':' + Copy(DM.QGeraRelTransTRANS_LAST.AsString, 13, 2));
    DM.QGeraRelTrans.Next;
  End;

  DM.QGeraRelTrans.Close;

  Arquivo1.SaveToFile('C:\SGD\Relatorios\Arquivo de Transmiss�es_' + DM.IBInventarioINV_LOJA.Value + '.CSV');

  FreeAndNil(Arquivo1);

  ShowMessage('Arquivo Gerado com Sucesso na pasta "C:\SGD\Relatorios\"');
  Screen.Cursor := CrDefault;
end;

procedure TFrmInventario.DBGrid1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin

  // N�O DEIXA INSERIR LINHAS EM BRANCO NO GRIG;
  if (Key in [VK_DOWN]) and (DBGrid1.DataSource.DataSet.RecNo = DBGrid1.DataSource.DataSet.RecordCount) then
    Abort;

  if (Key in [VK_DELETE]) then
    Abort;
  // ###Imprime relat�rio de acur�cia###
  { if (key = vk_F12) then
    Begin


    RelAcuracy(DM.IBMapeamentoMAP_SECINI.AsString, DM.IBMapeamentoMAP_SECFIN.AsString);
    End; }

end;

procedure TFrmInventario.mniRelatriodeQuantidades11Click(Sender: TObject);
begin
  Try
    BEGIN
      DM.IBLayoutInput.Open;
      DM.Query1.Close;
      DM.Query1.SQL.Clear;
      DM.Query1.SQL.Add('SELECT * FROM LAYOUT_INPUT ');
      DM.Query1.SQL.Add('WHERE CLIENTE = :CLIENTE');
      DM.Query1.Params.ParamByName('CLIENTE').Value := DM.IBInventarioINV_LOJA.AsString;
      DM.Query1.Open;
    END;
    // if (Dm.Query1.FieldByName('REL_AUDITORIA_PECA').Value = 'T') then
    BEGIN
      DM.QRelQuantidades.Close;
      DM.QRelQuantidades.Open;
      if DM.QRelQuantidades.RecordCount > 0 then
      begin
        DM.RvPrjWis.Close;
        DM.RvPrjWis.projectfile := 'C:\SGD\Rave\Quantidades1.rav';
        DM.RvPrjWis.Open;
        DM.RvSysWis.DefaultDest := rdPreview;
        DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
        DM.RvPrjWis.ExecuteReport('PecasBloco');
        // Salvando relat�rio em PDF automaticamente
        Try
          DM.RvSysWis.DefaultDest := rdFile;
          DM.RvSysWis.DoNativeOutput := False;
          DM.RvSysWis.RenderObject := RvPDF;
          DM.RvSysWis.OutputFileName := 'C:\SGD\Relatorios\Relatorio de Peca a Peca_' + DM.IBInventarioINV_LOJA.AsString
            + '_' + DM.IBInventarioINV_NUMLOJA.AsString + '.pdf'; // caminho onde vai gerar o arquivo pdf
          DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
          DM.RvPrjWis.projectfile := 'C:\SGD\Rave\Quantidades1.rav';
          // caminho onde esta o arquivo que ele vai exportar para pdf
          DM.RvPrjWis.Engine := DM.RvSysWis;
          DM.RvPrjWis.Execute;
        Except
          ShowMessage('Erro ao gerar o relat�rio em PDF');
        end;
        // Salvando relat�rio em TXT automaticamente

        Try
          DM.RvSysWis.DefaultDest := rdFile;
          DM.RvSysWis.DoNativeOutput := False;
          DM.RvSysWis.RenderObject := RvTxt;
          DM.RvSysWis.OutputFileName := 'C:\SGD\Relatorios\Relatorio de Peca a Peca_' + DM.IBInventarioINV_LOJA.AsString
            + '_' + DM.IBInventarioINV_NUMLOJA.AsString + '.txt'; // caminho onde vai gerar o arquivo pdf
          DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
          DM.RvPrjWis.projectfile := 'C:\SGD\Rave\Quantidades1.rav';
          // caminho onde esta o arquivo que ele vai exportar para pdf
          DM.RvPrjWis.Engine := DM.RvSysWis;
          DM.RvPrjWis.Execute;
        except
          ShowMessage('Erro ao gerar o relat�rio em TXT');
        end;
      end
      else
        Application.MessageBox('N�o existem quantidades diferente de 1', 'Mensagem do sistema',
          MB_ICONEXCLAMATION + MB_OK);
    END
    { else
      begin
      Application.MessageBox('Cliente n�o cadastrado, para emitir o relat�rio contate o administrador do sistema','Mensagem do sistema', MB_ICONEXCLAMATION+ MB_OK);
      end; }
  Finally
    DM.QRelQuantidades.Close;
    DM.IBLayoutInput.Edit;
    DM.qrySQL.Close;
    DM.qrySQL.SQL.Clear;
    DM.qrySQL.SQL.Add('UPDATE LAYOUT_INPUT SET REL_IMPRESSO = :OK ');
    DM.qrySQL.SQL.Add('WHERE CLIENTE = :CLIENTE ');
    DM.qrySQL.Params.ParamByName('OK').Value := 'X';
    DM.qrySQL.Params.ParamByName('CLIENTE').Value := DM.IBInventarioINV_LOJA.Value;
    DM.qrySQL.ExecSQL;
    DM.Transacao.CommitRetaining;
  end;

end;

procedure TFrmInventario.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if MessageDlg('Deseja realmente fechar o Sistema!?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    try
      IF DM.IBInventario.State = Dsedit then
      begin
        DM.IBInventario.Post;
        DM.Transacao.CommitRetaining;
      end;
      Action := caFree;
      Halt(1);
    except
    end;
  end
  else
  begin
    Action := caNone;
  end;

end;

procedure TFrmInventario.btnHoraGlobalClick(Sender: TObject);
begin
  If DM.IBHorarios.RecordCount > 0 then
  Begin
    Application.CreateForm(TFrmHorarioGlobal, FrmHorarioGlobal); // cria o form
    FrmHorarioGlobal.ShowModal;
    FreeAndNil(FrmHorarioGlobal); // libera o form da memoria

  End
  Else
    Application.MessageBox('N�o existe nenhum conferente registrado para lan�ar hor�rio global', 'Mensagem do sistema',
      MB_ICONERROR + MB_OK);
end;

procedure TFrmInventario.Edit1Change(Sender: TObject);
begin
  with DM.qrMapeamentoInsert do
  begin
    Close;
    ParamByName('secini').Value := Edit1.Text;
    ParamByName('secfin').Value := Edit1.Text;
    Open;

    if DM.qrMapeamentoInsert.RecordCount > 0 then
    begin
      Application.MessageBox('J� existe uma SE��O cadastrada com esse intervalo, favor inserir um novo RANGE!',
        'Mensagem do Sistema', MB_ICONERROR + MB_OK);
      Edit1.Clear;
    end;

  end;

end;

procedure TFrmInventario.Edit2Change(Sender: TObject);
begin
  with DM.qrMapeamentoInsert do
  begin
    Close;
    ParamByName('secini').Value := Edit2.Text;
    ParamByName('secfin').Value := Edit2.Text;
    Open;

    if DM.qrMapeamentoInsert.RecordCount > 0 then
    begin
      Application.MessageBox('J� existe uma SE��O cadastrada com esse intervalo, favor inserir um novo RANGE',
        'Mensagem do Sistema', MB_ICONERROR + MB_OK);
      Edit2.Clear;
    end;

  end;
end;

procedure TFrmInventario.RelatriodeAuditoriaFaixadeQuantidade1Click(Sender: TObject);
begin
  Begin
    Application.CreateForm(TFrmProdQuant, FrmProdQuant); // cria o form
    FrmProdQuant.ShowModal;
    FreeAndNil(FrmProdQuant); // libera o form da memoria

  End
end;

procedure TFrmInventario.Rel_Acu_ColaboradorClick(Sender: TObject);
begin
  Try
    begin
      DM.QAcu_Colaborador.Close;
      DM.QAcu_Colaborador.Open;
      if DM.QAcu_Colaborador.RecordCount > 0 then
      Begin
        DM.RvPrjWis.Close;
        DM.RvPrjWis.projectfile := 'C:\SGD\Rave\Acu_Colaborador.rav';
        DM.RvPrjWis.Open;
        DM.RvSysWis.DefaultDest := rdPreview;
        DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
        DM.RvPrjWis.ExecuteReport('Acu_Colaborador');
        // Salvando relat�rio em PDF automaticamente
        Try
          DM.RvSysWis.DefaultDest := rdFile;
          DM.RvSysWis.DoNativeOutput := False;
          DM.RvSysWis.RenderObject := RvPDF;
          DM.RvSysWis.OutputFileName := 'C:\SGD\Relatorios\Acuracidade_por_colaborador_' +
            DM.IBInventarioINV_LOJA.AsString + '_' + DM.IBInventarioINV_NUMLOJA.AsString + '.pdf';
          // caminho onde vai gerar o arquivo pdf
          DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
          DM.RvPrjWis.projectfile := 'C:\SGD\Rave\Acu_Colaborador.rav';
          // caminho onde esta o arquivo que ele vai exportar para pdf
          DM.RvPrjWis.Engine := DM.RvSysWis;
          DM.RvPrjWis.Execute;
        Except
          ShowMessage('Erro ao gerar o relat�rio em PDF');
        end;
        // Salvando relat�rio em TXT automaticamente

        Try
          DM.RvSysWis.DefaultDest := rdFile;
          DM.RvSysWis.DoNativeOutput := False;
          DM.RvSysWis.RenderObject := RvTxt;
          DM.RvSysWis.OutputFileName := 'C:\SGD\Relatorios\Acuracidade_por_colaborador_' +
            DM.IBInventarioINV_LOJA.AsString + '_' + DM.IBInventarioINV_NUMLOJA.AsString + '.txt';
          // caminho onde vai gerar o arquivo pdf
          DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
          DM.RvPrjWis.projectfile := 'C:\SGD\Rave\Acu_Colaborador.rav';
          // caminho onde esta o arquivo que ele vai exportar para pdf
          DM.RvPrjWis.Engine := DM.RvSysWis;
          DM.RvPrjWis.Execute;
        except
          ShowMessage('Erro ao gerar o relat�rio em TXT');
        end;
      end

      else
        Application.MessageBox('N�o existem colaboradores cadastrados', 'Mensagem do sistema', MB_ICONERROR + MB_OK);
    End;
  Finally
    DM.QAcu_Colaborador.Close;
  End;

end;

procedure TFrmInventario.DBGrid6KeyPress(Sender: TObject; var Key: Char);
var
  Conferente, ConferenteAntigo, ConferenteNovo, matricula, MatriculaNova, MatriculaAntiga: String;
  Ros_ID, NumCaracteres: Integer;
begin
  { begin
    if ((DM.IBRoster.State = DsEdit) and (DBGrid6.SelectedField.FieldName = 'ROS_MATRICULA')) then
    begin
    if not((Key in ['0'..'9',Chr(8)]) or (Key = #13)) then
    Key := #0;
    end;
    MouseShowCursor(false);

    if (DBGrid6.SelectedField.FieldName = 'ROS_MATRICULA') THEN
    Key := AnsiUpperCase(Key)[Length(Key)];

    If (Key = #27) then
    begin
    MouseShowCursor(True);
    end;

    //Alterar Matricula com tecla Enter
    If (Key = #13) and (Dm.DsRoster.State = DsEdit)  then
    Begin

    Dm.IBRoster.Open;

    DM.IBRoster.Edit;

    Ros_ID := Dm.IBRosterROS_ID.value;

    MatriculaAntiga := Dm.IBRosterROS_MATRICULA.OldValue;

    Conferente := Dm.IBRosterROS_NOME.OldValue;

    MatriculaNova := DM.IBRosterROS_MATRICULA.value;

    NumCaracteres := Length(MatriculaNova);

    Matricula :=(MatriculaNova);

    MouseShowCursor(True);

    with dm.qrySQL do
    begin
    Close;
    SQL.Clear;
    SQL.Add('SELECT ROS_MATRICULA, ROS_ID FROM ROSTER ');
    SQL.Add('WHERE ROS_MATRICULA = :ROS_MATRICULA AND ROS_ID = :ROS_ID');
    ParamByName('ROS_MATRICULA').Value := Matricula;
    ParamByName('ROS_ID').Value    := Ros_ID;
    Open;
    end;

    if DM.qrySQL.RecordCount = 0 THEN
    BEGIN
    if MessageDlg('Voc� esta prestes a alterar a Matricula: '+MatriculaAntiga+' para Matricula: '+MatriculaNova+#13+'           Deseja realmente realizar essa altera��o!?', mtConfirmation, [mbYes,mbNo], 0) = mrYes then
    begin
    If (NumCaracteres <> 11)  then
    Begin
    ShowMessage('A matricula deve conter 11 digitos, por favor digite novamente');
    Dm.Transacao.RollbackRetaining;
    Dm.IBRoster.Cancel;
    MouseShowCursor(True);
    End
    else
    begin
    Dm.Transacao.CommitRetaining;
    Dm.IBRoster.Post;
    MouseShowCursor(True);
    end;

    end;
    end;
    end;
    end;
    begin
    if ((DM.IBRoster.State = DsEdit) and (DBGrid6.SelectedField.FieldName = 'ROS_NOME')) then
    begin
    if not((Key in ['a'..'z',Chr(8)]) or (Key in ['A'..'Z',Chr(8)]) or (Key = #13)) then
    Key := #0;
    end;
    MouseShowCursor(false);

    if (DBGrid6.SelectedField.FieldName = 'ROS_NOME') THEN
    Key := AnsiUpperCase(Key)[Length(Key)];

    If (Key = #27) then
    begin
    MouseShowCursor(True);
    end;

    //Alterar Matricula com tecla Enter
    If (Key = #13) and (Dm.DsRoster.State = DsEdit)  then
    Begin

    Dm.IBRoster.Open;

    DM.IBRoster.Edit;

    Ros_ID := Dm.IBRosterROS_ID.value;

    Matricula := Dm.IBRosterROS_MATRICULA.OldValue;

    ConferenteAntigo := Dm.IBRosterROS_NOME.OldValue;

    ConferenteNovo := DM.IBRosterROS_NOME.value;

    NumCaracteres := Length(ConferenteNovo);

    Conferente :=(ConferenteNovo);

    MouseShowCursor(True);

    with dm.qrySQL do
    begin
    Close;
    SQL.Clear;
    SQL.Add('SELECT ROS_MATRICULA, ROS_ID FROM ROSTER ');
    SQL.Add('WHERE ROS_MATRICULA = :ROS_MATRICULA AND ROS_ID = :ROS_ID');
    ParamByName('ROS_MATRICULA').Value := Matricula;
    ParamByName('ROS_ID').Value    := Ros_ID;
    Open;
    end;

    if DM.qrySQL.RecordCount = 0 THEN
    BEGIN
    if MessageDlg('Voc� esta prestes a alterar o nome do conferente de : '+ConferenteAntigo+' para : '+ConferenteNovo+#13+'           Deseja realmente realizar essa altera��o!?', mtConfirmation, [mbYes,mbNo], 0) = mrYes then
    begin
    Dm.Transacao.CommitRetaining;
    Dm.IBRoster.Post;
    MouseShowCursor(True);
    End
    else
    begin
    Dm.Transacao.RollbackRetaining;
    Dm.IBRoster.Cancel;
    MouseShowCursor(True);
    end;

    end;
    end;
    end; }
end;

procedure TFrmInventario.DBGrid6KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  IDROSTER: Integer;
begin

  { IDROSTER := DM.IBRosterROS_ID.value;
    //N�O DEIXA INSERIR LINHAS EM BRANCO NO GRIG;
    if (KEY in [VK_DOWN]) and ( DBGrid6.DataSource.DataSet.RecNo = DBGrid6.DataSource.DataSet.RecordCount )  then
    abort;

    //Alterar Se��o com tecla de sobe e desce
    If ((Key = VK_DOWN) or (Key = VK_UP) or (Key = VK_PRIOR) or (Key = VK_NEXT))  and (Dm.DsRoster.State = DsEdit) then
    Begin
    key := 13;
    End; }
end;

procedure TFrmInventario.RelatriodeAuditoriaFaixadeQuantidadeSEOEAN1Click(Sender: TObject);
begin
  Application.CreateForm(TFrmAuditQtdSecEanReal, FrmAuditQtdSecEanReal); // cria o form
  FrmAuditQtdSecEanReal.ShowModal;
  FreeAndNil(FrmAuditQtdSecEanReal); // libera o form da memoria
end;

procedure TFrmInventario.btnImportClick(Sender: TObject);
begin
  Application.CreateForm(TFrmImportaCad, FrmImportaCad); // cria o form
  FrmImportaCad.ShowModal;
  FreeAndNil(FrmImportaCad); // libera o form da memoria
end;

procedure TFrmInventario.DBEdit9Click(Sender: TObject);
begin
  DM.IBInventario.Edit;
end;

procedure TFrmInventario.DBEdit9Exit(Sender: TObject);
begin
  // DM.IBInventario.Post;
  // DM.Transacao.CommitRetaining;
end;

procedure TFrmInventario.TabSheet1Exit(Sender: TObject);
begin
  If DM.IBInventario.State = Dsedit then
  begin
    DM.IBInventario.Post;
    DM.Transacao.CommitRetaining;
  end;
end;

procedure TFrmInventario.DBGrid1KeyPress(Sender: TObject; var Key: Char);
begin
  if DBGrid1.SelectedIndex = 3 then
  begin
    Key := #0;
    beep;
  end;
  if DBGrid1.SelectedIndex = 4 then
  begin
    Key := #0;
    beep;
  end;
end;

procedure TFrmInventario.Relatriodeprodutospareados1Click(Sender: TObject);
begin
  Try
    DM.QRelPareado.Close;
    DM.QRelPareado.Open;
    if DM.QRelPareado.RecordCount > 0 then
    Begin
      DM.RvPrjWis.Close;
      DM.RvPrjWis.projectfile := 'C:\SGD\Rave\RelPareados.rav';
      DM.RvPrjWis.Open;
      DM.RvSysWis.DefaultDest := rdPreview;
      DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
      DM.RvPrjWis.ExecuteReport('RelPareados');
      // Salvando relat�rio em PDF automaticamente
      Try
        DM.RvSysWis.DefaultDest := rdFile;
        DM.RvSysWis.DoNativeOutput := False;
        DM.RvSysWis.RenderObject := RvPDF;
        DM.RvSysWis.OutputFileName := 'C:\SGD\Relatorios\Relatorio de Produtos Pareados_' +
          DM.IBInventarioINV_LOJA.AsString + '_' + DM.IBInventarioINV_NUMLOJA.AsString + '.pdf';
        // caminho onde vai gerar o arquivo pdf
        DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
        DM.RvPrjWis.projectfile := 'C:\SGD\Rave\RelPareados.rav';
        // caminho onde esta o arquivo que ele vai exportar para pdf
        DM.RvPrjWis.Engine := DM.RvSysWis;
        DM.RvPrjWis.Execute;
      Except
        ShowMessage('Erro ao gerar o relat�rio em PDF');
      end;
      // Salvando relat�rio em TXT automaticamente

      Try
        DM.RvSysWis.DefaultDest := rdFile;
        DM.RvSysWis.DoNativeOutput := False;
        DM.RvSysWis.RenderObject := RvTxt;
        DM.RvSysWis.OutputFileName := 'C:\SGD\Relatorios\Relatorio de Produtos Pareados_' +
          DM.IBInventarioINV_LOJA.AsString + '_' + DM.IBInventarioINV_NUMLOJA.AsString + '.txt';
        // caminho onde vai gerar o arquivo pdf
        DM.RvSysWis.SystemSetups := DM.RvSysWis.SystemSetups - [ssAllowSetup];
        DM.RvPrjWis.projectfile := 'C:\SGD\Rave\RelPareados.rav';
        // caminho onde esta o arquivo que ele vai exportar para pdf
        DM.RvPrjWis.Engine := DM.RvSysWis;
        DM.RvPrjWis.Execute;
      except
        ShowMessage('Erro ao gerar o relat�rio em TXT');
      end;
    end

    else
      Application.MessageBox('N�o existem produtos pareados', 'Mensagem do sistema', MB_ICONERROR + MB_OK);
  Finally
    DM.QRelPiceCount.Close;
  End;

end;

procedure TFrmInventario.RelatriodeAuditoriaSint1Click(Sender: TObject);
begin
  Application.CreateForm(TFrmAudit_Sint, FrmAudit_Sint); // cria o form
  FrmAudit_Sint.ShowModal;
  FreeAndNil(FrmAudit_Sint); // libera o form da memoria
end;

procedure TFrmInventario.GerarArquivoTcnicoOnofre1Click(Sender: TObject);
begin
  GeraArqFinalONF;
end;

end.
