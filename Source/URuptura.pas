unit URuptura;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons, ComCtrls, RpRave,
  DB, RpCon, RpConDS, RpBase, RpSystem, RpDefine, Grids, DBGrids, RpRender,
  RpRenderPDF, RpRenderText, sBitBtn, sLabel;

type
  TFrmRuptura = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    StatusBar1: TStatusBar;
    Panel3: TPanel;
    Label3: TLabel;
    Label2: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Label4: TLabel;
    Label5: TLabel;
    Edit3: TEdit;
    Edit4: TEdit;
    BitBtn2: TBitBtn;
    Label6: TLabel;
    RvPDF: TRvRenderPDF;
    RvTxt: TRvRenderText;
    RgIntEan: TRadioGroup;
    sLabelFX1: TsLabelFX;
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmRuptura: TFrmRuptura;

implementation

uses UDm, Udm2, UFuncProc, UInventario, IBQuery;

{$R *.dfm}

procedure TFrmRuptura.BitBtn2Click(Sender: TObject);
begin
 case RgIntEan.ItemIndex of
  0:
    begin
     if (Edit1.Text = '') then
      Application.MessageBox('� necess�rio informar o Dep�sito Inicial.','Mensagem do Sistema', MB_ICONERROR + MB_OK)
     else
     if (Edit2.Text = '') then
      Application.MessageBox('� necess�rio informar o Dep�sito Final.','Mensagem do Sistema', MB_ICONERROR + MB_OK)
     else
     if (Edit3.Text = '') then
      Application.MessageBox('� necess�rio informar o Loja Inicial.','Mensagem do Sistema', MB_ICONERROR + MB_OK)
     else
     if (Edit4.Text = '') then
      Application.MessageBox('� necess�rio informar o Loja Final.','Mensagem do Sistema', MB_ICONERROR + MB_OK)
     else
      Begin
         try
          DM.IBContagens.Edit;
            // marca como Loja o campo deposito = null
             DM2.UpdateRupLoja.ExecSQL;
             DM.Transacao.CommitRetaining;
             Dm2.UpdateRupLoja.Close;

            // marca como Deposito o campo deposito = null
             Dm2.UpdateRupDeposito.ExecSQL;
             DM.Transacao.CommitRetaining;
             Dm2.UpdateRupDeposito.Close;

             //apaga conteudo tabela ruptura
             DM.QrySql2.Close;
             DM.QrySql2.SQL.Clear;
             DM.QrySql2.SQL.Add('delete from ruptura ');
             DM.QrySql2.ExecSQL;
             DM.Transacao.CommitRetaining;
             dm.QrySql2.Close;

             //alimenta tabela de ruptura com os ranges de se��o
             DM2.QRupturaRange.Close;
             DM2.QRupturaRange.Params.ParamByName('DepIni').Value   := Edit1.Text;
             DM2.QRupturaRange.Params.ParamByName('Depfim').Value   := Edit2.Text;
             DM2.QRupturaRange.Params.ParamByName('Rupini').Value   := Edit3.Text;
             DM2.QRupturaRange.Params.ParamByName('RupFim').Value   := Edit4.Text;
             DM2.QRupturaRange.ExecSQL;
             DM.Transacao.CommitRetaining;
             DM2.QRupturaRange.Close;

            DM.QImportaDados.Close;
            DM.QImportaDados.SQL.Clear;
            DM.QImportaDados.SQL.Add('SET STATISTICS INDEX RUPTURA_IDX1 ');
            DM.QImportaDados.ExecSQL;

            DM.QImportaDados.Close;
            DM.QImportaDados.SQL.Clear;
            DM.QImportaDados.SQL.Add('SET STATISTICS INDEX RUPTURA_IDX2 ');
            DM.QImportaDados.ExecSQL;
            DM.Transacao.CommitRetaining;

         try
           //selecione os ranges de se��es
           DM2.QRupturaTable.Close;
           DM2.QRupturaTable.Open;

           Dm2.QRupturaLoja.Close;
           Dm2.QRupturaLoja.Params.ParamByName('RUP_LOJA').Value     := Dm2.QRupturaTableRUP_DEPOSITO.Value;
           Dm2.QRupturaLoja.Params.ParamByName('RUP_LOJAFIM').Value  := Dm2.QRupturaTableRUP_DEPOSITOFIM.Value;
           Dm2.QRupturaLoja.ExecSQL;
           DM.Transacao.CommitRetaining;
           Dm2.QRupturaLoja.Close;

           DM2.QRupturaDeposito.Close;
           DM2.QRupturaDeposito.Params.ParamByName('RUP_DEPOSITO').Value     := Dm2.QRupturaTableRUP_LOJA.Value;
           DM2.QRupturaDeposito.Params.ParamByName('RUP_DEPOSITOFIM').Value  := Dm2.QRupturaTableRUP_LOJAFIM.Value;
           DM2.QRupturaDeposito.ExecSQL;
           DM.Transacao.CommitRetaining;
           DM2.QRupturaDeposito.Close;

            DM.QImportaDados.Close;
            DM.QImportaDados.SQL.Clear;
            DM.QImportaDados.SQL.Add('SET STATISTICS INDEX CONTAGENS_IDX1 ');
            DM.QImportaDados.ExecSQL;

            DM.QImportaDados.Close;
            DM.QImportaDados.SQL.Clear;
            DM.QImportaDados.SQL.Add('SET STATISTICS INDEX IDX_CONTAGENS ');
            DM.QImportaDados.ExecSQL;

            DM.QImportaDados.Close;
            DM.QImportaDados.SQL.Clear;
            DM.QImportaDados.SQL.Add('SET STATISTICS INDEX IDX_CONTAGENS1 ');
            DM.QImportaDados.ExecSQL;

            DM.QImportaDados.Close;
            DM.QImportaDados.SQL.Clear;
            DM.QImportaDados.SQL.Add('SET STATISTICS INDEX PK_CONTAGENS ');
            DM.QImportaDados.ExecSQL;

            DM.Transacao.CommitRetaining;

          except
            ShowMessage('N�o existem contagens para gerar o relat�rio de Ruptura!');
          end;

         except
           ShowMessage('Erro ao Gravar');
         end;
      end;
     ShowMessage('Grava��o do Range de Ruptura com sucesso!');
     Edit1.Clear;
     Edit2.Clear;
     Edit3.Clear;
     Edit4.Clear;

     Try
     DM2.QRelRuptura.Close;
     Dm2.QRelRuptura.Open;

     DM.RvPrjWis.Close;
     Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\RupturaEan.rav';
     Dm.RvPrjWis.Open;
     Dm.RvSysWis.DefaultDest := rdPreview;
     Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup] ;
     Dm.RvPrjWis.ExecuteReport('RupturaEan');
    Finally
      ShowMessage('Relat�rio Gerado com sucesso!');
    End;
     // Salvando relat�rio em PDF automaticamente
     Try
       DM.RvPrjWis.Close;
       Dm.RvSysWis.DefaultDest:= rdFile;
       Dm.RvSysWis.DoNativeOutput:= false;
       Dm.RvSysWis.RenderObject:= rvPdf;
       Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Relatorio Ruptura por c�digo Ean_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
       Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
       Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\RupturaEan.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
       Dm.RvPrjWis.Engine:= Dm.RvSysWis;
       Dm.RvPrjWis.Open;
       Dm.RvPrjWis.Execute;
     Except
       ShowMessage('Erro ao salvar o relat�rio em PDF');
     end;
      // Salvando relat�rio em TXT automaticamente
     Try
       DM.RvPrjWis.Close;
       Dm.RvSysWis.DefaultDest:= rdFile;
       Dm.RvSysWis.DoNativeOutput:= false;
       Dm.RvSysWis.RenderObject:= rvTxt;
       Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Relatorio Ruptura por c�digo Ean_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.txt'; //caminho onde vai gerar o arquivo pdf
       Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
       Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\RupturaEan.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
       Dm.RvPrjWis.Engine:= Dm.RvSysWis;
       Dm.RvPrjWis.Open;
       Dm.RvPrjWis.Execute;
     except
       ShowMessage('Erro ao salvar o relat�rio em TXT');
     end;
   end;
  1:
    begin
     if (Edit1.Text = '') then
      Application.MessageBox('� necess�rio informar o Dep�sito Inicial.','Mensagem do Sistema', MB_ICONERROR + MB_OK)
     else
     if (Edit2.Text = '') then
      Application.MessageBox('� necess�rio informar o Dep�sito Final.','Mensagem do Sistema', MB_ICONERROR + MB_OK)
     else
     if (Edit3.Text = '') then
      Application.MessageBox('� necess�rio informar o Loja Inicial.','Mensagem do Sistema', MB_ICONERROR + MB_OK)
     else
     if (Edit4.Text = '') then
      Application.MessageBox('� necess�rio informar o Loja Final.','Mensagem do Sistema', MB_ICONERROR + MB_OK)
     else
     Begin
         try
          DM.IBContagens.Edit;

             DM2.UpdateRupLoja.ExecSQL;
             DM.Transacao.CommitRetaining;
             Dm2.UpdateRupLoja.Close;

             Dm2.UpdateRupDeposito.ExecSQL;
             DM.Transacao.CommitRetaining;
             Dm2.UpdateRupDeposito.Close;

             DM.QrySql2.Close;
             DM.QrySql2.SQL.Clear;
             DM.QrySql2.SQL.Add('delete from ruptura ');
             DM.QrySql2.ExecSQL;
             DM.Transacao.CommitRetaining;
             dm.QrySql2.Close;

             DM2.QRupturaRange.Close;
             DM2.QRupturaRange.Params.ParamByName('DepIni').Value   := Edit1.Text;
             DM2.QRupturaRange.Params.ParamByName('Depfim').Value   := Edit2.Text;
             DM2.QRupturaRange.Params.ParamByName('Rupini').Value   := Edit3.Text;
             DM2.QRupturaRange.Params.ParamByName('RupFim').Value   := Edit4.Text;
             DM2.QRupturaRange.ExecSQL;
             DM.Transacao.CommitRetaining;
             DM2.QRupturaRange.Close;

            DM.QImportaDados.Close;
            DM.QImportaDados.SQL.Clear;
            DM.QImportaDados.SQL.Add('SET STATISTICS INDEX RUPTURA_IDX1 ');
            DM.QImportaDados.ExecSQL;

            DM.QImportaDados.Close;
            DM.QImportaDados.SQL.Clear;
            DM.QImportaDados.SQL.Add('SET STATISTICS INDEX RUPTURA_IDX2 ');
            DM.QImportaDados.ExecSQL;
            DM.Transacao.CommitRetaining;
          try

           DM2.QRupturaTable.Close;
           DM2.QRupturaTable.Open;

           Dm2.QRupturaLojaInt.Close;
           Dm2.QRupturaLojaInt.Params.ParamByName('RUP_LOJA').Value     := Dm2.QRupturaTableRUP_DEPOSITO.Value;
           Dm2.QRupturaLojaInt.Params.ParamByName('RUP_LOJAFIM').Value  := Dm2.QRupturaTableRUP_DEPOSITOFIM.Value;
           Dm2.QRupturaLojaInt.ExecSQL;
           DM.Transacao.CommitRetaining;
           Dm2.QRupturaLojaInt.Close;

           DM2.QRupturaDepositoInt.Close;
           DM2.QRupturaDepositoInt.Params.ParamByName('RUP_DEPOSITO').Value     := Dm2.QRupturaTableRUP_LOJA.Value;
           DM2.QRupturaDepositoInt.Params.ParamByName('RUP_DEPOSITOFIM').Value  := Dm2.QRupturaTableRUP_LOJAFIM.Value;
           DM2.QRupturaDepositoInt.ExecSQL;
           DM.Transacao.CommitRetaining;
           DM2.QRupturaDepositoInt.Close;

           DM.QImportaDados.Close;
           DM.QImportaDados.SQL.Clear;
           DM.QImportaDados.SQL.Add('SET STATISTICS INDEX CONTAGENS_IDX1 ');
           DM.QImportaDados.ExecSQL;

           DM.QImportaDados.Close;
           DM.QImportaDados.SQL.Clear;
           DM.QImportaDados.SQL.Add('SET STATISTICS INDEX IDX_CONTAGENS ');
           DM.QImportaDados.ExecSQL;

           DM.QImportaDados.Close;
           DM.QImportaDados.SQL.Clear;
           DM.QImportaDados.SQL.Add('SET STATISTICS INDEX IDX_CONTAGENS1 ');
           DM.QImportaDados.ExecSQL;

           DM.QImportaDados.Close;
           DM.QImportaDados.SQL.Clear;
           DM.QImportaDados.SQL.Add('SET STATISTICS INDEX PK_CONTAGENS ');
           DM.QImportaDados.ExecSQL;
           DM.Transacao.CommitRetaining;

           except
            ShowMessage('N�o existem contagens para gerar o relat�rio de Ruptura!');
          end;

         except
           ShowMessage('Erro ao Gravar');
         end;
     end;
     ShowMessage('Grava��o do Range de Ruptura com sucesso!');
     Edit1.Clear;
     Edit2.Clear;
     Edit3.Clear;
     Edit4.Clear;

     DM2.QRelRupturaInt.Close;
     Dm2.QRelRupturaInt.Open;

     if (Dm.IBInventarioINV_LOJA.Value = 'STM') or (Dm.IBInventarioINV_LOJA.Value = 'ETY') then
       begin
         Try
         DM.RvPrjWis.Close;
         Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\RupturaInterno_STM.rav';
         Dm.RvPrjWis.Open;
         Dm.RvSysWis.DefaultDest := rdPreview;
         Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup] ;
         Dm.RvPrjWis.ExecuteReport('RupturaInterno_STM');
         Finally
           ShowMessage('Relat�rio Gerado com sucesso!');
         End;

        // Salvando relat�rio em PDF automaticamente
         Try
           DM.RvPrjWis.Close;
           Dm.RvSysWis.DefaultDest:= rdFile;
           Dm.RvSysWis.DoNativeOutput:= false;
           Dm.RvSysWis.RenderObject:= rvPdf;
           Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Relatorio Ruptura por c�digo interno_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
           Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
           Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\RupturaInterno_STM.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
           Dm.RvPrjWis.Engine:= Dm.RvSysWis;
           Dm.RvPrjWis.Open;
           Dm.RvPrjWis.Execute;
         Except
           ShowMessage('Erro ao salvar o relat�rio em PDF');
         end;

        // Salvando relat�rio em TXT automaticamente
         Try
           DM.RvPrjWis.Close;
           Dm.RvSysWis.DefaultDest:= rdFile;
           Dm.RvSysWis.DoNativeOutput:= false;
           Dm.RvSysWis.RenderObject:= rvTxt;
           Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Relatorio Ruptura por c�digo interno_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.txt'; //caminho onde vai gerar o arquivo pdf
           Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
           Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\RupturaInterno_STM.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
           Dm.RvPrjWis.Engine:= Dm.RvSysWis;
           Dm.RvPrjWis.Open;
           Dm.RvPrjWis.Execute;
          except
           ShowMessage('Erro ao salvar o relat�rio em TXT');
         end;
       end
    Else
      begin
        Try
          DM.RvPrjWis.Close;
          Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\RupturaInterno.rav';
          Dm.RvPrjWis.Open;
          Dm.RvSysWis.DefaultDest := rdPreview;
          Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup] ;
          Dm.RvPrjWis.ExecuteReport('RupturaInterno');
        Finally
          ShowMessage('Relat�rio Gerado com sucesso!');
        End;

       // Salvando relat�rio em PDF automaticamente
        Try
          DM.RvPrjWis.Close;
          Dm.RvSysWis.DefaultDest:= rdFile;
          Dm.RvSysWis.DoNativeOutput:= false;
          Dm.RvSysWis.RenderObject:= rvPdf;
          Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Relatorio Ruptura por c�digo interno_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
          Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
          Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\RupturaInterno.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
          Dm.RvPrjWis.Engine:= Dm.RvSysWis;
          Dm.RvPrjWis.Open;
          Dm.RvPrjWis.Execute;
        Except
          ShowMessage('Erro ao salvar o relat�rio em PDF');
        end;

       // Salvando relat�rio em TXT automaticamente
        Try
          DM.RvPrjWis.Close;
          Dm.RvSysWis.DefaultDest:= rdFile;
          Dm.RvSysWis.DoNativeOutput:= false;
          Dm.RvSysWis.RenderObject:= rvTxt;
          Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Relatorio Ruptura por c�digo interno_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.txt'; //caminho onde vai gerar o arquivo pdf
          Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
          Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\RupturaInterno.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
          Dm.RvPrjWis.Engine:= Dm.RvSysWis;
          Dm.RvPrjWis.Open;
          Dm.RvPrjWis.Execute;
         except
          ShowMessage('Erro ao salvar o relat�rio em TXT');
        end;
      end;
 end;
end;
end;

end.

