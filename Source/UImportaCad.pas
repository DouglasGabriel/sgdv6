unit UImportaCad;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Grids, DBGrids, Buttons, ComCtrls, StrUtils,
  sGauge, Data.DB, sLabel, ShellAnimations;

type
  TFrmImportaCad = class(TForm)
    BitBtn1: TBitBtn;
    Panel1: TPanel;
    Panel2: TPanel;
    StatusBar1: TStatusBar;
    Panel3: TPanel;
    Panel4: TPanel;
    DBGrid1: TDBGrid;
    BitBtn2: TBitBtn;
    gauge: TsGauge;
    BitBtnMeia: TButton;
    Memo1: TMemo;
    MMOrdena: TMemo;
    sLabelFX1: TsLabelFX;
    Animate1: TAnimate;
    procedure FormShow(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtnMeiaClick(Sender: TObject);
    procedure bbtesteClick(Sender: TObject);
  private

  public
    { Public declarations }
  end;

var
  FrmImportaCad: TFrmImportaCad;

implementation

uses UDm, UFuncProc, UPrincipal, ULogin, Udm2;

{$R *.dfm}

procedure TFrmImportaCad.FormShow(Sender: TObject);
begin
  // Abrir tabela
  Dm.IBInventario.Open;

  // Verificar se o invent�rio esta cadastrado para ativar o bot�o para importa��o
  if (Dm.IBInventario.RecordCount = 1) and (Dm.IBInventarioINV_CADASTRO.AsBoolean = False) then
  Begin
    BitBtn1.Enabled := True;
    BitBtn2.Enabled := False;
    // BitBtn3.Enabled := False;
  End
  else if (Dm.IBInventario.RecordCount = 1) and (Dm.IBInventarioINV_CADASTRO.AsBoolean = True) then
  Begin
    BitBtn1.Enabled := True;
    BitBtn2.Enabled := True;
    // BitBtn3.Enabled := True;
  End
  else if (Dm.IBInventario.RecordCount = 0) then
  Begin
    BitBtn1.Enabled := False;
    BitBtn2.Enabled := False;
    // BitBtn3.Enabled := False;
  End;

  If NivelUsu = 2 then
  begin
    // BitBtn4.Enabled := False;
    // BitBtn5.Enabled := False;
  end;

  If Dm.IBInventarioINV_LOJA.Value = 'GDS' then
  begin
    BitBtnMeia.Caption := 'Saldos GDS';
    BitBtnMeia.Visible := True;
  end;

  { If Dm.IBInventarioINV_LOJA.Value = 'MRS' then
    begin
    BitBtnMeia.Visible := True;
    end
    else
    begin
    BitBtnMeia.Visible := False;
    end; }

end;

procedure TFrmImportaCad.BitBtn1Click(Sender: TObject);
var
  // Contador : integer;
  Txt: Textfile;
  // Caminho: String;
  // Entrada: string;
  // NUMLOJA: String[4];
  // DESCRICAO: String[20];
  // EAN : String[13];
  // QTD: string[5];
  // PRECO: string[7];
  // PRECODEC: string[2];
  // PRECOFINAL: string[10];
  // COD_INT : string[6];
  // TotaldeLinhas :integer;
  // aContLinhas: TStringList;

begin
  Animate1.Visible := true;
  Animate1.Active := True;
  if (Dm.IBInventarioINV_LOJA.Value = 'CP1') or (Dm.IBInventarioINV_LOJA.Value = 'CP2') or
    (Dm.IBInventarioINV_LOJA.Value = 'CP3') then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdCPV;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  Else If Dm.IBInventarioINV_LOJA.Value = 'SAR' then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdSAR;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  else If Dm.IBInventarioINV_LOJA.Value = 'TNT' then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdTNT;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  else If Dm.IBInventarioINV_LOJA.Value = 'DIA' then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdDIA;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  else If Dm.IBInventarioINV_LOJA.Value = 'FRN' then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdFRN;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  else If Dm.IBInventarioINV_LOJA.Value = 'SAF' then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdSAF;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  else If Dm.IBInventarioINV_LOJA.Value = 'RLD' then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdRLD;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  else If Dm.IBInventarioINV_LOJA.Value = 'AND' then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdAND;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  else If Dm.IBInventarioINV_LOJA.Value = 'MRS' then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdMRS;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  else If Dm.IBInventarioINV_LOJA.Value = 'CEA' then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdCEA;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  else If Dm.IBInventarioINV_LOJA.Value = 'PRN' then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdPRN;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  else If (Dm.IBInventarioINV_LOJA.Value = 'ONF') or (Dm.IBInventarioINV_LOJA.Value = 'OEL') then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdONF;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  else If (Dm.IBInventarioINV_LOJA.Value = 'OCD') or (Dm.IBInventarioINV_LOJA.Value = 'ONP') then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdOCD;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  else If Dm.IBInventarioINV_LOJA.Value = 'PET' then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdPET;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  else If Dm.IBInventarioINV_LOJA.Value = 'CIC' then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdCIC;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  else If Dm.IBInventarioINV_LOJA.Value = 'SMP' then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdSMP;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  else If Dm.IBInventarioINV_LOJA.Value = 'SMK' then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdSMK;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  else If Dm.IBInventarioINV_LOJA.Value = 'SMD' then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdSMK;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  else If Dm.IBInventarioINV_LOJA.Value = 'PDA' then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdPDA;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  else If Dm.IBInventarioINV_LOJA.Value = 'VLG' then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdVLG;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  else If Dm.IBInventarioINV_LOJA.Value = 'DLG' then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdDLG;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  else If (Dm.IBInventarioINV_LOJA.Value = 'STM') or (Dm.IBInventarioINV_LOJA.Value = 'ETY') then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdSTM;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  else If Dm.IBInventarioINV_LOJA.Value = 'LOP' then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdLOP;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  else If Dm.IBInventarioINV_LOJA.Value = 'TMC' then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdTMC;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  else If Dm.IBInventarioINV_LOJA.Value = 'BAR' then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdBAR;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  else If Dm.IBInventarioINV_LOJA.Value = 'MAX' then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdMAX;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  else

    If Dm.IBInventarioINV_LOJA.Value = 'SEB' then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdSEB;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  else If Dm.IBInventarioINV_LOJA.Value = 'NIK' then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdNIK;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  else If Dm.IBInventarioINV_LOJA.Value = 'ROS' then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdROS;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  { else
    If Dm.IBInventarioINV_LOJA.Value = 'CEC' then
    begin
    Dm.IBCadastro.Open;
    ImportaCadProdCEC;
    Dm.IBCadastro.Close;
    gauge.Progress:=1;
    end }
  else If Dm.IBInventarioINV_LOJA.Value = 'BGB' then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdBGB;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  else If Dm.IBInventarioINV_LOJA.Value = 'WAL' then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdWAL;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  Else If Dm.IBInventarioINV_LOJA.Value = 'GLM' then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdGLM;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  Else If Dm.IBInventarioINV_LOJA.Value = 'CLS' then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdCLS;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  Else If Dm.IBInventarioINV_LOJA.Value = 'JFL' then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdJFL;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  Else If Dm.IBInventarioINV_LOJA.Value = 'ALX' then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdALX;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  Else If Dm.IBInventarioINV_LOJA.Value = 'PLT' then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdPLT;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  Else If Dm.IBInventarioINV_LOJA.Value = 'IDL' then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdIDL;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  Else If Dm.IBInventarioINV_LOJA.Value = 'NCL' then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdNCL;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  Else If Dm.IBInventarioINV_LOJA.Value = 'LOC' then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdLOC;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  Else If Dm.IBInventarioINV_LOJA.Value = 'BYD' then
  begin
    Dm.IBCadastro.Open;
    ImportaCadProdBYD;
    Dm.IBCadastro.Close;
    gauge.Progress := 1;
  end
  else
    // NATURA
    if Dm.IBInventarioINV_LOJA.Value = 'NTR' then
    begin
      Dm.IBCadastro.Open;
      ImportaCadProdNTR;
      Dm.IBCadastro.Close;
      gauge.Progress := 1;
    end
    else
      // NATURA
      if Dm.IBInventarioINV_LOJA.Value = 'EDR' then
      begin
        Dm.IBCadastro.Open;
        ImportaCadProdEDR;
        Dm.IBCadastro.Close;
        gauge.Progress := 1;
      end
      else if Dm.IBInventarioINV_LOJA.Value = 'DFY' then
      begin
        Dm.IBCadastro.Open;
        ImportaCadProdDFY;
        Dm.IBCadastro.Close;
        gauge.Progress := 1;
      end
      else if Dm.IBInventarioINV_LOJA.Value = 'BVL' then
      begin
        Dm.IBCadastro.Open;
        ImportaCadProdBVL;
        Dm.IBCadastro.Close;
        gauge.Progress := 1;
      end
      else if Dm.IBInventarioINV_LOJA.Value = 'CND' then
      begin
        Dm.IBCadastro.Open;
        ImportaCadProdCND;
        Dm.IBCadastro.Close;
        gauge.Progress := 1;
      end
      else if Dm.IBInventarioINV_LOJA.Value = 'SLL' then
      begin
        Dm.IBCadastro.Open;
        ImportaCadProdSLL;
        Dm.IBCadastro.Close;
        gauge.Progress := 1;
      end
      else if Dm.IBInventarioINV_LOJA.Value = 'SMN' then
      begin
        Dm.IBCadastro.Open;
        ImportaCadProdSMN;
        Dm.IBCadastro.Close;
        gauge.Progress := 1;
      end
      else if Dm.IBInventarioINV_LOJA.Value = 'MBZ' then
      begin
        Dm.IBCadastro.Open;
        ImportaCadProdMBZ;
        Dm.IBCadastro.Close;
        gauge.Progress := 1;
      end
      else if Dm.IBInventarioINV_LOJA.Value = 'DVL' then
      begin
        Dm.IBCadastro.Open;
        ImportaCadProdDVL;
        Dm.IBCadastro.Close;
        gauge.Progress := 1;
      end
      Else if (Dm.IBInventarioINV_LOJA.Value = 'LUB') or (Dm.IBInventarioINV_LOJA.Value = 'EME') then
      begin
        Dm.IBCadastro.Open;
        ImportaCadProdLUB;
        Dm.IBCadastro.Close;
        gauge.Progress := 1;
      end
      Else if (Dm.IBInventarioINV_LOJA.Value = 'LLB') or (Dm.IBInventarioINV_LOJA.Value = 'BBO') or
        (Dm.IBInventarioINV_LOJA.Value = 'JHJ') or (Dm.IBInventarioINV_LOJA.Value = 'NOR') or
        (Dm.IBInventarioINV_LOJA.Value = 'RSC') then
      begin
        Dm.IBCadastro.Open;
        ImportaCadProdLLB;
        Dm.IBCadastro.Close;
        gauge.Progress := 1;
      end
      Else if Dm.IBInventarioINV_LOJA.Value = 'MKR' then
      begin
        Dm.IBCadastro.Open;
        ImportaCadProdMKR;
        Dm.IBCadastro.Close;
        gauge.Progress := 1;
      end
      Else if Dm.IBInventarioINV_LOJA.Value = 'LVL' then
      begin
        Dm.IBCadastro.Open;
        ImportaCadProdLVL;
        Dm.IBCadastro.Close;
        gauge.Progress := 0;
      end
      Else if Dm.IBInventarioINV_LOJA.Value = 'BHC' then
      begin
        Dm.IBCadastro.Open;
        ImportaCadProdBHC;
        Dm.IBCadastro.Close;
        gauge.Progress := 0;
      end
      Else if Dm.IBInventarioINV_LOJA.Value = 'PMT' then
      begin
        Dm.IBCadastro.Open;
        ImportaCadProdPMT;
        Dm.IBCadastro.Close;
        gauge.Progress := 0;
      end
      Else if Dm.IBInventarioINV_LOJA.Value = 'GDS' then
      begin
        Dm.IBCadastro.Open;
        ImportaCadProdGDS;
        Dm.IBCadastro.Close;
        gauge.Progress := 0;
      end
      Else if Dm.IBInventarioINV_LOJA.Value = 'PSL' then
      begin
        Dm.IBCadastro.Open;
        ImportaCadProdPSL;
        Dm.IBCadastro.Close;
        gauge.Progress := 0;
      end
      Else if (Dm.IBInventarioINV_LOJA.Value = 'SET') or (Dm.IBInventarioINV_LOJA.Value = 'SEP') then
      begin
        Dm.IBCadastro.Open;
        ImportaCadProdSET;
        Dm.IBCadastro.Close;
        gauge.Progress := 0;
      end
      Else if Dm.IBInventarioINV_LOJA.Value = 'RCH' then
      begin
        Dm.IBCadastro.Open;
        ImportaCadProdRCH;
        Dm.IBCadastro.Close;
        gauge.Progress := 0;
      end
      Else if Dm.IBInventarioINV_LOJA.Value = 'MCP' then
      begin
        Dm.IBCadastro.Open;
        ImportaCadProdMCP;
        Dm.IBCadastro.Close;
        gauge.Progress := 0;
      end
      Else if Dm.IBInventarioINV_LOJA.Value = 'FMS' then
      begin
        Dm.IBCadastro.Open;
        ImportaCadProdFMS;
        Dm.IBCadastro.Close;
        gauge.Progress := 0;
      end
      Else if Dm.IBInventarioINV_LOJA.Value = 'MCF' then
      begin
        Dm.IBCadastro.Open;
        ImportaCadProdMCF;
        Dm.IBCadastro.Close;
        gauge.Progress := 0;
      end
      Else If Dm.IBInventarioINV_LOJA.Value = 'SRR' then
      begin
        Dm.IBCadastro.Open;
        ImportaCadProdSRR;
        Dm.IBCadastro.Close;
        gauge.Progress := 1;
      end
      Else If Dm.IBInventarioINV_LOJA.Value = 'TNG' then
      begin
        Dm.IBCadastro.Open;
        ImportaCadProdTNG;
        Dm.IBCadastro.Close;
        gauge.Progress := 1;
      end
      Else If Dm.IBInventarioINV_LOJA.Value = 'SED' then
      begin
        Dm.IBCadastro.Open;
        ImportaCadProdSED;
        Dm.IBCadastro.Close;
        gauge.Progress := 1;
      end
      Else If Dm.IBInventarioINV_LOJA.Value = 'ANC' then
      begin
        Dm.IBCadastro.Open;
        ImportaCadProdANC;
        Dm.IBCadastro.Close;
        gauge.Progress := 1;
      end
      Else If Dm.IBInventarioINV_LOJA.Value = 'PNB' then
      begin
        Dm.IBCadastro.Open;
        ImportaCadProdPNB;
        Dm.IBCadastro.Close;
        gauge.Progress := 1;
      end
      Else If Dm.IBInventarioINV_LOJA.Value = 'CPS' then
      begin
        Dm.IBCadastro.Open;
        ImportaCadProdCPS;
        Dm.IBCadastro.Close;
        gauge.Progress := 1;
      end
      Else If Dm.IBInventarioINV_LOJA.Value = 'DSP' then
        begin
          Dm.IBCadastro.Open;
          ImportaCadProdDSP;
          Dm.IBCadastro.Close;
          gauge.Progress := 1;
        end
      else If Dm.IBInventarioINV_LOJA.Value = 'OUT' then
        begin
          Dm.IBCadastro.Open;
          ImportaCadProdOUT;
          Dm.IBCadastro.Close;
          gauge.Progress := 1;
        end
      else
      begin
        Dm.IBCadastro.Open;
        ImportaCadastro;
        Dm.IBCadastro.Close;
        gauge.Progress := 1;
      end;

  begin
    FrmPrincipal.btnAbrir.Enabled := True; // Habilita o bot�o abrir um invent�rio quando o cadastro for importado.
    FrmPrincipal.btnRestore.Enabled := True;
    FrmPrincipal.btnBackup.Enabled := True;
  end;

  Memo1.Lines.Add('-----------------------------------------------------------------------------');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('INFORMA��ES SOBRE O INVENT�RIO');
  Memo1.Lines.Add('');
  Memo1.Lines.Add('O.S: ' + Dm.IBInventarioINV_OS.Value);
  Memo1.Lines.Add('Data do Invent�rio: ' + Dm.IBInventarioINV_DATAINV.AsString);
  Memo1.Lines.Add('Cliente: ' + Dm.IBInventarioINV_LOJA.Value);
  Memo1.Lines.Add('Loja: ' + Dm.IBInventarioINV_NUMLOJA.Value);
  Memo1.Lines.Add('L�der do Invent�rio: ' + Dm.IBInventarioINV_GESTOR.Value);
  Memo1.Lines.Add('Telefone: ' + Dm.IBInventarioINV_TELEFONE.Value);
  Memo1.Lines.Add('Endere�o: ' + Dm.IBInventarioINV_ENDERECO.Value);
  Memo1.Lines.Add('Gerente da Loja: ' + Dm.IBInventarioINV_Gerente.Value);
  Memo1.Lines.Add('Status do Invent�rio: ' + Dm.IBInventarioINV_STATUS.Value);
  Memo1.Lines.Add('-----------------------------------------------------------------------------');
  Memo1.Lines.Add('');
  Animate1.Active := False;
  Animate1.Visible := False;
  Memo1.Lines.SaveToFile('C:\SGD\Log\LogText.log');
  AdicionaLog(DateTimeToStr(Now) + '> Importa��o de Cadastro do Cliente realizada com sucesso! ');
  Screen.Cursor := crDefault;
end;

procedure TFrmImportaCad.BitBtn2Click(Sender: TObject);
Var
  Txt: Textfile;
  EAN_ANTERIOR, COD_INT: String;
  ImpEAN, ImpCOD_INT, ImpDESCRICAO: TStringList;
  SizeDESCRICAO, SizeEAN, SizeCOD_INT, CONT: integer;
  Lista: TStringList;
  entrada: string;
  contador, totaldelinhas, i: integer;
  endereco: string;

  // Auxiliares para carga de cadastro grandes
  cad_id_ini, cad_id_fim, cad_id_ult: integer;
  array_produto: TStringList;

Begin
  Screen.Cursor := crSqlWait;
  try
    CONT := 0;

    gauge.Progress := 0;
    ImpEAN := TStringList.Create;
    ImpCOD_INT := TStringList.Create;
    ImpDESCRICAO := TStringList.Create;
    Dm.IBLayoutInput.Open;
    Dm.IBInventario.Open;

    With Dm.IBLayoutInput Do
    begin
      Close;
      SelectSql.Clear;
      SelectSql.Add('SELECT * FROM LAYOUT_INPUT WHERE CLIENTE = :CLIENTE');
      ParamByName('CLIENTE').AsString := Dm.IBInventarioINV_LOJA.Value;
      Open;
      Try
        ExtractStrings([','], [' '], PChar(Dm.IBLayoutInputEAN.Value), ImpEAN);
        SizeEAN := StrToInt(ImpEAN[1]);
      except
      End;
      Try
        ExtractStrings([','], [' '], PChar(Dm.IBLayoutInputCOD_INT.Value), ImpCOD_INT);
        if ImpCOD_INT[0] <> '*' Then
          SizeCOD_INT := StrToInt(ImpCOD_INT[1])
        Else
          SizeCOD_INT := 0
      except
        SizeCOD_INT := 0;
      End;
      Try
        ExtractStrings([','], [' '], PChar(Dm.IBLayoutInputDESCRICAO.Value), ImpDESCRICAO);
        If ImpDESCRICAO[0] <> '*' Then
          SizeDESCRICAO := 20
        else
          SizeDESCRICAO := 0
          // SizeDESCRICAO :=  StrToInt(ImpDESCRICAO[1]);
      except
        SizeDESCRICAO := 0;
      End;
    End;

    WITH Dm.qrySQL DO
    BEGIN
      Close;
      SQL.Clear;
      SQL.Add('SELECT * FROM INVENTARIO');
      Open;
    end;

    IF (Dm.qrySQL.FieldByName('INV_LOJA').Value = 'CP1') or (Dm.qrySQL.FieldByName('INV_LOJA').Value = 'CP2') then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_CODINT, CAD_DESC FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_CODINT, CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY CAD_CODINT');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_CODINT').AsString, 12, '0') +
          AjustaStrEsq(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, '0'));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'CP3' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_CODINT, CAD_EAN, CAD_DESC FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_CODINT, CAD_EAN, CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 20, ' ') +
          AjustaStrEsq(Dm.Query1.FieldByName('CAD_CODINT').AsString, 12, '0') +
          AjustaStrEsq(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, '0'));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    else IF (Dm.qrySQL.FieldByName('INV_LOJA').Value = 'SAR') or (Dm.qrySQL.FieldByName('INV_LOJA').Value = 'SRR') then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_CODINT, CAD_EAN  FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_CODINT, CAD_EAN ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 13, '0'));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'DIA' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_CODINT, CAD_EAN, CAD_DESC  FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_CODINT, CAD_EAN, CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 13, '0') +
          AjustaStrEsq(Dm.Query1.FieldByName('CAD_CODINT').AsString, 6, '0') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'TMC' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_CODINT, CAD_EAN, CAD_DESC  FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_CODINT, CAD_EAN, CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 15, '0') +
          AjustaStrEsq(Dm.Query1.FieldByName('CAD_CODINT').AsString, 10, '0') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'TNT' then
    begin
      Application.ProcessMessages;
      EAN_ANTERIOR := '';
      Dm2.CDSImportaCadastroTNT.Open;
      Dm2.CDSImportaCadastroTNT.First;
      Dm2.CDSImportaCadastroTNT.Last;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm2.CDSImportaCadastroTNT.RecordCount;

      Dm2.CDSImportaCadastroTNT.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\produto.txt');
      ReWrite(Txt);

      while not Dm2.CDSImportaCadastroTNT.Eof do
      Begin
        IF EAN_ANTERIOR <> Dm2.CDSImportaCadastroTNTCAD_EAN.Value THEN
        Begin
          Writeln(Txt, AjustaStrDir(Dm2.CDSImportaCadastroTNTCAD_EAN.AsString, 18, ' ') +
            AjustaStrDir(Dm2.CDSImportaCadastroTNTCAD_CODINT.AsString, 18, ' ') +
            AjustaStrDir(Dm2.CDSImportaCadastroTNTCAD_DESC.Value, 20, ' '));
        End;
        EAN_ANTERIOR := Dm2.CDSImportaCadastroTNTCAD_EAN.Value;
        Dm2.CDSImportaCadastroTNT.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);

    end
    else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'FRN' then
    begin
      Application.ProcessMessages;
      Dm.QDeletaTabela.Close;
      Dm.QDeletaTabela.SQL.Clear;
      Dm.QDeletaTabela.SQL.Add('DELETE FROM PRODUTO');
      Dm.QDeletaTabela.Open;
      Dm.Transacao.CommitRetaining;
      Dm.QDeletaTabela.Close;
      // seta o generator da tabela cadastro para 0
      Dm.QDeletaTabela.Close;
      Dm.QDeletaTabela.SQL.Clear;
      Dm.QDeletaTabela.SQL.Add('SET GENERATOR PRODUTO_PROD_ID_GEN TO 0');
      Dm.QDeletaTabela.Open;
      Dm.Transacao.CommitRetaining;
      Dm.QDeletaTabela.Close;

      // Abri a tabela e o TXT para
      // come�ar a importa��o

      AssignFile(Txt, ('C:\SGD\Cadastro\CADASTRO_PROD.txt')); // NOME do arquivo texto
      Reset(Txt);
      caminho := 'C:\SGD\Cadastro\cadastro_prod.txt';
      contador := 0;
      Lista := TStringList.Create;
      AssignFile(Txt, 'c:\SGD\Cadastro\cadastro_prod.txt');
      Reset(Txt);
      // Verifica o total de linhas no TXT para o progressbar
      totaldelinhas := quantaslinhastxt('C:\SGD\Cadastro\CADASTRO_PROD.txt');

      FrmImportaCad.gauge.MaxValue := totaldelinhas;

      Dm.QImportaDados.Close;
      Dm.QImportaDados.SQL.Clear;
      Dm.QImportaDados.SQL.Add('SET STATISTICS INDEX PK_PRODUTO ');
      Dm.QImportaDados.ExecSQL;
      Application.ProcessMessages;

      Dm.QImportaDados.Close;
      Dm.QImportaDados.SQL.Clear;
      Dm.QImportaDados.SQL.Add('SET STATISTICS INDEX IDX_PRODUTO ');
      Dm.QImportaDados.ExecSQL;
      Application.ProcessMessages;

      Try
        try
          While Not Eoln(Txt) do
          begin

            Dm.QueryImporta.Close;
            Dm.QueryImporta.SQL.Clear;
            Dm.QueryImporta.SQL.Add('INSERT INTO PRODUTO(PROD_EAN, PROD_CODINT, PROD_DESC) ');
            Dm.QueryImporta.SQL.Add('VALUES (:PROD_EAN, :PROD_CODINT, :PROD_DESC) ');
            Application.ProcessMessages;

            Readln(Txt, entrada);

            Dm.QueryImporta.ParamByName('PROD_CODINT').Value := RetiraSpace(Copy(entrada, 1, 15));
            Dm.QueryImporta.ParamByName('PROD_DESC').Value := RetiraSpace(Copy(entrada, 17, 41));
            Dm.QueryImporta.ParamByName('PROD_EAN').Value := StringReplace(RetiraSpace(Copy(entrada, 59, 17)), '-', '',
              [rfReplaceAll, rfIgnoreCase]) + RetiraSpace(Copy(entrada, 1, 15));

            Dm.QueryImporta.ExecSQL;
            contador := contador + 1;
            FrmImportaCad.gauge.Progress := FrmImportaCad.gauge.Progress + 1;
            Application.ProcessMessages;
          end;
        except
          ShowMessage('erro na linha ' + IntToStr(contador));
        end;

      Finally
        CloseFile(Txt);
        Application.ProcessMessages;
        Dm.Transacao.CommitRetaining;
        Dm.QueryImporta.Close;
        Application.MessageBox('Importado arquivo IGT com sucesso!' + #13 + 'Aguarde enquanto gera o arquivo ...',
          'Mensagem do sistema', MB_ICONINFORMATION + MB_OK);
        Screen.Cursor := crDefault;
        FrmImportaCad.gauge.Progress := 0;
      end;
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT PROD_EAN, PROD_DESC FROM PRODUTO ');
      Dm.Query1.SQL.Add('GROUP BY PROD_EAN, PROD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY PROD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrDir(Dm.Query1.FieldByName('PROD_EAN').AsString, 20, ' ') +
          AjustaStrDir(Dm.Query1.FieldByName('PROD_DESC').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'SAF' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_CODINT, CAD_EAN, CAD_DESC FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_CODINT, CAD_EAN, CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 13, '0') +
          AjustaStrEsq(Dm.Query1.FieldByName('CAD_CODINT').AsString, 9, '0') +
          AjustaStrEsq(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, '0'));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'RLD' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_CODINT, CAD_EAN, CAD_DESC FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_CODINT, CAD_EAN, CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 18, '0') +
          AjustaStrEsq(Dm.Query1.FieldByName('CAD_CODINT').AsString, 10, '0') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    else IF (Dm.qrySQL.FieldByName('INV_LOJA').Value = 'ONF') OR (Dm.qrySQL.FieldByName('INV_LOJA').Value = 'OEL') OR (Dm.qrySQL.FieldByName('INV_LOJA').Value = 'DSP')then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_CODINT, CAD_EAN, CAD_DESC FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_CODINT, CAD_EAN, CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 13, '0') +
          AjustaStrEsq(Dm.Query1.FieldByName('CAD_CODINT').AsString, 10, '0') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    else IF (Dm.qrySQL.FieldByName('INV_LOJA').Value = 'OCD') or (Dm.qrySQL.FieldByName('INV_LOJA').Value = 'ONP') then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_CODINT, CAD_EAN, CAD_DESC FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_CODINT, CAD_EAN, CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 13, '0') +
          AjustaStrEsq(Dm.Query1.FieldByName('CAD_CODINT').AsString, 10, '0') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    else IF (Dm.qrySQL.FieldByName('INV_LOJA').Value = 'CIC') then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_CODINT, CAD_EAN, CAD_DESC FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_CODINT, CAD_EAN, CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 13, '0') +
          AjustaStrEsq(Dm.Query1.FieldByName('CAD_CODINT').AsString, 8, '0') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'MRS' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 13, '0'));
        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'AND' then
    begin
      Application.ProcessMessages;
      Dm.QDeletaTabela.Close;
      Dm.QDeletaTabela.SQL.Clear;
      Dm.QDeletaTabela.SQL.Add('DELETE FROM PRODUTO');
      Dm.QDeletaTabela.Open;
      Dm.Transacao.CommitRetaining;
      Dm.QDeletaTabela.Close;
      // seta o generator da tabela cadastro para 0
      Dm.QDeletaTabela.Close;
      Dm.QDeletaTabela.SQL.Clear;
      Dm.QDeletaTabela.SQL.Add('SET GENERATOR PRODUTO_PROD_ID_GEN TO 0');
      Dm.QDeletaTabela.Open;
      Dm.Transacao.CommitRetaining;
      Dm.QDeletaTabela.Close;

      // Abri a tabela e o TXT para
      // come�ar a importa��o

      AssignFile(Txt, ('C:\SGD\Cadastro\CADASTRO_PROD.txt')); // NOME do arquivo texto
      Reset(Txt);
      caminho := 'C:\SGD\Cadastro\cadastro_prod.txt';
      contador := 0;
      Lista := TStringList.Create;
      AssignFile(Txt, 'c:\SGD\Cadastro\cadastro_prod.txt');
      Reset(Txt);
      // Verifica o total de linhas no TXT para o progressbar
      totaldelinhas := quantaslinhastxt('C:\SGD\Cadastro\CADASTRO_PROD.txt');

      FrmImportaCad.gauge.MaxValue := totaldelinhas;

      Dm.QImportaDados.Close;
      Dm.QImportaDados.SQL.Clear;
      Dm.QImportaDados.SQL.Add('SET STATISTICS INDEX PK_PRODUTO ');
      Dm.QImportaDados.ExecSQL;
      Application.ProcessMessages;

      Dm.QImportaDados.Close;
      Dm.QImportaDados.SQL.Clear;
      Dm.QImportaDados.SQL.Add('SET STATISTICS INDEX IDX_PRODUTO ');
      Dm.QImportaDados.ExecSQL;
      Application.ProcessMessages;

      Try
        try
          While Not Eoln(Txt) do
          begin

            Dm.QueryImporta.Close;
            Dm.QueryImporta.SQL.Clear;
            Dm.QueryImporta.SQL.Add('INSERT INTO PRODUTO(PROD_EAN, PROD_DESC) ');
            Dm.QueryImporta.SQL.Add('VALUES (:PROD_EAN, :PROD_DESC) ');
            Application.ProcessMessages;

            Lista.Clear;
            Readln(Txt, entrada);

            Lista.Text := RetiraSpace(StringReplace(entrada, ';', #13, [rfReplaceAll, rfIgnoreCase]));

            ExtractStrings([';'], [' '], PChar(entrada), Lista);

            Dm.QueryImporta.ParamByName('PROD_EAN').Value := Lista[0];
            Dm.QueryImporta.ParamByName('PROD_DESC').Value := Lista[1];

            contador := contador + 1;

            Dm.QueryImporta.ExecSQL;
            FrmImportaCad.gauge.Progress := FrmImportaCad.gauge.Progress + 1;
            Application.ProcessMessages;
          end;
        except
          ShowMessage('erro na linha ' + IntToStr(contador));
        end;

      Finally
        CloseFile(Txt);
        Application.ProcessMessages;
        Dm.Transacao.CommitRetaining;
        Dm.QueryImporta.Close;
        Application.MessageBox('Importado arquivo IGT com sucesso!' + #13 + 'Aguarde enquanto gera o arquivo ...',
          'Mensagem do sistema', MB_ICONINFORMATION + MB_OK);
        Screen.Cursor := crDefault;
        FrmImportaCad.gauge.Progress := 0;
      end;
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT PROD_EAN, PROD_DESC FROM PRODUTO ');
      Dm.Query1.SQL.Add('GROUP BY PROD_EAN, PROD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY PROD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrDir(Dm.Query1.FieldByName('PROD_EAN').AsString, 20, ' ') +
          AjustaStrDir(Dm.Query1.FieldByName('PROD_DESC').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'CEA' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 18, '0'));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);

      IF Dm.Query1.Eof then
      begin
        Application.ProcessMessages;
        Dm.QrySql2.Close;
        Dm.QrySql2.SQL.Clear;
        Dm.QrySql2.SQL.Add('SELECT CAD_EAN, CAD_LOTE FROM CADASTRO ');
        Dm.QrySql2.SQL.Add('WHERE CAD_LOTE IS NOT NULL ');
        Dm.QrySql2.SQL.Add('GROUP BY CAD_EAN, CAD_LOTE ');
        Dm.QrySql2.SQL.Add('ORDER BY CAD_EAN ');
        Dm.QrySql2.Open;

        gauge.MinValue := 0;
        gauge.MaxValue := Dm.Query1.RecordCount;

        Dm.QrySql2.First;
        AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\numserie.txt');
        ReWrite(Txt);

        while not Dm.QrySql2.Eof do
        Begin
          Writeln(Txt, AjustaStrEsq(Dm.QrySql2.FieldByName('CAD_EAN').Value, 13, '0') +
            AjustaStrEsq(Dm.QrySql2.FieldByName('CAD_LOTE').Value, 20, '0'));

          Dm.QrySql2.Next;
          Application.ProcessMessages;
          gauge.Progress := gauge.Progress + 1;
        End;

        CloseFile(Txt);

      end;

    end
    else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'PRN' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_CODINT, CAD_EAN, CAD_DESC FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_CODINT, CAD_EAN, CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 13, '0') +
          AjustaStrEsq(Dm.Query1.FieldByName('CAD_CODINT').AsString, 9, '0') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'MRG' then
    begin
      Application.ProcessMessages;
      Dm.QDeletaTabela.Close;
      Dm.QDeletaTabela.SQL.Clear;
      Dm.QDeletaTabela.SQL.Add('DELETE FROM PRODUTO');
      Dm.QDeletaTabela.Open;
      Dm.Transacao.CommitRetaining;
      Dm.QDeletaTabela.Close;
      // seta o generator da tabela cadastro para 0
      Dm.QDeletaTabela.Close;
      Dm.QDeletaTabela.SQL.Clear;
      Dm.QDeletaTabela.SQL.Add('SET GENERATOR PRODUTO_PROD_ID_GEN TO 0');
      Dm.QDeletaTabela.Open;
      Dm.Transacao.CommitRetaining;
      Dm.QDeletaTabela.Close;

      // Abri a tabela e o TXT para
      // come�ar a importa��o

      AssignFile(Txt, ('C:\SGD\Cadastro\CADASTRO_PROD.txt')); // NOME do arquivo texto
      Reset(Txt);
      caminho := 'C:\SGD\Cadastro\cadastro_prod.txt';
      contador := 0;
      Lista := TStringList.Create;
      AssignFile(Txt, 'c:\SGD\Cadastro\cadastro_prod.txt');
      Reset(Txt);
      // Verifica o total de linhas no TXT para o progressbar
      totaldelinhas := quantaslinhastxt('C:\SGD\Cadastro\CADASTRO_PROD.txt');

      FrmImportaCad.gauge.MaxValue := totaldelinhas;

      Dm.QImportaDados.Close;
      Dm.QImportaDados.SQL.Clear;
      Dm.QImportaDados.SQL.Add('SET STATISTICS INDEX PK_PRODUTO ');
      Dm.QImportaDados.ExecSQL;
      Application.ProcessMessages;

      Dm.QImportaDados.Close;
      Dm.QImportaDados.SQL.Clear;
      Dm.QImportaDados.SQL.Add('SET STATISTICS INDEX IDX_PRODUTO ');
      Dm.QImportaDados.ExecSQL;
      Application.ProcessMessages;

      Try
        try
          While Not Eoln(Txt) do
          begin
            Application.ProcessMessages;
            Readln(Txt, entrada);

            Dm.QueryImporta.Close;
            Dm.QueryImporta.SQL.Clear;
            Dm.QueryImporta.SQL.Add('INSERT INTO PRODUTO (PROD_EAN, PROD_CODINT) ');
            Dm.QueryImporta.SQL.Add('VALUES (:PROD_EAN, :PROD_CODINT) ');

            Dm.QueryImporta.ParamByName('PROD_EAN').Value := UpperCase((Copy(entrada, 1, 20)));
            Dm.QueryImporta.ParamByName('PROD_CODINT').Value := UpperCase((Copy(entrada, 1, 20)));

            contador := contador + 1;

            Dm.QueryImporta.ExecSQL;
            FrmImportaCad.gauge.Progress := FrmImportaCad.gauge.Progress + 1;
            Application.ProcessMessages;
          end;
        except
          ShowMessage('erro na linha ' + IntToStr(contador));
        end;

      Finally
        CloseFile(Txt);
        Application.ProcessMessages;
        Dm.Transacao.CommitRetaining;
        Dm.QueryImporta.Close;
        Application.MessageBox('Importado arquivo IGT com sucesso!' + #13 + 'Aguarde enquanto gera o arquivo ...',
          'Mensagem do sistema', MB_ICONINFORMATION + MB_OK);
        Screen.Cursor := crDefault;
        FrmImportaCad.gauge.Progress := 0;
      end;
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT PROD_EAN, PROD_CODINT FROM PRODUTO ');
      Dm.Query1.SQL.Add('GROUP BY PROD_EAN, PROD_CODINT ');
      Dm.Query1.SQL.Add('ORDER BY PROD_EAN, PROD_CODINT ');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrDir(Dm.Query1.FieldByName('PROD_EAN').AsString, 20, ' ') +
          AjustaStrDir(Dm.Query1.FieldByName('PROD_CODINT').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'CLB' then
    begin
      Application.ProcessMessages;
      Dm.QDeletaTabela.Close;
      Dm.QDeletaTabela.SQL.Clear;
      Dm.QDeletaTabela.SQL.Add('DELETE FROM PRODUTO');
      Dm.QDeletaTabela.Open;
      Dm.Transacao.CommitRetaining;
      Dm.QDeletaTabela.Close;
      // seta o generator da tabela cadastro para 0
      Dm.QDeletaTabela.Close;
      Dm.QDeletaTabela.SQL.Clear;
      Dm.QDeletaTabela.SQL.Add('SET GENERATOR PRODUTO_PROD_ID_GEN TO 0');
      Dm.QDeletaTabela.Open;
      Dm.Transacao.CommitRetaining;
      Dm.QDeletaTabela.Close;

      // Abri a tabela e o TXT para
      // come�ar a importa��o

      AssignFile(Txt, ('C:\SGD\Cadastro\CADASTRO_PROD.txt')); // NOME do arquivo texto
      Reset(Txt);
      caminho := 'C:\SGD\Cadastro\cadastro_prod.txt';
      contador := 0;
      Lista := TStringList.Create;
      AssignFile(Txt, 'c:\SGD\Cadastro\cadastro_prod.txt');
      Reset(Txt);
      // Verifica o total de linhas no TXT para o progressbar
      totaldelinhas := quantaslinhastxt('C:\SGD\Cadastro\CADASTRO_PROD.txt');

      FrmImportaCad.gauge.MaxValue := totaldelinhas;

      Dm.QImportaDados.Close;
      Dm.QImportaDados.SQL.Clear;
      Dm.QImportaDados.SQL.Add('SET STATISTICS INDEX PK_PRODUTO ');
      Dm.QImportaDados.ExecSQL;
      Application.ProcessMessages;

      Dm.QImportaDados.Close;
      Dm.QImportaDados.SQL.Clear;
      Dm.QImportaDados.SQL.Add('SET STATISTICS INDEX IDX_PRODUTO ');
      Dm.QImportaDados.ExecSQL;
      Application.ProcessMessages;

      Try
        try
          While Not Eoln(Txt) do
          begin
            Application.ProcessMessages;
            Readln(Txt, entrada);

            Dm.QueryImporta.Close;
            Dm.QueryImporta.SQL.Clear;
            Dm.QueryImporta.SQL.Add('INSERT INTO PRODUTO (PROD_EAN) ');
            Dm.QueryImporta.SQL.Add('VALUES (:PROD_EAN) ');

            Dm.QueryImporta.ParamByName('PROD_EAN').Value := UpperCase((Copy(entrada, 1, 19)));

            contador := contador + 1;

            Dm.QueryImporta.ExecSQL;
            FrmImportaCad.gauge.Progress := FrmImportaCad.gauge.Progress + 1;
            Application.ProcessMessages;
          end;
        except
          ShowMessage('erro na linha ' + IntToStr(contador));
        end;

      Finally
        CloseFile(Txt);
        Application.ProcessMessages;
        Dm.Transacao.CommitRetaining;
        Dm.QueryImporta.Close;
        Application.MessageBox('Importado arquivo IGT com sucesso!' + #13 + 'Aguarde enquanto gera o arquivo ...',
          'Mensagem do sistema', MB_ICONINFORMATION + MB_OK);
        Screen.Cursor := crDefault;
        FrmImportaCad.gauge.Progress := 0;
      end;
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT PROD_EAN FROM PRODUTO ');
      Dm.Query1.SQL.Add('GROUP BY PROD_EAN ');
      Dm.Query1.SQL.Add('ORDER BY PROD_EAN ');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrDir(Dm.Query1.FieldByName('PROD_EAN').AsString, 19, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'CTD' then
    begin
      Application.ProcessMessages;
      Dm.QDeletaTabela.Close;
      Dm.QDeletaTabela.SQL.Clear;
      Dm.QDeletaTabela.SQL.Add('DELETE FROM PRODUTO');
      Dm.QDeletaTabela.Open;
      Dm.Transacao.CommitRetaining;
      Dm.QDeletaTabela.Close;
      // seta o generator da tabela cadastro para 0
      Dm.QDeletaTabela.Close;
      Dm.QDeletaTabela.SQL.Clear;
      Dm.QDeletaTabela.SQL.Add('SET GENERATOR PRODUTO_PROD_ID_GEN TO 0');
      Dm.QDeletaTabela.Open;
      Dm.Transacao.CommitRetaining;
      Dm.QDeletaTabela.Close;

      // Abri a tabela e o TXT para
      // come�ar a importa��o

      AssignFile(Txt, ('C:\SGD\Cadastro\CADASTRO_PROD.txt')); // NOME do arquivo texto
      Reset(Txt);
      caminho := 'C:\SGD\Cadastro\cadastro_prod.txt';
      contador := 0;
      Lista := TStringList.Create;
      AssignFile(Txt, 'c:\SGD\Cadastro\cadastro_prod.txt');
      Reset(Txt);
      // Verifica o total de linhas no TXT para o progressbar
      totaldelinhas := quantaslinhastxt('C:\SGD\Cadastro\CADASTRO_PROD.txt');

      FrmImportaCad.gauge.MaxValue := totaldelinhas;

      Dm.QImportaDados.Close;
      Dm.QImportaDados.SQL.Clear;
      Dm.QImportaDados.SQL.Add('SET STATISTICS INDEX PK_PRODUTO ');
      Dm.QImportaDados.ExecSQL;
      Application.ProcessMessages;

      Dm.QImportaDados.Close;
      Dm.QImportaDados.SQL.Clear;
      Dm.QImportaDados.SQL.Add('SET STATISTICS INDEX IDX_PRODUTO ');
      Dm.QImportaDados.ExecSQL;
      Application.ProcessMessages;

      Try
        try
          While Not Eoln(Txt) do
          begin
            Application.ProcessMessages;
            Readln(Txt, entrada);

            Dm.QueryImporta.Close;
            Dm.QueryImporta.SQL.Clear;
            Dm.QueryImporta.SQL.Add('INSERT INTO PRODUTO (PROD_EAN) ');
            Dm.QueryImporta.SQL.Add('VALUES (:PROD_EAN) ');

            Dm.QueryImporta.ParamByName('PROD_EAN').Value := UpperCase((Copy(entrada, 1, 17)));

            contador := contador + 1;

            Dm.QueryImporta.ExecSQL;
            FrmImportaCad.gauge.Progress := FrmImportaCad.gauge.Progress + 1;
            Application.ProcessMessages;
          end;
        except
          ShowMessage('erro na linha ' + IntToStr(contador));
        end;

      Finally
        CloseFile(Txt);
        Application.ProcessMessages;
        Dm.Transacao.CommitRetaining;
        Dm.QueryImporta.Close;
        Application.MessageBox('Importado arquivo IGT com sucesso!' + #13 + 'Aguarde enquanto gera o arquivo ...',
          'Mensagem do sistema', MB_ICONINFORMATION + MB_OK);
        Screen.Cursor := crDefault;
        FrmImportaCad.gauge.Progress := 0;
      end;
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT PROD_EAN FROM PRODUTO ');
      Dm.Query1.SQL.Add('GROUP BY PROD_EAN ');
      Dm.Query1.SQL.Add('ORDER BY PROD_EAN ');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrDir(Dm.Query1.FieldByName('PROD_EAN').AsString, 17, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'PET' then
    begin
      { Application.ProcessMessages;
        DM.QDeletaTabela.Close;
        DM.QDeletaTabela.SQL.Clear;
        DM.QDeletaTabela.SQL.Add('DELETE FROM PRODUTO');
        DM.QDeletaTabela.Open;
        DM.Transacao.CommitRetaining;
        DM.QDeletaTabela.Close;
        //seta o generator da tabela cadastro para 0
        DM.QDeletaTabela.Close;
        DM.QDeletaTabela.SQL.Clear;
        DM.QDeletaTabela.SQL.Add('SET GENERATOR PRODUTO_PROD_ID_GEN TO 0');
        DM.QDeletaTabela.Open;
        DM.Transacao.CommitRetaining;
        DM.QDeletaTabela.Close;

        //Abri a tabela e o TXT para
        //come�ar a importa��o

        AssignFile(Txt,('C:\SGD\Cadastro\CADASTRO_PROD.csv')); // NOME do arquivo texto
        Reset(Txt);
        caminho := 'C:\SGD\Cadastro\cadastro_prod.csv';
        Contador := 0;
        Lista := TStringList.Create;
        AssignFile(Txt,'c:\SGD\Cadastro\cadastro_prod.csv');
        Reset(Txt);
        //Verifica o total de linhas no TXT para o progressbar
        TotaldeLinhas := quantaslinhastxt('C:\SGD\Cadastro\CADASTRO_PROD.csv');

        FrmImportaCad.gauge.MaxValue := TotaldeLinhas;

        DM.QImportaDados.Close;
        DM.QImportaDados.SQL.Clear;
        DM.QImportaDados.SQL.Add('SET STATISTICS INDEX PK_PRODUTO ');
        DM.QImportaDados.ExecSQL;
        Application.ProcessMessages;

        DM.QImportaDados.Close;
        DM.QImportaDados.SQL.Clear;
        DM.QImportaDados.SQL.Add('SET STATISTICS INDEX IDX_PRODUTO ');
        DM.QImportaDados.ExecSQL;
        Application.ProcessMessages;

        Try
        try
        While Not Eoln(txt) do
        begin

        { Dm.QueryImporta.Close;
        Dm.QueryImporta.SQL.Clear;
        DM.QueryImporta.SQL.Add('INSERT INTO PRODUTO(PROD_EAN, PROD_CODINT, PROD_DESC) ');
        DM.QueryImporta.SQL.Add('VALUES (:PROD_EAN, :PROD_CODINT, :PROD_DESC) ');
        Application.ProcessMessages;

        Lista.Clear;
        Readln(Txt, Entrada);

        Lista.Text := StringReplace(entrada,';',#13, [rfReplaceAll, rfIgnoreCase]);

        if RetiraSpace(Lista[3]) = '0' then
        begin
        DM.QueryImporta.ParamByName('PROD_EAN').Value             := Lista[2];
        end
        else
        if RetiraSpace(Lista[3]) = '00' then
        begin
        DM.QueryImporta.ParamByName('PROD_EAN').Value             := Lista[2];
        end
        else
        if RetiraSpace(Lista[3]) = '000' then
        begin
        DM.QueryImporta.ParamByName('PROD_EAN').Value             := Lista[2];
        end
        else
        if RetiraSpace(Lista[3]) = '0000' then
        begin
        DM.QueryImporta.ParamByName('PROD_EAN').Value             := Lista[2];
        end
        else
        if RetiraSpace(Lista[3]) = '00000' then
        begin
        DM.QueryImporta.ParamByName('PROD_EAN').Value             := Lista[2];
        end
        else
        if RetiraSpace(Lista[3]) = '000000' then
        begin
        DM.QueryImporta.ParamByName('PROD_EAN').Value             := Lista[2];
        end
        else
        if RetiraSpace(Lista[3]) = '0000000' then
        begin
        DM.QueryImporta.ParamByName('PROD_EAN').Value             := Lista[2];
        end
        else
        if RetiraSpace(Lista[3]) = '00000000' then
        begin
        DM.QueryImporta.ParamByName('PROD_EAN').Value             := Lista[2];
        end
        else
        if RetiraSpace(Lista[3]) = '000000000' then
        begin
        DM.QueryImporta.ParamByName('PROD_EAN').Value             := Lista[2];
        end
        else
        if RetiraSpace(Lista[3]) = '0000000000' then
        begin
        DM.QueryImporta.ParamByName('PROD_EAN').Value             := Lista[2];
        end
        else

        if RetiraSpace(Lista[3]) = '00000000000' then
        begin
        DM.QueryImporta.ParamByName('PROD_EAN').Value             := Lista[2];
        end
        else

        if RetiraSpace(Lista[3]) = '000000000000' then
        begin
        DM.QueryImporta.ParamByName('PROD_EAN').Value             := Lista[2];
        end
        else

        if RetiraSpace(Lista[3]) = '0000000000000' then
        begin
        DM.QueryImporta.ParamByName('PROD_EAN').Value             := Lista[2];
        end
        else

        if RetiraSpace(Lista[3]) = '00000000000000' then
        begin
        DM.QueryImporta.ParamByName('PROD_EAN').Value             := Lista[2];
        end
        else

        if RetiraSpace(Lista[3]) = '000000000000000' then
        begin
        DM.QueryImporta.ParamByName('PROD_EAN').Value             := Lista[2];
        end
        else
        if RetiraSpace(Lista[3]) = '' then
        begin
        DM.QueryImporta.ParamByName('PROD_EAN').Value             := Lista[2];
        end
        else
        DM.QueryImporta.ParamByName('PROD_EAN').Value             := Lista[3];
        DM.QueryImporta.ParamByName('PROD_CODINT').Value          := Lista[2];
        DM.QueryImporta.ParamByName('PROD_DESC').Value            := Lista[4];


        contador := Contador +1;

        DM.QueryImporta.ExecSQL;
        FrmImportaCad.gauge.progress := FrmImportaCad.gauge.progress +1;
        Application.ProcessMessages;
        end;
        except
        ShowMessage('erro na linha ' +IntToStr(contador));
        end;

        Finally
        CloseFile(TXT);
        Application.ProcessMessages;
        Dm.Transacao.CommitRetaining;
        DM.QueryImporta.Close;
        Application.MessageBox('Importado arquivo IGT com sucesso!'+#13+'Aguarde enquanto gera o arquivo ...','Mensagem do sistema', MB_ICONINFORMATION+ MB_OK);
        Screen.Cursor := crDefault;
        FrmImportaCad.Gauge.Progress:=0;
        end;
        Application.ProcessMessages; }
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN, CAD_CODINT, CAD_DESC FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN, CAD_CODINT, CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN  ');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrDir(Dm.Query1.FieldByName('CAD_EAN').AsString, 15, ' ') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_CODINT').AsString, 15, ' ') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    else IF ((Dm.qrySQL.FieldByName('INV_LOJA').Value = 'SMP') or (Dm.qrySQL.FieldByName('INV_LOJA').Value = 'SMK') or
      (Dm.qrySQL.FieldByName('INV_LOJA').Value = 'SMD')) then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT PROD_EAN, PROD_DESC FROM PRODUTO ');
      Dm.Query1.SQL.Add('GROUP BY PROD_EAN, PROD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY PROD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrDir(Dm.Query1.FieldByName('PROD_EAN').AsString, 25, ' ') +
          AjustaStrDir(Dm.Query1.FieldByName('PROD_DESC').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'PDA' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN, CAD_CODINT FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN, CAD_CODINT ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 13, '0') +
          AjustaStrEsq(Dm.Query1.FieldByName('CAD_CODINT').AsString, 6, '0'));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'LOP' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN, CAD_CODINT, CAD_DESC FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN, CAD_CODINT, CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 18, '0') +
          AjustaStrEsq(Dm.Query1.FieldByName('CAD_CODINT').AsString, 12, '0') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'VLG' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN, CAD_CODINT, CAD_DESC FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN, CAD_CODINT, CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 13, '0') +
          AjustaStrEsq(Dm.Query1.FieldByName('CAD_CODINT').AsString, 14, '0') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'DLG' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN, CAD_CODINT, CAD_DESC FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN, CAD_CODINT, CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 13, '0') +
          AjustaStrEsq(Dm.Query1.FieldByName('CAD_CODINT').AsString, 13, '0') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'BAR' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN, ');
      Dm.Query1.SQL.Add('CAD_CODINT, ');
      Dm.Query1.SQL.Add('CAD_DESC ');
      Dm.Query1.SQL.Add('FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN, ');
      Dm.Query1.SQL.Add('CAD_CODINT, ');
      Dm.Query1.SQL.Add('CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY 1 ');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrDir(Dm.Query1.FieldByName('CAD_EAN').AsString, 15, ' ') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_CODINT').AsString, 6, ' ') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'MAX' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN, ');
      Dm.Query1.SQL.Add('CAD_CODINT, ');
      Dm.Query1.SQL.Add('CAD_DESC ');
      Dm.Query1.SQL.Add('FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN, ');
      Dm.Query1.SQL.Add('CAD_CODINT, ');
      Dm.Query1.SQL.Add('CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY 1 ');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 13, '0') +
          AjustaStrEsq(Dm.Query1.FieldByName('CAD_CODINT').AsString, 6, '0') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    Else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'SEB' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN, ');
      Dm.Query1.SQL.Add('CAD_DESC ');
      Dm.Query1.SQL.Add('FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN, ');
      Dm.Query1.SQL.Add('CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY 1 ');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrDir(Dm.Query1.FieldByName('CAD_EAN').AsString, 13, ' ') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end

    Else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'SED' then
    begin
      Application.ProcessMessages;
      EAN_ANTERIOR := '';
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN, CAD_CODINT, CAD_DESC FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN, CAD_CODINT, CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN, CAD_CODINT');
      Dm.Query1.Open;

      Dm.Query1.First;
      Dm.Query1.Last;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        IF EAN_ANTERIOR <> Dm.Query1.FieldByName('CAD_EAN').Value THEN
        Begin
          Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 13, '0') +
            AjustaStrEsq(Dm.Query1.FieldByName('CAD_CODINT').AsString, 6, '0') +
            AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').Value, 20, ' '));
        End;
        EAN_ANTERIOR := Dm.Query1.FieldByName('CAD_EAN').Value;
        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end

    Else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'ROS' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN, CAD_CODINT, CAD_DESC, CAD_GRUPOCOM FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN, CAD_CODINT, CAD_DESC, CAD_GRUPOCOM ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN, CAD_CODINT');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, Dm.Query1.FieldByName('CAD_EAN').AsString + Dm.Query1.FieldByName('CAD_CODINT').AsString +
          Copy(Dm.Query1.FieldByName('CAD_DESC').AsString, 1, 20));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    Else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'BGB' then
    begin
      Dm.qrPopulaCadastroTmp.Close;
      Dm.qrPopulaCadastroTmp.ExecSQL;

      Dm.qrCadastroAuxiliar.Close;
      Dm.qrCadastroAuxiliar.Open;

      cad_id_ini := Dm.qrCadastroAuxiliar.FieldByName('MMIN').AsInteger;
      cad_id_fim := Dm.qrCadastroAuxiliar.FieldByName('MMAX').AsInteger;
      cad_id_ult := cad_id_ini;

      // Usando passos de 10000   ]
      gauge.MinValue := 0;
      gauge.MaxValue := cad_id_fim;

      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\produto.txt');
      ReWrite(Txt);

      while cad_id_ult <= cad_id_fim do
      begin

        EAN_ANTERIOR := '';
        Dm.qrCadastroAuxiliar2.Close;
        Dm.qrCadastroAuxiliar2.ParamByName('CAD_ID_INI').Value := cad_id_ult;
        Dm.qrCadastroAuxiliar2.ParamByName('CAD_ID_FIM').Value := cad_id_ult + 10000;
        Dm.qrCadastroAuxiliar2.Open;

        // Ajusta ultimo CAD_ID processado
        cad_id_ult := cad_id_ult + 10001;

        Dm.qrCadastroAuxiliar2.Last;
        Dm.qrCadastroAuxiliar2.First;

        while not Dm.qrCadastroAuxiliar2.Eof do
        Begin
          IF EAN_ANTERIOR <> Dm.qrCadastroAuxiliar2CAD_EAN.Value THEN
          Begin
            Writeln(Txt, AjustaStrEsq(Dm.qrCadastroAuxiliar2CAD_EAN.AsString, 14, '0') +
              Dm.qrCadastroAuxiliar2CAD_CODINT.AsString);
          End;
          EAN_ANTERIOR := Dm.qrCadastroAuxiliar2CAD_EAN.Value;
          Dm.qrCadastroAuxiliar2.Next;
          Application.ProcessMessages;
          gauge.Progress := gauge.Progress + 1;
        End;

      end;

      CloseFile(Txt);

      Dm.Transacao.CommitRetaining;

      AdicionaLog(DateTimeToStr(Now) + '> Gera��o do arquivo IGT PRODUTO para coletor realizada com sucesso! ');

    end
    Else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'GLM' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_CODINT, ');
      Dm.Query1.SQL.Add('CAD_DESC ');
      Dm.Query1.SQL.Add('FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_CODINT, ');
      Dm.Query1.SQL.Add('CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY CAD_CODINT ');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrDir(Dm.Query1.FieldByName('CAD_CODINT').AsString, 13, ' '),
          AjustaStrDir(Dm.Query1.FieldByName('CAD_CODINT').AsString, 4, ' ') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    Else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'CLS' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN, CAD_CODINT, ');
      Dm.Query1.SQL.Add('CAD_DESC ');
      Dm.Query1.SQL.Add('FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN,CAD_CODINT, ');
      Dm.Query1.SQL.Add('CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN ');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 13, '0') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_CODINT').AsString, 23, ' ') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, ' '));
        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    else IF (Dm.qrySQL.FieldByName('INV_LOJA').Value = 'PLT') then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_CODINT, CAD_EAN, CAD_DESC FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_CODINT, CAD_EAN, CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 13, '0') +
          AjustaStrEsq(Dm.Query1.FieldByName('CAD_CODINT').AsString, 12, '0') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    else IF (Dm.qrySQL.FieldByName('INV_LOJA').Value = 'NCL') then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_CODINT, CAD_EAN, CAD_DESC FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_CODINT, CAD_EAN, CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        // Writeln(Txt,AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString,13,'0')+AjustaStrEsq(Dm.Query1.FieldByName('CAD_CODINT').AsString,12,'0')+AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString,20,' '));
        Writeln(Txt, AjustaStrDir(Dm.Query1.FieldByName('CAD_EAN').AsString, 14, ' ') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_CODINT').AsString, 6, ' ') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    else IF (Dm.qrySQL.FieldByName('INV_LOJA').Value = 'LOC') then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN, CAD_CODINT, CAD_DESC FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN, CAD_CODINT, CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        // Writeln(Txt,AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString,13,'0')+AjustaStrEsq(Dm.Query1.FieldByName('CAD_CODINT').AsString,12,'0')+AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString,20,' '));
        Writeln(Txt, AjustaStrDir(Dm.Query1.FieldByName('CAD_EAN').AsString, 13, ' ') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_CODINT').AsString, 18, ' ') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    else if (Dm.qrySQL.FieldByName('INV_LOJA').Value = 'NTR') then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_CODINT, CAD_DESC FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_CODINT, CAD_EAN, CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      begin
        Writeln(Txt, AjustaStrDir(Dm.Query1.FieldByName('CAD_CODINT').AsString, 14, ' ') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      end;

      CloseFile(Txt);
    end
    Else if (Dm.qrySQL.FieldByName('INV_LOJA').Value = 'EDR') then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_CODINT, CAD_EAN, CAD_DESC FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN, CAD_CODINT, CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN, CAD_CODINT, CAD_DESC');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      begin
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 13, '0') +
          AjustaStrEsq(Dm.Query1.FieldByName('CAD_CODINT').AsString, 5, '0') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      end;

      CloseFile(Txt);
    end
    else IF (Dm.qrySQL.FieldByName('INV_LOJA').Value = 'DFY') then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_CODINT, CAD_EAN, CAD_DESC FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN, CAD_CODINT, CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 14, '0') +
          AjustaStrEsq(Dm.Query1.FieldByName('CAD_CODINT').AsString, 9, '0') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'BVL' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN, ');
      Dm.Query1.SQL.Add('CAD_DESC ');
      Dm.Query1.SQL.Add('FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN, ');
      Dm.Query1.SQL.Add('CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY 1 ');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrDir(Dm.Query1.FieldByName('CAD_EAN').AsString, 12, ' ') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    Else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'CND' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN, CAD_DESC FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN, CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 18, '0') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    Else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'SLL' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN FROM CADASTRO ');
      // DM.Query1.SQL.Add('GROUP BY CAD_EAN ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 14, '0'));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    Else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'SMN' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN, CAD_CODINT, CAD_DESC FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN, CAD_CODINT, CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrDir(Dm.Query1.FieldByName('CAD_EAN').AsString, 15, ' '),
          AjustaStrEsq(Dm.Query1.FieldByName('CAD_CODINT').AsString, 6, '0') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    Else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'MBZ' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN, CAD_DESC FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN, CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrDir(Dm.Query1.FieldByName('CAD_EAN').AsString, 7, ' ') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    Else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'SAR' then
    begin
      Application.ProcessMessages;
      EAN_ANTERIOR := '';
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN, CAD_CODINT, CAD_DESC FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN, CAD_CODINT, CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN, CAD_CODINT');
      Dm.Query1.Open;

      Dm.Query1.First;
      Dm.Query1.Last;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        IF EAN_ANTERIOR <> Dm.Query1.FieldByName('CAD_EAN').Value THEN
        Begin
          Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 15, '0') +
            AjustaStrEsq(Dm.Query1.FieldByName('CAD_CODINT').AsString, 6, '0') +
            AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').Value, 20, ' '));
        End;
        EAN_ANTERIOR := Dm.Query1.FieldByName('CAD_EAN').Value;
        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    Else IF (Dm.qrySQL.FieldByName('INV_LOJA').Value = 'DVL') then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_CODINT, CAD_EAN, CAD_DESC FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_CODINT, CAD_EAN, CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        // Writeln(Txt,AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString,13,'0')+AjustaStrEsq(Dm.Query1.FieldByName('CAD_CODINT').AsString,12,'0')+AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString,20,' '));
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 13, '0') +
          AjustaStrEsq(Dm.Query1.FieldByName('CAD_CODINT').AsString, 8, '0') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    else IF (Dm.qrySQL.FieldByName('INV_LOJA').Value = 'LUB') or (Dm.qrySQL.FieldByName('INV_LOJA').Value = 'EME') then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN, ');
      Dm.Query1.SQL.Add('CAD_DESC ');
      Dm.Query1.SQL.Add('FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN, ');
      Dm.Query1.SQL.Add('CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY 1 ');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrDir(Dm.Query1.FieldByName('CAD_EAN').AsString, 15, ' ') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    else IF (Dm.qrySQL.FieldByName('INV_LOJA').Value = 'LLB') or (Dm.qrySQL.FieldByName('INV_LOJA').Value = 'BBO') or
      (Dm.qrySQL.FieldByName('INV_LOJA').Value = 'JHJ') or (Dm.qrySQL.FieldByName('INV_LOJA').Value = 'NOR') or
      (Dm.qrySQL.FieldByName('INV_LOJA').Value = 'RSC') then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN, ');
      Dm.Query1.SQL.Add('CAD_DESC ');
      Dm.Query1.SQL.Add('FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN, ');
      Dm.Query1.SQL.Add('CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY 1 ');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrDir(Dm.Query1.FieldByName('CAD_EAN').AsString, 16, ' ') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    Else IF (Dm.qrySQL.FieldByName('INV_LOJA').Value = 'MKR') then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_CODINT, CAD_EAN, CAD_DESC FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_CODINT, CAD_EAN, CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        // Writeln(Txt,AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString,13,'0')+AjustaStrEsq(Dm.Query1.FieldByName('CAD_CODINT').AsString,12,'0')+AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString,20,' '));
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 14, '0') +
          AjustaStrEsq(Dm.Query1.FieldByName('CAD_CODINT').AsString, 6, '0') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    Else IF (Dm.qrySQL.FieldByName('INV_LOJA').Value = 'LVL') then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN, CAD_DESC FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN, CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 6, '0') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    Else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'BHC' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN, CAD_CODINT, CAD_DESC FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN, CAD_CODINT, CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        IF EAN_ANTERIOR <> Dm.Query1.FieldByName('CAD_EAN').Value THEN
        Begin
          Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 14, '0') +
            AjustaStrEsq(Dm.Query1.FieldByName('CAD_CODINT').AsString, 7, '0') +
            AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, ' '));
        End;
        EAN_ANTERIOR := Dm.Query1.FieldByName('CAD_EAN').Value;

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    Else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'PMT' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN, CAD_CODINT, CAD_DESC FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN, CAD_CODINT, CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrDir(Dm.Query1.FieldByName('CAD_EAN').AsString, 13, ' ') +
          AjustaStrEsq(Dm.Query1.FieldByName('CAD_CODINT').AsString, 10, '0') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    Else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'GDS' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN, CAD_NUMINV, CAD_DESC FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN, CAD_NUMINV, CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrDir(Dm.Query1.FieldByName('CAD_EAN').AsString, 16, ' ') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_NUMINV').AsString, 17, ' ') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;
      CloseFile(Txt);

      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT PROD_CODINT FROM PRODUTO ');
      Dm.Query1.SQL.Add('GROUP BY PROD_CODINT ');
      Dm.Query1.SQL.Add('ORDER BY PROD_CODINT');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\local3.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        IF endereco <> Dm.Query1.FieldByName('PROD_CODINT').Value THEN
          Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('PROD_CODINT').AsString, 5, '0'));
        endereco := Dm.Query1.FieldByName('PROD_CODINT').Value;

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;
      CloseFile(Txt);

      { Lista := TStringList.Create;
        Lista.LoadFromFile('C:\SGD\IGT_CE\IgtCeSinc\produto.txt');

        for I := 0 to Lista.Count - 1 do
        begin
        MMOrdena.Lines.Add(Lista.Strings[i]);
        end;

        Ordenar_Produto(MMOrdena.Lines);
        Lista.Clear;
        for i := 0 to MMOrdena.Lines.Count - 1 do
        begin
        Lista.Add(MMOrdena.Lines.Strings[i])
        end; }
    end
    Else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'PSL' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN, CAD_CODINT, CAD_DESC FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN, CAD_CODINT, CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 13, '0') +
          AjustaStrEsq(Dm.Query1.FieldByName('CAD_CODINT').AsString, 13, '0'));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    Else IF (Dm.qrySQL.FieldByName('INV_LOJA').Value = 'SET') or (Dm.qrySQL.FieldByName('INV_LOJA').Value = 'SEP') then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN, CAD_CODINT, CAD_DESC FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN, CAD_CODINT, CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 13, '0') +
          AjustaStrEsq(Dm.Query1.FieldByName('CAD_CODINT').AsString, 6, '0') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    Else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'RCH' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN, CAD_CODINT FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN, CAD_CODINT ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrDir(Dm.Query1.FieldByName('CAD_EAN').AsString, 12, ' ') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_CODINT').AsString, 12, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    Else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'MCP' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN, CAD_CODINT FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN, CAD_CODINT ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrDir(Dm.Query1.FieldByName('CAD_EAN').AsString, 11, ' ') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_CODINT').AsString, 11, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    Else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'MCF' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN, CAD_CODINT FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN, CAD_CODINT ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrDir(Dm.Query1.FieldByName('CAD_EAN').AsString, 14, ' ') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_CODINT').AsString, 14, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    Else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'FMS' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN, CAD_CODINT FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN, CAD_CODINT ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 13, '0') +
          AjustaStrEsq(Dm.Query1.FieldByName('CAD_CODINT').AsString, 10, '0'));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    Else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'TNG' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrDir(Dm.Query1.FieldByName('CAD_EAN').AsString, 17, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    Else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'ANC' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN, CAD_CODINT, CAD_DESC FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN, CAD_CODINT, CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 14, '0') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_CODINT').AsString, 11, ' ') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, ' '));
        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    Else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'PNB' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN, CAD_DESC FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN, CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 13, '0')); //+
//          AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    Else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'CPS' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN, CAD_DESC FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN, CAD_DESC ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Produto.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 9, '0') +
          AjustaStrDir(Dm.Query1.FieldByName('CAD_DESC').AsString, 20, ' '));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    Else
    begin
      // Popular cadastro temporario j� ordenado por EAN+INTERNO

      Dm.qrPopulaCadastroTmp.Close;
      Dm.qrPopulaCadastroTmp.ExecSQL;

      Dm.qrCadastroAuxiliar.Close;
      Dm.qrCadastroAuxiliar.Open;

      cad_id_ini := Dm.qrCadastroAuxiliar.FieldByName('MMIN').AsInteger;
      cad_id_fim := Dm.qrCadastroAuxiliar.FieldByName('MMAX').AsInteger;
      cad_id_ult := cad_id_ini;

      // Usando passos de 10000   ]
      gauge.MinValue := 0;
      gauge.MaxValue := cad_id_fim;

      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\produto.txt');
      ReWrite(Txt);

      while cad_id_ult <= cad_id_fim do
      begin

        EAN_ANTERIOR := '';
        Dm.qrCadastroAuxiliar2.Close;
        Dm.qrCadastroAuxiliar2.ParamByName('CAD_ID_INI').Value := cad_id_ult;
        Dm.qrCadastroAuxiliar2.ParamByName('CAD_ID_FIM').Value := cad_id_ult + 10000;
        Dm.qrCadastroAuxiliar2.Open;

        // Ajusta ultimo CAD_ID processado
        cad_id_ult := cad_id_ult + 10001;

        Dm.qrCadastroAuxiliar2.Last;
        Dm.qrCadastroAuxiliar2.First;

        while not Dm.qrCadastroAuxiliar2.Eof do
        Begin
          IF EAN_ANTERIOR <> Dm.qrCadastroAuxiliar2CAD_EAN.Value THEN
          Begin
            Writeln(Txt, AjustaStrDir(Dm.qrCadastroAuxiliar2CAD_EAN.AsString, SizeEAN, ' ') +
              AjustaStrDir(Dm.qrCadastroAuxiliar2CAD_CODINT.AsString, SizeCOD_INT, ' ') +
              AjustaStrDir(Dm.qrCadastroAuxiliar2CAD_DESC.Value, SizeDESCRICAO, ' '));
          End;
          EAN_ANTERIOR := Dm.qrCadastroAuxiliar2CAD_EAN.Value;
          Dm.qrCadastroAuxiliar2.Next;
          Application.ProcessMessages;
          gauge.Progress := gauge.Progress + 1;
        End;

      end;

      CloseFile(Txt);

      Dm.Transacao.CommitRetaining;

      AdicionaLog(DateTimeToStr(Now) + '> Gera��o do arquivo IGT PRODUTO para coletor realizada com sucesso! ');

    end;

    IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'DCS' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN, CAD_PRECOTXT FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN,CAD_PRECOTXT ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Precouni.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 14, '0') +
          AjustaStrEsq(Dm.Query1.FieldByName('CAD_PRECOTXT').AsString, 9, '0'));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    Else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'ANG' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN, CAD_PRECOTXT FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN,CAD_PRECOTXT ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Precouni.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 14, '0') +
          AjustaStrEsq(Dm.Query1.FieldByName('CAD_PRECOTXT').AsString, 9, '0'));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    Else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'BEL' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN, CAD_PRECOTXT FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN,CAD_PRECOTXT ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Precouni.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 13, '0') +
          AjustaStrEsq(Dm.Query1.FieldByName('CAD_PRECOTXT').AsString, 9, '0'));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end
    else IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'PDA' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN, CAD_PRECOTXT FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN, CAD_PRECOTXT ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Precouni.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 20, '0') +
          AjustaStrEsq(Dm.Query1.FieldByName('CAD_PRECOTXT').AsString, 9, '0'));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end;

    IF Dm.qrySQL.FieldByName('INV_LOJA').Value = 'ASA' then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN, CAD_PRECOTXT FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN,CAD_PRECOTXT ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Precouni.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 13, '0') +
          AjustaStrEsq(Dm.Query1.FieldByName('CAD_PRECOTXT').AsString, 9, '0'));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end;

    IF not((Dm.qrySQL.FieldByName('INV_LOJA').Value = 'ASA') or (Dm.qrySQL.FieldByName('INV_LOJA').Value = 'PDA') or
      (Dm.qrySQL.FieldByName('INV_LOJA').Value = 'DCS') or (Dm.qrySQL.FieldByName('INV_LOJA').Value = 'ANG') or
      (Dm.qrySQL.FieldByName('INV_LOJA').Value = 'BEL')) then
    begin
      Application.ProcessMessages;
      Dm.Query1.Close;
      Dm.Query1.SQL.Clear;
      Dm.Query1.SQL.Add('SELECT CAD_EAN, CAD_PRECOTXT FROM CADASTRO ');
      Dm.Query1.SQL.Add('GROUP BY CAD_EAN,CAD_PRECOTXT ');
      Dm.Query1.SQL.Add('ORDER BY CAD_EAN');
      Dm.Query1.Open;

      gauge.MinValue := 0;
      gauge.MaxValue := Dm.Query1.RecordCount;

      Dm.Query1.First;
      AssignFile(Txt, 'C:\SGD\IGT_CE\IgtCeSinc\Precouni.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
        Writeln(Txt, AjustaStrEsq(Dm.Query1.FieldByName('CAD_EAN').AsString, 13, '0') +
          AjustaStrEsq(Dm.Query1.FieldByName('CAD_PRECOTXT').AsString, 9, '0'));

        Dm.Query1.Next;
        Application.ProcessMessages;
        gauge.Progress := gauge.Progress + 1;
      End;

      CloseFile(Txt);
    end;
    AdicionaLog(DateTimeToStr(Now) + '> Gera��o do arquivo IGT PRECOUNI para coletor realizada com sucesso! ');

    { IF DM.qrySQL.FieldByName('INV_LOJA').Value = 'ONP' then
      begin
      Application.ProcessMessages;
      DM.QDeletaTabela.Close;
      DM.QDeletaTabela.SQL.Clear;
      DM.QDeletaTabela.SQL.Add('DELETE FROM LOTE');
      DM.QDeletaTabela.Open;
      DM.Transacao.CommitRetaining;
      DM.QDeletaTabela.Close;
      //seta o generator da tabela cadastro para 0
      DM.QDeletaTabela.Close;
      DM.QDeletaTabela.SQL.Clear;
      DM.QDeletaTabela.SQL.Add('SET GENERATOR LOT_ID TO 0');
      DM.QDeletaTabela.Open;
      DM.Transacao.CommitRetaining;
      DM.QDeletaTabela.Close;

      //Abri a tabela e o TXT para
      //come�ar a importa��o

      AssignFile(Txt,('C:\SGD\Cadastro\CADASTRO_PROD.txt')); // NOME do arquivo texto
      Reset(Txt);
      caminho := 'C:\SGD\Cadastro\cadastro_prod.txt';
      Contador := 0;
      Lista := TStringList.Create;
      AssignFile(Txt,'c:\SGD\Cadastro\cadastro_prod.txt');
      Reset(Txt);
      //Verifica o total de linhas no TXT para o progressbar
      TotaldeLinhas := quantaslinhastxt('C:\SGD\Cadastro\CADASTRO_PROD.txt');

      FrmImportaCad.gauge.MaxValue := TotaldeLinhas;

      DM.QImportaDados.Close;
      DM.QImportaDados.SQL.Clear;
      DM.QImportaDados.SQL.Add('SET STATISTICS INDEX RDB$PRIMARY1 ');
      DM.QImportaDados.ExecSQL;
      Application.ProcessMessages;

      DM.QImportaDados.Close;
      DM.QImportaDados.SQL.Clear;
      DM.QImportaDados.SQL.Add('SET STATISTICS INDEX IDX_LOTE ');
      DM.QImportaDados.ExecSQL;
      Application.ProcessMessages;

      Try
      try
      While Not Eoln(txt) do
      begin
      Application.ProcessMessages;
      Readln(Txt, Entrada);

      Dm.QueryImporta.Close;
      Dm.QueryImporta.SQL.Clear;
      DM.QueryImporta.SQL.Add('INSERT INTO LOTE (LOT_LOTE) ');
      DM.QueryImporta.SQL.Add('VALUES (:LOT_LOTE) ');

      DM.QueryImporta.ParamByName('LOT_LOTE').Value             := Copy(Entrada,88,10);

      contador := Contador +1;

      DM.QueryImporta.ExecSQL;
      FrmImportaCad.gauge.progress := FrmImportaCad.gauge.progress +1;
      Application.ProcessMessages;
      end;
      except
      ShowMessage('erro na linha ' +IntToStr(contador));
      end;

      Finally
      CloseFile(TXT);
      Application.ProcessMessages;
      Dm.Transacao.CommitRetaining;
      DM.QueryImporta.Close;
      Application.MessageBox('Importado arquivo IGT com sucesso!'+#13+'Aguarde enquanto gera o arquivo ...','Mensagem do sistema', MB_ICONINFORMATION+ MB_OK);
      Screen.Cursor := crDefault;
      FrmImportaCad.Gauge.Progress:=0;
      end;
      Application.ProcessMessages;
      DM.Query1.Close;
      DM.Query1.SQL.Clear;
      DM.Query1.SQL.Add('SELECT LOT_LOTE FROM LOTE ');
      DM.Query1.SQL.Add('WHERE LOT_LOTE NOT IN ("","0","00","000","0000","00000","000000","0000000","00000000","000000000","0000000000")');
      DM.Query1.SQL.Add('GROUP BY LOT_LOTE ');
      DM.Query1.SQL.Add('ORDER BY LOT_LOTE ');
      DM.Query1.Open;

      Gauge.MinValue:=0;
      Gauge.MaxValue:=  DM.Query1.RecordCount;

      DM.Query1.First;
      AssignFile(Txt,'C:\SGD\IGT_CE\IgtCeSinc\Local3.txt');
      ReWrite(Txt);

      while not Dm.Query1.Eof do
      Begin
      Writeln(Txt,AjustaStrDir(Dm.Query1.FieldByName('LOT_LOTE').AsString,25,' '));

      Dm.Query1.Next;
      Application.ProcessMessages;
      Gauge.Progress := Gauge.Progress +1;
      End;

      CloseFile(Txt);

      end; }

    { If Dm.IBLayoutInputENDERECO.Value = 'T' then
      begin
      Application.ProcessMessages;
      DM.qrySQL.Close;
      DM.qrySQL.SQL.Clear;
      DM.qrySQL.SQL.Add('Select MAP_SECINI from mapeamento order by MAP_SECINI');
      DM.qrySQL.Open;

      Gauge.MinValue:=0;
      Gauge.MaxValue:=  DM.qrySQL.RecordCount;

      DM.qrySQL.First;
      AssignFile(Txt,'C:\SGD\IGT_CE\IgtCeSinc\local1.txt');
      ReWrite(Txt);

      while not DM.qrySQL.Eof do
      Begin
      IF Dm.IBLayoutInputEND_ZERO.AsString = 'T' THEN
      Writeln(Txt,AjustaStrEsq(DM.qrySQL.FieldByName('MAP_SECINI').AsString, 25,'0'))
      else
      Writeln(Txt,AjustaStrEsq(DM.qrySQL.FieldByName('MAP_SECINI').AsString,25,' '));

      DM.qrySQL.Next;
      Application.ProcessMessages;
      Gauge.Progress := Gauge.Progress +1;
      End;

      CloseFile(Txt);
      end; }

    { If Dm.IBLayoutInputLOTE.Value = 'T' then
      begin
      Application.ProcessMessages;
      DM.qrySQL.Close;
      DM.qrySQL.SQL.Clear;
      DM.qrySQL.SQL.Add('Select LOT_LOTE from LOTE order by LOT_LOTE');
      DM.qrySQL.Open;

      Gauge.MinValue:=0;
      Gauge.MaxValue:=  DM.qrySQL.RecordCount;

      DM.qrySQL.First;
      AssignFile(Txt,'C:\SGD\IGT_CE\IgtCeSinc\local2.txt');
      ReWrite(Txt);

      while not DM.qrySQL.Eof do
      Begin
      IF Dm.IBLayoutInputLOTE_ZERO.AsString = 'T' THEN
      Writeln(Txt,AjustaStrEsq(DM.qrySQL.FieldByName('LOT_LOTE').AsString, 25,'0'))
      else
      Writeln(Txt,AjustaStrEsq(DM.qrySQL.FieldByName('LOT_LOTE').AsString,25,' '));

      DM.qrySQL.Next;
      Application.ProcessMessages;
      Gauge.Progress := Gauge.Progress +1;
      End;

      CloseFile(Txt);
      end; }

  Finally
    gauge.Progress := 0;
    Dm.CDSImportaCad.Close;
    Application.MessageBox
      ('Cadastro Gerado para o coletor. Na pasta "C:\SGD\IGT_CE\IgtCeSinc". Os coletores j� podem ser carregados!',
      'Mensagem do Sistema', MB_ICONINFORMATION + MB_OK);
    Screen.Cursor := crDefault;
  end;
end;

procedure TFrmImportaCad.Button1Click(Sender: TObject);
begin
  // ImportaEndereco;
  // Dm2.IBEndereco.Close;
  gauge.Progress := 1;
end;

procedure TFrmImportaCad.BitBtn3Click(Sender: TObject);
begin
  validaEndereco;
end;

procedure TFrmImportaCad.BitBtn4Click(Sender: TObject);
begin
  ImportaEnd;
  Dm.Conexao.Connected := False;
  Dm.Conexao.Connected := True;
end;

procedure TFrmImportaCad.BitBtn5Click(Sender: TObject);
begin
  ExcluiEnd;
end;

procedure TFrmImportaCad.BitBtnMeiaClick(Sender: TObject);
begin
  if Dm.IBInventarioINV_LOJA.Value = 'GDS' then
    ImportaCadSaldoGDS
  Else
    ImportaCadProdMEIA;

end;

procedure TFrmImportaCad.bbtesteClick(Sender: TObject);
begin
  ShowMessage(FloatToStr(StrToFloat('1111111111111111111111111')));
end;

end.
