unit UDivergenciaQtd;

interface

uses
    Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
    Dialogs, StdCtrls, Buttons, ComCtrls, ExtCtrls, sBitBtn, RpDefine, RpRender,
    RpRenderPDF, RpRenderText, DBCtrls, sLabel;

type
  TFrmDivergenciaQtd = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    StatusBar1: TStatusBar;
    Panel3: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Lbl_ValorFinal: TLabel;
    Edit1: TEdit;
    sBitBtn1: TsBitBtn;
    RadioGroup1: TRadioGroup;
    CmbSetor: TComboBox;
    CheckBoxDiv: TCheckBox;
    CmbSetorFim: TComboBox;
    Edt_ValorFinal: TEdit;
    RvPDF: TRvRenderPDF;
    sLabelFX1: TsLabelFX;
    procedure sBitBtn1Click(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure CmbSetorFimClick(Sender: TObject);
    procedure CmbSetorClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmDivergenciaQtd: TFrmDivergenciaQtd;

implementation

uses Udm2, UDm;

{$R *.dfm}

procedure TFrmDivergenciaQtd.sBitBtn1Click(Sender: TObject);
begin
  Try
    Dm2.QDivZerados.Close;
    Dm2.QDivZerados.SQL.Clear;
    Dm2.QDivZerados.SQL.Add('SELECT CO.QTD, CA.CAD_CODINT,CA.CAD_UN, CA.CAD_DESC, CA.CAD_QUANTIDADE, CA.CAD_SETOR,(CA.CAD_PRECO), CA.CAD_EAN ');
    Dm2.QDivZerados.SQL.Add('from CONTAGENS_DIV CO RIGHT join CADASTRO_CONSO CA on (CA.CAD_CODINT = CO.CAD_CODINT) ');
    Dm2.QDivZerados.SQL.Add('where (CA.CAD_QUANTIDADE >= :CAD_QUANTIDADE) and (ca.cad_Setor >= :cad_Setor) and  (ca.cad_setor <= :cad_setorFim) and (CO.QTD IS NULL) and (ca.cad_quantidade > 0) ');
    Dm2.QDivZerados.SQL.Add('ORDER BY CA.CAD_QUANTIDADE DESC');


    If Dm.IBInventarioINV_LOJA.Value = 'LOP' then
      begin
          Dm2.QDivDesc1.Close;
          Dm2.QDivDesc2.Close;
          Dm2.QDivDesc1.ParamByName('DIF_QTD').Value := Edit1.Text;
          Dm2.QDivDesc1.Open;
          Dm2.QDivDesc2.Open;
          Dm.RvPrjWis.Close;
          Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\DivergenciaDesc2.rav';
          Dm.RvPrjWis.Open;
          Dm.RvSysWis.DefaultDest := rdPreview;
          Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
          Dm.RvPrjWis.ExecuteReport('DivergenciaDesc2')
      end
    else
       If RadioGroup1.ItemIndex = 3 then
         begin
           if MessageDlg('O relat�rio poder� demorar alguns minutos! Aguarde...', mtConfirmation, [mbOk], 0) = mrOk then
            Screen.Cursor := crSQLWait;

            Dm2.QDiv1.Close;
            Dm2.QDiv2.Close;
            Dm2.QDiv1.ParamByName('VALOR').Value := Edit1.text;
            Dm2.QDiv1.ParamByName('VALORFIM').Value := '9999999999999999999999';
            Dm2.QDiv1.Open;
            Dm2.QDiv2.Open;
            Dm.RvPrjWis.Close;
            Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Cega2.rav';
            Dm.RvPrjWis.Open;
            Dm.RvSysWis.DefaultDest := rdPreview;
            Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
            Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\cega.pdf';
            Dm.RvPrjWis.ExecuteReport('Cega2');

        // Salvando relat�rio em PDF automaticamente - quantidade
         Try
           Dm.RvSysWis.DefaultDest:= rdFile;
           Dm.RvSysWis.DoNativeOutput:= false;
           Dm.RvSysWis.RenderObject:= rvPdf;
           Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Diverg�ncia Cega_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
           Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
           Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Divergencia.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
           Dm.RvPrjWis.Engine:= Dm.RvSysWis;
           Dm.RvPrjWis.Execute;
         Except
           ShowMessage('Erro ao gerar o relat�rio em PDF');
         end;


            Screen.Cursor := crDefault;
         end
       else

    If RadioGroup1.ItemIndex = 0 then
      begin
       if MessageDlg('O relat�rio poder� demorar alguns minutos! Aguarde...', mtConfirmation, [mbOk], 0) = mrOk then
        Screen.Cursor:= crSQLWait;

        //Dm2.QDiv1.Close;
        //Dm2.QDiv2.Close;
        Dm.IBContagens.Close;
        Dm.IBContagens.Open;
        Dm2.QDivQtd.Close;
        Dm2.QDivQtd1.Close;
        If Edit1.Text <> '' then
         begin
          Dm2.QDivQtd.ParamByName('divergencia').Value := Edit1.Text;
          //Dm2.QDiv1.ParamByName('VALOR').Value := Edit1.Text;
          //Dm2.QDiv1.ParamByName('VALORFIM').Value := '9999999999999999999999';
         end;
        {else
         begin
          Dm2.QDiv1.ParamByName('VALOR').Value := 100;
          Dm2.QDiv1.ParamByName('VALORFIM').Value := '9999999999999999999999';
         end;}

//        Dm2.QDiv1.Open;
//        Dm2.QDiv2.Open;

        Dm2.QDivQtd.Open;
        Dm2.QDivQtd1.Open;
        Dm.RvPrjWis.Close;
        Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Divergencia_Qtd.rav';
        //Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Divergencia2.rav';
        Dm.RvPrjWis.Open;
        Dm.RvSysWis.DefaultDest := rdPreview;
        Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
        Dm.RvPrjWis.ExecuteReport('Divergencia_Qtd');
//        Dm.RvPrjWis.ExecuteReport('Divergencia2);

        // Salvando relat�rio em PDF automaticamente - quantidade
         Try
           Dm.RvSysWis.DefaultDest:= rdFile;
           Dm.RvSysWis.DoNativeOutput:= false;
           Dm.RvSysWis.RenderObject:= rvPdf;
           Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Diverg�ncia por quantidade_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
           Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
         //  Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Divergencia2.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
           Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Divergencia_Qtd.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
           Dm.RvPrjWis.Engine:= Dm.RvSysWis;
           Dm.RvPrjWis.Execute;
         Except
           ShowMessage('Erro ao gerar o relat�rio em PDF');
         end;

        Screen.Cursor := crDefault;
      end
    else
      If RadioGroup1.ItemIndex = 1 then
          begin
           if MessageDlg('O relat�rio poder� demorar alguns minutos! Aguarde...', mtConfirmation, [mbOk], 0) = mrOk then
            Screen.Cursor := crSQLWait;

            Dm2.QDiv1.Close;
            Dm2.QDiv1.Sql.Clear;
            Dm2.QDiv1.SQL.Add('SELECT CA.CAD_CODINT, CO.QTD, CA.CAD_SETOR,CA.CAD_UN,CA.CAD_DESC, CA.CAD_PRECO, CA.CAD_QUANTIDADE, ');
            Dm2.QDiv1.SQL.Add('(CASE  when ((CO.QTD - CA.CAD_QUANTIDADE ) < 1) then ((CO.QTD - CA.CAD_QUANTIDADE ) * -1) ');
            Dm2.QDiv1.SQL.Add('else (CO.QTD - CA.CAD_QUANTIDADE ) end) as DIF_QTD, (CASE when ((CA.CAD_PRECO * (CO.QTD - CA.CAD_QUANTIDADE )) < 1) then ');
            Dm2.QDiv1.SQL.Add('((CA.CAD_PRECO * (CO.QTD - cA.CAD_QUANTIDADE )) * -1) else (CA.CAD_PRECO * (CO.QTD - cA.CAD_QUANTIDADE )) end) as DIF_VALOR, ');
            Dm2.QDiv1.SQL.Add('(CASE when ((CA.CAD_PRECO * (CO.QTD - cA.CAD_QUANTIDADE )) is not null) then ');
            Dm2.QDiv1.SQL.Add('((CA.CAD_PRECO * (CO.QTD - cA.CAD_QUANTIDADE )) * 1) else ');
            Dm2.QDiv1.SQL.Add('(CA.CAD_PRECO * (CO.QTD - cA.CAD_QUANTIDADE ) * 1) end) as DIF_VALOR2, ');
            Dm2.QDiv1.SQL.Add('(CASE WHEN (CO.QTD = CA.CAD_QUANTIDADE) THEN ');
            Dm2.QDiv1.SQL.Add('(CO.QTD) ELSE (SUM(CO.QTD))END) AS QTDWIS ');
            Dm2.QDiv1.SQL.Add('from CONTAGENS_DIV CO INNER JOIN CADASTRO_CONSO CA on (CA.CAD_CODINT = CO.CAD_CODINT) ');
            Dm2.QDiv1.SQL.Add(' where (CO.QTD >= :DIF_QTD) ');
            Dm2.QDiv1.SQL.Add('and (ca.cad_Setor >= :cad_setor) and  (ca.cad_setor <= :cad_setorFim) ');
            Dm2.QDiv1.SQL.Add('GROUP BY CA.CAD_CODINT, CO.QTD, CA.CAD_DESC, CA.CAD_PRECO, CA.CAD_QUANTIDADE,CA.CAD_SETOR,CA.CAD_UN ');
            Dm2.QDiv1.SQL.Add('ORDER BY DIF_VALOR desc, dif_qtd DESC, CA.CAD_SETOR desc ');
            If CmbSetor.Text = '' then
            ShowMessage('� preciso selecionar um setor')
            else
              begin
                Dm2.QDiv1.ParamByName('CAD_SETOR').Value    := CmbSetor.Text;
                Dm2.QDiv1.ParamByName('CAD_SETORFim').Value := CmbSetorFim.Text;
              end;
            If Edit1.Text <> '' then
            Dm2.QDiv1.ParamByName('DIF_QTD').Value := Edit1.Text
            else
            Dm2.QDiv1.ParamByName('DIF_QTD').Value := 100;
            Dm2.QDiv1.Open;
            Dm2.QDiv2.Open;
            Dm.RvPrjWis.Close;
            Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Divergencia2.rav';
            Dm.RvPrjWis.Open;
            Dm.RvSysWis.DefaultDest := rdPreview;
            Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
            Dm.RvPrjWis.ExecuteReport('Divergencia2');

        // Salvando relat�rio em PDF automaticamente - quantidade
         Try
           Dm.RvSysWis.DefaultDest:= rdFile;
           Dm.RvSysWis.DoNativeOutput:= false;
           Dm.RvSysWis.RenderObject:= rvPdf;
           Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Diverg�ncia por departamento_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
           Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
           Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Divergencia.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
           Dm.RvPrjWis.Engine:= Dm.RvSysWis;
           Dm.RvPrjWis.Execute;
         Except
           ShowMessage('Erro ao gerar o relat�rio em PDF');
         end;

            Screen.Cursor := crDefault;
          end
      else
        If RadioGroup1.ItemIndex = 2 then
         if not CheckBoxDiv.Checked then
          begin
           if MessageDlg('O relat�rio poder� demorar alguns minutos! Aguarde...', mtConfirmation, [mbOk], 0) = mrOk then
             Screen.Cursor := crSQLWait;


             Dm2.QDivZerados.Close;
            if (CmbSetor.Text = '') and (CmbSetorFim.Text = '') then
              begin
              ShowMessage('Esse cliente n�o possui setores cadastrados, clique em "SEM DEPARTAMENTO"');
              Dm2.QDivZerados.ParamByName('CAD_SETOR').Value    :=0;
              Dm2.QDivZerados.ParamByName('CAD_SETORFIM').Value :=0;

              end
            else
               begin
                  Dm2.QDivZerados.ParamByName('CAD_SETOR').Value    := CmbSetor.Text;
                  Dm2.QDivZerados.ParamByName('CAD_SETORFIM').Value := CmbSetorFim.Text;
               end;
              If Edit1.Text <> '' then
              Dm2.QDivZerados.ParamByName('CAD_QUANTIDADE').Value := Edit1.Text
              else
              Dm2.QDivZerados.ParamByName('CAD_QUANTIDADE').Value :=0;

              Dm2.QDivZerados.Open;
              Dm.RvPrjWis.Close;
              Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Rel_DivZerados2.rav';
              Dm.RvPrjWis.Open;
              Dm.RvSysWis.DefaultDest := rdPreview;
              Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
              Dm.RvPrjWis.ExecuteReport('Rel_DivZerados2');

        // Salvando relat�rio em PDF automaticamente - n�o contados
         Try
           Dm.RvSysWis.DefaultDest:= rdFile;
           Dm.RvSysWis.DoNativeOutput:= false;
           Dm.RvSysWis.RenderObject:= rvPdf;
           Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Diverg�ncia itens nao contados_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
           Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
           Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Divergencia.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
           Dm.RvPrjWis.Engine:= Dm.RvSysWis;
           Dm.RvPrjWis.Execute;
         Except
           ShowMessage('Erro ao gerar o relat�rio em PDF');
         end;


              Screen.Cursor := crDefault;
          end


         else
            begin
             if MessageDlg('O relat�rio poder� demorar alguns minutos! Aguarde...', mtConfirmation, [mbOk], 0) = mrOk then
             Screen.Cursor := crSQLWait;

             Dm2.QDivZerados.Close;
             Dm2.QDivZerados.SQL.Clear;
             Dm2.QDivZerados.SQL.Add('SELECT CO.QTD, CA.CAD_CODINT,  CA.CAD_DESC, CA.CAD_QUANTIDADE, CO.CON_EAN, CA.CAD_SETOR,(CA.CAD_PRECO), CA.CAD_EAN, CA.CAD_UN');
             Dm2.QDivZerados.SQL.Add('FROM CONTAGENS_CONSO CO RIGHT join CADASTRO_CONSO CA on (CA.CAD_CODINT = CO.CAD_CODINT)');
             Dm2.QDivZerados.SQL.Add('where (CA.CAD_QUANTIDADE >= :CAD_QUANTIDADE) and (CO.QTD IS NULL) and (ca.cad_quantidade > 0)');
             Dm2.QDivZerados.SQL.Add('ORDER BY CA.CAD_QUANTIDADE DESC');
             If Edit1.Text <> '' then
              Dm2.QDivZerados.ParamByName('CAD_QUANTIDADE').Value := Edit1.Text
             else
              Dm2.QDivZerados.ParamByName('CAD_QUANTIDADE').Value := 0;
             Dm2.QDivZerados.Open;

             Dm.RvPrjWis.Close;
             Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Rel_DivZerados2.rav';
             Dm.RvPrjWis.Open;
             Dm.RvSysWis.DefaultDest := rdPreview;
             Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
             Dm.RvPrjWis.ExecuteReport('Rel_DivZerados2');
        // Salvando relat�rio em PDF automaticamente - n�o contados
         Try
           Dm.RvSysWis.DefaultDest:= rdFile;
           Dm.RvSysWis.DoNativeOutput:= false;
           Dm.RvSysWis.RenderObject:= rvPdf;
           Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Diverg�ncia itens nao contados_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
           Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
           Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Divergencia.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
           Dm.RvPrjWis.Engine:= Dm.RvSysWis;
           Dm.RvPrjWis.Execute;
         Except
           ShowMessage('Erro ao gerar o relat�rio em PDF');
         end;



             Screen.Cursor := crDefault;

            end
        else
           If RadioGroup1.ItemIndex = 4 then
              begin
               if MessageDlg('O relat�rio poder� demorar alguns minutos! Aguarde...', mtConfirmation, [mbOk], 0) = mrOk then
                Screen.Cursor := crSQLWait;


                Dm2.QDivEndereco.Close;
                If (Edit1.Text <> '') and (Edt_ValorFinal.Text <> '') then
                  begin
                    Dm2.QDivEndereco.ParamByName('DIF_QTD').Value      := Edit1.Text;
                    Dm2.QDivEndereco.ParamByName('DIF_QTDFINAL').Value := Edt_ValorFinal.Text;
                  end
                else
                  begin
                    Dm2.QDivEndereco.ParamByName('DIF_QTD').Value := 100;
                    Dm2.QDivEndereco.ParamByName('DIF_QTDFINAL').Value := 100;
                  end;
                  
                    Dm2.QDivEndereco.Open;
                    Dm.RvPrjWis.Close;
                    Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\RelDivEndereco2.rav';
                    Dm.RvPrjWis.Open;
                    DM.RvPrjWis.SetParam('ValorMin','VLR. MIN. :  ' + edit1.Text);
                    DM.RvPrjWis.SetParam('ValorMax','VLR. MAX. :  ' + Edt_ValorFinal.Text);
                    Dm.RvSysWis.DefaultDest := rdPreview;
                    Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
                    Dm.RvPrjWis.ExecuteReport('RelDivEndereco3');

        // Salvando relat�rio em PDF automaticamente - quantidade
         Try
           Dm.RvSysWis.DefaultDest:= rdFile;
           Dm.RvSysWis.DoNativeOutput:= false;
           Dm.RvSysWis.RenderObject:= rvPdf;
           Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Diverg�ncia por Endereco_Secao_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
           Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
           Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\Divergencia.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
           Dm.RvPrjWis.Engine:= Dm.RvSysWis;
           Dm.RvPrjWis.Execute;
         Except
           ShowMessage('Erro ao gerar o relat�rio em PDF');
         end;



                 Screen.Cursor := crDefault;
              end;



  Except
  ShowMEssage('Erro ao gerar a diverg�ncia');
  End;
end;

procedure TFrmDivergenciaQtd.RadioGroup1Click(Sender: TObject);
begin
If RadioGroup1.ItemIndex = 0 then
  begin
    Edt_ValorFinal.Visible:=False; // Desabilita o a caixa de texto valor final
    Lbl_ValorFinal.Visible:=False; // Desabilita a o label valor final
    Label2.Visible:=True;
    label3.Visible:=False;
    Label4.Visible:=False;
    Edit1.Visible := True;
    CmbSetor.Visible := False;
    CmbSetorFim.Visible:=False;
    CheckBoxDiv.Visible := False;
    Label2.Caption := 'Quantidade M�nima: ';
  End
else
  If RadioGroup1.ItemIndex = 1 then
    begin
      Edt_ValorFinal.Visible:=False; // Desabilita o a caixa de texto valor final
      Lbl_ValorFinal.Visible:=False; // Desabilita a o label valor final
      Label2.Visible:=True;
      label3.Visible:=True;
      Label4.Visible:=True;
      CmbSetor.Visible := true;
      CmbSetorFim.Visible:= True;
      CheckBoxDiv.Visible := False;
      Edit1.Visible := true;
      Label2.Caption := 'Quantidade M�nima: ';
      CmbSetor.Items.Clear;
      CmbSetorFim.Items.Clear;
      Dm.Qsetor.Close;
      Dm.Qsetor.Open;
      Dm.Qsetor.First;
      Try While Not Dm.Qsetor.Eof do
        begin
          CmbSetor.Items.Add(Dm.QsetorCAD_SETOR.Value);
          CmbSetorFim.Items.Add(Dm.QsetorCAD_SETOR.Value);
          Dm.Qsetor.Next;
        End;
      Except
      End;
    End
Else
  If RadioGroup1.ItemIndex = 2 Then
    Begin
      Edt_ValorFinal.Visible:=False; // Desabilita o a caixa de texto valor final
      Lbl_ValorFinal.Visible:=False; // Desabilita a o label valor final

     CheckBoxDiv.Visible := True;

        if CheckBoxDiv.Checked then
         begin
         Label2.Caption := 'Quantidade M�nima:  ';
         CmbSetor.Visible := False;
         Label3.Visible := False;
         end
        else
          Label2.Caption := 'Quantidade M�nima:  ';
          Label2.visible:=True;
          Label3.Visible:=True;
          Label4.Visible:=True;
          Edit1.Visible:=True;
          CmbSetor.Visible:=True;
          CmbSetorFim.Visible:=True;
          CmbSetor.Items.Clear;
          CmbSetorFim.Items.Clear;
          Dm.Qsetor.Close;
          Dm.Qsetor.Open;
          Dm.Qsetor.First;
      Try While Not Dm.Qsetor.Eof do
        begin
          CmbSetor.Items.Add(Dm.QsetorCAD_SETOR.Value);
          CmbSetorFim.Items.Add(Dm.QsetorCAD_SETOR.Value);
          Dm.Qsetor.Next;
        End;
      Except
      End;
    End
  else
If RadioGroup1.ItemIndex = 3 then
  begin
    Edt_ValorFinal.Visible:=False;// Desabilita o a caixa de texto valor final
    Lbl_ValorFinal.Visible:=False;// Desabilita a o label valor final
    Label2.Visible:=True;
    label3.Visible:=False;
    Label4.Visible:=False;
    Edit1.Visible := True;
    CmbSetor.Visible := False;
    CmbSetorFim.Visible:=False;
    CheckBoxDiv.Visible := False;
    Label2.Caption := 'Diverg�ncia Cega : ';
  End
else
   If RadioGroup1.ItemIndex = 4 then
  begin
    Edt_ValorFinal.Visible:=True;// habilita a caixa de texto do valor final para a diverg�ncia por endere�o
    Lbl_ValorFinal.Visible:=True; // habilita o label valor final para a diverg�ncia por endere�o
    Label2.Visible:=True;
    label3.Visible:=False;
    Label4.Visible:=False;
    Edit1.Visible := True;
    CmbSetor.Visible := False;
    CmbSetorFim.Visible:=False;
    CheckBoxDiv.Visible := False;
    Label2.Caption := 'Quantidade M�nima: ';
  End
end;

procedure TFrmDivergenciaQtd.CmbSetorFimClick(Sender: TObject);
begin
if CmbSetorFim.Text < CmbSetor.Text   then
          begin
           ShowMessage('O Setor final n�o pode ser menor que o setor inicial');
          end;
end;

procedure TFrmDivergenciaQtd.CmbSetorClick(Sender: TObject);
begin
if (CmbSetorFim.Text <> '') and (CmbSetorFim.Text < CmbSetor.Text) then
          begin
           ShowMessage('O Setor final n�o pode ser menor que o setor inicial');
          end;
end;

end.
