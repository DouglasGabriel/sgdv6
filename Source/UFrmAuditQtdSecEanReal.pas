unit UFrmAuditQtdSecEanReal;

interface

uses
   Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ComCtrls, ExtCtrls, sBitBtn, RpDefine, RpRender,
  RpRenderPDF, RpRenderText, DBCtrls, sLabel;

type
  TFrmAuditQtdSecEanReal = class(TForm)
    Panel2: TPanel;
    Panel3: TPanel;
    Label2: TLabel;
    Lbl_ValorFinal: TLabel;
    Edit1: TEdit;
    sBitBtn1: TsBitBtn;
    Edit2: TEdit;
    StatusBar1: TStatusBar;
    rvTxt: TRvRenderText;
    rvPdf: TRvRenderPDF;
    sLabelFX1: TsLabelFX;
    procedure sBitBtn1Click(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure Edit2KeyPress(Sender: TObject; var Key: Char);
    procedure Edit1Exit(Sender: TObject);
    procedure Edit2Exit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmAuditQtdSecEanReal: TFrmAuditQtdSecEanReal;

implementation

uses UDm, UInventario;

{$R *.dfm}

procedure TFrmAuditQtdSecEanReal.sBitBtn1Click(Sender: TObject);
var NivelUsu :Integer;
begin
Try
  try

    If (Edit1.Text <> '') and  (Edit2.Text <> '') then
      begin
       if StrToInt(Edit2.Text) < StrToInt(Edit1.Text) then
        begin
         ShowMessage('A Quantidade inicial deve ser menor que quantidade final. Digite um intervalo v�lido');
         Edit1.Clear;
         Edit2.Clear;
         Edit1.SetFocus;
        end
       else
        begin
          Dm.QRelAudQtdSecEan.Close;
          Dm.QRelAudQtdSecEan.ParamByName('QTDINI').Value := Edit1.Text;
          Dm.QRelAudQtdSecEan.ParamByName('QTDFIM').Value := Edit2.Text;
          Dm.QRelAudQtdSecEan.Open;

          if Dm.QRelAudQtdSecEan.RecordCount > 0 then
            begin
             Dm.RvPrjWis.Close;
             Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\AuditoriaEanSecValor.rav';
             Dm.RvPrjWis.Open;
             Dm.RvSysWis.DefaultDest := rdPreview;
             Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
             Dm.RvPrjWis.ExecuteReport('RelEanSecQtd');

              // Salvando relat�rio em PDF automaticamente
              Try
                Dm.RvSysWis.DefaultDest:= rdFile;
                Dm.RvSysWis.DoNativeOutput:= false;
                Dm.RvSysWis.RenderObject:= rvPdf;
                Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Relatorio de Auditoria Se��o por faixa de Quantidade_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
                Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
                Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\AuditoriaEanSecValor.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
                Dm.RvPrjWis.Engine:= Dm.RvSysWis;
                Dm.RvPrjWis.Execute;
              Except
                ShowMessage('Erro ao gerar o relat�rio em PDF');
              end;

              // Salvando relat�rio em TXT automaticamente
              Try
                Dm.RvSysWis.DefaultDest:= rdFile;
                Dm.RvSysWis.DoNativeOutput:= false;
                Dm.RvSysWis.RenderObject:= rvTxt;
                Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Relatorio de Auditoria Se��o por faixa de Quantidade_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.txt'; //caminho onde vai gerar o arquivo pdf
                Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
                Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\AuditoriaEanSecValor.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
                Dm.RvPrjWis.Engine:= Dm.RvSysWis;
                Dm.RvPrjWis.Execute;
              except
                on e: exception do
                  ShowMessage('Erro ao gerar o relat�rio em TXT: ' + e.ClassName + ' disparou "' + e.Message + '"');
              end;


            end
          else
            begin
             Application.MessageBox('N�o existem produtos nessa faixa de quantidade','Mensagem do sistema', MB_ICONEXCLAMATION+ MB_OK);
            end;
        end;
      end
    else
      Begin
       ShowMessage('N�o foi informado o intervalo de quantidades. O sistema ir� atribuir quantidades de 1 at� 100');
       Dm.QRelAudQtdSecEan.Close;
       Dm.QRelAudQtdSecEan.ParamByName('QTDINI').Value := 1;
       Dm.QRelAudQtdSecEan.ParamByName('QTDFIM').Value := 100;
       Dm.QRelAudQtdSecEan.Open;

        if Dm.QRelAudQtdSecEan.RecordCount > 0 then
         begin
          Dm.RvPrjWis.Close;
          Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\AuditoriaEanSecValor.rav';
          Dm.RvPrjWis.Open;
          Dm.RvSysWis.DefaultDest := rdPreview;
          Dm.RvSysWis.SystemSetups := Dm.RvSysWis.SystemSetups - [ssAllowSetup];
          Dm.RvPrjWis.ExecuteReport('RelEanSecQtd');

                      // Salvando relat�rio em PDF automaticamente
            Try
              Dm.RvSysWis.DefaultDest:= rdFile;
              Dm.RvSysWis.DoNativeOutput:= false;
              Dm.RvSysWis.RenderObject:= rvPdf;
              Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Relatorio de Auditoria Se��o por faixa de Quantidade_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.pdf'; //caminho onde vai gerar o arquivo pdf
              Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
              Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\AuditoriaEanSecValor.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
              Dm.RvPrjWis.Engine:= Dm.RvSysWis;
              Dm.RvPrjWis.Execute;
            Except
              ShowMessage('Erro ao gerar o relat�rio em PDF');
            end;

            // Salvando relat�rio em TXT automaticamente
            Try
              Dm.RvSysWis.DefaultDest:= rdFile;
              Dm.RvSysWis.DoNativeOutput:= false;
              Dm.RvSysWis.RenderObject:= rvTxt;
              Dm.RvSysWis.OutputFileName:= 'C:\SGD\Relatorios\Relatorio de Auditoria Se��o por faixa de Quantidade_'+Dm.IBInventarioINV_LOJA.AsString+'_'+Dm.IBInventarioINV_NUMLOJA.AsString+'.txt'; //caminho onde vai gerar o arquivo pdf
              Dm.RvSysWis.SystemSetups:= Dm.RvSysWis.SystemSetups -[ssAllowSetup];
              Dm.RvPrjWis.projectfile:= 'C:\SGD\Rave\AuditoriaEanSecValor.rav'; //caminho onde esta o arquivo que ele vai exportar para pdf
              Dm.RvPrjWis.Engine:= Dm.RvSysWis;
              Dm.RvPrjWis.Execute;
            except
              ShowMessage('Erro ao gerar o relat�rio em TXT');
            end;

            
         end
        else
            begin
             Application.MessageBox('N�o existem produtos nessa faixa de quantidade','Mensagem do sistema', MB_ICONEXCLAMATION+ MB_OK);
            end;
      end;

  except
     Application.MessageBox('Erro ao gerar relat�rio','Mensagem do sistema', MB_ICONERROR+ MB_OK);
  end;

Finally
   Dm.QRelAudQtdSecEan.Close;
End;
end;

procedure TFrmAuditQtdSecEanReal.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',#8]) then
         begin
          key:=#0;
          beep;
         end;
end;

procedure TFrmAuditQtdSecEanReal.Edit2KeyPress(Sender: TObject; var Key: Char);
begin
if not (key in ['0'..'9',#8]) then
   begin
     key:=#0;
     beep;
   end;
end;

procedure TFrmAuditQtdSecEanReal.Edit1Exit(Sender: TObject);
begin
 if not (Edit2.Text = '') then
  begin
   if StrToInt(Edit2.Text) < StrToInt(Edit1.Text) then
      begin
       ShowMessage('A Quantidade inicial deve ser menor que quantidade final. Digite um intervalo v�lido');
       Edit1.Clear;
       Edit2.Clear;
       Edit1.SetFocus;
      end
  end;
end;

procedure TFrmAuditQtdSecEanReal.Edit2Exit(Sender: TObject);
begin
 if (StrToInt(Edit2.Text) < StrToInt(Edit1.Text)) then
    begin
     ShowMessage('A Quantidade inicial deve ser menor que quantidade final. Digite um intervalo v�lido');
     Edit1.Clear;
     Edit2.Clear;
     Edit1.SetFocus;
    end
end;

end.
